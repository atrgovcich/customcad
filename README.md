# CAD Panel

This project was created as a simple pet project that over time snowballed into what it is today.  This is the work of a single author (myself) and is in no way well written.  I never imagined that I would ever release this code, as it was more of a sandbox test bed for my c# development.  That being said, I have released the CadPanel control as a nuget package (https://www.nuget.org/packages/CadPanel/0.0.2) and it can be used in any Windows Forms app running Net 4.7.2 or later.

# Description

The CAD Panel is a Windows Forms control that can be added to any Windows Forms app with some simple programmatic language.  It is meant as a basic 2-D drawing utliity to allow user drafting input on a form.

## Features

The following features are included in the drafting component:
<ul>
  <li>Create primitive shapes including lines, polylines, polygons, circles, and arcs</li>
  <li>Copy/paste objects, including with base points</li>
  <li>Move and rotate objects</li>
  <li>Reshape objects by dragging vertices</li>
  <li>Snapping (end point, perpenduclar, quadrant, etc) and control over snaps and tolerances</li>
  <li>Layers to control line color and width</li>
  <li>Boolean operations through the use of PolyBoolCS</li>
  <li>Delaunay triangulation using Triangle.NET</li>
  <li>Basic geometric properties of closed shapes</li>
  <li>Numerous others</li>
</ul>

In addition to the basic drafting capabilities, CAD Panel contains a fiber section analysis suite that allows for moment - curvature and axial force - moment analysis of sections.  This is a common utility in structural analysis and design.  The fiber section analysis includes the following features:

<ul>
  <li>Non-linear material templates for concrete and steel materials</li>
  <li>Rebar definitions, and ability to add custom defintions</li>
  <li>Prestressing steel sections, and full support for prestressed (or post-tensioned) analysis</li>
  <li>Control over mesh size and quality</li>
  <li>Ability to analyze at multiple load combinations and angles</li>
  <li>Visual stress / strain representations using the CAD Panel itself</li>
  <li>Ability to query any element at any step in a moment - curvature analysis</li>
</ul>

# How to Use

Unfortunately you cannot simply drag-and-drop the CAD Panel onto a form...yet.  There is some issue with this and it will crash.  Instead, it should be added programatically.  Follow the steps given below to succesfully implement on a form.

### SetStyle in Form Constructor

To allow the custom painting to occur, you must add two lines of code to your Form's constructor:

```
public Form1()
{
    SetStyle(
        ControlStyles.AllPaintingInWmPaint |
        ControlStyles.UserPaint, true);

    SetStyle(ControlStyles.ResizeRedraw, true);

    InitializeComponent();
}
```

### Create Panel Initialization

An instance of the CAD Panel needs to be created and kept as a variable in your Form.  It is also a good idea to set up minimum dimensions of your form so that the panel doesn't get oddly sized as the form is resized.  When you initialize, set the CAD Panel size relative to the Form's current size.

```
private CadPanelControl _cadPanel;
private int _minFormWidth = 400;
private int _minFormHeight = 400;

private void InitializeCadPanel()
{
    _cadPanel = new CadPanelControl();
    _cadPanel.Name = "CadPanel";

    Point location = new Point();
    location.X = 10;
    location.Y = 10;

    this.MinimumSize = new Size(_minFormWidth, _minFormHeight);

    _cadPanel.Location = location;
    _cadPanel.Size = new Size(this.Width - 40, this.Height - 60);

    this.Controls.Add(_cadPanel);
}
```

### Call the CAD Panel Initializer from Form.Load

Next the initializing function needs to be called.  This is usually done on Form.Load().  A Form.Resize event should also be added so that every time the form is resized, the CAD Panel is also resized.

```
private void Form1_Load(object sender, EventArgs e)
{
    this.Resize += new EventHandler(ResizeAllControls);
    InitializeCadPanel();
}
```

### Create a Resize Event for the Form and Method to Resize Panel

Make sure that your resize event references an actual method, and that method should then resize the CAD Panel:

```
private void ResizeAllControls(object sender, EventArgs e)
{
    ResizeCadPanel();
    this.Invalidate(); //Need to invalidate because we set the userpaint option
}

private void ResizeCadPanel()
{
    _cadPanel.Size = new Size(this.Width - 40, this.Height - 60);
}
```

### (Optional) Create Save and Load Functions

This is optional.  Once all of the above steps are complete, the panel should be ready for use.  The CAD Panel contains functions that will return save files, and functions that can be passed save files to load a file.  Create save and load functions as shown below (link to buttons):

```
private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
{
    String fPath = SaveFileBrowser();

    if (fPath != null)
    {
        byte[] serData = _cadPanel.SavePanelInstanceData();

        File.WriteAllBytes(fPath, serData);
    }
}

private string SaveFileBrowser()
{
    SaveFileDialog folderBrowser = new SaveFileDialog();

    String savePath = null;

    folderBrowser.Title = "Save File";

    folderBrowser.OverwritePrompt = true;

    folderBrowser.Filter = "CPAN Files (.cpan)|*.cpan";

    if (folderBrowser.ShowDialog() == DialogResult.OK)
    {
        savePath = folderBrowser.FileName;
    }

    return savePath;
}

private void openToolStripMenuItem_Click(object sender, EventArgs e)
{
    string fPath = OpenFileBrowser();

    if (fPath != null)
    {
        byte[] serData = File.ReadAllBytes(fPath);

        if (serData != null)
        {
            _cadPanel.LoadPanelInstanceData(serData);
        }
    }
}

private string OpenFileBrowser()
{
    SaveFileDialog folderBrowser = new SaveFileDialog();

    string openPath = null;

    folderBrowser.Title = "Open File";

    folderBrowser.OverwritePrompt = false;

    folderBrowser.Filter = "CPAN Files (.cpan)|*.cpan";

    if (folderBrowser.ShowDialog() == DialogResult.OK)
    {
        openPath = folderBrowser.FileName;
    }

    return openPath;
}
```

The save files can be retrieved in either a JSON format (can become VERY large) or a compressed JSON (binary) format that creates manageable file sizes even with large numbers of objects defined.