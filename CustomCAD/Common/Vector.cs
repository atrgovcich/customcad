﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace CustomCAD.Common
{
    public class Vector
    {
        #region Public Properties
        public double X { get; set; }
        public double Y { get; set; }
        #endregion

        #region Constructors
        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the length of the vector
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            if (X == 0 && Y == 0)
            {
                return 0;
            }
            else
            {
                return Sqrt(Pow(X, 2) + Pow(Y, 2));
            }
        }

        /// <summary>
        /// Returns the unit vector of the given vector
        /// </summary>
        /// <returns></returns>
        public Vector UnitVector()
        {
            double length = this.Length();

            return new Vector(X / length, Y / length);
        }

        /// <summary>
        /// Returns a new vector perpendicular to the given vector
        /// </summary>
        /// <returns></returns>
        public Vector PerpendicularVector()
        {
            return new Vector(Y, -X);
        }
        #endregion
    }
}
