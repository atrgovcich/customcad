﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;
using System.Drawing;

namespace CustomCAD.Common
{
    public class PointD
    {
        public double X { get; set; }
        public double Y { get; set; }

        private const int ROUND = 4;
        private const double COINCIDENCE_TOLERANCE = 1.0E-6;

        #region Constructors
        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion
        public double DistanceTo(PointD other)
        {
            return Sqrt(Pow(other.X - X, 2) + Pow(other.Y - Y, 2));
        }

        public double GetAngleFromPoint(PointD other)
        {
            double angle = 0;

            if (Round(X - other.X, ROUND) == 0)
            {
                if (Y > other.Y)
                {
                    angle = PI / 2.0;
                }
                else if (Y < other.Y)
                {
                    angle = 3.0 * PI / 2.0;
                }
            }
            else if (Round(Y - other.Y, ROUND) == 0)
            {
                if (X > other.X)
                {
                    angle = 0;
                }
                else if (X < other.X)
                {
                    angle = PI;
                }
            }
            else
            {
                double tempangle = Atan((Y - other.Y) / (X - other.X));

                if (tempangle >= 0)
                {
                    if (Y >= other.Y)
                    {
                        //quadrant 1
                        angle = tempangle;
                    }
                    else
                    {
                        //quadrant 3
                        angle = PI + tempangle;
                    }
                }
                else
                {
                    if (Y >= other.Y)
                    {
                        //quadrant 2
                        angle = PI + tempangle;
                    }
                    else
                    {
                        //quadrant 4
                        angle = 2d * PI + tempangle;
                    }
                }
            }

            return angle;
        }

        public Point ConvertToPixelPoint(PointD zeroPoint, double drawingScale)
        {
            try
            {
                int px = Convert.ToInt32(zeroPoint.X + X * drawingScale);
                int py = Convert.ToInt32(zeroPoint.Y - Y * drawingScale);

                Point pixelPoint = new Point(px, py);

                return pixelPoint;
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to convert from a drawing point to canvas pixel coordinates." + Environment.NewLine + ex.Message);
            }
        }

        public static bool operator ==(PointD p1, PointD p2)
        {
            if (Abs(p1.X - p2.X) <= COINCIDENCE_TOLERANCE)
            {
                if (Abs(p1.Y - p2.Y) <= COINCIDENCE_TOLERANCE)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool operator !=(PointD p1, PointD p2)
        {
            if (p1 == p2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
