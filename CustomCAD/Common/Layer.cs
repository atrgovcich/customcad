﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CustomCAD.Common
{
    public class Layer
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public Color Color { get; set; }
        public int Width { get; set; }

    }
}
