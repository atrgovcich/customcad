﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using CustomCAD.DrawingElements;
using System.Drawing;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;

namespace CustomCAD.Common
{
    public static class GlobalStorageContainer
    {
        #region Selection Properties
        public static Color SelectedObjectLineColor;
        public static Color SelectedObjectPointColor;
        public static int SelectedObjectPointWidth;
        public static int SelectedObjectLineWidth;
        public static bool DrawVerticesOfMultipleObjects;
        #endregion

        #region Canvas Properties
        public static int CanvasWidth = 1200;
        public static int CanvasHeight = 600;
        public static Point BitmapInsertionPoint = new Point(0, 0);
        
        #endregion

        #region Drawing Properties
        public static double BaseDrawingScale = 20;
        public static double DrawingScale = 20;
        public static PointD ZeroPoint = new PointD(0, 0);
        public static double MagnificationLevel = 1.0;
        public static int WheelClicks = 0;
        public static double PreviousMagnificationLevel = 1.0;
        public static Rectangle MagnifyRectangle = new Rectangle(0, 0, CanvasWidth, CanvasHeight);
        public static int RefreshRate = 25;
        #endregion

        #region Layer Properties
        public static List<Layer> Layers;
        #endregion

        #region Saving and Opening
        public static string StartPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        #endregion

        #region Form Management
        public static List<Form> FormList = new List<Form>();
        #endregion

        #region Drawing Element Management
        public static List<IDrawingElement> DrawingElementList = new List<IDrawingElement>();
        public static List<List<IDrawingElement>> PreviousDrawingStates = new List<List<IDrawingElement>>();
        #endregion

        #region Object Selection
        public static double BaseSelectionDistance = 1.0;
        public static double SelectionDistance = 1.0;
        #endregion

        #region Snap Properties
        public static double BaseSnapDistance = 1.0;
        public static double SnapDdistance = 1.0;
        #endregion


    }
}
