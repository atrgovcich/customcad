﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using System.Drawing;

namespace CustomCAD.Extensions
{
    public static class PointExtensions
    {
        public static PointD ConvertToGlobalCoord(this Point p, Point zeroPoint, double drawingScale)
        {
            double x = (p.X - zeroPoint.X) / drawingScale;
            double y = (zeroPoint.Y - p.Y) / drawingScale;

            return new PointD(x, y);
        }
    }
}
