﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomCAD.ConcreteElements
{
    public class RebarDefinition
    {
        public double Diameter { get; set; }
        public double Area { get; set; }
        public string Name { get; set; }
    }
}
