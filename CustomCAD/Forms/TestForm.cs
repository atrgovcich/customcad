﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPanel;
using System.Drawing.Drawing2D;
using System.Diagnostics.Contracts;
using System.IO;

namespace CustomCAD.Forms
{
    public partial class TestForm : Form
    {
        #region Private Form Properties
        private int _minFormWidth = 400;
        private int _minFormHeight = 400;
        #endregion

        private CadPanelControl cadPanel;

        public TestForm()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint, true);

            SetStyle(ControlStyles.ResizeRedraw, true);

            InitializeComponent();
        }

        private void TestForm_Load(object sender, EventArgs e)
        {
            this.Size = new Size(800, 600);
            this.Resize += new EventHandler(ResizeAllControls);
            //this.Paint += new PaintEventHandler(TestForm_Paint);
            InitializeCadPanel();
        }

        private void InitializeCadPanel()
        {
            cadPanel = new CadPanelControl();
            cadPanel.Name = "CadPanel";

            Point location = new Point();
            location.X = 10;
            location.Y = menuStrip1.Location.Y + menuStrip1.Height + 8;

            this.MinimumSize = new Size(_minFormWidth, _minFormHeight);

            cadPanel.Location = location;
            cadPanel.Size = new Size(this.Width - 40, this.Height - 60 - menuStrip1.Height);

            this.Controls.Add(cadPanel);
        }

        private void ResizeAllControls(object sender, EventArgs e)
        {
            ResizeCadPanel();
            this.Invalidate();
        }
        private void ResizeCadPanel()
        {
            cadPanel.Size = new Size(this.Width - 40, this.Height - 60 - menuStrip1.Height);

        }

        #region Paint Overrides

        //public bool TransparentBackground { get; set; } = false;
        //public bool ClipChildControls { get; set; } = true;

        //protected override void OnPaint(PaintEventArgs pe)
        //{
        //    base.OnPaint(pe);
        //}

        //protected override void OnPaintBackground(PaintEventArgs pevent)
        //{
        //    if (TransparentBackground == true)
        //    {
        //        base.OnPaintBackground(pevent);
        //    }
        //    else
        //    {
        //        if (ClipChildControls == false)
        //        {
        //            base.OnPaintBackground(pevent);
        //        }
        //        else
        //        {
        //            if (BackgroundImage != null)
        //            {
        //                base.OnPaintBackground(pevent);
        //            }
        //            else
        //            {
        //                Contract.Requires(pevent != null);
        //                // We need the true client rectangle as clip rectangle causes
        //                // problems on "Windows Classic" theme.  
        //                Rectangle rect = this.ClientRectangle;
        //                PaintBackgroundNoChildren(pevent, rect, BackColor);
        //            }
        //        }
        //    }
        //}

        //protected void PaintBackgroundNoChildren(PaintEventArgs e, Rectangle rectangle, Color backColor)
        //{
        //    List<Rectangle> clippedRectangles = ChildContrlsClientRectangles();

        //    PaintClippedBackground(e, rectangle, clippedRectangles, backColor);
        //}

        //protected void PaintClippedBackground(PaintEventArgs e, Rectangle clientRectangle, List<Rectangle> clippedRectangles, Color backColor)
        //{
        //    // Common case of just painting the background.  For this, we
        //    // use GDI because it is faster for simple things than creating
        //    // a graphics object, brush, etc.  Also, we may be able to
        //    // use a system brush, avoiding the brush create altogether.
        //    //
        //    Color color = backColor;


        //    GraphicsPath gp = new GraphicsPath();
        //    gp.AddRectangle(clientRectangle);
        //    Region r = new Region(gp);

        //    foreach (Rectangle hole in clippedRectangles)
        //    {
        //        var gpe = new GraphicsPath();
        //        gpe.AddRectangle(hole);
        //        r.Exclude(gpe);
        //        gpe.Dispose();
        //    }

        //    gp.Dispose();

        //    using (Brush brush = new SolidBrush(color))
        //    {
        //        e.Graphics.FillRegion(brush,r);
        //    }
        //}

        //protected List<Rectangle> ChildContrlsClientRectangles()
        //{
        //    List<Rectangle> clientRectangles = new List<Rectangle>();

        //    foreach (Control c in Controls)
        //    {
        //        clientRectangles.Add(c.Bounds);
        //    }

        //    return clientRectangles;
        //}
        #endregion

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String fPath = SaveFileBrowser();

            if (fPath != null)
            {
                byte[] serData = cadPanel.SavePanelInstanceData();

                File.WriteAllBytes(fPath, serData);
            }
        }

        private string SaveFileBrowser()
        {
            SaveFileDialog folderBrowser = new SaveFileDialog();

            String savePath = null;

            folderBrowser.Title = "Save File";

            folderBrowser.OverwritePrompt = true;

            folderBrowser.Filter = "CPAN Files (.cpan)|*.cpan";

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                savePath = folderBrowser.FileName;
            }

            return savePath;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fPath = OpenFileBrowser();

            if (fPath != null)
            {
                byte[] serData = File.ReadAllBytes(fPath);

                if (serData != null)
                {
                    cadPanel.LoadPanelInstanceData(serData);
                }
            }
        }

        private string OpenFileBrowser()
        {
            SaveFileDialog folderBrowser = new SaveFileDialog();

            string openPath = null;

            folderBrowser.Title = "Open File";

            folderBrowser.OverwritePrompt = false;

            folderBrowser.Filter = "CPAN Files (.cpan)|*.cpan";

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                openPath = folderBrowser.FileName;
            }

            return openPath;
        }
    }
}
