﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CustomCAD.Common;
using System.Drawing.Imaging;
using static CustomCAD.Common.GlobalStorageContainer;
using CustomCAD.DrawingElements;
using CustomCAD.Extensions;

namespace CustomCAD.Forms
{
    public partial class CADForm : Form
    {
        Timer CheckTimer = new Timer() { Interval = 25, Enabled = false };

        private Bitmap DraftingBitmap = new Bitmap(CanvasWidth, CanvasHeight, PixelFormat.Format32bppRgb);
        private PictureBox DraftingPictureBox;
        private TextBox InputTextBox;
        private Label CoordLabel;

        #region Private Form Properties
        private int _minFormWidth = 400;
        private int _minFormHeight = 400;
        #endregion

        #region Private Mouse Event Variables
        private Point PreviousMousePoint = new Point(0, 0);
        private Point MousePoint = new Point(0, 0);
        #endregion

        #region Input Text Box
        private int _readOnlyChars = 0;
        #endregion

        public CADForm()
        {
            InitializeComponent();
        }

        private void CADForm_Load(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            this.MinimumSize = new Size(_minFormWidth, _minFormHeight);
            this.KeyPreview = true;

            this.Resize += new EventHandler(ResizeAllControls);
            this.KeyPress += new KeyPressEventHandler(DraftingPictureBoxHasFocus_KeyPress);

            RunInitializationRoutines();

            this.Width = 900;
        }

        #region Form Initialization
        private void RunInitializationRoutines()
        {
            InitializeLayers();
            InitializeDraftingPictureBox();
            InitializeInputTextBox();
            InitializeCoordinateLabel();

            CreateDraftingGraphics();
        }
        private void InitializeLayers()
        {
            GlobalStorageContainer.Layers = new List<Layer>();

            Layer defaultLayer = new Layer()
            {
                Name = "Default",
                Color = Color.Blue,
                Number = 0,
                Width = 1
            };

            Layer rebarLayer = new Layer()
            {
                Name = "Rebar",
                Color = Color.Black,
                Number = 1,
                Width = 2
            };

            GlobalStorageContainer.Layers.Add(defaultLayer);
            GlobalStorageContainer.Layers.Add(rebarLayer);
        }

        private void InitializeDraftingPictureBox()
        {
            Point location = new Point();
            location.X = 10;
            location.Y = 8;

            DraftingPictureBox = new PictureBox()
            {
                Location = location,
                Width = CanvasWidth,
                Height = CanvasHeight,
                Name = "DraftingPictureBox",
                BorderStyle = BorderStyle.FixedSingle
            };

            DraftingPictureBox.Paint += new PaintEventHandler(DraftingPictureBox_Paint);
            DraftingPictureBox.MouseMove += new MouseEventHandler(DraftingPictureBox_MouseMove);
            DraftingPictureBox.MouseEnter += new EventHandler(DraftingPictureBox_MouseEnter);
            DraftingPictureBox.MouseLeave += new EventHandler(DraftingPictureBox_MouseLeave);

            this.Controls.Add(DraftingPictureBox);
        }
       
        private void InitializeInputTextBox()
        {
            int boxWidth = DraftingPictureBox.Width - 4;
            int boxHeight = 15;

            InputTextBox = new TextBox()
            {
                Name = "InputTextBox",
                Multiline = true,
                Size = new Size(boxWidth, boxHeight),
                Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - boxHeight - 2),
                BackColor = Color.LightGray,
                BorderStyle = BorderStyle.None,
                Text = "",
                Enabled = true,
                ReadOnly = false
            };

            this.Controls.Add(InputTextBox);
            InputTextBox.BringToFront();
        }
        
        private void InitializeCoordinateLabel()
        {
            CoordLabel = new Label()
            {
                Name = "CoordinateLabel",
                Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - 50),
                Text = "test",
                Height = 27
            };

            this.Controls.Add(CoordLabel);
            CoordLabel.BringToFront();
        }
        #endregion

        #region Graphics and Paitning
        private void CreateDraftingGraphics()
        {
            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Graphics g = Graphics.FromImage(DraftingBitmap);

                g.Clear(Color.White);

                //Draw the objects in the DrawingElements list
                if (DrawingElementList != null)
                {
                    if (DrawingElementList.Count > 0)
                    {
                        foreach (IDrawingElement elem in DrawingElementList)
                        {
                            elem.Draw(ref g, ZeroPoint, DrawingScale);
                        }
                    }
                }

                g.Dispose();
                DraftingPictureBox.Invalidate();
            }
        }

        private void DraftingPictureBox_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bicubic;
            e.Graphics.DrawImage(DraftingBitmap, BitmapInsertionPoint.X, BitmapInsertionPoint.Y);
        }
        #endregion

        #region Form Resizing
        private void ResizeAllControls(object sender, EventArgs e)
        {
            ResizeDraftingPictureBox();
            ResizeInputTextBox();
            ResizeCoordinateLabel();
        }

        private void ResizeDraftingPictureBox()
        {
            if (this.Width >= _minFormWidth)
            {
                if (this.Height >= _minFormHeight)
                {
                    DraftingPictureBox.Width = this.Width - 40;
                    DraftingPictureBox.Height = this.Height - 60;

                    DraftingBitmap.Dispose();
                    DraftingBitmap = new Bitmap(DraftingPictureBox.Width, DraftingPictureBox.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

                    DrawingScale = BaseDrawingScale * MagnificationLevel;
                    SelectionDistance = BaseSelectionDistance / MagnificationLevel;
                    BaseSnapDistance = BaseSnapDistance / MagnificationLevel;

                    CreateDraftingGraphics();
                    DraftingPictureBox.Invalidate();
                }
            }
        }
       
        private void ResizeInputTextBox()
        {
            InputTextBox.Size = new Size(DraftingPictureBox.Width - 4, InputTextBox.Height); // = DraftingPictureBox.Width - 4;
            InputTextBox.Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - InputTextBox.Height - 2);
        }
        
        private void ResizeCoordinateLabel()
        {
            CoordLabel.Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - 50);
        }
        #endregion

        #region Mouse Events

        private void DraftingPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            PreviousMousePoint = MousePoint;
            MousePoint = new Point(e.X, e.Y);
        }

        private void DraftingPictureBox_MouseEnter(object sender, EventArgs e)
        {
            DraftingPictureBox.Focus();
            CheckTimer.Enabled = true;
        }

        private void DraftingPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CoordLabel.Focus();
            CheckTimer.Enabled = false;
        }
        #endregion

        #region Keyboard Events
        private void DraftingPictureBoxHasFocus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (DraftingPictureBox.ContainsFocus == true)
            {
                if (e.KeyChar == ChrW((int)Keys.Return))
                {

                }
                else if (e.KeyChar == ChrW((int)Keys.Escape))
                {
                    InputTextBox.Clear();
                    _readOnlyChars = 0;
                    CreateDraftingGraphics();
                }
                else if (e.KeyChar == ChrW((int)Keys.Space))
                {

                }
                else if (e.KeyChar == ChrW((int)Keys.ControlKey))
                {

                }
                else if (e.KeyChar == ChrW((int)Keys.Back))
                {
                    string myStr = InputTextBox.Text;

                    if (myStr.Length > 1)
                    {
                        if (myStr.Length > _readOnlyChars)
                        {
                            InputTextBox.Text = myStr.Left(myStr.Length - 1);
                        }
                    }
                    else
                    {
                        if (myStr.Length > _readOnlyChars)
                        {
                            InputTextBox.Text = "";
                        }
                    }
                }
                else
                {
                    InputTextBox.AppendText(e.KeyChar.ToString());
                }
            }
            
        }

        public static char ChrW(int code)
        {
            return (char)code;
        }
        #endregion

    }
}
