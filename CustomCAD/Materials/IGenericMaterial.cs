﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomCAD.Materials
{
    public interface IGenericMaterial
    {
        string Name { get; set; }

        double MonotonicStressStrain(double strain);
        double MonotonicStrainStress(double stress);
    }
}
