﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomCAD.Materials
{
    public abstract class ConcreteMaterial : IGenericMaterial
    {
        #region Public Implemented Properties
        public string Name { get; set; }
        #endregion

        #region Public Properties
        public bool Confined { get; set; } = false;
        #endregion

        #region Public Implemented Methods
        public abstract double MonotonicStressStrain(double strain);

        public abstract double MonotonicStrainStress(double stress);
        #endregion
    }
}
