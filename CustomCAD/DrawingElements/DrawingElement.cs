﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using System.Drawing;
using static System.Math;
using CustomCAD.ConcreteElements;

namespace CustomCAD.DrawingElements
{
    public abstract class DrawingElement : IDrawingElement
    {
        #region Implemented Public Properties
        public List<DrawingNode> Nodes { get; set; } = new List<DrawingNode>();
        public Layer Layer { get; set; }
        public bool Selected { get; set; } = false;
        public string Name { get; set; }
        #endregion

        #region Public Properties
        public bool IsRebar { get; set; } = false;
        public RebarDefinition Rebar { get; set; } = null;
        #endregion

        #region Private Variables
        private const int ROUND = 4;
        #endregion

        #region Abstract Methods
        public abstract void Draw(ref Graphics g, PointD zeroPoint, double drawingScale);
        public abstract void DrawTemp(ref Graphics g, PointD zeroPoint, PointD currentPoint, double drawingScale);
        public abstract IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint);
        #endregion

        #region Implemented Public Interface Methods
        /// <summary>
        /// Moves the drawing element
        /// </summary>
        /// <param name="basePoint">Base point of move command</param>
        /// <param name="currentPoint">Final point of move command</param>
        public void Move(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            foreach (DrawingNode node in Nodes)
            {
                node.Point.X += delta_X;
                node.Point.Y += delta_Y;
            }
        }

        /// <summary>
        /// Rotates the drawing element about a base point
        /// </summary>
        /// <param name="basePoint">Rotate base point</param>
        /// <param name="currentPoint">Final rotate point</param>
        public void Rotate(PointD basePoint, PointD currentPoint)
        {
            double rotationAngle = currentPoint.GetAngleFromPoint(basePoint);

            RotateThroughAngle(basePoint, rotationAngle);
        }

        /// <summary>
        /// Rotates the object through the specified angle
        /// </summary>
        /// <param name="basePoint">Rotation base point</param>
        /// <param name="angle">Angle, in radians, to rotat through</param>
        public void RotateThroughAngle(PointD basePoint, double angle)
        {
            foreach (DrawingNode node in Nodes)
            {
                double distToBasePoint = node.Point.DistanceTo(basePoint);

                if (distToBasePoint > 0)
                {
                    double oldAngle = node.Point.GetAngleFromPoint(basePoint);

                    double newAngle = oldAngle + angle;

                    if (newAngle > 2.0 * PI)
                    {
                        newAngle -= 2.0 * PI;
                    }

                    double newX = distToBasePoint * Cos(newAngle);
                    double newY = distToBasePoint * Sin(newAngle);

                    node.Point.X = newX;
                    node.Point.Y = newY;
                }
            }
        }
        #endregion

        #region Protected Methods

        #endregion
    }
}
