﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Materials;
using CustomCAD.ConcreteElements;
using System.Drawing;

namespace CustomCAD.DrawingElements
{
    public abstract class ClosedDrawingElement : DrawingElement
    {
        #region Public Properties
        public bool Hole { get; set; } = false;
        public IGenericMaterial Material { get; set; }
        public bool FillShape { get; set; }
        public Color? FillColor { get; set; } = null;
        #endregion

        #region Public Abstract Methods
        public abstract double Area();
        #endregion
    }
}
