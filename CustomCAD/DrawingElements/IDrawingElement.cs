﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using System.Drawing;

namespace CustomCAD.DrawingElements
{
    public interface IDrawingElement
    {
        List<DrawingNode> Nodes { get; set; }
        Layer Layer { get; set; }
        bool Selected { get; set; }
        string Name { get; set; }

        void Draw(ref Graphics g, PointD zeroPoint, double drawingScale);
        void DrawTemp(ref Graphics g, PointD zeroPoint, PointD currentPoint, double drawingScale);
        void Move(PointD basePoint, PointD currentPoint);
        void Rotate(PointD basePoint, PointD currentPoint);
        void RotateThroughAngle(PointD basePoint, double angle);
        IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint);


    }
}
