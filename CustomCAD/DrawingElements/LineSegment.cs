﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;

namespace CustomCAD.DrawingElements
{
    public class LineSegment
    {
        public DrawingNode StartNode { get; set; }
        public DrawingNode EndNode { get; set; }

        #region Constructors
        public LineSegment(DrawingNode startNode, DrawingNode endNode)
        {
            StartNode = startNode;
            EndNode = endNode;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the length of the line segment
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            return Sqrt(Pow(EndNode.Point.X - StartNode.Point.X, 2) + Pow(EndNode.Point.Y - StartNode.Point.Y, 2));
        }

        /// <summary>
        /// Reverses the direction of the line segment
        /// </summary>
        public void Reverse()
        {
            DrawingNode temp = EndNode;
            EndNode = StartNode;
            StartNode = temp;
        }
        #endregion
    }
}
