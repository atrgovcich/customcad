﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CustomCAD.Common;
using CustomCAD.ConcreteElements;
using static System.Math;

namespace CustomCAD.DrawingElements
{
    public class Polyline : DrawingElement
    {
        #region Public Properties
        
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns a copy of the polyline element
        /// </summary>
        /// <param name="basePoint">Copy base point</param>
        /// <param name="currentPoint">Current point</param>
        /// <returns></returns>
        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>();

            foreach (DrawingNode node in Nodes)
            {
                DrawingNode copiedNode = new DrawingNode()
                {
                    TypeOfNode = node.TypeOfNode,
                    Point = new PointD(node.Point.X + delta_X, node.Point.Y + delta_Y)
                };

                copiedNodes.Add(copiedNode);
            }

            Polyline copiedPolyline = new Polyline()
            {
                Nodes = copiedNodes,
                Layer = Layer,
                Name = Name,
                Selected = false
            };

            return copiedPolyline;
        }

        public override void Draw(ref Graphics g, PointD zeroPoint, double drawingScale)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = GlobalStorageContainer.SelectedObjectLineColor;
                myPen.Width = GlobalStorageContainer.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Point[] pixelPoints = new Point[Nodes.Count];

            for (int i = 0; i < Nodes.Count; i++)
            {
                pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
            }

            if (pixelPoints.GetUpperBound(0) > 0)
            {
                g.DrawLines(myPen, pixelPoints);
            }

            if (Selected == true)
            {
                SolidBrush myBrush = new SolidBrush(GlobalStorageContainer.SelectedObjectPointColor);

                for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                {
                    Rectangle boundingRectangle = new Rectangle
                        (
                            Convert.ToInt32(pixelPoints[i].X - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(pixelPoints[i].Y - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                            GlobalStorageContainer.SelectedObjectPointWidth,
                            GlobalStorageContainer.SelectedObjectPointWidth
                        );

                    g.FillEllipse(myBrush, boundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, PointD zeroPoint, PointD currentPoint, double drawingScale)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = GlobalStorageContainer.SelectedObjectLineColor;
                myPen.Width = GlobalStorageContainer.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            List<Point> pixelPointList = new List<Point>();

            foreach (DrawingNode node in Nodes)
            {
                pixelPointList.Add(node.Point.ConvertToPixelPoint(zeroPoint, drawingScale));
            }

            if (currentPoint != null)
            {
                pixelPointList.Add(currentPoint.ConvertToPixelPoint(zeroPoint, drawingScale));
            }

            Point[] pixelPoints = pixelPointList.ToArray();

            if (pixelPoints.GetUpperBound(0) > 0)
            {
                g.DrawLines(myPen, pixelPoints);
            }

            if (Selected == true)
            {
                SolidBrush myBrush = new SolidBrush(GlobalStorageContainer.SelectedObjectPointColor);

                for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                {
                    Rectangle boundingRectangle = new Rectangle
                        (
                            Convert.ToInt32(pixelPoints[i].X - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(pixelPoints[i].Y - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                            GlobalStorageContainer.SelectedObjectPointWidth,
                            GlobalStorageContainer.SelectedObjectPointWidth
                        );

                    g.FillEllipse(myBrush, boundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }
        #endregion

        #region Public Methods
        public List<LineSegment> Edges()
        {
            List<LineSegment> segments = new List<LineSegment>();

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                LineSegment seg = new LineSegment(Nodes[i], Nodes[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        public List<PointD> GetIntersectionWithInfiniteVector(PointD pA, Vector v)
        {
            PointD pB = new PointD(pA.X + v.X, pA.Y + v.Y);

            LineSegment line = new LineSegment(new DrawingNode(pA, NodeType.EndPoint), new DrawingNode(pB, NodeType.EndPoint));

            return GetIntersectionWithInfiniteLine(line);
        }

        public List<PointD> GetIntersectionWithInfiniteLine(LineSegment line)
        {
            PointD pA = line.StartNode.Point;
            PointD pB = line.EndNode.Point;

            List<PointD> intersections = new List<PointD>();

            foreach (LineSegment edge in this.Edges())
            {
                PointD p0 = edge.StartNode.Point;
                PointD p1 = edge.EndNode.Point;

                if (p0.X == p1.X)
                {
                    //the edge is a vertical line
                    if (pA.X != pB.X)
                    {
                        //the input line is NOT a vertical line
                        double m2 = (pB.Y - pA.Y) / (pB.X - pA.X);
                        double b2 = pA.Y - m2 * pA.X;

                        double intX = p0.X;
                        double intY = m2 * intX + b2;

                        PointD intersection = new PointD(intX, intY);

                        if (intersection.Y >= Min(p0.Y, p1.Y) && intersection.Y <= Max(p0.Y, p1.Y))
                        {
                            bool unique = true;

                            foreach (PointD p in intersections)
                            {
                                if (p.X == intersection.X && p.Y == intersection.Y)
                                {
                                    unique = false;
                                    break;
                                }
                            }

                            if (unique == true)
                            {
                                intersections.Add(intersection);
                            }
                        }
                    }
                }
                else
                {
                    //the edge is not a vertical line
                    double m1 = (p1.Y - p0.Y) / (p1.X - p0.X);
                    double b1 = p0.Y - m1 * p0.X;

                    if (pA.X == pB.X)
                    {
                        //the input line is a vertical line
                        double intX = pA.X;
                        double intY = m1 * intX + b1;

                        PointD intersection = new PointD(intX, intY);

                        if (intersection.X >= Min(p0.X, p1.X) && intersection.X <= Max(p0.X, p1.X))
                        {
                            bool unique = true;

                            foreach (PointD p in intersections)
                            {
                                if (p.X == intersection.X && p.Y == intersection.Y)
                                {
                                    unique = false;
                                    break;
                                }
                            }

                            if (unique == true)
                            {
                                intersections.Add(intersection);
                            }
                        }
                    }
                    else
                    {
                        double m2 = (pB.Y - pA.Y) / (pB.X - pA.X);
                        double b2 = pA.Y - m2 * pA.X;

                        if (m1 != m2)
                        {
                            double intX = (b2 - b1) / (m1 - m2);
                            double intY = m1 * intX + b1;

                            PointD intersection = new PointD(intX, intY);

                            if (intersection.X >= Min(p0.X, p1.X) && intersection.X <= Max(p0.X, p1.X))
                            {
                                //intersection piont lies within the edge bounds
                                bool unique = true;

                                foreach (PointD p in intersections)
                                {
                                    if (p.X == intersection.X && p.Y == intersection.Y)
                                    {
                                        unique = false;
                                        break;
                                    }
                                }

                                if (unique == true)
                                {
                                    intersections.Add(intersection);
                                }
                            }
                        }
                    }
                }
            }

            return intersections;
        }
        #endregion  
    }
}
