﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using static System.Math;

namespace CustomCAD.DrawingElements
{
    public class Circle : ClosedDrawingElement
    {
        #region Public Properties
        public double Radius { get; set; }
        #endregion

        #region Public Implemented Methods
        public override double Area()
        {
            return PI * Pow(Radius, 2);
        }

        public override void Draw(ref Graphics g, PointD zeroPoint, double drawingScale)
        {
            Point pixelCenterPoint = CenterPoint().Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            int pixelRadius = Convert.ToInt32(Radius * drawingScale);

            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = GlobalStorageContainer.SelectedObjectLineColor;
                myPen.Width = GlobalStorageContainer.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            g.DrawEllipse(myPen, boundingRectangle);

            if (Selected == true)
            {
                List<PointD> quadPoints = QuadrantPoints();

                List<Point> pixelQuadPoints = new List<Point>(5);

                pixelQuadPoints.Add(pixelCenterPoint);

                foreach (PointD p in quadPoints)
                {
                    pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                }

                SolidBrush myBrush = new SolidBrush(GlobalStorageContainer.SelectedObjectPointColor);

                foreach (Point p in pixelQuadPoints)
                {
                    Rectangle quadBoundingRectangle = new Rectangle(
                        Convert.ToInt32(p.X - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                        Convert.ToInt32(p.Y - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                        GlobalStorageContainer.SelectedObjectPointWidth,
                        GlobalStorageContainer.SelectedObjectPointWidth);

                    g.FillEllipse(myBrush, quadBoundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, PointD zeroPoint, PointD currentPoint, double drawingScale)
        {
            DrawingNode cp = CenterPoint();

            Point pixelCenterPoint = cp.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            Radius = Sqrt(Pow(cp.Point.X - currentPoint.X, 2) + Pow(cp.Point.Y - currentPoint.Y, 2));

            int pixelRadius = Convert.ToInt32(Radius * drawingScale);

            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = GlobalStorageContainer.SelectedObjectLineColor;
                myPen.Width = GlobalStorageContainer.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            g.DrawEllipse(myPen, boundingRectangle);

            if (Selected == true)
            {
                List<PointD> quadPoints = QuadrantPoints();

                List<Point> pixelQuadPoints = new List<Point>(5);

                pixelQuadPoints.Add(pixelCenterPoint);

                foreach (PointD p in quadPoints)
                {
                    pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                }

                SolidBrush myBrush = new SolidBrush(GlobalStorageContainer.SelectedObjectPointColor);

                foreach (Point p in pixelQuadPoints)
                {
                    Rectangle quadBoundingRectangle = new Rectangle(
                        Convert.ToInt32(p.X - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                        Convert.ToInt32(p.Y - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                        GlobalStorageContainer.SelectedObjectPointWidth,
                        GlobalStorageContainer.SelectedObjectPointWidth);

                    g.FillEllipse(myBrush, quadBoundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            DrawingNode currentCenterNode = CenterPoint();

            PointD copiedCenterPoint = new PointD(currentCenterNode.Point.X + delta_X, currentCenterNode.Point.Y + delta_Y);

            DrawingNode centerNode = new DrawingNode(copiedCenterPoint, NodeType.CenterPoint);

            Circle copiedCircle = new Circle()
            {
                Nodes = new List<DrawingNode>(1) { centerNode },
                Hole = Hole,
                IsRebar = IsRebar,
                Layer = Layer,
                Material = Material,
                Name = Name,
                Radius = Radius,
                Rebar = Rebar,
                Selected = false
            };

            return copiedCircle;
        }

        public bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //Circles can't be joine with other elements
            newElement = null;
            return false;
        }
        #endregion

        #region Public Methods
        public DrawingNode CenterPoint()
        {
            foreach (DrawingNode node in Nodes)
            {
                if (node.TypeOfNode.Equals(NodeType.CenterPoint) == true)
                {
                    return node;
                }
            }

            throw new Exception("No center point has been ddefined.");
        }

        public List<PointD> QuadrantPoints()
        {
            List<PointD> pointList = new List<PointD>(4);

            DrawingNode centerPoint = CenterPoint();

            pointList.Add(new PointD(centerPoint.Point.X - Radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y + Radius));
            pointList.Add(new PointD(centerPoint.Point.X + Radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y - Radius));

            return pointList;
        }
        #endregion
    }
}
