﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
using static System.Math;
using System.Drawing;

namespace CustomCAD.DrawingElements
{
    public class Polygon : ClosedDrawingElement
    {

        #region Public Implemented Methods
        /// <summary>
        /// Retunrs the area of the polygon
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return Abs(SignedArea());
        }
        #endregion

        #region Public Implemented Methods
        public override void Draw(ref Graphics g, PointD zeroPoint, double drawingScale)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = GlobalStorageContainer.SelectedObjectLineColor;
                myPen.Width = GlobalStorageContainer.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Point[] pixelPoints = new Point[Nodes.Count];

            for (int i = 0; i < Nodes.Count; i++)
            {
                pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
            }

            if (pixelPoints.GetUpperBound(0) >= 1)
            {
                if (FillShape == false)
                {
                    g.DrawPolygon(myPen, pixelPoints);
                }
                else
                {
                    Color? fillBrushColor = FillColor;

                    if (fillBrushColor == null)
                    {
                        if (Selected == false)
                        {
                            fillBrushColor = Layer.Color;
                        }
                        else
                        {
                            fillBrushColor = GlobalStorageContainer.SelectedObjectLineColor;
                        }
                    }

                    SolidBrush fillBrush = new SolidBrush(fillBrushColor.Value);

                    g.FillPolygon(fillBrush, pixelPoints);
                    fillBrush.Dispose();
                }

                if (Selected == true)
                {
                    SolidBrush myBrush = new SolidBrush(GlobalStorageContainer.SelectedObjectPointColor);

                    for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                    {
                        Rectangle boundingRectangle = new Rectangle
                            (
                                Convert.ToInt32(pixelPoints[i].X - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                                Convert.ToInt32(pixelPoints[i].Y - GlobalStorageContainer.SelectedObjectPointWidth / 2.0),
                                GlobalStorageContainer.SelectedObjectPointWidth,
                                GlobalStorageContainer.SelectedObjectPointWidth
                            );

                        g.FillEllipse(myBrush, boundingRectangle);
                    }

                    myBrush.Dispose();
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, PointD zeroPoint, PointD currentPoint, double drawingScale)
        {
            //Do nothing
        }

        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>();

            foreach (DrawingNode node in Nodes)
            {
                PointD copiedPoint = new PointD(node.Point.X + delta_X, node.Point.Y + delta_Y);

                DrawingNode copiedNode = new DrawingNode(copiedPoint, node.TypeOfNode);

                copiedNodes.Add(copiedNode);
            }

            Polygon copiedPolygon = new Polygon()
            {
                Nodes = copiedNodes,
                FillColor = FillColor,
                FillShape = FillShape,
                Hole = Hole,
                IsRebar = IsRebar,
                Layer = Layer,
                Material = Material,
                Name = Name,
                Rebar = Rebar,
                Selected = false
            };

            return copiedPolygon;
        }
        #endregion

        #region Public Methodds
        public double SignedArea()
        {
            double sum = 0;

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                PointD p0 = Nodes[i].Point;
                PointD p1 = Nodes[i + 1].Point;
                sum += p0.X * p1.Y - p1.X * p0.Y;
            }

            return sum / 2.0;
        }

        public PointD Centroid()
        {
            double sumX = 0;
            double sumY = 0;

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                PointD p0 = Nodes[i].Point;
                PointD p1 = Nodes[i + 1].Point;

                sumX += (p0.X + p1.X) * (p0.X * p1.Y - p1.X * p0.Y);
                sumY += (p0.Y + p1.Y) * (p0.X * p1.Y - p1.X * p0.Y);
            }

            double area = SignedArea();

            double cx = 1.0 / (6.0 * area) * sumX;
            double cy = 1.0 / (6.0 * area) * sumY;

            return new PointD(cx, cy);
        }

        public List<LineSegment> Edges()
        {
            List<LineSegment> segments = new List<LineSegment>();

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                LineSegment seg = new LineSegment(Nodes[i], Nodes[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        public static bool operator ==(Polygon pg1, Polygon pg2)
        {
            if (pg1.Equals(pg2) == true)
            {
                return true;
            }

            bool same = true;

            if (pg1.Nodes.Count == pg2.Nodes.Count)
            {
                for (int i = 0; i < pg1.Nodes.Count; i++)
                {
                    if (pg1.Nodes[i].Point != pg2.Nodes[i].Point)
                    {
                        same = false;
                        break;
                    }
                }
            }
            else
            {
                same = false;
            }

            if (same == false)
            {
                List<PointD> pg1_R = new List<PointD>(); //resampled
                List<PointD> pg2_R = new List<PointD>(); //resampled

                for (int i = 0; i < pg1.Nodes.Count; i++)
                {
                    pg1_R.Add(pg1.Nodes[i].Point);
                }

                for (int i = 0; i < pg2.Nodes.Count; i++)
                {
                    pg2_R.Add(pg2.Nodes[i].Point);
                }

                int count = 0;

                while (count <= pg1_R.Count - 3)
                {
                    PointD p1 = pg1_R[count];
                    PointD p2 = pg1_R[count + 1];
                    PointD p3 = pg1_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg1_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg1_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                count = 0;

                while (count <= pg2_R.Count - 3)
                {
                    PointD p1 = pg2_R[count];
                    PointD p2 = pg2_R[count + 1];
                    PointD p3 = pg2_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg2_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg2_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                //Now both polygons are resampled

                if (pg1_R.Count != pg2_R.Count)
                {
                    return false;
                }
                else
                {
                    int numSame = 0;

                    foreach (PointD p1 in pg1_R)
                    {
                        foreach (PointD p2 in pg2_R)
                        {
                            if (p1 == p2)
                            {
                                numSame++;
                                break;
                            }
                        }
                    }

                    if (numSame == pg1_R.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                return true;
            }
        }

        public static bool operator !=(Polygon pg1, Polygon pg2)
        {
            if (pg1 == pg2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
