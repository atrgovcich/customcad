﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomCAD.Common;
namespace CustomCAD.DrawingElements
{
    public class DrawingNode
    {
        public PointD Point { get; set; }
        public NodeType TypeOfNode { get; set; }

        #region Constructors
        public DrawingNode()
        {

        }

        public DrawingNode(PointD p, NodeType type)
        {
            Point = p;
            TypeOfNode = type;
        }
        #endregion
    }
}
