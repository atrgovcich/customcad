﻿using System;
using Utilities.Generic;
using Newtonsoft.Json;

namespace StructuralLoads.General
{
    public class DesignMethodType : Enumeration
    {
        public static readonly DesignMethodType ASD = new DesignMethodType(0, "ASD");
        public static readonly DesignMethodType LRFD = new DesignMethodType(1, "LRFD");
        public static readonly DesignMethodType Nominal = new DesignMethodType(2, "Nominal");

        [JsonConstructor]
        private DesignMethodType(int value, string description)
            : base(value, description)
        {
        }

        public static DesignMethodType FromDescription(string description)
        {
            try
            {
                return FromDescriptionCaseInsensitive<DesignMethodType>(description);
            }
            catch (ArgumentException)
            {
                return Nominal;
            }
        }
    }
}
