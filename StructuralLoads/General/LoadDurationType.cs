﻿using System;
using Utilities.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StructuralLoads.General
{
    public class LoadDurationType : Enumeration
    {
        public double Cd { get; }

        public static readonly LoadDurationType Permanent = new LoadDurationType(0, "Permanent", 0.9);
        public static readonly LoadDurationType TenYears = new LoadDurationType(1, "Ten Years", 1.0);
        public static readonly LoadDurationType TwoMonths = new LoadDurationType(2, "Two Months", 1.15);
        public static readonly LoadDurationType SevenDays = new LoadDurationType(3, "Seven Days", 1.25);
        public static readonly LoadDurationType TenMinutes = new LoadDurationType(4, "Ten Minutes", 1.6);
        public static readonly LoadDurationType Impact = new LoadDurationType(5, "Impact", 2.0);

        [JsonConstructor]
        private LoadDurationType(int value, string description, double Cd)
            : base(value, description)
        {
            this.Cd = Cd;
        }

        public static LoadDurationType FromDescription(string description)
        {
            try
            {
                return FromDescriptionCaseInsensitive<LoadDurationType>(description);
            }
            catch (ArgumentException)
            {
                return TenYears;
            }
        }
    }
}
