﻿using System.ComponentModel;

namespace StructuralLoads.General
{
    public enum LoadPatternType
    {
        [Description("Self Dead")]
        SelfDead,
        [Description("Superimposed Dead")]
        SuperimposedDead,
        [Description("Live")]
        Live,
        [Description("Live Roof")]
        LiveRoof,
        [Description("Live Storage")]
        LiveStorage,
        [Description("Snow")]
        Snow,
        [Description("Wind")]
        Wind,
        [Description("Components and Cladding Pressure")]
        ComponentsAndCladdingPressure,
        [Description("Components and Cladding Suction")]
        ComponentsAndCladdingSuction,
        [Description("Seismic")]
        Earthquake,
        [Description("Seismic Diaphragm")]
        Earthquake_Diaphragm,
        [Description("Unit Live")]
        UnitLive,
        [Description("Mixed")]
        Mixed,
        [Description("Live Out Of Plane Positive")]
        LiveOutOfPlanePositive,
        [Description("Live Out Of Plane Negative")]
        LiveOutOfPlaneNegative
    }
}
