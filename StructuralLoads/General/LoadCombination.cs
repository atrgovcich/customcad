﻿using System.Collections.Generic;
using System.IO;

namespace StructuralLoads.General
{
    public class LoadCombination
    {
        public string CombinationName { get; set; }
        public double C_d { get; set; } = 1;
        public double Lambda { get; set; } = 1;
        public DesignMethodType DesignMethod { get; set; } = DesignMethodType.ASD;
        public Dictionary<string, double> LoadFactors { get; set; }
        public LoadDurationType LoadDuration { get; set; } = LoadDurationType.TenYears;
        public double DL { get; set; }
        public double LL { get; set; }
        public double LR { get; set; }
        public double S { get; set; }
        public double W { get; set; }
        public double EQ { get; set; }
        public double H { get; set; }
        public double F { get; set; }
        public double ULL { get; set; }
        public double SDS { get; set; }

        //public static readonly LoadCombination ASDDefault = ProjectSettings.ASDLoadCombos[0];
        //public static readonly LoadCombination LRFDDefault = ProjectSettings.LRFDLoadCombos[0];

        public LoadCombination()
        {
        }

        public LoadCombination(string combinationName, Dictionary<string, double> loadFactors, DesignMethodType designMethod, LoadDurationType loadDuration, double c_d = 1, double lambda = 1)
        {
            CombinationName = combinationName;
            DesignMethod = designMethod;
            LoadFactors = loadFactors;
            C_d = c_d;
            Lambda = lambda;
            DL = loadFactors["DL"];
            LL = loadFactors["LL"];
            LR = loadFactors["LR"];
            S = loadFactors["S"];
            W = loadFactors["W"];
            EQ = loadFactors["EQ"];
            SDS = loadFactors["Sds"];
            LoadFactors = loadFactors;
            LoadDuration = loadDuration;
        }

        public LoadCombination(DesignMethodType designMethod, string combinationName, double deadLoad, double sds, double liveLoad, double liveRoof, double snow, double wind, double earthquake, LoadDurationType loadDuration, double c_d = 1, double lambda = 1, double unitLive = 0d)
        {
            CombinationName = combinationName;
            DesignMethod = designMethod;
            LoadDuration = loadDuration;
            C_d = c_d;
            Lambda = lambda;
            DL = deadLoad;
            LL = liveLoad;
            LR = liveRoof;
            S = snow;
            W = wind;
            EQ = earthquake;
            SDS = sds;
            ULL = unitLive;
            LoadFactors = new Dictionary<string, double> { };
            LoadFactors.Add("DL", DL);
            LoadFactors.Add("LL", LL);
            LoadFactors.Add("LR", LR);
            LoadFactors.Add("S", S);
            LoadFactors.Add("W", W);
            LoadFactors.Add("EQ", EQ);
            LoadFactors.Add("UnitLive", ULL);
        }

        //public static LoadCombination GetDefault(DesignMethodType designMethodType)
        //{
        //    if (designMethodType == DesignMethodType.ASD) return ASDDefault;
        //    if (designMethodType == DesignMethodType.LRFD) return LRFDDefault;

        //    throw new InvalidDataException("expected ASD or LRFD");
        //}

        public double GetLoadFactor(LoadPatternType LoadPattern)
        {
            var LoadFactor = 0d;

            if (LoadPattern.Equals(LoadPatternType.SelfDead) == true || LoadPattern.Equals(LoadPatternType.SuperimposedDead) == true)
            {
                LoadFactor = DL;
            }
            else if (LoadPattern.Equals(LoadPatternType.Live) == true)
            {
                LoadFactor = LL;
            }
            else if (LoadPattern.Equals(LoadPatternType.LiveStorage) == true)
            {
                LoadFactor = System.Math.Max(LL, 1.0);
            }
            else if (LoadPattern.Equals(LoadPatternType.LiveRoof) == true)
            {
                LoadFactor = LR;
            }
            else if (LoadPattern.Equals(LoadPatternType.Snow) == true)
            {
                LoadFactor = S;
            }
            else if (LoadPattern.Equals(LoadPatternType.SuperimposedDead) == true)
            {
                LoadFactor = DL;
            }
            else if (LoadPattern.Equals(LoadPatternType.Wind) == true)
            {
                LoadFactor = W;
            }
            else if (LoadPattern.Equals(LoadPatternType.ComponentsAndCladdingPressure) == true || LoadPattern.Equals(LoadPatternType.ComponentsAndCladdingSuction) == true)
            {
                LoadFactor = W;
            }
            else if (LoadPattern.Equals(LoadPatternType.LiveOutOfPlanePositive) == true || LoadPattern.Equals(LoadPatternType.LiveOutOfPlaneNegative) == true)
            {
                LoadFactor = LL;
            }
            else if (LoadPattern.Equals(LoadPatternType.Earthquake) == true)
            {
                LoadFactor = EQ;
            }
            else if (LoadPattern == LoadPatternType.UnitLive)
            {
                LoadFactor = ULL;
            }

            return LoadFactor;
        }

    }
}
