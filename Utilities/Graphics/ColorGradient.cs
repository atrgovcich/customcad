﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Drawing;
using static System.Math;

namespace Utilities.Graphics
{
    public class ColorGradient
    {
        public List<Tuple<double, Color>> ColorStops { get; set; }

        public ColorGradient()
        {

        }

        public ColorGradient(List<Tuple<double, Color>> colorStops)
        {
            ColorStops = colorStops.OrderBy(x => x.Item1).ToList();
        }

        public virtual Color GetColorAtValue(double value)
        {
            for (int i = 0; i < ColorStops.Count; i++)
            {
                if (ColorStops[i].Item1 == value)
                {
                    return ColorStops[i].Item2;
                }

                else if (ColorStops[i].Item1 > value)
                {
                    if (i > 0)
                    {
                        Color colorAbove = ColorStops[i].Item2;
                        Color colorBelow = ColorStops[i - 1].Item2;

                        double valAbove = ColorStops[i].Item1;
                        double valBelow = ColorStops[i - 1].Item1;

                        double red = 0;
                        double green = 0;
                        double blue = 0;
                        double alpha = 1;

                        double m_b = (colorAbove.B - colorBelow.B) / (0.5 * (valAbove - valBelow));
                        if (m_b > 0)
                        {
                            blue = Min(colorBelow.B + m_b * (value - valBelow), colorAbove.B);
                        }
                        else if (m_b < 0)
                        {
                            blue = Min(colorBelow.B, colorAbove.B - m_b * (valAbove - value));
                        }
                        else
                        {
                            blue = colorBelow.B;
                        }

                        double m_g = (colorAbove.G - colorBelow.G) / (0.5 * (valAbove - valBelow));
                        if (m_g > 0)
                        {
                            green = Min(colorBelow.G + m_g * (value - valBelow), colorAbove.G);
                        }
                        else if (m_g < 0)
                        {
                            green = Min(colorBelow.G, colorAbove.G - m_g * (valAbove - value));
                        }
                        else
                        {
                            green = colorBelow.G;
                        }

                        double m_r = (colorAbove.R - colorBelow.R) / (0.5 * (valAbove - valBelow));
                        if (m_r > 0)
                        {
                            red = Min(colorBelow.R + m_r * (value - valBelow), colorAbove.R);
                        }
                        else if (m_r < 0)
                        {
                            red = Min(colorBelow.R, colorAbove.R - m_r * (valAbove - value));
                        }
                        else
                        {
                            red = colorBelow.R;
                        }

                        double m_a = (colorAbove.A - colorBelow.A) / (0.5 * (valAbove - valBelow));
                        if (m_a > 0)
                        {
                            alpha = Min(colorBelow.A + m_a * (value - valBelow), colorAbove.A);
                        }
                        else if (m_a < 0)
                        {
                            alpha = Min(colorBelow.A, colorAbove.A - m_a * (valAbove - value));
                        }
                        else
                        {
                            alpha = colorBelow.A;
                        }

                        return Color.FromArgb(Convert.ToInt32(alpha),Convert.ToInt32(red), Convert.ToInt32(green), Convert.ToInt32(blue));
                    }
                    else
                    {
                        return ColorStops[i].Item2;
                    }
                }
            }

            return ColorStops[0].Item2;
        }

        public static ColorGradient BlueGreenRedGradient(double minValue, double maxValue)
        {
            double middleValue = (minValue + maxValue) / 2.0;

            List<Tuple<double, Color>> colorStops = new List<Tuple<double, Color>>();

            Tuple<double, Color> stop1 = new Tuple<double, Color>(minValue, Color.FromArgb(0, 0, 255));
            Tuple<double, Color> stop2 = new Tuple<double, Color>(middleValue, Color.FromArgb(0, 255, 0));
            Tuple<double, Color> stop3 = new Tuple<double, Color>(maxValue, Color.FromArgb(255, 0, 0));

            colorStops.Add(stop1);
            colorStops.Add(stop2);
            colorStops.Add(stop3);

            return new ColorGradient(colorStops);
        }

        public static ColorGradient BlackPinkGradient(double minValue, double maxValue)
        {
            List<Tuple<double, Color>> colorStops = new List<Tuple<double, Color>>();

            Tuple<double, Color> stop1 = new Tuple<double, Color>(minValue, Color.FromArgb(0, 0, 0));
            Tuple<double, Color> stop2 = new Tuple<double, Color>(maxValue, Color.FromArgb(255, 58, 236));

            colorStops.Add(stop1);
            colorStops.Add(stop2);

            return new ColorGradient(colorStops);
        }
    }
}
