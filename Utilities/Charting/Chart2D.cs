﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot;
using System.Drawing;
using System.Linq;
using Utilities.Geometry;

namespace Utilities.Charting
{
    /// <summary>
    /// 2-Dimensional X-Y chart, derived from the abstract Chart class
    /// </summary>
    public class Chart2D : Chart
    {
        private string _horizontalAxisLabel = "X";
        private string _verticalAxisLabel = "Y";

        private double minX = 0;
        private double maxX = 0;
        private double minY = 0;
        private double maxY = 0;

        /// <summary>
        /// The OxyPlot PlotModel object.  This is where all of the chart functionality lives.
        /// </summary>
        protected PlotModel _model { get; }

        #region Properties

        /// <summary>
        /// Gets or sets the horizontal axis label (title)
        /// </summary>
        public string HorizontalAxisLabel
        {
            get
            {
                return _horizontalAxisLabel;
            }
            set
            {
                _horizontalAxisLabel = value;

                _model.Axes[0].Title = value;
            }
        }

        /// <summary>
        /// Gets or sets the vertical axis label (title)
        /// </summary>
        public string VerticalAxisLabel
        {
            get
            {
                return _verticalAxisLabel;
            }
            set
            {
                _verticalAxisLabel = value;

                _model.Axes[1].Title = value;
            }
        }

        public PlotModel Model
        {
            get
            {
                return _model;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new 2D Chart with X and Y axes, a white background, and a legend in the top right
        /// </summary>
        /// <param name="title">Chart title</param>
        public Chart2D(string title)
            : base(title)
        {
            var white = Color.White;

            _model = new PlotModel
            {
                PlotType = PlotType.XY,
                PlotAreaBackground = OxyColor.FromArgb(white.A, white.R, white.G, white.B),
                Title = title
            };

            _model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                Minimum = -10,
                Maximum = 10,
                Title = _horizontalAxisLabel
            });

            _model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                Minimum = -10,
                Maximum = 10,
                Title = _verticalAxisLabel
            });

            _model.LegendPosition = LegendPosition.RightTop;
            _model.LegendPlacement = LegendPlacement.Outside;
        }

        #endregion

        #region Data
        /// <summary>
        /// Adds a linear data series from a list of Point2D data
        /// </summary>
        /// <param name="data">List of Point2D representing X & Y coordinates</param>
        /// <param name="label">Series name / label</param>
        /// <param name="lineColor">Series line color</param>
        /// <param name="lineStyle">Series line style</param>
        /// <param name="marker">Series marker style (note that marker color is the same as the series color)</param>
        public void AddSeriesFromXY(List<PointD> data, string label, Color lineColor,
            LineStyle lineStyle, MarkerType marker)
        {
            var xySeries = new LineSeries
            {
                Title = label,
                Color = GetOxyColorFromDrawingColor(lineColor),
                LineStyle = lineStyle,
                MarkerType = marker
            };

            xySeries.MarkerFill = xySeries.Color;
            xySeries.MarkerStroke = xySeries.Color;

            for (int i = 0; i < data.Count; i++)
            {
                PointD p = data[i];

                xySeries.Points.Add(new DataPoint(p.X, p.Y));
            }

            xySeries.InterpolationAlgorithm = InterpolationAlgorithms.CanonicalSpline;

            _model.Series.Add(xySeries);

            for (int i = 0; i < data.Count; i++)
            {
                PointD p = data[i];
                if (_model.Series.Count < 2)
                {
                    if (i == 0)
                    {
                        minX = p.X;
                        maxX = p.X;
                        minY = p.Y;
                        maxY = p.Y;
                    }
                }
                
                if (p.X > maxX)
                {
                    maxX = p.X;
                }
                if (p.X < minX)
                {
                    minX = p.X;
                }
                if (p.Y > maxY)
                {
                    maxY = p.Y;
                }
                if (p.Y < minY)
                {
                    minY = p.Y;
                }
            }
            
            _model.Axes[0].Minimum = minX - Math.Abs(0.1 * minX);
            _model.Axes[0].Maximum = maxX + Math.Abs(0.1 * maxX);
            _model.Axes[1].Minimum = minY - Math.Abs(0.1 * minY);
            _model.Axes[1].Maximum = maxY + Math.Abs(0.1 * maxY);
        }

        /// <summary>
        /// Adds a linear data series from a list of Point2D data, with no markers
        /// </summary>
        /// <param name="data">List of Point2D representing X & Y coordinates</param>
        /// <param name="label">Series name / label</param>
        /// <param name="lineColor">Series line color</param>
        /// <param name="lineStyle">Series line style</param>
        public void AddSeriesFromXY(List<PointD> data, string label, Color lineColor,
            LineStyle lineStyle)
        {
            AddSeriesFromXY(data, label, lineColor, lineStyle, MarkerType.None);
        }

        /// <summary>
        /// Adds a linear data series from a list of Point2D data, with solid line and no markers
        /// </summary>
        /// <param name="data">List of Point2D representing X & Y coordinates</param>
        /// <param name="label">Series name / label</param>
        /// <param name="lineColor">Series line color</param>
        public void AddSeriesFromXY(List<PointD> data, string label, Color lineColor)
        {
            AddSeriesFromXY(data, label, lineColor, LineStyle.Automatic, MarkerType.None);
        }

        /// <summary>
        /// Adds a linear data series from a list of Point2D data, with solid, blue line and no markers
        /// </summary>
        /// <param name="data">List of Point2D representing X & Y coordinates</param>
        /// <param name="label">Series name / label</param>
        public void AddSeriesFromXY(List<PointD> data, string label)
        {
            AddSeriesFromXY(data, label, Color.Blue, LineStyle.Automatic, MarkerType.None);
        }

        /// <summary>
        /// Adds a linear data series from a list of Point2D data, with solid, blue line
        /// </summary>
        /// <param name="data">List of Point2D representing X & Y coordinates</param>
        /// <param name="label">Series name / label</param>
        /// <param name="marker">Series marker style (note that marker color is the same as the series color)</param>
        public void AddSeriesFromXY(List<PointD> data, string label, MarkerType marker)
        {
            AddSeriesFromXY(data, label, Color.Blue, LineStyle.Automatic, marker);
        }
        #endregion

        #region Exporting
        /// <summary>
        /// Exports the chart to an SVG image
        /// </summary>
        /// <param name="pixelWidth">Width of SVG image, in pixels</param>
        /// <param name="pixelHeight">Height of SVG image, in pixels</param>
        /// <returns></returns>
        public string ExportToSVG(int pixelWidth, int pixelHeight)
        {
            var exp = new SvgExporter
            {
                Width = pixelWidth,
                Height = pixelHeight
            };

            return exp.ExportToString(_model);
        }

        #endregion

        #region Axis Controls
        /// <summary>
        /// Sets the horizontal axis minimum and maximum bounds
        /// </summary>
        /// <param name="min">Minimum axis bound</param>
        /// <param name="max">Maximum axis bound</param>
        public void SetHorizontalAxisBounds(double min, double max)
        {
            _model.Axes[0].Minimum = min;
            _model.Axes[0].Maximum = max;
        }

        /// <summary>
        /// Sets the vertical axis minimum and maximum bounds
        /// </summary>
        /// <param name="min">Minimum axis bounds</param>
        /// <param name="max">Maximum axis bounds</param>
        public void SetVerticalAxisBounds(double min, double max)
        {
            _model.Axes[1].Minimum = min;
            _model.Axes[1].Maximum = max;
        }

        /// <summary>
        /// Sets the horizontal axis to either normal or logarithmic scale
        /// </summary>
        /// <param name="targetScale">Normal or Logarithmic</param>
        public void SetHorizontalAxisScale(AxisScale targetScale)
        {
            string axisName = _model.Axes[0].Title;
            bool logarithmic = _model.Axes[0].IsLogarithmic();
            double axisMin = _model.Axes[0].Minimum;
            double axisMax = _model.Axes[0].Maximum;

            if (targetScale == AxisScale.Logarithmic && !logarithmic)
            {
                var newAxis = new LogarithmicAxis
                {
                    Title = axisName,
                    Minimum = axisMin,
                    Maximum = axisMax,
                    Position = AxisPosition.Bottom,
                };

                _model.Axes[0] = newAxis;

                _model.InvalidatePlot(false);
            }
            else if (targetScale == AxisScale.Normal && logarithmic)
            {
                var newAxis = new LinearAxis
                {
                    Title = axisName,
                    Minimum = axisMin,
                    Maximum = axisMax,
                    Position = AxisPosition.Bottom
                };

                _model.Axes[0] = newAxis;

                _model.InvalidatePlot(false);
            }
        }

        /// <summary>
        /// Sets the vertical axis to either normal or logarithmic scale
        /// </summary>
        /// <param name="targetScale"></param>
        public void SetVerticalAxisScale(AxisScale targetScale)
        {
            string axisName = _model.Axes[1].Title;
            bool logarithmic = _model.Axes[1].IsLogarithmic();
            double axisMin = _model.Axes[1].Minimum;
            double axisMax = _model.Axes[1].Maximum;

            if (targetScale == AxisScale.Logarithmic && !logarithmic)
            {
                var newAxis = new LogarithmicAxis
                {
                    Title = axisName,
                    Minimum = axisMin,
                    Maximum = axisMax,
                    Position = AxisPosition.Bottom,
                };

                _model.Axes[1] = newAxis;

                _model.InvalidatePlot(false);
            }
            else if (targetScale == AxisScale.Normal && logarithmic)
            {
                var newAxis = new LinearAxis
                {
                    Title = axisName,
                    Minimum = axisMin,
                    Maximum = axisMax,
                    Position = AxisPosition.Bottom
                };

                _model.Axes[1] = newAxis;

                _model.InvalidatePlot(false);
            }
        }

        #endregion

        #region Line Controls
        public void SetInterpolation(InterpolationType interp)
        {
            if (_model.Series != null)
            {
                if (_model.Series.Count > 0)
                {
                    bool canInterpolate = true;

                    IInterpolationAlgorithm interpAlgorithm = null;

                    switch (interp)
                    {
                        case InterpolationType.None:
                            canInterpolate = false;
                            interpAlgorithm = null;
                            break;
                        case InterpolationType.CanonicalSpline:
                            interpAlgorithm = InterpolationAlgorithms.CanonicalSpline;
                            break;
                        case InterpolationType.CatmullRomSpline:
                            interpAlgorithm = InterpolationAlgorithms.CatmullRomSpline;
                            break;
                        case InterpolationType.ChordalCatmullRomSpline:
                            interpAlgorithm = InterpolationAlgorithms.ChordalCatmullRomSpline;
                            break;
                        case InterpolationType.UniformCatmullRomSpline:
                            interpAlgorithm = InterpolationAlgorithms.UniformCatmullRomSpline;
                            break;
                        default:
                            canInterpolate = false;
                            interpAlgorithm = null;
                            break;
                    }

                    foreach (LineSeries ser in _model.Series)
                    {
                        ser.InterpolationAlgorithm = interpAlgorithm;
                        ser.CanTrackerInterpolatePoints = canInterpolate;
                    }
                }
            }

        }
        #endregion
    }
}
