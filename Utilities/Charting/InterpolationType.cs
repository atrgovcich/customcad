﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Charting
{
    public enum InterpolationType
    {
        None,
        CanonicalSpline,
        CatmullRomSpline,
        UniformCatmullRomSpline,
        ChordalCatmullRomSpline
    }
}
