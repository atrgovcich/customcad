﻿using OxyPlot;
using System.Drawing;

namespace Utilities.Charting
{
    public abstract class Chart
    {
        public string Title { get; }

        protected Chart(string title)
        {
            Title = title;
        }

        protected static OxyColor GetOxyColorFromDrawingColor(Color color)
        {
            return OxyColor.FromArgb(color.A, color.R, color.G, color.B);
        }
    }
}
