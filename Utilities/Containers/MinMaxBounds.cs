﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;

namespace Utilities.Containers
{
    public class MinMaxBounds
    {
        public double MaxX { get; set; }
        public double MinX { get; set; }
        public double MaxY { get; set; }
        public double MinY { get; set; }

        public MinMaxBounds GetMaximumBounds(MinMaxBounds other)
        {
            MinMaxBounds newBounds = new MinMaxBounds();

            newBounds.MaxX = Max(MaxX, other.MaxX);
            newBounds.MaxY = Max(MaxY, other.MaxY);
            newBounds.MinX = Min(MinX, other.MinX);
            newBounds.MinY = Min(MinY, other.MinY);

            return newBounds;
        }
    }
}
