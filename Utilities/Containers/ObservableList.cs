﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Utilities.Containers
{
    //https://codereview.stackexchange.com/questions/111358/eventlistt-a-generic-list-implementation-that-raises-events-when-items-are-add
    public class ObservableList<T> : IList<T>
    {
        [JsonProperty]
        private IList<T> _list;

        private Dictionary<T, int> _drawOrder;

        //private List<KeyValuePair<T, int>> _drawOrderList;

        [JsonProperty]
        private int _minDrawIndex = 0;
        [JsonProperty]
        private int _maxDrawIndex = 0;

        [JsonConstructor]
        public ObservableList()
        {
            _list = new List<T>();
            _drawOrder = new Dictionary<T, int>();
        }

        public List<KeyValuePair<T, int>> DrawOrderList
        {
            get
            {
                return _drawOrder.ToList();
            }
            set
            {
                _drawOrder = value.ToDictionary(kv => kv.Key, kv => kv.Value);
            }
        }

        [JsonIgnore]
        public IList<T> OrderedList
        {
            get
            {
                return _drawOrder.OrderBy(x => x.Value).Select(x => x.Key).ToList();
            }
        }

        public bool ContainedElementModified { get; set; } = true;

        public ObservableList(IEnumerable<T> collection)
        {
            _list = new List<T>(collection);

            int i = -1;

            foreach (T val in collection)
            {
                i++;
                _drawOrder.Add(val, i);
            }

            _maxDrawIndex = i;
        }

        public ObservableList(int capacity)
        {
            _list = new List<T>(capacity);
        }

        public event EventHandler<EventListArgs<T>> ItemAdded;
        public event EventHandler<EventListArgs<T>> ItemRemoved;

        private void RaiseEvent(EventHandler<EventListArgs<T>> eventHandler, T item, int index)
        {
            var eh = eventHandler;
            eh?.Invoke(this, new EventListArgs<T>(item, index));
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            var index = _list.Count;
            _list.Add(item);
            _maxDrawIndex++;
            _drawOrder.Add(item, _maxDrawIndex);
            RaiseEvent(ItemAdded, item, index);
        }

        public void AddRange(IEnumerable<T> itemList)
        {
            if (itemList != null)
            {
                bool addedItems = false;

                List<T> itemsAdded = null;

                int index = _list.Count;

                foreach(T item in itemList)
                {
                    _list.Add(item);
                    _maxDrawIndex++;
                    _drawOrder.Add(item, _maxDrawIndex);

                    if (addedItems == false)
                    {
                        addedItems = true;
                        itemsAdded = new List<T>();
                        itemsAdded.Add(item);
                    }
                }

                if (addedItems == true)
                {
                    RaiseEvent(ItemAdded, itemsAdded[0], index);
                }
            }
        }

        public void Clear()
        {
            if (_list.Count > 0)
            {
                T firstItem = _list[0];

                _list.Clear();

                _drawOrder.Clear();

                _minDrawIndex = 0;

                _maxDrawIndex = 0;

                RaiseEvent(ItemRemoved, firstItem, -1);
            }
           
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            var index = _list.IndexOf(item);

            if (index < 0)
                return false;

            RemoveAt(index);

            return true;
        }

        public bool RemoveRange(IList<T> items)
        {
            Dictionary<T, int> indeces = new Dictionary<T, int>(_list.Count);

            for (int i = 0; i < _list.Count; i++)
            {
                indeces.Add(_list[i], i);
            }

            List<T> itemsSorted = items.OrderBy(x => indeces[x]).ToList();

            for (int i = itemsSorted.Count - 1; i >= 0; i--)
            {
                if (indeces.TryGetValue(itemsSorted[i], out int ind) == true)
                {
                    RemoveAt(ind);
                }
            }

            return true;
        }

        public int Count => _list.Count;
        public bool IsReadOnly => false;

        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _list.Insert(index, item);
            _maxDrawIndex++;
            _drawOrder.Add(item, _maxDrawIndex);
            RaiseEvent(ItemAdded, item, index);
        }

        public void RemoveAt(int index)
        {
            var item = _list[index];

            _list.RemoveAt(index);

            _drawOrder.Remove(item);

            RaiseEvent(ItemRemoved, item, index);
        }

        public T this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }

        public int GetDrawOrder(T item)
        {
            if (_drawOrder.TryGetValue(item, out int drawIndex) == true)
            {
                return drawIndex;
            }
            else
            {
                return int.MinValue;
            }
        }

        public void BringToFront(T item)
        {
            if (_drawOrder.TryGetValue(item, out int drawIndex) == true)
            {
                //The item exists in the dictionary
                _maxDrawIndex++;
                _drawOrder[item] = _maxDrawIndex;

                _list = _list.OrderBy(x => _drawOrder[x]).ToList();
            }
        }

        public void BringToFront(IEnumerable<T> itemList)
        {
            if (itemList != null)
            {
                foreach (T item in itemList)
                {
                    if (_drawOrder.TryGetValue(item, out int drawIndex) == true)
                    {
                        //The item exists in the dictionary
                        _maxDrawIndex++;
                        _drawOrder[item] = _maxDrawIndex;
                    }
                }

                _list = _list.OrderBy(x => _drawOrder[x]).ToList();
            }
        }

        public void SendToBack(T item)
        {
            if (_drawOrder.TryGetValue(item, out int drawIndex) == true)
            {
                //The item exists in the dictionary
                _minDrawIndex--;
                _drawOrder[item] = _minDrawIndex;

                _list = _list.OrderBy(x => _drawOrder[x]).ToList();
            }
        }

        public void SendToBack(IEnumerable<T> itemList)
        {
            if (itemList != null)
            {
                foreach (T item in itemList)
                {
                    if (_drawOrder.TryGetValue(item, out int drawIndex) == true)
                    {
                        //The item exists in the dictionary
                        _minDrawIndex--;
                        _drawOrder[item] = _minDrawIndex;
                    }
                }

                _list = _list.OrderBy(x => _drawOrder[x]).ToList();
            }
        }

        #region Linq
        public IEnumerable<TResult> Select<TResult>(Func<T, TResult> selector)
        {
            if (_list == null)
            {
                throw new Exception("List is null.");
            }

            if (selector == null)
            {
                throw new Exception("Selector is null.");
            }

            return _list.Select(selector);
        }

        public IOrderedEnumerable<T> OrderByDescending<TKey>(Func<T, TKey> selector)
        {
            if (_list == null)
            {
                throw new Exception("List is null.");
            }

            if (selector == null)
            {
                throw new Exception("Selector is null.");
            }

            return _list.OrderByDescending(selector);
        }

        public IOrderedEnumerable<T> OrderBy<TKey>(Func<T, TKey> selector)
        {
            if (_list == null)
            {
                throw new Exception("List is null.");
            }

            if (selector == null)
            {
                throw new Exception("Selector is null.");
            }

            return _list.OrderBy(selector);
        }

        public IEnumerable<T> Where(Func<T, bool> predicate)
        {
            if (_list == null)
            {
                throw new Exception("List is null.");
            }

            if (predicate == null)
            {
                throw new Exception("Predicate is null.");
            }

            return _list.Where(predicate);
        }

        public List<T> ToList()
        {
            return _list.ToList();
        }
        #endregion
    }

    public class EventListArgs<T> : EventArgs
    {
        public EventListArgs(T item, int index)
        {
            Item = item;
            Index = index;
        }

        public T Item { get; }
        public int Index { get; }
    }
}
