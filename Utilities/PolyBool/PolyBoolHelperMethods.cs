﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using PolyBoolCS;
using System.Linq;
using static System.Math;

using PointPB = PolyBoolCS.Point;

namespace Utilities.PolyBool
{
    public static class PolyBoolHelperMethods
    {
        public static List<GenericPolygon> ConvertFromPolyBoolPolygon(PointList region)
        {
            List<PointPB> vertices = PolyBoolHelper.ConvertRegionToPointList(region);

            var convertedVertices = ConvertFromPolyBoolPoints(vertices).ToList();

            var convertedBoundaries = new List<GenericPolygon>();

            List<PointD> nodeList = new List<PointD>(convertedVertices.Count + 1);

            for (int i = 0; i < convertedVertices.Count; i++)
            {
                nodeList.Add(convertedVertices[i]);
            }

            nodeList.Add(nodeList[0]); //Close the polygon

            GenericPolygon poly = new GenericPolygon(nodeList);

            convertedBoundaries.Add(poly);

            return convertedBoundaries;
        }

        public static IEnumerable<PointPB> ConvertToPolyBoolPoints(IEnumerable<PointD> points)
        {
            var convertedPoints = new List<PointPB>();

            foreach (var point in points)
            {
                convertedPoints.Add(point.ConvertToPolyBoolPoint());
            }

            return convertedPoints;
        }

        public static IEnumerable<PointD> ConvertFromPolyBoolPoints(IEnumerable<PointPB> points)
        {
            var convertedPoints = new List<PointD>();

            foreach (var point in points)
            {
                convertedPoints.Add(ConvertFromPolyBoolPoint(point));
            }

            return convertedPoints;
        }

        public static PointD ConvertFromPolyBoolPoint(PointPB point)
        {
            return new PointD(point.x, point.y);
        }

        private static List<GenericPolygon> ConvertPolyBoolRegionsToPolygonG(Polygon poly)
        {
            if (poly == null)
            {
                return new List<GenericPolygon>();
            }

            List<GenericPolygon> shapeList = new List<GenericPolygon>(poly.regions.Count);

            for (int i = 0; i < poly.regions.Count; i++)
            {
                PointList region = poly.regions[i];

                List<PointPB> pointList = PolyBoolHelper.ConvertRegionToPointList(region);

                List<PointD> nodeList = new List<PointD>(pointList.Count + 1);

                for (int j = 0; j < pointList.Count; j++)
                {
                    PointPB p = pointList[j];

                    PointD pD = ConvertFromPolyBoolPoint(p);

                    nodeList.Add(pD);
                }

                nodeList.Add(nodeList[0]); //Close the polygon

                GenericPolygon convertedPoly = new GenericPolygon(nodeList);

                shapeList.Add(convertedPoly);
            }

            return shapeList;
        }

        public static List<GenericPolygon> BooleanUnion(List<GenericPolygon> polygons, double orthogonalTolerance)
        {
            List<PolyBoolCS.Polygon> polyBoolPolygons = new List<PolyBoolCS.Polygon>();

            for (int i = 0; i < polygons.Count(); i++)
            {
                GenericPolygon polygon = polygons[i];

                List<PointPB> pointList = new List<PointPB>();

                List<PointD> polygonPoints = new List<PointD>(polygon.Points);

                polygonPoints.RemoveAt(polygonPoints.Count - 1);

                for (int j = 0; j < polygonPoints.Count; j++)
                {
                    PointD point = polygonPoints[j];

                    pointList.Add(point.ConvertToPolyBoolPoint());
                }

                polyBoolPolygons.Add(PolyBoolHelper.ConvertToPolygon(pointList));
            }

            Polygon unionedPolygon = GetUnionOfPolygons(polyBoolPolygons);

            List<GenericPolygon> unionedPolygonG = ConvertPolyBoolRegionsToPolygonG(unionedPolygon);

            return unionedPolygonG;
        }

        public static List<GenericPolygon> BooleanDifference(GenericPolygon poly1, GenericPolygon poly2)
        {
            var convertedBoundary = poly1.ConvertToPolyBoolPolygon();
            var convertedOther = poly2.ConvertToPolyBoolPolygon();

            var polybool = new PolyBoolCS.PolyBool();

            Polygon difference = polybool.difference(convertedBoundary, convertedOther);

            if (difference.regions.Any())
            {
                var differencePolygons = new List<GenericPolygon>();

                foreach (var region in difference.regions)
                {
                    differencePolygons.AddRange(ConvertFromPolyBoolPolygon(region));
                }

                return differencePolygons;
            }

            return new List<GenericPolygon>();
        }

        public static List<GenericPolygon> BooleanIntersection(GenericPolygon poly1, GenericPolygon poly2)
        {
            var convertedBoundary = poly1.ConvertToPolyBoolPolygon();
            var convertedOther = poly2.ConvertToPolyBoolPolygon();

            var polybool = new PolyBoolCS.PolyBool();

            Polygon intersection = polybool.intersect(convertedBoundary, convertedOther);

            if (intersection.regions.Any())
            {
                var overlappingPolygons = new List<GenericPolygon>();

                foreach (var region in intersection.regions)
                {
                    if (region.Count != 0)
                    {
                        overlappingPolygons.AddRange(ConvertFromPolyBoolPolygon(region));
                    }
                }

                return overlappingPolygons;
            }
            else
            {
                if (convertedBoundary.regions.Count == 1)
                {
                    if (convertedOther.regions.Count == 1)
                    {
                        if (AreRegionsExactlyTheSame(convertedBoundary.regions[0], convertedOther.regions[0]) == true)
                        {
                            var overlappingPolygons = new List<GenericPolygon>();

                            overlappingPolygons.AddRange(ConvertFromPolyBoolPolygon(convertedBoundary.regions[0]));

                            return overlappingPolygons;
                        }
                    }
                }
            }

            return new List<GenericPolygon>();
        }

        private static bool AreRegionsExactlyTheSame(PointList region1, PointList region2)
        {
            bool same = true;

            if (region1.Count == region2.Count)
            {
                for (int i = 0; i < region1.Count; i++)
                {
                    if (region1[i] != region2[i])
                    {
                        same = false;
                        break;
                    }
                }
            }
            else
            {
                same = false;
            }

            if (same == false)
            {
                PointList region1_R = new PointList(); //resampled
                PointList region2_R = new PointList(); //resampled

                for (int i = 0; i < region1.Count; i++)
                {
                    region1_R.Add(region1[i]);
                }

                for (int i = 0; i < region2.Count; i++)
                {
                    region2_R.Add(region2[i]);
                }

                int count = 0;

                while (count <= region1_R.Count - 3)
                {
                    PointPB p1 = region1_R[count];
                    PointPB p2 = region1_R[count + 1];
                    PointPB p3 = region1_R[count + 2];

                    double m12, m23;

                    if (p1.x == p2.x)
                    {
                        if (p2.x == p3.x)
                        {
                            //slopes are the same, remove point 2
                            region1_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.y - p1.y) / (p2.x - p1.x);
                        if (p2.x != p3.x)
                        {
                            m23 = (p3.y - p2.y) / (p3.x - p2.x);
                            if (m12 == m23)
                            {
                                region1_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                count = 0;

                while (count <= region2_R.Count - 3)
                {
                    PointPB p1 = region2_R[count];
                    PointPB p2 = region2_R[count + 1];
                    PointPB p3 = region2_R[count + 2];

                    double m12, m23;

                    if (p1.x == p2.x)
                    {
                        if (p2.x == p3.x)
                        {
                            //slopes are the same, remove point 2
                            region2_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.y - p1.y) / (p2.x - p1.x);
                        if (p2.x != p3.x)
                        {
                            m23 = (p3.y - p2.y) / (p3.x - p2.x);
                            if (m12 == m23)
                            {
                                region2_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                //Now both polygons are resampled

                if (region1_R.Count != region2_R.Count)
                {
                    return false;
                }
                else
                {
                    int numSame = 0;

                    for (int i = 0; i < region1_R.Count; i++)
                    {
                        PointPB p1 = region1_R[i];

                        for (int j = 0; j < region2_R.Count; j++)
                        {
                            PointPB p2 = region2_R[j];

                            if (p1 == p2)
                            {
                                numSame++;
                                break;
                            }
                        }
                    }

                    if (numSame == region1_R.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                return true;
            }
        }

        public static PolyBoolCS.Polygon GetUnionOfPolygons(List<PolyBoolCS.Polygon> polygonList)
        {
            PolyBoolCS.Polygon poly = null;

            if (polygonList.Count == 1)
            {
                return polygonList[0];
            }
            else
            {
                for (int i = 1; i < polygonList.Count; i++)
                {
                    if (i == 1)
                    {
                        poly = new PolyBoolCS.PolyBool().union(polygonList[0], polygonList[i]);
                    }
                    else
                    {
                        poly = new PolyBoolCS.PolyBool().union(poly, polygonList[i]);
                    }
                }
            }

            return poly;
        }
    }
}
