﻿using System;
using System.IO;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static void WriteMatrix(double[,] A)
        {
            string fileName = Path.GetTempPath() + "Matrix.txt";

            if (fileName != null)
            {
                using (StreamWriter strWriter = new StreamWriter(fileName))
                {
                    for (int i = 0; i <= A.GetUpperBound(0); i++)
                    {
                        string myStr = "";
                        for (int j = 0; j <= A.GetUpperBound(1); j++)
                        {
                            if (j < A.GetUpperBound(1))
                            {
                                myStr = myStr + Convert.ToString(A[i, j]) + ",";
                            }
                            else
                            {
                                myStr = myStr + Convert.ToString(A[i, j]);
                            }
                        }

                        strWriter.WriteLine(myStr);
                    }
                }
            }
        }
    }
}
