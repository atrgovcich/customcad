﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] eye(int n)
        {
            //n is the number of indeces of the square matrix
            if (n < 1)
            {
                throw new ArgumentException("The number of indeces must be greater than or equal to 1 (leading to a 2x2 or larger matrix).");
            }
            double[,] B = new double[n + 1, n + 1];

            int i = 0;
            int j = 0;

            for (i = 0; i <= n; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    if (i == j)
                    {
                        B[i, j] = 1d;
                    }
                    else
                    {
                        B[i, j] = 0d;
                    }
                }
            }

            return B;
        }
    }
}
