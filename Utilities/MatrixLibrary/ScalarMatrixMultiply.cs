﻿namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] ScalarMatrixMult(double A, double[,] B)
        {
            double[,] C = new double[B.GetUpperBound(0) + 1, B.GetUpperBound(1) + 1];

            int i = 0;
            int j = 0;

            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                for (j = 0; j <= B.GetUpperBound(1); j++)
                {
                    C[i, j] = B[i, j] * A;
                }
            }

            return C;
        }
    }
}
