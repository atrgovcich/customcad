﻿using System.Collections.Generic;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] RemoveRow(double[,] A, int[] C)
        {

            List<double[]> RowList = new List<double[]>();

            for (int i = 0; i <= A.GetUpperBound(0); i++)
            {
                double[] row = new double[A.GetUpperBound(1) + 1];
                for (int j = 0; j <= A.GetUpperBound(1); j++)
                {
                    row[j] = A[i, j];
                }

                RowList.Add(row);
            }

            List<int> IndexList = new List<int>();

            for (int i = 0; i <= RowList.Count - 1; i++)
            {
                IndexList.Add(i);
            }


            for (int i = 0; i <= C.GetUpperBound(0); i++)
            {
                //Find the index that we need to remove
                int IndexToRemove = -1;
                for (int j = 0; j <= IndexList.Count - 1; j++)
                {
                    if (IndexList[j] == C[i])
                    {
                        IndexToRemove = j;
                        break;
                    }
                }

                RowList.RemoveAt(IndexToRemove);
                IndexList.RemoveAt(IndexToRemove);
            }



            double[,] Ret = new double[RowList.Count, RowList[0].GetUpperBound(0) + 1];
            for (int i = 0; i <= RowList.Count - 1; i++)
            {
                for (int j = 0; j <= RowList[i].GetUpperBound(0); j++)
                {
                    Ret[i, j] = RowList[i][j];
                }
            }



            return Ret;
        }

        public static int[,] RemoveRow(int[,] A, int[] C)
        {

            List<int[]> RowList = new List<int[]>();

            for (int i = 0; i <= A.GetUpperBound(0); i++)
            {
                int[] row = new int[A.GetUpperBound(1) + 1];
                for (int j = 0; j <= A.GetUpperBound(1); j++)
                {
                    row[j] = A[i, j];
                }

                RowList.Add(row);
            }

            List<int> IndexList = new List<int>();

            for (int i = 0; i <= RowList.Count - 1; i++)
            {
                IndexList.Add(i);
            }


            for (int i = 0; i <= C.GetUpperBound(0); i++)
            {
                //Find the index that we need to remove
                int IndexToRemove = -1;
                for (int j = 0; j <= IndexList.Count - 1; j++)
                {
                    if (IndexList[j] == C[i])
                    {
                        IndexToRemove = j;
                        break;
                    }
                }

                RowList.RemoveAt(IndexToRemove);
                IndexList.RemoveAt(IndexToRemove);
            }



            int[,] Ret = new int[RowList.Count, RowList[0].GetUpperBound(0) + 1];
            for (int i = 0; i <= RowList.Count - 1; i++)
            {
                for (int j = 0; j <= RowList[i].GetUpperBound(0); j++)
                {
                    Ret[i, j] = RowList[i][j];
                }
            }



            return Ret;
        }
    }
}
