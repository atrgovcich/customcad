﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] ReducedRowEchelonForm(double[,] A, out double detScale)
        {
            double scale = 1;

            double[,] B = new double[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];
            Array.Copy(A, B, A.Length);

            int lead = 0;
            int rowCount = A.GetUpperBound(0) + 1;
            int colCount = A.GetUpperBound(1) + 1;

            for (int r = 0; r < rowCount; r++)
            {
                if (lead >= colCount)
                {
                    break;
                }
                else
                {
                    int i = r;
                    while (B[i,lead] == 0)
                    {
                        i++;
                        if (rowCount == i)
                        {
                            i = r;
                            lead++;

                            if (colCount == lead)
                            {
                                detScale = scale;
                                return B;
                            }
                        }
                    }

                    //Swap rows i and r
                    B = SwapRows(B, i, r);
                    scale = scale * -1;

                    if (B[r,lead] != 0)
                    {
                        //divide row r by B[r, lead]
                        scale = scale * B[r, lead];
                        for (int j = 0; j <= B.GetUpperBound(1); j++)
                        {
                            B[r, j] /= B[r, lead];
                        }

                    }

                    for (int j = 0; j < rowCount; j++)
                    {
                        if (j != r)
                        {
                            //Subtract B[j,lead] * row r from row j
                            for (int k = 0; k < colCount; k++)
                            {
                                B[j, k] -= B[j, lead] * B[r, k];
                            }
                        }
                    }
                    lead++;
                }
            }
            detScale = scale;
            return B;
        }
    }
}
