﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] MatrixMult(double[,] A, double[,] B)
        {
            int m = A.GetUpperBound(0);
            int n = A.GetUpperBound(1);

            int p = B.GetUpperBound(0);
            int q = B.GetUpperBound(1);

            double[,] C = new double[m + 1, q + 1];

            if (n != p)
            {
                throw new ArgumentException("The number of columns in Matrix A must equal the number of rows in Matrix B.");
            }
            else
            {

                for (int i = 0; i <= m; i++)
                {
                    for (int j = 0; j <= q; j++)
                    {
                        for (int k = 0; k <= p; k++)
                        {
                            C[i, j] += A[i, k] * B[k, j];
                        }
                    }
                }
            }

            return C;
        }
    }
}
