﻿using static System.Math;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] CofactorMatrix(double[,] A)
        {
            double[,] C = new double[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];

            int h = 0;
            int i = 0;
            int j = 0;
            int k = 0;

            for (h = 0; h <= A.GetUpperBound(0); h++)
            {
                for (i = 0; i <= A.GetUpperBound(1); i++) //Loop through the columns
                {
                    double[,] B = new double[A.GetUpperBound(0) - 1 + 1, A.GetUpperBound(1) - 1 + 1]; //Define B as a new matrix that has dimensions 1 less than A
                    int brow = 0; //Set the row index of B
                    int bcol = 0; //Set the column index of B

                    for (j = 0; j <= A.GetUpperBound(0); j++) //Loop through the rows of A starting at the 1st row
                    {
                        if (j != h)
                        {
                            for (k = 0; k <= A.GetUpperBound(1); k++) //Loop through the columns of A starting at the 1st column
                            {
                                if (k != i) //If the column is not the column we are crossign out, fill an index of B
                                {
                                    B[brow, bcol] = A[j, k];
                                    bcol += 1; //Increment the column index
                                }
                            }
                            bcol = 0; //Zero out the column index so that we start at 0 on the next row
                            brow += 1; //Increment the row index by 1
                        }
                    }
                    C[h, i] = Pow(-1, (h + 1) + (i + 1)) * Determinant(B);
                }
            }

            return C;
        }
    }
}
