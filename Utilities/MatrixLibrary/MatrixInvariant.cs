﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double Invariant(double[,] A, int Num)
        {
            double inv = 0;

            if (Num == 1)
            {
                //1st invariant
                int i = 0;
                for (i = 0; i <= A.GetUpperBound(0); i++)
                {
                    inv += A[i, i];
                }
            }
            else if (Num == 2)
            {
                //2nd invariant
                inv = (1d / 2d) * Invariant(MatrixMult(A, A), 1);
            }
            else if (Num == 3)
            {
                inv = Determinant(A);
            }
            else
            {
                throw new ArgumentException("The invariant number must be either 1, 2, or 3.");
            }

            return inv;
        }
    }
}
