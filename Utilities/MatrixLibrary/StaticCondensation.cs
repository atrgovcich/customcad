﻿using System;
using System.Collections.Generic;
using Utilities.Extensions.DotNetNative;
using static Utilities.Extensions.DotNetNative.DoubleExtensions;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static StaticCondensationReturn StaticCondensation(double[,] Kmatrix, double[,] Fmatrix, List<int> DoFtoCondenseOut, bool Expand = true)
        {
            if (DoFtoCondenseOut.Count < 1)
            {
                return new StaticCondensationReturn
                {
                    Kc = Kmatrix,
                    Fc = Fmatrix,
                    DoF_Index = new int[0, 0]
                };
            }
            int[,] RowIndex = new int[Kmatrix.GetUpperBound(0) + 1, 1];
            int[,] ColIndex = new int[Kmatrix.GetUpperBound(1) + 1, 1];

            double[,] newKmatrix = new double[Kmatrix.GetUpperBound(0) + 1, Kmatrix.GetUpperBound(1) + 1];
            Array.Copy(Kmatrix, newKmatrix, Kmatrix.Length);

            double[,] newFmatrix = new double[Fmatrix.GetUpperBound(0) + 1, Fmatrix.GetUpperBound(1) + 1];
            Array.Copy(Fmatrix, newFmatrix, Fmatrix.Length);

            for (int i = 0; i <= Kmatrix.GetUpperBound(0); i++)
            {
                RowIndex[i, 0] = i;
            }
            for (int i = 0; i <= Kmatrix.GetUpperBound(1); i++)
            {
                ColIndex[i, 0] = i;
            }

            int NextSwitch = Kmatrix.GetUpperBound(0);

            for (int i = Kmatrix.GetUpperBound(0); i >= 0; i--)
            {

                if (DoFtoCondenseOut.Contains(i) == true)
                {
                    newKmatrix = SwapRows(newKmatrix, i, NextSwitch);
                    newKmatrix = SwapColumns(newKmatrix, i, NextSwitch);

                    newFmatrix = SwapRows(newFmatrix, i, NextSwitch);

                    RowIndex = SwapRows(RowIndex, i, NextSwitch);
                    ColIndex = SwapRows(ColIndex, i, NextSwitch);

                    NextSwitch -= 1;
                }
            }

            //Re-arrange matrices so that the DoFs are in order
            List<int> IndexList = new List<int>();
            for (int i = 0; i <= RowIndex.GetUpperBound(0) - DoFtoCondenseOut.Count; i++)
            {
                IndexList.Add(RowIndex[i, 0]);
            }

            IndexList.Sort();

            int k = 0;

            while (k <= RowIndex.GetUpperBound(0) - DoFtoCondenseOut.Count)
            {
                if (k != IndexList.IndexOf(RowIndex[k, 0]))
                {
                    newKmatrix = SwapRows(newKmatrix, k, IndexList.IndexOf(RowIndex[k, 0]));
                    newKmatrix = SwapColumns(newKmatrix, k, IndexList.IndexOf(RowIndex[k, 0]));

                    newFmatrix = SwapRows(newFmatrix, k, IndexList.IndexOf(RowIndex[k, 0]));

                    RowIndex = SwapRows(RowIndex, k, IndexList.IndexOf(RowIndex[k, 0]));
                    ColIndex = SwapRows(ColIndex, k, IndexList.IndexOf(RowIndex[k, 0]));

                    k -= 1;
                }
                k += 1;
            }

            double[,] K11 = new double[newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1, newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count + 1];
            double[,] K12 = new double[newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1, DoFtoCondenseOut.Count];
            double[,] K21 = new double[DoFtoCondenseOut.Count, newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1];
            double[,] K22 = new double[DoFtoCondenseOut.Count, DoFtoCondenseOut.Count];

            double[,] f1 = new double[newFmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1, 1];
            double[,] f2 = new double[DoFtoCondenseOut.Count, 1];

            for (int i = 0; i <= newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count; i++)
            {
                for (int j = 0; j <= newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count; j++)
                {
                    K11[i, j] = newKmatrix[i, j];
                }
            }

            for (int i = 0; i <= newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count; i++)
            {
                for (int j = newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count + 1; j <= newKmatrix.GetUpperBound(1); j++)
                {
                    K12[i, j - (newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count + 1)] = newKmatrix[i, j];
                }
            }

            for (int i = newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1; i <= newKmatrix.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count; j++)
                {
                    K21[i - (newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1), j] = newKmatrix[i, j];
                }
            }

            for (int i = newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1; i <= newKmatrix.GetUpperBound(0); i++)
            {
                for (int j = newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count + 1; j <= newKmatrix.GetUpperBound(1); j++)
                {
                    K22[i - (newKmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1), j - (newKmatrix.GetUpperBound(1) - DoFtoCondenseOut.Count + 1)] = newKmatrix[i, j];
                }
            }

            for (int i = 0; i <= newFmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count; i++)
            {
                f1[i, 0] = newFmatrix[i, 0];
            }

            for (int i = newFmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1; i <= newFmatrix.GetUpperBound(0); i++)
            {
                f2[i - (newFmatrix.GetUpperBound(0) - DoFtoCondenseOut.Count + 1), 0] = newFmatrix[i, 0];
            }

            double[,] Kc;
            double[,] fc;

            Kc = MatrixAdd(K11, ScalarMatrixMult(-1d, MatrixMult(K12, MatrixMult(InverseMatrix(K22), K21))));
            fc = MatrixAdd(f1, ScalarMatrixMult(-1d, MatrixMult(K12, MatrixMult(InverseMatrix(K22), f2))));

            //If either Kc or fc is NaN or infinity, it means that the K22 matrix was not invertible
            //In this case, it generally means that the K22 matrix is all zeroes, as we are trying
            //to condense out degrees of freedom with zero stiffness.  In this case, set the condensed
            //stiffness matrix to the K11 matrix and the condensed force vector to the f1 vector.
            if (Kc.ContainsNanOrInfinity() == true || fc.ContainsNanOrInfinity() == true)
            {
                Kc = K11;
                fc = f1;
            }

            double[,] Kc_exp;

            if (Expand == true)
            {
                Kc_exp = new double[Kmatrix.GetUpperBound(0) + 1, Kmatrix.GetUpperBound(1) + 1];

                for (int i = 0; i <= Kc_exp.GetUpperBound(0); i++)
                {
                    for (int j = 0; j <= Kc_exp.GetUpperBound(1); j++)
                    {
                        if (i > Kc_exp.GetUpperBound(0) - DoFtoCondenseOut.Count)
                        {
                            Kc_exp[i, j] = 0;
                        }
                        else if (j > Kc_exp.GetUpperBound(1) - DoFtoCondenseOut.Count)
                        {
                            Kc_exp[i, j] = 0;
                        }
                        else
                        {
                            Kc_exp[i, j] = Kc[i, j];
                        }
                    }
                }

                //After expanding the condensed matrix to include the condensed DoFs with zero stiffnessj
                //re-arrange again back into original order
                k = 0;

                while (k <= Kc_exp.GetUpperBound(0))
                {
                    if (k != RowIndex[k, 0])
                    {
                        Kc_exp = SwapRows(Kc_exp, k, RowIndex[k, 0]);
                        Kc_exp = SwapColumns(Kc_exp, k, RowIndex[k, 0]);

                        RowIndex = SwapRows(RowIndex, k, RowIndex[k, 0]);
                        ColIndex = SwapRows(ColIndex, k, RowIndex[k, 0]);

                        k -= 1;
                    }
                    k += 1;
                }
            }
            else
            {
                Kc_exp = Kc;
            }

            

            return new StaticCondensationReturn()
            {
                Kc = Kc_exp,
                Fc = fc,
                DoF_Index = RowIndex
            };
        }

        public struct StaticCondensationReturn
        {
            public double[,] Kc;
            public double[,] Fc;
            public int[,] DoF_Index;
        }
    }
}
