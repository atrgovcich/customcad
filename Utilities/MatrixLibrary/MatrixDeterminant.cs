﻿using System;
using static System.Math;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double Determinant(double[,] A)
        {
            if (A.GetUpperBound(0) < 1)
            {
                //If there are less than 2 rows in the matrix
                throw new ArgumentException("The matrix must have at least 2 rows and 2 columns for the determinant to be calculated.");
            }

            double det = 0;

            if (A.GetUpperBound(0) > 1)
            {
                //If the matrix is greater than a 2x2
                int i = 0;
                for (i = 0; i <= A.GetUpperBound(1); i++)
                {
                    double[,] B = new double[A.GetUpperBound(0) - 1 + 1, A.GetUpperBound(1) - 1 + 1];
                    int brow = 0; //Set the row index of B
                    int bcol = 0; //Set the column index of B

                    //Loop through A starting at the second forw and find our sub-matrix
                    int j = 0;
                    int k = 0;
                    for (j = 1; j <= A.GetUpperBound(0); j++) //Loop through the rows of A, starting at the second row
                    {
                        for (k = 0; k <= A.GetUpperBound(1); k++) //Loop through the columns of A, starting at the first column
                        {
                            if (k != i)
                            {
                                //If the column is not the column we are crossing out, fill an index of B
                                B[brow, bcol] = A[j, k];
                                bcol += 1; //Increment the column index
                            }
                        }
                        bcol = 0; //Zero out the column index so that we start at 0 on the next row
                        brow += 1; //Increment the rown index by 1
                    }
                    det += Pow(-1, 1 + (i + 1)) * A[0, i] * Determinant(B);
                }
            }
            else
            {
                //The matrix is a 2x2
                det += A[0, 0] * A[1, 1] - A[0, 1] * A[1, 0];
            }

            return det;
        }
    }
}
