﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] SwapColumns(double[,] A, int col1, int col2)
        {
            double[,] B = new double[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];
            Array.Copy(A, B, A.Length);

            double[,] c1 = new double[B.GetUpperBound(0) + 1, 0 + 1];

            int i = 0;
            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                c1[i, 0] = B[i, col1];
            }

            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                B[i, col1] = B[i, col2];
            }

            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                B[i, col2] = c1[i, 0];
            }

            return B;
        }

        public static int[,] SwapColumns(int[,] A, int col1, int col2)
        {
            int[,] B = new int[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];
            Array.Copy(A, B, A.Length);

            int[,] c1 = new int[B.GetUpperBound(0) + 1, 0 + 1];

            int i = 0;
            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                c1[i, 0] = B[i, col1];
            }

            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                B[i, col1] = B[i, col2];
            }

            for (i = 0; i <= B.GetUpperBound(0); i++)
            {
                B[i, col2] = c1[i, 0];
            }

            return B;
        }
    }
}
