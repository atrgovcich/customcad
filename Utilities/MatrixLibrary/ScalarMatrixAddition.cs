﻿namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] ScalarMatrixAddition(double A, double[,] B)
        {
            double[,] C = new double[B.GetUpperBound(0) + 1, B.GetUpperBound(1) + 1];

            for (int i = 0; i <= B.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= B.GetUpperBound(1); j++)
                {
                    C[i, j] = B[i, j] + A;
                }
            }

            return C;
        }
    }
}
