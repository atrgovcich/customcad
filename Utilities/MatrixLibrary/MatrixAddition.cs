﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] MatrixAdd(double[,] A, double[,] B)
        {
            int n = A.GetUpperBound(0);
            int m = A.GetUpperBound(1);
            int p = B.GetUpperBound(0);
            int q = B.GetUpperBound(1);

            if (n != p)
            {
                throw new Exception("Matrix row counts do not match.");
            }

            if (m != q)
            {
                throw new Exception("Matrix column counts do not match.");
            }

            double[,] C = new double[n + 1, m + 1];

            int i = 0;
            int j = 0;
            for (i = 0; i <= n; i++)
            {
                for (j = 0; j <= m; j++)
                {
                    C[i, j] = A[i, j] + B[i, j];
                }
            }

            return C;
        }
    }
}
