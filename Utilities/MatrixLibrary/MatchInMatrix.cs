﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static object Match(object[,] A, object LookUpVal, int LookUpCol, int ReturnCol, int LoopIndex)
        {
            //Returns the value in the specified Return Column associated with the Look Up Value found in the Look Up Column
            object RetVal = null;
            if (LoopIndex == 0)
            {
                int i = 0;
                for (i = 0; i <= A.GetUpperBound(0); i++)
                {
                    if (String.Compare(Convert.ToString(LookUpVal), Convert.ToString(A[i, LookUpCol]), ignoreCase: true) == 0)
                    {
                        RetVal = A[i, ReturnCol];
                        break;
                    }
                }
            }
            else if (LoopIndex == 1)
            {
                int i = 0;
                for (i = 0; i <= A.GetUpperBound(1); i++)
                {
                    if (String.Compare(Convert.ToString(LookUpVal), Convert.ToString(A[LookUpCol, i]), ignoreCase: true) == 0)
                    {
                        RetVal = A[ReturnCol, i];
                        break;
                    }
                }
            }
            else
            {
                throw new ArgumentException("The LoopIndex must be either 0 (loops through rows) or 1 (loops through columns).");
            }
            return RetVal;
        }
    }
}
