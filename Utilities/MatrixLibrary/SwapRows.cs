﻿using System;

namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] SwapRows(double[,] A, int row1, int row2)
        {
            double[,] B = new double[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];
            Array.Copy(A, B, A.Length);

            double[,] r1 = new double[0 + 1, B.GetUpperBound(1) + 1];

            int i = 0;
            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                r1[0, i] = B[row1, i];
            }

            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                B[row1, i] = B[row2, i];
            }

            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                B[row2, i] = r1[0, i];
            }

            return B;
        }

        public static int[,] SwapRows(int[,] A, int row1, int row2)
        {
            int[,] B = new int[A.GetUpperBound(0) + 1, A.GetUpperBound(1) + 1];
            Array.Copy(A, B, A.Length);

            int[,] r1 = new int[0 + 1, B.GetUpperBound(1) + 1];

            int i = 0;
            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                r1[0, i] = B[row1, i];
            }

            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                B[row1, i] = B[row2, i];
            }

            for (i = 0; i <= B.GetUpperBound(1); i++)
            {
                B[row2, i] = r1[0, i];
            }

            return B;
        }
    }
}
