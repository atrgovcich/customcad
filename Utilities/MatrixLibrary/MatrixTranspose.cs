﻿namespace Utilities.MatrixLibrary
{
    public static partial class MatrixLibrary
    {
        public static double[,] Transpose(double[,] A)
        {
            int m = A.GetUpperBound(0);
            int n = A.GetUpperBound(1);

            double[,] T = new double[n + 1, m + 1];

            int i = 0;
            int j = 0;

            for (i = 0; i <= m; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    T[j, i] = A[i, j];
                }
            }

            return T;
        }

        public static int[,] Transpose(int[,] A)
        {
            int m = A.GetUpperBound(0);
            int n = A.GetUpperBound(1);

            int[,] T = new int[n + 1, m + 1];

            int i = 0;
            int j = 0;

            for (i = 0; i <= m; i++)
            {
                for (j = 0; j <= n; j++)
                {
                    T[j, i] = A[i, j];
                }
            }

            return T;
        }
    }
}
