﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;

namespace Utilities.Geometry
{
    public class Edge
    {
        public PointD StartPoint { get; set; }
        public PointD EndPoint { get; set; }

        #region Constructors
        public Edge(PointD startPoint, PointD endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the length of the line segment
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            return Sqrt(Pow(EndPoint.X - StartPoint.X, 2) + Pow(EndPoint.Y - StartPoint.Y, 2));
        }

        /// <summary>
        /// Reverses the direction of the line segment
        /// </summary>
        public void Reverse()
        {
            PointD temp = EndPoint;
            EndPoint = StartPoint;
            StartPoint = temp;
        }

        /// <summary>
        /// Returns the mid-point of the edge
        /// </summary>
        /// <returns></returns>
        public PointD Midpoint()
        {
            return new PointD((StartPoint.X + EndPoint.X) / 2.0, (StartPoint.Y + EndPoint.Y) / 2.0);
        }

        /// <summary>
        /// Returns which side of the edge a point falls on
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public SideOfLine WhichSide(PointD p)
        {
            PointD p0 = StartPoint;
            PointD p1 = EndPoint;

            double val = (p1.X - p0.X) * (p.Y - p0.Y) - (p.X - p0.X) * (p1.Y - p0.Y);

            if (val > 0)
            {
                return SideOfLine.Left;
            }
            else if (val < 0)
            {
                return SideOfLine.Right;
            }
            else
            {
                return SideOfLine.Coincident;
            }
        }

        /// <summary>
        /// Returns the perpendicular distance between the edge and the give point
        /// </summary>
        /// <param name="p">Point to compute perpendicular distance to</param>
        /// <returns></returns>
        public double PerpendicularDistance(PointD p)
        {
            if (StartPoint.X == EndPoint.X)
            {
                //Vertical line
                return Abs(p.X - StartPoint.X);
            }
            else if (StartPoint.Y == EndPoint.Y)
            {
                //Horizontal line
                return Abs(p.Y - StartPoint.Y);
            }
            else
            {
                double m1 = (EndPoint.Y - StartPoint.Y) / (EndPoint.X - StartPoint.X);

                double b1 = StartPoint.Y - m1 * StartPoint.X;

                double m2 = -1.0 / m1;

                double b2 = p.Y - m2 * p.X;

                double xInt = (b2 - b1) / (m1 - m2);

                double yInt = m1 * xInt + b1;

                PointD intercept = new PointD(xInt, yInt);

                return p.DistanceTo(intercept);
            }
        }

        /// <summary>
        /// Creates a new edge parallel to the current edge, whose start point is the given point
        /// </summary>
        /// <param name="p">Start point of the new edge</param>
        /// <returns></returns>
        public Edge CreateParallelEdgeAtPoint(PointD p)
        {
            if (StartPoint.X == EndPoint.X)
            {
                //Vertical line
                PointD newStartPoint = new PointD(p.X, StartPoint.Y);
                PointD newEndPoint = new PointD(p.X, EndPoint.Y);

                return new Edge(newStartPoint, newEndPoint);
            }
            else if (StartPoint.Y == EndPoint.Y)
            {
                //Horizontal line
                PointD newStartPoint = new PointD(StartPoint.X, p.Y);
                PointD newEndPoint = new PointD(EndPoint.X, p.Y);

                return new Edge(newStartPoint, newEndPoint);
            }
            else
            {
                double m = (EndPoint.Y - StartPoint.Y) / (EndPoint.X - StartPoint.X);

                double xDist = (EndPoint.X - StartPoint.X);

                PointD newEndPoint = new PointD(p.X + xDist, p.Y + xDist * m);

                return new Edge(p, newEndPoint);
            }
        }
        #endregion
    }
}
