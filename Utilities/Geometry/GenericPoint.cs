﻿using System;
using static System.Math;
using Utilities.DataStructures.QuadTree;

namespace Utilities.Geometry
{
    public class GenericPoint : GenericShape
    {
        #region Public Properties
        public PointD Point { get; set; }
        #endregion

        #region Constructors
        public GenericPoint(double x, double y)
        {
            Point = new PointD(x, y);
        }

        public GenericPoint(PointD p)
        {
            Point = p;
        }
        #endregion

        #region Public Methods
        public override GenericShape GetDiscretizedPoly(double dist)
        {
            return this;
        }

        public override Rect BoundingRectangle()
        {
            double x = Point.X;
            double y = Point.Y;

            int x0 = Convert.ToInt32(Floor(x - 0.25));
            int y0 = Convert.ToInt32(Floor(y - 0.25));

            int width = Convert.ToInt32(Ceiling((x + 0.25) - x0));
            int height = Convert.ToInt32(Ceiling((y + 0.25) - y0));

            return new Rect(x0, y0, width, height);
        }
        #endregion
    }
}
