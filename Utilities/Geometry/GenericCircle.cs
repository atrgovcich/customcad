﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using Utilities.DataStructures.QuadTree;
using Utilities.Extensions.DotNetNative;
using Newtonsoft.Json;

namespace Utilities.Geometry
{
    public class GenericCircle : GenericClosedShape
    {
        #region Public Properties
        /// <summary>
        /// Centroid of the circle
        /// </summary>
        public PointD Center { get; set; }
        /// <summary>
        /// Radius of the circle
        /// </summary>
        public double Radius { get; set; }
        /// <summary>
        /// Diameter of the circle
        /// </summary>
        [JsonIgnore]
        public double Diameter
        {
            get
            {
                return 2.0 * Radius;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the generic circle class
        /// </summary>
        /// <param name="center">Centroid of the circle</param>
        /// <param name="radius">Radius of the circle</param>
        public GenericCircle(PointD center, double radius) : base()
        {
            Center = center;
            Radius = radius;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the area of the circle
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return PI * (Radius * Radius);
        }

        /// <summary>
        /// Returns the centroid of the circle
        /// </summary>
        /// <returns></returns>
        public override PointD Centroid()
        {
            return Center;
        }

        /// <summary>
        /// Returns the circumference of the circle
        /// </summary>
        /// <returns></returns>
        public double Circumference()
        {
            return 2.0 * PI * Radius;
        }

        /// <summary>
        /// Gets the point at the specified angle from horizontal.
        /// </summary>
        /// <param name="theta">Angle to retrieve point, in radians, measured from positive X-axis</param>
        /// <returns></returns>
        public PointD GetPointAtAngle(double theta)
        {
            double r = Radius;

            PointD cP = Center;

            PointD p = new PointD(cP.X + r * Cos(theta), cP.Y + r * Sin(theta));

            return p;
        }

        /// <summary>
        /// Checks whether a point lies within the circle
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public bool PointLiesWithin(PointD p)
        {
            PointD cP = Center;

            if (cP.DistanceTo(p) <= Radius)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the moment of inertia about the X-axis
        /// </summary>
        /// <returns></returns>
        public override double Ixx()
        {
            return (PI / 4.0) * Pow(Radius, 4);
        }

        /// <summary>
        /// Returns the moment of inertia about the Y-axis
        /// </summary>
        /// <returns></returns>
        public override double Iyy()
        {
            return (PI / 4.0) * Pow(Radius, 4);
        }

        /// <summary>
        /// Returns the product of inertia
        /// </summary>
        /// <returns></returns>
        public override double Ixy()
        {
            return 0;
        }

        /// <summary>
        /// Returns the section modulus about the X-axis, assuming an elastic section
        /// </summary>
        /// <returns></returns>
        public override double Sxx()
        {
            return (PI / 4.0) * Pow(Radius, 3);
        }

        /// <summary>
        /// Returns the section modulus about the Y-axis, assuming an elastic section
        /// </summary>
        /// <returns></returns>
        public override double Syy()
        {
            return (PI / 4.0) * Pow(Radius, 3);
        }

        /// <summary>
        /// Returns the circumference of the circle
        /// </summary>
        /// <returns></returns>
        public override double Perimeter()
        {
            return Circumference();
        }

        /// <summary>
        /// Checks whether a point lies within the circle
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public override bool ContainsPoint(PointD p)
        {
            PointD cP = Center;

            if (cP.DistanceTo(p) <= Radius)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Rect BoundingRectangle()
        {
            PointD cp = Center;

            int x0 = Convert.ToInt32(Floor(cp.X - Radius));
            int y0 = Convert.ToInt32(Floor(cp.Y - Radius));

            int width = Convert.ToInt32(Ceiling(cp.X + Radius - x0));
            int height = width;

            return new Rect(x0, y0, width, height);
        }

        /// <summary>
        /// Determines whether this circle lies fully within another closed shape
        /// </summary>
        /// <param name="other">Other closed shape</param>
        /// <returns></returns>
        public override bool IsFullyWithin(IGenericClosedShape other)
        {
            PointD cP = Center;

            bool inside = true;

            if (other.GetType().Equals(typeof(GenericPolygon)) == true)
            {
                GenericPolygon poly = (GenericPolygon)other;

                List<PointD> otherNodes = poly.Points;

                for (int i = 0; i < otherNodes.Count; i++)
                {
                    if (otherNodes[i].DistanceTo(cP) < Radius)
                    {
                        inside = false;

                        break;
                    }
                }

                //Now check if any edges intersect the circle
                if (inside == true)
                {
                    List<Edge> edges = poly.Edges();

                    for (int i = 0; i < edges.Count; i++)
                    {
                        if (this.IntersectsWithLine(edges[i], out List<PointD> intersections) == true)
                        {
                            if (edges[i].StartPoint.DistanceTo(cP) != Radius || edges[i].EndPoint.DistanceTo(cP) != Radius)
                            {
                                inside = false;

                                break;
                            }
                        }
                    }
                }

            }
            else if (other.GetType().Equals(typeof(GenericCircle)) == true || other.GetType().IsSubclassOf(typeof(GenericCircle)) == true)
            {
                GenericCircle circle = (GenericCircle)other;

                List<PointD> otherNodes = new List<PointD>(5);

                otherNodes.AddRange(circle.QuadrantPoints());

                otherNodes.Add(circle.Center);

                for (int i = 0; i < otherNodes.Count; i++)
                {
                    if (otherNodes[i].DistanceTo(cP) - Radius < -(1.0 * Pow(10, -_precision)))
                    {
                        inside = false;

                        break;
                    }
                }
            }

            return inside;
        }

        /// <summary>
        /// Returns a list of the four quadrant points
        /// </summary>
        /// <returns></returns>
        public List<PointD> QuadrantPoints()
        {
            List<PointD> pointList = new List<PointD>(4);

            PointD centerPoint = Center;

            pointList.Add(new PointD(centerPoint.X - Radius, centerPoint.Y));
            pointList.Add(new PointD(centerPoint.X, centerPoint.Y + Radius));
            pointList.Add(new PointD(centerPoint.X + Radius, centerPoint.Y));
            pointList.Add(new PointD(centerPoint.X, centerPoint.Y - Radius));

            return pointList;
        }

        public bool IntersectsWithLine(Edge line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (IntersectsWithInfiniteLine(line, out List<PointD> infIntersections) == true)
            {
                PointD p0 = line.StartPoint;
                PointD p1 = line.EndPoint;

                for (int i = 0; i < infIntersections.Count; i++)
                {
                    PointD p = infIntersections[i];

                    if (Round(p1.X - p0.X, _precision) == 0)
                    {
                        //vertical line
                        if (p.Y >= Min(p0.Y, p1.Y) && p.Y <= Max(p0.Y, p1.Y))
                        {
                            intersections.Add(p);
                        }
                    }
                    else
                    {
                        //horizontal line or line with slope
                        if (p.X >= Min(p0.X, p1.X) && p.X <= Max(p0.X, p1.X))
                        {
                            intersections.Add(p);
                        }
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the ciricle intersects with an infinite line
        /// </summary>
        /// <param name="line">Line segment that defines the infinite line</param>
        /// <param name="intersections">Out parameter: List of intersections</param>
        /// <returns></returns>
        public bool IntersectsWithInfiniteLine(Edge line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            double r = Radius;

            PointD cN = Center;

            PointD cP = cN;

            PointD p0 = line.StartPoint;

            PointD p1 = line.EndPoint;

            if (Round(p1.X - p0.X, _precision) == 0)
            {
                //line is vertical
                if (p0.X < cP.X - r || p0.X > cP.X + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double xInt = p0.X;

                    double yInt1 = Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    double yInt2 = -Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    intersections.Add(new PointD(xInt, yInt1));

                    intersections.Add(new PointD(xInt, yInt2));

                    return true;
                }
            }
            else if (Round(p1.Y - p0.Y, _precision) == 0)
            {
                //line is horizontal
                if (p0.Y < cP.Y - r || p0.Y > cP.Y + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double yInt = p0.Y;

                    double xInt1 = Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    double xInt2 = -Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    intersections.Add(new PointD(xInt1, yInt));

                    intersections.Add(new PointD(xInt2, yInt));

                    return true;
                }
            }
            else
            {
                double ms = (p1.Y - p0.Y) / (p1.X - p0.X);

                double bs = p0.Y - ms * p0.X;

                double a = cP.X;

                double b = cP.Y;

                double a1 = (-1.0 - Pow(ms, 2));

                double b1 = (2.0 * a - 2.0 * ms * bs + 2.0 * ms * b);

                double c1 = (Pow(r, 2) + 2.0 * bs * b - Pow(a, 2) - Pow(bs, 2) - Pow(b, 2));

                double xInt1 = (-b1 + Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double xInt2 = (-b1 - Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double yInt1 = ms * xInt1 + bs;

                double yInt2 = ms * xInt2 + bs;

                if (xInt1.IsNanOrInfinity() == false && yInt1.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt1, yInt1));
                }

                if (xInt2.IsNanOrInfinity() == false && yInt2.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt2, yInt2));
                }

                if (intersections.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets a polygon version of this shape, discretized into line segments based on the input distance
        /// </summary>
        /// <param name="dist">Input distance to discretize shape</param>
        /// <returns></returns>
        public override GenericShape GetDiscretizedPoly(double dist)
        {
            List<PointD> nodeList = new List<PointD>();

            double theta_1 = 2.0 * Asin((0.5 * dist) / Radius);

            if (theta_1.IsNanOrInfinity() == true)
            {
                double preset = 30.0; //degrees, preset if the dist is too large

                theta_1 = preset.ToDegrees();
            }

            int minAnglePoints = 8;

            int numAnglePoints = Max(minAnglePoints, Convert.ToInt32(Ceiling(2.0 * PI / theta_1)));

            double thetaInterval = 2.0 * PI / numAnglePoints;

            for (int i = 0; i < numAnglePoints; i++)
            {
                double theta = -0.5 * thetaInterval + i * thetaInterval;

                PointD edgePoint = GetPointAtAngle(theta);

                nodeList.Add(edgePoint);
            }

            nodeList.Add(nodeList[0]);

            GenericPolygon pg = new GenericPolygon(nodeList);
            pg.Hole = this.Hole;
            
            return pg;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Gets the Y-coordinates (1 or 2) given the specified X-coordinate
        /// </summary>
        /// <param name="x">X-coordinate to retrieve Y-coordinates at</param>
        /// <returns></returns>
        protected Tuple<double, double> GetYCoordinate(double x)
        {
            PointD cP = Center;

            if (x < cP.X - Radius || x > cP.X + Radius)
            {
                return null;
            }
            else
            {
                double y1 = cP.Y + Sqrt(Radius * Radius - (x - cP.X) * (x - cP.X));

                double y2 = cP.Y - Sqrt(Radius * Radius - (x - cP.X) * (x - cP.X));

                return new Tuple<double, double>(y1, y2);
            }
        }

        /// <summary>
        /// Gets the value of the derivative (slope) at the given x and y coordinates
        /// </summary>
        /// <param name="x">X-coordinate</param>
        /// <param name="y">Y-coordinate</param>
        /// <returns></returns>
        protected double GetValueOfDerivateAt(double x, double y)
        {
            PointD cP = Center;

            if (y == cP.Y)
            {
                //Vertical line
                return double.PositiveInfinity;
            }
            else
            {
                return -1.0 * ((x - cP.X) / (y - cP.Y));
            }
        }
        #endregion
    }
}
