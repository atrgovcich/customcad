﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;

namespace Utilities.Geometry
{
    public class Vector
    {
        #region Public Properties
        /// <summary>
        /// X component of vector
        /// </summary>
        public double X { get; set; }
        /// <summary>
        /// Y component of vector
        /// </summary>
        public double Y { get; set; }

        public double Magintude
        {
            get
            {
                return Length();
            }
        }
        #endregion

        #region Constructors
        public Vector(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns the length of the vector
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            if (X == 0 && Y == 0)
            {
                return 0;
            }
            else
            {
                return Sqrt(Pow(X, 2) + Pow(Y, 2));
            }
        }

        public Vector UnitVector()
        {
            return new Vector(X / Magintude, Y / Magintude);
        }

        /// <summary>
        /// Returns a new vector perpendicular to the given vector
        /// </summary>
        /// <returns></returns>
        public Vector PerpendicularVector()
        {
            return new Vector(Y, -X);
        }

        public Vector Reverse()
        {
            return new Vector(-X, -Y);
        }
        #endregion
    }
}
