﻿namespace Utilities.Geometry
{
    public struct CircleDefinition
    {
        public PointD Centroid { get; set; }
        public double Radius { get; set; }

        public CircleDefinition(PointD centroid, double radius)
        {
            Centroid = centroid;
            Radius = radius;
        }
    }
}
