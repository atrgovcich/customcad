﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;

namespace Utilities.Geometry
{
    public static class GeometryFunctions
    {
        /// <summary>
        /// Checks if a point is inside of a polygon.  Returns 0 only when 'p' is outside of the polygon.
        /// Otherwise, returns != 0.
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <param name="polygon">Polygon in question</param>
        /// <returns></returns>
        public static int WindingNumberInside(PointD p, GenericPolygon polygon)
        {
            //V = vertex
            //Vertices is list of polygon vertices
            //Returns winding number, which is == 0 only when V is outside of the polygon
            //Credit for this code goes to: http://geomalgorithms.com/a03-_inclusion.html
            List<PointD> Vertices = polygon.Points;

            int wn = 0;

            int lastIndex = Vertices.Count;

            if (Vertices[0].X == Vertices[Vertices.Count - 1].X && Vertices[0].Y == Vertices[Vertices.Count - 1].Y)
            {
                lastIndex = Vertices.Count - 1;
            }

            //Loop through all edges of the polygon
            for (int i = 0; i < lastIndex; i++)
            {
                PointD nextPoint;

                if (i == lastIndex - 1)
                {
                    if (lastIndex == Vertices.Count)
                    {
                        nextPoint = Vertices[0];
                    }
                    else
                    {
                        nextPoint = Vertices[i + 1];
                    }
                }
                else
                {
                    nextPoint = Vertices[i + 1];
                }

                if (Vertices[i].Y <= p.Y) //If the start point 'y' value is less than the point's 'y' value
                {
                    if (nextPoint.Y > p.Y) //An upward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, p) > 0) //V is left of the dge
                        {
                            wn += 1;
                        }
                    }
                }
                else
                {
                    if (nextPoint.Y <= p.Y) //A downward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, p) < 0) //V is right of the edge
                        {
                            wn -= 1;
                        }
                    }
                }
            }

            return wn;
        }

        /// <summary>
        /// Checks which side of a line the point lies on.
        /// Returns less than 0 if p is left of the line passing through p0 & p1
        /// Returns 0 if p is on the line passing through p0 & p1
        /// Returns greater than 0 if p is right of the line passing through p0 & p1
        /// </summary>
        /// <param name="p0">Line start point</param>
        /// <param name="p1">Line end point</param>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public static double IsLeft(PointD p0, PointD p1, PointD p)
        {
            double val = (p1.X - p0.X) * (p.Y - p0.Y) - (p.X - p0.X) * (p1.Y - p0.Y);

            return val;
        }

        /// <summary>
        /// Returns the definition of a circle from three points
        /// </summary>
        /// <param name="p1">First point</param>
        /// <param name="p2">Second point</param>
        /// <param name="p3">Third point</param>
        /// <returns></returns>
        public static CircleDefinition GetThreePointCircleEquation(PointD p1, PointD p2, PointD p3)
        {
            double x1, y1, x2, y2, x3, y3, cx, cy, r;

            x1 = p1.X;
            y1 = p1.Y;

            x2 = p2.X;
            y2 = p2.Y;

            x3 = p3.X;
            y3 = p3.Y;

            cx = ((Pow(x1, 2) + Pow(y1, 2)) * (y2 - y3) + (Pow(x2, 2) + Pow(y2, 2)) * (y3 - y1) + (Pow(x3, 2) + Pow(y3, 2)) * (y1 - y2)) / (2 * (x1 * (y2 - y3) - y1 * (x2 - x3) + x2 * y3 - x3 * y2));
            cy = ((Pow(x1, 2) + Pow(y1, 2)) * (x3 - x2) + (Pow(x2, 2) + Pow(y2, 2)) * (x1 - x3) + (Pow(x3, 2) + Pow(y3, 2)) * (x2 - x1)) / (2 * (x1 * (y2 - y3) - y1 * (x2 - x3) + x2 * y3 - x3 * y2));
            r = Sqrt(Pow((cx - x1), 2) + Pow((cy - y1), 2));

            PointD centroid = new PointD(cx, cy);
            return new CircleDefinition(centroid, r);
        }
    }
}
