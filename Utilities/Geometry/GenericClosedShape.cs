﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.DataStructures.QuadTree;

namespace Utilities.Geometry
{
    public abstract class GenericClosedShape : GenericShape, IGenericClosedShape
    {
        #region Public Properties
        public bool Hole { get; set; } = false;
        #endregion

        #region Constructors
        public GenericClosedShape()
        {

        }
        #endregion

        #region Public Abstract Methods
        public abstract double Area();

        public abstract PointD Centroid();

        public abstract double Perimeter();
        public abstract double Ixx();
        public abstract double Iyy();
        public abstract double Ixy();
        public abstract double Sxx();
        public abstract double Syy();
        public abstract bool IsFullyWithin(IGenericClosedShape other);
        public abstract bool ContainsPoint(PointD p);

        
        #endregion
    }
}
