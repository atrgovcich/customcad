﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using Utilities.DataStructures.QuadTree;
using PolyBoolCS;

using PointPB = PolyBoolCS.Point;

namespace Utilities.Geometry
{
    public partial class PointD
    {
        /// <summary>
        /// X-coordinate of the point
        /// </summary>
        public double X { get; set; }
        /// <summary>
        /// Y-coordinate of the point
        /// </summary>
        public double Y { get; set; }

        private const int ROUND = 4;
        public const double COINCIDENCE_TOLERANCE = 1.0E-6;
        public const int PRECISION = 6;

        #region Constructors
        /// <summary>
        /// Creates a new instance of the point (double) class
        /// </summary>
        /// <param name="x">X-coordinate</param>
        /// <param name="y">Y-coordinate</param>
        public PointD(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Computes the distance to another point
        /// </summary>
        /// <param name="other">Point to calculate the distance to</param>
        /// <returns></returns>
        public double DistanceTo(PointD other)
        {
            return Sqrt(Pow(other.X - X, 2) + Pow(other.Y - Y, 2));
        }

        /// <summary>
        /// Computes the angle between this point and another point.  Returns angle in radians.
        /// </summary>
        /// <param name="other">Other PointD</param>
        /// <returns></returns>
        public double GetAngleFromPoint(PointD other)
        {
            double angle = 0;

            if (Round(X - other.X, ROUND) == 0)
            {
                if (Y > other.Y)
                {
                    angle = PI / 2.0;
                }
                else if (Y < other.Y)
                {
                    angle = 3.0 * PI / 2.0;
                }
            }
            else if (Round(Y - other.Y, ROUND) == 0)
            {
                if (X > other.X)
                {
                    angle = 0;
                }
                else if (X < other.X)
                {
                    angle = PI;
                }
            }
            else
            {
                double tempangle = Atan((Y - other.Y) / (X - other.X));

                if (tempangle >= 0)
                {
                    if (Y >= other.Y)
                    {
                        //quadrant 1
                        angle = tempangle;
                    }
                    else
                    {
                        //quadrant 3
                        angle = PI + tempangle;
                    }
                }
                else
                {
                    if (Y >= other.Y)
                    {
                        //quadrant 2
                        angle = PI + tempangle;
                    }
                    else
                    {
                        //quadrant 4
                        angle = 2d * PI + tempangle;
                    }
                }
            }

            return angle;
        }

        /// <summary>
        /// Returns the angle, in radians, to the next point
        /// </summary>
        /// <param name="nextPoint">Point that forms line from this point</param>
        /// <returns></returns>
        public double GetAngleTo(PointD nextPoint)
        {
            double angle = 0;

            if (nextPoint.Y > this.Y && nextPoint.X > this.X)
            {
                //Quadrant 1
                angle = Atan((nextPoint.Y - this.Y) / (nextPoint.X - this.X));
            }
            else if (nextPoint.Y > this.Y && nextPoint.X < this.X)
            {
                //Quadrant 2
                angle = PI - Abs(Atan((nextPoint.Y - this.Y) / (nextPoint.X - this.X)));
            }
            else if (nextPoint.Y < this.Y && nextPoint.X < this.X)
            {
                //Quadrant 3
                angle = Abs(Atan((nextPoint.Y - this.Y) / (nextPoint.X - this.X))) + PI;
            }
            else if (nextPoint.Y < this.Y && nextPoint.X > this.X)
            {
                //Quadrant 4
                angle = 2d * PI + Atan((nextPoint.Y - this.Y) / (nextPoint.X - this.X));
            }
            else if (nextPoint.Y == this.Y && nextPoint.X > this.X)
            {
                angle = 0d;
            }
            else if (nextPoint.Y == this.Y && nextPoint.X < this.X)
            {
                angle = PI;
            }
            else if (nextPoint.Y > this.Y && nextPoint.X == this.X)
            {
                angle = PI / 2d;
            }
            else if (nextPoint.Y < this.Y && nextPoint.X == this.X)
            {
                angle = 3d * PI / 2d;
            }

            return angle;
        }

        /// <summary>
        /// Checks if a point is at the same location as another point, within an orthogonal distance tolerance.  Returns true if points are within tolerance.
        /// </summary>
        /// <param name="other">Point to check against</param>
        /// <param name="orthogonalTolerance">Orthogonal distance tolerance</param>
        /// <returns></returns>
        public bool IsSameAsOtherPointWithinTolerance(PointD other, double orthogonalTolerance)
        {
            double d = DistanceTo(other);

            if (d <= orthogonalTolerance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if a point is at the same location as another point, within an orthogonal distance tolerance.  Returns true if points are within tolerance.
        /// </summary>
        /// <param name="other">Point to check against</param>
        /// <param name="orthogonalTolerance">Orthogonal distance tolerance</param>
        /// <param name="orthognalDistance">Out parameter that gives the distance to the other point</param>
        /// <returns></returns>
        public bool IsSameAsOtherPointWithinTolerance(PointD other, double orthogonalTolerance, out double orthognalDistance)
        {
            double d = DistanceTo(other);

            if (d <= orthogonalTolerance)
            {
                orthognalDistance = d;

                return true;
            }
            else
            {
                orthognalDistance = -1;

                return false;
            }
        }

        public Rect BoundingRectangle()
        {
            double x = this.X;
            double y = this.Y;

            int x0 = Convert.ToInt32(Floor(x - 0.25));
            int y0 = Convert.ToInt32(Floor(y - 0.25));

            int width = Convert.ToInt32(Ceiling((x + 0.25) - x0));
            int height = Convert.ToInt32(Ceiling((y + 0.25) - y0));

            return new Rect(x0, y0, width, height);
        }

        public PointPB ConvertToPolyBoolPoint()
        {
            //return new Point(point.X, point.Y);
            return new PointPB(Math.Round(this.X, PRECISION), Math.Round(this.Y, PRECISION));
        }
        #endregion

        #region Operators
        public static bool operator ==(PointD p1, PointD p2)
        {
            if (ReferenceEquals(p1, p2))
            {
                return true;
            }

            if (ReferenceEquals(p1, null))
            {
                return false;
            }

            if (ReferenceEquals(p2, null))
            {
                return false;
            }

            if (Abs(p1.X - p2.X) <= COINCIDENCE_TOLERANCE)
            {
                if (Abs(p1.Y - p2.Y) <= COINCIDENCE_TOLERANCE)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool operator !=(PointD p1, PointD p2)
        {
            if (p1 == p2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
