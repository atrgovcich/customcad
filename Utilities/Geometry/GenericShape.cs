﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.DataStructures.QuadTree;

namespace Utilities.Geometry
{
    public abstract class GenericShape : IGenericGeometry
    {
        protected int _precision = 4;

        public abstract Rect BoundingRectangle();

        public abstract GenericShape GetDiscretizedPoly(double dist);
    }
}
