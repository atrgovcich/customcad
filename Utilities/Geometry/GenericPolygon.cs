﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using static Utilities.Geometry.GeometryFunctions;
using Utilities.DataStructures.QuadTree;
using Utilities.Containers;
using PolyBoolCS;
using System.Linq;
using Newtonsoft.Json;

namespace Utilities.Geometry
{
    public class GenericPolygon : GenericClosedShape
    {
        #region Public Properties
        /// <summary>
        /// List of polygon points.  This is a closed shape (i.e. the last point is equal to the first point).
        /// </summary>
        public List<PointD> Points { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the gneric polygon.
        /// </summary>
        [JsonConstructor]
        public GenericPolygon() : base()
        {
            Points = new List<PointD>();
        }

        /// <summary>
        /// Creates a new instance of the generic polygon with a list of points.
        /// The generic polygon is a closed shape, meaning that the first point
        /// must be equal to the last point.  The constructor will close the shape
        /// if it is not already.
        /// </summary>
        /// <param name="points"></param>
        public GenericPolygon(List<PointD> points)
        {
            List<PointD> newList = new List<PointD>(points);

            if (newList != null)
            {
                if (newList.Count >= 3)
                {
                    if (newList[0].Equals(newList[newList.Count-1]) == false)
                    {
                        if (newList[0] == newList[newList.Count - 1])
                        {
                            newList[newList.Count - 1] = newList[0];   //The points are at the same location, but are not reference equals.
                                                                    //Set the last point equal to the first point
                        }
                        else
                        {
                            newList.Add(newList[0]); //Add the first point as the last point to close the shape
                        }
                    }
                }
            }

            Points = newList;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Retunrs the area of the polygon
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return Abs(SignedArea());
        }

        /// <summary>
        /// Returns a list of the edges that make up the element
        /// </summary>
        /// <returns></returns>
        public List<Edge> Edges()
        {
            List<Edge> segments = new List<Edge>();

            for (int i = 0; i < Points.Count - 1; i++)
            {
                Edge seg = new Edge(Points[i], Points[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        /// <summary>
        /// Rotates the polygon through the specified angle given a base point.
        /// </summary>
        /// <param name="basePoint">Base point for rotation</param>
        /// <param name="angle">Angle to rotate through, in radians</param>
        public void RotateThroughAngle(PointD basePoint, double angle)
        {
            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD node = Points[i];

                double distToBasePoint = node.DistanceTo(basePoint);

                if (distToBasePoint > 0)
                {
                    double oldAngle = node.GetAngleFromPoint(basePoint);

                    double newAngle = oldAngle + angle;

                    if (newAngle > 2.0 * PI)
                    {
                        newAngle -= 2.0 * PI;
                    }

                    double newX = basePoint.X + distToBasePoint * Cos(newAngle);
                    double newY = basePoint.Y + distToBasePoint * Sin(newAngle);

                    node.X = newX;
                    node.Y = newY;
                }
            }
        }

        /// <summary>
        /// Returns the centroid of the polygon
        /// </summary>
        /// <returns></returns>
        public override PointD Centroid()
        {
            double sumX = 0;
            double sumY = 0;

            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD p0 = Points[i];
                PointD p1 = Points[i + 1];

                sumX += (p0.X + p1.X) * (p0.X * p1.Y - p1.X * p0.Y);
                sumY += (p0.Y + p1.Y) * (p0.X * p1.Y - p1.X * p0.Y);
            }

            double area = SignedArea();

            double cx = 1.0 / (6.0 * area) * sumX;
            double cy = 1.0 / (6.0 * area) * sumY;

            return new PointD(cx, cy);
        }

        /// <summary>
        /// Returns the perimeter of the polygon
        /// </summary>
        /// <returns></returns>
        public override double Perimeter()
        {
            List<Edge> edges = Edges();

            double perimeter = 0;

            for (int i = 0; i < edges.Count; i++)
            {
                perimeter += edges[i].Length();
            }

            return perimeter;
        }

        /// <summary>
        /// Returns the moment of inertia about the X-axis
        /// </summary>
        /// <returns></returns>
        public override double Ixx()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD n0 = Points[i];
                PointD n1 = Points[i + 1];

                double x0 = n0.X;
                double y0 = n0.Y;

                double x1 = n1.X;
                double y1 = n1.Y;

                total += (x0 * y1 - x1 * y0) * (Pow(x0, 2) + x0 * x1 + Pow(x1, 2));
            }

            total /= 12.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            double d = centroid.X;

            total -= a * d * d;

            return Abs(total);
        }

        /// <summary>
        /// Returns the moment of inertia about the Y-axis
        /// </summary>
        /// <returns></returns>
        public override double Iyy()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD n0 = Points[i];
                PointD n1 = Points[i + 1];

                double x0 = n0.X;
                double y0 = n0.Y;

                double x1 = n1.X;
                double y1 = n1.Y;

                total += (x0 * y1 - x1 * y0) * (Pow(y0, 2) + y0 * y1 + Pow(y1, 2));
            }

            total /= 12.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            double d = centroid.Y;

            total -= a * d * d;

            return Abs(total);
        }

        /// <summary>
        /// Returns the product of inertia
        /// </summary>
        /// <returns></returns>
        public override double Ixy()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD n0 = Points[i];
                PointD n1 = Points[i + 1];

                double x0 = n0.X;
                double y0 = n0.Y;

                double x1 = n1.X;
                double y1 = n1.Y;

                total += (x0 * y1 - x1 * y0) * (x0 * y1 + 2.0 * x0 * y0 + 2.0 * x1 * y1 + x1 * y0);
            }

            total /= 24.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            total -= a * centroid.X * centroid.Y;

            return Abs(total);
        }

        /// <summary>
        /// Returns the section modulus about the X-axis, assuming an elastic section
        /// </summary>
        /// <returns></returns>
        public override double Sxx()
        {
            //Get maximum and minimum y coordinate
            double minY = 0;
            double maxY = 0;

            for (int i = 0; i < Points.Count; i++)
            {
                if (i == 0)
                {
                    minY = Points[i].Y;
                    maxY = Points[i].Y;
                }
                else
                {
                    minY = Min(minY, Points[i].Y);
                    maxY = Max(maxY, Points[i].Y);
                }
            }

            double ix = Ixx();
            PointD centroid = Centroid();

            return ix / Max(Abs(centroid.Y - minY), Abs(centroid.Y - maxY));
        }

        /// <summary>
        /// Returns the section modulus about the Y-axis, assuming an elastic section
        /// </summary>
        /// <returns></returns>
        public override double Syy()
        {
            //Get maximum and minimum x coordinate
            double minX = 0;
            double maxX = 0;

            for (int i = 0; i < Points.Count; i++)
            {
                if (i == 0)
                {
                    minX = Points[i].X;
                    maxX = Points[i].X;
                }
                else
                {
                    minX = Min(minX, Points[i].X);
                    maxX = Max(maxX, Points[i].X);
                }
            }

            double iy = Iyy();
            PointD centroid = Centroid();

            return iy / Max(Abs(centroid.X - minX), Abs(centroid.X - maxX));
        }

        /// <summary>
        /// Returns the signed area of the polygon
        /// </summary>
        /// <returns></returns>
        public double SignedArea()
        {
            double sum = 0;

            for (int i = 0; i < Points.Count - 1; i++)
            {
                PointD p0 = Points[i];
                PointD p1 = Points[i + 1];
                sum += p0.X * p1.Y - p1.X * p0.Y;
            }

            return sum / 2.0;
        }

        /// <summary>
        /// Determines whether the specified point lies within the boundary of the polygon.  Uses the winding number algorithm.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool PointLiesWithin(PointD p)
        {
            if (WindingNumberInside(p, this) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Returns the bounding rectangle for the element, for use in the quad tree
        /// </summary>
        /// <returns></returns>
        public override Rect BoundingRectangle()
        {
            double minX = 0, minY = 0, maxX = 0, maxY = 0;

            int count = 0;

            foreach (PointD node in Points)
            {
                if (count == 0)
                {
                    minX = node.X;
                    minY = node.Y;
                    maxX = node.X;
                    maxY = node.Y;
                }
                else
                {
                    if (node.X < minX)
                    {
                        minX = node.X;
                    }

                    if (node.Y < minY)
                    {
                        minY = node.Y;
                    }

                    if (node.X > maxX)
                    {
                        maxX = node.X;
                    }

                    if (node.Y > maxY)
                    {
                        maxY = node.Y;
                    }
                }

                count++;
            }

            int x0 = Convert.ToInt32(Floor(minX));
            int y0 = Convert.ToInt32(Floor(minY));

            int width = Convert.ToInt32(Ceiling(maxX - x0));
            int height = Convert.ToInt32(Ceiling(maxY - y0));

            return new Rect(x0, y0, width, height);
        }

        public MinMaxBounds GetMinMaxBounds()
        {
            List<PointD> points = Points;

            double maxX = 0;
            double minX = 0;
            double maxY = 0;
            double minY = 0;

            for (int i = 0; i < points.Count; i++)
            {
                PointD p = points[i];
                if (i == 0)
                {
                    maxX = p.X;
                    minX = p.X;
                    maxY = p.Y;
                    minY = p.Y;
                }
                else
                {
                    if (p.X < minX)
                    {
                        minX = p.X;
                    }
                    if (p.X > maxX)
                    {
                        maxX = p.X;
                    }
                    if (p.Y < minY)
                    {
                        minY = p.Y;
                    }
                    if (p.Y > maxY)
                    {
                        maxY = p.Y;
                    }
                }
            }

            return new MinMaxBounds() { MaxX = maxX, MinX = minX, MaxY = maxY, MinY = minY };
        }

        /// <summary>
        /// Determines whether the specified point lies within the boundary of the polygon.  Uses the winding number algorithm.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public override bool ContainsPoint(PointD p)
        {
            if (WindingNumberInside(p, this) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool IsFullyWithin(IGenericClosedShape other)
        {
            bool inside = true;

            if (other.GetType().Equals(typeof(GenericPolygon)) == true || other.GetType().IsSubclassOf(typeof(GenericPolygon)) == true)
            {
                GenericPolygon poly = (GenericPolygon)other;

                for (int i = 0; i < Points.Count; i++)
                {
                    if (GeometryFunctions.WindingNumberInside(Points[i], poly) == 0)
                    {
                        inside = false;

                        break;
                    }
                }
            }
            else if (other.GetType().Equals(typeof(GenericCircle)) == true || other.GetType().IsSubclassOf(typeof(GenericCircle)) == true)
            {
                GenericCircle circle = (GenericCircle)other;

                PointD cP = circle.Center;

                for (int i = 0; i < Points.Count; i++)
                {
                    if (Abs(Points[i].DistanceTo(cP) - circle.Radius) > 1.0 * Pow(10, -_precision))
                    {
                        inside = false;

                        break;
                    }
                }
            }

            return inside;
        }

        public override GenericShape GetDiscretizedPoly(double dist)
        {
            return this;
        }

        /// <summary>
        /// Converts the generic polygon to a PolyBool polygon
        /// </summary>
        /// <returns></returns>
        public Polygon ConvertToPolyBoolPolygon()
        {
            List<PointD> vertices = new List<PointD>(Points);

            vertices.RemoveAt(vertices.Count - 1); //The methods were written for polygons that are not closed

            var convertedVertices = PolyBool.PolyBoolHelperMethods.ConvertToPolyBoolPoints(vertices);

            return PolyBoolHelper.ConvertToPolygon(convertedVertices.ToList());
        }
        #endregion

        #region Operators
        public static bool operator ==(GenericPolygon pg1, GenericPolygon pg2)
        {
            if (pg1.Equals(pg2) == true)
            {
                return true;
            }

            if (object.ReferenceEquals(pg1, null))
            {
                return object.ReferenceEquals(pg2, null);
            }

            if (object.ReferenceEquals(pg2, null))
            {
                return object.ReferenceEquals(pg1, null);
            }

            bool same = true;

            if (pg1.Points.Count == pg2.Points.Count)
            {
                for (int i = 0; i < pg1.Points.Count; i++)
                {
                    if (pg1.Points[i] != pg2.Points[i])
                    {
                        same = false;
                        break;
                    }
                }
            }
            else
            {
                same = false;
            }

            if (same == false)
            {
                List<PointD> pg1_R = new List<PointD>(); //resampled
                List<PointD> pg2_R = new List<PointD>(); //resampled

                for (int i = 0; i < pg1.Points.Count; i++)
                {
                    pg1_R.Add(pg1.Points[i]);
                }

                for (int i = 0; i < pg2.Points.Count; i++)
                {
                    pg2_R.Add(pg2.Points[i]);
                }

                int count = 0;

                while (count <= pg1_R.Count - 3)
                {
                    PointD p1 = pg1_R[count];
                    PointD p2 = pg1_R[count + 1];
                    PointD p3 = pg1_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg1_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg1_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                count = 0;

                while (count <= pg2_R.Count - 3)
                {
                    PointD p1 = pg2_R[count];
                    PointD p2 = pg2_R[count + 1];
                    PointD p3 = pg2_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg2_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg2_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                //Now both polygons are resampled

                if (pg1_R.Count != pg2_R.Count)
                {
                    return false;
                }
                else
                {
                    int numSame = 0;

                    foreach (PointD p1 in pg1_R)
                    {
                        foreach (PointD p2 in pg2_R)
                        {
                            if (p1 == p2)
                            {
                                numSame++;
                                break;
                            }
                        }
                    }

                    if (numSame == pg1_R.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                return true;
            }
        }

        public static bool operator !=(GenericPolygon pg1, GenericPolygon pg2)
        {
            if (pg1 == pg2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}
