﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using Utilities.DataStructures.QuadTree;
using Newtonsoft.Json;

namespace Utilities.Geometry
{
    public class GenericPolyline : GenericShape
    {
        #region Public Properties
        /// <summary>
        /// List of polyline points
        /// </summary>
        public List<PointD> Points { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the gneric polyline.
        /// </summary>
        [JsonConstructor]
        public GenericPolyline() : base()
        {
            Points = new List<PointD>();
        }

        /// <summary>
        /// Creates a new instance of the generic polyline with a list of points.
        /// </summary>
        /// <param name="points">List of points</param>
        public GenericPolyline(List<PointD> points)
        {
            if (points != null)
            {
                if (points.Count >= 2)
                {
                    Points = new List<PointD>(points);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Returns a list of the edges that make up the element
        /// </summary>
        /// <returns></returns>
        public List<Edge> Edges()
        {
            List<Edge> segments = new List<Edge>();

            for (int i = 0; i < Points.Count - 1; i++)
            {
                Edge seg = new Edge(Points[i], Points[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        /// <summary>
        /// Rotates the polygon through the specified angle given a base point.
        /// </summary>
        /// <param name="basePoint">Base point for rotation</param>
        /// <param name="angle">Angle to rotate through, in radians</param>
        public void RotateThroughAngle(PointD basePoint, double angle)
        {
            for (int i = 0; i < Points.Count; i++)
            {
                PointD node = Points[i];

                double distToBasePoint = node.DistanceTo(basePoint);

                if (distToBasePoint > 0)
                {
                    double oldAngle = node.GetAngleFromPoint(basePoint);

                    double newAngle = oldAngle + angle;

                    if (newAngle > 2.0 * PI)
                    {
                        newAngle -= 2.0 * PI;
                    }

                    double newX = basePoint.X + distToBasePoint * Cos(newAngle);
                    double newY = basePoint.Y + distToBasePoint * Sin(newAngle);

                    node.X = newX;
                    node.Y = newY;
                }
            }
        }

        /// <summary>
        /// Returns the perimeter of the polygon
        /// </summary>
        /// <returns></returns>
        public double Perimeter()
        {
            List<Edge> edges = Edges();

            double perimeter = 0;

            for (int i = 0; i < edges.Count; i++)
            {
                perimeter += edges[i].Length();
            }

            return perimeter;
        }

        /// <summary>
        /// Gets the bounding rectangle of the polyline.  Used for inclusion in the quad tree data structure.
        /// </summary>
        /// <returns></returns>
        public override Rect BoundingRectangle()
        {
            double minX = 0, minY = 0, maxX = 0, maxY = 0;

            int count = 0;

            foreach (PointD node in Points)
            {
                if (count == 0)
                {
                    minX = node.X;
                    minY = node.Y;
                    maxX = node.X;
                    maxY = node.Y;
                }
                else
                {
                    if (node.X < minX)
                    {
                        minX = node.X;
                    }

                    if (node.Y < minY)
                    {
                        minY = node.Y;
                    }

                    if (node.X > maxX)
                    {
                        maxX = node.X;
                    }

                    if (node.Y > maxY)
                    {
                        maxY = node.Y;
                    }
                }

                count++;
            }

            int x0 = Convert.ToInt32(Floor(minX));
            int y0 = Convert.ToInt32(Floor(minY));

            int width = Convert.ToInt32(Ceiling(maxX - x0));
            int height = Convert.ToInt32(Ceiling(maxY - y0));

            return new Rect(x0, y0, width, height);
        }

        /// <summary>
        /// Returns a polyline version of this line (i.e itself)
        /// </summary>
        /// <param name="dist">Distance to discretize</param>
        /// <returns></returns>
        public override GenericShape GetDiscretizedPoly(double dist)
        {
            return this;
        }
        #endregion
    }
}
