﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.DataStructures.QuadTree;

namespace Utilities.Geometry
{
    public interface IGenericGeometry
    {
        Rect BoundingRectangle();

        GenericShape GetDiscretizedPoly(double dist);
    }
}
