﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Geometry
{
    public enum SideOfLine
    {
        Left,
        Right,
        Coincident
    }
}
