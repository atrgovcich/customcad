﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.DataStructures.QuadTree;

namespace Utilities.Geometry
{
    public interface IGenericClosedShape : IGenericGeometry
    {
        double Area();

        PointD Centroid();

        double Perimeter();
        double Ixx();
        double Iyy();
        double Ixy();
        double Sxx();
        double Syy();
        bool IsFullyWithin(IGenericClosedShape other);
        bool ContainsPoint(PointD p);
        bool Hole { get; set; }
    }
}
