﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace CalculationLogger.Codes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ConcreteCode
    {
        [Description("ACI 318-11")]
        ACI_318_11,
        [Description("ACI 318-14")]
        ACI_318_14
    }
}