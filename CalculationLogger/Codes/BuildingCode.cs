﻿namespace CalculationLogger.Codes
{
    public struct BuildingCode
    {
        public SteelCode Steel { get; set; }
        public ColdFormedSteelCode ColdFormedSteel { get; set; }
        public WoodCode Wood { get; set; }
        public ConcreteCode Concrete { get; set; }
        public MasonryCode Masonry { get; set; }
        public GoverningCode Governing { get; set; }
    }
}