﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace CalculationLogger.Codes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ColdFormedSteelCode
    {
        [Description("AISI S100-12")]
        AISI_S100_12,
        [Description("AISI S100-16")]
        AISI_S100_16
    }
}