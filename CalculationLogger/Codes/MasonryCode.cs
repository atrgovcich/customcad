﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace CalculationLogger.Codes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MasonryCode
    {
        [Description("ACI 530-11")]
        ACI_530_11,
        [Description("ACI 530-13")]
        ACI_530_13
    }
}