﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace CalculationLogger.Codes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SteelCode
    {
        [Description("AISC 360-10")]
        AISC_360_10,
        [Description("AISC 360-16")]
        AISC_360_16
    }
}