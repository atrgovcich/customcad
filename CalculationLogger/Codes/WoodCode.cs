﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace CalculationLogger.Codes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum WoodCode
    {
        [Description("NDS 2012")]
        AWC_NDS_2012,
        [Description("NDS 2015")]
        AWC_NDS_2015
    }
}