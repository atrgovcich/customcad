﻿using StructuralLoads.General;

namespace CalculationLogger.Logging
{
    public class DesignCalculation : RecordedCalculation
    {
        public DesignMethodType DesignMethod { get; set; }
        public LoadCombination LoadCombination { get; set; }

        public DesignCalculation(string header, CalculationType calcType, DesignMethodType designMethod,
            LoadCombination loadCombination)
            : base(header, calcType)
        {
            DesignMethod = designMethod;
            LoadCombination = loadCombination;
        }
    }
}