﻿using System.Collections.Generic;
using System.Text;
using CalculationLogger.Printing;

namespace CalculationLogger.Logging
{
    public class CalculationLog
    {
        public string UniqueID { get; set; }

        public List<RecordedCalculation> Calculations { get; set; } = new List<RecordedCalculation>();

        public CalculationLog(string id)
        {
            UniqueID = id;
        }

        public void AddCalculation(RecordedCalculation calculation)
        {
            Calculations.Add(calculation);
        }

        public void ClearCalculations()
        {
            Calculations.Clear();
        }


        public string PrintAllCalculations(MarkdownConverterType converter)
        {
            var SB = new StringBuilder();

            foreach (RecordedCalculation calc in Calculations)
            {
                SB.Append(calc.PrintAllResults(EquationDisplayParameters.DisplayAll, false, converter));
            }

            return SB.ToString();
        }

        public string PrintAllCalculations(CalculationType calcType, MarkdownConverterType converter)
        {
            var SB = new StringBuilder();

            foreach (RecordedCalculation calc in Calculations)
            {
                if (calc.Type == calcType)
                {
                    SB.Append(calc.PrintAllResults(EquationDisplayParameters.DisplayAll, false, converter));
                }
            }

            return SB.ToString();
        }
    }
}