﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CalculationLogger.Logging
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ResultLimitType
    {
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Override,
        MaximumOf,
        MinimumOf
    }
}