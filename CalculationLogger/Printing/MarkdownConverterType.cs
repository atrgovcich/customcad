﻿namespace CalculationLogger.Printing
{
    public enum MarkdownConverterType
    {
        Markdig,
        Pandoc
    }
}
