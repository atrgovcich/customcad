﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace CalculationLogger.Printing
{
    public static class Printer
    {
        public static string WriteCalculationMarkdownFile(string calcString, string markdownFileName, MarkdownConverterType markdownConverter)
        {
            string executingPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            string calculationOutputFolder = Path.Combine(executingPath, @"CalculationOutput");

            //string markdownFileName = "LateralCalculationOutput";

            string markdownFilePath = Path.Combine(executingPath, @"CalculationOutput\" + markdownFileName + ".md");

            File.WriteAllText(markdownFilePath, calcString);

            string outputHTML = null;

            string htmlFilePath = Path.Combine(executingPath, @"CalculationOutput\" + markdownFileName + ".html");

            if (markdownConverter.Equals(MarkdownConverterType.Pandoc) == true)
            {
                CallPandocConverter(calculationOutputFolder, markdownFilePath, markdownFileName);

                string htmlFileString = File.ReadAllText(htmlFilePath);

                outputHTML = InsertMathJaxConfiguration(htmlFileString);

                File.WriteAllText(htmlFilePath, outputHTML);
            }
            else if (markdownConverter.Equals(MarkdownConverterType.Markdig) == true)
            {
                CallMarkdigConverter(calculationOutputFolder, markdownFilePath, markdownFileName, executingPath);

                outputHTML = File.ReadAllText(htmlFilePath);
            }

            return outputHTML;
        }

        private static void CallMarkdigConverter(string outputFolder, string markdownFilePath, string markdownFileName, string executingPath)
        {
            var sB = new StringBuilder();

            string htmlHeader = File.ReadAllText(Path.Combine(executingPath, @"CalculationOutput\" + "HTML_Header.html"));

            sB.Append(htmlHeader);
            sB.Append(Environment.NewLine);

            string mdString = File.ReadAllText(markdownFilePath);

            var pipelineBuilder = new Markdig.MarkdownPipelineBuilder();
            pipelineBuilder = Markdig.MarkdownExtensions.UseAdvancedExtensions(pipelineBuilder);
            pipelineBuilder.Extensions.AddIfNotAlready(new Markdig.Extensions.Tables.PipeTableExtension(new Markdig.Extensions.Tables.PipeTableOptions()));
            Markdig.MarkdownPipeline pipeline = pipelineBuilder.Build();

            var result = Markdig.Markdown.ToHtml(mdString, pipeline);

            sB.Append(result);

            sB.Append(Environment.NewLine);
            sB.Append(@"</body>");
            sB.Append(Environment.NewLine);
            sB.Append(@"</html>");

            File.WriteAllText(outputFolder + @"\" + markdownFileName + ".html", sB.ToString());

        }

        private static string InsertMathJaxConfiguration(string htmlInput)
        {
            string startSearchString = "<script src=";
            int startIndex = htmlInput.IndexOf(startSearchString);

            string endSearchString = "</script>";
            int endIndex = htmlInput.IndexOf(endSearchString, startIndex);

            string configInsert = Environment.NewLine + "MathJax.Hub.Config({" + Environment.NewLine
                + "jax: [\"input/TeX\",\"output/HTML-CSS\"]," + Environment.NewLine
                + "displayAlign: \"left\"" + Environment.NewLine + "});";

            string htmlOutput = htmlInput.Insert(endIndex, configInsert);

            return htmlOutput;
        }

        private static void CallPandocConverter(string outputFolder, string markdownFilePath, string markdownFileName)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            string arguments = "/C CD " + outputFolder + "&" + "pandoc --mathjax --metadata pagetitle=\"CalculationPackage\" " + markdownFileName + ".md -s -o " + markdownFileName + ".html -c CalculationTemplate.css";
            startInfo.Arguments = arguments;
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
        }
    }
}
