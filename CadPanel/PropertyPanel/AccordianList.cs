﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CadPanel.UserControls;

namespace CadPanel.PropertyPanel
{
    public class AccordianList
    {
        public Point Location { get; set; } = new Point(5, 5);

        private int _gapBetweenPanels = 5;

        protected List<CollapsiblePanel> _collapsiblePanelList = new List<CollapsiblePanel>();

        #region Public Methods
        public void AddCollapsiblePanel(CollapsiblePanel p)
        {
            if (_collapsiblePanelList.Contains(p) == false)
            {
                p.Resize += new EventHandler(ChildPanels_Resize);

                _collapsiblePanelList.Add(p);
            }
        }

        public void RemoveCollapsiblePanel(CollapsiblePanel p)
        {
            if (_collapsiblePanelList.Contains(p) == true)
            {
                _collapsiblePanelList.Remove(p);
            }
        }

        public void ChildPanels_Resize(object sender, System.EventArgs e)
        {
            ArrangePanels();
        }

        public void AddAccordianListToControl(Control c)
        {
            //Remove the controls if they already exist on the form
            for (int i = 0; i < _collapsiblePanelList.Count; i++)
            {
                if (c.Controls.Contains(_collapsiblePanelList[i]) == true)
                {
                    c.Controls.Remove(_collapsiblePanelList[i]);
                }
            }

            //Add the controls
            for (int i = 0; i < _collapsiblePanelList.Count; i++)
            {
                c.Controls.Add(_collapsiblePanelList[i]);
            }

            ArrangePanels();
        }

        public void DisplayListControls()
        {
            ArrangePanels();
        }
        #endregion

        #region Protected Methods
        protected void ArrangePanels()
        {
            int currentY = Location.Y;

            for (int i = 0; i < _collapsiblePanelList.Count; i++)
            {
                CollapsiblePanel p = _collapsiblePanelList[i];

                if (p.ShowPanel == true)
                {
                    p.Location = new Point(Location.X, currentY);

                    currentY += p.Height + _gapBetweenPanels;

                    p.Show();
                }
                else
                {
                    p.Hide();
                }
            }
        }
        #endregion
    }
}
