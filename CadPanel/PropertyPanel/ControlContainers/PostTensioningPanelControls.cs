﻿using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class PostTensioningPanelControls
    {
        public ComboBox PostTensioningTypeComboBox { get; set; }
        public ComboBox JackingAndGroutingComboBox { get; set; }
        public TextBox PretensionTextBox { get; set; }
    }
}
