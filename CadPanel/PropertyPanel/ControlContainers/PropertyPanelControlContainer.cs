﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class PropertyPanelControlContainer
    {
        public GeneralPanelControls GeneralControls { get; set; }
        public ClosedGeometryPanelControls ClosedGeometryControls { get; set; }
        public NodesPanelControls NodesControls { get; set; }
        public RebarPanelControls RebarControls { get; set; }
        public OpenGeometryPanelControls OpenGeometryControls { get; set; }
        public MaterialPanelControls MaterialControls { get; set; }
        public PostTensioningPanelControls PostTensioningControls { get; set; }
        public UserPropertyPanelControls UserPropertyControls { get; set; }

    }
}
