﻿using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class ClosedGeometryPanelControls
    {
        public CheckBox TreatAsHoleCheckBox { get; set; }
        public TextBox AreaTextBox { get; set; }
        public TextBox PerimeterTextBox { get; set; }
        public TextBox CentroidTextBox { get; set; }
        public TextBox IxxTextBox { get; set; }
        public TextBox IyyTextBox { get; set; }
        public TextBox IxyTextBox { get; set; }
        public TextBox SxxTextBox { get; set; }
        public TextBox SyyTextBox { get; set; }

    }
}
