﻿using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class GeneralPanelControls
    {
        public TextBox ElementNameTextBox { get; set; }
        public ComboBox LayerComboBox { get; set; }
    }
}
