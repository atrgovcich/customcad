﻿using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class RebarPanelControls
    {
        public CheckBox TreatAsRebarCheckBox { get; set; }
        public ComboBox RebarSizeComboBox { get; set; }
    }
}
