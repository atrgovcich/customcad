﻿using System.Windows.Forms;

namespace CadPanel.PropertyPanel.ControlContainers
{
    public class UserPropertyPanelControls
    {
        public DataGridView DataGrid { get; set; }
        public Button AddButton { get; set; }
        public Button RemoveButton { get; set; }
    }
}
