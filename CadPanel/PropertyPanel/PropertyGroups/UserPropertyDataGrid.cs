﻿using System;
using System.Windows.Forms;
using System.Drawing;
using CadPanel.Extensions;
using CadPanel.UserControls;
using Utilities.Extensions.DotNetNative;
using CadPanel.Forms;
using CadPanel.UserProperties;
using CadPanel.Common;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public class UserPropertyDataGrid : PropertyGroup
    {
        #region Public Properties
        public string LabelText { get; set; } = string.Empty;
        public int DataGridWidth { get; set; } = 50;
        public int DataGridHeight { get; set; } = 18;
        public int ButtonWidth { get; set; } = 20;
        public int ButtonHeight { get; set; } = 15;

        protected int _selectedRow = -1;

        public override bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
                _dataGrid.Enabled = _enabled;
                _buttonAdd.Enabled = _enabled;
                _buttonRemove.Enabled = _enabled;
            }
        }

        public DataGridView DataGrid
        {
            get
            {
                return _dataGrid;
            }
        }

        public Button AddButton
        {
            get
            {
                return _buttonAdd;
            }
        }

        public Button RemoveButton
        {
            get
            {
                return _buttonRemove;
            }
        }
        #endregion

        #region Protected Fields
        protected DataGridView _dataGrid;
        protected Label _label;
        protected Button _buttonAdd;
        protected Button _buttonRemove;
        #endregion

        #region Public Methods
        public override string RetrievePropertyString()
        {
            return string.Empty;
        }

        public override void SetPropertyString(string prop)
        {
            
        }

        public override int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont)
        {
            string labelName = "label_" + Name.RemoveWhitespace();

            int labelWidth = 0;
            int labelHeight = 0;

            if (this.LabelText != null && this.LabelText != string.Empty)
            {
                if (cp.ContainsControl(labelName, out Control matchingControl) == true)
                {
                    cp.Controls.Remove(matchingControl);
                }

                _label = new Label() { Text = LabelText };
                _label.UseCompatibleTextRendering = true;
                _label.Font = labelFont;
                _label.Location = basePoint;
                labelWidth = _label.TextWidth();
                _label.Width = labelWidth;
                labelHeight = _label.TextHeight();
                _label.Height = labelHeight;
                cp.Controls.Add(_label);
            }

            string dgvName = "dataGridView_" + Name.RemoveWhitespace();

            if (cp.ContainsControl(dgvName, out Control matchingDgvControl) == true)
            {
                cp.Controls.Remove(matchingDgvControl);
            }

            _dataGrid = new DataGridView();
            _dataGrid.Name = dgvName;
            _dataGrid.Height = DataGridHeight;
            _dataGrid.Width = DataGridWidth;
            _dataGrid.Columns.Add(new DataGridViewTextBoxColumn());
            _dataGrid.Columns.Add(new DataGridViewTextBoxColumn());
            _dataGrid.CellContentClick += new DataGridViewCellEventHandler(this.DGV_CellContentClick);

            _dataGrid.Columns[0].ReadOnly = true;
            _dataGrid.Columns[0].HeaderText = "Name";
            _dataGrid.Columns[0].Name = "Name";
            _dataGrid.Columns[0].Width = Convert.ToInt32(0.8 * DataGridWidth / 2.0);

            _dataGrid.Columns[1].ReadOnly = false;
            _dataGrid.Columns[1].HeaderText = "Value";
            _dataGrid.Columns[1].Name = "Value";
            _dataGrid.Columns[1].Width = Convert.ToInt32(0.8 * DataGridWidth / 2.0);

            _dataGrid.Location = new Point(basePoint.X, basePoint.Y + labelHeight + 5);
            _dataGrid.Enabled = _enabled;
            _dataGrid.AllowUserToAddRows = false;
            _dataGrid.AllowUserToDeleteRows = false;
            _dataGrid.AllowUserToOrderColumns = false;
            _dataGrid.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
            _dataGrid.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;

            cp.Controls.Add(_dataGrid);

            int dgvBottom = basePoint.Y + labelHeight + 5 + _dataGrid.Height;

            string addButtonName = "button_" + Name.RemoveWhitespace() + "_Add";

            if (cp.ContainsControl(addButtonName, out Control addButtonControl) == true)
            {
                cp.Controls.Remove(addButtonControl);
            }

            _buttonAdd = new Button();
            _buttonAdd.Name = addButtonName;
            _buttonAdd.Width = ButtonWidth;
            _buttonAdd.Height = ButtonHeight;
            _buttonAdd.Text = "Add";
            _buttonAdd.Location = new Point(basePoint.X, dgvBottom + 5);
            _buttonAdd.FlatStyle = FlatStyle.Standard;
            _buttonAdd.BackgroundImage = null;
            _buttonAdd.BackgroundImageLayout = ImageLayout.Tile;
            _buttonAdd.Click += new EventHandler(button_Add_Click);

            cp.Controls.Add(_buttonAdd);

            string removeButtonName = "button_" + Name.RemoveWhitespace() + "_Remove";

            if (cp.ContainsControl(removeButtonName, out Control removeButtonControl) == true)
            {
                cp.Controls.Remove(removeButtonControl);
            }

            _buttonRemove = new Button();
            _buttonRemove.Name = removeButtonName;
            _buttonRemove.Width = ButtonWidth;
            _buttonRemove.Height = ButtonHeight;
            _buttonRemove.Text = "Remove";
            _buttonRemove.Location = new Point(basePoint.X + _buttonAdd.Width + 5, dgvBottom + 5);

            cp.Controls.Add(_buttonRemove);

            return dgvBottom + 5 + ButtonHeight;
        }
        #endregion

        private void button_Add_Click(object sender, EventArgs e)
        {
            AddUserPropertyForm frm = new AddUserPropertyForm();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    bool unique = true;

                    for (int i = 0; i < _dataGrid.Rows.Count; i++)
                    {
                        string name = Convert.ToString(_dataGrid[0, i].Value);

                        if (string.Compare(name, frm.PropertyName, ignoreCase: true) == 0)
                        {
                            unique = false;
                            break;
                        }
                    }

                    if (unique == false)
                    {
                        throw new Exception($"The property name '{frm.PropertyName}' is already in use.  Property names must be unique.");
                    }
                    else
                    {
                        _dataGrid.Rows.Add();
                        int lastRow = _dataGrid.Rows.Count - 1;
                        _dataGrid[0, lastRow].Value = frm.PropertyName;

                        if (frm.PropertyType.Equals(UserPropertyType.Number) == true)
                        {
                            _dataGrid[1, lastRow].ValueType = typeof(double);
                            _dataGrid[1, lastRow].Value = Convert.ToString(frm.PropertyValue);
                        }
                        if (frm.PropertyType.Equals(UserPropertyType.Text) == true)
                        {
                            _dataGrid[1, lastRow].ValueType = typeof(string);
                            _dataGrid[1, lastRow].Value = Convert.ToString(frm.PropertyValue);
                        }
                        else if (frm.PropertyType.Equals(UserPropertyType.Boolean) == true)
                        {
                            DataGridViewCheckBoxCell cell = new DataGridViewCheckBoxCell();
                            cell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                            cell.ThreeState = true;
                            cell.IndeterminateValue = CheckState.Indeterminate;
                            cell.TrueValue = CheckState.Checked;
                            cell.FalseValue = CheckState.Unchecked;
                            cell.Style.NullValue = CheckState.Indeterminate;

                            _dataGrid[1, lastRow] = cell;

                            cell.ReadOnly = true;

                            bool val = (bool)frm.PropertyValue;

                            if (val == true)
                            {
                                _dataGrid[1, lastRow].Value = CheckState.Checked;
                            }
                            else
                            {
                                _dataGrid[1, lastRow].Value = CheckState.Unchecked;
                            }
                        }
                        else if (frm.PropertyType.Equals(UserPropertyType.Color) == true)
                        {
                            DataGridViewButtonCell cell = new DataGridViewButtonCell();
                            cell.FlatStyle = FlatStyle.Popup;

                            _dataGrid[1, lastRow] = cell;
                            _dataGrid[1, lastRow].ValueType = typeof(Color);
                            _dataGrid[1,lastRow].Style.BackColor = (Color)(frm.PropertyValue);

                            _dataGrid[1, lastRow].Value = "modifying";
                            _dataGrid[1, lastRow].Value = "";
                        }
                    }               
                }
                catch(Exception ex)
                {
                    MessageBox.Show("There was an error adding the user property:" + Environment.NewLine + Environment.NewLine + ex.Message);
                    frm.Dispose();
                }
            }

            if (frm != null)
            {
                frm.Dispose();
            }

        }

        private void DGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.ColumnIndex >= 0)
            {
                if (dgv[e.ColumnIndex, e.RowIndex].GetType().Equals(typeof(DataGridViewButtonCell)) == true)
                {
                    Color currentColor = dgv[e.ColumnIndex, e.RowIndex].Style.BackColor;

                    ColorDialog cDialog = new ColorDialog();

                    cDialog.Color = currentColor;

                    if (cDialog.ShowDialog() == DialogResult.OK)
                    {
                        currentColor = cDialog.Color;

                        dgv[e.ColumnIndex, e.RowIndex].Style.BackColor = currentColor;

                        dgv[e.ColumnIndex, e.RowIndex].Value = "modifying";

                        dgv[e.ColumnIndex, e.RowIndex].Value = "";
                    }
                }
            }

            _selectedRow = e.RowIndex;
        }
    }
}
