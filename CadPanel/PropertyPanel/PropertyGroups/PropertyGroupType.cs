﻿
namespace CadPanel.PropertyPanel.PropertyGroups
{
    public enum PropertyGroupType
    {
        TextBoxWithLabel,
        ComboBoxWithLabel,
        CheckBox,
        RichTextBoxWithLabel,
        UserPropertyDataGrid
    }
}
