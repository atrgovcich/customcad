﻿using System;
using System.Windows.Forms;
using CadPanel.UserControls;
using Utilities.Extensions.DotNetNative;
using System.Drawing;
using CadPanel.Extensions;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public class RichTextBoxWithLabel : PropertyGroup
    {
        #region Public Properties
        public string LabelText { get; set; } = string.Empty;
        public int RichTextBoxWidth { get; set; } = 50;
        public int RichTextBoxHeight { get; set; } = 18;
        public bool ReadOnly { get; set; } = true;
        public override bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
                _richTextBox.Enabled = _enabled;
            }
        }
        public RichTextBox RichTextBox
        {
            get
            {
                return _richTextBox;
            }
        }
        #endregion

        #region Protected Fields
        protected RichTextBox _richTextBox;
        protected Label _label;
        #endregion

        #region Public Methods
        public override int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont)
        {
            string labelName = "label_" + this.Name.RemoveWhitespace();

            _label = null;
            _richTextBox = null;

            int labelWidth = 0;
            int labelHeight = 0;


            if (this.LabelText != null && this.LabelText != string.Empty)
            {
                _label = new Label() { Text = this.LabelText };
                _label.UseCompatibleTextRendering = true;
                _label.Font = labelFont;
                _label.Location = basePoint;
                labelWidth = _label.TextWidth();
                _label.Width = labelWidth;
                labelHeight = _label.TextHeight();
                _label.Height = labelHeight;
                cp.Controls.Add(_label);
            }

            _richTextBox = new RichTextBox()
            {
                Name = "richTextBox_" + this.Name.RemoveWhitespace(),
                AutoSize = false,
                Width = RichTextBoxWidth,
                Height = RichTextBoxHeight,
                ReadOnly = ReadOnly,
                WordWrap = false
            };

            int richTextBoxerticalLocation = Convert.ToInt32(basePoint.Y + labelHeight + 3);
            int richTextBoxHorizontalLocation = basePoint.X;

            _richTextBox.Location = new Point(richTextBoxHorizontalLocation, richTextBoxerticalLocation);

            cp.Controls.Add(_richTextBox);

            return _richTextBox.Location.Y + _richTextBox.Height;
        }

        public override string RetrievePropertyString()
        {
            if (_richTextBox != null)
            {
                return _richTextBox.Text;
            }
            else
            {
                return string.Empty;
            }
        }

        public override void SetPropertyString(string prop)
        {
            if (prop != null)
            {
                if (_richTextBox != null)
                {
                    _richTextBox.Text = prop;
                }
            }
        }
        #endregion
    }
}
