﻿using System;
using Utilities.Extensions.DotNetNative;
using System.Windows.Forms;
using CadPanel.Extensions;
using System.Drawing;
using static System.Math;
using CadPanel.UserControls;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public class ComboBoxWithLabel : PropertyGroup
    {
        #region Public Properties
        public string LabelText { get; set; }
        public string PostText { get; set; }
        public int ComboBoxWidth { get; set; } = 100;
        public int ComboBoxHeight { get; set; } = 18;

        public ComboBoxStyle DropDownStyle = ComboBoxStyle.DropDownList;

        public override bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;

                if (_comboBox != null)
                {
                    _comboBox.Enabled = _enabled;
                }
            }
        }
        public bool PlaceLabelAboveComboBox { get; set; } = false;

        public ComboBox ComboBox
        {
            get
            {
                return _comboBox;
            }
        }
        #endregion

        #region Protected Fields
        protected ComboBox _comboBox;
        protected Label _preLabel;
        protected Label _postLabel;
        #endregion

        #region Public Methods
        public override int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont)
        {
            string labelName = "label_" + this.Name.RemoveWhitespace();

            _preLabel = null;
            _postLabel = null;
            _comboBox = null;

            int preLabelWidth = 0;
            int preLabelHeight = 0;


            if (this.LabelText != null && this.LabelText != string.Empty)
            {
                _preLabel = new Label() { Text = this.LabelText };
                _preLabel.UseCompatibleTextRendering = true;
                _preLabel.Font = labelFont;
                _preLabel.Location = basePoint;
                preLabelWidth = _preLabel.TextWidth();
                _preLabel.Width = preLabelWidth;
                preLabelHeight = _preLabel.TextHeight();
                _preLabel.Height = preLabelHeight;
                cp.Controls.Add(_preLabel);
            }

            _comboBox = new ComboBox()
            {
                Name = "comboBox_" + this.Name.RemoveWhitespace(),
                AutoSize = false,
                Width = ComboBoxWidth,
                Height = ComboBoxHeight,
                DropDownStyle = DropDownStyle
            };

            int comboBoxVerticalLocation = Convert.ToInt32(basePoint.Y + preLabelHeight * 0.5 - 0.5 * _comboBox.Height);
            int comboBoxHorizontalLocation = basePoint.X + preLabelWidth + 5;

            if (PlaceLabelAboveComboBox == true)
            {
                comboBoxVerticalLocation = Convert.ToInt32(basePoint.Y + preLabelHeight + 3);
                comboBoxHorizontalLocation = basePoint.X;
            }

            _comboBox.Location = new Point(comboBoxHorizontalLocation, comboBoxVerticalLocation);

            cp.Controls.Add(_comboBox);

            if (this.PostText != null && this.PostText != string.Empty)
            {
                _postLabel = new Label() { Text = this.PostText };
                _preLabel.UseCompatibleTextRendering = true;
                _postLabel.Font = labelFont;
                _postLabel.Height = _postLabel.TextHeight();
                _postLabel.Location = new Point(_comboBox.Location.X + _comboBox.Width + 3, Convert.ToInt32(Ceiling(_comboBox.Location.Y + 0.5 * _comboBox.Height - 0.5 * _postLabel.Height)));

                cp.Controls.Add(_postLabel);
            }

            return Math.Max(_preLabel.Location.Y + preLabelHeight, _comboBox.Location.Y + _comboBox.Height);
        }

        public override string RetrievePropertyString()
        {
            if (_comboBox != null)
            {
                return _comboBox.Text;
            }
            else
            {
                return string.Empty;
            }
        }

        public override void SetPropertyString(string prop)
        {
            if (prop != null)
            {
                if (_comboBox != null)
                {
                    for (int i = 0; i < _comboBox.Items.Count; i++)
                    {
                        string cbVal = _comboBox.Items[i].ToString();

                        if (string.Compare(prop, cbVal, ignoreCase: true) == 0)
                        {
                            _comboBox.SelectedIndex = i;

                            break;
                        }
                    }
                }
            }
        }

        public void AddItemToComboBox(string s)
        {
            bool unique = true;

            for (int i = 0;i < _comboBox.Items.Count; i++)
            {
                string item = _comboBox.Items[i].ToString();

                if (string.Compare(s, item, ignoreCase: true) == 0)
                {
                    unique = false;

                    break;
                }
            }

            if (unique == true)
            {
                _comboBox.Items.Add(s);
            }
        }

        public void InsertItemInComboBox(string s, int index)
        {
            bool unique = true;

            for (int i = 0; i < _comboBox.Items.Count; i++)
            {
                string item = _comboBox.Items[i].ToString();

                if (string.Compare(s, item, ignoreCase: true) == 0)
                {
                    unique = false;

                    break;
                }
            }

            if (unique == true)
            {
                if (index >= 0 && index < _comboBox.Items.Count)
                {
                    _comboBox.Items.Insert(index, s);
                }
                
            }
        }

        public void RemoveItemFromComboBox(string s)
        {
            int removeIndex = -1;

            for (int i = 0;i < _comboBox.Items.Count; i++)
            {
                string item = _comboBox.Items[i].ToString();

                if (string.Compare(item, s, ignoreCase: true) == 0)
                {
                    removeIndex = i;

                    break;
                }
            }

            if (removeIndex >= 0)
            {
                _comboBox.Items.RemoveAt(removeIndex);
            }
        }

        public void RemoveItemFromComboBoxAtIndex(int index)
        {
            if (_comboBox != null)
            {
                if (index >= 0 && index < _comboBox.Items.Count)
                {
                    _comboBox.Items.RemoveAt(index);
                }
            }
        }
        #endregion


    }
}
