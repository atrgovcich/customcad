﻿using System;
using Utilities.Extensions.DotNetNative;
using System.Windows.Forms;
using System.Drawing;
using CadPanel.Extensions;
using CadPanel.UserControls;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public class TextBoxWithLabel : PropertyGroup
    {
        #region Public Properties
        public string LabelText { get; set; } = string.Empty;
        public string PostText { get; set; } = string.Empty;
        public int TextBoxWidth { get; set; } = 50;
        public int TextBoxHeight { get; set; } = 18;
        public bool ReadOnly { get; set; } = true;
        public override bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
                _textBox.Enabled = _enabled;
            }
        }
        public TextBox TextBox
        {
            get
            {
                return _textBox;
            }
        }
        #endregion

        #region Protected Fields
        protected TextBox _textBox;
        protected Label _preLabel;
        protected Label _postLabel;
        #endregion

        #region Public Methods
        public override int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont)
        {
            string labelName = "label_" + this.Name.RemoveWhitespace();

            _preLabel = null;
            _postLabel = null;
            _textBox = null;

            int preLabelWidth = 0;
            int preLabelHeight = 0;


            if (this.LabelText != null && this.LabelText != string.Empty)
            {
                _preLabel = new Label() { Text = this.LabelText };
                _preLabel.UseCompatibleTextRendering = true;
                _preLabel.Font = labelFont;
                _preLabel.Location = basePoint;
                preLabelWidth = _preLabel.TextWidth();
                _preLabel.Width = preLabelWidth;
                preLabelHeight = _preLabel.TextHeight();
                _preLabel.Height = preLabelHeight;
                cp.Controls.Add(_preLabel);
            }

            _textBox = new TextBox()
            {
                Name = "textBox_" + this.Name.RemoveWhitespace(),
                AutoSize = false,
                Width = TextBoxWidth,
                Height = TextBoxHeight,
                Size = new Size(TextBoxWidth, TextBoxHeight)
            };
            
            if (ReadOnly == true)
            {
                _textBox.ReadOnly = true;
            }

            int textBoxVerticalLocation = Convert.ToInt32(basePoint.Y + preLabelHeight * 0.5 - 0.5 * _textBox.Height);

            _textBox.Location = new Point(basePoint.X + preLabelWidth + 5, textBoxVerticalLocation);

            cp.Controls.Add(_textBox);

            if (this.PostText != null && this.PostText != string.Empty)
            {
                _postLabel = new Label() { Text = this.PostText };
                _preLabel.UseCompatibleTextRendering = true;
                _postLabel.Font = labelFont;
                _postLabel.Height = _postLabel.TextHeight();
                _postLabel.Location = new Point(_textBox.Location.X + _textBox.Width + 3, basePoint.Y);

                cp.Controls.Add(_postLabel);
            }

            return Math.Max(basePoint.Y + preLabelHeight, basePoint.Y + _textBox.Height);
        }

        public override string RetrievePropertyString()
        {
            if (_textBox != null)
            {
                return _textBox.Text;
            }
            else
            {
                return string.Empty;
            }
        }

        public override void SetPropertyString(string prop)
        {
            if (prop != null)
            {
                if (_textBox != null)
                {
                    _textBox.Text = prop;
                }
            }
        }
        #endregion
    }
}
