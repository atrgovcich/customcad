﻿using System.Drawing;
using CadPanel.UserControls;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public abstract class PropertyGroup
    {
        public string Name { get; set; }
        public PropertyGroupType TypeOfPropertyGroup { get; set; }

        protected bool _enabled = true;
        public abstract bool Enabled { get; set; }

        public abstract int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont);

        public abstract string RetrievePropertyString();

        public abstract void SetPropertyString(string prop);
    }
}
