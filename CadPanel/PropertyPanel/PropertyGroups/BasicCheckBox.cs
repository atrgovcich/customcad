﻿using System.Windows.Forms;
using System.Drawing;
using CadPanel.Extensions;
using CadPanel.UserControls;

namespace CadPanel.PropertyPanel.PropertyGroups
{
    public class BasicCheckBox : PropertyGroup
    {
        #region Public Properties
        public string CheckBoxText { get; set; } = string.Empty;
        public int PixelIndent { get; set; } = 0;
        public override bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;

                if (_checkBox != null)
                {
                    _checkBox.Enabled = _enabled;
                }
            }
        }
        public bool Checked
        {
            get
            {
                if (_checkBox != null)
                {
                    return _checkBox.Checked;
                }
                else
                {
                    return false;
                }
            }
        }
        public CheckBox CheckBox
        {
            get
            {
                return _checkBox;
            }
        }

        public bool ThreeState { get; set; } = true;

        #endregion

        #region Public Methods
        public override string RetrievePropertyString()
        {
            if (_checkBox != null)
            {
                return _checkBox.Checked.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public override void SetPropertyString(string prop)
        {
            bool checkState = false;

            if (string.Compare(prop, "true", ignoreCase: true) == 0)
            {
                checkState = true;
            }

            _checkBox.Checked = checkState;
        }

        public override int AddToCollapsiblePanel(CollapsiblePanel cp, Point basePoint, Font labelFont)
        {
            string controlName = "checkBox_" + Name;

            if (cp.ContainsControl(controlName, out Control matchingControl) == true)
            {
                cp.Controls.Remove(matchingControl);
            }

            if (ThreeState == true)
            {
                _checkBox = new CheckBoxTS();
            }
            else
            {
                _checkBox = new CheckBox();
            }

            _checkBox.Name = controlName;
            _checkBox.Location = new Point(basePoint.X + PixelIndent, basePoint.Y);
            _checkBox.Font = labelFont;
            _checkBox.UseCompatibleTextRendering = true;
            _checkBox.Enabled = _enabled;
            _checkBox.Text = CheckBoxText;
            _checkBox.AutoSize = true;


            cp.Controls.Add(_checkBox);

            return basePoint.Y + _checkBox.Height;
        }


        #endregion

        #region Protected Fields
        protected CheckBox _checkBox;
        #endregion


    }
}
