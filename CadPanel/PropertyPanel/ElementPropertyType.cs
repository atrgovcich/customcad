﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPanel.PropertyPanel
{
    public enum ElementPropertyType
    {
        General,
        Nodes,
        ClosedGeometry,
        OpenGeometry,
        Rebar,
        Material,
        PostTensioning,
        UserProperties
    }
}
