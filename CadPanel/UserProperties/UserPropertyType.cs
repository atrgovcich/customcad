﻿using System.ComponentModel;

namespace CadPanel.UserProperties
{
    public enum UserPropertyType
    {
        [Description("Text")]
        Text,
        [Description("Number")]
        Number,
        [Description("Boolean")]
        Boolean,
        [Description("Color")]
        Color
    }
}
