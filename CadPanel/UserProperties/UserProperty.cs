﻿using System.Drawing;

namespace CadPanel.UserProperties
{
    public abstract class UserProperty
    {
        #region Public Properties
        /// <summary>
        /// Type of user property
        /// </summary>
        public UserPropertyType PropertyType { get; protected set; }

        /// <summary>
        /// Property name
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// Value assigned to user property
        /// </summary>
        public abstract object PropertyValue { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Returns a new, blank, number property
        /// </summary>
        /// <returns></returns>
        public static UserProperty NumberProperty()
        {
            return new UserPropertyNumber();
        }

        /// <summary>
        /// Returns a new number property with the given value
        /// </summary>
        /// <param name="name">Name of property</param>
        /// <param name="value">Number value</param>
        /// <returns></returns>
        public static UserProperty NumberProperty(string name, double value)
        {
            return new UserPropertyNumber(name, value);
        }

        /// <summary>
        /// Returns a new, blank, text property
        /// </summary>
        /// <returns></returns>
        public static UserProperty TextProperty()
        {
            return new UserPropertyText();
        }

        /// <summary>
        /// Returns a new text property with the given value
        /// </summary>
        /// <param name="name">Property name</param>
        /// <param name="value">Text value</param>
        /// <returns></returns>
        public static UserProperty TextProperty(string name, string value)
        {
            return new UserPropertyText(name, value);
        }

        /// <summary>
        /// Returns a new, blank, boolean property
        /// </summary>
        /// <returns></returns>
        public static UserProperty BooleanProperty()
        {
            return new UserPropertyBoolean();
        }

        /// <summary>
        /// Returns a new boolean property with the given value
        /// </summary>
        /// <param name="name">Property name</param>
        /// <param name="value">Boolean value</param>
        /// <returns></returns>
        public static UserProperty BooleanProperty(string name, bool value)
        {
            return new UserPropertyBoolean(name, value);
        }

        /// <summary>
        /// Returns a new, blank, color property
        /// </summary>
        /// <returns></returns>
        public static UserProperty ColorProperty()
        {
            return new UserPropertyColor();
        }

        /// <summary>
        /// Returns a new color property with the given value
        /// </summary>
        /// <param name="name">Property name</param>
        /// <param name="value">Color value</param>
        /// <returns></returns>
        public static UserProperty ColorProperty(string name, Color value)
        {
            return new UserPropertyColor(name, value);
        }
        #endregion
    }
}
