﻿using System.Drawing;
using System;

namespace CadPanel.UserProperties
{
    public class UserPropertyColor : UserProperty
    {
        #region Public Properties
        private Color _value;

        /// <summary>
        /// Returns the value of the property
        /// </summary>
        public Color Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Returns the value of the property, as an object type
        /// </summary>
        public override object PropertyValue
        {
            get
            {
                return _value;
            }
            set
            {
                if (value.GetType().Equals(typeof(Color)) == true)
                {
                    _value = (Color)value;
                }
                else
                {
                    throw new ArgumentException("Value of the user property must be of type 'color'");
                }
            }
        }
        #endregion

        #region Constructors
        public UserPropertyColor()
        {
            PropertyType = UserPropertyType.Color;
        }

        public UserPropertyColor(string name, Color value) : this()
        {
            PropertyName = name;
            Value = value;
        }
        #endregion
    }
}
