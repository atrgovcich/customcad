﻿using System;
using Utilities.Extensions.DotNetNative;

namespace CadPanel.UserProperties
{
    public class UserPropertyNumber : UserProperty
    {
        #region Public Properties
        private double _value;

        /// <summary>
        /// Returns the value of the property
        /// </summary>
        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Returns the value of the property, as an object type
        /// </summary>
        public override object PropertyValue
        {
            get
            {
                return _value;
            }
            set
            {
                if (value.IsNumericType() == true)
                {
                    _value = (double)value;
                }
                else
                {
                    throw new ArgumentException("Value of the user property must be of type 'double', 'float', or 'int'");
                }
            }
        }
        #endregion

        #region Constructors
        public UserPropertyNumber()
        {
            PropertyType = UserPropertyType.Number;
        }

        public UserPropertyNumber(string name, double value) : this()
        {
            PropertyName = name;
            Value = value;
        }
        #endregion
    }
}
