﻿using System;

namespace CadPanel.UserProperties
{
    public class UserPropertyBoolean : UserProperty
    {
        #region Public Properties
        private bool _value;

        /// <summary>
        /// Returns the value of the property
        /// </summary>
        public bool Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Returns the value of the property, as an object type
        /// </summary>
        public override object PropertyValue
        {
            get
            {
                return _value;
            }
            set
            {
                if (value.GetType().Equals(typeof(bool)) == true)
                {
                    _value = (bool)value;
                }
                else
                {
                    throw new ArgumentException("Value of the user property must be of type 'boolean'");
                }
            }
        }
        #endregion

        #region Constructors
        public UserPropertyBoolean()
        {
            PropertyType = UserPropertyType.Boolean;
        }

        public UserPropertyBoolean(string name, bool value) : this()
        {
            PropertyName = name;
            Value = value;
        }
        #endregion
    }
}
