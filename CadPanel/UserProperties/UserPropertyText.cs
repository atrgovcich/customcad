﻿using System;

namespace CadPanel.UserProperties
{
    public class UserPropertyText : UserProperty
    {
        #region Public Properties
        private string _value;

        /// <summary>
        /// Returns the value of the property
        /// </summary>
        public string Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Returns the value of the property, as an object type
        /// </summary>
        public override object PropertyValue
        {
            get
            {
                return _value;
            }
            set
            {
                if (value.GetType().Equals(typeof(string)) == true)
                {
                    _value = (string)value;
                }
                else
                {
                    throw new ArgumentException("Value of the user property must be of type 'string'");
                }
            }
        }
        #endregion

        #region Constructors
        public UserPropertyText()
        {
            PropertyType = UserPropertyType.Text;
        }

        public UserPropertyText(string name, string value) : this()
        {
            PropertyName = name;
            Value = value;
        }
        #endregion
    }
}
