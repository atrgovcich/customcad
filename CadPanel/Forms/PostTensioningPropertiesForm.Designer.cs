﻿namespace CadPanel.Forms
{
    partial class PostTensioningPropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Close = new System.Windows.Forms.Button();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Edit = new System.Windows.Forms.Button();
            this.button_Remove = new System.Windows.Forms.Button();
            this.dataGridView_Types = new System.Windows.Forms.DataGridView();
            this.TypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StrandType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaterialName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Area = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Types)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(356, 385);
            this.button_Close.Margin = new System.Windows.Forms.Padding(2);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(57, 23);
            this.button_Close.TabIndex = 9;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(11, 342);
            this.button_Add.Margin = new System.Windows.Forms.Padding(2);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(58, 21);
            this.button_Add.TabIndex = 8;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // button_Edit
            // 
            this.button_Edit.Location = new System.Drawing.Point(73, 342);
            this.button_Edit.Margin = new System.Windows.Forms.Padding(2);
            this.button_Edit.Name = "button_Edit";
            this.button_Edit.Size = new System.Drawing.Size(58, 21);
            this.button_Edit.TabIndex = 7;
            this.button_Edit.Text = "Edit";
            this.button_Edit.UseVisualStyleBackColor = true;
            this.button_Edit.Click += new System.EventHandler(this.button_Edit_Click);
            // 
            // button_Remove
            // 
            this.button_Remove.Location = new System.Drawing.Point(135, 342);
            this.button_Remove.Margin = new System.Windows.Forms.Padding(2);
            this.button_Remove.Name = "button_Remove";
            this.button_Remove.Size = new System.Drawing.Size(58, 21);
            this.button_Remove.TabIndex = 6;
            this.button_Remove.Text = "Remove";
            this.button_Remove.UseVisualStyleBackColor = true;
            this.button_Remove.Click += new System.EventHandler(this.Button_Remove_Click);
            // 
            // dataGridView_Types
            // 
            this.dataGridView_Types.AllowUserToAddRows = false;
            this.dataGridView_Types.AllowUserToDeleteRows = false;
            this.dataGridView_Types.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Types.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeName,
            this.StrandType,
            this.MaterialName,
            this.Diameter,
            this.Area});
            this.dataGridView_Types.Location = new System.Drawing.Point(11, 11);
            this.dataGridView_Types.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridView_Types.Name = "dataGridView_Types";
            this.dataGridView_Types.ReadOnly = true;
            this.dataGridView_Types.RowTemplate.Height = 28;
            this.dataGridView_Types.Size = new System.Drawing.Size(402, 328);
            this.dataGridView_Types.TabIndex = 5;
            // 
            // TypeName
            // 
            this.TypeName.HeaderText = "Name";
            this.TypeName.Name = "TypeName";
            this.TypeName.ReadOnly = true;
            this.TypeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TypeName.Width = 125;
            // 
            // StrandType
            // 
            this.StrandType.HeaderText = "Strand Type";
            this.StrandType.Name = "StrandType";
            this.StrandType.ReadOnly = true;
            this.StrandType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StrandType.Width = 125;
            // 
            // MaterialName
            // 
            this.MaterialName.HeaderText = "Material";
            this.MaterialName.Name = "MaterialName";
            this.MaterialName.ReadOnly = true;
            this.MaterialName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MaterialName.Width = 125;
            // 
            // Diameter
            // 
            this.Diameter.HeaderText = "Diameter (in)";
            this.Diameter.Name = "Diameter";
            this.Diameter.ReadOnly = true;
            this.Diameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Diameter.Width = 75;
            // 
            // Area
            // 
            this.Area.HeaderText = "Steel Area (sq in)";
            this.Area.Name = "Area";
            this.Area.ReadOnly = true;
            this.Area.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Area.Width = 75;
            // 
            // PostTensioningPropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 417);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Edit);
            this.Controls.Add(this.button_Remove);
            this.Controls.Add(this.dataGridView_Types);
            this.Name = "PostTensioningPropertiesForm";
            this.Text = "Manage Post Tensioning";
            this.Load += new System.EventHandler(this.PostTensioningPropertiesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Types)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button button_Close;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Edit;
        internal System.Windows.Forms.Button button_Remove;
        internal System.Windows.Forms.DataGridView dataGridView_Types;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn StrandType;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaterialName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Area;
    }
}