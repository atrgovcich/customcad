﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FiberSectionAnalysis.AxialForceMomentAnalysis;
using FiberSectionAnalysis.General;

namespace CadPanel.Forms
{
    public partial class AddFiberSectionAnalysisCase : Form
    {
        private List<GeneralAnalysisCase> _pmCases;

        public string NewName { get; set; } = null;

        public AddFiberSectionAnalysisCase(List<GeneralAnalysisCase> pmCases)
        {
            InitializeComponent();

            _pmCases = pmCases;
        }

        private void AddAxialForceMomentAnalysisCaseForm_Load(object sender, EventArgs e)
        {

        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            string name = textBox_Name.Text;

            try
            {
                bool unique = true;

                if (_pmCases != null && _pmCases.Count > 0)
                {
                    for (int i = 0; i < _pmCases.Count; i++)
                    {
                        if (string.Compare(_pmCases[i].Name, name, ignoreCase: true) == 0)
                        {
                            unique = false;

                            break;
                        }
                    }
                }

                if (unique == false)
                {
                    throw new Exception($"The name '{name}' is already in use.  Please enter a unique analysis case name.");
                }
                else
                {
                    NewName = name;

                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + ex.Message, "Input Error");
            }
        }
    }
}
