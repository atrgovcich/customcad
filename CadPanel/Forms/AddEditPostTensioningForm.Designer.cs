﻿namespace CadPanel.Forms
{
    partial class AddEditPostTensioningForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_InternalStrandType = new System.Windows.Forms.Label();
            this.comboBox_InternalStrandType = new System.Windows.Forms.ComboBox();
            this.comboBox_Material = new System.Windows.Forms.ComboBox();
            this.Label_Material = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.textBox_Diameter = new System.Windows.Forms.TextBox();
            this.textBox_Area = new System.Windows.Forms.TextBox();
            this.Label_StrandType = new System.Windows.Forms.Label();
            this.comboBox_StrandType = new System.Windows.Forms.ComboBox();
            this.Label_Area = new System.Windows.Forms.Label();
            this.Label_Diameter = new System.Windows.Forms.Label();
            this.Label_Name = new System.Windows.Forms.Label();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label_InternalStrandDiam = new System.Windows.Forms.Label();
            this.textBox_InternalStrandDiam = new System.Windows.Forms.TextBox();
            this.label_InternalStrandUnits = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_InternalStrandType
            // 
            this.label_InternalStrandType.AutoSize = true;
            this.label_InternalStrandType.Location = new System.Drawing.Point(33, 167);
            this.label_InternalStrandType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_InternalStrandType.Name = "label_InternalStrandType";
            this.label_InternalStrandType.Size = new System.Drawing.Size(115, 13);
            this.label_InternalStrandType.TabIndex = 34;
            this.label_InternalStrandType.Text = "Internal Strand Type = ";
            // 
            // comboBox_InternalStrandType
            // 
            this.comboBox_InternalStrandType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_InternalStrandType.FormattingEnabled = true;
            this.comboBox_InternalStrandType.Location = new System.Drawing.Point(150, 165);
            this.comboBox_InternalStrandType.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_InternalStrandType.Name = "comboBox_InternalStrandType";
            this.comboBox_InternalStrandType.Size = new System.Drawing.Size(135, 21);
            this.comboBox_InternalStrandType.TabIndex = 33;
            // 
            // comboBox_Material
            // 
            this.comboBox_Material.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Material.FormattingEnabled = true;
            this.comboBox_Material.Location = new System.Drawing.Point(150, 136);
            this.comboBox_Material.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Material.Name = "comboBox_Material";
            this.comboBox_Material.Size = new System.Drawing.Size(135, 21);
            this.comboBox_Material.TabIndex = 26;
            // 
            // Label_Material
            // 
            this.Label_Material.AutoSize = true;
            this.Label_Material.Location = new System.Drawing.Point(92, 138);
            this.Label_Material.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Material.Name = "Label_Material";
            this.Label_Material.Size = new System.Drawing.Size(56, 13);
            this.Label_Material.TabIndex = 32;
            this.Label_Material.Text = "Material = ";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(221, 83);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(35, 13);
            this.Label2.TabIndex = 31;
            this.Label2.Text = "sq. in.";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(221, 58);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(18, 13);
            this.Label1.TabIndex = 30;
            this.Label1.Text = "in.";
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(324, 242);
            this.button_OK.Margin = new System.Windows.Forms.Padding(2);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(62, 25);
            this.button_OK.TabIndex = 28;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(390, 242);
            this.button_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(62, 25);
            this.button_Cancel.TabIndex = 29;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // textBox_Diameter
            // 
            this.textBox_Diameter.Location = new System.Drawing.Point(150, 56);
            this.textBox_Diameter.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Diameter.Name = "textBox_Diameter";
            this.textBox_Diameter.Size = new System.Drawing.Size(68, 20);
            this.textBox_Diameter.TabIndex = 20;
            // 
            // textBox_Area
            // 
            this.textBox_Area.Location = new System.Drawing.Point(150, 81);
            this.textBox_Area.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Area.Name = "textBox_Area";
            this.textBox_Area.Size = new System.Drawing.Size(68, 20);
            this.textBox_Area.TabIndex = 23;
            // 
            // Label_StrandType
            // 
            this.Label_StrandType.AutoSize = true;
            this.Label_StrandType.Location = new System.Drawing.Point(72, 110);
            this.Label_StrandType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_StrandType.Name = "Label_StrandType";
            this.Label_StrandType.Size = new System.Drawing.Size(77, 13);
            this.Label_StrandType.TabIndex = 27;
            this.Label_StrandType.Text = "Strand Type = ";
            // 
            // comboBox_StrandType
            // 
            this.comboBox_StrandType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_StrandType.FormattingEnabled = true;
            this.comboBox_StrandType.Location = new System.Drawing.Point(150, 108);
            this.comboBox_StrandType.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_StrandType.Name = "comboBox_StrandType";
            this.comboBox_StrandType.Size = new System.Drawing.Size(135, 21);
            this.comboBox_StrandType.TabIndex = 25;
            this.comboBox_StrandType.SelectedIndexChanged += new System.EventHandler(this.ComboBox_StrandType_SelectedIndexChanged);
            // 
            // Label_Area
            // 
            this.Label_Area.AutoSize = true;
            this.Label_Area.Location = new System.Drawing.Point(67, 83);
            this.Label_Area.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Area.Name = "Label_Area";
            this.Label_Area.Size = new System.Drawing.Size(80, 13);
            this.Label_Area.TabIndex = 24;
            this.Label_Area.Text = "Area of Steel = ";
            // 
            // Label_Diameter
            // 
            this.Label_Diameter.AutoSize = true;
            this.Label_Diameter.Location = new System.Drawing.Point(46, 58);
            this.Label_Diameter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Diameter.Name = "Label_Diameter";
            this.Label_Diameter.Size = new System.Drawing.Size(100, 13);
            this.Label_Diameter.TabIndex = 22;
            this.Label_Diameter.Text = "Outside Diameter = ";
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(11, 9);
            this.Label_Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(62, 13);
            this.Label_Name.TabIndex = 21;
            this.Label_Name.Text = "Type Name";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(14, 24);
            this.textBox_Name.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(207, 20);
            this.textBox_Name.TabIndex = 19;
            // 
            // label_InternalStrandDiam
            // 
            this.label_InternalStrandDiam.AutoSize = true;
            this.label_InternalStrandDiam.Location = new System.Drawing.Point(12, 195);
            this.label_InternalStrandDiam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_InternalStrandDiam.Name = "label_InternalStrandDiam";
            this.label_InternalStrandDiam.Size = new System.Drawing.Size(133, 13);
            this.label_InternalStrandDiam.TabIndex = 35;
            this.label_InternalStrandDiam.Text = "Internal Strand Diameter = ";
            // 
            // textBox_InternalStrandDiam
            // 
            this.textBox_InternalStrandDiam.Location = new System.Drawing.Point(150, 193);
            this.textBox_InternalStrandDiam.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_InternalStrandDiam.Name = "textBox_InternalStrandDiam";
            this.textBox_InternalStrandDiam.Size = new System.Drawing.Size(68, 20);
            this.textBox_InternalStrandDiam.TabIndex = 36;
            // 
            // label_InternalStrandUnits
            // 
            this.label_InternalStrandUnits.AutoSize = true;
            this.label_InternalStrandUnits.Location = new System.Drawing.Point(221, 195);
            this.label_InternalStrandUnits.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_InternalStrandUnits.Name = "label_InternalStrandUnits";
            this.label_InternalStrandUnits.Size = new System.Drawing.Size(18, 13);
            this.label_InternalStrandUnits.TabIndex = 37;
            this.label_InternalStrandUnits.Text = "in.";
            // 
            // AddEditPostTensioningForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 276);
            this.Controls.Add(this.label_InternalStrandUnits);
            this.Controls.Add(this.textBox_InternalStrandDiam);
            this.Controls.Add(this.label_InternalStrandDiam);
            this.Controls.Add(this.label_InternalStrandType);
            this.Controls.Add(this.comboBox_InternalStrandType);
            this.Controls.Add(this.comboBox_Material);
            this.Controls.Add(this.Label_Material);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.textBox_Diameter);
            this.Controls.Add(this.textBox_Area);
            this.Controls.Add(this.Label_StrandType);
            this.Controls.Add(this.comboBox_StrandType);
            this.Controls.Add(this.Label_Area);
            this.Controls.Add(this.Label_Diameter);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.textBox_Name);
            this.MaximumSize = new System.Drawing.Size(480, 315);
            this.MinimumSize = new System.Drawing.Size(480, 315);
            this.Name = "AddEditPostTensioningForm";
            this.Text = "Add or Edit Post Tensioning";
            this.Load += new System.EventHandler(this.AddEditPostTensioningForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label label_InternalStrandType;
        internal System.Windows.Forms.ComboBox comboBox_InternalStrandType;
        internal System.Windows.Forms.ComboBox comboBox_Material;
        internal System.Windows.Forms.Label Label_Material;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_Cancel;
        internal System.Windows.Forms.TextBox textBox_Diameter;
        internal System.Windows.Forms.TextBox textBox_Area;
        internal System.Windows.Forms.Label Label_StrandType;
        internal System.Windows.Forms.ComboBox comboBox_StrandType;
        internal System.Windows.Forms.Label Label_Area;
        internal System.Windows.Forms.Label Label_Diameter;
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.TextBox textBox_Name;
        internal System.Windows.Forms.Label label_InternalStrandDiam;
        internal System.Windows.Forms.TextBox textBox_InternalStrandDiam;
        internal System.Windows.Forms.Label label_InternalStrandUnits;
    }
}