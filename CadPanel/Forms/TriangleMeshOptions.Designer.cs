﻿namespace CadPanel.Forms
{
    partial class TriangleMeshOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_MeshOptions = new System.Windows.Forms.GroupBox();
            this.checkBox_SweepLine = new System.Windows.Forms.CheckBox();
            this.checkBox_QualityMesh = new System.Windows.Forms.CheckBox();
            this.checkBox_Convex = new System.Windows.Forms.CheckBox();
            this.checkBox_ConformingDelaunay = new System.Windows.Forms.CheckBox();
            this.groupBox_Refinement = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_RefineAfterSmooth = new System.Windows.Forms.CheckBox();
            this.numericUpDown_NumberOfSmooth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_NumberOfRefinements = new System.Windows.Forms.NumericUpDown();
            this.groupBox_MeshConstraints = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_MaxAngle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_MinAngle = new System.Windows.Forms.TextBox();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.groupBox_MeshOptions.SuspendLayout();
            this.groupBox_Refinement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumberOfSmooth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumberOfRefinements)).BeginInit();
            this.groupBox_MeshConstraints.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_MeshOptions
            // 
            this.groupBox_MeshOptions.Controls.Add(this.checkBox_SweepLine);
            this.groupBox_MeshOptions.Controls.Add(this.checkBox_QualityMesh);
            this.groupBox_MeshOptions.Controls.Add(this.checkBox_Convex);
            this.groupBox_MeshOptions.Controls.Add(this.checkBox_ConformingDelaunay);
            this.groupBox_MeshOptions.Location = new System.Drawing.Point(12, 19);
            this.groupBox_MeshOptions.Name = "groupBox_MeshOptions";
            this.groupBox_MeshOptions.Size = new System.Drawing.Size(232, 144);
            this.groupBox_MeshOptions.TabIndex = 0;
            this.groupBox_MeshOptions.TabStop = false;
            this.groupBox_MeshOptions.Text = "Mesh Options";
            // 
            // checkBox_SweepLine
            // 
            this.checkBox_SweepLine.AutoSize = true;
            this.checkBox_SweepLine.Location = new System.Drawing.Point(6, 111);
            this.checkBox_SweepLine.Name = "checkBox_SweepLine";
            this.checkBox_SweepLine.Size = new System.Drawing.Size(103, 21);
            this.checkBox_SweepLine.TabIndex = 3;
            this.checkBox_SweepLine.Text = "Sweep Line";
            this.checkBox_SweepLine.UseVisualStyleBackColor = true;
            // 
            // checkBox_QualityMesh
            // 
            this.checkBox_QualityMesh.AutoSize = true;
            this.checkBox_QualityMesh.Location = new System.Drawing.Point(6, 84);
            this.checkBox_QualityMesh.Name = "checkBox_QualityMesh";
            this.checkBox_QualityMesh.Size = new System.Drawing.Size(112, 21);
            this.checkBox_QualityMesh.TabIndex = 2;
            this.checkBox_QualityMesh.Text = "Quality Mesh";
            this.checkBox_QualityMesh.UseVisualStyleBackColor = true;
            // 
            // checkBox_Convex
            // 
            this.checkBox_Convex.AutoSize = true;
            this.checkBox_Convex.Location = new System.Drawing.Point(6, 57);
            this.checkBox_Convex.Name = "checkBox_Convex";
            this.checkBox_Convex.Size = new System.Drawing.Size(76, 21);
            this.checkBox_Convex.TabIndex = 1;
            this.checkBox_Convex.Text = "Convex";
            this.checkBox_Convex.UseVisualStyleBackColor = true;
            // 
            // checkBox_ConformingDelaunay
            // 
            this.checkBox_ConformingDelaunay.AutoSize = true;
            this.checkBox_ConformingDelaunay.Location = new System.Drawing.Point(6, 30);
            this.checkBox_ConformingDelaunay.Name = "checkBox_ConformingDelaunay";
            this.checkBox_ConformingDelaunay.Size = new System.Drawing.Size(166, 21);
            this.checkBox_ConformingDelaunay.TabIndex = 0;
            this.checkBox_ConformingDelaunay.Text = "Conforming Delaunay";
            this.checkBox_ConformingDelaunay.UseVisualStyleBackColor = true;
            // 
            // groupBox_Refinement
            // 
            this.groupBox_Refinement.Controls.Add(this.label2);
            this.groupBox_Refinement.Controls.Add(this.label1);
            this.groupBox_Refinement.Controls.Add(this.checkBox_RefineAfterSmooth);
            this.groupBox_Refinement.Controls.Add(this.numericUpDown_NumberOfSmooth);
            this.groupBox_Refinement.Controls.Add(this.numericUpDown_NumberOfRefinements);
            this.groupBox_Refinement.Location = new System.Drawing.Point(12, 169);
            this.groupBox_Refinement.Name = "groupBox_Refinement";
            this.groupBox_Refinement.Size = new System.Drawing.Size(232, 130);
            this.groupBox_Refinement.TabIndex = 1;
            this.groupBox_Refinement.TabStop = false;
            this.groupBox_Refinement.Text = "Refinement";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Number of Smoothings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Number of Refinements";
            // 
            // checkBox_RefineAfterSmooth
            // 
            this.checkBox_RefineAfterSmooth.AutoSize = true;
            this.checkBox_RefineAfterSmooth.Location = new System.Drawing.Point(24, 88);
            this.checkBox_RefineAfterSmooth.Name = "checkBox_RefineAfterSmooth";
            this.checkBox_RefineAfterSmooth.Size = new System.Drawing.Size(193, 21);
            this.checkBox_RefineAfterSmooth.TabIndex = 2;
            this.checkBox_RefineAfterSmooth.Text = "Refine After Each Smooth";
            this.checkBox_RefineAfterSmooth.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_NumberOfSmooth
            // 
            this.numericUpDown_NumberOfSmooth.Location = new System.Drawing.Point(169, 60);
            this.numericUpDown_NumberOfSmooth.Name = "numericUpDown_NumberOfSmooth";
            this.numericUpDown_NumberOfSmooth.Size = new System.Drawing.Size(48, 22);
            this.numericUpDown_NumberOfSmooth.TabIndex = 1;
            // 
            // numericUpDown_NumberOfRefinements
            // 
            this.numericUpDown_NumberOfRefinements.Location = new System.Drawing.Point(169, 32);
            this.numericUpDown_NumberOfRefinements.Name = "numericUpDown_NumberOfRefinements";
            this.numericUpDown_NumberOfRefinements.Size = new System.Drawing.Size(48, 22);
            this.numericUpDown_NumberOfRefinements.TabIndex = 0;
            // 
            // groupBox_MeshConstraints
            // 
            this.groupBox_MeshConstraints.Controls.Add(this.label5);
            this.groupBox_MeshConstraints.Controls.Add(this.label6);
            this.groupBox_MeshConstraints.Controls.Add(this.textBox_MaxAngle);
            this.groupBox_MeshConstraints.Controls.Add(this.label4);
            this.groupBox_MeshConstraints.Controls.Add(this.label3);
            this.groupBox_MeshConstraints.Controls.Add(this.textBox_MinAngle);
            this.groupBox_MeshConstraints.Location = new System.Drawing.Point(12, 307);
            this.groupBox_MeshConstraints.Name = "groupBox_MeshConstraints";
            this.groupBox_MeshConstraints.Size = new System.Drawing.Size(232, 100);
            this.groupBox_MeshConstraints.TabIndex = 2;
            this.groupBox_MeshConstraints.TabStop = false;
            this.groupBox_MeshConstraints.Text = "Mesh Constraints";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(155, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "deg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Max Angle";
            // 
            // textBox_MaxAngle
            // 
            this.textBox_MaxAngle.Location = new System.Drawing.Point(82, 60);
            this.textBox_MaxAngle.Name = "textBox_MaxAngle";
            this.textBox_MaxAngle.Size = new System.Drawing.Size(67, 22);
            this.textBox_MaxAngle.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(155, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "deg";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Min Angle";
            // 
            // textBox_MinAngle
            // 
            this.textBox_MinAngle.Location = new System.Drawing.Point(82, 32);
            this.textBox_MinAngle.Name = "textBox_MinAngle";
            this.textBox_MinAngle.Size = new System.Drawing.Size(67, 22);
            this.textBox_MinAngle.TabIndex = 0;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(229, 430);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 30);
            this.button_Cancel.TabIndex = 3;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(148, 430);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 30);
            this.button_OK.TabIndex = 4;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // TriangleMeshOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 478);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.groupBox_MeshConstraints);
            this.Controls.Add(this.groupBox_Refinement);
            this.Controls.Add(this.groupBox_MeshOptions);
            this.MaximumSize = new System.Drawing.Size(335, 525);
            this.MinimumSize = new System.Drawing.Size(335, 525);
            this.Name = "TriangleMeshOptions";
            this.Text = "Triangle Mesh Options";
            this.Load += new System.EventHandler(this.TriangleMeshOptions_Load);
            this.groupBox_MeshOptions.ResumeLayout(false);
            this.groupBox_MeshOptions.PerformLayout();
            this.groupBox_Refinement.ResumeLayout(false);
            this.groupBox_Refinement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumberOfSmooth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_NumberOfRefinements)).EndInit();
            this.groupBox_MeshConstraints.ResumeLayout(false);
            this.groupBox_MeshConstraints.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_MeshOptions;
        private System.Windows.Forms.CheckBox checkBox_Convex;
        private System.Windows.Forms.CheckBox checkBox_ConformingDelaunay;
        private System.Windows.Forms.CheckBox checkBox_QualityMesh;
        private System.Windows.Forms.CheckBox checkBox_SweepLine;
        private System.Windows.Forms.GroupBox groupBox_Refinement;
        private System.Windows.Forms.NumericUpDown numericUpDown_NumberOfSmooth;
        private System.Windows.Forms.NumericUpDown numericUpDown_NumberOfRefinements;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_RefineAfterSmooth;
        private System.Windows.Forms.GroupBox groupBox_MeshConstraints;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_MaxAngle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_MinAngle;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
    }
}