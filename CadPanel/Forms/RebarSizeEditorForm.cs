﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ReinforcedConcrete.Reinforcement;

namespace CadPanel.Forms
{
    public partial class RebarSizeEditorForm : Form
    {
        #region Private Fields
        private List<RebarDefinition> _rebarSizes;
        private List<RebarDefinition> _defaultRebarSizes;

        private List<int> _selectedRows = new List<int>();
        #endregion

        #region Public Properties
        public bool AcceptedChanges { get; protected set; } = false;

        public List<RebarDefinition> TempRebarSizes
        {
            get
            {
                return _rebarSizes;
            }
        }
        #endregion

        public RebarSizeEditorForm(List<RebarDefinition> rebarSizes, List<RebarDefinition> defaultRebarSizes)
        {
            InitializeComponent();

            _rebarSizes = new List<RebarDefinition>(rebarSizes); //Create a copy of the list

            _defaultRebarSizes = new List<RebarDefinition>(defaultRebarSizes);
        }

        private void RebarSizeEditorForm_Load(object sender, EventArgs e)
        {
            dataGridView_Rebar.CellClick += new DataGridViewCellEventHandler(DataGridView_Rebar_CellContentClicked);

            dataGridView_Rebar.RowStateChanged += new DataGridViewRowStateChangedEventHandler(DataGridView_Rebar_RowStateChanged);

            this.Resize += new EventHandler(RebarSizeEditor_Resize);

            if (_rebarSizes != null && _rebarSizes.Count > 0)
            {
                for (int i = 0; i< _rebarSizes.Count; i++)
                {
                    DataGridViewRow row = new DataGridViewRow();
                    row.CreateCells(dataGridView_Rebar);
                    row.Cells[0].Value = _rebarSizes[i].Name;
                    row.Cells[1].Value = _rebarSizes[i].Diameter;
                    row.Cells[2].Value = _rebarSizes[i].Area;

                    dataGridView_Rebar.Rows.Add(row);
                }
            }

            ResizeAllComponents();
        }

        private void RebarSizeEditorForm_Resize(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_Accept_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < dataGridView_Rebar.Rows.Count - 1; i++)
                {
                    string name1 = Convert.ToString(dataGridView_Rebar[0, i].Value);

                    for (int j = i + 1; j < dataGridView_Rebar.Rows.Count; j++)
                    {
                        string name2 = Convert.ToString(dataGridView_Rebar[0, j].Value);

                        if (string.Compare(name1, name2, ignoreCase: true) == 0)
                        {
                            throw new Exception($"The name '{name1}' is used more than once.  All names must be unique.");
                        }
                    }
                }

                this.DialogResult = DialogResult.OK;

                for (int i = 0; i < dataGridView_Rebar.Rows.Count; i++)
                {
                    _rebarSizes[i].Name = Convert.ToString(dataGridView_Rebar[0, i].Value);
                    _rebarSizes[i].Diameter = Convert.ToDouble(dataGridView_Rebar[1, i].Value);
                    _rebarSizes[i].Area = Convert.ToDouble(dataGridView_Rebar[2, i].Value);
                }

                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("There was an error attempting to save the rebar definitions:" + Environment.NewLine + Environment.NewLine + ex.Message, "ERROR", MessageBoxButtons.OK);
            }  
        }

        private void DataGridView_Rebar_CellContentClicked(object sender, DataGridViewCellEventArgs e)
        {
            int r = e.RowIndex;

            _selectedRows.Clear();

            _selectedRows.Add(r);
        }

        private void DataGridView_Rebar_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged != DataGridViewElementStates.Selected)
            {
                return;
            }
            else
            {
                DataGridView dgv = (DataGridView)sender;

                _selectedRows.Clear();

                DataGridViewSelectedRowCollection selRows = dgv.SelectedRows;

                foreach (DataGridViewRow r in selRows)
                {
                    _selectedRows.Add(r.Index);
                }
            }
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (_selectedRows != null)
            {
                if (_selectedRows.Count > 0)
                {
                    _selectedRows = _selectedRows.OrderByDescending(x => x).ToList();

                    for (int i = 0; i < _selectedRows.Count; i++)
                    {
                        dataGridView_Rebar.Rows.RemoveAt(_selectedRows[i]);

                        _rebarSizes.RemoveAt(_selectedRows[i]);
                    }

                    _selectedRows.Clear();
                }
            }
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            List<string> currentNames = new List<string>(dataGridView_Rebar.Rows.Count);

            for (int i = 0; i < dataGridView_Rebar.Rows.Count; i++)
            {
                currentNames.Add(Convert.ToString(dataGridView_Rebar[0, i].Value));
            }

            AddNewRebarForm frm = new AddNewRebarForm(currentNames);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _rebarSizes.Add(frm.NewRebarDefinition);

                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView_Rebar);
                row.Cells[0].Value = _rebarSizes[_rebarSizes.Count - 1].Name;
                row.Cells[1].Value = _rebarSizes[_rebarSizes.Count - 1].Diameter;
                row.Cells[2].Value = _rebarSizes[_rebarSizes.Count - 1].Area;

                dataGridView_Rebar.Rows.Add(row);
            }
        }

        private void ResizeAllComponents()
        {
            button_Cancel.Location = new Point(this.Width - 30 - button_Cancel.Width, this.Height - 80);

            button_Accept.Location = new Point(button_Cancel.Location.X - 5 - button_Accept.Width, button_Cancel.Location.Y);

            button_Reset.Location = new Point(this.Width - 30 - button_Reset.Width, dataGridView_Rebar.Location.Y);

            button_Add.Location = new Point(button_Reset.Location.X, button_Reset.Location.Y + button_Reset.Height + 5);

            button_Delete.Location = new Point(button_Reset.Location.X, button_Add.Location.Y + button_Add.Height + 5);

            dataGridView_Rebar.Width = button_Reset.Location.X - 5 - dataGridView_Rebar.Location.X;

            dataGridView_Rebar.Height = button_Cancel.Location.Y - 20 - dataGridView_Rebar.Location.Y;
        }

        private void RebarSizeEditor_Resize(object sender, EventArgs e)
        {
            ResizeAllComponents();
        }

        private void ResetToDefault()
        {
            dataGridView_Rebar.Rows.Clear();

            _rebarSizes.Clear();

            for (int i = 0; i < _defaultRebarSizes.Count; i++)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGridView_Rebar);
                row.Cells[0].Value = _defaultRebarSizes[i].Name;
                row.Cells[1].Value = _defaultRebarSizes[i].Diameter;
                row.Cells[2].Value = _defaultRebarSizes[i].Area;

                dataGridView_Rebar.Rows.Add(row);

                _rebarSizes.Add(_defaultRebarSizes[i]);
            }
        }

        private void Button_Reset_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will reset the rebar to the default list.  Are you sure you want to continue?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                ResetToDefault();
            }
        }
    }
}
