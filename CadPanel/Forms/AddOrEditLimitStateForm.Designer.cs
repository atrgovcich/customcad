﻿namespace CadPanel.Forms
{
    partial class AddOrEditLimitStateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_Material = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_Compression = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_Tension = new System.Windows.Forms.TextBox();
            this.checkBox_Termination = new System.Windows.Forms.CheckBox();
            this.panel_Chart = new System.Windows.Forms.Panel();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox_Material
            // 
            this.comboBox_Material.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Material.FormattingEnabled = true;
            this.comboBox_Material.Location = new System.Drawing.Point(12, 65);
            this.comboBox_Material.Name = "comboBox_Material";
            this.comboBox_Material.Size = new System.Drawing.Size(228, 21);
            this.comboBox_Material.TabIndex = 0;
            this.comboBox_Material.SelectedIndexChanged += new System.EventHandler(this.comboBox_Material_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Material";
            // 
            // textBox_Name
            // 
            this.textBox_Name.Location = new System.Drawing.Point(12, 26);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(228, 20);
            this.textBox_Name.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Define Name";
            // 
            // textBox_Compression
            // 
            this.textBox_Compression.Location = new System.Drawing.Point(15, 105);
            this.textBox_Compression.Name = "textBox_Compression";
            this.textBox_Compression.Size = new System.Drawing.Size(100, 20);
            this.textBox_Compression.TabIndex = 4;
            this.textBox_Compression.TextChanged += new System.EventHandler(this.textBox_Compression_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Compression Strain";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(137, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Tension Strain";
            // 
            // textBox_Tension
            // 
            this.textBox_Tension.Location = new System.Drawing.Point(140, 105);
            this.textBox_Tension.Name = "textBox_Tension";
            this.textBox_Tension.Size = new System.Drawing.Size(100, 20);
            this.textBox_Tension.TabIndex = 6;
            this.textBox_Tension.TextChanged += new System.EventHandler(this.textBox_Tension_TextChanged);
            // 
            // checkBox_Termination
            // 
            this.checkBox_Termination.AutoSize = true;
            this.checkBox_Termination.Location = new System.Drawing.Point(15, 131);
            this.checkBox_Termination.Name = "checkBox_Termination";
            this.checkBox_Termination.Size = new System.Drawing.Size(127, 17);
            this.checkBox_Termination.TabIndex = 8;
            this.checkBox_Termination.Text = "Termination limit state";
            this.checkBox_Termination.UseVisualStyleBackColor = true;
            // 
            // panel_Chart
            // 
            this.panel_Chart.Location = new System.Drawing.Point(267, 26);
            this.panel_Chart.Name = "panel_Chart";
            this.panel_Chart.Size = new System.Drawing.Size(409, 329);
            this.panel_Chart.TabIndex = 9;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(601, 415);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 10;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(520, 415);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 11;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // AddOrEditLimitStateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 450);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.panel_Chart);
            this.Controls.Add(this.checkBox_Termination);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_Tension);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_Compression);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_Name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox_Material);
            this.Name = "AddOrEditLimitStateForm";
            this.Text = "Add or Edit Limit State";
            this.Load += new System.EventHandler(this.AddOrEditLimitStateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_Material;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_Compression;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_Tension;
        private System.Windows.Forms.CheckBox checkBox_Termination;
        private System.Windows.Forms.Panel panel_Chart;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
    }
}