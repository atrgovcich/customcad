﻿namespace CadPanel.Forms
{
    partial class SnappingOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox_PointSnapsOn = new System.Windows.Forms.CheckBox();
            this.groupBox_PointSnaps = new System.Windows.Forms.GroupBox();
            this.checkBox_ApparentOrthoIntersections = new System.Windows.Forms.CheckBox();
            this.checkBox_Intersections = new System.Windows.Forms.CheckBox();
            this.checkBox_Perpendicular = new System.Windows.Forms.CheckBox();
            this.checkBox_QuadrantPoints = new System.Windows.Forms.CheckBox();
            this.checkBox_CenterPoints = new System.Windows.Forms.CheckBox();
            this.checkBox_MidPoints = new System.Windows.Forms.CheckBox();
            this.checkBox_EndPoints = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_SnapDistance = new System.Windows.Forms.TextBox();
            this.groupBox_OrthoSnaps = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_SnapAngleFuzz = new System.Windows.Forms.TextBox();
            this.textBox_SnapAngleInterval = new System.Windows.Forms.TextBox();
            this.checkBox_OrthoSnapsOn = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.groupBox_PointSnaps.SuspendLayout();
            this.groupBox_OrthoSnaps.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox_PointSnapsOn
            // 
            this.checkBox_PointSnapsOn.AutoSize = true;
            this.checkBox_PointSnapsOn.Location = new System.Drawing.Point(12, 15);
            this.checkBox_PointSnapsOn.Name = "checkBox_PointSnapsOn";
            this.checkBox_PointSnapsOn.Size = new System.Drawing.Size(100, 17);
            this.checkBox_PointSnapsOn.TabIndex = 0;
            this.checkBox_PointSnapsOn.Text = "Point Snaps On";
            this.checkBox_PointSnapsOn.UseVisualStyleBackColor = true;
            this.checkBox_PointSnapsOn.CheckedChanged += new System.EventHandler(this.checkBox_PointSnapsOn_CheckedChanged);
            // 
            // groupBox_PointSnaps
            // 
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_ApparentOrthoIntersections);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_Intersections);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_Perpendicular);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_QuadrantPoints);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_CenterPoints);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_MidPoints);
            this.groupBox_PointSnaps.Controls.Add(this.checkBox_EndPoints);
            this.groupBox_PointSnaps.Controls.Add(this.label1);
            this.groupBox_PointSnaps.Controls.Add(this.textBox_SnapDistance);
            this.groupBox_PointSnaps.Location = new System.Drawing.Point(12, 38);
            this.groupBox_PointSnaps.Name = "groupBox_PointSnaps";
            this.groupBox_PointSnaps.Size = new System.Drawing.Size(299, 174);
            this.groupBox_PointSnaps.TabIndex = 1;
            this.groupBox_PointSnaps.TabStop = false;
            this.groupBox_PointSnaps.Text = "Point Snap Toggles";
            // 
            // checkBox_ApparentOrthoIntersections
            // 
            this.checkBox_ApparentOrthoIntersections.AutoSize = true;
            this.checkBox_ApparentOrthoIntersections.Location = new System.Drawing.Point(134, 42);
            this.checkBox_ApparentOrthoIntersections.Name = "checkBox_ApparentOrthoIntersections";
            this.checkBox_ApparentOrthoIntersections.Size = new System.Drawing.Size(161, 17);
            this.checkBox_ApparentOrthoIntersections.TabIndex = 9;
            this.checkBox_ApparentOrthoIntersections.Text = "Apparent Ortho Intersections";
            this.checkBox_ApparentOrthoIntersections.UseVisualStyleBackColor = true;
            // 
            // checkBox_Intersections
            // 
            this.checkBox_Intersections.AutoSize = true;
            this.checkBox_Intersections.Location = new System.Drawing.Point(134, 19);
            this.checkBox_Intersections.Name = "checkBox_Intersections";
            this.checkBox_Intersections.Size = new System.Drawing.Size(86, 17);
            this.checkBox_Intersections.TabIndex = 8;
            this.checkBox_Intersections.Text = "Intersections";
            this.checkBox_Intersections.UseVisualStyleBackColor = true;
            // 
            // checkBox_Perpendicular
            // 
            this.checkBox_Perpendicular.AutoSize = true;
            this.checkBox_Perpendicular.Location = new System.Drawing.Point(9, 111);
            this.checkBox_Perpendicular.Name = "checkBox_Perpendicular";
            this.checkBox_Perpendicular.Size = new System.Drawing.Size(91, 17);
            this.checkBox_Perpendicular.TabIndex = 7;
            this.checkBox_Perpendicular.Text = "Perpendicular";
            this.checkBox_Perpendicular.UseVisualStyleBackColor = true;
            // 
            // checkBox_QuadrantPoints
            // 
            this.checkBox_QuadrantPoints.AutoSize = true;
            this.checkBox_QuadrantPoints.Location = new System.Drawing.Point(9, 88);
            this.checkBox_QuadrantPoints.Name = "checkBox_QuadrantPoints";
            this.checkBox_QuadrantPoints.Size = new System.Drawing.Size(102, 17);
            this.checkBox_QuadrantPoints.TabIndex = 6;
            this.checkBox_QuadrantPoints.Text = "Quadrant Points";
            this.checkBox_QuadrantPoints.UseVisualStyleBackColor = true;
            // 
            // checkBox_CenterPoints
            // 
            this.checkBox_CenterPoints.AutoSize = true;
            this.checkBox_CenterPoints.Location = new System.Drawing.Point(9, 65);
            this.checkBox_CenterPoints.Name = "checkBox_CenterPoints";
            this.checkBox_CenterPoints.Size = new System.Drawing.Size(89, 17);
            this.checkBox_CenterPoints.TabIndex = 5;
            this.checkBox_CenterPoints.Text = "Center Points";
            this.checkBox_CenterPoints.UseVisualStyleBackColor = true;
            // 
            // checkBox_MidPoints
            // 
            this.checkBox_MidPoints.AutoSize = true;
            this.checkBox_MidPoints.Location = new System.Drawing.Point(9, 42);
            this.checkBox_MidPoints.Name = "checkBox_MidPoints";
            this.checkBox_MidPoints.Size = new System.Drawing.Size(75, 17);
            this.checkBox_MidPoints.TabIndex = 4;
            this.checkBox_MidPoints.Text = "Mid Points";
            this.checkBox_MidPoints.UseVisualStyleBackColor = true;
            // 
            // checkBox_EndPoints
            // 
            this.checkBox_EndPoints.AutoSize = true;
            this.checkBox_EndPoints.Location = new System.Drawing.Point(9, 19);
            this.checkBox_EndPoints.Name = "checkBox_EndPoints";
            this.checkBox_EndPoints.Size = new System.Drawing.Size(77, 17);
            this.checkBox_EndPoints.TabIndex = 3;
            this.checkBox_EndPoints.Text = "End Points";
            this.checkBox_EndPoints.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Snap Distance = ";
            // 
            // textBox_SnapDistance
            // 
            this.textBox_SnapDistance.Location = new System.Drawing.Point(101, 139);
            this.textBox_SnapDistance.Name = "textBox_SnapDistance";
            this.textBox_SnapDistance.Size = new System.Drawing.Size(58, 20);
            this.textBox_SnapDistance.TabIndex = 2;
            // 
            // groupBox_OrthoSnaps
            // 
            this.groupBox_OrthoSnaps.Controls.Add(this.label5);
            this.groupBox_OrthoSnaps.Controls.Add(this.label4);
            this.groupBox_OrthoSnaps.Controls.Add(this.label2);
            this.groupBox_OrthoSnaps.Controls.Add(this.textBox_SnapAngleFuzz);
            this.groupBox_OrthoSnaps.Controls.Add(this.textBox_SnapAngleInterval);
            this.groupBox_OrthoSnaps.Location = new System.Drawing.Point(12, 254);
            this.groupBox_OrthoSnaps.Name = "groupBox_OrthoSnaps";
            this.groupBox_OrthoSnaps.Size = new System.Drawing.Size(202, 84);
            this.groupBox_OrthoSnaps.TabIndex = 3;
            this.groupBox_OrthoSnaps.TabStop = false;
            this.groupBox_OrthoSnaps.Text = "Ortho Snap Options";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Snap Interval = ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(172, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "deg";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Snap Angle Fuzz = ";
            // 
            // textBox_SnapAngleFuzz
            // 
            this.textBox_SnapAngleFuzz.Location = new System.Drawing.Point(108, 23);
            this.textBox_SnapAngleFuzz.Name = "textBox_SnapAngleFuzz";
            this.textBox_SnapAngleFuzz.Size = new System.Drawing.Size(58, 20);
            this.textBox_SnapAngleFuzz.TabIndex = 2;
            // 
            // textBox_SnapAngleInterval
            // 
            this.textBox_SnapAngleInterval.Location = new System.Drawing.Point(108, 49);
            this.textBox_SnapAngleInterval.Name = "textBox_SnapAngleInterval";
            this.textBox_SnapAngleInterval.Size = new System.Drawing.Size(58, 20);
            this.textBox_SnapAngleInterval.TabIndex = 5;
            // 
            // checkBox_OrthoSnapsOn
            // 
            this.checkBox_OrthoSnapsOn.AutoSize = true;
            this.checkBox_OrthoSnapsOn.Location = new System.Drawing.Point(12, 231);
            this.checkBox_OrthoSnapsOn.Name = "checkBox_OrthoSnapsOn";
            this.checkBox_OrthoSnapsOn.Size = new System.Drawing.Size(102, 17);
            this.checkBox_OrthoSnapsOn.TabIndex = 2;
            this.checkBox_OrthoSnapsOn.Text = "Ortho Snaps On";
            this.checkBox_OrthoSnapsOn.UseVisualStyleBackColor = true;
            this.checkBox_OrthoSnapsOn.CheckedChanged += new System.EventHandler(this.checkBox_OrthoSnapsOn_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 280);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "deg";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(238, 367);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 4;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(157, 367);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 5;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.button_OK_Click);
            // 
            // SnappingOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 402);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox_OrthoSnaps);
            this.Controls.Add(this.checkBox_OrthoSnapsOn);
            this.Controls.Add(this.groupBox_PointSnaps);
            this.Controls.Add(this.checkBox_PointSnapsOn);
            this.MaximumSize = new System.Drawing.Size(340, 440);
            this.MinimumSize = new System.Drawing.Size(340, 440);
            this.Name = "SnappingOptionsForm";
            this.Text = "Snapping Options";
            this.Load += new System.EventHandler(this.SnappingOptions_Load);
            this.groupBox_PointSnaps.ResumeLayout(false);
            this.groupBox_PointSnaps.PerformLayout();
            this.groupBox_OrthoSnaps.ResumeLayout(false);
            this.groupBox_OrthoSnaps.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox_PointSnapsOn;
        private System.Windows.Forms.GroupBox groupBox_PointSnaps;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_SnapDistance;
        private System.Windows.Forms.GroupBox groupBox_OrthoSnaps;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_SnapAngleFuzz;
        private System.Windows.Forms.CheckBox checkBox_OrthoSnapsOn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_SnapAngleInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.CheckBox checkBox_ApparentOrthoIntersections;
        private System.Windows.Forms.CheckBox checkBox_Intersections;
        private System.Windows.Forms.CheckBox checkBox_Perpendicular;
        private System.Windows.Forms.CheckBox checkBox_QuadrantPoints;
        private System.Windows.Forms.CheckBox checkBox_CenterPoints;
        private System.Windows.Forms.CheckBox checkBox_MidPoints;
        private System.Windows.Forms.CheckBox checkBox_EndPoints;
    }
}