﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static System.Math;
using ReinforcedConcrete.Reinforcement;

namespace CadPanel.Forms
{
    public partial class AddNewRebarForm : Form
    {
        private RebarDefinition _newRebarDef;

        private List<string> _existingNames = new List<string>();

        public RebarDefinition NewRebarDefinition
        {
            get
            {
                return _newRebarDef;
            }
        }
        public AddNewRebarForm(List<string> existingNames)
        {
            InitializeComponent();

            _existingNames = existingNames;
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            try
            {
                string name = textBox_Name.Text;

                if (string.IsNullOrWhiteSpace(name) == true)
                {
                    throw new Exception("A name must be provided.");
                }
                else
                {
                    for (int i = 0; i < _existingNames.Count; i++)
                    {
                        if (string.Compare(_existingNames[i], name, ignoreCase: true) == 0)
                        {
                            throw new Exception($"Name must be unique.  The name {name} is already in use.");
                        }
                    }
                }

                if (double.TryParse(textBox_Diameter.Text, out double diameter) == false)
                {
                    throw new Exception("The diameter must be numeric");
                }

                if (double.TryParse(textBox_Area.Text, out double area) == false)
                {
                    throw new Exception("The area must be numeric");
                }

                if (diameter <= 0)
                {
                    throw new Exception("The diameter must be greater than zero");
                }

                if (area <= 0)
                {
                    throw new Exception("The area must be greater than zero");
                }

                _newRebarDef = new RebarDefinition() { Name = name, Diameter = diameter, Area = area };

                this.DialogResult = DialogResult.OK;

                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("There was an error while attempting to add a new rebar defintion" + Environment.NewLine + Environment.NewLine + ex.Message, "ERROR", MessageBoxButtons.OK);
            }
        }

        private void Button_CalcArea_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_Diameter.Text, out double diam) == true)
            {
                double area = PI / 4.0 * diam * diam;

                textBox_Area.Text = Convert.ToString(Round(area, 3));
            }
        }

        private void AddNewRebarForm_Load(object sender, EventArgs e)
        {

        }
    }
}
