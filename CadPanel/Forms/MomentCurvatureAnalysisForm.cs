﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using FiberSectionAnalysis.MomentCurvatureAnalysis;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.LimitStates;
using FiberSectionAnalysis.Elements;
using CadPanel.DrawingElements;
using System.Runtime.InteropServices;
using Utilities.Charting;
using OxyPlot.WindowsForms;
using Materials.Inelastic.Concrete;
using System.Linq;
using Utilities.Extensions.DotNetNative;
using Materials;
using Utilities.Geometry;
using static System.Math;
using CadPanel.Common;
using Utilities.Graphics;
using FiberSectionAnalysis.Results;
using Utilities.Containers;
using System.Text;

namespace CadPanel.Forms
{
    public partial class MomentCurvatureAnalysisForm : Form
    {
        #region Private Form Properties
        private int _minFormWidth = 1160;
        private int _minFormHeight = 750;
        #endregion

        #region Drawing Suspension Static
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;
        #endregion

        #region Private Fields
        private List<MomentCurvatureAnalysisCase> _mcCases;

        private bool _suspendAutoPopulation = false;

        List<IGenericMaterial> _allMaterials;

        private MomentCurvatureAnalysisCase _selectedAnalysisCase = null;

        private List<IDrawingElement> _selectedElements = null;

        private int _precision;

        private List<LimitState> _allLimitStates;

        private Chart2D _chart = null;

        private Panel _chartPanel = null;

        private int _chartWidth = 400;
        private int _chartHeight = 300;

        private PlotView _plotView;

        private string _defaultLimitStateLabelString = "No limit states defined";

        private string _defaultLoadsLabelString = "No axial forces defined";

        private bool _displayReduced = false;

        private Dictionary<IDrawingElement, IFiberSectionElement> _elementReferences = new Dictionary<IDrawingElement, IFiberSectionElement>();
        #endregion

        private CadPanelControl _cadPanel;

        public MomentCurvatureAnalysisForm(List<MomentCurvatureAnalysisCase> mcCases, List<IDrawingElement> selectedElements, int precision, List<LimitState> allLimitStates, List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _mcCases = mcCases;
            _precision = precision;
            _allLimitStates = allLimitStates;
            _selectedElements = selectedElements;
            _allMaterials = allMaterials;

        }

        private void MomentCurvatureAnalysisForm_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(_minFormWidth, _minFormHeight);

            this.Resize += new EventHandler(MomentCurvatureAnalysisForm_Resize);

            this.Size = new Size(_minFormWidth, _minFormHeight);

            InitializeCadPanel();

            InitializeChart();

            ResizeControls();

            DisableAllBoxes();

            PopulateAnalysisCaseComboBox();

            if (_mcCases != null && _mcCases.Count > 0)
            {
                comboBox_AnalysisCases.SelectedIndex = 0;

                EnableAllBoxes();
            }

            _cadPanel.SelectedElements.ItemAdded += new EventHandler<EventListArgs<IDrawingElement>>(CadPanelSelectedElements_SelectedItemsChanged);
            _cadPanel.SelectedElements.ItemRemoved += new EventHandler<EventListArgs<IDrawingElement>>(CadPanelSelectedElements_SelectedItemsChanged);

            _cadPanel.ZoomExtents();
        }

        private void InitializeChart()
        {
            _chartWidth = splitContainer_Main.Panel2.Width / 2 - 15;

            _chartHeight = splitContainer_Main.Panel2.Height / 2 - 15;

            _chartPanel = new Panel()
            {
                Location = new System.Drawing.Point(10, 10),
                BorderStyle = BorderStyle.Fixed3D
            };

            _chartPanel.Size = new Size(_chartWidth, _chartHeight);

            _plotView = new PlotView();

            _plotView.Location = new System.Drawing.Point(0, 0);

            _plotView.Width = _chartWidth;

            _plotView.Height = _chartHeight;

            _chartPanel.Controls.Add(_plotView);

            splitContainer_Main.Panel2.Controls.Add(_chartPanel);
        }
        private void InitializeCadPanel()
        {
            _cadPanel = new CadPanelControl();
            _cadPanel.Name = "CadPanel";
            //_cadPanel.MinimumSize = new Size(10, 10);

            Point location = new Point(splitContainer_Main.Panel2.Width / 2 + 5, 10);

            _cadPanel.Location = location;
            _cadPanel.Size = new Size(splitContainer_Main.Panel2.Width / 2 - 15, splitContainer_Main.Panel2.Height / 2 - 15);

            _cadPanel.AllowKeyboardCommands = false;
            _cadPanel.ShowPropertyPanel = false;
            _cadPanel.AllowDrawing = false;
            
            splitContainer_Main.Panel2.Controls.Add(_cadPanel);
        }

        private void PopulateAnalysisCaseComboBox()
        {
            if (_mcCases != null)
            {
                comboBox_AnalysisCases.Items.Clear();

                for (int i = 0; i < _mcCases.Count; i++)
                {
                    comboBox_AnalysisCases.Items.Add(_mcCases[i].Name);
                }
            }
        }

        #region Paint Overrides

        //public bool TransparentBackground { get; set; } = false;
        //public bool ClipChildControls { get; set; } = true;

        //protected override void OnPaint(PaintEventArgs pe)
        //{
        //    base.OnPaint(pe);
        //}

        //protected override void OnPaintBackground(PaintEventArgs pevent)
        //{
        //    if (TransparentBackground == true)
        //    {
        //        base.OnPaintBackground(pevent);
        //    }
        //    else
        //    {
        //        if (ClipChildControls == false)
        //        {
        //            base.OnPaintBackground(pevent);
        //        }
        //        else
        //        {
        //            if (BackgroundImage != null)
        //            {
        //                base.OnPaintBackground(pevent);
        //            }
        //            else
        //            {
        //                Contract.Requires(pevent != null);
        //                // We need the true client rectangle as clip rectangle causes
        //                // problems on "Windows Classic" theme.  
        //                Rectangle rect = this.ClientRectangle;
        //                PaintBackgroundNoChildren(pevent, rect, BackColor);
        //            }
        //        }
        //    }
        //}

        //protected void PaintBackgroundNoChildren(PaintEventArgs e, Rectangle rectangle, Color backColor)
        //{
        //    List<Rectangle> clippedRectangles = ChildContrlsClientRectangles();

        //    PaintClippedBackground(e, rectangle, clippedRectangles, backColor);
        //}

        //protected void PaintClippedBackground(PaintEventArgs e, Rectangle clientRectangle, List<Rectangle> clippedRectangles, Color backColor)
        //{
        //    // Common case of just painting the background.  For this, we
        //    // use GDI because it is faster for simple things than creating
        //    // a graphics object, brush, etc.  Also, we may be able to
        //    // use a system brush, avoiding the brush create altogether.
        //    //
        //    Color color = backColor;


        //    GraphicsPath gp = new GraphicsPath();
        //    gp.AddRectangle(clientRectangle);
        //    Region r = new Region(gp);

        //    foreach (Rectangle hole in clippedRectangles)
        //    {
        //        var gpe = new GraphicsPath();
        //        gpe.AddRectangle(hole);
        //        r.Exclude(gpe);
        //        gpe.Dispose();
        //    }

        //    gp.Dispose();

        //    using (Brush brush = new SolidBrush(color))
        //    {
        //        e.Graphics.FillRegion(brush, r);
        //    }
        //}

        //protected List<Rectangle> ChildContrlsClientRectangles()
        //{
        //    List<Rectangle> clientRectangles = new List<Rectangle>();

        //    foreach (Control c in Controls)
        //    {
        //        clientRectangles.Add(c.Bounds);
        //    }

        //    return clientRectangles;
        //}
        #endregion

        private void SplitContainer_Main_SplitterMoved(object sender, SplitterEventArgs e)
        {
            ResizeControls();
        }

        private void ResizeControls()
        {
            splitContainer_Main.Width = this.Width - 40;
            splitContainer_Main.Height = this.Height - 120;

            ResizeControlsInSplitPanel2();
            ResizeCloseButton();
            ResizeControlsInSplitPanel1();
        }

        private void ResizeChart()
        {
            _chartWidth = splitContainer_Main.Panel2.Width / 2 - 15;

            _chartHeight = splitContainer_Main.Panel2.Height / 2 - 15;

            _chartPanel.Size = new Size(_chartWidth, _chartHeight);

            _plotView.Size = new Size(_chartWidth, _chartHeight);
        }

        private void ResizeCadPanel()
        {
            Point location = new Point(splitContainer_Main.Panel2.Width / 2 + 5, 10);

            _cadPanel.Location = location;
            _cadPanel.Size = new Size(splitContainer_Main.Panel2.Width / 2 - 15, splitContainer_Main.Height / 2 - 15);
        }

        private void ResizeDetailedDataRichTextBox()
        {
            richTextBox_DetailedData.Location = new Point(_cadPanel.Location.X, _cadPanel.Location.Y + _cadPanel.Height + 15);
            richTextBox_DetailedData.Size = new Size(_cadPanel.Width, splitContainer_Main.Height / 2 - 20);
        }

        private void MomentCurvatureAnalysisForm_Resize(object sender, EventArgs e)
        {
            ResizeControls();
        }

        private void ResizeControlsInSplitPanel2()
        {
            ResizeCadPanel();
            ResizeChart();
            ResizeDetailedDataRichTextBox();

            groupBox_ResultsDisplay.Location = new Point(groupBox_ResultsDisplay.Location.X, splitContainer_Main.Height - groupBox_ResultsDisplay.Height - 20);
        }

        private void ResizeCloseButton()
        {
            button_Close.Location = new Point(this.Width - button_Close.Width - 30, this.Height - button_Close.Height - 50);
        }

        private void ResizeControlsInSplitPanel1()
        {
            button_Analyze.Location = new Point(splitContainer_Main.Panel1.Width - button_Analyze.Width - 20, splitContainer_Main.Height - 35);
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Control Events
        private void TextBox_MeshSize_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double meshSize) == true)
                {
                    if (meshSize > 0)
                    {
                        _selectedAnalysisCase.MeshSize = meshSize;
                    }
                }
            }
        }

        private void TextBox_NumberOfPoints_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (int.TryParse(tB.Text, out int numPoints) == true)
                {
                    if (numPoints > 0)
                    {
                        _selectedAnalysisCase.NumberOfPoints = numPoints;
                    }
                }
            }
        }

        private void TextBox_SingleAngle_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double angle) == true)
                {
                    if (angle >= 0 && angle <= 360)
                    {
                        _selectedAnalysisCase.Angle = angle;
                    }
                }
            }
        }

        private void TextBox_MaxIterations_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (int.TryParse(tB.Text, out int iter) == true)
                {
                    if (iter > 0)
                    {
                        _selectedAnalysisCase.MaxNumberOfIterations = iter;
                    }
                }
            }
        }

        private void TextBox_Tolerance_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double tolerance) == true)
                {
                    if (tolerance > 0)
                    {
                        _selectedAnalysisCase.AcceptableForceDifference = tolerance;
                    }
                }
            }
        }

        private void CheckBox_IncludePT_CheckedChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                CheckBox cB = (CheckBox)sender;

                _selectedAnalysisCase.AppliedAxialForceIncludesPT = cB.Checked;
            }

        }

        private void RadioButton_ClosedHoops_CheckedChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                RadioButton thisRadioButton = (RadioButton)sender;

                if (thisRadioButton.Checked == true)
                {
                    _selectedAnalysisCase.ConfinementType = ConfinementType.ClosedTies;
                }
            }

        }

        private void RadioButton_Spirals_CheckedChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                RadioButton thisRadioButton = (RadioButton)sender;

                if (thisRadioButton.Checked == true)
                {
                    _selectedAnalysisCase.ConfinementType = ConfinementType.Spiral;
                }
            }
        }

        private void ComboBox_AnalysisCases_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                if (_mcCases != null && _mcCases.Count > 0)
                {
                    ComboBox cB = (ComboBox)sender;

                    if (cB.SelectedIndex >= 0)
                    {
                        _selectedAnalysisCase = GetAnalysisCaseFromName(cB.Text);

                        PopulateAnalysisCaseInformation(cB.Text);

                        EnableAllBoxes();

                        AddElementsToCadPanel();

                        PopulateAnalysisResultsComboBox();

                        SetNumericUpDown();
                    }
                    else
                    {
                        _selectedAnalysisCase = null;

                        DisableAllBoxes();
                    }
                }
                else
                {
                    _selectedAnalysisCase = null;

                    DisableAllBoxes();
                }
            }
        }

        private void Button_Analyze_Click(object sender, EventArgs e)
        {
            MomentCurvatureSuite analysisSuite = CreateAnalysisSuite(_selectedAnalysisCase.MeshSize);

            analysisSuite.AnalyzeSuite();

            _selectedAnalysisCase.AnalysisSuite = analysisSuite;

            AddElementsToCadPanel();

            PopulateAnalysisResultsComboBox();

            _cadPanel.ZoomExtents();
        }

        private void Button_Loads_Click(object sender, EventArgs e)
        {
            DefineMomentCurvatureLoadsForm frm = new DefineMomentCurvatureLoadsForm(_selectedAnalysisCase.AppliedAxialForces);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _selectedAnalysisCase.AppliedAxialForces = frm.Loads;

                if (_selectedAnalysisCase.AppliedAxialForces != null && _selectedAnalysisCase.AppliedAxialForces.Count > 0)
                {
                    label_Loads.Text = $"There are {_selectedAnalysisCase.AppliedAxialForces.Count} axial forces defined";
                }
                else
                {
                    label_Loads.Text = _defaultLoadsLabelString;
                }
            }

            frm.Dispose();
        }

        private void CheckBox_DisplayReduced_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            _displayReduced = cB.Checked;

            if (comboBox_Analysis.SelectedIndex >= 0)
            {
                PopulateMomentCurvatureDiagram(comboBox_Analysis.SelectedIndex);
            }
        }

        private void ComboBox_Analysis_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                ComboBox cB = (ComboBox)sender;

                if (cB.SelectedIndex >= 0)
                {
                    PopulateMomentCurvatureDiagram(cB.SelectedIndex);

                    SetNumericUpDown();
                }
            }
        }
        #endregion

        private void AddElementsToCadPanel()
        {
            if (_selectedAnalysisCase != null)
            {
                if (_selectedAnalysisCase.AnalysisSuite != null)
                {
                    if (_selectedAnalysisCase.AnalysisSuite.SectionModel != null)
                    {
                        Dictionary<IDrawingElement, IFiberSectionElement> elementReferences = new Dictionary<IDrawingElement, IFiberSectionElement>();

                        UnselectAllCadPanelElements();

                        _cadPanel.DeleteAllElements();

                        _elementReferences = new Dictionary<IDrawingElement, IFiberSectionElement>();

                        FiberSection sectionModel = _selectedAnalysisCase.AnalysisSuite.SectionModel;

                        if (sectionModel.FiberMeshElements != null)
                        {
                            for (int i = 0; i < sectionModel.FiberMeshElements.Count; i++)
                            {
                                PolygonG pg = ConvertGenericPolygonToPolygonG(sectionModel.FiberMeshElements[i], _cadPanel.InstanceData.Layers[0]);

                                pg.FillShape = true;

                                if (pg != null)
                                {
                                    _cadPanel.AddDrawingElement(pg);

                                    _elementReferences.Add(pg, sectionModel.FiberMeshElements[i]);
                                }
                            }
                        }

                        if (sectionModel.FiberRebarElements != null)
                        {
                            for (int i = 0; i < sectionModel.FiberRebarElements.Count; i++)
                            {
                                RebarSectionG rbs = ConvertFiberSectionRebartoRebarSectionG(sectionModel.FiberRebarElements[i], _cadPanel.InstanceData.Layers[0]);

                                if (rbs != null)
                                {
                                    rbs.FillShape = true;

                                    _cadPanel.AddDrawingElement(rbs);

                                    _elementReferences.Add(rbs, sectionModel.FiberRebarElements[i]);
                                }
                            }
                        }

                        if (sectionModel.FiberPostTensioningElements != null)
                        {
                            for (int i = 0; i < sectionModel.FiberPostTensioningElements.Count; i++)
                            {
                                PostTensioningG ptG = ConvertFiberSectionPostTensioningToPostTensioningG(sectionModel.FiberPostTensioningElements[i], _cadPanel.InstanceData.Layers[0]);

                                if (ptG != null)
                                {
                                    _cadPanel.AddDrawingElement(ptG);

                                    _elementReferences.Add(ptG, sectionModel.FiberPostTensioningElements[i]);
                                }
                            }
                        }

                        _cadPanel.CreateDraftingGraphics();
                    }
                }
                
            }
        }
        private void PopulateMomentCurvatureDiagram(int index)
        {
            if (_selectedAnalysisCase.AnalysisSuite != null)
            {
                if (_selectedAnalysisCase.AnalysisSuite.Analyses != null)
                {
                    if (_selectedAnalysisCase.AnalysisSuite.Analyses.Count > 0)
                    {
                        MomentCurvature mc = _selectedAnalysisCase.AnalysisSuite.Analyses[index];

                        _chart = new Chart2D($"Moment Curvature Diagram");

                        _chart.HorizontalAxisLabel = "Curvature (rad/in)";

                        _chart.VerticalAxisLabel = "Moment (kip-in)";

                        List<PointD> xyData = new List<PointD>();

                        List<PointD> xyDataReduced = new List<PointD>();

                        for (int i = 0; i < mc.AnalysisResults.Count; i++)
                        {
                            double curv = mc.AnalysisResults[i].Curvature;

                            double moment = mc.AnalysisResults[i].MomentMajor;

                            PointD p = new PointD(curv, moment);

                            xyData.Add(p);

                            double momentRed = mc.AnalysisResults[i].ReducedMomentMajor;

                            PointD pR = new PointD(curv, momentRed);

                            xyDataReduced.Add(pR);
                        }

                        double xMax = xyData.Select(p => p.X).Max(x => x);
                        double xMin = xyData.Select(p => p.X).Min(x => x);

                        double yMax = xyData.Select(p => p.Y).Max(x => x);
                        double yMin = xyData.Select(p => p.Y).Min(x => x);

                        _chart.AddSeriesFromXY(xyData, "MC");

                        if (_displayReduced == true)
                        {
                            _chart.AddSeriesFromXY(xyDataReduced, @"φMC", Color.Orange);
                        }

                        _chart.SetHorizontalAxisBounds(0.9 * xMin, 1.1 * xMax);
                        _chart.SetVerticalAxisBounds(0.9 * yMin, 1.1 * yMax);

                        _chart.SetInterpolation(InterpolationType.None);

                        _plotView.Model = _chart.Model;

                        _plotView.Invalidate();
                    }
                }
            }
            
        }


        private void SetNumericUpDown()
        {
            if (_selectedAnalysisCase != null)
            {
                if (_selectedAnalysisCase.AnalysisSuite != null)
                {
                    if (_selectedAnalysisCase.AnalysisSuite.Analyses != null)
                    {
                        if (comboBox_Analysis.SelectedIndex >= 0)
                        {
                            MomentCurvature mc = _selectedAnalysisCase.AnalysisSuite.Analyses[comboBox_Analysis.SelectedIndex];
                            numericUpDown_Point.Maximum = mc.AnalysisResults.Count;
                            numericUpDown_Point.Minimum = 1;
                            numericUpDown_Point.Value = 1;
                            NumericUpDown_Point_ValueChanged(numericUpDown_Point, new EventArgs());
                        }
                    }
                }
            }            
        }
        private void PopulateAnalysisResultsComboBox()
        {
            if (_suspendAutoPopulation == false)
            {
                comboBox_Analysis.Items.Clear();

                if (_selectedAnalysisCase.AnalysisSuite != null)
                {
                    if (_selectedAnalysisCase.AnalysisSuite.Analyses != null)
                    {
                        if (_selectedAnalysisCase.AnalysisSuite.Analyses.Count > 0)
                        {
                            List<MomentCurvature> analyses = _selectedAnalysisCase.AnalysisSuite.Analyses;

                            for (int i = 0; i < analyses.Count; i++)
                            {
                                comboBox_Analysis.Items.Add($"Angle = {Round(analyses[i].AnalysisAngle.ToDegrees(), 0)}, P = {Round(analyses[i].AppliedAxialForce, 0)}");
                            }

                            comboBox_Analysis.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void DisableAllBoxes()
        {
            groupBox_AnalysisOptions.Enabled = false;
            groupBox_DesignOptions.Enabled = false;
            groupBox_Loading.Enabled = false;
            button_Analyze.Enabled = false;
            groupBox_LimitStates.Enabled = false;
        }

        private void EnableAllBoxes()
        {
            groupBox_AnalysisOptions.Enabled = true;
            groupBox_DesignOptions.Enabled = true;
            groupBox_Loading.Enabled = true;
            button_Analyze.Enabled = true;
            groupBox_LimitStates.Enabled = true;
        }

        private void PopulateAnalysisCaseInformation(string caseName)
        {
            _suspendAutoPopulation = true;

            MomentCurvatureAnalysisCase mcCase = GetAnalysisCaseFromName(caseName);

            if (mcCase != null)
            {
                textBox_MeshSize.Text = Convert.ToString(mcCase.MeshSize);

                textBox_NumberOfPoints.Text = Convert.ToString(mcCase.NumberOfPoints);

                textBox_SingleAngle.Text = Convert.ToString(mcCase.Angle);

                textBox_MaxIterations.Text = Convert.ToString(mcCase.MaxNumberOfIterations);

                textBox_Tolerance.Text = Convert.ToString(mcCase.AcceptableForceDifference);

                if (mcCase.ConfinementType.Equals(ConfinementType.ClosedTies) == true)
                {
                    radioButton_ClosedHoops.Checked = true;
                }
                else
                {
                    radioButton_Spirals.Checked = true;
                }

                checkBox_IncludePT.Checked = mcCase.AppliedAxialForceIncludesPT;

                if (mcCase.LimitStates != null && mcCase.LimitStates.Count > 0)
                {
                    label_LimitStates.Text = $"There are {mcCase.LimitStates.Count} limit state(s) defined";
                }
                else
                {
                    label_LimitStates.Text = _defaultLimitStateLabelString;
                }

                if (mcCase.AppliedAxialForces != null && mcCase.AppliedAxialForces.Count > 0)
                {
                    label_Loads.Text = $"There are {mcCase.AppliedAxialForces.Count} axial force(s) defined";
                }
                else
                {
                    label_Loads.Text = _defaultLoadsLabelString;
                }

            }

            _suspendAutoPopulation = false;
        }

        private MomentCurvatureAnalysisCase GetAnalysisCaseFromName(string name)
        {
            if (_mcCases != null)
            {
                for (int i = 0; i < _mcCases.Count; i++)
                {
                    if (string.Compare(_mcCases[i].Name, name, ignoreCase: true) == 0)
                    {
                        return _mcCases[i];
                    }
                }
            }

            return null;
        }

        private void Button_NewAnalysis_Click(object sender, EventArgs e)
        {
            AddFiberSectionAnalysisCase frm = new AddFiberSectionAnalysisCase(_mcCases.Cast<GeneralAnalysisCase>().ToList());

            if (frm.ShowDialog() == DialogResult.OK)
            {
                MomentCurvatureAnalysisCase newCase = new MomentCurvatureAnalysisCase() { Name = frm.NewName };

                if (_mcCases == null)
                {
                    _mcCases = new List<MomentCurvatureAnalysisCase>();
                }

                _mcCases.Add(newCase);

                _suspendAutoPopulation = true;

                comboBox_AnalysisCases.Items.Add(newCase.Name);

                comboBox_AnalysisCases.SelectedIndex = comboBox_AnalysisCases.Items.Count - 1;

                ClearAllInformation();

                _selectedAnalysisCase = newCase;

                PopulateAnalysisCaseInformation(newCase.Name);

                _suspendAutoPopulation = false;

                EnableAllBoxes();
            }

            frm.Dispose();
        }

        private void ClearAllInformation()
        {
            textBox_MeshSize.Text = string.Empty;
            textBox_NumberOfPoints.Text = string.Empty;
            textBox_SingleAngle.Text = string.Empty;
            textBox_MaxIterations.Text = string.Empty;
            textBox_Tolerance.Text = string.Empty;

            radioButton_ClosedHoops.Checked = true;

            textBox_SingleAngle.Enabled = true;

            label_LimitStates.Text = _defaultLimitStateLabelString;

            checkBox_IncludePT.Checked = false;

            _plotView.Model = null;

            _plotView.Invalidate();

            comboBox_Analysis.Items.Clear();

            comboBox_Analysis.SelectedIndex = -1;

        }

        private MomentCurvatureSuite CreateAnalysisSuite(double meshSize)
        {
            bool containsClosedShapes = false;

            for (int i = 0; i < _selectedElements.Count; i++)
            {
                if (_selectedElements[i].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                {
                    containsClosedShapes = true;

                    break;
                }
            }

            if (containsClosedShapes == true)
            {
                FiberSection sectionModel = FiberSection.BuildFiberSectionFromListOfElements(ConvertSelectedElementsToFiberSectionShapes(_selectedElements), meshSize, _precision);

                sectionModel.Confinement = _selectedAnalysisCase.ConfinementType;

                List<double> anglesToAnalyzeRadians = new List<double>();

                anglesToAnalyzeRadians.Add(_selectedAnalysisCase.Angle.ToRadians());

                Dictionary<IGenericMaterial, List<LimitState>> limitStateDictionary = new Dictionary<IGenericMaterial, List<LimitState>>();

                for (int i = 0; i < _selectedAnalysisCase.LimitStates.Count; i++)
                {
                    LimitState ls = _selectedAnalysisCase.LimitStates[i];

                    if (limitStateDictionary.TryGetValue(ls.Material, out List<LimitState> lsValues) == true)
                    {
                        lsValues.Add(ls);
                    }
                    else
                    {
                        List<LimitState> lsList = new List<LimitState>();

                        lsList.Add(ls);

                        limitStateDictionary.Add(ls.Material, lsList);
                    }
                }

                MomentCurvatureSuite analysisSuite = new MomentCurvatureSuite(anglesToAnalyzeRadians, _selectedAnalysisCase.AppliedAxialForces)
                {
                    IncludeInitialPostTensioningForceInAxialLoad = _selectedAnalysisCase.AppliedAxialForceIncludesPT,
                    NumberOfPoints = _selectedAnalysisCase.NumberOfPoints,
                    SectionModel = sectionModel,
                    MaterialLimitStates = limitStateDictionary,
                    AxialForceConvergenceTolerance = _selectedAnalysisCase.AcceptableForceDifference,
                    MaxNumberOfIterations = _selectedAnalysisCase.MaxNumberOfIterations,
                    
                };

                return analysisSuite;
            }

            return null;
        }

        private List<GenericShape> ConvertSelectedElementsToFiberSectionShapes(List<IDrawingElement> selectedElements)
        {
            List<GenericShape> fiberSectionShapes = new List<GenericShape>();

            if (selectedElements != null)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    GenericShape shape = selectedElements[i].ConvertToFiberSectionElement();

                    if (shape != null)
                    {
                        fiberSectionShapes.Add(shape);
                    }

                }
            }

            return fiberSectionShapes;
        }

        private void Button_DefineLimitStates_Click(object sender, EventArgs e)
        {
            SelectLimitStatesForm frm = new SelectLimitStatesForm(_allLimitStates, _selectedAnalysisCase.LimitStates, _allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _selectedAnalysisCase.LimitStates = frm.SelectedLimitStates;
            }

            if (_selectedAnalysisCase.LimitStates == null)
            {
                label_LimitStates.Text = _defaultLimitStateLabelString;
            }
            else if (_selectedAnalysisCase.LimitStates.Count <= 0)
            {
                label_LimitStates.Text = _defaultLimitStateLabelString;
            }
            else
            {
                label_LimitStates.Text = $"There are {_selectedAnalysisCase.LimitStates.Count} limit states selected";
            }

            frm.Dispose();
        }

        private PolygonG ConvertGenericPolygonToPolygonG(GenericPolygon pg, Layer layer)
        {
            if (pg != null)
            {
                PolygonG pgG = new PolygonG();

                List<DrawingNode> nodeList = new List<DrawingNode>(pg.Points.Count);

                for (int i = 0; i < pg.Points.Count - 1; i++)
                {
                    DrawingNode n = new DrawingNode(pg.Points[i], NodeType.EndPoint);

                    nodeList.Add(n);
                }

                nodeList.Add(nodeList[0]);

                pgG.Nodes = nodeList;

                pgG.Layer = layer;

                return pgG;
            }
            else
            {
                return null;
            }
        }

        private RebarSectionG ConvertFiberSectionRebartoRebarSectionG(FiberSectionRebarElement rebar, Layer layer)
        {
            if (rebar != null)
            {
                if (rebar.RebarSize != null)
                {
                    RebarSectionG rbs = new RebarSectionG(rebar.RebarSize);

                    rbs.Nodes.Add(new DrawingNode(rebar.Center, NodeType.CenterPoint));

                    rbs.Layer = layer;

                    return rbs;
                }
            }

            return null;          
        }

        private PostTensioningG ConvertFiberSectionPostTensioningToPostTensioningG(FiberSectionPostTensioningElement pt, Layer layer)
        {
            if (pt != null)
            {
                if (pt.TypeOfPT != null)
                {
                    PostTensioningG ptG = new PostTensioningG(pt.Center, pt.TypeOfPT, layer, pt.InitialPrestress, pt.JackingAndGrouting);

                    return ptG;
                }
            }

            return null;
        }

        private void NumericUpDown_Point_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown nud = (NumericUpDown)sender;

            if (_suspendAutoPopulation == false)
            {
                if (comboBox_Analysis.SelectedIndex >= 0)
                {
                    if (nud.Value >= 1)
                    {
                        //UnselectAllCadPanelElements();

                        MomentCurvature mc = _selectedAnalysisCase.AnalysisSuite.Analyses[comboBox_Analysis.SelectedIndex];

                        MomentCurvatureAnalysisResult mcRes = (MomentCurvatureAnalysisResult)mc.AnalysisResults[(int)nud.Value - 1];

                        double minStrain = 0;
                        double maxStrain = 0;

                        for (int i = 0; i < mc.AnalysisResults.Count; i++)
                        {
                            MomentCurvatureAnalysisResult mcResLoc = (MomentCurvatureAnalysisResult)mc.AnalysisResults[i];

                            if (i == 0)
                            {
                                minStrain = mcResLoc.MinFiberElementStrain;
                                maxStrain = mcResLoc.MaxFiberElementStrain;
                            }
                            else
                            {
                                minStrain = Min(minStrain, mcResLoc.MinFiberElementStrain);
                                maxStrain = Max(maxStrain, mcResLoc.MaxFiberElementStrain);
                            }
                        }

                        ColorGradient meshGradient = ColorGradient.BlueGreenRedGradient(minStrain, maxStrain);

                        ColorGradient barGradient = ColorGradient.BlackPinkGradient(minStrain, maxStrain);

                        FiberSectionElementResults fsElemResults = mc.AllElementResults[(int)nud.Value - 1];

                        ColorCadPanelElementsBasedOnStrain(meshGradient, barGradient, fsElemResults);

                        UpdateDetailedDataRichTextBox();
                    }
                }
            }
        }

        private void ColorCadPanelElementsBasedOnStrain(ColorGradient meshGradient, ColorGradient barGradient, FiberSectionElementResults elemResults)
        {
            if (_cadPanel != null)
            {
                if (_cadPanel.InstanceData.DrawingElementList != null)
                {
                    for (int i = 0; i < _cadPanel.InstanceData.DrawingElementList.Count; i++)
                    {
                        IDrawingElement elem = _cadPanel.InstanceData.DrawingElementList[i];

                        if (_elementReferences.TryGetValue(elem, out IFiberSectionElement fsElem) == true)
                        {
                            if (fsElem.GetType().Equals(typeof(FiberSectionMeshElement)) == true)
                            {
                                FiberSectionMeshElement fsMeshElem = (FiberSectionMeshElement)fsElem;

                                if (elemResults.MeshElementResults.TryGetValue(fsMeshElem, out FiberElementResult res) == true)
                                {
                                    Color elemColor = meshGradient.GetColorAtValue(res.Strain);

                                    ClosedDrawingElement closedElem = (ClosedDrawingElement)elem;

                                    closedElem.FillColor = elemColor;
                                }
                            }
                            else if (fsElem.GetType().Equals(typeof(FiberSectionRebarElement)) == true)
                            {
                                FiberSectionRebarElement fsRebarElem = (FiberSectionRebarElement)fsElem;

                                if (elemResults.RebarElementResults.TryGetValue(fsRebarElem, out RebarSectionResult res) == true)
                                {
                                    Color elemColor = barGradient.GetColorAtValue(res.Strain);

                                    ClosedDrawingElement closedElem = (ClosedDrawingElement)elem;

                                    closedElem.FillColor = elemColor;
                                }
                            }
                        }
                        
                    }

                    _cadPanel.CreateDraftingGraphics();
                }
            }
        }

        private void UnselectAllCadPanelElements()
        {
            if (_cadPanel != null)
            {
                if (_cadPanel.SelectedElements != null)
                {
                    int count = _cadPanel.SelectedElements.Count;

                    while (count > 0)
                    {
                        _cadPanel.SelectedElements[0].Selected = false;
                        _cadPanel.SelectedElements.RemoveAt(0);
                        count--;
                    }

                    _cadPanel.CreateDraftingGraphics();
                }
            }
        }

        private void CadPanelSelectedElements_SelectedItemsChanged(object sender, EventListArgs<IDrawingElement> e)
        {
            UpdateDetailedDataRichTextBox();
        }

        private void UpdateDetailedDataRichTextBox()
        {
            richTextBox_DetailedData.Clear();

            if (_cadPanel.SelectedElements != null)
            {
                if (comboBox_Analysis.SelectedIndex >= 0)
                {
                    MomentCurvature mc = _selectedAnalysisCase.AnalysisSuite.Analyses[comboBox_Analysis.SelectedIndex];

                    if (numericUpDown_Point.Value >= 1)
                    {
                        FiberSectionElementResults fsElemResults = mc.AllElementResults[(int)numericUpDown_Point.Value - 1];

                        MomentCurvatureAnalysisResult fsAnalysisResult = (MomentCurvatureAnalysisResult)mc.AnalysisResults[(int)numericUpDown_Point.Value - 1];

                        if (_cadPanel.SelectedElements.Count <= 0)
                        {
                            StringBuilder sB = new StringBuilder();

                            sB.AppendLine($"Curvature = {Round(fsAnalysisResult.Curvature, 5)} rad/in");
                            sB.AppendLine($"Applied Axial Force = {Round(fsAnalysisResult.AppliedAxialForce, 2)} kips");
                            sB.AppendLine($"Computed Axial Force = {Round(fsAnalysisResult.AxialForce, 2)} kips");
                            sB.AppendLine($"Axial Force Difference = {Round(fsAnalysisResult.AxialForceDifference, 2)} kips");
                            sB.AppendLine($"Mx = {Round(fsAnalysisResult.MomentX, 2)} kip-in");
                            sB.AppendLine($"φMx = {Round(fsAnalysisResult.ReducedMomentX, 2)} kip-in");
                            sB.AppendLine($"My = {Round(fsAnalysisResult.MomentY, 2)} kip-in");
                            sB.AppendLine($"φMy = {Round(fsAnalysisResult.ReducedMomentY, 2)} kip-in");
                            sB.AppendLine($"Mmaj = {Round(fsAnalysisResult.MomentMajor, 2)} kip-in");
                            sB.AppendLine($"φMmaj = {Round(fsAnalysisResult.ReducedMomentMajor, 2)} kip-in");
                            sB.AppendLine($"Mmin = {Round(fsAnalysisResult.MomentMinor, 2)} kip-in");
                            sB.AppendLine($"φMmin = {Round(fsAnalysisResult.ReducedMomentMinor, 2)} kip-in");

                            richTextBox_DetailedData.Text = sB.ToString();
                        }
                        else if (_cadPanel.SelectedElements.Count == 1)
                        {
                            IDrawingElement elem = _cadPanel.SelectedElements[0];

                            if (_elementReferences.TryGetValue(elem, out IFiberSectionElement fsElem) == true)
                            {
                                StringBuilder sB = new StringBuilder();

                                if (fsElem.GetType().Equals(typeof(FiberSectionMeshElement)) == true)
                                {
                                    FiberSectionMeshElement fsMeshElem = (FiberSectionMeshElement)fsElem;

                                    if (fsElemResults.MeshElementResults.TryGetValue(fsMeshElem, out FiberElementResult res) == true)
                                    {
                                        sB.AppendLine($"Element Type = Mesh Element");
                                        sB.AppendLine($"Material = {fsElem.Material.Name}");
                                        sB.AppendLine($"Element Area = {Round(fsMeshElem.Area(), 2)} in^2");
                                        sB.AppendLine($"Strain = {Round(res.Strain, 5)}");
                                        sB.AppendLine($"Stress = {Round(res.Stress, 2)} ksi");
                                        sB.AppendLine($"Force = {Round(res.Force, 2)} kips");
                                        sB.AppendLine($"Mx = {Round(res.Mx, 2)} kip-in");
                                        sB.AppendLine($"My = {Round(res.My, 2)} kip-in");
                                        sB.AppendLine($"Mmaj = {Round(res.Mmaj, 2)} kip-in");
                                        sB.AppendLine($"Mmin = {Round(res.Mmin, 2)} kip-in");
                                    }
                                }
                                else if (fsElem.GetType().Equals(typeof(FiberSectionRebarElement)) == true)
                                {
                                    FiberSectionRebarElement fsMeshElem = (FiberSectionRebarElement)fsElem;

                                    if (fsElemResults.RebarElementResults.TryGetValue(fsMeshElem, out RebarSectionResult res) == true)
                                    {
                                        sB.AppendLine($"Element Type = Rebar Element");
                                        sB.AppendLine($"Material = {fsElem.Material.Name}");
                                        sB.AppendLine($"Element Area = {Round(fsMeshElem.Area(), 2)} in^2");
                                        sB.AppendLine($"Strain = {Round(res.Strain, 5)}");
                                        sB.AppendLine($"Stress = {Round(res.Stress, 2)} ksi");
                                        sB.AppendLine($"Force = {Round(res.Force, 2)} kips");
                                        sB.AppendLine($"Mx = {Round(res.Mx, 2)} kip-in");
                                        sB.AppendLine($"My = {Round(res.My, 2)} kip-in");
                                        sB.AppendLine($"Mmaj = {Round(res.Mmaj, 2)} kip-in");
                                        sB.AppendLine($"Mmin = {Round(res.Mmin, 2)} kip-in");
                                        sB.AppendLine($"Material Removed = {fsMeshElem.MaterialRemoved.Name}");
                                        sB.AppendLine($"Force Removed = {Round(res.ForceRemoved, 2)} kips");
                                    }
                                }
                                else if (fsElem.GetType().Equals(typeof(FiberSectionPostTensioningElement)) == true)
                                {
                                    FiberSectionPostTensioningElement fsMeshElem = (FiberSectionPostTensioningElement)fsElem;

                                    if (fsElemResults.PostTensioningElementResults.TryGetValue(fsMeshElem, out PostTensioningResult res) == true)
                                    {
                                        sB.AppendLine($"Element Type = PT Element");
                                        sB.AppendLine($"Material = {fsElem.Material.Name}");
                                        sB.AppendLine($"Tendon Area = {Round(fsMeshElem.TendonArea, 2)} in^2");
                                        sB.AppendLine($"Casing Area = {Round(fsMeshElem.Area(), 2)} in^2");
                                        sB.AppendLine($"Initial Prestress = {Round(fsMeshElem.InitialPrestress, 2)} ksi");
                                        sB.AppendLine($"Initial Prestrain = {Round(fsMeshElem.InitialPrestrain, 5)}");
                                        sB.AppendLine($"Strain = {Round(res.Strain, 5)}");
                                        sB.AppendLine($"Stress = {Round(res.Stress, 2)} ksi");
                                        sB.AppendLine($"Force = {Round(res.Force, 2)} kips");
                                        sB.AppendLine($"Mx = {Round(res.Mx, 2)} kip-in");
                                        sB.AppendLine($"My = {Round(res.My, 2)} kip-in");
                                        sB.AppendLine($"Mmaj = {Round(res.Mmaj, 2)} kip-in");
                                        sB.AppendLine($"Mmin = {Round(res.Mmin, 2)} kip-in");
                                        sB.AppendLine($"Material Removed = {fsMeshElem.MaterialRemoved.Name}");
                                        sB.AppendLine($"Force Removed = {Round(res.ForceRemoved, 2)} kips");
                                    }
                                }

                                richTextBox_DetailedData.Text = sB.ToString();
                            }
                        }
                    }
                }
            }
        }
    }
}
