﻿using System;
using System.Windows.Forms;

namespace CadPanel.Forms
{
    public partial class OverwritePropertyForm : Form
    {
        public bool UseSameResponse { get; set; } = false;
        public OverwritePropertyForm()
        {
            InitializeComponent();
        }

        private void OverwritePropertyForm_Load(object sender, EventArgs e)
        {

        }

        private void button_No_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;

            UseSameResponse = checkBox_UseLater.Checked;

            this.Close();
        }

        private void button_Yes_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;

            UseSameResponse = checkBox_UseLater.Checked;

            this.Close();
        }
    }
}
