﻿using System;
using System.Windows.Forms;
using Utilities.Extensions.DotNetNative;
using CadPanel.UserProperties;

namespace CadPanel.Forms
{
    public partial class AddUserPropertyForm : Form
    {
        public string PropertyName { get; protected set; }
        public UserPropertyType PropertyType { get; protected set; }
        public object PropertyValue { get; protected set; }

        public AddUserPropertyForm()
        {
            InitializeComponent();
        }

        private void AddUserPropertyForm_Load(object sender, EventArgs e)
        {
            PopulatePropertyTypesComboBox();

            comboBox_PropertyType.SelectedIndex = 0;
            comboBox_Bool.SelectedIndex = 0;
        }

        private void PopulatePropertyTypesComboBox()
        {
            comboBox_PropertyType.Items.Clear();

            Array items = System.Enum.GetValues(typeof(UserPropertyType));

            foreach (int item in items)
            {
                comboBox_PropertyType.Items.Add(((UserPropertyType)item).GetDescription());
            }
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_PropertyType.SelectedIndex >= 0)
                {
                    if (string.IsNullOrWhiteSpace(textBox_Name.Text) == true)
                    {
                        throw new Exception("The property name cannot be empty");
                    }
                    else
                    {
                        PropertyName = textBox_Name.Text;
                    }

                    UserPropertyType propType = (UserPropertyType)(comboBox_PropertyType.SelectedIndex);
                    PropertyType = propType;

                    if (propType.Equals(UserPropertyType.Boolean) == true)
                    {
                        if (comboBox_Bool.SelectedIndex >= 0)
                        {
                            PropertyValue = Convert.ToBoolean(comboBox_Bool.Text);
                        }
                    }
                    else if (propType.Equals(UserPropertyType.Number) == true)
                    {
                        if (string.IsNullOrWhiteSpace(textBox_Value.Text) == true)
                        {
                            PropertyValue = 0;
                        }
                        else if (double.TryParse(textBox_Value.Text, out double val) == true)
                        {
                            PropertyValue = val;
                        }
                        else
                        {
                            throw new Exception("Value must be numeric for a 'Number' property type");
                        }
                    }
                    else if (propType.Equals(UserPropertyType.Text) == true)
                    {
                        PropertyValue = textBox_Value.Text;
                    }
                    else if (propType.Equals(UserPropertyType.Color) == true)
                    {
                        PropertyValue = button_Color.BackColor;
                    }

                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
                else
                {
                    throw new Exception("A property type must be selected");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + ex.Message);
            }

        }

        private void button_Color_Click(object sender, EventArgs e)
        {
            ColorDialog cDialog = new ColorDialog();

            cDialog.Color = button_Color.BackColor;

            if (cDialog.ShowDialog() == DialogResult.OK)
            {
                button_Color.BackColor = cDialog.Color;
            }
        }

        private void comboBox_PropertyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_PropertyType.SelectedIndex >= 0)
            {
                UserPropertyType propType = (UserPropertyType)(comboBox_PropertyType.SelectedIndex);

                if (propType.Equals(UserPropertyType.Number) == true || propType.Equals(UserPropertyType.Text) == true)
                {
                    textBox_Value.Show();
                    comboBox_Bool.Hide();
                    button_Color.Hide();
                }
                else if (propType.Equals(UserPropertyType.Boolean) == true)
                {
                    comboBox_Bool.Show();
                    comboBox_Bool.Location = textBox_Value.Location;
                    textBox_Value.Hide();
                    button_Color.Hide();
                }
                else if (propType.Equals(UserPropertyType.Color) == true)
                {
                    button_Color.Show();
                    button_Color.Location = textBox_Value.Location;
                    textBox_Value.Hide();
                    comboBox_Bool.Hide();
                }
            }
        }
    }
}
