﻿namespace CadPanel.Forms
{
    partial class DefineMomentCurvatureLoadsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_Loads = new System.Windows.Forms.DataGridView();
            this.AxialForce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.label_Note = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Loads)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Loads
            // 
            this.dataGridView_Loads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Loads.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AxialForce});
            this.dataGridView_Loads.Location = new System.Drawing.Point(12, 50);
            this.dataGridView_Loads.Name = "dataGridView_Loads";
            this.dataGridView_Loads.Size = new System.Drawing.Size(168, 236);
            this.dataGridView_Loads.TabIndex = 0;
            // 
            // AxialForce
            // 
            this.AxialForce.HeaderText = "Axial Force (kips)";
            this.AxialForce.Name = "AxialForce";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Define Axial Loads to Analyze";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(167, 329);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 2;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(86, 329);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 3;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // label_Note
            // 
            this.label_Note.Location = new System.Drawing.Point(12, 289);
            this.label_Note.Name = "label_Note";
            this.label_Note.Size = new System.Drawing.Size(184, 37);
            this.label_Note.TabIndex = 4;
            this.label_Note.Text = "Note:  compression is positive, tension is negative";
            // 
            // DefineMomentCurvatureLoadsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 361);
            this.Controls.Add(this.label_Note);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Loads);
            this.MaximumSize = new System.Drawing.Size(270, 400);
            this.MinimumSize = new System.Drawing.Size(270, 400);
            this.Name = "DefineMomentCurvatureLoadsForm";
            this.Text = "Moment Curvature Loads";
            this.Load += new System.EventHandler(this.DefineMomentCurvatureLoadsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Loads)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Loads;
        private System.Windows.Forms.DataGridViewTextBoxColumn AxialForce;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
        private System.Windows.Forms.Label label_Note;
    }
}