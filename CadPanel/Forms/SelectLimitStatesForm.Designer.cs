﻿namespace CadPanel.Forms
{
    partial class SelectLimitStatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_LimitStates = new System.Windows.Forms.DataGridView();
            this.Include = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Material = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LSName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompressionStrain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TensionStrain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Termination = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.button_New = new System.Windows.Forms.Button();
            this.button_Edit = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_LimitStates)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_LimitStates
            // 
            this.dataGridView_LimitStates.AllowUserToAddRows = false;
            this.dataGridView_LimitStates.AllowUserToDeleteRows = false;
            this.dataGridView_LimitStates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_LimitStates.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Include,
            this.Material,
            this.LSName,
            this.CompressionStrain,
            this.TensionStrain,
            this.Termination});
            this.dataGridView_LimitStates.Location = new System.Drawing.Point(12, 12);
            this.dataGridView_LimitStates.MultiSelect = false;
            this.dataGridView_LimitStates.Name = "dataGridView_LimitStates";
            this.dataGridView_LimitStates.Size = new System.Drawing.Size(658, 320);
            this.dataGridView_LimitStates.TabIndex = 0;
            // 
            // Include
            // 
            this.Include.HeaderText = "Include";
            this.Include.Name = "Include";
            // 
            // Material
            // 
            this.Material.HeaderText = "Material";
            this.Material.Name = "Material";
            this.Material.ReadOnly = true;
            this.Material.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // LSName
            // 
            this.LSName.HeaderText = "Name";
            this.LSName.Name = "LSName";
            this.LSName.ReadOnly = true;
            this.LSName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // CompressionStrain
            // 
            this.CompressionStrain.HeaderText = "Compr Strain";
            this.CompressionStrain.Name = "CompressionStrain";
            this.CompressionStrain.ReadOnly = true;
            this.CompressionStrain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // TensionStrain
            // 
            this.TensionStrain.HeaderText = "Tension Strain";
            this.TensionStrain.Name = "TensionStrain";
            this.TensionStrain.ReadOnly = true;
            this.TensionStrain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Termination
            // 
            this.Termination.HeaderText = "Termination";
            this.Termination.Name = "Termination";
            this.Termination.ReadOnly = true;
            this.Termination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(595, 415);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 1;
            this.button_Cancel.Text = "Close";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(569, 338);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(101, 26);
            this.button_Delete.TabIndex = 3;
            this.button_Delete.Text = "Delete Selected";
            this.button_Delete.UseVisualStyleBackColor = true;
            // 
            // button_New
            // 
            this.button_New.Location = new System.Drawing.Point(462, 338);
            this.button_New.Name = "button_New";
            this.button_New.Size = new System.Drawing.Size(101, 26);
            this.button_New.TabIndex = 4;
            this.button_New.Text = "Define New";
            this.button_New.UseVisualStyleBackColor = true;
            this.button_New.Click += new System.EventHandler(this.button_New_Click);
            // 
            // button_Edit
            // 
            this.button_Edit.Location = new System.Drawing.Point(355, 338);
            this.button_Edit.Name = "button_Edit";
            this.button_Edit.Size = new System.Drawing.Size(101, 26);
            this.button_Edit.TabIndex = 5;
            this.button_Edit.Text = "Edit Selected";
            this.button_Edit.UseVisualStyleBackColor = true;
            this.button_Edit.Click += new System.EventHandler(this.button_Edit_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(514, 415);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 6;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // SelectLimitStatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 450);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Edit);
            this.Controls.Add(this.button_New);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.dataGridView_LimitStates);
            this.Name = "SelectLimitStatesForm";
            this.Text = "Select Limit States";
            this.Load += new System.EventHandler(this.SelectLimitStatesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_LimitStates)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_LimitStates;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_Delete;
        private System.Windows.Forms.Button button_New;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Include;
        private System.Windows.Forms.DataGridViewTextBoxColumn Material;
        private System.Windows.Forms.DataGridViewTextBoxColumn LSName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompressionStrain;
        private System.Windows.Forms.DataGridViewTextBoxColumn TensionStrain;
        private System.Windows.Forms.DataGridViewTextBoxColumn Termination;
        private System.Windows.Forms.Button button_Edit;
        private System.Windows.Forms.Button button_OK;
    }
}