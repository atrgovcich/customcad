﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Materials;
using Materials.Inelastic.Steel;
using ReinforcedConcrete.PostTensioning;
using Utilities.Extensions.DotNetNative;
using static CadPanel.Common.CommonMethods;

namespace CadPanel.Forms
{
    public partial class AddEditPostTensioningForm : Form
    {
        private bool _editing = false;
        private List<IGenericMaterial> _allMaterials;
        List<PostTensioningInnerSection> _allInnerSections;
        List<PostTensioningDefinition> _allPostTensioning;
        PostTensioningDefinition _defToEdit;

        public AddEditPostTensioningForm(bool editing, List<PostTensioningDefinition> allPostTensioning, List<PostTensioningInnerSection> allInnerSections, List<IGenericMaterial> allMaterials, PostTensioningDefinition defToEdit)
        {
            InitializeComponent();

            _editing = editing;

            _allMaterials = allMaterials;
            _allInnerSections = allInnerSections;
            _allPostTensioning = allPostTensioning;
            _defToEdit = defToEdit;
        }

        private void AddEditPostTensioningForm_Load(object sender, EventArgs e)
        {
            label_InternalStrandDiam.Hide();
            label_InternalStrandType.Hide();
            label_InternalStrandUnits.Hide();

            comboBox_InternalStrandType.Hide();
            textBox_InternalStrandDiam.Hide();
            textBox_InternalStrandDiam.Enabled = false;
            comboBox_InternalStrandType.Enabled = false;

            PopulateMaterialsComboBox();
            PopulateStrandTypeComboBox();
            PopulateInternalStrandTypeComboBox();

            if (_editing == false)
            {
                if (comboBox_Material.Items.Count > 0)
                {
                    comboBox_Material.SelectedIndex = 0;
                }

                comboBox_StrandType.SelectedIndex = 1;
                comboBox_InternalStrandType.SelectedIndex = 1;
            }
            else
            {
                textBox_Name.Text = _defToEdit.Name;
                textBox_Diameter.Text = Convert.ToString(_defToEdit.OuterDiameter);
                textBox_Area.Text = Convert.ToString(_defToEdit.SteelArea);
                comboBox_Material.Text = _defToEdit.Material.Name;
                comboBox_StrandType.SelectedIndex = (int)_defToEdit.StrandType;

                PostTensioningStrandType innerStrandType = _defToEdit.InnerStrandType;
                string innerStrandDescr = innerStrandType.GetDescription();

                for (int i = 0; i < comboBox_InternalStrandType.Items.Count; i++)
                {
                    if (string.Compare(Convert.ToString(comboBox_InternalStrandType.Items[i]), innerStrandDescr, ignoreCase: true) == 0)
                    {
                        comboBox_InternalStrandType.SelectedIndex = i;

                        break;
                    }
                }
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }


        private void PopulateMaterialsComboBox()
        {
            comboBox_Material.Items.Clear();

            for (int i = 0; i < _allMaterials.Count; i++)
            {
                if (_allMaterials[i].GetType().Equals(typeof(ModifiedRambergOsgoodSteel)) == true)
                {
                    comboBox_Material.Items.Add(_allMaterials[i].Name);
                }
            }
        }

        private void PopulateStrandTypeComboBox()
        {
            comboBox_StrandType.Items.Clear();

            Array items = System.Enum.GetValues(typeof(PostTensioningStrandType));

            foreach (int item in items)
            {
                comboBox_StrandType.Items.Add(((PostTensioningStrandType)item).GetDescription());
            }
        }

        private void PopulateInternalStrandTypeComboBox()
        {
            comboBox_InternalStrandType.Items.Clear();

            Array items = System.Enum.GetValues(typeof(PostTensioningStrandType));

            foreach (int item in items)
            {
                if (((PostTensioningStrandType)item).Equals(PostTensioningStrandType.NineteenStrandBundle) == false)
                {
                    comboBox_InternalStrandType.Items.Add(((PostTensioningStrandType)item).GetDescription());
                }
            }
        }

        private bool ValidateInputs(out string message)
        {
            string name = textBox_Name.Text;

            if (_editing == false)
            {
                //Check the type name is unique
                for (int i = 0; i < _allPostTensioning.Count; i++)
                {
                    if (string.Compare(name, _allPostTensioning[i].Name, ignoreCase: true) == 0)
                    {
                        message = "The name entered is already in use.  Please enter a unique name.";

                        return false;
                    }
                }
            }
            else
            {
                for (int i = 0; i < _allPostTensioning.Count; i++)
                {
                    if (_allPostTensioning[i].Equals(_defToEdit) == false)
                    {
                        if (string.Compare(name, _allPostTensioning[i].Name, ignoreCase: true) == 0)
                        {
                            message = "The name entered is already in use.  Please enter a unique name.";

                            return false;
                        }
                    }
                }
            }

            if (double.TryParse(textBox_Diameter.Text, out double d) == true)
            {
                if (d <= 0)
                {
                    message = "Outside diameter must be a positive value.";

                    return false;
                }
            }
            else
            {
                message = "Outside diameter must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Area.Text, out double a) == true)
            {
                if (a <= 0)
                {
                    message = "Steel area must be a positive value.";

                    return false;
                }
            }
            else
            {
                message = "Steel area must be a numeric value.";

                return false;
            }

            if (textBox_InternalStrandDiam.Enabled == true)
            {
                if (double.TryParse(textBox_InternalStrandDiam.Text, out double id) == true)
                {
                    if (id <= 0)
                    {
                        message = "Inner strand diameter must be a positive value.";

                        return false;
                    }
                }
                else
                {
                    message = "Inner strand diameter must be a numeric value.";

                    return false;
                }
            }

            message = string.Empty;

            return true;
        }

        private void ComboBox_StrandType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cB = (ComboBox)sender;

            if (cB.SelectedIndex == (int)PostTensioningStrandType.NineteenStrandBundle)
            {
                label_InternalStrandDiam.Show();
                label_InternalStrandType.Show();
                label_InternalStrandUnits.Show();

                comboBox_InternalStrandType.Show();
                textBox_InternalStrandDiam.Show();
                textBox_InternalStrandDiam.Enabled = true;
                comboBox_InternalStrandType.Enabled = true;

                if (_editing == true)
                {
                    if (_defToEdit.PTSection.GetType().IsSubclassOf(typeof(MultiStrandBundle)) == true)
                    {
                        MultiStrandBundle msb = (MultiStrandBundle)_defToEdit.PTSection;

                        textBox_InternalStrandDiam.Text = Convert.ToString(msb.InternalStrandDiameter);            
                    }
                }
            }
            else
            {
                label_InternalStrandDiam.Hide();
                label_InternalStrandType.Hide();
                label_InternalStrandUnits.Hide();

                comboBox_InternalStrandType.Hide();
                textBox_InternalStrandDiam.Hide();
                textBox_InternalStrandDiam.Enabled = false;
                comboBox_InternalStrandType.Enabled = false;
            }
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                if (_editing == true)
                {
                    _defToEdit.Name = textBox_Name.Text;
                    _defToEdit.OuterDiameter = Convert.ToDouble(textBox_Diameter.Text);
                    _defToEdit.SteelArea = Convert.ToDouble(textBox_Area.Text);
                    _defToEdit.Material = GetMaterialFromName(comboBox_Material.Text, _allMaterials);

                    if (comboBox_InternalStrandType.Enabled == true)
                    {
                        _defToEdit.InnerStrandType = EnumExtensions.GetEnumFromDescription<PostTensioningStrandType>(comboBox_InternalStrandType.Text);
                        _defToEdit.InnerStrandDiameter = Convert.ToDouble(textBox_InternalStrandDiam.Text);
                    }

                    _defToEdit.StrandType = (PostTensioningStrandType)comboBox_StrandType.SelectedIndex;
                }
                else
                {
                    string name = textBox_Name.Text;
                    double outerDiameter = Convert.ToDouble(textBox_Diameter.Text);
                    double steelArea = Convert.ToDouble(textBox_Area.Text);
                    PostTensioningStrandType strandType = (PostTensioningStrandType)comboBox_StrandType.SelectedIndex;
                    IGenericMaterial material = GetMaterialFromName(comboBox_Material.Text, _allMaterials);

                    PostTensioningStrandType innerStrandType = PostTensioningStrandType.SevenWireStrand;
                    double innerStrandDiameter = 0;

                    if (comboBox_InternalStrandType.Enabled == true)
                    {
                        innerStrandType = EnumExtensions.GetEnumFromDescription<PostTensioningStrandType>(comboBox_InternalStrandType.Text);
                        innerStrandDiameter = Convert.ToDouble(textBox_InternalStrandDiam.Text);
                    }

                    PostTensioningDefinition newPtDef = new PostTensioningDefinition(name, strandType, material, outerDiameter, steelArea, innerStrandType, innerStrandDiameter);

                    _allPostTensioning.Add(newPtDef);
                }

                this.Close();
            }
            else
            {
                MessageBox.Show("There was an error with the inputs:" + Environment.NewLine + Environment.NewLine + "ERROR:" + Environment.NewLine + message);
            }
        }
    }
}
