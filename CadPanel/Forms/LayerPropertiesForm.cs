﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CadPanel.Common;
using Utilities.Extensions.DotNetNative;

namespace CadPanel.Forms
{
    public partial class LayerPropertiesForm : Form
    {
        public List<Layer> WorkingLayersList;

        private int _selectedRow = -1;

        private int _minWidth = 400;
        private int _minHeight = 400;

        public LayerPropertiesForm(List<Layer> layers)
        {
            InitializeComponent();

            WorkingLayersList = new List<Layer>(layers);
        }

        private void LayerPropertiesForm_Load(object sender, EventArgs e)
        {
            dgv_Layers.AllowUserToAddRows = false;
            dgv_Layers.AllowUserToOrderColumns = false;
            dgv_Layers.AllowUserToDeleteRows = false;

            dgv_Layers.CellContentClick += new DataGridViewCellEventHandler(this.DgvLayers_CellContentClick);

            this.Resize += new System.EventHandler(ResizeFormComponents);

            InitializeLayersDGV();

            dgv_Layers["LayerName", 0].ReadOnly = true;

            this.MinimumSize = new Size(_minWidth, _minHeight);

            ResizeFormComponents(this, new EventArgs());
        }

        private void InitializeLayersDGV()
        {
            if (WorkingLayersList != null)
            {
                for (int i = 0; i < WorkingLayersList.Count; i++)
                {
                    AddLayerToDGV(WorkingLayersList[i]);
                }
            }
        }

        private void AddLayerToDGV(Layer layer)
        {
            dgv_Layers.Rows.Add();

            int i = dgv_Layers.Rows.Count - 1;

            dgv_Layers["LayerName", i].Value = layer.Name;
            dgv_Layers["LayerColor", i].Style.BackColor = layer.Color;
            dgv_Layers["LineThickness", i].Value = layer.Width;
            dgv_Layers["Visible", i].Value = layer.Visible;
            dgv_Layers["Frozen", i].Value = layer.Frozen;
            dgv_Layers["Print", i].Value = layer.Print;
        }

        private void DgvLayers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.ColumnIndex >= 0)
            {
                if (string.Compare(dgv.Columns[e.ColumnIndex].Name, "LayerColor", ignoreCase:true) == 0)
                {
                    Color currentColor = dgv[e.ColumnIndex, e.RowIndex].Style.BackColor;

                    ColorDialog cDialog = new ColorDialog();

                    cDialog.Color = currentColor;

                    if (cDialog.ShowDialog() == DialogResult.OK)
                    {
                        currentColor = cDialog.Color;

                        dgv[e.ColumnIndex, e.RowIndex].Style.BackColor = currentColor;
                    }
                }
            }

            _selectedRow = e.RowIndex;
        }

        private void Button_AddLayer_Click(object sender, EventArgs e)
        {
            string newName = "New Layer";

            bool newNameUnique = true;

            List<int> layerNumberSuffixes = new List<int>();

            for (int i = 0; i < WorkingLayersList.Count; i++)
            {
                if (string.Compare(WorkingLayersList[i].Name.Left(newName.Length), newName, ignoreCase:true) == 0)
                {
                    if (string.Compare(WorkingLayersList[i].Name, newName, ignoreCase: true) == 0)
                    {
                        newNameUnique = false;
                    }

                    string rightChars = WorkingLayersList[i].Name.Right(WorkingLayersList[i].Name.Length - newName.Length);

                    int indexOfLeft = rightChars.IndexOf('(');

                    int indexOfRight = rightChars.IndexOf(')');

                    if (indexOfLeft >= 0 && indexOfRight >= 0)
                    {
                        string textBtwn = rightChars.Substring(indexOfLeft + 1, indexOfRight - indexOfLeft - 1);

                        if (int.TryParse(textBtwn, out int result) == true)
                        {
                            layerNumberSuffixes.Add(result);
                        }
                    }
                }
            }

            bool requiresSuffix = (newNameUnique == true) ? false : true;

            if (layerNumberSuffixes.Count > 0)
            {
                layerNumberSuffixes = layerNumberSuffixes.OrderBy(x => x).ToList();
            }

            int nextAvailableNumber = 1;

            for (int i = 0; i < layerNumberSuffixes.Count; i++)
            {
                if (layerNumberSuffixes[i] == nextAvailableNumber)
                {
                    nextAvailableNumber++;
                }
            }

            if (requiresSuffix == true)
            {
                newName = newName + " (" + nextAvailableNumber + ")";
            }

            Layer newLayer = new Layer(newName, 0, 1, Color.Black);

            AddLayerToDGV(newLayer);

            WorkingLayersList.Add(newLayer);
        }

        private void Button_RemoveLayer_Click(object sender, EventArgs e)
        {
            if (_selectedRow >= 1 && _selectedRow <= dgv_Layers.Rows.Count)
            {
                dgv_Layers.Rows.RemoveAt(_selectedRow);

                WorkingLayersList.RemoveAt(_selectedRow);
            }
        }

        private void Button_Apply_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgv_Layers.Rows.Count; i++)
            {
                if (string.Compare(WorkingLayersList[i].Name, "Default", ignoreCase: false) != 0)
                {
                    WorkingLayersList[i].Name = (string)dgv_Layers["LayerName", i].Value;
                }

                WorkingLayersList[i].Color = dgv_Layers["LayerColor", i].Style.BackColor;
                WorkingLayersList[i].Width = Convert.ToInt32(dgv_Layers["LineThickness", i].Value);
                WorkingLayersList[i].Visible = (bool)dgv_Layers["Visible", i].Value;
                WorkingLayersList[i].Frozen = (bool)dgv_Layers["Frozen", i].Value;
                WorkingLayersList[i].Print = (bool)dgv_Layers["Print", i].Value;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void ResizeFormComponents(object sender, EventArgs e)
        {
            Button_Cancel.Location = new Point(this.Width - Button_Cancel.Width - 30, this.Height - Button_Cancel.Height - 50);

            Button_Apply.Location = new Point(Button_Cancel.Location.X - 5 - Button_Apply.Width, Button_Cancel.Location.Y);

            dgv_Layers.Width = this.Width - 40;

            dgv_Layers.Height = Button_Apply.Location.Y - dgv_Layers.Location.Y - 20;


        }
    }
}
