﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CadPanel.Forms
{
    public partial class DefineMomentCurvatureLoadsForm : Form
    {
        private List<double> _loads = new List<double>();

        public List<double> Loads
        {
            get
            {
                return _loads;
            }
        }

        public DefineMomentCurvatureLoadsForm(List<double> loads)
        {
            InitializeComponent();

            if (loads != null)
            {
                _loads = new List<double>(loads);
            }
            else
            {
                _loads = new List<double>();
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            _loads = new List<double>();

            for (int i = 0; i < dataGridView_Loads.Rows.Count - 1; i++)
            {
                if(double.TryParse(Convert.ToString(dataGridView_Loads[0,i].Value), out double load) == true)
                {
                    _loads.Add(load);
                }
            }

            this.Close();
        }

        private void PopulateDataGridWithLoads()
        {
            if (_loads != null && _loads.Count > 0)
            {
                for (int i = 0; i < _loads.Count; i ++)
                {
                    dataGridView_Loads.Rows.Add();
                    dataGridView_Loads[0, i].Value = Convert.ToString(_loads[i]);
                }
            }
        }

        private void DefineMomentCurvatureLoadsForm_Load(object sender, EventArgs e)
        {
            PopulateDataGridWithLoads();
        }
    }
}
