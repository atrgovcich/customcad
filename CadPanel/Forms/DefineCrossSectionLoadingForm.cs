﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using FiberSectionAnalysis.Loads;

namespace CadPanel.Forms
{
    public partial class DefineCrossSectionLoadingForm : Form
    {
        private List<CrossSectionLoad> _loadList { get; set; }

        private DataGridView _selectedDataGridView;

        public List<CrossSectionLoad> Loads
        {
            get
            {
                return _loadList;
            }
        }

        public DefineCrossSectionLoadingForm(List<CrossSectionLoad> loadList)
        {
            InitializeComponent();

            _loadList = loadList;

            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
                BindingFlags.Instance | BindingFlags.SetProperty, null,
                dataGridView_Loads, new object[] { true });

            dataGridView_Loads.Click += new EventHandler(DataGridView_Loads_Click);

            dataGridView_Loads.KeyDown += new KeyEventHandler(KeyPaste);

            _selectedDataGridView = dataGridView_Loads;
        }

        private void DefineCrossSectionLoadingForm_Load(object sender, EventArgs e)
        {
            PopulateDataGridView();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void PopulateDataGridView()
        {
            if (_loadList != null && _loadList.Count > 0)
            {
                dataGridView_Loads.Rows.Clear();

                for (int i = 0; i < _loadList.Count; i++)
                {
                    dataGridView_Loads.Rows.Add();

                    dataGridView_Loads[0, i].Value = _loadList[i].LoadCase;

                    dataGridView_Loads[1, i].Value = _loadList[i].P;

                    dataGridView_Loads[2, i].Value = _loadList[i].Mx;

                    dataGridView_Loads[3, i].Value = _loadList[i].My;

                    dataGridView_Loads[4, i].Value = _loadList[i].Mx_top;

                    dataGridView_Loads[5, i].Value = _loadList[i].My_top;

                    dataGridView_Loads[6, i].Value = _loadList[i].Mx_bot;

                    dataGridView_Loads[7, i].Value = _loadList[i].My_bot;

                    dataGridView_Loads[8, i].Value = _loadList[i].Vx;

                    dataGridView_Loads[9, i].Value = _loadList[i].Vy;
                }
            }
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView_Loads.Rows != null)
                {
                    if (_loadList != null)
                    {
                        _loadList.Clear();
                    }
                    else
                    {
                        _loadList = new List<CrossSectionLoad>();
                    }

                    if (dataGridView_Loads.Rows.Count > 1)
                    {
                        if (ValidateDataGridView(out string validationMessage) == true)
                        {
                            for (int i = 0; i < dataGridView_Loads.Rows.Count - 1; i++)
                            {
                                CrossSectionLoad load = new CrossSectionLoad()
                                {
                                    LoadCase = Convert.ToString(dataGridView_Loads[0, i].Value),
                                    P = Convert.ToDouble(dataGridView_Loads[1, i].Value),
                                    Mx = Convert.ToDouble(dataGridView_Loads[2, i].Value),
                                    My = Convert.ToDouble(dataGridView_Loads[3, i].Value),
                                    Mx_top = Convert.ToDouble(dataGridView_Loads[4, i].Value),
                                    My_top = Convert.ToDouble(dataGridView_Loads[5, i].Value),
                                    Mx_bot = Convert.ToDouble(dataGridView_Loads[6, i].Value),
                                    My_bot = Convert.ToDouble(dataGridView_Loads[7, i].Value),
                                    Vx = Convert.ToDouble(dataGridView_Loads[8, i].Value),
                                    Vy = Convert.ToDouble(dataGridView_Loads[9, i].Value)
                                };

                                _loadList.Add(load);
                            }
                        }
                        else
                        {
                            throw new Exception(validationMessage);
                        }
                    }
                }
                else
                {
                    _loadList.Clear();
                }

                this.DialogResult = DialogResult.OK;

                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occured while validating loads:" + Environment.NewLine + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateDataGridView(out string message)
        {
            message = string.Empty;

            if (dataGridView_Loads.Rows != null)
            {
                for (int i = 0; i < dataGridView_Loads.Rows.Count - 1; i++)
                {
                    for (int j = 1; j < dataGridView_Loads.Columns.Count; j++)
                    {
                        if (double.TryParse(Convert.ToString(dataGridView_Loads[j,i].Value), out double val) == false)
                        {
                            string cellVal = Convert.ToString(dataGridView_Loads[j, i].Value);

                            if (cellVal != "" || String.Compare(cellVal, string.Empty,ignoreCase: true) != 0)
                            {
                                message = $"Value in row {i}, column {j} is not numeric";

                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        private void KeyPaste (object sender, KeyEventArgs e)
        {
            if (_selectedDataGridView != null)
            {
                if (e.KeyCode.Equals(Keys.V) && e.Control == true)
                {
                    Paste();
                }
            }
        }

        private void Paste()
        {
            PasteData(_selectedDataGridView);
        }

        private void PasteData(DataGridView dgv)
        {
            string[] tArr = Clipboard.GetText().Trim().Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            string[] arT;

            int r = dgv.CurrentCellAddress.Y;

            int c = dgv.CurrentCellAddress.X;

            if (r >= 0 && c >= 0)
            {
                if (tArr.Length > dgv.Rows.Count - r)
                {
                    dgv.Rows.Add(tArr.Length - (dgv.Rows.Count - r) + 1);
                }

                for (int i = 0; i <= tArr.Length - 1; i++)
                {
                    if (tArr[i] != "")
                    {
                        arT = tArr[i].Split(Convert.ToChar(9)); //tab character

                        int cc = c;

                        for (int j = 0; j <= arT.Length - 1; j++)
                        {
                            if (cc > dgv.Columns.Count - 1)
                            {
                                break;
                            }

                            if (r > dgv.Rows.Count - 1)
                            {
                                return;
                            }

                            dgv[cc, r].Value = arT[j];

                            cc += 1;
                        }

                        r += 1;
                    }
                }
            }
        }

        private void DataGridView_Loads_Click(object sender, EventArgs e)
        {
            _selectedDataGridView = dataGridView_Loads;
        }
    }
}
