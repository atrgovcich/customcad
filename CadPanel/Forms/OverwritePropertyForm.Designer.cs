﻿namespace CadPanel.Forms
{
    partial class OverwritePropertyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label_Message = new System.Windows.Forms.Label();
            this.checkBox_UseLater = new System.Windows.Forms.CheckBox();
            this.button_No = new System.Windows.Forms.Button();
            this.button_Yes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Warning";
            // 
            // label_Message
            // 
            this.label_Message.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Message.Location = new System.Drawing.Point(12, 39);
            this.label_Message.Name = "label_Message";
            this.label_Message.Size = new System.Drawing.Size(425, 54);
            this.label_Message.TabIndex = 1;
            this.label_Message.Text = "The user property being added is already contained within one or more of the elem" +
    "ents, but is defined as a different proeprty type.  Would you like to overrwrite" +
    " the existing property type?";
            // 
            // checkBox_UseLater
            // 
            this.checkBox_UseLater.AutoSize = true;
            this.checkBox_UseLater.Location = new System.Drawing.Point(15, 96);
            this.checkBox_UseLater.Name = "checkBox_UseLater";
            this.checkBox_UseLater.Size = new System.Drawing.Size(241, 17);
            this.checkBox_UseLater.TabIndex = 2;
            this.checkBox_UseLater.Text = "Use this response for all subsequent warnings";
            this.checkBox_UseLater.UseVisualStyleBackColor = true;
            // 
            // button_No
            // 
            this.button_No.Location = new System.Drawing.Point(362, 153);
            this.button_No.Name = "button_No";
            this.button_No.Size = new System.Drawing.Size(75, 23);
            this.button_No.TabIndex = 3;
            this.button_No.Text = "No";
            this.button_No.UseVisualStyleBackColor = true;
            this.button_No.Click += new System.EventHandler(this.button_No_Click);
            // 
            // button_Yes
            // 
            this.button_Yes.Location = new System.Drawing.Point(281, 153);
            this.button_Yes.Name = "button_Yes";
            this.button_Yes.Size = new System.Drawing.Size(75, 23);
            this.button_Yes.TabIndex = 4;
            this.button_Yes.Text = "Yes";
            this.button_Yes.UseVisualStyleBackColor = true;
            this.button_Yes.Click += new System.EventHandler(this.button_Yes_Click);
            // 
            // OverwritePropertyForm
            // 
            this.ClientSize = new System.Drawing.Size(449, 188);
            this.Controls.Add(this.button_Yes);
            this.Controls.Add(this.button_No);
            this.Controls.Add(this.checkBox_UseLater);
            this.Controls.Add(this.label_Message);
            this.Controls.Add(this.label1);
            this.Name = "OverwritePropertyForm";
            this.Text = "Overwrite Properties";
            this.Load += new System.EventHandler(this.OverwritePropertyForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_Message;
        private System.Windows.Forms.CheckBox checkBox_UseLater;
        private System.Windows.Forms.Button button_No;
        private System.Windows.Forms.Button button_Yes;
    }
}