﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FiberSectionAnalysis.LimitStates;
using Materials;
using CadPanel.Common;
using Utilities.Charting;
using OxyPlot.WindowsForms;
using Utilities.Geometry;
using System.Threading;

namespace CadPanel.Forms
{
    public partial class AddOrEditLimitStateForm : Form
    {
        private bool _editing;
        private LimitState _lsToEdit;
        private List<IGenericMaterial> _materialList;
        private IGenericMaterial _selectedMaterial = null;

        private Panel _chartPanel;
        private int _chartWidth = 100;
        private int _chartHeight = 100;
        private PlotView _plotView;
        private Chart2D _chart = null;

        private LimitState _newLimitState = null;

        public LimitState NewLimitState
        {
            get
            {
                return _newLimitState;
            }
        }

        public AddOrEditLimitStateForm(bool editing, List<IGenericMaterial> materialList, LimitState lsToEdit)
        {
            InitializeComponent();

            _editing = editing;
            _lsToEdit = lsToEdit;
            _materialList = materialList;

            this.Resize += new EventHandler(ResizeFormComponents);
        }

        private void AddOrEditLimitStateForm_Load(object sender, EventArgs e)
        {
            InitializeChart();

            PopulateMaterialsComboBox();

            if (_editing == true)
            {
                if (_lsToEdit != null)
                {
                    textBox_Name.Text = _lsToEdit.Name;

                    textBox_Compression.Text = Convert.ToString(_lsToEdit.CompressionLimitStrain);

                    textBox_Tension.Text = Convert.ToString(_lsToEdit.TensionLimitStrain);

                    checkBox_Termination.Checked = _lsToEdit.TerminationLimitState;

                    comboBox_Material.SelectedIndex = _materialList.IndexOf(_lsToEdit.Material);

                    button_OK.Text = "Save";
                }
            }

            ResizeAll();


        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }


        private void ResizeAll()
        {
            ResizeButtons();

            ResizePanel();

            ResizeChart();
        }

        private void ResizePanel()
        {
            panel_Chart.Height = button_Cancel.Location.Y - 10 - panel_Chart.Location.Y;

            panel_Chart.Width = button_Cancel.Location.X + button_Cancel.Width - panel_Chart.Location.X;
        }

        private void ResizeButtons()
        {
            button_Cancel.Location = new Point(this.Width - button_Cancel.Width - 30, this.Height - button_Cancel.Height - 50);

            button_OK.Location = new Point(button_Cancel.Location.X - 5 - button_OK.Width, button_Cancel.Location.Y);
        }

        private void ResizeFormComponents(object sender, EventArgs e)
        {
            ResizeAll();
        }

        private void PopulateMaterialsComboBox()
        {
            comboBox_Material.Items.Clear();

            for (int i = 0; i < _materialList.Count; i++)
            {
                comboBox_Material.Items.Add(_materialList[i].Name);
            }
        }

        private void comboBox_Material_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_Material.SelectedIndex >= 0)
            {
                _selectedMaterial = CommonMethods.GetMaterialFromName(comboBox_Material.Text, _materialList);

                if (_selectedMaterial != null)
                {
                    List<PointD> curve = GetStressStrainCurve();

                    PopulateChart(curve);

                    textBox_Compression_TextChanged(textBox_Compression, new EventArgs());

                    textBox_Tension_TextChanged(textBox_Tension, new EventArgs());
                }
            }

        }

        private void InitializeChart()
        {
            _chartWidth = panel_Chart.Width;

            _chartHeight = panel_Chart.Height;

            _plotView = new PlotView();

            _plotView.Location = new System.Drawing.Point(0, 0);

            _plotView.Width = _chartWidth;

            _plotView.Height = _chartHeight;

            panel_Chart.Controls.Add(_plotView);
        }

        private void ResizeChart()
        {
            if (_plotView != null)
            {
                _plotView.Size = new Size(panel_Chart.Width, panel_Chart.Height);
            }
        }

        private List<PointD> GetStressStrainCurve()
        {
            List<PointD> points = new List<PointD>();

            if (_selectedMaterial != null)
            {
                double minStrain = -_selectedMaterial.MaxTensionStrain();
                double maxStrain = _selectedMaterial.MaxCompressionStrain();

                double strainInterval = (maxStrain - minStrain) / 100;

                for (double i = minStrain; i <= maxStrain; i += strainInterval)
                {
                    double stress = _selectedMaterial.MonotonicStressStrain(i);

                    points.Add(new PointD(i, stress));
                }
            }

            return points;
        }

        private void PopulateChart(List<PointD> points)
        {
            _chart = new Chart2D($"Stress Strain Diagram");

            _chart.HorizontalAxisLabel = "Strain";

            _chart.VerticalAxisLabel = "Stress (ksi)";

            if (points != null && points.Count > 0)
            {
                List<PointD> xyData = points;

                double xMax = xyData.Select(p => p.X).Max(x => x);
                double xMin = xyData.Select(p => p.X).Min(x => x);

                double yMax = xyData.Select(p => p.Y).Max(x => x);
                double yMin = xyData.Select(p => p.Y).Min(x => x);

                _chart.AddSeriesFromXY(xyData, "S");

                _chart.SetHorizontalAxisBounds(0.9 * xMin, 1.1 * xMax);
                _chart.SetVerticalAxisBounds(0.9 * yMin, 1.1 * yMax);
            }

            _chart.SetInterpolation(InterpolationType.None);

            _plotView.Model = _chart.Model;

            _plotView.Invalidate();
        }

        private void textBox_Compression_TextChanged(object sender, EventArgs e)
        {
            if (_selectedMaterial != null)
            {
                double maxTensionStrain = _selectedMaterial.MaxTensionStrain();

                double maxCompressionStrain = _selectedMaterial.MaxCompressionStrain();

                TextBox tB = (TextBox)sender;

                OxyPlot.Series.Series compressionSeries = null; ;

                OxyPlot.Series.Series tensionSeries = null;

                foreach (OxyPlot.Series.Series s in _chart.Model.Series)
                {
                    if (string.Compare(s.Title, "C", ignoreCase: true) == 0)
                    {
                        compressionSeries = s;
                    }

                    if (string.Compare(s.Title, "T", ignoreCase: true) == 0)
                    {
                        tensionSeries = s;
                    }
                }

                if (double.TryParse(tB.Text, out double val) == true)
                {
                    if (val > 0)
                    {
                        if (compressionSeries != null)
                        {
                            if (_chart.Model.Series.Contains(compressionSeries) == true)
                            {
                                _chart.Model.Series.Remove(compressionSeries);
                            }
                        }

                        if (val <= maxCompressionStrain)
                        {
                            double stressVal = _selectedMaterial.MonotonicStressStrain(val);

                            List<PointD> xyData = new List<PointD>(1);

                            xyData.Add(new PointD(val, stressVal));

                            _chart.AddSeriesFromXY(xyData, "C", Color.Red, OxyPlot.LineStyle.Automatic,OxyPlot.MarkerType.Circle);
                        }

                        _plotView.Invalidate();

                        _plotView.Size = new Size(panel_Chart.Width - 1, panel_Chart.Height - 1);

                        _plotView.Size = new Size(panel_Chart.Width, panel_Chart.Height);
                    }
                }
            }
        }

        private void textBox_Tension_TextChanged(object sender, EventArgs e)
        {
            if (_selectedMaterial != null)
            {
                double maxTensionStrain = _selectedMaterial.MaxTensionStrain();

                double maxCompressionStrain = _selectedMaterial.MaxCompressionStrain();

                TextBox tB = (TextBox)sender;

                OxyPlot.Series.Series compressionSeries = null; ;

                OxyPlot.Series.Series tensionSeries = null;

                foreach (OxyPlot.Series.Series s in _chart.Model.Series)
                {
                    if (string.Compare(s.Title, "C", ignoreCase: true) == 0)
                    {
                        compressionSeries = s;
                    }

                    if (string.Compare(s.Title, "T", ignoreCase: true) == 0)
                    {
                        tensionSeries = s;
                    }
                }

                if (double.TryParse(tB.Text, out double val) == true)
                {
                    if (val > 0)
                    {
                        if (tensionSeries != null)
                        {
                            if (_chart.Model.Series.Contains(tensionSeries) == true)
                            {
                                _chart.Model.Series.Remove(tensionSeries);
                            }
                        }

                        if (val <= maxTensionStrain)
                        {
                            double stressVal = _selectedMaterial.MonotonicStressStrain(-val);

                            List<PointD> xyData = new List<PointD>(1);

                            xyData.Add(new PointD(-val, stressVal));

                            _chart.AddSeriesFromXY(xyData, "T", Color.Magenta, OxyPlot.LineStyle.Automatic, OxyPlot.MarkerType.Diamond);
                        }

                        _plotView.Invalidate();

                        _plotView.Size = new Size(panel_Chart.Width - 1, panel_Chart.Height - 1);

                        _plotView.Size = new Size(panel_Chart.Width, panel_Chart.Height);
                    }
                }
            }
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            if (_editing == true)
            {
                double comp = 1.0;
                double tens = 1.0;

                IGenericMaterial material = CommonMethods.GetMaterialFromName(comboBox_Material.Text, _materialList);

                if (string.IsNullOrWhiteSpace(textBox_Compression.Text) == false)
                {
                    comp = Convert.ToDouble(textBox_Compression.Text);
                }

                if (string.IsNullOrWhiteSpace(textBox_Tension.Text) == false)
                {
                    tens = Convert.ToDouble(textBox_Tension.Text);
                }

                _lsToEdit.Name = textBox_Name.Text;
                _lsToEdit.Material = material;
                _lsToEdit.CompressionLimitStrain = comp;
                _lsToEdit.TensionLimitStrain = tens;
                _lsToEdit.TerminationLimitState = checkBox_Termination.Checked;

                this.DialogResult = DialogResult.OK;

                this.Close();
            }
            else
            {
                if (ValidateInputs(out string message) == true)
                {
                    double comp = 1.0;
                    double tens = 1.0;

                    IGenericMaterial material = CommonMethods.GetMaterialFromName(comboBox_Material.Text, _materialList);

                    if (string.IsNullOrWhiteSpace(textBox_Compression.Text) == false)
                    {
                        comp = Convert.ToDouble(textBox_Compression.Text);
                    }

                    if (string.IsNullOrWhiteSpace(textBox_Tension.Text) == false)
                    {
                        tens = Convert.ToDouble(textBox_Tension.Text);
                    }

                    _newLimitState = new LimitState()
                    {
                        Name = textBox_Name.Text,
                        Material = material,
                        CompressionLimitStrain = comp,
                        TensionLimitStrain = tens,
                        TerminationLimitState = checkBox_Termination.Checked
                    };

                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
                else
                {
                    MessageBox.Show("An error was detected with the inputs" + Environment.NewLine + Environment.NewLine + "ERROR:" + Environment.NewLine + message);
                }
            }
        }

        private bool ValidateInputs(out string message)
        {
            if (string.IsNullOrWhiteSpace(textBox_Name.Text) == true)
            {
                message = "A valid name must be entered";

                return false;
            }

            if (comboBox_Material.SelectedIndex < 0)
            {
                message = "A valid material must be selected";

                return false;
            }

            if (string.IsNullOrEmpty(textBox_Compression.Text) == false)
            {
                if (double.TryParse(textBox_Compression.Text, out double comp) == false)
                {
                    message = "The compression strain must be numeric";

                    return false;
                }
                else if (comp <= 0)
                {
                    message = "The compression strain must be greater than 0";

                    return false;
                }
            }

            if (string.IsNullOrEmpty(textBox_Tension.Text) == false)
            {
                if (double.TryParse(textBox_Tension.Text, out double tens) == false)
                {
                    message = "The tension strain must be numeric";

                    return false;
                }
                else if (tens <= 0)
                {
                    message = "The tension strain must be greater than 0";

                    return false;
                }
            }

            message = string.Empty;

            return true;
        }
    }
}
