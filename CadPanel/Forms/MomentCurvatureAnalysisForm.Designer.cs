﻿namespace CadPanel.Forms
{
    partial class MomentCurvatureAnalysisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer_Main = new System.Windows.Forms.SplitContainer();
            this.groupBox_LimitStates = new System.Windows.Forms.GroupBox();
            this.label_LimitStates = new System.Windows.Forms.Label();
            this.button_DefineLimitStates = new System.Windows.Forms.Button();
            this.button_Analyze = new System.Windows.Forms.Button();
            this.groupBox_Loading = new System.Windows.Forms.GroupBox();
            this.label_Loads = new System.Windows.Forms.Label();
            this.button_Loads = new System.Windows.Forms.Button();
            this.checkBox_IncludePT = new System.Windows.Forms.CheckBox();
            this.groupBox_DesignOptions = new System.Windows.Forms.GroupBox();
            this.radioButton_Spirals = new System.Windows.Forms.RadioButton();
            this.radioButton_ClosedHoops = new System.Windows.Forms.RadioButton();
            this.button_DeleteCase = new System.Windows.Forms.Button();
            this.button_NewAnalysis = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_AnalysisCases = new System.Windows.Forms.ComboBox();
            this.groupBox_AnalysisOptions = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Tolerance = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_MaxIterations = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_NumberOfPoints = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_SingleAngle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_MeshSize = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBox_DetailedData = new System.Windows.Forms.RichTextBox();
            this.groupBox_ResultsDisplay = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown_Point = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox_Analysis = new System.Windows.Forms.ComboBox();
            this.checkBox_DisplayReduced = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button_Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_Main)).BeginInit();
            this.splitContainer_Main.Panel1.SuspendLayout();
            this.splitContainer_Main.Panel2.SuspendLayout();
            this.splitContainer_Main.SuspendLayout();
            this.groupBox_LimitStates.SuspendLayout();
            this.groupBox_Loading.SuspendLayout();
            this.groupBox_DesignOptions.SuspendLayout();
            this.groupBox_AnalysisOptions.SuspendLayout();
            this.groupBox_ResultsDisplay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Point)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer_Main
            // 
            this.splitContainer_Main.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer_Main.Location = new System.Drawing.Point(12, 27);
            this.splitContainer_Main.Name = "splitContainer_Main";
            // 
            // splitContainer_Main.Panel1
            // 
            this.splitContainer_Main.Panel1.Controls.Add(this.groupBox_LimitStates);
            this.splitContainer_Main.Panel1.Controls.Add(this.button_Analyze);
            this.splitContainer_Main.Panel1.Controls.Add(this.groupBox_Loading);
            this.splitContainer_Main.Panel1.Controls.Add(this.groupBox_DesignOptions);
            this.splitContainer_Main.Panel1.Controls.Add(this.button_DeleteCase);
            this.splitContainer_Main.Panel1.Controls.Add(this.button_NewAnalysis);
            this.splitContainer_Main.Panel1.Controls.Add(this.label1);
            this.splitContainer_Main.Panel1.Controls.Add(this.comboBox_AnalysisCases);
            this.splitContainer_Main.Panel1.Controls.Add(this.groupBox_AnalysisOptions);
            // 
            // splitContainer_Main.Panel2
            // 
            this.splitContainer_Main.Panel2.Controls.Add(this.richTextBox_DetailedData);
            this.splitContainer_Main.Panel2.Controls.Add(this.groupBox_ResultsDisplay);
            this.splitContainer_Main.Size = new System.Drawing.Size(874, 538);
            this.splitContainer_Main.SplitterDistance = 291;
            this.splitContainer_Main.TabIndex = 0;
            this.splitContainer_Main.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainer_Main_SplitterMoved);
            // 
            // groupBox_LimitStates
            // 
            this.groupBox_LimitStates.Controls.Add(this.label_LimitStates);
            this.groupBox_LimitStates.Controls.Add(this.button_DefineLimitStates);
            this.groupBox_LimitStates.Location = new System.Drawing.Point(12, 423);
            this.groupBox_LimitStates.Name = "groupBox_LimitStates";
            this.groupBox_LimitStates.Size = new System.Drawing.Size(225, 79);
            this.groupBox_LimitStates.TabIndex = 12;
            this.groupBox_LimitStates.TabStop = false;
            this.groupBox_LimitStates.Text = "Limit States";
            // 
            // label_LimitStates
            // 
            this.label_LimitStates.AutoSize = true;
            this.label_LimitStates.Location = new System.Drawing.Point(9, 49);
            this.label_LimitStates.Name = "label_LimitStates";
            this.label_LimitStates.Size = new System.Drawing.Size(110, 13);
            this.label_LimitStates.TabIndex = 1;
            this.label_LimitStates.Text = "No limit states defined";
            // 
            // button_DefineLimitStates
            // 
            this.button_DefineLimitStates.Location = new System.Drawing.Point(9, 19);
            this.button_DefineLimitStates.Name = "button_DefineLimitStates";
            this.button_DefineLimitStates.Size = new System.Drawing.Size(107, 23);
            this.button_DefineLimitStates.TabIndex = 25;
            this.button_DefineLimitStates.Text = "Define Limit States";
            this.button_DefineLimitStates.UseVisualStyleBackColor = true;
            this.button_DefineLimitStates.Click += new System.EventHandler(this.Button_DefineLimitStates_Click);
            // 
            // button_Analyze
            // 
            this.button_Analyze.Location = new System.Drawing.Point(209, 508);
            this.button_Analyze.Name = "button_Analyze";
            this.button_Analyze.Size = new System.Drawing.Size(75, 23);
            this.button_Analyze.TabIndex = 29;
            this.button_Analyze.Text = "Analyze";
            this.button_Analyze.UseVisualStyleBackColor = true;
            this.button_Analyze.Click += new System.EventHandler(this.Button_Analyze_Click);
            // 
            // groupBox_Loading
            // 
            this.groupBox_Loading.Controls.Add(this.label_Loads);
            this.groupBox_Loading.Controls.Add(this.button_Loads);
            this.groupBox_Loading.Controls.Add(this.checkBox_IncludePT);
            this.groupBox_Loading.Location = new System.Drawing.Point(12, 319);
            this.groupBox_Loading.Name = "groupBox_Loading";
            this.groupBox_Loading.Size = new System.Drawing.Size(225, 98);
            this.groupBox_Loading.TabIndex = 10;
            this.groupBox_Loading.TabStop = false;
            this.groupBox_Loading.Text = "Loading";
            // 
            // label_Loads
            // 
            this.label_Loads.AutoSize = true;
            this.label_Loads.Location = new System.Drawing.Point(12, 49);
            this.label_Loads.Name = "label_Loads";
            this.label_Loads.Size = new System.Drawing.Size(87, 13);
            this.label_Loads.TabIndex = 4;
            this.label_Loads.Text = "No loads defined";
            // 
            // button_Loads
            // 
            this.button_Loads.Location = new System.Drawing.Point(12, 19);
            this.button_Loads.Name = "button_Loads";
            this.button_Loads.Size = new System.Drawing.Size(107, 23);
            this.button_Loads.TabIndex = 21;
            this.button_Loads.Text = "Define Axial Forces";
            this.button_Loads.UseVisualStyleBackColor = true;
            this.button_Loads.Click += new System.EventHandler(this.Button_Loads_Click);
            // 
            // checkBox_IncludePT
            // 
            this.checkBox_IncludePT.AutoSize = true;
            this.checkBox_IncludePT.Location = new System.Drawing.Point(12, 72);
            this.checkBox_IncludePT.Name = "checkBox_IncludePT";
            this.checkBox_IncludePT.Size = new System.Drawing.Size(168, 17);
            this.checkBox_IncludePT.TabIndex = 23;
            this.checkBox_IncludePT.Text = "Axial Force Includes PT Force";
            this.checkBox_IncludePT.UseVisualStyleBackColor = true;
            this.checkBox_IncludePT.CheckedChanged += new System.EventHandler(this.CheckBox_IncludePT_CheckedChanged);
            // 
            // groupBox_DesignOptions
            // 
            this.groupBox_DesignOptions.Controls.Add(this.radioButton_Spirals);
            this.groupBox_DesignOptions.Controls.Add(this.radioButton_ClosedHoops);
            this.groupBox_DesignOptions.Location = new System.Drawing.Point(12, 236);
            this.groupBox_DesignOptions.Name = "groupBox_DesignOptions";
            this.groupBox_DesignOptions.Size = new System.Drawing.Size(225, 77);
            this.groupBox_DesignOptions.TabIndex = 9;
            this.groupBox_DesignOptions.TabStop = false;
            this.groupBox_DesignOptions.Text = "Design Options";
            // 
            // radioButton_Spirals
            // 
            this.radioButton_Spirals.AutoSize = true;
            this.radioButton_Spirals.Location = new System.Drawing.Point(9, 45);
            this.radioButton_Spirals.Name = "radioButton_Spirals";
            this.radioButton_Spirals.Size = new System.Drawing.Size(56, 17);
            this.radioButton_Spirals.TabIndex = 19;
            this.radioButton_Spirals.TabStop = true;
            this.radioButton_Spirals.Text = "Spirals";
            this.radioButton_Spirals.UseVisualStyleBackColor = true;
            this.radioButton_Spirals.CheckedChanged += new System.EventHandler(this.RadioButton_Spirals_CheckedChanged);
            // 
            // radioButton_ClosedHoops
            // 
            this.radioButton_ClosedHoops.AutoSize = true;
            this.radioButton_ClosedHoops.Location = new System.Drawing.Point(9, 22);
            this.radioButton_ClosedHoops.Name = "radioButton_ClosedHoops";
            this.radioButton_ClosedHoops.Size = new System.Drawing.Size(91, 17);
            this.radioButton_ClosedHoops.TabIndex = 17;
            this.radioButton_ClosedHoops.TabStop = true;
            this.radioButton_ClosedHoops.Text = "Closed Hoops";
            this.radioButton_ClosedHoops.UseVisualStyleBackColor = true;
            this.radioButton_ClosedHoops.CheckedChanged += new System.EventHandler(this.RadioButton_ClosedHoops_CheckedChanged);
            // 
            // button_DeleteCase
            // 
            this.button_DeleteCase.Location = new System.Drawing.Point(186, 27);
            this.button_DeleteCase.Name = "button_DeleteCase";
            this.button_DeleteCase.Size = new System.Drawing.Size(51, 23);
            this.button_DeleteCase.TabIndex = 5;
            this.button_DeleteCase.Text = "Delete";
            this.button_DeleteCase.UseVisualStyleBackColor = true;
            // 
            // button_NewAnalysis
            // 
            this.button_NewAnalysis.Location = new System.Drawing.Point(139, 27);
            this.button_NewAnalysis.Name = "button_NewAnalysis";
            this.button_NewAnalysis.Size = new System.Drawing.Size(41, 23);
            this.button_NewAnalysis.TabIndex = 3;
            this.button_NewAnalysis.Text = "New";
            this.button_NewAnalysis.UseVisualStyleBackColor = true;
            this.button_NewAnalysis.Click += new System.EventHandler(this.Button_NewAnalysis_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Select Analysis Case";
            // 
            // comboBox_AnalysisCases
            // 
            this.comboBox_AnalysisCases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AnalysisCases.FormattingEnabled = true;
            this.comboBox_AnalysisCases.Location = new System.Drawing.Point(12, 28);
            this.comboBox_AnalysisCases.Name = "comboBox_AnalysisCases";
            this.comboBox_AnalysisCases.Size = new System.Drawing.Size(121, 21);
            this.comboBox_AnalysisCases.TabIndex = 1;
            this.comboBox_AnalysisCases.SelectedIndexChanged += new System.EventHandler(this.ComboBox_AnalysisCases_SelectedIndexChanged);
            // 
            // groupBox_AnalysisOptions
            // 
            this.groupBox_AnalysisOptions.Controls.Add(this.label8);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_Tolerance);
            this.groupBox_AnalysisOptions.Controls.Add(this.label7);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_MaxIterations);
            this.groupBox_AnalysisOptions.Controls.Add(this.label6);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_NumberOfPoints);
            this.groupBox_AnalysisOptions.Controls.Add(this.label9);
            this.groupBox_AnalysisOptions.Controls.Add(this.label4);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_SingleAngle);
            this.groupBox_AnalysisOptions.Controls.Add(this.label5);
            this.groupBox_AnalysisOptions.Controls.Add(this.label3);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_MeshSize);
            this.groupBox_AnalysisOptions.Controls.Add(this.label2);
            this.groupBox_AnalysisOptions.Location = new System.Drawing.Point(12, 60);
            this.groupBox_AnalysisOptions.Name = "groupBox_AnalysisOptions";
            this.groupBox_AnalysisOptions.Size = new System.Drawing.Size(225, 170);
            this.groupBox_AnalysisOptions.TabIndex = 0;
            this.groupBox_AnalysisOptions.TabStop = false;
            this.groupBox_AnalysisOptions.Text = "Analysis Options";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(142, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "kips";
            // 
            // textBox_Tolerance
            // 
            this.textBox_Tolerance.Location = new System.Drawing.Point(81, 131);
            this.textBox_Tolerance.Name = "textBox_Tolerance";
            this.textBox_Tolerance.Size = new System.Drawing.Size(55, 20);
            this.textBox_Tolerance.TabIndex = 15;
            this.textBox_Tolerance.TextChanged += new System.EventHandler(this.TextBox_Tolerance_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 134);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Tolerance = ";
            // 
            // textBox_MaxIterations
            // 
            this.textBox_MaxIterations.Location = new System.Drawing.Point(99, 105);
            this.textBox_MaxIterations.Name = "textBox_MaxIterations";
            this.textBox_MaxIterations.Size = new System.Drawing.Size(55, 20);
            this.textBox_MaxIterations.TabIndex = 13;
            this.textBox_MaxIterations.TextChanged += new System.EventHandler(this.TextBox_MaxIterations_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 108);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Max Iterations = ";
            // 
            // textBox_NumberOfPoints
            // 
            this.textBox_NumberOfPoints.Location = new System.Drawing.Point(114, 51);
            this.textBox_NumberOfPoints.Name = "textBox_NumberOfPoints";
            this.textBox_NumberOfPoints.Size = new System.Drawing.Size(55, 20);
            this.textBox_NumberOfPoints.TabIndex = 9;
            this.textBox_NumberOfPoints.TextChanged += new System.EventHandler(this.TextBox_NumberOfPoints_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Number of Points = ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(121, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "deg";
            // 
            // textBox_SingleAngle
            // 
            this.textBox_SingleAngle.Location = new System.Drawing.Point(60, 79);
            this.textBox_SingleAngle.Name = "textBox_SingleAngle";
            this.textBox_SingleAngle.Size = new System.Drawing.Size(55, 20);
            this.textBox_SingleAngle.TabIndex = 11;
            this.textBox_SingleAngle.TextChanged += new System.EventHandler(this.TextBox_SingleAngle_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Angle = ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(143, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "in.";
            // 
            // textBox_MeshSize
            // 
            this.textBox_MeshSize.Location = new System.Drawing.Point(82, 25);
            this.textBox_MeshSize.Name = "textBox_MeshSize";
            this.textBox_MeshSize.Size = new System.Drawing.Size(55, 20);
            this.textBox_MeshSize.TabIndex = 7;
            this.textBox_MeshSize.TextChanged += new System.EventHandler(this.TextBox_MeshSize_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Mesh Size = ";
            // 
            // richTextBox_DetailedData
            // 
            this.richTextBox_DetailedData.Location = new System.Drawing.Point(305, 318);
            this.richTextBox_DetailedData.Name = "richTextBox_DetailedData";
            this.richTextBox_DetailedData.ReadOnly = true;
            this.richTextBox_DetailedData.Size = new System.Drawing.Size(256, 212);
            this.richTextBox_DetailedData.TabIndex = 100;
            this.richTextBox_DetailedData.TabStop = false;
            this.richTextBox_DetailedData.Text = "";
            // 
            // groupBox_ResultsDisplay
            // 
            this.groupBox_ResultsDisplay.Controls.Add(this.label11);
            this.groupBox_ResultsDisplay.Controls.Add(this.numericUpDown_Point);
            this.groupBox_ResultsDisplay.Controls.Add(this.label10);
            this.groupBox_ResultsDisplay.Controls.Add(this.comboBox_Analysis);
            this.groupBox_ResultsDisplay.Controls.Add(this.checkBox_DisplayReduced);
            this.groupBox_ResultsDisplay.Location = new System.Drawing.Point(13, 401);
            this.groupBox_ResultsDisplay.Name = "groupBox_ResultsDisplay";
            this.groupBox_ResultsDisplay.Size = new System.Drawing.Size(200, 130);
            this.groupBox_ResultsDisplay.TabIndex = 0;
            this.groupBox_ResultsDisplay.TabStop = false;
            this.groupBox_ResultsDisplay.Text = "Results Display";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Analysis Point =";
            // 
            // numericUpDown_Point
            // 
            this.numericUpDown_Point.Location = new System.Drawing.Point(95, 89);
            this.numericUpDown_Point.Name = "numericUpDown_Point";
            this.numericUpDown_Point.Size = new System.Drawing.Size(82, 20);
            this.numericUpDown_Point.TabIndex = 35;
            this.numericUpDown_Point.ValueChanged += new System.EventHandler(this.NumericUpDown_Point_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Select Case";
            // 
            // comboBox_Analysis
            // 
            this.comboBox_Analysis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Analysis.FormattingEnabled = true;
            this.comboBox_Analysis.Location = new System.Drawing.Point(11, 36);
            this.comboBox_Analysis.Name = "comboBox_Analysis";
            this.comboBox_Analysis.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Analysis.TabIndex = 31;
            this.comboBox_Analysis.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Analysis_SelectedIndexChanged);
            // 
            // checkBox_DisplayReduced
            // 
            this.checkBox_DisplayReduced.AutoSize = true;
            this.checkBox_DisplayReduced.Location = new System.Drawing.Point(11, 66);
            this.checkBox_DisplayReduced.Name = "checkBox_DisplayReduced";
            this.checkBox_DisplayReduced.Size = new System.Drawing.Size(102, 17);
            this.checkBox_DisplayReduced.TabIndex = 33;
            this.checkBox_DisplayReduced.Text = "Display reduced";
            this.checkBox_DisplayReduced.UseVisualStyleBackColor = true;
            this.checkBox_DisplayReduced.CheckedChanged += new System.EventHandler(this.CheckBox_DisplayReduced_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(898, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(811, 571);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 37;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // MomentCurvatureAnalysisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(898, 639);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.splitContainer_Main);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MomentCurvatureAnalysisForm";
            this.Text = "Moment Curvature Analysis";
            this.Load += new System.EventHandler(this.MomentCurvatureAnalysisForm_Load);
            this.splitContainer_Main.Panel1.ResumeLayout(false);
            this.splitContainer_Main.Panel1.PerformLayout();
            this.splitContainer_Main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_Main)).EndInit();
            this.splitContainer_Main.ResumeLayout(false);
            this.groupBox_LimitStates.ResumeLayout(false);
            this.groupBox_LimitStates.PerformLayout();
            this.groupBox_Loading.ResumeLayout(false);
            this.groupBox_Loading.PerformLayout();
            this.groupBox_DesignOptions.ResumeLayout(false);
            this.groupBox_DesignOptions.PerformLayout();
            this.groupBox_AnalysisOptions.ResumeLayout(false);
            this.groupBox_AnalysisOptions.PerformLayout();
            this.groupBox_ResultsDisplay.ResumeLayout(false);
            this.groupBox_ResultsDisplay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Point)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer_Main;
        private System.Windows.Forms.GroupBox groupBox_AnalysisOptions;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Button button_DeleteCase;
        private System.Windows.Forms.Button button_NewAnalysis;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_AnalysisCases;
        private System.Windows.Forms.TextBox textBox_NumberOfPoints;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_SingleAngle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_MeshSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox_DesignOptions;
        private System.Windows.Forms.CheckBox checkBox_IncludePT;
        private System.Windows.Forms.RadioButton radioButton_Spirals;
        private System.Windows.Forms.RadioButton radioButton_ClosedHoops;
        private System.Windows.Forms.TextBox textBox_MaxIterations;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_Tolerance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox_Loading;
        private System.Windows.Forms.Button button_Analyze;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.GroupBox groupBox_LimitStates;
        private System.Windows.Forms.Label label_LimitStates;
        private System.Windows.Forms.Button button_DefineLimitStates;
        private System.Windows.Forms.Label label_Loads;
        private System.Windows.Forms.Button button_Loads;
        private System.Windows.Forms.GroupBox groupBox_ResultsDisplay;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox_Analysis;
        private System.Windows.Forms.CheckBox checkBox_DisplayReduced;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown_Point;
        private System.Windows.Forms.RichTextBox richTextBox_DetailedData;
    }
}