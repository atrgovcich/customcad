﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReinforcedConcrete.PostTensioning;
using Utilities.Extensions.DotNetNative;
using CadPanel.DrawingElements;
using Materials;
using Utilities.Containers;

namespace CadPanel.Forms
{
    public partial class PostTensioningPropertiesForm : Form
    {
        private const int MIN_WIDTH = 400;
        private const int MIN_HEIGHT = 300;

        private int _currentRow = -1;

        private List<PostTensioningDefinition> _allPostTensioning;
        private List<PostTensioningInnerSection> _allInnerSections;
        private List<IGenericMaterial> _allMaterials;
        private ObservableList<IDrawingElement> _allDrawingElements;
        public bool Changed { get; set; } = false;

        public PostTensioningPropertiesForm(List<PostTensioningDefinition> allPostTensioning, List<PostTensioningInnerSection> allInnerSections, List<IGenericMaterial> allMaterials, ObservableList<IDrawingElement> allDrawingElements)
        {
            InitializeComponent();

            _allPostTensioning = allPostTensioning;
            _allDrawingElements = allDrawingElements;
            _allInnerSections = allInnerSections;
            _allMaterials = allMaterials;

            this.Resize += new EventHandler(ResizeFormComponents);
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PostTensioningPropertiesForm_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(MIN_WIDTH, MIN_HEIGHT);

            this.Width = 600;
            this.Height = 300;
            ResizeAll();
            PopulateDataGridView();

            dataGridView_Types.CellClick += new DataGridViewCellEventHandler(DataGridView_Types_CellClick);
        }

        private void DataGridView_Types_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dataGridView_Types.CurrentRow.Index;

            if (r <= dataGridView_Types.Rows.Count - 1)
            {
                _currentRow = r;
            }
            else
            {
                _currentRow = -1;
            }
        }

        private void PopulateDataGridView()
        {
            dataGridView_Types.Rows.Clear();

            for (int i = 0; i < _allPostTensioning.Count; i++)
            {
                PostTensioningDefinition pt = _allPostTensioning[i];

                dataGridView_Types.Rows.Add();
                dataGridView_Types[0, i].Value = pt.Name;
                dataGridView_Types[1, i].Value = pt.StrandType.GetDescription();
                dataGridView_Types[2, i].Value = pt.Material.Name;
                dataGridView_Types[3, i].Value = pt.OuterDiameter;
                dataGridView_Types[4, i].Value = pt.SteelArea;
            }
        }

        private void ResizeDataGridView()
        {
            dataGridView_Types.Width = this.Width - 50;
            dataGridView_Types.Height = this.Height - 120;
        }

        private void ResizeCloseButton()
        {
            int bWidth = button_Close.Width;
            int bHeight = button_Close.Height;

            button_Close.Location = new Point(this.Width - 30 - bWidth, this.Height - 50 - bHeight);
        }

        private void ResizeAddButton()
        {
            button_Add.Location = new Point(dataGridView_Types.Location.X, dataGridView_Types.Location.Y + dataGridView_Types.Height + 10);
        }

        private void ResizeEditButton()
        {
            button_Edit.Location = new Point(dataGridView_Types.Location.X + button_Add.Size.Width + 5, button_Add.Location.Y);
        }

        private void ResizeRemoveButton()
        {
            button_Remove.Location = new Point(dataGridView_Types.Location.X + button_Add.Size.Width + 5 + button_Edit.Size.Width + 5, button_Add.Location.Y);
        }

        private void ResizeFormComponents(object sender, EventArgs e)
        {
            ResizeAll();
        }

        private void ResizeAll()
        {
            ResizeDataGridView();
            ResizeAddButton();
            ResizeEditButton();
            ResizeRemoveButton();
            ResizeCloseButton();
        }

        private bool PostTensioningDefinitionInUse(PostTensioningDefinition pt)
        {
            if (_allDrawingElements != null)
            {
                for (int i = 0; i < _allDrawingElements.Count; i++)
                {
                    if (_allDrawingElements.GetType().Equals(typeof(PostTensioningG)) == true)
                    {
                        PostTensioningG ptG = (PostTensioningG)_allDrawingElements[i];

                        if (ptG.TypeOfPT.Equals(pt) == true)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private void Button_Remove_Click(object sender, EventArgs e)
        {
            if (_currentRow >= 0 && _currentRow < dataGridView_Types.Rows.Count)
            {
                PostTensioningDefinition pt = _allPostTensioning[_currentRow];

                if (PostTensioningDefinitionInUse(pt) == false)
                {
                    if (MessageBox.Show($"This will delete the following post tensioning object: {pt.Name}","Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        _allPostTensioning.RemoveAt(_currentRow);

                        PopulateDataGridView();

                        if (_currentRow > dataGridView_Types.Rows.Count - 1)
                        {
                            _currentRow = dataGridView_Types.Rows.Count - 1;
                        }

                        if (_currentRow >= 0)
                        {
                            dataGridView_Types.Rows[_currentRow].Cells[0].Selected = true;
                        }
                    }                    
                }
                else
                {
                    MessageBox.Show("The post-tensioning definition you are trying to delete is currently in use.  Delete the drawing elements that reference this definition first.");
                }
            }
        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            AddEditPostTensioningForm frm = new AddEditPostTensioningForm(false, _allPostTensioning, _allInnerSections, _allMaterials, null);

            frm.ShowDialog();

            frm.Dispose();

            PopulateDataGridView();
        }

        private void button_Edit_Click(object sender, EventArgs e)
        {
            if (_currentRow >= 0 && _currentRow < dataGridView_Types.Rows.Count)
            {
                PostTensioningDefinition defToEdit = GetPostTensioningDefinitionFromName(Convert.ToString(dataGridView_Types[0, _currentRow].Value));

                AddEditPostTensioningForm frm = new AddEditPostTensioningForm(true, _allPostTensioning, _allInnerSections, _allMaterials, defToEdit);

                frm.ShowDialog();

                frm.Dispose();

                PopulateDataGridView();

                if (_currentRow >= 0)
                {
                    for (int i = 0; i < dataGridView_Types.Rows.Count; i++)
                    {
                        if (i != _currentRow)
                        {
                            dataGridView_Types.Rows[i].Cells[0].Selected = false;
                        }
                        else
                        {
                            dataGridView_Types.Rows[i].Cells[0].Selected = true;
                        }
                    }
                }
            }
        }

        private PostTensioningDefinition GetPostTensioningDefinitionFromName(string name)
        {
            for (int i = 0; i < _allPostTensioning.Count; i++)
            {
                if (string.Compare(_allPostTensioning[i].Name, name, ignoreCase: true ) == 0)
                {
                    return _allPostTensioning[i];
                }
            }

            return null;
        }
    }
}
