﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using FiberSectionAnalysis.LimitStates;
using Materials;
using System.Drawing;

namespace CadPanel.Forms
{
    public partial class SelectLimitStatesForm : Form
    {
        private List<LimitState> _allLimitStates;
        private List<LimitState> _selectedLimitStates;
        private List<IGenericMaterial> _allMaterials;

        private int _currentRow = -1;

        #region Public Propertioes
        public List<LimitState> SelectedLimitStates
        {
            get
            {
                return _selectedLimitStates;
            }
        }
        #endregion

        public SelectLimitStatesForm(List<LimitState> allLimitStates, List<LimitState> selectedLimitStates, List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
                BindingFlags.Instance | BindingFlags.SetProperty, null,
                dataGridView_LimitStates, new object[] { true });

            _allMaterials = allMaterials;

            if (allLimitStates != null)
            {
                _allLimitStates = allLimitStates;
            }

            if (selectedLimitStates != null)
            {
                _selectedLimitStates = new List<LimitState>(selectedLimitStates);
            }

            dataGridView_LimitStates.CellContentClick += new DataGridViewCellEventHandler(DataGridView_LimitStates_CellContentClick);

            dataGridView_LimitStates.CellClick += new DataGridViewCellEventHandler(DataGridView_LimitStates_CellClick);

            this.Resize += new EventHandler(ResizeFormComponents);
        }

        private void SelectLimitStatesForm_Load(object sender, EventArgs e)
        {
            PopulateDataGridView_LimitStates();

            ResizeAll();
        }

        private void PopulateDataGridView_LimitStates()
        {
            if (_allLimitStates != null && _allLimitStates.Count > 0)
            {
                dataGridView_LimitStates.Rows.Clear();

                for (int i = 0; i < _allLimitStates.Count; i++)
                {
                    LimitState lS = _allLimitStates[i];

                    dataGridView_LimitStates.Rows.Add();

                    dataGridView_LimitStates[1, i].Value = lS.Material.Name;
                    dataGridView_LimitStates[2, i].Value = lS.Name;
                    dataGridView_LimitStates[3, i].Value = lS.CompressionLimitStrain;
                    dataGridView_LimitStates[4, i].Value = lS.TensionLimitStrain;

                    dataGridView_LimitStates[5, i].Value = (lS.TerminationLimitState == true) ? "True" : "False";
                }

                if (_selectedLimitStates != null && _selectedLimitStates.Count > 0)
                {
                    List<LimitState> lsToRemove = new List<LimitState>();

                    for(int i = 0; i < _selectedLimitStates.Count; i++)
                    {
                        if (_allLimitStates.Contains(_selectedLimitStates[i]) == true)
                        {
                            int ind = _allLimitStates.IndexOf(_selectedLimitStates[i]);

                            DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)dataGridView_LimitStates[0, ind];

                            cell.Value = true;
                        }
                        else
                        {
                            lsToRemove.Add(_selectedLimitStates[i]);
                        }
                    }

                    for (int i = 0; i < lsToRemove.Count; i++)
                    {
                        _selectedLimitStates.Remove(lsToRemove[i]);
                    }
                }


            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            this.Close();
        }

        private void DataGridView_LimitStates_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                _currentRow = e.RowIndex;
            }
            else
            {
                _currentRow = -1;
            }
        }
        private void DataGridView_LimitStates_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex>= 0 && e.ColumnIndex == 0)
            {
                DataGridViewRow row = dataGridView_LimitStates.Rows[e.RowIndex];

                bool boxChecked = false;

                if (dgv.IsCurrentCellDirty == true)
                {
                    boxChecked = Convert.ToBoolean(row.Cells[0].EditedFormattedValue);
                }
                else
                {
                    boxChecked = Convert.ToBoolean(row.Cells[0].FormattedValue);
                }

                if (boxChecked == true)
                {
                    if (_selectedLimitStates != null)
                    {
                        if (_selectedLimitStates.Contains(_allLimitStates[e.RowIndex]) == false)
                        {
                            _selectedLimitStates.Add(_allLimitStates[e.RowIndex]);
                        }
                    }
                    else
                    {
                        _selectedLimitStates = new List<LimitState>();

                        _selectedLimitStates.Add(_allLimitStates[e.RowIndex]);
                    }
                }
                else
                {
                    if (_selectedLimitStates != null)
                    {
                        if (_selectedLimitStates.Contains(_allLimitStates[e.RowIndex]) == true)
                        {
                            _selectedLimitStates.Remove(_allLimitStates[e.RowIndex]);
                        }
                    }
                }
            }
        }

        private void button_New_Click(object sender, EventArgs e)
        {
            AddOrEditLimitStateForm frm = new AddOrEditLimitStateForm(false, _allMaterials, null);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _allLimitStates.Add(frm.NewLimitState);

                PopulateDataGridView_LimitStates();
            }

            frm.Dispose();
        }

        private void button_Edit_Click(object sender, EventArgs e)
        {
            if (_currentRow >= 0)
            {
                LimitState lsToEdit = _allLimitStates[_currentRow];

                AddOrEditLimitStateForm frm = new AddOrEditLimitStateForm(true, _allMaterials, lsToEdit);

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    PopulateDataGridView_LimitStates();

                    SetDataGridViewSelectedRow(dataGridView_LimitStates, _currentRow);
                }
            }
        }

        private void SetDataGridViewSelectedRow(DataGridView dgv, int selectedRow)
        {
            dgv.ClearSelection();

            dgv.Rows[selectedRow].Cells[0].Selected = true;

            dgv.Rows[selectedRow].Selected = true;

            dgv.CurrentCell = dgv.Rows[selectedRow].Cells[0];
        }

        private void ResizeAll()
        {
            ResizeDataGridView();
            ResizeButtons();
        }

        private void ResizeDataGridView()
        {
            dataGridView_LimitStates.Width = this.Width - dataGridView_LimitStates.Location.X - 35;
            dataGridView_LimitStates.Height = this.Height - dataGridView_LimitStates.Location.Y - 120;
        }

        private void ResizeButtons()
        {
            button_Cancel.Location = new Point(this.Width - button_Cancel.Width - 35, this.Height - button_Cancel.Height - 50);
            button_OK.Location = new Point(button_Cancel.Location.X - 5 - button_OK.Width, button_Cancel.Location.Y);
            button_Delete.Location = new Point(dataGridView_LimitStates.Location.X + dataGridView_LimitStates.Width - button_Delete.Width,
                dataGridView_LimitStates.Location.Y + dataGridView_LimitStates.Height + 5);
            button_New.Location = new Point(button_Delete.Location.X - 5 - button_New.Width, button_Delete.Location.Y);
            button_Edit.Location = new Point(button_New.Location.X - 5 - button_Edit.Width, button_Delete.Location.Y);
        }

        private void ResizeFormComponents(object sender, EventArgs e)
        {
            ResizeAll();
        }
    }
}
