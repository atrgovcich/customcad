﻿namespace CadPanel.Forms.MaterialForms
{
    partial class MenegottoPintoSteelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Eqns = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.Label_Name = new System.Windows.Forms.Label();
            this.textBox_A2 = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.textBox_A1 = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.button_Calc = new System.Windows.Forms.Button();
            this.textBox_R = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.textBox_b = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.textBox_eu = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.textBox_ey = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.textBox_Es = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.textBox_fu = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.textBox_fy = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.button_ViewCyclicPlot = new System.Windows.Forms.Button();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.SuspendLayout();
            // 
            // button_Eqns
            // 
            this.button_Eqns.Location = new System.Drawing.Point(109, 294);
            this.button_Eqns.Name = "button_Eqns";
            this.button_Eqns.Size = new System.Drawing.Size(70, 25);
            this.button_Eqns.TabIndex = 167;
            this.button_Eqns.TabStop = false;
            this.button_Eqns.Text = "Equations";
            this.button_Eqns.UseVisualStyleBackColor = true;
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(319, 325);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(70, 25);
            this.button_Close.TabIndex = 165;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(243, 325);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 164;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(109, 325);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 166;
            this.button_ViewPlot.TabStop = false;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 161;
            this.Label_Name.Text = "Model Name";
            // 
            // textBox_A2
            // 
            this.textBox_A2.Location = new System.Drawing.Point(189, 239);
            this.textBox_A2.Name = "textBox_A2";
            this.textBox_A2.Size = new System.Drawing.Size(100, 20);
            this.textBox_A2.TabIndex = 159;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(186, 223);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(20, 13);
            this.Label3.TabIndex = 158;
            this.Label3.Text = "A2";
            // 
            // textBox_A1
            // 
            this.textBox_A1.Location = new System.Drawing.Point(189, 200);
            this.textBox_A1.Name = "textBox_A1";
            this.textBox_A1.Size = new System.Drawing.Size(100, 20);
            this.textBox_A1.TabIndex = 157;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(186, 185);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(20, 13);
            this.Label2.TabIndex = 156;
            this.Label2.Text = "A1";
            // 
            // button_Calc
            // 
            this.button_Calc.Location = new System.Drawing.Point(294, 80);
            this.button_Calc.Name = "button_Calc";
            this.button_Calc.Size = new System.Drawing.Size(40, 22);
            this.button_Calc.TabIndex = 155;
            this.button_Calc.Text = "Calc";
            this.button_Calc.UseVisualStyleBackColor = true;
            this.button_Calc.Click += new System.EventHandler(this.Button_Calc_Click);
            // 
            // textBox_R
            // 
            this.textBox_R.Location = new System.Drawing.Point(189, 162);
            this.textBox_R.Name = "textBox_R";
            this.textBox_R.Size = new System.Drawing.Size(100, 20);
            this.textBox_R.TabIndex = 154;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(186, 146);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(113, 13);
            this.Label19.TabIndex = 153;
            this.Label19.Text = "R = transition constant";
            // 
            // textBox_b
            // 
            this.textBox_b.Location = new System.Drawing.Point(15, 200);
            this.textBox_b.Name = "textBox_b";
            this.textBox_b.Size = new System.Drawing.Size(100, 20);
            this.textBox_b.TabIndex = 151;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(12, 185);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(134, 13);
            this.Label20.TabIndex = 152;
            this.Label20.Text = "b = material hardening ratio";
            // 
            // textBox_eu
            // 
            this.textBox_eu.Location = new System.Drawing.Point(189, 120);
            this.textBox_eu.Name = "textBox_eu";
            this.textBox_eu.Size = new System.Drawing.Size(100, 20);
            this.textBox_eu.TabIndex = 150;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(186, 104);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 149;
            this.Label9.Text = "Ultimate strain";
            // 
            // textBox_ey
            // 
            this.textBox_ey.Location = new System.Drawing.Point(189, 81);
            this.textBox_ey.Name = "textBox_ey";
            this.textBox_ey.Size = new System.Drawing.Size(100, 20);
            this.textBox_ey.TabIndex = 148;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(186, 67);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(58, 13);
            this.Label10.TabIndex = 147;
            this.Label10.Text = "Yield strain";
            // 
            // textBox_Es
            // 
            this.textBox_Es.Location = new System.Drawing.Point(14, 162);
            this.textBox_Es.Name = "textBox_Es";
            this.textBox_Es.Size = new System.Drawing.Size(100, 20);
            this.textBox_Es.TabIndex = 145;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(12, 146);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(158, 13);
            this.Label8.TabIndex = 146;
            this.Label8.Text = "Eo = initial tangent modulus (ksi)";
            // 
            // textBox_fu
            // 
            this.textBox_fu.Location = new System.Drawing.Point(14, 120);
            this.textBox_fu.Name = "textBox_fu";
            this.textBox_fu.Size = new System.Drawing.Size(100, 20);
            this.textBox_fu.TabIndex = 143;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(12, 104);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(97, 13);
            this.Label7.TabIndex = 144;
            this.Label7.Text = "Ultimate stress (ksi)";
            // 
            // textBox_fy
            // 
            this.textBox_fy.Location = new System.Drawing.Point(14, 81);
            this.textBox_fy.Name = "textBox_fy";
            this.textBox_fy.Size = new System.Drawing.Size(100, 20);
            this.textBox_fy.TabIndex = 142;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(12, 67);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(82, 13);
            this.Label6.TabIndex = 141;
            this.Label6.Text = "Yield stress (ksi)";
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(287, 22);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 206;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(340, 22);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 205;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // button_ViewCyclicPlot
            // 
            this.button_ViewCyclicPlot.Location = new System.Drawing.Point(12, 325);
            this.button_ViewCyclicPlot.Name = "button_ViewCyclicPlot";
            this.button_ViewCyclicPlot.Size = new System.Drawing.Size(91, 25);
            this.button_ViewCyclicPlot.TabIndex = 207;
            this.button_ViewCyclicPlot.Text = "View Cyclic Plot";
            this.button_ViewCyclicPlot.UseVisualStyleBackColor = true;
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 25);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(266, 21);
            this.comboBox_Names.TabIndex = 208;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // MenegottoPintoSteelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 361);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.button_ViewCyclicPlot);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.button_Eqns);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.textBox_A2);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.textBox_A1);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.button_Calc);
            this.Controls.Add(this.textBox_R);
            this.Controls.Add(this.Label19);
            this.Controls.Add(this.textBox_b);
            this.Controls.Add(this.Label20);
            this.Controls.Add(this.textBox_eu);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.textBox_ey);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.textBox_Es);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.textBox_fu);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.textBox_fy);
            this.Controls.Add(this.Label6);
            this.MaximumSize = new System.Drawing.Size(420, 400);
            this.MinimumSize = new System.Drawing.Size(420, 400);
            this.Name = "MenegottoPintoSteelForm";
            this.Text = "Menegotto Pinto Steel";
            this.Load += new System.EventHandler(this.MenegottoPintoSteelForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button button_Eqns;
        internal System.Windows.Forms.Button button_Close;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.TextBox textBox_A2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox textBox_A1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button button_Calc;
        internal System.Windows.Forms.TextBox textBox_R;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TextBox textBox_b;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.TextBox textBox_eu;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox textBox_ey;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox textBox_Es;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox textBox_fu;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox textBox_fy;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Delete;
        internal System.Windows.Forms.Button button_ViewCyclicPlot;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}