﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Materials;
using Materials.Inelastic.Concrete;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using FiberSectionAnalysis.LimitStates;
using CadPanel.UserControls;

namespace CadPanel.Forms.MaterialForms
{
    public partial class ManderUnconfinedConcreteForm : Form
    {
        private List<IGenericMaterial> _allMaterials;
        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();
        private List<IGenericMaterial> _addedMaterials = new List<IGenericMaterial>();
        private bool _onNewMaterial = false;
        private List<LimitState> _addedLimitStates = new List<LimitState>();

        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<IGenericMaterial> AddedMaterials
        {
            get
            {
                return _addedMaterials;
            }
        }

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }

        public ManderUnconfinedConcreteForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;

            comboBox_Names.BeforeUpdate += new CancelEventHandler(ComboBox_Names_BeforeUpdate);
        }

        private void ManderUnconfinedConcreteForm_Load(object sender, EventArgs e)
        {
            PopulateNamesComboBox();
        }

        private LimitState CreateNewLimitState()
        {
            string name = comboBox_Names.Text;

            return CreateNewLimitState(name);
        }

        private LimitState CreateNewLimitState(string name)
        {
            IGenericMaterial genericMat = GetMaterialFromName(name, _allMaterials);

            LimitState ls = new LimitState()
            {
                Material = (ManderConcrete)genericMat,
                CompressionLimitStrain = 0.003,
                TensionLimitStrain = 1.0,
                TerminationLimitState = true,
                Name = "ACI Crushing"
            };

            return ls;
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            if (_onNewMaterial == true)
            {
                _addedLimitStates.Add(CreateNewLimitState());
            }

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderConcrete castNat = (ManderConcrete)genericMat;

                    castNat.AutoChooseElasticModulusEquation = checkBox_AutoChooseEc.Checked;

                    if (checkBox_AutoChooseEc.Checked == false)
                    {
                        if (radioButton_NormalStrength.Checked == true)
                        {
                            castNat.ElasticModulusEquation = ElasticModulusEquationType.NormalStrength;
                        }
                        else
                        {
                            castNat.ElasticModulusEquation = ElasticModulusEquationType.HighStrength;
                        }
                    }

                    castNat.Fc = Convert.ToDouble(textBox_fc.Text);
                    castNat.Fpc = Convert.ToDouble(textBox_fpc.Text);
                    castNat.Ec = Convert.ToDouble(textBox_Ec.Text);
                    castNat.eps = Convert.ToDouble(textBox_eps.Text);
                    castNat.esp = Convert.ToDouble(textBox_esp.Text);
                    castNat.ecu = Convert.ToDouble(textBox_ecu.Text);
                    castNat.et = -Convert.ToDouble(textBox_et.Text);
                    castNat.Ft = -Convert.ToDouble(textBox_ft.Text);

                }
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateInputs(out string message)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc <= 0)
                {
                    message = "Compressive strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Compressive strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
            {
                if (Ec <= 0)
                {
                    message = "Elastic modulus must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Elastic modulus must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_eps.Text, out double eps) == true)
            {
                if (eps <= 0)
                {
                    message = "Strain at peak stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Strain at peak stress must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_esp.Text, out double esp) == true)
            {
                if (esp <= 0)
                {
                    message = "Spalling strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Spalling strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ecu.Text, out double ecu) == true)
            {
                if (ecu <= 0)
                {
                    message = "Ultimate strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_fpc.Text, out double fpc) == true)
            {
                if (fpc < 0)
                {
                    message = "Post crushing strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Post crushing strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_et.Text, out double et) == true)
            {
                if (et < 0)
                {
                    message = "Tensile strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft < 0)
                {
                    message = "Tensile stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile stress must be a numeric value.";

                return false;
            }

            message = string.Empty;

            return true;
        }

        private void Button_ViewPlot_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                ManderConcrete castMat = new ManderConcrete(genericMat.Name);

                castMat.Fc = Convert.ToDouble(textBox_fc.Text);
                castMat.Fpc = Convert.ToDouble(textBox_fpc.Text);
                castMat.Ec = Convert.ToDouble(textBox_Ec.Text);
                castMat.eps = Convert.ToDouble(textBox_eps.Text);
                castMat.esp = Convert.ToDouble(textBox_esp.Text);
                castMat.ecu = Convert.ToDouble(textBox_ecu.Text);
                castMat.et = -Convert.ToDouble(textBox_et.Text);
                castMat.Ft = -Convert.ToDouble(textBox_ft.Text);

                StressStrainChartForm frm = new StressStrainChartForm(castMat, 2.0 * castMat.et, castMat.esp);

                frm.ShowDialog();

                frm.Dispose();
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddMaterialNameForm frm = new AddMaterialNameForm(_allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ManderConcrete newMat = new ManderConcrete(frm.NewName);

                _allMaterials.Add(newMat);

                _addedMaterials.Add(newMat);

                comboBox_Names.Items.Add(newMat.Name);

                comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;

                _onNewMaterial = true;
            }

            frm.Dispose();
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete the selected material.  Any elements utilizing this material will have their material definition reset.  Proceed?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _onNewMaterial = false;

                int selIndex = comboBox_Names.SelectedIndex;

                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                comboBox_Names.Items.RemoveAt(selIndex);

                if (comboBox_Names.Items.Count > 0)
                {
                    if (selIndex <= comboBox_Names.Items.Count - 1)
                    {
                        comboBox_Names.SelectedIndex = selIndex;
                    }
                    else
                    {
                        comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;
                    }
                }
                else
                {
                    comboBox_Names.SelectedIndex = -1;

                    ComboBox_Names_SelectedIndexChanged(comboBox_Names, new EventArgs());
                }

                _allMaterials.Remove(genericMat);

                _deletedMaterials.Add(genericMat);
            }
        }

        private void ComboBox_Names_BeforeUpdate(object sender, CancelEventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (_onNewMaterial == true)
            {
                if (cB.PreviousIndex >= 0)
                {
                    _addedLimitStates.Add(CreateNewLimitState(Convert.ToString(cB.Items[cB.PreviousIndex])));
                }

                _onNewMaterial = false;
            }
        }

        private void ComboBox_Names_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cB = (ComboBox)sender;

            if (cB.SelectedIndex >= 0)
            {
                IGenericMaterial genericMat = GetMaterialFromName(cB.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderConcrete castMat = (ManderConcrete)genericMat;

                    textBox_fc.Text = Convert.ToString(castMat.Fc);
                    textBox_fpc.Text = Convert.ToString(castMat.Fpc);
                    textBox_ft.Text = Convert.ToString(-castMat.Ft);
                    textBox_et.Text = Convert.ToString(-castMat.et);
                    textBox_eps.Text = Convert.ToString(castMat.eps);
                    textBox_ecu.Text = Convert.ToString(castMat.ecu);
                    textBox_esp.Text = Convert.ToString(castMat.esp);
                    textBox_Ec.Text = Convert.ToString(castMat.Ec);

                    checkBox_AutoChooseEc.Checked = castMat.AutoChooseElasticModulusEquation;

                    if (castMat.ElasticModulusEquation.Equals(ElasticModulusEquationType.NormalStrength) == true)
                    {
                        radioButton_NormalStrength.Checked = true;
                    }
                    else
                    {
                        radioButton_HighStrength.Checked = true;
                    }
                }
            }
            else
            {
                textBox_fc.Clear();
                textBox_fpc.Clear();
                textBox_ft.Clear();
                textBox_et.Clear();
                textBox_eps.Clear();
                textBox_ecu.Clear();
                textBox_esp.Clear();
                textBox_Ec.Clear();
            }
        }

        private void Button_Calc_E_Click(object sender, EventArgs e)
        {
            IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

            if (genericMat != null)
            {
                ManderConcrete castMat = (ManderConcrete)genericMat;

                if (double.TryParse(textBox_fc.Text, out double fc) == true)
                {
                    if (fc > 0)
                    {
                        if (checkBox_AutoChooseEc.Checked == true)
                        {
                            textBox_Ec.Text = Convert.ToString(Round(castMat.ComputeElasticModulus(fc), 2));
                        }
                        else
                        {
                            ElasticModulusEquationType eqnType = (radioButton_NormalStrength.Checked == true) ? ElasticModulusEquationType.NormalStrength : ElasticModulusEquationType.HighStrength;

                            textBox_Ec.Text = Convert.ToString(Round(castMat.ComputeElasticModulus(fc, eqnType), 2));
                        }
                    }
                    else
                    {
                        MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
                }
            }

            
        }

        private void CheckBox_AutoChooseEc_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            if (cB.Checked == true)
            {
                groupBox_ElasticModulus.Hide();
            }
            else
            {
                groupBox_ElasticModulus.Show();
            }
        }

        private void Button_Calc_ft_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc > 0)
                {
                    textBox_ft.Text = Convert.ToString(Round(7.5 * Sqrt(fc * 1000.0) / 1000.0, 3));
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Calc_et_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft > 0)
                {
                    if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
                    {
                        if (Ec > 0)
                        {
                            textBox_et.Text = Convert.ToString(Round(ft / Ec, 5));
                        }
                        else
                        {
                            MessageBox.Show("The elastic modulus must be a positive value.", "Error", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The elastic modulus must be a numeric value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }


        private void PopulateNamesComboBox()
        {
            if (_allMaterials != null)
            {
                comboBox_Names.Items.Clear();

                for (int i = 0; i < _allMaterials.Count; i++)
                {
                    IGenericMaterial genericMat = _allMaterials[i];

                    if (genericMat.GetType().Equals(typeof(ManderConcrete)) == true)
                    {
                        comboBox_Names.Items.Add(genericMat.Name);
                    }
                }

                if (comboBox_Names.Items.Count > 0)
                {
                    comboBox_Names.SelectedIndex = 0;
                }
            }
        }
    }
}
