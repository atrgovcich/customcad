﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Materials;
using Materials.Inelastic.Concrete;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using FiberSectionAnalysis.LimitStates;
using System.ComponentModel;
using CadPanel.UserControls;

namespace CadPanel.Forms.MaterialForms
{
    public partial class HsuConcreteForm : Form
    {
        private List<IGenericMaterial> _allMaterials;
        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();
        private List<IGenericMaterial> _addedMaterials = new List<IGenericMaterial>();
        private List<LimitState> _addedLimitStates = new List<LimitState>();
        private bool _onNewMaterial = false;
        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<IGenericMaterial> AddedMaterials
        {
            get
            {
                return _addedMaterials;
            }
        }

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }

        public HsuConcreteForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;

            comboBox_Names.BeforeUpdate += new CancelEventHandler(ComboBox_Names_BeforeUpdate);
        }

        private void HsuConcreteForm_Load(object sender, EventArgs e)
        {
            PopulateNamesComboBox();
        }

        private void PopulateNamesComboBox()
        {
            if (_allMaterials != null)
            {
                comboBox_Names.Items.Clear();

                for (int i = 0; i < _allMaterials.Count; i++)
                {
                    IGenericMaterial genericMat = _allMaterials[i];

                    if (genericMat.GetType().Equals(typeof(HsuHighStrengthConcrete)) == true)
                    {
                        comboBox_Names.Items.Add(genericMat.Name);
                    }
                }

                if (comboBox_Names.Items.Count > 0)
                {
                    comboBox_Names.SelectedIndex = 0;
                }
            }
        }

        private LimitState CreateNewLimitState()
        {
            IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

            LimitState ls = new LimitState()
            {
                Material = (HsuHighStrengthConcrete)genericMat,
                CompressionLimitStrain = 0.003,
                TensionLimitStrain = 1.0,
                TerminationLimitState = true,
                Name = "ACI Crushing"
            };

            return ls;
        }

        private LimitState CreateNewLimitState(string name)
        {
            IGenericMaterial genericMat = GetMaterialFromName(name, _allMaterials);

            LimitState ls = new LimitState()
            {
                Material = (HsuHighStrengthConcrete)genericMat,
                CompressionLimitStrain = 0.003,
                TensionLimitStrain = 1.0,
                TerminationLimitState = true,
                Name = "ACI Crushing"
            };

            return ls;
        }

        private void ComboBox_Names_BeforeUpdate(object sender, CancelEventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (_onNewMaterial == true)
            {
                if (cB.PreviousIndex >= 0)
                {
                    _addedLimitStates.Add(CreateNewLimitState(Convert.ToString(cB.Items[cB.PreviousIndex])));
                }

                _onNewMaterial = false;
            }
        }

        private void ComboBox_Names_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cB = (ComboBox)sender;

            if (cB.SelectedIndex >= 0)
            {
                IGenericMaterial genericMat = GetMaterialFromName(cB.Text, _allMaterials);

                if (genericMat != null)
                {
                    HsuHighStrengthConcrete hsuMat = (HsuHighStrengthConcrete)genericMat;

                    textBox_fc.Text = Convert.ToString(hsuMat.Fc);
                    textBox_ft.Text = Convert.ToString(-hsuMat.Ft);
                    textBox_et.Text = Convert.ToString(-hsuMat.et);
                    textBox_eps.Text = Convert.ToString(hsuMat.eps);
                    textBox_ecu.Text = Convert.ToString(hsuMat.ecu);
                    textBox_e3f.Text = Convert.ToString(hsuMat.ec3);
                    textBox_Ec.Text = Convert.ToString(hsuMat.Ec);
                }
            }
            else
            {
                textBox_fc.Clear();
                textBox_ft.Clear();
                textBox_et.Clear();
                textBox_eps.Clear();
                textBox_ecu.Clear();
                textBox_e3f.Clear();
                textBox_Ec.Clear();
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            if (_onNewMaterial == true)
            {
                _addedLimitStates.Add(CreateNewLimitState());
            }

            this.Close();
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddMaterialNameForm frm = new AddMaterialNameForm(_allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                HsuHighStrengthConcrete newMat = new HsuHighStrengthConcrete(frm.NewName);

                _allMaterials.Add(newMat);

                _addedMaterials.Add(newMat);

                comboBox_Names.Items.Add(newMat.Name);

                comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;

                _onNewMaterial = true;
            }

            frm.Dispose();
        }

        private bool ValidateInputs(out string message)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc <= 0)
                {
                    message = "Compressive strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Compressive strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
            {
                if (Ec <= 0)
                {
                    message = "Elastic modulus must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Elastic modulus must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_eps.Text, out double eps) == true)
            {
                if (eps <= 0)
                {
                    message = "Strain at peak stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Strain at peak stress must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_e3f.Text, out double ec3) == true)
            {
                if (ec3 <= 0)
                {
                    message = "Strain at 0.3*f'c must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Strain at 0.3*f'c must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ecu.Text, out double ecu) == true)
            {
                if (ecu <= 0)
                {
                    message = "Ultimate strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_et.Text, out double et) == true)
            {
                if (et <= 0)
                {
                    message = "Tensile strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft <= 0)
                {
                    message = "Tensile stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile stress must be a numeric value.";

                return false;
            }

            message = string.Empty;

            return true;
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete the selected material.  Any elements utilizing this material will have their material definition reset.  Proceed?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _onNewMaterial = false;

                int selIndex = comboBox_Names.SelectedIndex;

                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                comboBox_Names.Items.RemoveAt(selIndex);

                if (comboBox_Names.Items.Count > 0)
                {
                    if (selIndex <= comboBox_Names.Items.Count - 1)
                    {
                        comboBox_Names.SelectedIndex = selIndex;
                    }
                    else
                    {
                        comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;
                    }
                }
                else
                {
                    comboBox_Names.SelectedIndex = -1;

                    ComboBox_Names_SelectedIndexChanged(comboBox_Names, new EventArgs());
                }

                _allMaterials.Remove(genericMat);

                _deletedMaterials.Add(genericMat);
            }
        }

        private void Button_Calc_E_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc > 0)
                {
                    textBox_Ec.Text = Convert.ToString(Round((40000.0 * Sqrt(fc * 1000.0) + 1000000.0) / 1000.0, 2));
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Calc_e3f_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc > 0)
                {
                    if (double.TryParse(textBox_eps.Text, out double eps) == true)
                    {
                        if (eps > 0)
                        {
                            textBox_e3f.Text = Convert.ToString(Round(HsuConcreteFind_e_lim(fc, eps), 5));
                        }
                        else
                        {
                            MessageBox.Show("The strain at peak stress must be a positive value.", "Error", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The strain at peak stress must be a numeric value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private double HsuConcreteFind_e_lim(double fc, double ecp)
        {
            double fc_mpa = fc * 1000.0 * 0.00689475729; //convert to mpa
            double g_conc = 2402.77; //Weight of concrete in kg/m^3

            double low = ecp;
            double high = 10.0 * ecp;

            double ec = (low + high) / 2.0;

            double target = 0.3 * fc_mpa;

            double tolerance = 0.001; // 1/10th of 1%

            double stress = 10.0 * fc_mpa;

            while (Abs(stress - target) / target > tolerance)
            {
                double beta = Pow(fc_mpa / 65.23, 3) + 2.59;
                double Eit = 0.0736 * Pow(g_conc, 1.51) * Pow(fc_mpa, 0.3);
                double e_0 = (1680.0 + 7.1 * fc_mpa) * Pow(10.0, -6);
                double n = 0;

                if (ec <= ecp)
                {
                    n = 1;
                }
                else
                {
                    if (fc_mpa <= 62)
                    {
                        n = 1;
                    }
                    else if (fc_mpa > 62 && fc_mpa <= 76)
                    {
                        n = 2;
                    }
                    else if (fc_mpa > 76 && fc_mpa <= 90)
                    {
                        n = 3;
                    }
                    else
                    {
                        n = 5;
                    }
                }

                if (ec > 0)
                {
                    stress = fc_mpa * ((n * beta * (ec / ecp)) / (n * beta - 1 + Pow(ec / ecp, n * beta)));
                }
                else
                {
                    stress = 0;
                }

                if (Abs(stress -target) / target > tolerance)
                {
                    if (stress < target)
                    {
                        high = ec;
                    }
                    else
                    {
                        low = ec;
                    }

                    ec = (high + low) / 2.0;
                }
            }

            return ec;
        }

        private void Button_Calc_ft_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc > 0)
                {
                    textBox_ft.Text = Convert.ToString(Round(7.5 * Sqrt(fc * 1000.0) / 1000.0, 3));
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Calc_et_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft > 0)
                {
                    if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
                    {
                        if (Ec > 0)
                        {
                            textBox_et.Text = Convert.ToString(Round(ft / Ec, 5));
                        }
                        else
                        {
                            MessageBox.Show("The elastic modulus must be a positive value.", "Error", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The elastic modulus must be a numeric value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                if (genericMat != null)
                {
                    HsuHighStrengthConcrete hsuMat = (HsuHighStrengthConcrete)genericMat;

                    hsuMat.Fc = Convert.ToDouble(textBox_fc.Text);
                    hsuMat.Ec = Convert.ToDouble(textBox_Ec.Text);
                    hsuMat.eps = Convert.ToDouble(textBox_eps.Text);
                    hsuMat.ec3 = Convert.ToDouble(textBox_e3f.Text);
                    hsuMat.ecu = Convert.ToDouble(textBox_ecu.Text);
                    hsuMat.et = -Convert.ToDouble(textBox_et.Text);
                    hsuMat.Ft = -Convert.ToDouble(textBox_ft.Text);
                }
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void Button_ViewPlot_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                HsuHighStrengthConcrete hsuMat = new HsuHighStrengthConcrete(genericMat.Name);

                hsuMat.Fc = Convert.ToDouble(textBox_fc.Text);
                hsuMat.Ec = Convert.ToDouble(textBox_Ec.Text);
                hsuMat.eps = Convert.ToDouble(textBox_eps.Text);
                hsuMat.ec3 = Convert.ToDouble(textBox_e3f.Text);
                hsuMat.ecu = Convert.ToDouble(textBox_ecu.Text);
                hsuMat.et = -Convert.ToDouble(textBox_et.Text);
                hsuMat.Ft = -Convert.ToDouble(textBox_ft.Text);

                StressStrainChartForm frm = new StressStrainChartForm(hsuMat, 2.0 * hsuMat.et, hsuMat.ecu);

                frm.ShowDialog();

                frm.Dispose();
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }

        }
    }
}
