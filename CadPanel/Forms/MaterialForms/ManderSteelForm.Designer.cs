﻿namespace CadPanel.Forms.MaterialForms
{
    partial class ManderSteelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.Label_Name = new System.Windows.Forms.Label();
            this.textBox_p = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.button_Calc = new System.Windows.Forms.Button();
            this.textBox_esh = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.textBox_eu = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.textBox_ey = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.textBox_Es = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.textBox_fu = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.textBox_fy = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.SuspendLayout();
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(19, 325);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 154;
            this.button_ViewPlot.TabStop = false;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(322, 325);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(70, 25);
            this.button_Cancel.TabIndex = 153;
            this.button_Cancel.Text = "Close";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(246, 325);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 152;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 149;
            this.Label_Name.Text = "Model Name";
            // 
            // textBox_p
            // 
            this.textBox_p.Location = new System.Drawing.Point(19, 211);
            this.textBox_p.Name = "textBox_p";
            this.textBox_p.Size = new System.Drawing.Size(100, 20);
            this.textBox_p.TabIndex = 146;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(16, 195);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(67, 13);
            this.Label2.TabIndex = 147;
            this.Label2.Text = "P (exponent)";
            // 
            // button_Calc
            // 
            this.button_Calc.Location = new System.Drawing.Point(273, 80);
            this.button_Calc.Name = "button_Calc";
            this.button_Calc.Size = new System.Drawing.Size(40, 22);
            this.button_Calc.TabIndex = 145;
            this.button_Calc.Text = "Calc";
            this.button_Calc.UseVisualStyleBackColor = true;
            this.button_Calc.Click += new System.EventHandler(this.Button_Calc_Click);
            // 
            // textBox_esh
            // 
            this.textBox_esh.Location = new System.Drawing.Point(19, 165);
            this.textBox_esh.Name = "textBox_esh";
            this.textBox_esh.Size = new System.Drawing.Size(100, 20);
            this.textBox_esh.TabIndex = 144;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(16, 149);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(124, 13);
            this.Label11.TabIndex = 143;
            this.Label11.Text = "Strain at strain hardening";
            // 
            // textBox_eu
            // 
            this.textBox_eu.Location = new System.Drawing.Point(167, 121);
            this.textBox_eu.Name = "textBox_eu";
            this.textBox_eu.Size = new System.Drawing.Size(100, 20);
            this.textBox_eu.TabIndex = 142;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(164, 105);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 141;
            this.Label9.Text = "Ultimate strain";
            // 
            // textBox_ey
            // 
            this.textBox_ey.Location = new System.Drawing.Point(167, 82);
            this.textBox_ey.Name = "textBox_ey";
            this.textBox_ey.Size = new System.Drawing.Size(100, 20);
            this.textBox_ey.TabIndex = 140;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(164, 68);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(58, 13);
            this.Label10.TabIndex = 139;
            this.Label10.Text = "Yield strain";
            // 
            // textBox_Es
            // 
            this.textBox_Es.Location = new System.Drawing.Point(167, 165);
            this.textBox_Es.Name = "textBox_Es";
            this.textBox_Es.Size = new System.Drawing.Size(100, 20);
            this.textBox_Es.TabIndex = 137;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(164, 149);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(102, 13);
            this.Label8.TabIndex = 138;
            this.Label8.Text = "Elastic modulus (ksi)";
            // 
            // textBox_fu
            // 
            this.textBox_fu.Location = new System.Drawing.Point(19, 121);
            this.textBox_fu.Name = "textBox_fu";
            this.textBox_fu.Size = new System.Drawing.Size(100, 20);
            this.textBox_fu.TabIndex = 135;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(16, 105);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(97, 13);
            this.Label7.TabIndex = 136;
            this.Label7.Text = "Ultimate stress (ksi)";
            // 
            // textBox_fy
            // 
            this.textBox_fy.Location = new System.Drawing.Point(19, 82);
            this.textBox_fy.Name = "textBox_fy";
            this.textBox_fy.Size = new System.Drawing.Size(100, 20);
            this.textBox_fy.TabIndex = 134;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(16, 68);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(82, 13);
            this.Label6.TabIndex = 133;
            this.Label6.Text = "Yield stress (ksi)";
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(287, 21);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 156;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(340, 21);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 155;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 24);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(266, 21);
            this.comboBox_Names.TabIndex = 157;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // ManderSteelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 361);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.textBox_p);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.button_Calc);
            this.Controls.Add(this.textBox_esh);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.textBox_eu);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.textBox_ey);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.textBox_Es);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.textBox_fu);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.textBox_fy);
            this.Controls.Add(this.Label6);
            this.MaximumSize = new System.Drawing.Size(420, 400);
            this.MinimumSize = new System.Drawing.Size(420, 400);
            this.Name = "ManderSteelForm";
            this.Text = "Mander Steel";
            this.Load += new System.EventHandler(this.ManderSteelForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.Button button_Cancel;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.TextBox textBox_p;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button button_Calc;
        internal System.Windows.Forms.TextBox textBox_esh;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox textBox_eu;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox textBox_ey;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox textBox_Es;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox textBox_fu;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox textBox_fy;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Delete;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}