﻿namespace CadPanel.Forms.MaterialForms
{
    partial class ModifiedRambergOsgoodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Name = new System.Windows.Forms.Label();
            this.textBox_r = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.textBox_fp0 = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.textBox_C = new System.Windows.Forms.TextBox();
            this.Label19 = new System.Windows.Forms.Label();
            this.textBox_Fpc = new System.Windows.Forms.TextBox();
            this.Label20 = new System.Windows.Forms.Label();
            this.textBox_eu = new System.Windows.Forms.TextBox();
            this.Label9 = new System.Windows.Forms.Label();
            this.textBox_Es = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.textBox_fu = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.textBox_fy = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.button_ViewCyclicPlot = new System.Windows.Forms.Button();
            this.button_Eqns = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.verticalButton1 = new CadPanel.VerticalButton();
            this.SuspendLayout();
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 171;
            this.Label_Name.Text = "Model Name";
            // 
            // textBox_r
            // 
            this.textBox_r.Location = new System.Drawing.Point(189, 156);
            this.textBox_r.Name = "textBox_r";
            this.textBox_r.Size = new System.Drawing.Size(100, 20);
            this.textBox_r.TabIndex = 156;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(186, 141);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(10, 13);
            this.Label3.TabIndex = 170;
            this.Label3.Text = "r";
            // 
            // textBox_fp0
            // 
            this.textBox_fp0.Location = new System.Drawing.Point(186, 115);
            this.textBox_fp0.Name = "textBox_fp0";
            this.textBox_fp0.Size = new System.Drawing.Size(100, 20);
            this.textBox_fp0.TabIndex = 155;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(182, 99);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(22, 13);
            this.Label2.TabIndex = 169;
            this.Label2.Text = "fp0";
            // 
            // textBox_C
            // 
            this.textBox_C.Location = new System.Drawing.Point(189, 200);
            this.textBox_C.Name = "textBox_C";
            this.textBox_C.Size = new System.Drawing.Size(100, 20);
            this.textBox_C.TabIndex = 157;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Location = new System.Drawing.Point(186, 184);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(14, 13);
            this.Label19.TabIndex = 168;
            this.Label19.Text = "C";
            // 
            // textBox_Fpc
            // 
            this.textBox_Fpc.Location = new System.Drawing.Point(14, 200);
            this.textBox_Fpc.Name = "textBox_Fpc";
            this.textBox_Fpc.Size = new System.Drawing.Size(100, 20);
            this.textBox_Fpc.TabIndex = 153;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.Location = new System.Drawing.Point(14, 184);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(106, 13);
            this.Label20.TabIndex = 167;
            this.Label20.Text = "Plastic Compr. Stress";
            // 
            // textBox_eu
            // 
            this.textBox_eu.Location = new System.Drawing.Point(186, 76);
            this.textBox_eu.Name = "textBox_eu";
            this.textBox_eu.Size = new System.Drawing.Size(100, 20);
            this.textBox_eu.TabIndex = 154;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(186, 60);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 13);
            this.Label9.TabIndex = 166;
            this.Label9.Text = "Ultimate strain";
            // 
            // textBox_Es
            // 
            this.textBox_Es.Location = new System.Drawing.Point(14, 156);
            this.textBox_Es.Name = "textBox_Es";
            this.textBox_Es.Size = new System.Drawing.Size(100, 20);
            this.textBox_Es.TabIndex = 152;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(12, 141);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(81, 13);
            this.Label8.TabIndex = 165;
            this.Label8.Text = "Elastic Modulus";
            // 
            // textBox_fu
            // 
            this.textBox_fu.Location = new System.Drawing.Point(14, 115);
            this.textBox_fu.Name = "textBox_fu";
            this.textBox_fu.Size = new System.Drawing.Size(100, 20);
            this.textBox_fu.TabIndex = 151;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(12, 99);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(97, 13);
            this.Label7.TabIndex = 164;
            this.Label7.Text = "Ultimate stress (ksi)";
            // 
            // textBox_fy
            // 
            this.textBox_fy.Location = new System.Drawing.Point(14, 76);
            this.textBox_fy.Name = "textBox_fy";
            this.textBox_fy.Size = new System.Drawing.Size(100, 20);
            this.textBox_fy.TabIndex = 150;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(12, 61);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(82, 13);
            this.Label6.TabIndex = 163;
            this.Label6.Text = "Yield stress (ksi)";
            // 
            // button_ViewCyclicPlot
            // 
            this.button_ViewCyclicPlot.Location = new System.Drawing.Point(15, 324);
            this.button_ViewCyclicPlot.Name = "button_ViewCyclicPlot";
            this.button_ViewCyclicPlot.Size = new System.Drawing.Size(91, 25);
            this.button_ViewCyclicPlot.TabIndex = 212;
            this.button_ViewCyclicPlot.Text = "View Cyclic Plot";
            this.button_ViewCyclicPlot.UseVisualStyleBackColor = true;
            // 
            // button_Eqns
            // 
            this.button_Eqns.Location = new System.Drawing.Point(112, 293);
            this.button_Eqns.Name = "button_Eqns";
            this.button_Eqns.Size = new System.Drawing.Size(70, 25);
            this.button_Eqns.TabIndex = 211;
            this.button_Eqns.TabStop = false;
            this.button_Eqns.Text = "Equations";
            this.button_Eqns.UseVisualStyleBackColor = true;
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(322, 324);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(70, 25);
            this.button_Close.TabIndex = 209;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(246, 324);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 208;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(112, 324);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 210;
            this.button_ViewPlot.TabStop = false;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(286, 24);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 214;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(339, 24);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 213;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 26);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(265, 21);
            this.comboBox_Names.TabIndex = 216;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // verticalButton1
            // 
            this.verticalButton1.Location = new System.Drawing.Point(400, 148);
            this.verticalButton1.Name = "verticalButton1";
            this.verticalButton1.Size = new System.Drawing.Size(75, 23);
            this.verticalButton1.TabIndex = 215;
            this.verticalButton1.Text = "verticalButton1";
            this.verticalButton1.UseVisualStyleBackColor = true;
            this.verticalButton1.VerticalText = null;
            // 
            // ModifiedRambergOsgoodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 361);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.verticalButton1);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.button_ViewCyclicPlot);
            this.Controls.Add(this.button_Eqns);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.textBox_r);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.textBox_fp0);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.textBox_C);
            this.Controls.Add(this.Label19);
            this.Controls.Add(this.textBox_Fpc);
            this.Controls.Add(this.Label20);
            this.Controls.Add(this.textBox_eu);
            this.Controls.Add(this.Label9);
            this.Controls.Add(this.textBox_Es);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.textBox_fu);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.textBox_fy);
            this.Controls.Add(this.Label6);
            this.MaximumSize = new System.Drawing.Size(420, 400);
            this.MinimumSize = new System.Drawing.Size(420, 400);
            this.Name = "ModifiedRambergOsgoodForm";
            this.Text = "ModifiedRambergOsgoodForm";
            this.Load += new System.EventHandler(this.ModifiedRambergOsgoodForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.TextBox textBox_r;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox textBox_fp0;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox textBox_C;
        internal System.Windows.Forms.Label Label19;
        internal System.Windows.Forms.TextBox textBox_Fpc;
        internal System.Windows.Forms.Label Label20;
        internal System.Windows.Forms.TextBox textBox_eu;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.TextBox textBox_Es;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox textBox_fu;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox textBox_fy;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button button_ViewCyclicPlot;
        internal System.Windows.Forms.Button button_Eqns;
        internal System.Windows.Forms.Button button_Close;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Delete;
        private VerticalButton verticalButton1;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}