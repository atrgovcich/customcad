﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Materials;
using Materials.Inelastic.Steel;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using FiberSectionAnalysis.LimitStates;
using CadPanel.UserControls;
using System.ComponentModel;

namespace CadPanel.Forms.MaterialForms
{
    public partial class ManderSteelForm : Form
    {
        private List<IGenericMaterial> _allMaterials;
        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();
        private List<IGenericMaterial> _addedMaterials = new List<IGenericMaterial>();
        private bool _onNewMaterial = false;
        private List<LimitState> _addedLimitStates = new List<LimitState>();

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }

        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<IGenericMaterial> AddedMaterials
        {
            get
            {
                return _addedMaterials;
            }
        }

        public ManderSteelForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;

            comboBox_Names.BeforeUpdate += new CancelEventHandler(ComboBox_Names_BeforeUpdate);
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;


            if (_onNewMaterial == true)
            {
                _addedLimitStates.AddRange(CreateNewLimitState());
            }

            this.Close();
        }

        private List<LimitState> CreateNewLimitState()
        {
            string name = comboBox_Names.Text;

            return CreateNewLimitState(name);
        }

        private List<LimitState> CreateNewLimitState(string name)
        {
            List<LimitState> newLimitStates = new List<LimitState>();

            IGenericMaterial genericMat = GetMaterialFromName(name, _allMaterials);

            LimitState ls1 = new LimitState()
            {
                Material = (ManderSteel)genericMat,
                CompressionLimitStrain = 0.09,
                TensionLimitStrain = 0.09,
                TerminationLimitState = true,
                Name = "Failure"
            };

            LimitState ls2 = new LimitState()
            {
                Material = (ManderSteel)genericMat,
                CompressionLimitStrain = 0.008,
                TensionLimitStrain = 0.008,
                TerminationLimitState = true,
                Name = "Strain Hardening"
            };

            newLimitStates.Add(ls1);
            newLimitStates.Add(ls2);

            return newLimitStates;
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddMaterialNameForm frm = new AddMaterialNameForm(_allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ManderSteel newMat = new ManderSteel(frm.NewName);

                _allMaterials.Add(newMat);

                _addedMaterials.Add(newMat);

                comboBox_Names.Items.Add(newMat.Name);

                comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;

                _onNewMaterial = true;
            }

            frm.Dispose();
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete the selected material.  Any elements utilizing this material will have their material definition reset.  Proceed?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _onNewMaterial = false;

                int selIndex = comboBox_Names.SelectedIndex;

                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                comboBox_Names.Items.RemoveAt(selIndex);

                if (comboBox_Names.Items.Count > 0)
                {
                    if (selIndex <= comboBox_Names.Items.Count - 1)
                    {
                        comboBox_Names.SelectedIndex = selIndex;
                    }
                    else
                    {
                        comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;
                    }
                }
                else
                {
                    comboBox_Names.SelectedIndex = -1;

                    ComboBox_Names_SelectedIndexChanged(comboBox_Names, new EventArgs());
                }

                _allMaterials.Remove(genericMat);

                _deletedMaterials.Add(genericMat);
            }
        }

        private void ComboBox_Names_BeforeUpdate(object sender, CancelEventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (_onNewMaterial == true)
            {
                if (cB.PreviousIndex >= 0)
                {
                    _addedLimitStates.AddRange(CreateNewLimitState(Convert.ToString(cB.Items[cB.PreviousIndex])));
                }

                _onNewMaterial = false;
            }
        }

        private void ComboBox_Names_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cB = (ComboBox)sender;

            if (cB.SelectedIndex >= 0)
            {
                IGenericMaterial genericMat = GetMaterialFromName(cB.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderSteel castMat = (ManderSteel)genericMat;

                    textBox_fy.Text = Convert.ToString(castMat.Fy);
                    textBox_Es.Text = Convert.ToString(castMat.Es);
                    textBox_esh.Text = Convert.ToString(castMat.esh);
                    textBox_eu.Text = Convert.ToString(castMat.eu);
                    textBox_ey.Text = Convert.ToString(castMat.ey);
                    textBox_fu.Text = Convert.ToString(castMat.Fu);
                    textBox_p.Text = Convert.ToString(castMat.P);
                }
            }
            else
            {
                textBox_fy.Clear();
                textBox_Es.Clear();
                textBox_esh.Clear();
                textBox_eu.Clear();
                textBox_ey.Clear();
                textBox_fu.Clear();
                textBox_p.Clear();
            }
        }

        private void Button_Calc_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fy.Text, out double fy) == true)
            {
                if (fy > 0)
                {
                    if (double.TryParse(textBox_Es.Text, out double Es) == true)
                    {
                        if (Es > 0)
                        {
                            textBox_ey.Text = Convert.ToString(Round(fy/Es, 5));
                        }
                        else
                        {
                            MessageBox.Show("The elastic modulus must be a positive value.", "Error", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The elastic modulus must be a numeric value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateInputs(out string message)
        {
            if (double.TryParse(textBox_fy.Text, out double fy) == true)
            {
                if (fy <= 0)
                {
                    message = "Yield strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Yield strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Es.Text, out double Es) == true)
            {
                if (Es <= 0)
                {
                    message = "Elastic modulus must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Elastic modulus must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ey.Text, out double ey) == true)
            {
                if (ey <= 0)
                {
                    message = "Yield strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Yield strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_esh.Text, out double esh) == true)
            {
                if (esh <= 0)
                {
                    message = "Strain at strain hardening must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Strain at strain hardening must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_eu.Text, out double eu) == true)
            {
                if (eu <= 0)
                {
                    message = "Ultimate strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_fu.Text, out double fu) == true)
            {
                if (fu <= 0)
                {
                    message = "Ultimate stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate stress must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_p.Text, out double p) == true)
            {
                if (p <= 0)
                {
                    message = "Exponent 'p' must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Exponent 'p' must be a numeric value.";

                return false;
            }

            message = string.Empty;

            return true;
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderSteel castMat = (ManderSteel)genericMat;

                    castMat.Fy = Convert.ToDouble(textBox_fy.Text);
                    castMat.Fu = Convert.ToDouble(textBox_fu.Text);
                    castMat.esh = Convert.ToDouble(textBox_esh.Text);
                    castMat.P = Convert.ToDouble(textBox_p.Text);
                    castMat.ey = Convert.ToDouble(textBox_ey.Text);
                    castMat.eu = Convert.ToDouble(textBox_eu.Text);
                    castMat.Es = Convert.ToDouble(textBox_Es.Text);
                }
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void Button_ViewPlot_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                ManderSteel castMat = new ManderSteel(genericMat.Name);

                castMat.Fy = Convert.ToDouble(textBox_fy.Text);
                castMat.Fu = Convert.ToDouble(textBox_fu.Text);
                castMat.esh = Convert.ToDouble(textBox_esh.Text);
                castMat.P = Convert.ToDouble(textBox_p.Text);
                castMat.ey = Convert.ToDouble(textBox_ey.Text);
                castMat.eu = Convert.ToDouble(textBox_eu.Text);
                castMat.Es = Convert.ToDouble(textBox_Es.Text);

                StressStrainChartForm frm = new StressStrainChartForm(castMat, 0, castMat.eu);

                frm.ShowDialog();

                frm.Dispose();
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void ManderSteelForm_Load(object sender, EventArgs e)
        {
            PopulateNamesComboBox();
        }

        private void PopulateNamesComboBox()
        {
            if (_allMaterials != null)
            {
                comboBox_Names.Items.Clear();

                for (int i = 0; i < _allMaterials.Count; i++)
                {
                    IGenericMaterial genericMat = _allMaterials[i];

                    if (genericMat.GetType().Equals(typeof(ManderSteel)) == true)
                    {
                        comboBox_Names.Items.Add(genericMat.Name);
                    }
                }

                if (comboBox_Names.Items.Count > 0)
                {
                    comboBox_Names.SelectedIndex = 0;
                }
            }
        }
    }
}
