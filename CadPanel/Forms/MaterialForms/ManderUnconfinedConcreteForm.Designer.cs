﻿namespace CadPanel.Forms.MaterialForms
{
    partial class ManderUnconfinedConcreteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Name = new System.Windows.Forms.Label();
            this.button_ViewCyclicPlot = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_Calc_ft = new System.Windows.Forms.Button();
            this.button_Calc_E = new System.Windows.Forms.Button();
            this.button_Calc_et = new System.Windows.Forms.Button();
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.textBox_Ec = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.textBox_eps = new System.Windows.Forms.TextBox();
            this.Label27 = new System.Windows.Forms.Label();
            this.textBox_esp = new System.Windows.Forms.TextBox();
            this.Label26 = new System.Windows.Forms.Label();
            this.textBox_ecu = new System.Windows.Forms.TextBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.textBox_et = new System.Windows.Forms.TextBox();
            this.Label24 = new System.Windows.Forms.Label();
            this.textBox_fpc = new System.Windows.Forms.TextBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.textBox_fc = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.textBox_ft = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.checkBox_AutoChooseEc = new System.Windows.Forms.CheckBox();
            this.groupBox_ElasticModulus = new System.Windows.Forms.GroupBox();
            this.radioButton_HighStrength = new System.Windows.Forms.RadioButton();
            this.radioButton_NormalStrength = new System.Windows.Forms.RadioButton();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.groupBox_ElasticModulus.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 119;
            this.Label_Name.Text = "Model Name";
            // 
            // button_ViewCyclicPlot
            // 
            this.button_ViewCyclicPlot.Location = new System.Drawing.Point(16, 318);
            this.button_ViewCyclicPlot.Name = "button_ViewCyclicPlot";
            this.button_ViewCyclicPlot.Size = new System.Drawing.Size(100, 25);
            this.button_ViewCyclicPlot.TabIndex = 116;
            this.button_ViewCyclicPlot.Text = "View Cyclic Plot";
            this.button_ViewCyclicPlot.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(153, 262);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(239, 39);
            this.Label1.TabIndex = 115;
            this.Label1.Text = "NOTE:  All values on this form should be positive.\r\n              The program ass" +
    "igns negatives to the\r\n               tension values during calculation.";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(322, 323);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(70, 25);
            this.button_Cancel.TabIndex = 114;
            this.button_Cancel.Text = "Close";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(246, 323);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 113;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // button_Calc_ft
            // 
            this.button_Calc_ft.Location = new System.Drawing.Point(326, 73);
            this.button_Calc_ft.Name = "button_Calc_ft";
            this.button_Calc_ft.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_ft.TabIndex = 112;
            this.button_Calc_ft.Text = "Calc";
            this.button_Calc_ft.UseVisualStyleBackColor = true;
            this.button_Calc_ft.Click += new System.EventHandler(this.Button_Calc_ft_Click);
            // 
            // button_Calc_E
            // 
            this.button_Calc_E.Location = new System.Drawing.Point(122, 153);
            this.button_Calc_E.Name = "button_Calc_E";
            this.button_Calc_E.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_E.TabIndex = 111;
            this.button_Calc_E.Text = "Calc";
            this.button_Calc_E.UseVisualStyleBackColor = true;
            this.button_Calc_E.Click += new System.EventHandler(this.Button_Calc_E_Click);
            // 
            // button_Calc_et
            // 
            this.button_Calc_et.Location = new System.Drawing.Point(326, 117);
            this.button_Calc_et.Name = "button_Calc_et";
            this.button_Calc_et.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_et.TabIndex = 110;
            this.button_Calc_et.Text = "Calc";
            this.button_Calc_et.UseVisualStyleBackColor = true;
            this.button_Calc_et.Click += new System.EventHandler(this.Button_Calc_et_Click);
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(122, 318);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 109;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // textBox_Ec
            // 
            this.textBox_Ec.Location = new System.Drawing.Point(17, 154);
            this.textBox_Ec.Name = "textBox_Ec";
            this.textBox_Ec.Size = new System.Drawing.Size(100, 20);
            this.textBox_Ec.TabIndex = 107;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(14, 139);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(102, 13);
            this.Label28.TabIndex = 108;
            this.Label28.Text = "Elastic modulus (ksi)";
            // 
            // textBox_eps
            // 
            this.textBox_eps.Location = new System.Drawing.Point(17, 279);
            this.textBox_eps.Name = "textBox_eps";
            this.textBox_eps.Size = new System.Drawing.Size(100, 20);
            this.textBox_eps.TabIndex = 105;
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(14, 262);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(103, 13);
            this.Label27.TabIndex = 106;
            this.Label27.Text = "Strain at peak stress";
            // 
            // textBox_esp
            // 
            this.textBox_esp.Location = new System.Drawing.Point(17, 240);
            this.textBox_esp.Name = "textBox_esp";
            this.textBox_esp.Size = new System.Drawing.Size(100, 20);
            this.textBox_esp.TabIndex = 103;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(14, 223);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(72, 13);
            this.Label26.TabIndex = 104;
            this.Label26.Text = "Spalling strain";
            // 
            // textBox_ecu
            // 
            this.textBox_ecu.Location = new System.Drawing.Point(17, 197);
            this.textBox_ecu.Name = "textBox_ecu";
            this.textBox_ecu.Size = new System.Drawing.Size(100, 20);
            this.textBox_ecu.TabIndex = 101;
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(14, 180);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(76, 13);
            this.Label25.TabIndex = 102;
            this.Label25.Text = "Crushing strain";
            // 
            // textBox_et
            // 
            this.textBox_et.Location = new System.Drawing.Point(220, 117);
            this.textBox_et.Name = "textBox_et";
            this.textBox_et.Size = new System.Drawing.Size(100, 20);
            this.textBox_et.TabIndex = 99;
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(217, 102);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(116, 13);
            this.Label24.TabIndex = 100;
            this.Label24.Text = "Tension strain capacity";
            // 
            // textBox_fpc
            // 
            this.textBox_fpc.Location = new System.Drawing.Point(17, 115);
            this.textBox_fpc.Name = "textBox_fpc";
            this.textBox_fpc.Size = new System.Drawing.Size(100, 20);
            this.textBox_fpc.TabIndex = 97;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(14, 100);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(134, 13);
            this.Label23.TabIndex = 98;
            this.Label23.Text = "Post crushing strength (ksi)";
            // 
            // textBox_fc
            // 
            this.textBox_fc.Location = new System.Drawing.Point(17, 73);
            this.textBox_fc.Name = "textBox_fc";
            this.textBox_fc.Size = new System.Drawing.Size(100, 20);
            this.textBox_fc.TabIndex = 95;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(14, 56);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(164, 13);
            this.Label4.TabIndex = 96;
            this.Label4.Text = "28 day compression strength (ksi)";
            // 
            // textBox_ft
            // 
            this.textBox_ft.Location = new System.Drawing.Point(220, 73);
            this.textBox_ft.Name = "textBox_ft";
            this.textBox_ft.Size = new System.Drawing.Size(100, 20);
            this.textBox_ft.TabIndex = 93;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(217, 56);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(108, 13);
            this.Label3.TabIndex = 94;
            this.Label3.Text = "Tension strength (ksi)";
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(286, 21);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 122;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(339, 21);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 121;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // checkBox_AutoChooseEc
            // 
            this.checkBox_AutoChooseEc.AutoSize = true;
            this.checkBox_AutoChooseEc.Location = new System.Drawing.Point(173, 156);
            this.checkBox_AutoChooseEc.Name = "checkBox_AutoChooseEc";
            this.checkBox_AutoChooseEc.Size = new System.Drawing.Size(123, 17);
            this.checkBox_AutoChooseEc.TabIndex = 123;
            this.checkBox_AutoChooseEc.Text = "Auto-choose Ec eqn";
            this.checkBox_AutoChooseEc.UseVisualStyleBackColor = true;
            this.checkBox_AutoChooseEc.CheckedChanged += new System.EventHandler(this.CheckBox_AutoChooseEc_CheckedChanged);
            // 
            // groupBox_ElasticModulus
            // 
            this.groupBox_ElasticModulus.Controls.Add(this.radioButton_HighStrength);
            this.groupBox_ElasticModulus.Controls.Add(this.radioButton_NormalStrength);
            this.groupBox_ElasticModulus.Location = new System.Drawing.Point(178, 182);
            this.groupBox_ElasticModulus.Name = "groupBox_ElasticModulus";
            this.groupBox_ElasticModulus.Size = new System.Drawing.Size(190, 67);
            this.groupBox_ElasticModulus.TabIndex = 124;
            this.groupBox_ElasticModulus.TabStop = false;
            this.groupBox_ElasticModulus.Text = "Elastic Modulus";
            // 
            // radioButton_HighStrength
            // 
            this.radioButton_HighStrength.AutoSize = true;
            this.radioButton_HighStrength.Location = new System.Drawing.Point(17, 42);
            this.radioButton_HighStrength.Name = "radioButton_HighStrength";
            this.radioButton_HighStrength.Size = new System.Drawing.Size(90, 17);
            this.radioButton_HighStrength.TabIndex = 1;
            this.radioButton_HighStrength.TabStop = true;
            this.radioButton_HighStrength.Text = "High Strength";
            this.radioButton_HighStrength.UseVisualStyleBackColor = true;
            // 
            // radioButton_NormalStrength
            // 
            this.radioButton_NormalStrength.AutoSize = true;
            this.radioButton_NormalStrength.Location = new System.Drawing.Point(17, 19);
            this.radioButton_NormalStrength.Name = "radioButton_NormalStrength";
            this.radioButton_NormalStrength.Size = new System.Drawing.Size(101, 17);
            this.radioButton_NormalStrength.TabIndex = 0;
            this.radioButton_NormalStrength.TabStop = true;
            this.radioButton_NormalStrength.Text = "Normal Strength";
            this.radioButton_NormalStrength.UseVisualStyleBackColor = true;
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 23);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(265, 21);
            this.comboBox_Names.TabIndex = 125;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // ManderUnconfinedConcreteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 361);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.groupBox_ElasticModulus);
            this.Controls.Add(this.checkBox_AutoChooseEc);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.button_ViewCyclicPlot);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Calc_ft);
            this.Controls.Add(this.button_Calc_E);
            this.Controls.Add(this.button_Calc_et);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.textBox_Ec);
            this.Controls.Add(this.Label28);
            this.Controls.Add(this.textBox_eps);
            this.Controls.Add(this.Label27);
            this.Controls.Add(this.textBox_esp);
            this.Controls.Add(this.Label26);
            this.Controls.Add(this.textBox_ecu);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.textBox_et);
            this.Controls.Add(this.Label24);
            this.Controls.Add(this.textBox_fpc);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.textBox_fc);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.textBox_ft);
            this.Controls.Add(this.Label3);
            this.MaximumSize = new System.Drawing.Size(420, 400);
            this.MinimumSize = new System.Drawing.Size(420, 400);
            this.Name = "ManderUnconfinedConcreteForm";
            this.Text = "Mander Unconfined Concrete";
            this.Load += new System.EventHandler(this.ManderUnconfinedConcreteForm_Load);
            this.groupBox_ElasticModulus.ResumeLayout(false);
            this.groupBox_ElasticModulus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.Button button_ViewCyclicPlot;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button button_Cancel;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_Calc_ft;
        internal System.Windows.Forms.Button button_Calc_E;
        internal System.Windows.Forms.Button button_Calc_et;
        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.TextBox textBox_Ec;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.TextBox textBox_eps;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.TextBox textBox_esp;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox textBox_ecu;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.TextBox textBox_et;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.TextBox textBox_fpc;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.TextBox textBox_fc;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox textBox_ft;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Delete;
        private System.Windows.Forms.CheckBox checkBox_AutoChooseEc;
        private System.Windows.Forms.GroupBox groupBox_ElasticModulus;
        private System.Windows.Forms.RadioButton radioButton_NormalStrength;
        private System.Windows.Forms.RadioButton radioButton_HighStrength;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}