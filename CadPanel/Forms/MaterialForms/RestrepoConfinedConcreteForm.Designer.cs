﻿namespace CadPanel.Forms.MaterialForms
{
    partial class RestrepoConfinedConcreteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox_HoopType = new System.Windows.Forms.GroupBox();
            this.radioButton_Circular = new System.Windows.Forms.RadioButton();
            this.radioButton_Spiral = new System.Windows.Forms.RadioButton();
            this.groupBox_ElasticModulus = new System.Windows.Forms.GroupBox();
            this.radioButton_HighStrength = new System.Windows.Forms.RadioButton();
            this.radioButton_NormalStrength = new System.Windows.Forms.RadioButton();
            this.checkBox_AutoChooseEc = new System.Windows.Forms.CheckBox();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.Label_Name = new System.Windows.Forms.Label();
            this.button_ViewCyclicPlot = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_Calc_E = new System.Windows.Forms.Button();
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.textBox_Ec = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.textBox_fcc = new System.Windows.Forms.TextBox();
            this.Label26 = new System.Windows.Forms.Label();
            this.textBox_ecu = new System.Windows.Forms.TextBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.textBox_st = new System.Windows.Forms.TextBox();
            this.Label23 = new System.Windows.Forms.Label();
            this.textBox_fc = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.GroupBox_HoopType.SuspendLayout();
            this.groupBox_ElasticModulus.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox_HoopType
            // 
            this.GroupBox_HoopType.Controls.Add(this.radioButton_Circular);
            this.GroupBox_HoopType.Controls.Add(this.radioButton_Spiral);
            this.GroupBox_HoopType.Location = new System.Drawing.Point(198, 73);
            this.GroupBox_HoopType.Margin = new System.Windows.Forms.Padding(2);
            this.GroupBox_HoopType.Name = "GroupBox_HoopType";
            this.GroupBox_HoopType.Padding = new System.Windows.Forms.Padding(2);
            this.GroupBox_HoopType.Size = new System.Drawing.Size(133, 65);
            this.GroupBox_HoopType.TabIndex = 184;
            this.GroupBox_HoopType.TabStop = false;
            this.GroupBox_HoopType.Text = "Hoop Type";
            // 
            // radioButton_Circular
            // 
            this.radioButton_Circular.AutoSize = true;
            this.radioButton_Circular.Location = new System.Drawing.Point(8, 36);
            this.radioButton_Circular.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Circular.Name = "radioButton_Circular";
            this.radioButton_Circular.Size = new System.Drawing.Size(91, 17);
            this.radioButton_Circular.TabIndex = 1;
            this.radioButton_Circular.Text = "Closed Hoops";
            this.radioButton_Circular.UseVisualStyleBackColor = true;
            // 
            // radioButton_Spiral
            // 
            this.radioButton_Spiral.AutoSize = true;
            this.radioButton_Spiral.Checked = true;
            this.radioButton_Spiral.Location = new System.Drawing.Point(8, 16);
            this.radioButton_Spiral.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Spiral.Name = "radioButton_Spiral";
            this.radioButton_Spiral.Size = new System.Drawing.Size(113, 17);
            this.radioButton_Spiral.TabIndex = 0;
            this.radioButton_Spiral.TabStop = true;
            this.radioButton_Spiral.Text = "Spiral Confinement";
            this.radioButton_Spiral.UseVisualStyleBackColor = true;
            // 
            // groupBox_ElasticModulus
            // 
            this.groupBox_ElasticModulus.Controls.Add(this.radioButton_HighStrength);
            this.groupBox_ElasticModulus.Controls.Add(this.radioButton_NormalStrength);
            this.groupBox_ElasticModulus.Location = new System.Drawing.Point(198, 177);
            this.groupBox_ElasticModulus.Name = "groupBox_ElasticModulus";
            this.groupBox_ElasticModulus.Size = new System.Drawing.Size(135, 67);
            this.groupBox_ElasticModulus.TabIndex = 183;
            this.groupBox_ElasticModulus.TabStop = false;
            this.groupBox_ElasticModulus.Text = "Elastic Modulus";
            // 
            // radioButton_HighStrength
            // 
            this.radioButton_HighStrength.AutoSize = true;
            this.radioButton_HighStrength.Location = new System.Drawing.Point(17, 42);
            this.radioButton_HighStrength.Name = "radioButton_HighStrength";
            this.radioButton_HighStrength.Size = new System.Drawing.Size(90, 17);
            this.radioButton_HighStrength.TabIndex = 1;
            this.radioButton_HighStrength.TabStop = true;
            this.radioButton_HighStrength.Text = "High Strength";
            this.radioButton_HighStrength.UseVisualStyleBackColor = true;
            // 
            // radioButton_NormalStrength
            // 
            this.radioButton_NormalStrength.AutoSize = true;
            this.radioButton_NormalStrength.Location = new System.Drawing.Point(17, 19);
            this.radioButton_NormalStrength.Name = "radioButton_NormalStrength";
            this.radioButton_NormalStrength.Size = new System.Drawing.Size(101, 17);
            this.radioButton_NormalStrength.TabIndex = 0;
            this.radioButton_NormalStrength.TabStop = true;
            this.radioButton_NormalStrength.Text = "Normal Strength";
            this.radioButton_NormalStrength.UseVisualStyleBackColor = true;
            // 
            // checkBox_AutoChooseEc
            // 
            this.checkBox_AutoChooseEc.AutoSize = true;
            this.checkBox_AutoChooseEc.Location = new System.Drawing.Point(193, 151);
            this.checkBox_AutoChooseEc.Name = "checkBox_AutoChooseEc";
            this.checkBox_AutoChooseEc.Size = new System.Drawing.Size(123, 17);
            this.checkBox_AutoChooseEc.TabIndex = 182;
            this.checkBox_AutoChooseEc.Text = "Auto-choose Ec eqn";
            this.checkBox_AutoChooseEc.UseVisualStyleBackColor = true;
            this.checkBox_AutoChooseEc.CheckedChanged += new System.EventHandler(this.CheckBox_AutoChooseEc_CheckedChanged);
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(286, 21);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 181;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(339, 21);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 180;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 178;
            this.Label_Name.Text = "Model Name";
            // 
            // button_ViewCyclicPlot
            // 
            this.button_ViewCyclicPlot.Location = new System.Drawing.Point(12, 348);
            this.button_ViewCyclicPlot.Name = "button_ViewCyclicPlot";
            this.button_ViewCyclicPlot.Size = new System.Drawing.Size(91, 25);
            this.button_ViewCyclicPlot.TabIndex = 177;
            this.button_ViewCyclicPlot.Text = "View Cyclic Plot";
            this.button_ViewCyclicPlot.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(14, 275);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(239, 39);
            this.Label1.TabIndex = 176;
            this.Label1.Text = "NOTE:  All values on this form should be positive.\r\n              The program ass" +
    "igns negatives to the\r\n               tension values during calculation.";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(317, 348);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(70, 25);
            this.button_Cancel.TabIndex = 175;
            this.button_Cancel.Text = "Close";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(241, 348);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 174;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // button_Calc_E
            // 
            this.button_Calc_E.Location = new System.Drawing.Point(121, 148);
            this.button_Calc_E.Name = "button_Calc_E";
            this.button_Calc_E.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_E.TabIndex = 172;
            this.button_Calc_E.Text = "Calc";
            this.button_Calc_E.UseVisualStyleBackColor = true;
            this.button_Calc_E.Click += new System.EventHandler(this.Button_Calc_E_Click);
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(109, 348);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 170;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // textBox_Ec
            // 
            this.textBox_Ec.Location = new System.Drawing.Point(16, 149);
            this.textBox_Ec.Name = "textBox_Ec";
            this.textBox_Ec.Size = new System.Drawing.Size(100, 20);
            this.textBox_Ec.TabIndex = 168;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(13, 134);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(102, 13);
            this.Label28.TabIndex = 169;
            this.Label28.Text = "Elastic modulus (ksi)";
            // 
            // textBox_fcc
            // 
            this.textBox_fcc.Location = new System.Drawing.Point(17, 189);
            this.textBox_fcc.Name = "textBox_fcc";
            this.textBox_fcc.Size = new System.Drawing.Size(100, 20);
            this.textBox_fcc.TabIndex = 166;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(14, 172);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(139, 13);
            this.Label26.TabIndex = 167;
            this.Label26.Text = "Confined core strength (ksi()";
            // 
            // textBox_ecu
            // 
            this.textBox_ecu.Location = new System.Drawing.Point(17, 229);
            this.textBox_ecu.Name = "textBox_ecu";
            this.textBox_ecu.Size = new System.Drawing.Size(100, 20);
            this.textBox_ecu.TabIndex = 164;
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(14, 212);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(76, 13);
            this.Label25.TabIndex = 165;
            this.Label25.Text = "Crushing strain";
            // 
            // textBox_st
            // 
            this.textBox_st.Location = new System.Drawing.Point(16, 111);
            this.textBox_st.Name = "textBox_st";
            this.textBox_st.Size = new System.Drawing.Size(100, 20);
            this.textBox_st.TabIndex = 160;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.Location = new System.Drawing.Point(13, 96);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(144, 13);
            this.Label23.TabIndex = 161;
            this.Label23.Text = "Transverse rebar spacing (in)";
            // 
            // textBox_fc
            // 
            this.textBox_fc.Location = new System.Drawing.Point(17, 73);
            this.textBox_fc.Name = "textBox_fc";
            this.textBox_fc.Size = new System.Drawing.Size(100, 20);
            this.textBox_fc.TabIndex = 158;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(14, 56);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(164, 13);
            this.Label4.TabIndex = 159;
            this.Label4.Text = "28 day compression strength (ksi)";
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 24);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(265, 21);
            this.comboBox_Names.TabIndex = 185;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // RestrepoConfinedConcreteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 386);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.GroupBox_HoopType);
            this.Controls.Add(this.groupBox_ElasticModulus);
            this.Controls.Add(this.checkBox_AutoChooseEc);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.button_ViewCyclicPlot);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Calc_E);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.textBox_Ec);
            this.Controls.Add(this.Label28);
            this.Controls.Add(this.textBox_fcc);
            this.Controls.Add(this.Label26);
            this.Controls.Add(this.textBox_ecu);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.textBox_st);
            this.Controls.Add(this.Label23);
            this.Controls.Add(this.textBox_fc);
            this.Controls.Add(this.Label4);
            this.MaximumSize = new System.Drawing.Size(415, 425);
            this.MinimumSize = new System.Drawing.Size(415, 425);
            this.Name = "RestrepoConfinedConcreteForm";
            this.Text = "Restrepo Confined Concrete";
            this.Load += new System.EventHandler(this.RestrepoConfinedConcreteForm_Load);
            this.GroupBox_HoopType.ResumeLayout(false);
            this.GroupBox_HoopType.PerformLayout();
            this.groupBox_ElasticModulus.ResumeLayout(false);
            this.groupBox_ElasticModulus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox_HoopType;
        internal System.Windows.Forms.RadioButton radioButton_Circular;
        internal System.Windows.Forms.RadioButton radioButton_Spiral;
        private System.Windows.Forms.GroupBox groupBox_ElasticModulus;
        private System.Windows.Forms.RadioButton radioButton_HighStrength;
        private System.Windows.Forms.RadioButton radioButton_NormalStrength;
        private System.Windows.Forms.CheckBox checkBox_AutoChooseEc;
        internal System.Windows.Forms.Button button_Add;
        internal System.Windows.Forms.Button button_Delete;
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.Button button_ViewCyclicPlot;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button button_Cancel;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_Calc_E;
        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.TextBox textBox_Ec;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.TextBox textBox_fcc;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox textBox_ecu;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.TextBox textBox_st;
        internal System.Windows.Forms.Label Label23;
        internal System.Windows.Forms.TextBox textBox_fc;
        internal System.Windows.Forms.Label Label4;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}