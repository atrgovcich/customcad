﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Materials;
using Utilities.Containers;
using CadPanel.DrawingElements;
using FiberSectionAnalysis.LimitStates;

namespace CadPanel.Forms.MaterialForms
{
    public partial class MaterialSelectionForm : Form
    {
        private List<IGenericMaterial> _allMaterials;

        private List<LimitState> _addedLimitStates = new List<LimitState>();

        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();

        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }
        public MaterialSelectionForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;
        }

        private void Button_Hsu_Edit_Click(object sender, EventArgs e)
        {
            HsuConcreteForm frm = new HsuConcreteForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);

            }
            frm.Dispose();
        }

        private void Button_ManderSteel_Edit_Click(object sender, EventArgs e)
        {
            ManderSteelForm frm = new ManderSteelForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_ManderUnconf_Edit_Click(object sender, EventArgs e)
        {
            ManderUnconfinedConcreteForm frm = new ManderUnconfinedConcreteForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_ManderConf_Edit_Click(object sender, EventArgs e)
        {
            ManderConfinedConcreteForm frm = new ManderConfinedConcreteForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_Restreppo_Edit_Click(object sender, EventArgs e)
        {
            RestrepoConfinedConcreteForm frm = new RestrepoConfinedConcreteForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_PWL_Edit_Click(object sender, EventArgs e)
        {
            PiecewiseLinearConfinedConcreteForm frm = new PiecewiseLinearConfinedConcreteForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_MenPint_Edit_Click(object sender, EventArgs e)
        {
            MenegottoPintoSteelForm frm = new MenegottoPintoSteelForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }

        private void Button_RambergOsgood_Edit_Click(object sender, EventArgs e)
        {
            ModifiedRambergOsgoodForm frm = new ModifiedRambergOsgoodForm(_allMaterials);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                _deletedMaterials.AddRange(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                _addedLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();
        }
    }
}
