﻿namespace CadPanel.Forms.MaterialForms
{
    partial class HsuConcreteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            this.button_Calc_e3f = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.Label_Name = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Button_Calc_ft = new System.Windows.Forms.Button();
            this.button_Calc_E = new System.Windows.Forms.Button();
            this.Button_Calc_et = new System.Windows.Forms.Button();
            this.button_ViewPlot = new System.Windows.Forms.Button();
            this.textBox_Ec = new System.Windows.Forms.TextBox();
            this.Label28 = new System.Windows.Forms.Label();
            this.textBox_eps = new System.Windows.Forms.TextBox();
            this.Label27 = new System.Windows.Forms.Label();
            this.textBox_ecu = new System.Windows.Forms.TextBox();
            this.Label26 = new System.Windows.Forms.Label();
            this.textBox_e3f = new System.Windows.Forms.TextBox();
            this.Label25 = new System.Windows.Forms.Label();
            this.textBox_et = new System.Windows.Forms.TextBox();
            this.Label24 = new System.Windows.Forms.Label();
            this.textBox_fc = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.textBox_ft = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.button_Add = new System.Windows.Forms.Button();
            this.comboBox_Names = new CadPanel.UserControls.ComboBoxWithPrevious();
            this.SuspendLayout();
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(320, 321);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(70, 25);
            this.button_Cancel.TabIndex = 117;
            this.button_Cancel.Text = "Close";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(244, 321);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(70, 25);
            this.button_OK.TabIndex = 116;
            this.button_OK.Text = "Save";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // button_Calc_e3f
            // 
            this.button_Calc_e3f.Location = new System.Drawing.Point(124, 195);
            this.button_Calc_e3f.Name = "button_Calc_e3f";
            this.button_Calc_e3f.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_e3f.TabIndex = 115;
            this.button_Calc_e3f.Text = "Calc";
            this.button_Calc_e3f.UseVisualStyleBackColor = true;
            this.button_Calc_e3f.Click += new System.EventHandler(this.Button_Calc_e3f_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(339, 23);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(47, 25);
            this.button_Delete.TabIndex = 114;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // Label_Name
            // 
            this.Label_Name.AutoSize = true;
            this.Label_Name.Location = new System.Drawing.Point(12, 9);
            this.Label_Name.Name = "Label_Name";
            this.Label_Name.Size = new System.Drawing.Size(67, 13);
            this.Label_Name.TabIndex = 112;
            this.Label_Name.Text = "Model Name";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 268);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(239, 39);
            this.Label1.TabIndex = 110;
            this.Label1.Text = "NOTE:  All values on this form should be positive.\r\n              The program ass" +
    "igns negatives to the\r\n               tension values during calculation.";
            // 
            // Button_Calc_ft
            // 
            this.Button_Calc_ft.Location = new System.Drawing.Point(326, 73);
            this.Button_Calc_ft.Name = "Button_Calc_ft";
            this.Button_Calc_ft.Size = new System.Drawing.Size(43, 22);
            this.Button_Calc_ft.TabIndex = 109;
            this.Button_Calc_ft.Text = "Calc";
            this.Button_Calc_ft.UseVisualStyleBackColor = true;
            this.Button_Calc_ft.Click += new System.EventHandler(this.Button_Calc_ft_Click);
            // 
            // button_Calc_E
            // 
            this.button_Calc_E.Location = new System.Drawing.Point(124, 112);
            this.button_Calc_E.Name = "button_Calc_E";
            this.button_Calc_E.Size = new System.Drawing.Size(43, 22);
            this.button_Calc_E.TabIndex = 108;
            this.button_Calc_E.Text = "Calc";
            this.button_Calc_E.UseVisualStyleBackColor = true;
            this.button_Calc_E.Click += new System.EventHandler(this.Button_Calc_E_Click);
            // 
            // Button_Calc_et
            // 
            this.Button_Calc_et.Location = new System.Drawing.Point(326, 117);
            this.Button_Calc_et.Name = "Button_Calc_et";
            this.Button_Calc_et.Size = new System.Drawing.Size(43, 22);
            this.Button_Calc_et.TabIndex = 107;
            this.Button_Calc_et.Text = "Calc";
            this.Button_Calc_et.UseVisualStyleBackColor = true;
            this.Button_Calc_et.Click += new System.EventHandler(this.Button_Calc_et_Click);
            // 
            // button_ViewPlot
            // 
            this.button_ViewPlot.Location = new System.Drawing.Point(12, 321);
            this.button_ViewPlot.Name = "button_ViewPlot";
            this.button_ViewPlot.Size = new System.Drawing.Size(70, 25);
            this.button_ViewPlot.TabIndex = 106;
            this.button_ViewPlot.Text = "View Plot";
            this.button_ViewPlot.UseVisualStyleBackColor = true;
            this.button_ViewPlot.Click += new System.EventHandler(this.Button_ViewPlot_Click);
            // 
            // textBox_Ec
            // 
            this.textBox_Ec.Location = new System.Drawing.Point(17, 112);
            this.textBox_Ec.Name = "textBox_Ec";
            this.textBox_Ec.Size = new System.Drawing.Size(100, 20);
            this.textBox_Ec.TabIndex = 104;
            // 
            // Label28
            // 
            this.Label28.AutoSize = true;
            this.Label28.Location = new System.Drawing.Point(14, 95);
            this.Label28.Name = "Label28";
            this.Label28.Size = new System.Drawing.Size(102, 13);
            this.Label28.TabIndex = 105;
            this.Label28.Text = "Elastic modulus (ksi)";
            // 
            // textBox_eps
            // 
            this.textBox_eps.Location = new System.Drawing.Point(14, 158);
            this.textBox_eps.Name = "textBox_eps";
            this.textBox_eps.Size = new System.Drawing.Size(100, 20);
            this.textBox_eps.TabIndex = 102;
            // 
            // Label27
            // 
            this.Label27.AutoSize = true;
            this.Label27.Location = new System.Drawing.Point(12, 141);
            this.Label27.Name = "Label27";
            this.Label27.Size = new System.Drawing.Size(103, 13);
            this.Label27.TabIndex = 103;
            this.Label27.Text = "Strain at peak stress";
            // 
            // textBox_ecu
            // 
            this.textBox_ecu.Location = new System.Drawing.Point(17, 240);
            this.textBox_ecu.Name = "textBox_ecu";
            this.textBox_ecu.Size = new System.Drawing.Size(100, 20);
            this.textBox_ecu.TabIndex = 100;
            // 
            // Label26
            // 
            this.Label26.AutoSize = true;
            this.Label26.Location = new System.Drawing.Point(14, 223);
            this.Label26.Name = "Label26";
            this.Label26.Size = new System.Drawing.Size(75, 13);
            this.Label26.TabIndex = 101;
            this.Label26.Text = "Ultimate Strain";
            // 
            // textBox_e3f
            // 
            this.textBox_e3f.Location = new System.Drawing.Point(17, 197);
            this.textBox_e3f.Name = "textBox_e3f";
            this.textBox_e3f.Size = new System.Drawing.Size(100, 20);
            this.textBox_e3f.TabIndex = 98;
            // 
            // Label25
            // 
            this.Label25.AutoSize = true;
            this.Label25.Location = new System.Drawing.Point(14, 180);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(79, 13);
            this.Label25.TabIndex = 99;
            this.Label25.Text = "Strain at 0.3*f\'c";
            // 
            // textBox_et
            // 
            this.textBox_et.Location = new System.Drawing.Point(220, 117);
            this.textBox_et.Name = "textBox_et";
            this.textBox_et.Size = new System.Drawing.Size(100, 20);
            this.textBox_et.TabIndex = 96;
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Location = new System.Drawing.Point(217, 102);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(116, 13);
            this.Label24.TabIndex = 97;
            this.Label24.Text = "Tension strain capacity";
            // 
            // textBox_fc
            // 
            this.textBox_fc.Location = new System.Drawing.Point(17, 73);
            this.textBox_fc.Name = "textBox_fc";
            this.textBox_fc.Size = new System.Drawing.Size(100, 20);
            this.textBox_fc.TabIndex = 94;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(14, 56);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(164, 13);
            this.Label4.TabIndex = 95;
            this.Label4.Text = "28 day compression strength (ksi)";
            // 
            // textBox_ft
            // 
            this.textBox_ft.Location = new System.Drawing.Point(220, 73);
            this.textBox_ft.Name = "textBox_ft";
            this.textBox_ft.Size = new System.Drawing.Size(100, 20);
            this.textBox_ft.TabIndex = 92;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(217, 56);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(108, 13);
            this.Label3.TabIndex = 93;
            this.Label3.Text = "Tension strength (ksi)";
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(286, 23);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(47, 25);
            this.button_Add.TabIndex = 118;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // comboBox_Names
            // 
            this.comboBox_Names.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Names.FormattingEnabled = true;
            this.comboBox_Names.Location = new System.Drawing.Point(15, 25);
            this.comboBox_Names.Name = "comboBox_Names";
            this.comboBox_Names.Size = new System.Drawing.Size(265, 21);
            this.comboBox_Names.TabIndex = 119;
            this.comboBox_Names.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Names_SelectedIndexChanged);
            // 
            // HsuConcreteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 356);
            this.Controls.Add(this.comboBox_Names);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Calc_e3f);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.Label_Name);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Button_Calc_ft);
            this.Controls.Add(this.button_Calc_E);
            this.Controls.Add(this.Button_Calc_et);
            this.Controls.Add(this.button_ViewPlot);
            this.Controls.Add(this.textBox_Ec);
            this.Controls.Add(this.Label28);
            this.Controls.Add(this.textBox_eps);
            this.Controls.Add(this.Label27);
            this.Controls.Add(this.textBox_ecu);
            this.Controls.Add(this.Label26);
            this.Controls.Add(this.textBox_e3f);
            this.Controls.Add(this.Label25);
            this.Controls.Add(this.textBox_et);
            this.Controls.Add(this.Label24);
            this.Controls.Add(this.textBox_fc);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.textBox_ft);
            this.Controls.Add(this.Label3);
            this.Name = "HsuConcreteForm";
            this.Text = "Hsu High Strength Concrete";
            this.Load += new System.EventHandler(this.HsuConcreteForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button button_Cancel;
        internal System.Windows.Forms.Button button_OK;
        internal System.Windows.Forms.Button button_Calc_e3f;
        internal System.Windows.Forms.Button button_Delete;
        internal System.Windows.Forms.Label Label_Name;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button Button_Calc_ft;
        internal System.Windows.Forms.Button button_Calc_E;
        internal System.Windows.Forms.Button Button_Calc_et;
        internal System.Windows.Forms.Button button_ViewPlot;
        internal System.Windows.Forms.TextBox textBox_Ec;
        internal System.Windows.Forms.Label Label28;
        internal System.Windows.Forms.TextBox textBox_eps;
        internal System.Windows.Forms.Label Label27;
        internal System.Windows.Forms.TextBox textBox_ecu;
        internal System.Windows.Forms.Label Label26;
        internal System.Windows.Forms.TextBox textBox_e3f;
        internal System.Windows.Forms.Label Label25;
        internal System.Windows.Forms.TextBox textBox_et;
        internal System.Windows.Forms.Label Label24;
        internal System.Windows.Forms.TextBox textBox_fc;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox textBox_ft;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Button button_Add;
        private UserControls.ComboBoxWithPrevious comboBox_Names;
    }
}