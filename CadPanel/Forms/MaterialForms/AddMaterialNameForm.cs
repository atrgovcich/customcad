﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Materials;

namespace CadPanel.Forms.MaterialForms
{
    public partial class AddMaterialNameForm : Form
    {
        private List<IGenericMaterial> _allMaterials;

        public string NewName { get; set; } = null;

        public AddMaterialNameForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;
        }

        private void AddMaterialNameForm_Load(object sender, EventArgs e)
        {

        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            string name = textBox_Name.Text;

            try
            {
                bool unique = true;

                if (_allMaterials != null && _allMaterials.Count > 0)
                {
                    for (int i = 0; i < _allMaterials.Count; i++)
                    {
                        if (string.Compare(_allMaterials[i].Name, name, ignoreCase: true) == 0)
                        {
                            unique = false;

                            break;
                        }
                    }
                }

                if (unique == false)
                {
                    throw new Exception($"The name '{name}' is already in use.  Please enter a unique material name.");
                }
                else
                {
                    NewName = name;

                    this.DialogResult = DialogResult.OK;

                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + ex.Message, "Input Error");
            }
        }
    }
}
