﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Materials;
using FiberSectionAnalysis.LimitStates;
using Materials.Inelastic.Steel;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using CadPanel.UserControls;
using System.ComponentModel;

namespace CadPanel.Forms.MaterialForms
{
    public partial class ModifiedRambergOsgoodForm : Form
    {
        private List<IGenericMaterial> _allMaterials;
        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();
        private List<IGenericMaterial> _addedMaterials = new List<IGenericMaterial>();
        private bool _onNewMaterial = false;
        private List<LimitState> _addedLimitStates = new List<LimitState>();

        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<IGenericMaterial> AddedMaterials
        {
            get
            {
                return _addedMaterials;
            }
        }

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }

        public ModifiedRambergOsgoodForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;

            comboBox_Names.BeforeUpdate += new CancelEventHandler(ComboBox_Names_BeforeUpdate);
        }

        private void ModifiedRambergOsgoodForm_Load(object sender, EventArgs e)
        {
            PopulateNamesComboBox();
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddMaterialNameForm frm = new AddMaterialNameForm(_allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ModifiedRambergOsgoodSteel newMat = new ModifiedRambergOsgoodSteel(frm.NewName);

                _allMaterials.Add(newMat);

                _addedMaterials.Add(newMat);

                comboBox_Names.Items.Add(newMat.Name);

                comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;

                _onNewMaterial = true;
            }

            frm.Dispose();
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete the selected material.  Any elements utilizing this material will have their material definition reset.  Proceed?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _onNewMaterial = false;

                int selIndex = comboBox_Names.SelectedIndex;

                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                comboBox_Names.Items.RemoveAt(selIndex);

                if (comboBox_Names.Items.Count > 0)
                {
                    if (selIndex <= comboBox_Names.Items.Count - 1)
                    {
                        comboBox_Names.SelectedIndex = selIndex;
                    }
                    else
                    {
                        comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;
                    }
                }
                else
                {
                    comboBox_Names.SelectedIndex = -1;

                    ComboBox_Names_SelectedIndexChanged(comboBox_Names, new EventArgs());
                }

                _allMaterials.Remove(genericMat);

                _deletedMaterials.Add(genericMat);
            }
        }

        private void ComboBox_Names_BeforeUpdate(object sender, CancelEventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (_onNewMaterial == true)
            {
                if (cB.PreviousIndex >= 0)
                {
                    _addedLimitStates.Add(CreateNewLimitState(Convert.ToString(cB.Items[cB.PreviousIndex])));
                }

                _onNewMaterial = false;
            }
        }

        private void ComboBox_Names_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (cB.SelectedIndex >= 0)
            {
                IGenericMaterial genericMat = GetMaterialFromName(cB.Text, _allMaterials);

                if (genericMat != null)
                {
                    ModifiedRambergOsgoodSteel castMat = (ModifiedRambergOsgoodSteel)genericMat;

                    textBox_fy.Text = Convert.ToString(castMat.Fy);
                    textBox_fu.Text = Convert.ToString(castMat.Fu);
                    textBox_Es.Text = Convert.ToString(castMat.Es);
                    textBox_Fpc.Text = Convert.ToString(castMat.Fpc);
                    textBox_eu.Text = Convert.ToString(castMat.eu);
                    textBox_fp0.Text = Convert.ToString(castMat.FP0);
                    textBox_r.Text = Convert.ToString(castMat.R);
                    textBox_C.Text = Convert.ToString(castMat.C);
                }
            }
            else
            {
                textBox_fy.Clear();
                textBox_fu.Clear();
                textBox_Es.Clear();
                textBox_Fpc.Clear();
                textBox_eu.Clear();
                textBox_fp0.Clear();
                textBox_r.Clear();
                textBox_C.Clear();
            }
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            if (_onNewMaterial == true)
            {
                _addedLimitStates.Add(CreateNewLimitState());
            }

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                if (genericMat != null)
                {
                    ModifiedRambergOsgoodSteel castMat = (ModifiedRambergOsgoodSteel)genericMat;

                    castMat.Fy = Convert.ToDouble(textBox_fy.Text);
                    castMat.Fu = Convert.ToDouble(textBox_fu.Text);
                    castMat.Es = Convert.ToDouble(textBox_Es.Text);
                    castMat.Fpc = Convert.ToDouble(textBox_Fpc.Text);
                    castMat.eu = Convert.ToDouble(textBox_eu.Text);
                    castMat.FP0 = Convert.ToDouble(textBox_fp0.Text);
                    castMat.R = Convert.ToDouble(textBox_r.Text);
                    castMat.C = Convert.ToDouble(textBox_C.Text);

                    if (_onNewMaterial == true)
                    {
                        _addedLimitStates.Add(CreateNewLimitState());

                        _onNewMaterial = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void Button_ViewPlot_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                ModifiedRambergOsgoodSteel castMat = new ModifiedRambergOsgoodSteel(genericMat.Name);

                castMat.Fy = Convert.ToDouble(textBox_fy.Text);
                castMat.Fu = Convert.ToDouble(textBox_fu.Text);
                castMat.Es = Convert.ToDouble(textBox_Es.Text);
                castMat.Fpc = Convert.ToDouble(textBox_Fpc.Text);
                castMat.eu = Convert.ToDouble(textBox_eu.Text);
                castMat.FP0 = Convert.ToDouble(textBox_fp0.Text);
                castMat.R = Convert.ToDouble(textBox_r.Text);
                castMat.C = Convert.ToDouble(textBox_C.Text);

                StressStrainChartForm frm = new StressStrainChartForm(castMat, -castMat.eu, castMat.eu);

                frm.ShowDialog();

                frm.Dispose();
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateInputs(out string message)
        {
            if (double.TryParse(textBox_fy.Text, out double fy) == true)
            {
                if (fy <= 0)
                {
                    message = "Yield strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Yield strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_fu.Text, out double fu) == true)
            {
                if (fu <= 0)
                {
                    message = "Ultimate strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Es.Text, out double Es) == true)
            {
                if (Es <= 0)
                {
                    message = "Elastic modulus must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Elastic modulus must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Fpc.Text, out double Fpc) == true)
            {
                if (Fpc <= 0)
                {
                    message = "Stress in compression at which material goes plastic must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Stress in compression at which material goes plastic must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_eu.Text, out double eu) == true)
            {
                if (eu <= 0)
                {
                    message = "Ultimate strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_fp0.Text, out double fp0) == true)
            {
                if (fp0 <= 0)
                {
                    message = "fp0 must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "fp0 must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_r.Text, out double R) == true)
            {
                if (R <= 0)
                {
                    message = "R must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "R must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_C.Text, out double C) == true)
            {
                if (C <= 0)
                {
                    message = "C must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "C must be a numeric value.";

                return false;
            }

            message = string.Empty;

            return true;
        }

        private void PopulateNamesComboBox()
        {
            if (_allMaterials != null)
            {
                comboBox_Names.Items.Clear();

                for (int i = 0; i < _allMaterials.Count; i++)
                {
                    IGenericMaterial genericMat = _allMaterials[i];

                    if (genericMat.GetType().Equals(typeof(ModifiedRambergOsgoodSteel)) == true)
                    {
                        comboBox_Names.Items.Add(genericMat.Name);
                    }
                }

                if (comboBox_Names.Items.Count > 0)
                {
                    comboBox_Names.SelectedIndex = 0;
                }
            }
        }

        private LimitState CreateNewLimitState()
        {
            IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

            ModifiedRambergOsgoodSteel castMat = (ModifiedRambergOsgoodSteel)genericMat;

            LimitState ls = new LimitState()
            {
                Material = castMat,
                CompressionLimitStrain = castMat.eu,
                TensionLimitStrain = castMat.eu,
                TerminationLimitState = true,
                Name = "Failure"
            };

            return ls;
        }

        private LimitState CreateNewLimitState(string name)
        {
            IGenericMaterial genericMat = GetMaterialFromName(name, _allMaterials);

            ModifiedRambergOsgoodSteel castMat = (ModifiedRambergOsgoodSteel)genericMat;

            LimitState ls = new LimitState()
            {
                Material = castMat,
                CompressionLimitStrain = castMat.eu,
                TensionLimitStrain = castMat.eu,
                TerminationLimitState = true,
                Name = "Failure"
            };

            return ls;
        }
    }
}
