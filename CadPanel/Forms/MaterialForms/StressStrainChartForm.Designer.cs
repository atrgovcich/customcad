﻿namespace CadPanel.Forms.MaterialForms
{
    partial class StressStrainChartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_MaterialName = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_MaterialName
            // 
            this.label_MaterialName.AutoSize = true;
            this.label_MaterialName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MaterialName.Location = new System.Drawing.Point(12, 9);
            this.label_MaterialName.Name = "label_MaterialName";
            this.label_MaterialName.Size = new System.Drawing.Size(73, 20);
            this.label_MaterialName.TabIndex = 0;
            this.label_MaterialName.Text = "Material";
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(555, 459);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 1;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // StressStrainChartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 494);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.label_MaterialName);
            this.Name = "StressStrainChartForm";
            this.Text = "Stress Strain Chart";
            this.Load += new System.EventHandler(this.StressStrainChartForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_MaterialName;
        private System.Windows.Forms.Button button_Close;
    }
}