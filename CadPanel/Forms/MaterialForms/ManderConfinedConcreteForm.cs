﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Materials;
using Materials.Inelastic.Concrete;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using FiberSectionAnalysis.LimitStates;
using CadPanel.UserControls;

namespace CadPanel.Forms.MaterialForms
{
    
    public partial class ManderConfinedConcreteForm : Form
    {
        private List<IGenericMaterial> _allMaterials;
        private List<IGenericMaterial> _deletedMaterials = new List<IGenericMaterial>();
        private List<IGenericMaterial> _addedMaterials = new List<IGenericMaterial>();
        private bool _onNewMaterial = false;
        private List<LimitState> _addedLimitStates = new List<LimitState>();

        public List<IGenericMaterial> DeletedMaterials
        {
            get
            {
                return _deletedMaterials;
            }
        }

        public List<IGenericMaterial> AddedMaterials
        {
            get
            {
                return _addedMaterials;
            }
        }

        public List<LimitState> AddedLimitStates
        {
            get
            {
                return _addedLimitStates;
            }
        }

        public ManderConfinedConcreteForm(List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _allMaterials = allMaterials;

            comboBox_Names.BeforeUpdate += new CancelEventHandler(ComboBox_Names_BeforeUpdate);
        }

        private void ManderConfinedConcreteForm_Load(object sender, EventArgs e)
        {
            PopulateNamesComboBox();
        }

        private void Button_Calc_ft_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc > 0)
                {
                    textBox_ft.Text = Convert.ToString(Round(7.5 * Sqrt(fc * 1000.0) / 1000.0, 3));
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Calc_et_Click(object sender, EventArgs e)
        {
            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft > 0)
                {
                    if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
                    {
                        if (Ec > 0)
                        {
                            textBox_et.Text = Convert.ToString(Round(ft / Ec, 5));
                        }
                        else
                        {
                            MessageBox.Show("The elastic modulus must be a positive value.", "Error", MessageBoxButtons.OK);
                        }
                    }
                    else
                    {
                        MessageBox.Show("The elastic modulus must be a numeric value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Calc_E_Click(object sender, EventArgs e)
        {
            IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

            if (genericMat != null)
            {
                ManderConfinedConcrete castMat = (ManderConfinedConcrete)genericMat;

                if (double.TryParse(textBox_fc.Text, out double fc) == true)
                {
                    if (fc > 0)
                    {
                        if (checkBox_AutoChooseEc.Checked == true)
                        {
                            textBox_Ec.Text = Convert.ToString(Round(castMat.ComputeElasticModulus(fc), 2));
                        }
                        else
                        {
                            ElasticModulusEquationType eqnType = (radioButton_NormalStrength.Checked == true) ? ElasticModulusEquationType.NormalStrength : ElasticModulusEquationType.HighStrength;

                            textBox_Ec.Text = Convert.ToString(Round(castMat.ComputeElasticModulus(fc, eqnType), 2));
                        }
                    }
                    else
                    {
                        MessageBox.Show("The compressive strength must be a positive value.", "Error", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    MessageBox.Show("The compressive strength must be a numeric value.", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void CheckBox_AutoChooseEc_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            if (cB.Checked == true)
            {
                groupBox_ElasticModulus.Hide();
            }
            else
            {
                groupBox_ElasticModulus.Show();
            }
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderConfinedConcrete castNat = (ManderConfinedConcrete)genericMat;

                    castNat.AutoChooseElasticModulusEquation = checkBox_AutoChooseEc.Checked;

                    if (checkBox_AutoChooseEc.Checked == false)
                    {
                        if (radioButton_NormalStrength.Checked == true)
                        {
                            castNat.ElasticModulusEquation = ElasticModulusEquationType.NormalStrength;
                        }
                        else
                        {
                            castNat.ElasticModulusEquation = ElasticModulusEquationType.HighStrength;
                        }
                    }

                    if (radioButton_Circular.Checked == true)
                    {
                        castNat.ConfiningType = ConfinementType.ClosedTies;
                    }
                    else
                    {
                        castNat.ConfiningType = ConfinementType.Spiral;
                    }

                    castNat.Fc = Convert.ToDouble(textBox_fc.Text);
                    castNat.TieSpacing = Convert.ToDouble(textBox_st.Text);
                    castNat.Ec = Convert.ToDouble(textBox_Ec.Text);
                    castNat.Fcc = Convert.ToDouble(textBox_fcc.Text);
                    castNat.ecu = Convert.ToDouble(textBox_ecu.Text);
                    castNat.et = -Convert.ToDouble(textBox_et.Text);
                    castNat.Ft = -Convert.ToDouble(textBox_ft.Text);

                    if (_onNewMaterial == true)
                    {
                        _addedLimitStates.Add(CreateNewLimitState());

                        _onNewMaterial = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private bool ValidateInputs(out string message)
        {
            if (double.TryParse(textBox_fc.Text, out double fc) == true)
            {
                if (fc <= 0)
                {
                    message = "Compressive strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Compressive strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_Ec.Text, out double Ec) == true)
            {
                if (Ec <= 0)
                {
                    message = "Elastic modulus must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Elastic modulus must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_st.Text, out double eps) == true)
            {
                if (eps <= 0)
                {
                    message = "Transverse rebar spacing must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Transverse rebar spacing must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_fcc.Text, out double esp) == true)
            {
                if (esp <= 0)
                {
                    message = "Confined core strength must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Confined core strength must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ecu.Text, out double ecu) == true)
            {
                if (ecu <= 0)
                {
                    message = "Ultimate strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Ultimate strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_et.Text, out double et) == true)
            {
                if (et < 0)
                {
                    message = "Tensile strain must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile strain must be a numeric value.";

                return false;
            }

            if (double.TryParse(textBox_ft.Text, out double ft) == true)
            {
                if (ft < 0)
                {
                    message = "Tensile stress must be greater than zero.";

                    return false;
                }
            }
            else
            {
                message = "Tensile stress must be a numeric value.";

                return false;
            }

            message = string.Empty;

            return true;
        }

        private void Button_ViewPlot_Click(object sender, EventArgs e)
        {
            if (ValidateInputs(out string message) == true)
            {
                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                ManderConfinedConcrete castMat = new ManderConfinedConcrete(genericMat.Name);

                castMat.Fc = Convert.ToDouble(textBox_fc.Text);
                castMat.Fcc = Convert.ToDouble(textBox_fcc.Text);
                castMat.Ec = Convert.ToDouble(textBox_Ec.Text);
                castMat.TieSpacing = Convert.ToDouble(textBox_st.Text);
                castMat.ecu = Convert.ToDouble(textBox_ecu.Text);
                castMat.et = -Convert.ToDouble(textBox_et.Text);
                castMat.Ft = -Convert.ToDouble(textBox_ft.Text);

                StressStrainChartForm frm = new StressStrainChartForm(castMat, 2.0 * castMat.et, castMat.ecu);

                frm.ShowDialog();

                frm.Dispose();
            }
            else
            {
                MessageBox.Show("An input error was detected:" + Environment.NewLine + Environment.NewLine + message, "Input Error", MessageBoxButtons.OK);
            }
        }

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete the selected material.  Any elements utilizing this material will have their material definition reset.  Proceed?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _onNewMaterial = false;

                int selIndex = comboBox_Names.SelectedIndex;

                IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

                comboBox_Names.Items.RemoveAt(selIndex);

                if (comboBox_Names.Items.Count > 0)
                {
                    if (selIndex <= comboBox_Names.Items.Count - 1)
                    {
                        comboBox_Names.SelectedIndex = selIndex;
                    }
                    else
                    {
                        comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;
                    }
                }
                else
                {
                    comboBox_Names.SelectedIndex = -1;

                    ComboBox_Names_SelectedIndexChanged(comboBox_Names, new EventArgs());
                }

                _allMaterials.Remove(genericMat);

                _deletedMaterials.Add(genericMat);
            }
        }

        private void ComboBox_Names_BeforeUpdate(object sender, CancelEventArgs e)
        {
            ComboBoxWithPrevious cB = (ComboBoxWithPrevious)sender;

            if (_onNewMaterial == true)
            {
                if (cB.PreviousIndex >= 0)
                {
                    _addedLimitStates.Add(CreateNewLimitState(Convert.ToString(cB.Items[cB.PreviousIndex])));
                }

                _onNewMaterial = false;
            }
        }

        private void ComboBox_Names_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cB = (ComboBox)sender;

            if (cB.SelectedIndex >= 0)
            {
                IGenericMaterial genericMat = GetMaterialFromName(cB.Text, _allMaterials);

                if (genericMat != null)
                {
                    ManderConfinedConcrete castMat = (ManderConfinedConcrete)genericMat;

                    textBox_fc.Text = Convert.ToString(castMat.Fc);
                    textBox_fcc.Text = Convert.ToString(castMat.Fcc);
                    textBox_ft.Text = Convert.ToString(-castMat.Ft);
                    textBox_et.Text = Convert.ToString(-castMat.et);
                    textBox_st.Text = Convert.ToString(castMat.TieSpacing);
                    textBox_ecu.Text = Convert.ToString(castMat.ecu);
                    textBox_Ec.Text = Convert.ToString(castMat.Ec);

                    checkBox_AutoChooseEc.Checked = castMat.AutoChooseElasticModulusEquation;

                    if (castMat.ElasticModulusEquation.Equals(ElasticModulusEquationType.NormalStrength) == true)
                    {
                        radioButton_NormalStrength.Checked = true;
                    }
                    else
                    {
                        radioButton_HighStrength.Checked = true;
                    }

                    if (castMat.ConfiningType.Equals(ConfinementType.Spiral) == true)
                    {
                        radioButton_Spiral.Checked = true;
                    }
                    else
                    {
                        radioButton_Circular.Checked = true;
                    }
                }
            }
            else
            {
                textBox_fc.Clear();
                textBox_fcc.Clear();
                textBox_ft.Clear();
                textBox_et.Clear();
                textBox_st.Clear();
                textBox_ecu.Clear();
                textBox_Ec.Clear();
            }
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            AddMaterialNameForm frm = new AddMaterialNameForm(_allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ManderConfinedConcrete newMat = new ManderConfinedConcrete(frm.NewName);

                _allMaterials.Add(newMat);

                _addedMaterials.Add(newMat);

                comboBox_Names.Items.Add(newMat.Name);

                comboBox_Names.SelectedIndex = comboBox_Names.Items.Count - 1;

                _onNewMaterial = true;
            }

            frm.Dispose();
        }

        private void PopulateNamesComboBox()
        {
            if (_allMaterials != null)
            {
                comboBox_Names.Items.Clear();

                for (int i = 0; i < _allMaterials.Count; i++)
                {
                    IGenericMaterial genericMat = _allMaterials[i];

                    if (genericMat.GetType().Equals(typeof(ManderConfinedConcrete)) == true)
                    {
                        comboBox_Names.Items.Add(genericMat.Name);
                    }
                }

                if (comboBox_Names.Items.Count > 0)
                {
                    comboBox_Names.SelectedIndex = 0;
                }
            }
        }

        private LimitState CreateNewLimitState()
        {
            IGenericMaterial genericMat = GetMaterialFromName(comboBox_Names.Text, _allMaterials);

            ManderConfinedConcrete castMat = (ManderConfinedConcrete)genericMat;

            LimitState ls = new LimitState()
            {
                Material = castMat,
                CompressionLimitStrain = castMat.ecu,
                TensionLimitStrain = 1.0,
                TerminationLimitState = true,
                Name = "Core Crushing"
            };

            return ls;
        }

        private LimitState CreateNewLimitState(string name)
        {
            IGenericMaterial genericMat = GetMaterialFromName(name, _allMaterials);

            ManderConfinedConcrete castMat = (ManderConfinedConcrete)genericMat;

            LimitState ls = new LimitState()
            {
                Material = castMat,
                CompressionLimitStrain = castMat.ecu,
                TensionLimitStrain = 1.0,
                TerminationLimitState = true,
                Name = "Core Crushing"
            };

            return ls;
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            if (_onNewMaterial == true)
            {
                _addedLimitStates.Add(CreateNewLimitState());
            }

            this.Close();
        }
    }
}
