﻿namespace CadPanel.Forms.MaterialForms
{
    partial class MaterialSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.Label_Custom = new System.Windows.Forms.Label();
            this.Label_PWL_Conf = new System.Windows.Forms.Label();
            this.Label_Restreppo = new System.Windows.Forms.Label();
            this.Button8 = new System.Windows.Forms.Button();
            this.Label_ManderConf = new System.Windows.Forms.Label();
            this.Button_PWL_Edit = new System.Windows.Forms.Button();
            this.Button_Restreppo_Edit = new System.Windows.Forms.Button();
            this.Button_ManderConf_Edit = new System.Windows.Forms.Button();
            this.Label_Hsu = new System.Windows.Forms.Label();
            this.Label_Crisafulli = new System.Windows.Forms.Label();
            this.Label_ManderUnconf = new System.Windows.Forms.Label();
            this.Button_Hsu_Edit = new System.Windows.Forms.Button();
            this.Button_ManderUnconf_Edit = new System.Windows.Forms.Button();
            this.Button18 = new System.Windows.Forms.Button();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.Label_Custom_Steel = new System.Windows.Forms.Label();
            this.Label_CyclicBLH = new System.Windows.Forms.Label();
            this.Label_Kent = new System.Windows.Forms.Label();
            this.Label_MenPint = new System.Windows.Forms.Label();
            this.Label_Mander_Steel = new System.Windows.Forms.Label();
            this.Button20 = new System.Windows.Forms.Button();
            this.Button_KentSteel_Edit = new System.Windows.Forms.Button();
            this.Button11 = new System.Windows.Forms.Button();
            this.Button_MenPint_Edit = new System.Windows.Forms.Button();
            this.Button_ManderSteel_Edit = new System.Windows.Forms.Button();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.GroupBox4 = new System.Windows.Forms.GroupBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Button_RambergOsgood_Edit = new System.Windows.Forms.Button();
            this.button_Close = new System.Windows.Forms.Button();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            this.GroupBox3.SuspendLayout();
            this.TabPage3.SuspendLayout();
            this.GroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Location = new System.Drawing.Point(12, 12);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(496, 287);
            this.TabControl1.TabIndex = 12;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.GroupBox1);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(488, 261);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Concrete Models";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.Label_Custom);
            this.GroupBox1.Controls.Add(this.Label_PWL_Conf);
            this.GroupBox1.Controls.Add(this.Label_Restreppo);
            this.GroupBox1.Controls.Add(this.Button8);
            this.GroupBox1.Controls.Add(this.Label_ManderConf);
            this.GroupBox1.Controls.Add(this.Button_PWL_Edit);
            this.GroupBox1.Controls.Add(this.Button_Restreppo_Edit);
            this.GroupBox1.Controls.Add(this.Button_ManderConf_Edit);
            this.GroupBox1.Controls.Add(this.Label_Hsu);
            this.GroupBox1.Controls.Add(this.Label_Crisafulli);
            this.GroupBox1.Controls.Add(this.Label_ManderUnconf);
            this.GroupBox1.Controls.Add(this.Button_Hsu_Edit);
            this.GroupBox1.Controls.Add(this.Button_ManderUnconf_Edit);
            this.GroupBox1.Controls.Add(this.Button18);
            this.GroupBox1.Location = new System.Drawing.Point(15, 15);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(449, 224);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Concrete Material Models";
            // 
            // Label_Custom
            // 
            this.Label_Custom.AutoSize = true;
            this.Label_Custom.Enabled = false;
            this.Label_Custom.Location = new System.Drawing.Point(57, 192);
            this.Label_Custom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Custom.Name = "Label_Custom";
            this.Label_Custom.Size = new System.Drawing.Size(120, 13);
            this.Label_Custom.TabIndex = 18;
            this.Label_Custom.Text = "Custom Concrete Model";
            // 
            // Label_PWL_Conf
            // 
            this.Label_PWL_Conf.AutoSize = true;
            this.Label_PWL_Conf.Location = new System.Drawing.Point(1, 165);
            this.Label_PWL_Conf.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_PWL_Conf.Name = "Label_PWL_Conf";
            this.Label_PWL_Conf.Size = new System.Drawing.Size(178, 13);
            this.Label_PWL_Conf.TabIndex = 76;
            this.Label_PWL_Conf.Text = "Piecewise Linear Confined Concrete";
            // 
            // Label_Restreppo
            // 
            this.Label_Restreppo.AutoSize = true;
            this.Label_Restreppo.Location = new System.Drawing.Point(35, 138);
            this.Label_Restreppo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Restreppo.Name = "Label_Restreppo";
            this.Label_Restreppo.Size = new System.Drawing.Size(141, 13);
            this.Label_Restreppo.TabIndex = 75;
            this.Label_Restreppo.Text = "Restrepo Confined Concrete";
            // 
            // Button8
            // 
            this.Button8.Enabled = false;
            this.Button8.Location = new System.Drawing.Point(181, 188);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(52, 20);
            this.Button8.TabIndex = 5;
            this.Button8.Text = "Edit";
            this.Button8.UseVisualStyleBackColor = true;
            // 
            // Label_ManderConf
            // 
            this.Label_ManderConf.AutoSize = true;
            this.Label_ManderConf.Location = new System.Drawing.Point(43, 112);
            this.Label_ManderConf.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_ManderConf.Name = "Label_ManderConf";
            this.Label_ManderConf.Size = new System.Drawing.Size(134, 13);
            this.Label_ManderConf.TabIndex = 74;
            this.Label_ManderConf.Text = "Mander Confined Concrete";
            // 
            // Button_PWL_Edit
            // 
            this.Button_PWL_Edit.Location = new System.Drawing.Point(181, 162);
            this.Button_PWL_Edit.Name = "Button_PWL_Edit";
            this.Button_PWL_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_PWL_Edit.TabIndex = 70;
            this.Button_PWL_Edit.Text = "Edit";
            this.Button_PWL_Edit.UseVisualStyleBackColor = true;
            this.Button_PWL_Edit.Click += new System.EventHandler(this.Button_PWL_Edit_Click);
            // 
            // Button_Restreppo_Edit
            // 
            this.Button_Restreppo_Edit.Location = new System.Drawing.Point(181, 135);
            this.Button_Restreppo_Edit.Name = "Button_Restreppo_Edit";
            this.Button_Restreppo_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_Restreppo_Edit.TabIndex = 69;
            this.Button_Restreppo_Edit.Text = "Edit";
            this.Button_Restreppo_Edit.UseVisualStyleBackColor = true;
            this.Button_Restreppo_Edit.Click += new System.EventHandler(this.Button_Restreppo_Edit_Click);
            // 
            // Button_ManderConf_Edit
            // 
            this.Button_ManderConf_Edit.Location = new System.Drawing.Point(181, 109);
            this.Button_ManderConf_Edit.Name = "Button_ManderConf_Edit";
            this.Button_ManderConf_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_ManderConf_Edit.TabIndex = 68;
            this.Button_ManderConf_Edit.Text = "Edit";
            this.Button_ManderConf_Edit.UseVisualStyleBackColor = true;
            this.Button_ManderConf_Edit.Click += new System.EventHandler(this.Button_ManderConf_Edit_Click);
            // 
            // Label_Hsu
            // 
            this.Label_Hsu.AutoSize = true;
            this.Label_Hsu.Location = new System.Drawing.Point(36, 85);
            this.Label_Hsu.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Hsu.Name = "Label_Hsu";
            this.Label_Hsu.Size = new System.Drawing.Size(140, 13);
            this.Label_Hsu.TabIndex = 17;
            this.Label_Hsu.Text = "Hsu High-Strength Concrete";
            // 
            // Label_Crisafulli
            // 
            this.Label_Crisafulli.AutoSize = true;
            this.Label_Crisafulli.Enabled = false;
            this.Label_Crisafulli.Location = new System.Drawing.Point(54, 58);
            this.Label_Crisafulli.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Crisafulli.Name = "Label_Crisafulli";
            this.Label_Crisafulli.Size = new System.Drawing.Size(123, 13);
            this.Label_Crisafulli.TabIndex = 16;
            this.Label_Crisafulli.Text = "Crisafulli Concrete Model";
            // 
            // Label_ManderUnconf
            // 
            this.Label_ManderUnconf.AutoSize = true;
            this.Label_ManderUnconf.Location = new System.Drawing.Point(31, 31);
            this.Label_ManderUnconf.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_ManderUnconf.Name = "Label_ManderUnconf";
            this.Label_ManderUnconf.Size = new System.Drawing.Size(147, 13);
            this.Label_ManderUnconf.TabIndex = 15;
            this.Label_ManderUnconf.Text = "Mander Unconfined Concrete";
            // 
            // Button_Hsu_Edit
            // 
            this.Button_Hsu_Edit.Location = new System.Drawing.Point(181, 82);
            this.Button_Hsu_Edit.Name = "Button_Hsu_Edit";
            this.Button_Hsu_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_Hsu_Edit.TabIndex = 13;
            this.Button_Hsu_Edit.Text = "Edit";
            this.Button_Hsu_Edit.UseVisualStyleBackColor = true;
            this.Button_Hsu_Edit.Click += new System.EventHandler(this.Button_Hsu_Edit_Click);
            // 
            // Button_ManderUnconf_Edit
            // 
            this.Button_ManderUnconf_Edit.Location = new System.Drawing.Point(181, 28);
            this.Button_ManderUnconf_Edit.Name = "Button_ManderUnconf_Edit";
            this.Button_ManderUnconf_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_ManderUnconf_Edit.TabIndex = 8;
            this.Button_ManderUnconf_Edit.Text = "Edit";
            this.Button_ManderUnconf_Edit.UseVisualStyleBackColor = true;
            this.Button_ManderUnconf_Edit.Click += new System.EventHandler(this.Button_ManderUnconf_Edit_Click);
            // 
            // Button18
            // 
            this.Button18.Enabled = false;
            this.Button18.Location = new System.Drawing.Point(181, 55);
            this.Button18.Name = "Button18";
            this.Button18.Size = new System.Drawing.Size(52, 20);
            this.Button18.TabIndex = 7;
            this.Button18.Text = "Edit";
            this.Button18.UseVisualStyleBackColor = true;
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.GroupBox3);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(488, 261);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Longitudinal Steel";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.Label_Custom_Steel);
            this.GroupBox3.Controls.Add(this.Label_CyclicBLH);
            this.GroupBox3.Controls.Add(this.Label_Kent);
            this.GroupBox3.Controls.Add(this.Label_MenPint);
            this.GroupBox3.Controls.Add(this.Label_Mander_Steel);
            this.GroupBox3.Controls.Add(this.Button20);
            this.GroupBox3.Controls.Add(this.Button_KentSteel_Edit);
            this.GroupBox3.Controls.Add(this.Button11);
            this.GroupBox3.Controls.Add(this.Button_MenPint_Edit);
            this.GroupBox3.Controls.Add(this.Button_ManderSteel_Edit);
            this.GroupBox3.Location = new System.Drawing.Point(15, 15);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(467, 240);
            this.GroupBox3.TabIndex = 1;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Steel Model";
            // 
            // Label_Custom_Steel
            // 
            this.Label_Custom_Steel.AutoSize = true;
            this.Label_Custom_Steel.Enabled = false;
            this.Label_Custom_Steel.Location = new System.Drawing.Point(75, 138);
            this.Label_Custom_Steel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Custom_Steel.Name = "Label_Custom_Steel";
            this.Label_Custom_Steel.Size = new System.Drawing.Size(101, 13);
            this.Label_Custom_Steel.TabIndex = 24;
            this.Label_Custom_Steel.Text = "Custom Steel Model";
            // 
            // Label_CyclicBLH
            // 
            this.Label_CyclicBLH.AutoSize = true;
            this.Label_CyclicBLH.Enabled = false;
            this.Label_CyclicBLH.Location = new System.Drawing.Point(41, 111);
            this.Label_CyclicBLH.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_CyclicBLH.Name = "Label_CyclicBLH";
            this.Label_CyclicBLH.Size = new System.Drawing.Size(138, 13);
            this.Label_CyclicBLH.TabIndex = 23;
            this.Label_CyclicBLH.Text = "Cyclic Bi-Linear Steel Model";
            // 
            // Label_Kent
            // 
            this.Label_Kent.AutoSize = true;
            this.Label_Kent.Enabled = false;
            this.Label_Kent.Location = new System.Drawing.Point(90, 84);
            this.Label_Kent.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Kent.Name = "Label_Kent";
            this.Label_Kent.Size = new System.Drawing.Size(88, 13);
            this.Label_Kent.TabIndex = 22;
            this.Label_Kent.Text = "Kent Steel Model";
            // 
            // Label_MenPint
            // 
            this.Label_MenPint.AutoSize = true;
            this.Label_MenPint.Location = new System.Drawing.Point(33, 58);
            this.Label_MenPint.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_MenPint.Name = "Label_MenPint";
            this.Label_MenPint.Size = new System.Drawing.Size(144, 13);
            this.Label_MenPint.TabIndex = 21;
            this.Label_MenPint.Text = "Menegotto-Pinto Steel Model";
            // 
            // Label_Mander_Steel
            // 
            this.Label_Mander_Steel.AutoSize = true;
            this.Label_Mander_Steel.Location = new System.Drawing.Point(76, 31);
            this.Label_Mander_Steel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_Mander_Steel.Name = "Label_Mander_Steel";
            this.Label_Mander_Steel.Size = new System.Drawing.Size(102, 13);
            this.Label_Mander_Steel.TabIndex = 20;
            this.Label_Mander_Steel.Text = "Mander Steel Model";
            // 
            // Button20
            // 
            this.Button20.Enabled = false;
            this.Button20.Location = new System.Drawing.Point(181, 108);
            this.Button20.Name = "Button20";
            this.Button20.Size = new System.Drawing.Size(52, 20);
            this.Button20.TabIndex = 15;
            this.Button20.Text = "Edit";
            this.Button20.UseVisualStyleBackColor = true;
            // 
            // Button_KentSteel_Edit
            // 
            this.Button_KentSteel_Edit.Enabled = false;
            this.Button_KentSteel_Edit.Location = new System.Drawing.Point(181, 81);
            this.Button_KentSteel_Edit.Name = "Button_KentSteel_Edit";
            this.Button_KentSteel_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_KentSteel_Edit.TabIndex = 13;
            this.Button_KentSteel_Edit.Text = "Edit";
            this.Button_KentSteel_Edit.UseVisualStyleBackColor = true;
            // 
            // Button11
            // 
            this.Button11.Enabled = false;
            this.Button11.Location = new System.Drawing.Point(181, 135);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(52, 20);
            this.Button11.TabIndex = 9;
            this.Button11.Text = "Edit";
            this.Button11.UseVisualStyleBackColor = true;
            // 
            // Button_MenPint_Edit
            // 
            this.Button_MenPint_Edit.Location = new System.Drawing.Point(181, 55);
            this.Button_MenPint_Edit.Name = "Button_MenPint_Edit";
            this.Button_MenPint_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_MenPint_Edit.TabIndex = 4;
            this.Button_MenPint_Edit.Text = "Edit";
            this.Button_MenPint_Edit.UseVisualStyleBackColor = true;
            this.Button_MenPint_Edit.Click += new System.EventHandler(this.Button_MenPint_Edit_Click);
            // 
            // Button_ManderSteel_Edit
            // 
            this.Button_ManderSteel_Edit.Location = new System.Drawing.Point(181, 28);
            this.Button_ManderSteel_Edit.Name = "Button_ManderSteel_Edit";
            this.Button_ManderSteel_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_ManderSteel_Edit.TabIndex = 2;
            this.Button_ManderSteel_Edit.Text = "Edit";
            this.Button_ManderSteel_Edit.UseVisualStyleBackColor = true;
            this.Button_ManderSteel_Edit.Click += new System.EventHandler(this.Button_ManderSteel_Edit_Click);
            // 
            // TabPage3
            // 
            this.TabPage3.Controls.Add(this.GroupBox4);
            this.TabPage3.Location = new System.Drawing.Point(4, 22);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(488, 261);
            this.TabPage3.TabIndex = 5;
            this.TabPage3.Text = "Prestressing Steel";
            this.TabPage3.UseVisualStyleBackColor = true;
            // 
            // GroupBox4
            // 
            this.GroupBox4.Controls.Add(this.Label1);
            this.GroupBox4.Controls.Add(this.Button_RambergOsgood_Edit);
            this.GroupBox4.Location = new System.Drawing.Point(15, 15);
            this.GroupBox4.Name = "GroupBox4";
            this.GroupBox4.Size = new System.Drawing.Size(457, 78);
            this.GroupBox4.TabIndex = 1;
            this.GroupBox4.TabStop = false;
            this.GroupBox4.Text = "Prestressing Steel Material Models";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(43, 32);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(133, 13);
            this.Label1.TabIndex = 21;
            this.Label1.Text = "Modified Ramberg-Osgood";
            // 
            // Button_RambergOsgood_Edit
            // 
            this.Button_RambergOsgood_Edit.Location = new System.Drawing.Point(181, 29);
            this.Button_RambergOsgood_Edit.Name = "Button_RambergOsgood_Edit";
            this.Button_RambergOsgood_Edit.Size = new System.Drawing.Size(52, 20);
            this.Button_RambergOsgood_Edit.TabIndex = 14;
            this.Button_RambergOsgood_Edit.Text = "Edit";
            this.Button_RambergOsgood_Edit.UseVisualStyleBackColor = true;
            this.Button_RambergOsgood_Edit.Click += new System.EventHandler(this.Button_RambergOsgood_Edit_Click);
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(433, 305);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 13;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // MaterialSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 341);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.TabControl1);
            this.MaximumSize = new System.Drawing.Size(540, 380);
            this.MinimumSize = new System.Drawing.Size(540, 380);
            this.Name = "MaterialSelectionForm";
            this.Text = "MaterialSelectionForm";
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.TabPage3.ResumeLayout(false);
            this.GroupBox4.ResumeLayout(false);
            this.GroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label Label_Custom;
        internal System.Windows.Forms.Label Label_PWL_Conf;
        internal System.Windows.Forms.Label Label_Restreppo;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Label Label_ManderConf;
        internal System.Windows.Forms.Button Button_PWL_Edit;
        internal System.Windows.Forms.Button Button_Restreppo_Edit;
        internal System.Windows.Forms.Button Button_ManderConf_Edit;
        internal System.Windows.Forms.Label Label_Hsu;
        internal System.Windows.Forms.Label Label_Crisafulli;
        internal System.Windows.Forms.Label Label_ManderUnconf;
        internal System.Windows.Forms.Button Button_Hsu_Edit;
        internal System.Windows.Forms.Button Button_ManderUnconf_Edit;
        internal System.Windows.Forms.Button Button18;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.Label Label_Custom_Steel;
        internal System.Windows.Forms.Label Label_CyclicBLH;
        internal System.Windows.Forms.Label Label_Kent;
        internal System.Windows.Forms.Label Label_MenPint;
        internal System.Windows.Forms.Label Label_Mander_Steel;
        internal System.Windows.Forms.Button Button20;
        internal System.Windows.Forms.Button Button_KentSteel_Edit;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button Button_MenPint_Edit;
        internal System.Windows.Forms.Button Button_ManderSteel_Edit;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.GroupBox GroupBox4;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button Button_RambergOsgood_Edit;
        private System.Windows.Forms.Button button_Close;
    }
}