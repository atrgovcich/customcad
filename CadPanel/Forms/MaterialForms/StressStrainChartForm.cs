﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Materials;
using Utilities.Charting;
using OxyPlot.WindowsForms;
using Utilities.Geometry;

namespace CadPanel.Forms.MaterialForms
{
    public partial class StressStrainChartForm : Form
    {
        private IGenericMaterial _material;
        private double _minStrain = 0;
        private double _maxStrain = 0;

        private Panel _chartPanel;
        private int _chartWidth = 100;
        private int _chartHeight = 100;
        private PlotView _plotView;
        private Chart2D _chart = null;

        public StressStrainChartForm(IGenericMaterial material, double minStrain, double maxStrain)
        {
            InitializeComponent();

            _material = material;

            _minStrain = minStrain;

            _maxStrain = maxStrain;

            label_MaterialName.Text = material.Name;
        }

        private void StressStrainChartForm_Load(object sender, EventArgs e)
        {
            InitializeChart();

            List<PointD> points = GetStressStrainCurve();

            PopulateChart(points);

            this.Resize += new EventHandler(StressStrainChartForm_Resize);

            StressStrainChartForm_Resize(this, new EventArgs());
        }

        private void InitializeChart()
        {
            _chartWidth = this.Width - 40;

            _chartHeight = this.Height - 80;

            _chartPanel = new Panel()
            {
                Location = new System.Drawing.Point(20, 40),
                BorderStyle = BorderStyle.Fixed3D
            };

            _chartPanel.Size = new Size(_chartWidth, _chartHeight);

            _plotView = new PlotView();

            _plotView.Location = new System.Drawing.Point(0, 0);

            _plotView.Width = _chartWidth;

            _plotView.Height = _chartHeight;

            _chartPanel.Controls.Add(_plotView);

            this.Controls.Add(_chartPanel);
        }

        private void ResizeChart()
        {
            _chartWidth = this.Width - 60;

            _chartHeight = this.Height - 140;

            _chartPanel.Size = new Size(_chartWidth, _chartHeight);

            _plotView.Size = new Size(_chartWidth, _chartHeight);
        }

        private void StressStrainChartForm_Resize(object sender, EventArgs e)
        {
            ResizeChart();

            button_Close.Location = new Point(this.Width - button_Close.Width - 30, this.Height - button_Close.Height - 55);
        }

        private List<PointD> GetStressStrainCurve()
        {
            List<PointD> points = new List<PointD>();

            if (_material != null)
            {
                double strainInterval = (_maxStrain - _minStrain) / 100;

                for (double i = _minStrain; i <= _maxStrain; i += strainInterval)
                {
                    double stress = _material.MonotonicStressStrain(i);

                    points.Add(new PointD(i, stress));
                }
            }

            return points;
        }

        private void PopulateChart(List<PointD> points)
        {
            _chart = new Chart2D($"Stress Strain Diagram");

            _chart.HorizontalAxisLabel = "Strain";

            _chart.VerticalAxisLabel = "Stress (ksi)";

            if (points != null && points.Count > 0)
            {
                List<PointD> xyData = points;

                double xMax = xyData.Select(p => p.X).Max(x => x);
                double xMin = xyData.Select(p => p.X).Min(x => x);

                double yMax = xyData.Select(p => p.Y).Max(x => x);
                double yMin = xyData.Select(p => p.Y).Min(x => x);

                _chart.AddSeriesFromXY(xyData, "S");

                _chart.SetHorizontalAxisBounds(0.9 * xMin, 1.1 * xMax);
                _chart.SetVerticalAxisBounds(0.9 * yMin, 1.1 * yMax);
            }

            _chart.SetInterpolation(InterpolationType.None);

            _plotView.Model = _chart.Model;

            _plotView.Invalidate();
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
