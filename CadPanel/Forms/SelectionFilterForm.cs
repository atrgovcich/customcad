﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using CadPanel.DrawingElements;
using Utilities.Extensions.DotNetNative;
using System.Drawing;

namespace CadPanel.Forms
{
    public partial class SelectionFilterForm : Form
    {
        private List<Type> _filteredTypes = new List<Type>();

        private Dictionary<Type, CheckBox> _checkBoxes;

        public List<Type> FilteredTypes
        {
            get
            {
                return _filteredTypes;
            }
        }

        public SelectionFilterForm(List<Type> selectedTypes)
        {
            InitializeComponent();

            _checkBoxes = BuildCheckBoxes(selectedTypes);

            //Add check boxes to panel
            foreach (KeyValuePair<Type, CheckBox> kvp in _checkBoxes)
            {
                panel_Boxes.Controls.Add(kvp.Value);
            }
        }

        private Dictionary<Type, CheckBox> BuildCheckBoxes(List<Type> selectedTypes)
        {
            Dictionary<Type, CheckBox> checkBoxes = new Dictionary<Type, CheckBox>();

            int currentX = 7;
            int currentY = 5;

            for (int i = 0; i < selectedTypes.Count; i++)
            {
                string className = selectedTypes[i].Name;

                className = className.Left(className.Length - 1);

                CheckBox cb = new CheckBox();
                cb.Text = className;
                cb.Checked = true;
                cb.Location = new Point(currentX, currentY);

                currentY += cb.Height;

                checkBoxes.Add(selectedTypes[i], cb);
            }

            return checkBoxes;
        }
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            _filteredTypes.Clear();

            foreach(KeyValuePair<Type, CheckBox> kvp in _checkBoxes)
            {
                if (kvp.Value.Checked == true)
                {
                    _filteredTypes.Add(kvp.Key);
                }
            }

            this.Close();
        }

        private void SelectionFilterForm_Load(object sender, EventArgs e)
        {

        }
    }
}
