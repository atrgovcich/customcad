﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using FiberSectionAnalysis.AxialForceMomentAnalysis;
using Materials.Inelastic.Concrete;
using CadPanel.DrawingElements;
using Utilities.Geometry;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Elements;
using Materials;
using Utilities.Extensions.DotNetNative;
using FiberSectionAnalysis.LimitStates;
using CadPanel.PolyBool;
using Utilities.Charting;
using static System.Math;
using FiberSectionAnalysis.Enums;
using System.Runtime.InteropServices;
using FiberSectionAnalysis.Loads;
using System.Reflection;
using OxyPlot.WindowsForms;
using FiberSectionAnalysis.Geometry;

namespace CadPanel.Forms
{
    public partial class AxialForceMomentAnalysisForm : Form
    {
        #region Drawing Suspension Static
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;
        #endregion

        #region Private Fields
        private List<AxialForceMomentAnalysisCase> _pmCases;

        List<IGenericMaterial> _allMaterials;

        private bool _suspendAutoPopulation = false;

        private string _defaultLoadingLabelString = "No loading defined";

        private string _defaultLimitStateLabelString = "No limit states defined";

        private AxialForceMomentAnalysisCase _selectedAnalysisCase = null;

        private List<IDrawingElement> _selectedElements = null;

        private int _precision;

        private List<LimitState> _allLimitStates;

        private Chart2D _chart = null;

        private WebBrowser _chartBrowser = null;

        private int _chartWidth = 400;
        private int _chartHeight = 300;

        private bool _applyPhiFactor = false;
        private SectionAxis _axisType = SectionAxis.Major;

        private PlotView _plotView;

        #endregion

        public AxialForceMomentAnalysisForm(List<AxialForceMomentAnalysisCase> pmCases, List<IDrawingElement> selectedElements, int precision, List<LimitState> allLimitStates, List<IGenericMaterial> allMaterials)
        {
            InitializeComponent();

            _pmCases = pmCases;

            _selectedElements = selectedElements;

            _precision = precision;

            _allLimitStates = allLimitStates;

            _allMaterials = allMaterials;

            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
                BindingFlags.Instance | BindingFlags.SetProperty, null,
                dataGridView_PM, new object[] { true });
        }

        private void AxialForceMomentAnalysisForm_Load(object sender, EventArgs e)
        {
            textBox_MeshSize.TextChanged += new EventHandler(TextBox_MeshSize_TextChanged);
            textBox_NumberOfPoints.TextChanged += new EventHandler(TextBox_NumberOfPoints_TextChanged);
            textBox_SingleAngle.TextChanged += new EventHandler(TextBox_SingleAngle_TextChanged);
            textBox_AngleInterval.TextChanged += new EventHandler(TextBox_AngleInterval_TextChanged);

            PopulateAnalysisCaseComboBox();

            if (_pmCases != null && _pmCases.Count > 0)
            {
                comboBox_AnalysisCases.SelectedIndex = 0;
            }
            else
            {
                DisableAllBoxes();
            }

            _chartWidth = splitContainer_PM.Panel2.Width - (groupBox_PMPlot.Location.X + groupBox_PMPlot.Width) - 30;

            _chartHeight = groupBox_PMPlot.Location.Y + groupBox_PMPlot.Height - groupBox_PlotType.Location.Y;

            Panel browserPanel = new Panel()
            {
                Location = new System.Drawing.Point(groupBox_PlotType.Location.X + groupBox_PlotType.Width + 15, groupBox_PlotType.Location.Y),
                Width = _chartWidth,
                Height = _chartHeight,
                BorderStyle = BorderStyle.Fixed3D
            };

            _plotView = new PlotView();

            _plotView.Location = new System.Drawing.Point(0,0);

            _plotView.Width = _chartWidth;

            _plotView.Height = _chartHeight;

            browserPanel.Controls.Add(_plotView);

            splitContainer_PM.Panel2.Controls.Add(browserPanel);

            groupBox_MMPlot.Location = groupBox_PMPlot.Location;

            groupBox_MMPlot.Hide();

            dataGridView_PM.Hide();
        }

        #region Private Methods
        private void PopulateAnalysisCaseComboBox()
        {
            if (_pmCases != null)
            {
                comboBox_AnalysisCases.Items.Clear();

                for (int i = 0; i < _pmCases.Count; i++)
                {
                    comboBox_AnalysisCases.Items.Add(_pmCases[i].Name);
                }
            }
        }

        private void PopulateAnalysisCaseInformation(string caseName)
        {
            _suspendAutoPopulation = true;

            AxialForceMomentAnalysisCase pmCase = GetAnalysisCaseFromName(caseName);

            if (pmCase != null)
            {
                textBox_MeshSize.Text = Convert.ToString(pmCase.MeshSize);

                textBox_NumberOfPoints.Text = Convert.ToString(pmCase.NumberOfPoints);

                if (pmCase.AnalysisType.Equals(AxialForceMomentAnalysisType.SingleAngle) == true)
                {
                    textBox_SingleAngle.Text = Convert.ToString(pmCase.SingleAngle);

                    textBox_AngleInterval.Clear();

                    radioButton_RunSingle.Checked = true;
                }
                else
                {
                    textBox_AngleInterval.Text = Convert.ToString(pmCase.AngleInterval);

                    textBox_SingleAngle.Clear();

                    radioButton_RunSuite.Checked = true;
                }

                if (pmCase.ConfinementType.Equals(ConfinementType.ClosedTies) == true)
                {
                    radioButton_ClosedHoops.Checked = true;
                }
                else
                {
                    radioButton_Spirals.Checked = true;
                }

                if (pmCase.Loads != null && pmCase.Loads.Count > 0)
                {
                    label_LoadingDefined.Text = $"There are {pmCase.Loads.Count} load case(s) defined";
                }

                if (pmCase.LimitStates != null && pmCase.LimitStates.Count > 0)
                {
                    label_LimitStates.Text = $"There are {pmCase.LimitStates.Count} limit state(s) defined";
                }

                checkBox_IncludePT.Checked = pmCase.IncludeInitialPostTensioningForceInAxialLoad;
            }

            _suspendAutoPopulation = false;
        }

        private AxialForceMomentAnalysisCase GetAnalysisCaseFromName(string name)
        {
            if (_pmCases != null)
            {
                for (int i = 0; i < _pmCases.Count; i++)
                {
                    if (string.Compare(_pmCases[i].Name, name, ignoreCase: true) == 0)
                    {
                        return _pmCases[i];
                    }
                }
            }

            return null;
        }
        #endregion

        private void RadioButton_RunSingle_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton thisRadioButton = (RadioButton)sender;

            if (thisRadioButton.Checked == true)
            {
                textBox_SingleAngle.Enabled = true;

                textBox_AngleInterval.Enabled = false;

                _selectedAnalysisCase.AnalysisType = AxialForceMomentAnalysisType.SingleAngle;
            }
        }

        private void RadioButton_RunSuite_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton thisRadioButton = (RadioButton)sender;

            if (thisRadioButton.Checked == true)
            {
                textBox_SingleAngle.Enabled = false;

                textBox_AngleInterval.Enabled = true;

                _selectedAnalysisCase.AnalysisType = AxialForceMomentAnalysisType.Suite;
            }
        }

        private void ComboBox_AnalysisCases_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                if (_pmCases != null && _pmCases.Count > 0)
                {
                    ComboBox cB = (ComboBox)sender;

                    if (cB.SelectedIndex >= 0)
                    {
                        _selectedAnalysisCase = GetAnalysisCaseFromName(cB.Text);

                        PopulateAnalysisCaseInformation(cB.Text);

                        if (_selectedAnalysisCase.Loads == null)
                        {
                            label_LoadingDefined.Text = _defaultLoadingLabelString;
                        }
                        else if (_selectedAnalysisCase.Loads.Count <= 0)
                        {
                            label_LoadingDefined.Text = _defaultLoadingLabelString;
                        }
                        else
                        {
                            label_LoadingDefined.Text = $"There are {_selectedAnalysisCase.Loads.Count} load cases defined";
                        }

                        if (_selectedAnalysisCase.LimitStates == null)
                        {
                            label_LimitStates.Text = _defaultLimitStateLabelString;
                        }
                        else if (_selectedAnalysisCase.LimitStates.Count <= 0)
                        {
                            label_LimitStates.Text = _defaultLimitStateLabelString;
                        }
                        else
                        {
                            label_LimitStates.Text = $"There are {_selectedAnalysisCase.LimitStates.Count} limit states selected";
                        }

                        EnableAllBoxes();
                    }
                    else
                    {
                        _selectedAnalysisCase = null;

                        DisableAllBoxes();
                    }
                }
                else
                {
                    _selectedAnalysisCase = null;

                    DisableAllBoxes();
                }
            }
        }

        private void DisableAllBoxes()
        {
            groupBox_AnalysisOptions.Enabled = false;
            groupBox_DesignOptions.Enabled = false;
            groupBox_Loading.Enabled = false;
            button_Analyze.Enabled = false;
            groupBox_LimitStates.Enabled = false;
        }

        private void EnableAllBoxes()
        {
            groupBox_AnalysisOptions.Enabled = true;
            groupBox_DesignOptions.Enabled = true;
            groupBox_Loading.Enabled = true;
            button_Analyze.Enabled = true;
            groupBox_LimitStates.Enabled = true;
        }

        private void Button_NewAnalysis_Click(object sender, EventArgs e)
        {
            AddFiberSectionAnalysisCase frm = new AddFiberSectionAnalysisCase(_pmCases.Cast< GeneralAnalysisCase>().ToList());

            if (frm.ShowDialog() == DialogResult.OK)
            {
                AxialForceMomentAnalysisCase newCase = new AxialForceMomentAnalysisCase() { Name = frm.NewName };

                if (_pmCases == null)
                {
                    _pmCases = new List<AxialForceMomentAnalysisCase>();
                }

                _pmCases.Add(newCase);

                _suspendAutoPopulation = true;

                comboBox_AnalysisCases.Items.Add(newCase.Name);

                comboBox_AnalysisCases.SelectedIndex = comboBox_AnalysisCases.Items.Count - 1;

                _selectedAnalysisCase = newCase;

                ClearAllInformation();

                _suspendAutoPopulation = false;

                EnableAllBoxes();
            }

            frm.Dispose();
        }

        private void ClearAllInformation()
        {
            textBox_MeshSize.Text = string.Empty;
            textBox_NumberOfPoints.Text = string.Empty;
            textBox_SingleAngle.Text = string.Empty;
            textBox_AngleInterval.Text = string.Empty;

            radioButton_ClosedHoops.Checked = true;

            radioButton_RunSingle.Checked = true;

            textBox_AngleInterval.Enabled = false;

            textBox_SingleAngle.Enabled = true;

            label_LoadingDefined.Text = _defaultLoadingLabelString;

            label_LimitStates.Text = _defaultLimitStateLabelString;

            checkBox_IncludePT.Checked = false;
        }

        private void RadioButton_ClosedHoops_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton thisRadioButton = (RadioButton)sender;

            if (thisRadioButton.Checked == true)
            {
                _selectedAnalysisCase.ConfinementType = ConfinementType.ClosedTies;
            }
        }

        private void RadioButton_Spirals_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton thisRadioButton = (RadioButton)sender;

            if (thisRadioButton.Checked == true)
            {
                _selectedAnalysisCase.ConfinementType = ConfinementType.Spiral;
            }
        }

        private void TextBox_MeshSize_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double meshSize) == true)
                {
                    if (meshSize > 0)
                    {
                        _selectedAnalysisCase.MeshSize = meshSize;
                    }
                }
            }
            
        }

        private void TextBox_NumberOfPoints_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (int.TryParse(tB.Text, out int numPoints) == true)
                {
                    if (numPoints > 0)
                    {
                        _selectedAnalysisCase.NumberOfPoints = numPoints;
                    }
                }
            }
            
        }

        private void TextBox_SingleAngle_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double angle) == true)
                {
                    if (angle >= 0 && angle <= 360)
                    {
                        _selectedAnalysisCase.SingleAngle = angle;
                    }
                }
            }
            
        }

        private void TextBox_AngleInterval_TextChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                TextBox tB = (TextBox)sender;

                if (double.TryParse(tB.Text, out double angle) == true)
                {
                    if (angle >= 0 && angle <= 360)
                    {
                        _selectedAnalysisCase.AngleInterval = angle;
                    }
                }
            }
            
        }

        private void Button_DefineLoading_Click(object sender, EventArgs e)
        {
            DefineCrossSectionLoadingForm frm = new DefineCrossSectionLoadingForm(_selectedAnalysisCase.Loads);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _selectedAnalysisCase.Loads = frm.Loads;

                if (_selectedAnalysisCase.Loads == null)
                {
                    label_LoadingDefined.Text = _defaultLoadingLabelString;
                }
                else if (_selectedAnalysisCase.Loads.Count <= 0)
                {
                    label_LoadingDefined.Text = _defaultLoadingLabelString;
                }
                else
                {
                    label_LoadingDefined.Text = $"There are {_selectedAnalysisCase.Loads.Count} load cases defined";
                }
            }

            frm.Dispose();
        }

        private void CheckBox_IncludePT_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            _selectedAnalysisCase.IncludeInitialPostTensioningForceInAxialLoad = cB.Checked;
        }

        private bool ValidateInputs(out string message)
        {
            message = string.Empty;

            if (double.TryParse(textBox_MeshSize.Text,out double meshSize) == false)
            {
                message = "Mesh size is not numeric";

                return false;
            }
            else
            {
                if (meshSize <= 0)
                {
                    message = "Mesh size must be greater than 0";

                    return false;
                }
            }

            if (int.TryParse(textBox_NumberOfPoints.Text, out int numPoints) == false)
            {
                message = "Number of points is not numeric";

                return false;
            }
            else
            {
                if (numPoints <= 0)
                {
                    message = "Number of points must be greater than 0";

                    return false;
                }
            }

            if (radioButton_RunSingle.Checked == true)
            {
                if (double.TryParse(textBox_SingleAngle.Text, out double singleAngle) == false)
                {
                    message = "The angle to analyze must be numeric";

                    return false;
                }
                else
                {
                    if (singleAngle < 0 || singleAngle > 360)
                    {
                        message = "The angle to analyze must be greater than or equal to 0 deg and less than or equal to 360 deg";

                        return false;
                    }
                }
            }

            if (radioButton_RunSuite.Checked == true)
            {
                if (double.TryParse(textBox_AngleInterval.Text, out double angleInterval) == false)
                {
                    message = "The angle to analyze must be numeric";

                    return false;
                }
                else
                {
                    if (angleInterval < 0 || angleInterval > 360)
                    {
                        message = "The angle interval must be greater than or equal to 0 deg and less than or equal to 360 deg";

                        return false;
                    }
                }
            }

            return true;
        }

        private AxialForceMomentSuite CreateAnalysisSuite(double meshSize)
        {
            bool containsClosedShapes = false;

            for (int i = 0; i < _selectedElements.Count; i++)
            {
                if (_selectedElements[i].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                {
                    containsClosedShapes = true;

                    break;
                }
            }

            if (containsClosedShapes == true)
            {
                //List<PrePolygon> preMesh = CommonMethods.CreateTriangleNetMesh(_selectedElements, meshSize, _precision);

                //List<GenericPolygon> polygons = Triangle.TriangleHelperMethods.ConvertFromPrePolgyonsToGenericPolygons(preMesh);

                //List<FiberSectionMeshElement> meshElements = GeometryHelpers.ConvertGenericPolygonsToMeshElements(polygons);

                //List<FiberSectionRebarElement> rebarElements = GetFiberRebarElements();

                //List<FiberSectionPostTensioningElement> postTensioningElements = GetFiberPostTensioningElements();

                //FiberSection sectionModel = new FiberSection()
                //{
                //    FiberMeshElements = meshElements,
                //    FiberRebarElements = rebarElements,
                //    FiberPostTensioningElements = postTensioningElements,
                //    Confinement = _selectedAnalysisCase.ConfinementType
                //};

                //sectionModel.BuildMeshQuadTree();

                //AssignMaterialsToFiberElements(sectionModel);

                FiberSection sectionModel = FiberSection.BuildFiberSectionFromListOfElements(ConvertSelectedElementsToFiberSectionShapes(_selectedElements), meshSize, _precision);

                sectionModel.Confinement = _selectedAnalysisCase.ConfinementType;

                List<double> anglesToAnalyzeRadians = new List<double>();

                if (_selectedAnalysisCase.AnalysisType.Equals(AxialForceMomentAnalysisType.SingleAngle) == true)
                {
                    anglesToAnalyzeRadians.Add(_selectedAnalysisCase.SingleAngle.ToRadians());
                }
                else
                {
                    for (double i = 0; i < 360; i += _selectedAnalysisCase.AngleInterval)
                    {
                        anglesToAnalyzeRadians.Add(i.ToRadians());
                    }
                }

                Dictionary<IGenericMaterial, List<LimitState>> limitStateDictionary = new Dictionary<IGenericMaterial, List<LimitState>>();

                for (int i = 0; i < _selectedAnalysisCase.LimitStates.Count; i++)
                {
                    LimitState ls = _selectedAnalysisCase.LimitStates[i];

                    if (limitStateDictionary.TryGetValue(ls.Material, out List<LimitState> lsValues) == true)
                    {
                        lsValues.Add(ls);
                    }
                    else
                    {
                        List<LimitState> lsList = new List<LimitState>();

                        lsList.Add(ls);

                        limitStateDictionary.Add(ls.Material, lsList);
                    }
                }

                AxialForceMomentSuite analysisSuite = new AxialForceMomentSuite(anglesToAnalyzeRadians)
                {
                    IncludeInitialPostTensioningForceInAxialLoad = _selectedAnalysisCase.IncludeInitialPostTensioningForceInAxialLoad,
                    NumberOfPoints = _selectedAnalysisCase.NumberOfPoints,
                    SectionModel = sectionModel,
                    MaterialLimitStates = limitStateDictionary
                };

                return analysisSuite;
            }

            return null;
        }

        private void Button_DefineLimitStates_Click(object sender, EventArgs e)
        {
            SelectLimitStatesForm frm = new SelectLimitStatesForm(_allLimitStates, _selectedAnalysisCase.LimitStates, _allMaterials);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _selectedAnalysisCase.LimitStates = frm.SelectedLimitStates;
            }

            if (_selectedAnalysisCase.LimitStates == null)
            {
                label_LimitStates.Text = _defaultLimitStateLabelString;
            }
            else if (_selectedAnalysisCase.LimitStates.Count <= 0)
            {
                label_LimitStates.Text = _defaultLimitStateLabelString;
            }
            else
            {
                label_LimitStates.Text = $"There are {_selectedAnalysisCase.LimitStates.Count} limit states selected";
            }

            frm.Dispose();
        }

        private void Button_Analyze_Click(object sender, EventArgs e)
        {
            //try
            //{
                if (ValidateInputs(out string validationMessage) == true)
                {
                    AxialForceMomentSuite analysisSuite = CreateAnalysisSuite(_selectedAnalysisCase.MeshSize);

                    analysisSuite.AnalyzeSuite();

                    _selectedAnalysisCase.AnalysisSuite = analysisSuite;

                    _selectedAnalysisCase.PopulateMomentMomentDiagrams();

                    PopulateResultsAfterAnalysis();

                }
                else
                {
                    throw new Exception(validationMessage);
                }
            //}
            //catch(Exception ex)
            //{
            //    MessageBox.Show("An analysis suite could not be created" + Environment.NewLine + Environment.NewLine + ex.Message, "Error", MessageBoxButtons.OK);
            //}
        }

        private void PopulateResultsAfterAnalysis()
        {
            _suspendAutoPopulation = true;

            radioButton_PM.Checked = true;

            radioButton_Major.Checked = true;

            _suspendAutoPopulation = false;

            PopulateAnalysisAngleComboBox();

            PopulateLoadCaseComboBox();

            dataGridView_PM.Show();
        }

        private void PopulateAxialForceMomentDiagram(int index, bool applyPhiFactor, SectionAxis axis)
        {
            AxialForceMoment pm = _selectedAnalysisCase.AnalysisSuite.Analyses[index];

            string axisType = axis.GetDescription();

            _chart = new Chart2D($"Axial Force Moment Diagram - {axisType}");

            _chart.HorizontalAxisLabel = "Moment (kip-in)";

            _chart.VerticalAxisLabel = "Axial Force (kips)";

            List<PointD> xyData = new List<PointD>();

            for (int i = 0; i < pm.AnalysisResults.Count; i++)
            {
                double force = (applyPhiFactor == true) ? pm.AnalysisResults[i].ReducedAxialForce : pm.AnalysisResults[i].AxialForce;

                double moment = 0;

                switch (axis)
                {
                    case SectionAxis.Major:
                        moment = (applyPhiFactor == true) ? pm.AnalysisResults[i].ReducedMomentMajor : pm.AnalysisResults[i].MomentMajor;
                        break;
                    case SectionAxis.Minor:
                        moment = (applyPhiFactor == true) ? pm.AnalysisResults[i].ReducedMomentMinor : pm.AnalysisResults[i].MomentMinor;
                        break;
                    case SectionAxis.X:
                        moment = (applyPhiFactor == true) ? pm.AnalysisResults[i].ReducedMomentX : pm.AnalysisResults[i].MomentX;
                        break;
                    case SectionAxis.Y:
                        moment = (applyPhiFactor == true) ? pm.AnalysisResults[i].ReducedMomentY : pm.AnalysisResults[i].MomentY;
                        break;
                    default:
                        moment = 0;
                        break;
                }

                PointD p = new PointD(moment, force);

                xyData.Add(p);
            }

            double xMax = xyData.Select(p => p.X).Max(x => x);
            double xMin = xyData.Select(p => p.X).Min(x => x);

            double yMax = xyData.Select(p => p.Y).Max(x => x);
            double yMin = xyData.Select(p => p.Y).Min(x => x);

            _chart.SetHorizontalAxisBounds(xMin, xMax);
            _chart.SetVerticalAxisBounds(yMin, yMax);

            _chart.AddSeriesFromXY(xyData, "PM");

            _chart.SetInterpolation(InterpolationType.None);

            _plotView.Model = _chart.Model;

            _plotView.Invalidate();

            //string chartSVG = _chart.ExportToSVG(_chartWidth, _chartHeight);

            //_chartBrowser.DocumentText = chartSVG;
        }

        private void PopulatePhiFactorDiagram(int index)
        {
            AxialForceMoment pm = _selectedAnalysisCase.AnalysisSuite.Analyses[index];

            _chart = new Chart2D($"Phi Factor Diagram");

            _chart.HorizontalAxisLabel = "Curvature (rad-in)";

            _chart.VerticalAxisLabel = "Phi";

            List<PointD> xyData = new List<PointD>();

            for (int i = 0; i < pm.AnalysisResults.Count; i++)
            {
                double phiFactor = pm.AnalysisResults[i].PhiFactor;

                double curvature = pm.AnalysisResults[i].Curvature;

                PointD p = new PointD(curvature, phiFactor);

                xyData.Add(p);
            }

            double xMax = xyData.Select(p => p.X).Max(x => x);
            double xMin = xyData.Select(p => p.X).Min(x => x);

            double yMax = xyData.Select(p => p.Y).Max(x => x);
            double yMin = xyData.Select(p => p.Y).Min(x => x);

            _chart.SetHorizontalAxisBounds(xMin, xMax);
            _chart.SetVerticalAxisBounds(yMin, yMax);

            _chart.AddSeriesFromXY(xyData, "Phi");

            _chart.SetInterpolation(InterpolationType.None);

            string chartSVG = _chart.ExportToSVG(_chartWidth, _chartHeight);


            _chartBrowser.DocumentText = chartSVG;
        }

        private void PopulateAnalysisAngleComboBox()
        {
            comboBox_Angle.Items.Clear();

            if (_selectedAnalysisCase.AnalysisSuite != null)
            {
                if (_selectedAnalysisCase.AnalysisSuite.Analyses != null && _selectedAnalysisCase.AnalysisSuite.Analyses.Count > 0)
                {
                    for (int i = 0; i < _selectedAnalysisCase.AnalysisSuite.Analyses.Count; i++)
                    {
                        AxialForceMoment pm = _selectedAnalysisCase.AnalysisSuite.Analyses[i];

                        double angleInDegrees = pm.AnalysisAngle.ToDegrees();

                        comboBox_Angle.Items.Add($"Angle = {Round(angleInDegrees, 0)} deg");
                    }

                    comboBox_Angle.SelectedIndex = 0;
                }
            }
        }

        private void PopulateLoadCaseComboBox()
        {
            comboBox_Loads.Items.Clear();

            if (_selectedAnalysisCase.Loads != null && _selectedAnalysisCase.Loads.Count > 0)
            {
                for (int i = 0; i < _selectedAnalysisCase.Loads.Count; i++)
                {
                    comboBox_Loads.Items.Add(_selectedAnalysisCase.Loads[i].LoadCase);
                }

                comboBox_Loads.SelectedIndex = 0;
            }
        }

        private void PopulateDataGridView_PM(int index)
        {
            if (index >= 0)
            {
                if (_suspendAutoPopulation == false)
                {
                    dataGridView_PM.Rows.Clear();

                    AxialForceMoment analysis = _selectedAnalysisCase.AnalysisSuite.Analyses[index];

                    for (int i = 0; i < analysis.AnalysisResults.Count; i++)
                    {
                        dataGridView_PM.Rows.Add();

                        dataGridView_PM[0, i].Value = Round(analysis.AnalysisResults[i].Curvature,6);
                        dataGridView_PM[1, i].Value = Round(analysis.AnalysisResults[i].AxialForce,2);
                        dataGridView_PM[2, i].Value = Round(analysis.AnalysisResults[i].MomentMajor,2);
                        dataGridView_PM[3, i].Value = Round(analysis.AnalysisResults[i].MomentMinor, 2);
                        dataGridView_PM[4, i].Value = Round(analysis.AnalysisResults[i].PhiFactor, 3);
                        dataGridView_PM[5, i].Value = Round(analysis.AnalysisResults[i].ReducedAxialForce, 2);
                        dataGridView_PM[6, i].Value = Round(analysis.AnalysisResults[i].ReducedMomentMajor, 2);
                        dataGridView_PM[7, i].Value = Round(analysis.AnalysisResults[i].ReducedMomentMinor, 2);
                        dataGridView_PM[8, i].Value = analysis.AnalysisResults[i].LimitState;
                    }
                }
            }
        }

        private List<ClosedDrawingElement> GetAllClosedELementsSelected()
        {
            List<ClosedDrawingElement> allClosedElements = new List<ClosedDrawingElement>();

            for (int i = 0; i < _selectedElements.Count; i++)
            {
                IDrawingElement elem = _selectedElements[i];

                if (elem.GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                {
                    if (elem.GetType().Equals(typeof(RebarSectionG)) == true || elem.GetType().Equals(typeof(PostTensioningG)) == true)
                    {
                        //Don't add rebar or pt sections
                        continue;
                    }

                    ClosedDrawingElement closedElem = (ClosedDrawingElement)elem;

                    if (closedElem.IsRebar == false)
                    {
                        //Only add elements that are not plan rebar
                        if (closedElem.Material != null)
                        {
                            //Only add elements that have a material assigned
                            if (closedElem.Hole == false)
                            {
                                allClosedElements.Add(closedElem);
                            }
                        }
                    }
                }
            }

            return allClosedElements;
        }

        private void SplitCrossingClosedShapes(ref List<ClosedDrawingElement> allClosedElements)
        {
            int ind = 0;

            while (ind < allClosedElements.Count - 1)
            {
                ClosedDrawingElement elem1 = allClosedElements[ind];

                PolygonG poly1 = null;

                if (elem1.GetType().Equals(typeof(PolygonG)) == true || elem1.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                {
                    poly1 = (PolygonG)elem1;
                }
                else
                {
                    poly1 = (PolygonG)elem1.GetDiscretizedPoly(_selectedAnalysisCase.MeshSize);
                }

                if (poly1 != null)
                {
                    int innerInd = ind + 1;

                    int startCount = allClosedElements.Count;

                    while (innerInd < startCount)
                    {
                        ClosedDrawingElement elem2 = allClosedElements[innerInd];

                        PolygonG poly2 = null;

                        if (elem2.GetType().Equals(typeof(PolygonG)) == true || elem2.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                        {
                            poly2 = (PolygonG)elem2;
                        }
                        else
                        {
                            poly2 = (PolygonG)elem2.GetDiscretizedPoly(_selectedAnalysisCase.MeshSize);
                        }

                        if (poly2 != null)
                        {
                            if (poly1.IsFullyWithin(poly2) || poly2.IsFullyWithin(poly1))
                            {
                                //do nothing

                            }
                            else
                            {
                                List<PolygonG> intersectedPolygons = PolyBoolHelperMethods.BooleanIntersection(poly1, poly2);

                                if (intersectedPolygons != null && intersectedPolygons.Count > 0)
                                {
                                    for (int i = 0; i < intersectedPolygons.Count; i++)
                                    {
                                        intersectedPolygons[i].Material = elem2.Material;
                                    }

                                    List<PolygonG> differencedPolygons = PolyBoolHelperMethods.BooleanDifference(poly2, poly1);

                                    if (differencedPolygons != null && differencedPolygons.Count > 0)
                                    {
                                        for (int i = 0; i < differencedPolygons.Count; i++)
                                        {
                                            differencedPolygons[i].Material = elem2.Material;
                                        }

                                        allClosedElements.RemoveAt(innerInd);

                                        allClosedElements.InsertRange(innerInd, differencedPolygons);

                                        innerInd += differencedPolygons.Count - 1;

                                        startCount += differencedPolygons.Count - 1;

                                        allClosedElements.InsertRange(innerInd + 1, intersectedPolygons);

                                        innerInd += intersectedPolygons.Count;

                                        startCount += intersectedPolygons.Count;
                                    }
                                }
                            }
                        }

                        innerInd++;
                    }
                }
                ind++;
            }
        }

        private Dictionary<ClosedDrawingElement, List<ClosedDrawingElement>> GetObjectHeirarchies(List<ClosedDrawingElement> allClosedELements)
        {
            Dictionary<ClosedDrawingElement, List<ClosedDrawingElement>> objectHeirarchy = new Dictionary<ClosedDrawingElement, List<ClosedDrawingElement>>();

            for (int i = 0; i < allClosedELements.Count; i++)
            {
                ClosedDrawingElement elem1 = allClosedELements[i];

                List<ClosedDrawingElement> elementsWithin = new List<ClosedDrawingElement>();

                for (int j = 0; j < allClosedELements.Count; j++)
                {
                    if (j == i)
                    {
                        continue;
                    }

                    ClosedDrawingElement elem2 = allClosedELements[j];

                    if (elem2.IsFullyWithin(elem1) == true)
                    {
                        elementsWithin.Add(elem2);
                    }
                }

                objectHeirarchy.Add(elem1, elementsWithin);
            }

            return objectHeirarchy;
        }

        public int GetNumberOfElementsThatContainMe(Dictionary<ClosedDrawingElement, List<ClosedDrawingElement>> objectHeirarchy, List<ClosedDrawingElement> filterList, ClosedDrawingElement elem)
        {
            int count = 0;

            foreach(KeyValuePair<ClosedDrawingElement, List<ClosedDrawingElement>> kvp in objectHeirarchy)
            {
                if (filterList.Contains(kvp.Key) == true)
                {
                    if (kvp.Value.Contains(elem) == true)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private void ComboBox_Angle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                ComboBox cB = (ComboBox)sender;

                if (cB.SelectedIndex >= 0)
                {
                    if (radioButton_PM.Checked == true)
                    {
                        PopulateAxialForceMomentDiagram(cB.SelectedIndex, _applyPhiFactor, _axisType);

                        PopulateDataGridView_PM(cB.SelectedIndex);
                    }
                }
            }
        }

        private void ComboBox_Loads_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_suspendAutoPopulation == false)
            {
                ComboBox cB = (ComboBox)sender;

                if (cB.SelectedIndex >= 0)
                {
                    CrossSectionLoad load = _selectedAnalysisCase.Loads[cB.SelectedIndex];

                    if (radioButton_MM.Checked == true)
                    {
                        PopulateMomentMomentDiagram(cB.SelectedIndex, _applyPhiFactor);

                        label_LoadP.Text = $"P = {Round(load.P, 2)} kips";
                        label_LoadMx.Text = $"Mx = {Round(load.Mx, 2)} kip-in";
                        label_LoadMy.Text = $"My = {Round(load.My, 2)} kip-in";
                    }
                }
            }
        }

        private void PopulateMomentMomentDiagram(int index, bool applyReductionFactors)
        {
            CrossSectionLoad load = _selectedAnalysisCase.Loads[index];

            List<PointD> mmDiagram = null;

            if (applyReductionFactors == true)
            {
                if (_selectedAnalysisCase.ReducedMomentMomentDiagramsGlobal != null)
                {
                    if (_selectedAnalysisCase.ReducedMomentMomentDiagramsGlobal.TryGetValue(load, out mmDiagram) == false)
                    {
                        return;
                    }
                }
            }
            else
            {
                if (_selectedAnalysisCase.MomentMomentDiagramsGlobal != null)
                {
                    if (_selectedAnalysisCase.MomentMomentDiagramsGlobal.TryGetValue(load, out mmDiagram) == false)
                    {
                        return;
                    }
                }
            }

            _chart = new Chart2D($"Moment Moment Diagram - {load.LoadCase}");

            _chart.HorizontalAxisLabel = "Moment about X (kip-in)";

            _chart.VerticalAxisLabel = "Moment about Y (kips)";

            if (mmDiagram != null)
            {
                List<PointD> xyData = mmDiagram;

                double xMax = xyData.Select(p => p.X).Max(x => x);
                double xMin = xyData.Select(p => p.X).Min(x => x);

                double yMax = xyData.Select(p => p.Y).Max(x => x);
                double yMin = xyData.Select(p => p.Y).Min(x => x);

                _chart.AddSeriesFromXY(xyData, "MM");

                _chart.AddSeriesFromXY(new List<PointD>() { new PointD(load.Mx, load.My) }, "Load", Color.Red, OxyPlot.LineStyle.Automatic, OxyPlot.MarkerType.Diamond);

                xMin = xMin - Math.Abs(0.1 * xMin);
                xMax = xMax + Math.Abs(0.1 * xMax);
                yMin = yMin - Math.Abs(0.1 * yMin);
                yMax = yMax + Math.Abs(0.1 * yMax);

                _chart.SetHorizontalAxisBounds(xMin, xMax);
                _chart.SetVerticalAxisBounds(yMin, yMax);

                _chart.SetInterpolation(InterpolationType.None);
            }

            _plotView.Model = _chart.Model;

            _plotView.Invalidate();

            //string chartSVG = _chart.ExportToSVG(Convert.ToInt32(_chartWidth), Convert.ToInt32(_chartHeight));

            #region Modify SVG
            //string[] delim = { Environment.NewLine, "\n" }; // "\n" added in case you manually appended a newline
            //string[] lines = chartSVG.Split(delim, StringSplitOptions.None);

            //List<string> linesToAdd = new List<string>();

            //for (int i = 0; i <= lines.GetUpperBound(0); i++)
            //{
            //    if (string.Compare(lines[i].Left(5), "<?xml",ignoreCase:true) != 0 && string.Compare(lines[i].Left(9), "<!DOCTYPE", ignoreCase:true) != 0)
            //    {
            //        linesToAdd.Add(lines[i]);
            //    }
            //}


            //StringBuilder sB = new StringBuilder();

            //sB.Append("<!DOCTYPE HTML>");
            //sB.AppendLine("<html>");
            //sB.AppendLine("<head>");
            //sB.AppendLine("<meta http-equiv=\"x - ua - compatible\" content=\"IE = 11\">");
            //sB.AppendLine("<meta http-equiv=\"Content - Type\" content=\"text / html; charset = utf - 8\">");
            //sB.AppendLine("<title>MM Chart</title>");
            //sB.AppendLine("<style>");
            //sB.AppendLine("</style>");
            //sB.AppendLine("</head>");
            //sB.AppendLine("<body>");
            //sB.AppendLine("<div>");

            //for (int i = 0; i < linesToAdd.Count; i++)
            //{
            //    sB.AppendLine(linesToAdd[i]);
            //}
            ////sB.AppendLine(chartSVG);
            //sB.AppendLine("</div>");
            //sB.AppendLine("</body>");
            //sB.AppendLine("</html>");

            //string s = sB.ToString();

            #endregion



            //_chartBrowser.DocumentText = chartSVG;
        }

        private void CheckBox_ReductionFactors_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            _applyPhiFactor = cB.Checked;

            if (_suspendAutoPopulation == false)
            {
                if (radioButton_PM.Checked == true)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);
                    }
                }
                else if (radioButton_MM.Checked == true)
                {
                    if (comboBox_Loads.SelectedIndex >= 0)
                    {
                        PopulateMomentMomentDiagram(comboBox_Loads.SelectedIndex, _applyPhiFactor);
                    }
                }
            }
        }

        private void RadioButton_Major_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                _axisType = SectionAxis.Major;
            }

            if (_suspendAutoPopulation == false)
            {
                if (radioButton_PM.Checked == true)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);
                    }
                }
            }
        }

        private void RadioButton_Minor_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                _axisType = SectionAxis.Minor;
            }

            if (_suspendAutoPopulation == false)
            {
                if (radioButton_PM.Checked == true)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);
                    }
                }
            }
        }

        private void RadioButton_X_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                _axisType = SectionAxis.X;
            }

            if (_suspendAutoPopulation == false)
            {
                if (radioButton_PM.Checked == true)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);
                    }
                }
            }
        }

        private void RadioButton_Y_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                _axisType = SectionAxis.Y;
            }

            if (_suspendAutoPopulation == false)
            {
                if (radioButton_PM.Checked == true)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);
                    }
                }
            }
        }

        private void RadioButton_PhiFactors_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                if (_suspendAutoPopulation == false)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulatePhiFactorDiagram(comboBox_Angle.SelectedIndex);
                    }
                }
            }
        }

        private void RadioButton_PM_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                groupBox_PMPlot.Show();

                if (_suspendAutoPopulation == false)
                {
                    if (comboBox_Angle.SelectedIndex >= 0)
                    {
                        PopulateAxialForceMomentDiagram(comboBox_Angle.SelectedIndex, _applyPhiFactor, _axisType);

                        PopulateDataGridView_PM(comboBox_Angle.SelectedIndex);

                        dataGridView_PM.Show();
                    }
                }
            }
            else
            {
                groupBox_PMPlot.Hide();
            }
        }

        private void RadioButton_MM_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rB = (RadioButton)sender;

            if (rB.Checked == true)
            {
                groupBox_MMPlot.Show();

                if (_suspendAutoPopulation == false)
                {
                    dataGridView_PM.Hide();

                    if (comboBox_Loads.SelectedIndex >= 0)
                    {
                        PopulateMomentMomentDiagram(comboBox_Loads.SelectedIndex, _applyPhiFactor);

                        CrossSectionLoad load = _selectedAnalysisCase.Loads[comboBox_Loads.SelectedIndex];

                        label_LoadP.Text = $"P = {Round(load.P, 2)} kips";
                        label_LoadMx.Text = $"Mx = {Round(load.Mx, 2)} kip-in";
                        label_LoadMy.Text = $"My = {Round(load.My, 2)} kip-in";
                    }
                }
            }
            else
            {
                groupBox_MMPlot.Hide();
            }
        }

        #region Drawing Suspension
        public static void SuspendDrawing(Control parent)
        {
            SendMessage(parent.Handle, WM_SETREDRAW, false, 0);
        }

        public static void ResumeDrawing(Control parent)
        {
            SendMessage(parent.Handle, WM_SETREDRAW, true, 0);
            parent.Refresh();
        }


        #endregion

        private void button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private List<GenericShape> ConvertSelectedElementsToFiberSectionShapes(List<IDrawingElement> selectedElements)
        {
            List<GenericShape> fiberSectionShapes = new List<GenericShape>();

            if (selectedElements != null)
            {
                for (int i = 0; i < selectedElements.Count; i++)
                {
                    GenericShape shape = selectedElements[i].ConvertToFiberSectionElement();

                    if (shape != null)
                    {
                        fiberSectionShapes.Add(shape);
                    }

                }
            }

            return fiberSectionShapes;
        }

        #region Obsolete
        private void AssignMaterialsToFiberElements(FiberSection sectionModel)
        {
            List<ClosedDrawingElement> allClosedElements = GetAllClosedELementsSelected();

            SplitCrossingClosedShapes(ref allClosedElements);

            Dictionary<ClosedDrawingElement, List<ClosedDrawingElement>> objectHeirarchy = GetObjectHeirarchies(allClosedElements);

            List<ClosedDrawingElement> elementsAlreadyAssigned = new List<ClosedDrawingElement>();

            List<ClosedDrawingElement> remainingElements = new List<ClosedDrawingElement>(allClosedElements);

            while (remainingElements.Count > 0)
            {
                List<ClosedDrawingElement> elementsNotContainedByOthers = new List<ClosedDrawingElement>();

                for (int i = 0; i < remainingElements.Count; i++)
                {
                    if (GetNumberOfElementsThatContainMe(objectHeirarchy, remainingElements, remainingElements[i]) == 0)
                    {
                        elementsNotContainedByOthers.Add(remainingElements[i]);
                    }
                }

                for (int i = 0; i < elementsNotContainedByOthers.Count; i++)
                {
                    //Get potential elements from quad tree
                    List<FiberSectionMeshElement> nearbyElements = sectionModel.MeshQuadTree.GetNodesInside(elementsNotContainedByOthers[i].BoundingRectangle()).ToList();

                    for (int j = 0; j < nearbyElements.Count; j++)
                    {
                        FiberSectionMeshElement meshElem = nearbyElements[j];

                        if (elementsNotContainedByOthers[i].ContainsPoint(meshElem.Centroid()) == true)
                        {
                            meshElem.Material = elementsNotContainedByOthers[i].Material;
                        }
                    }

                    remainingElements.Remove(elementsNotContainedByOthers[i]);
                }
            }

            if (sectionModel.FiberRebarElements != null && sectionModel.FiberRebarElements.Count > 0)
            {
                for (int i = 0; i < sectionModel.FiberRebarElements.Count; i++)
                {
                    FiberSectionRebarElement bar = sectionModel.FiberRebarElements[i];

                    List<FiberSectionMeshElement> nearbyElements = sectionModel.MeshQuadTree.GetNodesInside(bar.Center.BoundingRectangle()).ToList();

                    if (nearbyElements != null)
                    {
                        for (int j = 0; j < nearbyElements.Count; j++)
                        {
                            if (nearbyElements[j].ContainsPoint(bar.Center) == true)
                            {
                                bar.MaterialRemoved = nearbyElements[j].Material;

                                break;
                            }
                        }
                    }

                }
            }

            if (sectionModel.FiberPostTensioningElements != null && sectionModel.FiberPostTensioningElements.Count > 0)
            {
                for (int i = 0; i < sectionModel.FiberPostTensioningElements.Count; i++)
                {
                    FiberSectionPostTensioningElement pt = sectionModel.FiberPostTensioningElements[i];

                    List<FiberSectionMeshElement> nearbyElements = sectionModel.MeshQuadTree.GetNodesInside(pt.Center.BoundingRectangle()).ToList();

                    if (nearbyElements != null)
                    {
                        for (int j = 0; j < nearbyElements.Count; j++)
                        {
                            if (nearbyElements[j].ContainsPoint(pt.Center) == true)
                            {
                                pt.MaterialRemoved = nearbyElements[j].Material;

                                break;
                            }
                        }
                    }

                }
            }
        }

        private List<FiberSectionRebarElement> GetFiberRebarElements()
        {
            List<FiberSectionRebarElement> rebarElements = new List<FiberSectionRebarElement>();

            for (int i = 0; i < _selectedElements.Count; i++)
            {
                IDrawingElement element = _selectedElements[i];

                if (element.GetType().Equals(typeof(RebarSectionG)) == true)
                {
                    RebarSectionG rebarSection = (RebarSectionG)element;

                    IGenericMaterial inelMat = rebarSection.Material;

                    if (inelMat != null)
                    {
                        FiberSectionRebarElement rebarElement = new FiberSectionRebarElement(rebarSection.Centroid(), rebarSection.Rebar, element.Material);

                        rebarElements.Add(rebarElement);
                    }
                }
            }

            return rebarElements;
        }

        private List<FiberSectionPostTensioningElement> GetFiberPostTensioningElements()
        {
            List<FiberSectionPostTensioningElement> ptElements = new List<FiberSectionPostTensioningElement>();

            for (int i = 0; i < _selectedElements.Count; i++)
            {
                IDrawingElement element = _selectedElements[i];

                if (element.GetType().Equals(typeof(PostTensioningG)) == true)
                {
                    PostTensioningG ptSection = (PostTensioningG)element;

                    IGenericMaterial inelMat = ptSection.TypeOfPT.Material;

                    if (inelMat != null)
                    {
                        FiberSectionPostTensioningElement ptElement = new FiberSectionPostTensioningElement(ptSection.CenterPoint().Point, ptSection.TypeOfPT, ptSection.InitialPrestress, ptSection.JackingAndGrouting);

                        ptElements.Add(ptElement);
                    }
                }
            }

            return ptElements;
        }

        private GenericShape ConvertPolygonGtoPolygonFS(PolygonG pg)
        {
            PolygonFS pgFS = new PolygonFS(pg.Nodes.Select(x => x.Point).ToList(), pg.Material);
            pgFS.Hole = pg.Hole;
            pgFS.IsRebar = pg.IsRebar;

            return pgFS;
        }

        private GenericShape ConvertCircleGtoCircleFS(CircleG circle)
        {
            CircleFS cFS = new CircleFS(circle.CenterPoint().Point, circle.Radius, circle.Material);
            cFS.Hole = circle.Hole;
            cFS.IsRebar = circle.IsRebar;

            return cFS;
        }

        private GenericShape ConvertPolylineGtoPolylineFS(PolylineG pl)
        {
            PolylineFS pFS = new PolylineFS(pl.Nodes.Select(x => x.Point).ToList());
            pFS.IsRebar = pl.IsRebar;

            return pFS;
        }

        private GenericShape ConvertRebarSectionGtoRebarSectionFS(RebarSectionG rb)
        {
            RebarSectionFS rbFS = new RebarSectionFS(rb.CenterPoint().Point, rb.Rebar, rb.Material);

            return rbFS;
        }

        private GenericShape ConvertPostTensioningGtoPostTensioningSectionFS(PostTensioningG pt)
        {
            PostTensioningSectionFS ptFS = new PostTensioningSectionFS(pt.CenterPoint().Point, pt.TypeOfPT, pt.InitialPrestress, pt.JackingAndGrouting);

            return ptFS;
        }

        
        #endregion

    }
}
