﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TriangleNetHelper.ContainerClasses;

namespace CadPanel.Forms
{
    public partial class TriangleMeshOptions : Form
    {
        private MeshingOptions _meshOptions;
        public TriangleMeshOptions(MeshingOptions options)
        {
            InitializeComponent();

            _meshOptions = options;
        }

        private void TriangleMeshOptions_Load(object sender, EventArgs e)
        {
            if (_meshOptions != null)
            {
                PopulateControlsFromMeshOptions();
            }
        }

        private void PopulateControlsFromMeshOptions()
        {
            checkBox_ConformingDelaunay.Checked = _meshOptions.ConformingDelaunay;
            checkBox_Convex.Checked = _meshOptions.Convex;
            checkBox_QualityMesh.Checked = _meshOptions.QualityMesh;
            checkBox_RefineAfterSmooth.Checked = _meshOptions.RefineAfterSmooth;
            checkBox_SweepLine.Checked = _meshOptions.SweepLine;

            numericUpDown_NumberOfRefinements.Value = _meshOptions.NumberOfRefinements;
            numericUpDown_NumberOfSmooth.Value = _meshOptions.NumberOfSmooth;

            textBox_MinAngle.Text = Convert.ToString(_meshOptions.MinimumAngle);
            textBox_MaxAngle.Text = Convert.ToString(_meshOptions.MaximumAngle);
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            double minAngle = 0;
            double maxAngle = 0;

            try
            {
                if (double.TryParse(textBox_MinAngle.Text, out minAngle) == false)
                {
                    throw new Exception("Minimum angle must be numeric");
                }
                else
                {
                    if (minAngle < 15 || minAngle > 60)
                    {
                        throw new Exception("Minimum angle must be between 15 and 60 degrees");
                    }
                }

                if (double.TryParse(textBox_MaxAngle.Text, out maxAngle) == false)
                {
                    throw new Exception("Maximum angle must be numeric");
                }
                else
                {
                    if (maxAngle <= minAngle || maxAngle > 180)
                    {
                        throw new Exception("Maximum angle must be larger than the minimum angle and less than 180 degrees");
                    }
                }

                _meshOptions.MinimumAngle = minAngle;

                _meshOptions.MaximumAngle = maxAngle;

                _meshOptions.NumberOfRefinements = Convert.ToInt32(numericUpDown_NumberOfRefinements.Value);

                _meshOptions.NumberOfSmooth = Convert.ToInt32(numericUpDown_NumberOfSmooth.Value);

                _meshOptions.RefineAfterSmooth = checkBox_RefineAfterSmooth.Checked;

                _meshOptions.ConformingDelaunay = checkBox_ConformingDelaunay.Checked;

                _meshOptions.Convex = checkBox_Convex.Checked;

                _meshOptions.QualityMesh = checkBox_QualityMesh.Checked;

                _meshOptions.SweepLine = checkBox_SweepLine.Checked;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("The following validation error was encountered:" + Environment.NewLine + Environment.NewLine + ex.Message, "Validation Error", MessageBoxButtons.OK);
            }
        }
    }
}
