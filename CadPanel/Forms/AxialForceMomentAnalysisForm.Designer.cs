﻿namespace CadPanel.Forms
{
    partial class AxialForceMomentAnalysisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer_PM = new System.Windows.Forms.SplitContainer();
            this.groupBox_LimitStates = new System.Windows.Forms.GroupBox();
            this.label_LimitStates = new System.Windows.Forms.Label();
            this.button_DefineLimitStates = new System.Windows.Forms.Button();
            this.button_Analyze = new System.Windows.Forms.Button();
            this.groupBox_Loading = new System.Windows.Forms.GroupBox();
            this.label_LoadingDefined = new System.Windows.Forms.Label();
            this.button_DefineLoading = new System.Windows.Forms.Button();
            this.groupBox_DesignOptions = new System.Windows.Forms.GroupBox();
            this.checkBox_IncludePT = new System.Windows.Forms.CheckBox();
            this.radioButton_Spirals = new System.Windows.Forms.RadioButton();
            this.radioButton_ClosedHoops = new System.Windows.Forms.RadioButton();
            this.button_DeleteCase = new System.Windows.Forms.Button();
            this.groupBox_AnalysisOptions = new System.Windows.Forms.GroupBox();
            this.textBox_NumberOfPoints = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_AngleInterval = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_SingleAngle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton_RunSuite = new System.Windows.Forms.RadioButton();
            this.radioButton_RunSingle = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_MeshSize = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_NewAnalysis = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox_AnalysisCases = new System.Windows.Forms.ComboBox();
            this.groupBox_MMPlot = new System.Windows.Forms.GroupBox();
            this.label_LoadMy = new System.Windows.Forms.Label();
            this.label_LoadMx = new System.Windows.Forms.Label();
            this.label_LoadP = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox_Loads = new System.Windows.Forms.ComboBox();
            this.dataGridView_PM = new System.Windows.Forms.DataGridView();
            this.Curvature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mmaj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mmin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Phi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhiP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhiMmaj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhiMmin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Limit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_PMPlot = new System.Windows.Forms.GroupBox();
            this.radioButton_Y = new System.Windows.Forms.RadioButton();
            this.radioButton_X = new System.Windows.Forms.RadioButton();
            this.radioButton_Minor = new System.Windows.Forms.RadioButton();
            this.radioButton_Major = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_Angle = new System.Windows.Forms.ComboBox();
            this.groupBox_Options = new System.Windows.Forms.GroupBox();
            this.checkBox_ReductionFactors = new System.Windows.Forms.CheckBox();
            this.groupBox_PlotType = new System.Windows.Forms.GroupBox();
            this.radioButton_MM = new System.Windows.Forms.RadioButton();
            this.radioButton_PM = new System.Windows.Forms.RadioButton();
            this.button_Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_PM)).BeginInit();
            this.splitContainer_PM.Panel1.SuspendLayout();
            this.splitContainer_PM.Panel2.SuspendLayout();
            this.splitContainer_PM.SuspendLayout();
            this.groupBox_LimitStates.SuspendLayout();
            this.groupBox_Loading.SuspendLayout();
            this.groupBox_DesignOptions.SuspendLayout();
            this.groupBox_AnalysisOptions.SuspendLayout();
            this.groupBox_MMPlot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PM)).BeginInit();
            this.groupBox_PMPlot.SuspendLayout();
            this.groupBox_Options.SuspendLayout();
            this.groupBox_PlotType.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer_PM
            // 
            this.splitContainer_PM.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer_PM.Location = new System.Drawing.Point(12, 12);
            this.splitContainer_PM.Name = "splitContainer_PM";
            // 
            // splitContainer_PM.Panel1
            // 
            this.splitContainer_PM.Panel1.Controls.Add(this.groupBox_LimitStates);
            this.splitContainer_PM.Panel1.Controls.Add(this.button_Analyze);
            this.splitContainer_PM.Panel1.Controls.Add(this.groupBox_Loading);
            this.splitContainer_PM.Panel1.Controls.Add(this.groupBox_DesignOptions);
            this.splitContainer_PM.Panel1.Controls.Add(this.button_DeleteCase);
            this.splitContainer_PM.Panel1.Controls.Add(this.groupBox_AnalysisOptions);
            this.splitContainer_PM.Panel1.Controls.Add(this.button_NewAnalysis);
            this.splitContainer_PM.Panel1.Controls.Add(this.label1);
            this.splitContainer_PM.Panel1.Controls.Add(this.comboBox_AnalysisCases);
            // 
            // splitContainer_PM.Panel2
            // 
            this.splitContainer_PM.Panel2.Controls.Add(this.groupBox_MMPlot);
            this.splitContainer_PM.Panel2.Controls.Add(this.dataGridView_PM);
            this.splitContainer_PM.Panel2.Controls.Add(this.groupBox_PMPlot);
            this.splitContainer_PM.Panel2.Controls.Add(this.groupBox_Options);
            this.splitContainer_PM.Panel2.Controls.Add(this.groupBox_PlotType);
            this.splitContainer_PM.Size = new System.Drawing.Size(853, 589);
            this.splitContainer_PM.SplitterDistance = 284;
            this.splitContainer_PM.TabIndex = 0;
            // 
            // groupBox_LimitStates
            // 
            this.groupBox_LimitStates.Controls.Add(this.label_LimitStates);
            this.groupBox_LimitStates.Controls.Add(this.button_DefineLimitStates);
            this.groupBox_LimitStates.Location = new System.Drawing.Point(15, 458);
            this.groupBox_LimitStates.Name = "groupBox_LimitStates";
            this.groupBox_LimitStates.Size = new System.Drawing.Size(225, 79);
            this.groupBox_LimitStates.TabIndex = 8;
            this.groupBox_LimitStates.TabStop = false;
            this.groupBox_LimitStates.Text = "Limit States";
            // 
            // label_LimitStates
            // 
            this.label_LimitStates.AutoSize = true;
            this.label_LimitStates.Location = new System.Drawing.Point(9, 49);
            this.label_LimitStates.Name = "label_LimitStates";
            this.label_LimitStates.Size = new System.Drawing.Size(110, 13);
            this.label_LimitStates.TabIndex = 1;
            this.label_LimitStates.Text = "No limit states defined";
            // 
            // button_DefineLimitStates
            // 
            this.button_DefineLimitStates.Location = new System.Drawing.Point(9, 19);
            this.button_DefineLimitStates.Name = "button_DefineLimitStates";
            this.button_DefineLimitStates.Size = new System.Drawing.Size(107, 23);
            this.button_DefineLimitStates.TabIndex = 0;
            this.button_DefineLimitStates.Text = "Define Limit States";
            this.button_DefineLimitStates.UseVisualStyleBackColor = true;
            this.button_DefineLimitStates.Click += new System.EventHandler(this.Button_DefineLimitStates_Click);
            // 
            // button_Analyze
            // 
            this.button_Analyze.Location = new System.Drawing.Point(196, 555);
            this.button_Analyze.Name = "button_Analyze";
            this.button_Analyze.Size = new System.Drawing.Size(75, 23);
            this.button_Analyze.TabIndex = 7;
            this.button_Analyze.Text = "Analyze";
            this.button_Analyze.UseVisualStyleBackColor = true;
            this.button_Analyze.Click += new System.EventHandler(this.Button_Analyze_Click);
            // 
            // groupBox_Loading
            // 
            this.groupBox_Loading.Controls.Add(this.label_LoadingDefined);
            this.groupBox_Loading.Controls.Add(this.button_DefineLoading);
            this.groupBox_Loading.Location = new System.Drawing.Point(15, 373);
            this.groupBox_Loading.Name = "groupBox_Loading";
            this.groupBox_Loading.Size = new System.Drawing.Size(225, 79);
            this.groupBox_Loading.TabIndex = 6;
            this.groupBox_Loading.TabStop = false;
            this.groupBox_Loading.Text = "Loading";
            // 
            // label_LoadingDefined
            // 
            this.label_LoadingDefined.AutoSize = true;
            this.label_LoadingDefined.Location = new System.Drawing.Point(9, 49);
            this.label_LoadingDefined.Name = "label_LoadingDefined";
            this.label_LoadingDefined.Size = new System.Drawing.Size(96, 13);
            this.label_LoadingDefined.TabIndex = 1;
            this.label_LoadingDefined.Text = "No loading defined";
            // 
            // button_DefineLoading
            // 
            this.button_DefineLoading.Location = new System.Drawing.Point(9, 19);
            this.button_DefineLoading.Name = "button_DefineLoading";
            this.button_DefineLoading.Size = new System.Drawing.Size(107, 23);
            this.button_DefineLoading.TabIndex = 0;
            this.button_DefineLoading.Text = "Define Loading";
            this.button_DefineLoading.UseVisualStyleBackColor = true;
            this.button_DefineLoading.Click += new System.EventHandler(this.Button_DefineLoading_Click);
            // 
            // groupBox_DesignOptions
            // 
            this.groupBox_DesignOptions.Controls.Add(this.checkBox_IncludePT);
            this.groupBox_DesignOptions.Controls.Add(this.radioButton_Spirals);
            this.groupBox_DesignOptions.Controls.Add(this.radioButton_ClosedHoops);
            this.groupBox_DesignOptions.Location = new System.Drawing.Point(15, 265);
            this.groupBox_DesignOptions.Name = "groupBox_DesignOptions";
            this.groupBox_DesignOptions.Size = new System.Drawing.Size(225, 102);
            this.groupBox_DesignOptions.TabIndex = 5;
            this.groupBox_DesignOptions.TabStop = false;
            this.groupBox_DesignOptions.Text = "Design Options";
            // 
            // checkBox_IncludePT
            // 
            this.checkBox_IncludePT.AutoSize = true;
            this.checkBox_IncludePT.Location = new System.Drawing.Point(9, 70);
            this.checkBox_IncludePT.Name = "checkBox_IncludePT";
            this.checkBox_IncludePT.Size = new System.Drawing.Size(161, 17);
            this.checkBox_IncludePT.TabIndex = 2;
            this.checkBox_IncludePT.Text = "Include PT Force in Diagram";
            this.checkBox_IncludePT.UseVisualStyleBackColor = true;
            this.checkBox_IncludePT.CheckedChanged += new System.EventHandler(this.CheckBox_IncludePT_CheckedChanged);
            // 
            // radioButton_Spirals
            // 
            this.radioButton_Spirals.AutoSize = true;
            this.radioButton_Spirals.Location = new System.Drawing.Point(9, 45);
            this.radioButton_Spirals.Name = "radioButton_Spirals";
            this.radioButton_Spirals.Size = new System.Drawing.Size(56, 17);
            this.radioButton_Spirals.TabIndex = 1;
            this.radioButton_Spirals.TabStop = true;
            this.radioButton_Spirals.Text = "Spirals";
            this.radioButton_Spirals.UseVisualStyleBackColor = true;
            this.radioButton_Spirals.CheckedChanged += new System.EventHandler(this.RadioButton_Spirals_CheckedChanged);
            // 
            // radioButton_ClosedHoops
            // 
            this.radioButton_ClosedHoops.AutoSize = true;
            this.radioButton_ClosedHoops.Location = new System.Drawing.Point(9, 22);
            this.radioButton_ClosedHoops.Name = "radioButton_ClosedHoops";
            this.radioButton_ClosedHoops.Size = new System.Drawing.Size(91, 17);
            this.radioButton_ClosedHoops.TabIndex = 0;
            this.radioButton_ClosedHoops.TabStop = true;
            this.radioButton_ClosedHoops.Text = "Closed Hoops";
            this.radioButton_ClosedHoops.UseVisualStyleBackColor = true;
            this.radioButton_ClosedHoops.CheckedChanged += new System.EventHandler(this.RadioButton_ClosedHoops_CheckedChanged);
            // 
            // button_DeleteCase
            // 
            this.button_DeleteCase.Location = new System.Drawing.Point(189, 30);
            this.button_DeleteCase.Name = "button_DeleteCase";
            this.button_DeleteCase.Size = new System.Drawing.Size(51, 23);
            this.button_DeleteCase.TabIndex = 4;
            this.button_DeleteCase.Text = "Delete";
            this.button_DeleteCase.UseVisualStyleBackColor = true;
            // 
            // groupBox_AnalysisOptions
            // 
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_NumberOfPoints);
            this.groupBox_AnalysisOptions.Controls.Add(this.label9);
            this.groupBox_AnalysisOptions.Controls.Add(this.label6);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_AngleInterval);
            this.groupBox_AnalysisOptions.Controls.Add(this.label7);
            this.groupBox_AnalysisOptions.Controls.Add(this.label4);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_SingleAngle);
            this.groupBox_AnalysisOptions.Controls.Add(this.label5);
            this.groupBox_AnalysisOptions.Controls.Add(this.radioButton_RunSuite);
            this.groupBox_AnalysisOptions.Controls.Add(this.radioButton_RunSingle);
            this.groupBox_AnalysisOptions.Controls.Add(this.label3);
            this.groupBox_AnalysisOptions.Controls.Add(this.textBox_MeshSize);
            this.groupBox_AnalysisOptions.Controls.Add(this.label2);
            this.groupBox_AnalysisOptions.Location = new System.Drawing.Point(15, 69);
            this.groupBox_AnalysisOptions.Name = "groupBox_AnalysisOptions";
            this.groupBox_AnalysisOptions.Size = new System.Drawing.Size(225, 190);
            this.groupBox_AnalysisOptions.TabIndex = 3;
            this.groupBox_AnalysisOptions.TabStop = false;
            this.groupBox_AnalysisOptions.Text = "Analysis Options";
            // 
            // textBox_NumberOfPoints
            // 
            this.textBox_NumberOfPoints.Location = new System.Drawing.Point(112, 45);
            this.textBox_NumberOfPoints.Name = "textBox_NumberOfPoints";
            this.textBox_NumberOfPoints.Size = new System.Drawing.Size(55, 20);
            this.textBox_NumberOfPoints.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Number of Points = ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(176, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "deg";
            // 
            // textBox_AngleInterval
            // 
            this.textBox_AngleInterval.Location = new System.Drawing.Point(115, 153);
            this.textBox_AngleInterval.Name = "textBox_AngleInterval";
            this.textBox_AngleInterval.Size = new System.Drawing.Size(55, 20);
            this.textBox_AngleInterval.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Angle Interval = ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(138, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "deg";
            // 
            // textBox_SingleAngle
            // 
            this.textBox_SingleAngle.Location = new System.Drawing.Point(77, 99);
            this.textBox_SingleAngle.Name = "textBox_SingleAngle";
            this.textBox_SingleAngle.Size = new System.Drawing.Size(55, 20);
            this.textBox_SingleAngle.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Angle = ";
            // 
            // radioButton_RunSuite
            // 
            this.radioButton_RunSuite.AutoSize = true;
            this.radioButton_RunSuite.Location = new System.Drawing.Point(9, 130);
            this.radioButton_RunSuite.Name = "radioButton_RunSuite";
            this.radioButton_RunSuite.Size = new System.Drawing.Size(95, 17);
            this.radioButton_RunSuite.TabIndex = 4;
            this.radioButton_RunSuite.TabStop = true;
            this.radioButton_RunSuite.Text = "Run Full Onion";
            this.radioButton_RunSuite.UseVisualStyleBackColor = true;
            this.radioButton_RunSuite.CheckedChanged += new System.EventHandler(this.RadioButton_RunSuite_CheckedChanged);
            // 
            // radioButton_RunSingle
            // 
            this.radioButton_RunSingle.AutoSize = true;
            this.radioButton_RunSingle.Location = new System.Drawing.Point(9, 75);
            this.radioButton_RunSingle.Name = "radioButton_RunSingle";
            this.radioButton_RunSingle.Size = new System.Drawing.Size(107, 17);
            this.radioButton_RunSingle.TabIndex = 3;
            this.radioButton_RunSingle.TabStop = true;
            this.radioButton_RunSingle.Text = "Run Single Angle";
            this.radioButton_RunSingle.UseVisualStyleBackColor = true;
            this.radioButton_RunSingle.CheckedChanged += new System.EventHandler(this.RadioButton_RunSingle_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(141, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "in.";
            // 
            // textBox_MeshSize
            // 
            this.textBox_MeshSize.Location = new System.Drawing.Point(80, 19);
            this.textBox_MeshSize.Name = "textBox_MeshSize";
            this.textBox_MeshSize.Size = new System.Drawing.Size(55, 20);
            this.textBox_MeshSize.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mesh Size = ";
            // 
            // button_NewAnalysis
            // 
            this.button_NewAnalysis.Location = new System.Drawing.Point(142, 30);
            this.button_NewAnalysis.Name = "button_NewAnalysis";
            this.button_NewAnalysis.Size = new System.Drawing.Size(41, 23);
            this.button_NewAnalysis.TabIndex = 2;
            this.button_NewAnalysis.Text = "New";
            this.button_NewAnalysis.UseVisualStyleBackColor = true;
            this.button_NewAnalysis.Click += new System.EventHandler(this.Button_NewAnalysis_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Analysis Case";
            // 
            // comboBox_AnalysisCases
            // 
            this.comboBox_AnalysisCases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AnalysisCases.FormattingEnabled = true;
            this.comboBox_AnalysisCases.Location = new System.Drawing.Point(15, 31);
            this.comboBox_AnalysisCases.Name = "comboBox_AnalysisCases";
            this.comboBox_AnalysisCases.Size = new System.Drawing.Size(121, 21);
            this.comboBox_AnalysisCases.TabIndex = 0;
            this.comboBox_AnalysisCases.SelectedIndexChanged += new System.EventHandler(this.ComboBox_AnalysisCases_SelectedIndexChanged);
            // 
            // groupBox_MMPlot
            // 
            this.groupBox_MMPlot.Controls.Add(this.label_LoadMy);
            this.groupBox_MMPlot.Controls.Add(this.label_LoadMx);
            this.groupBox_MMPlot.Controls.Add(this.label_LoadP);
            this.groupBox_MMPlot.Controls.Add(this.label10);
            this.groupBox_MMPlot.Controls.Add(this.comboBox_Loads);
            this.groupBox_MMPlot.Location = new System.Drawing.Point(160, 173);
            this.groupBox_MMPlot.Name = "groupBox_MMPlot";
            this.groupBox_MMPlot.Size = new System.Drawing.Size(141, 173);
            this.groupBox_MMPlot.TabIndex = 4;
            this.groupBox_MMPlot.TabStop = false;
            this.groupBox_MMPlot.Text = "MM Plot";
            // 
            // label_LoadMy
            // 
            this.label_LoadMy.AutoSize = true;
            this.label_LoadMy.Location = new System.Drawing.Point(14, 116);
            this.label_LoadMy.Name = "label_LoadMy";
            this.label_LoadMy.Size = new System.Drawing.Size(33, 13);
            this.label_LoadMy.TabIndex = 4;
            this.label_LoadMy.Text = "My = ";
            // 
            // label_LoadMx
            // 
            this.label_LoadMx.AutoSize = true;
            this.label_LoadMx.Location = new System.Drawing.Point(14, 96);
            this.label_LoadMx.Name = "label_LoadMx";
            this.label_LoadMx.Size = new System.Drawing.Size(33, 13);
            this.label_LoadMx.TabIndex = 3;
            this.label_LoadMx.Text = "Mx = ";
            // 
            // label_LoadP
            // 
            this.label_LoadP.AutoSize = true;
            this.label_LoadP.Location = new System.Drawing.Point(14, 75);
            this.label_LoadP.Name = "label_LoadP";
            this.label_LoadP.Size = new System.Drawing.Size(26, 13);
            this.label_LoadP.TabIndex = 2;
            this.label_LoadP.Text = "P = ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Load Case";
            // 
            // comboBox_Loads
            // 
            this.comboBox_Loads.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Loads.FormattingEnabled = true;
            this.comboBox_Loads.Location = new System.Drawing.Point(17, 41);
            this.comboBox_Loads.Name = "comboBox_Loads";
            this.comboBox_Loads.Size = new System.Drawing.Size(105, 21);
            this.comboBox_Loads.TabIndex = 0;
            this.comboBox_Loads.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Loads_SelectedIndexChanged);
            // 
            // dataGridView_PM
            // 
            this.dataGridView_PM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_PM.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Curvature,
            this.P,
            this.Mmaj,
            this.Mmin,
            this.Phi,
            this.PhiP,
            this.PhiMmaj,
            this.PhiMmin,
            this.Limit});
            this.dataGridView_PM.Location = new System.Drawing.Point(13, 401);
            this.dataGridView_PM.Name = "dataGridView_PM";
            this.dataGridView_PM.Size = new System.Drawing.Size(538, 177);
            this.dataGridView_PM.TabIndex = 3;
            // 
            // Curvature
            // 
            this.Curvature.HeaderText = "Curvature";
            this.Curvature.Name = "Curvature";
            this.Curvature.ReadOnly = true;
            this.Curvature.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Curvature.Width = 75;
            // 
            // P
            // 
            this.P.HeaderText = "P";
            this.P.Name = "P";
            this.P.ReadOnly = true;
            this.P.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.P.Width = 60;
            // 
            // Mmaj
            // 
            this.Mmaj.HeaderText = "Mmaj";
            this.Mmaj.Name = "Mmaj";
            this.Mmaj.ReadOnly = true;
            this.Mmaj.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Mmaj.Width = 60;
            // 
            // Mmin
            // 
            this.Mmin.HeaderText = "Mmin";
            this.Mmin.Name = "Mmin";
            this.Mmin.ReadOnly = true;
            this.Mmin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Mmin.Width = 60;
            // 
            // Phi
            // 
            this.Phi.HeaderText = "Phi";
            this.Phi.Name = "Phi";
            this.Phi.ReadOnly = true;
            this.Phi.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Phi.Width = 60;
            // 
            // PhiP
            // 
            this.PhiP.HeaderText = "Phi P";
            this.PhiP.Name = "PhiP";
            this.PhiP.ReadOnly = true;
            this.PhiP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PhiP.Width = 60;
            // 
            // PhiMmaj
            // 
            this.PhiMmaj.HeaderText = "Phi Mmaj";
            this.PhiMmaj.Name = "PhiMmaj";
            this.PhiMmaj.ReadOnly = true;
            this.PhiMmaj.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PhiMmaj.Width = 60;
            // 
            // PhiMmin
            // 
            this.PhiMmin.HeaderText = "Phi Mmin";
            this.PhiMmin.Name = "PhiMmin";
            this.PhiMmin.ReadOnly = true;
            this.PhiMmin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PhiMmin.Width = 60;
            // 
            // Limit
            // 
            this.Limit.HeaderText = "Limit";
            this.Limit.Name = "Limit";
            this.Limit.ReadOnly = true;
            this.Limit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Limit.Width = 60;
            // 
            // groupBox_PMPlot
            // 
            this.groupBox_PMPlot.Controls.Add(this.radioButton_Y);
            this.groupBox_PMPlot.Controls.Add(this.radioButton_X);
            this.groupBox_PMPlot.Controls.Add(this.radioButton_Minor);
            this.groupBox_PMPlot.Controls.Add(this.radioButton_Major);
            this.groupBox_PMPlot.Controls.Add(this.label8);
            this.groupBox_PMPlot.Controls.Add(this.comboBox_Angle);
            this.groupBox_PMPlot.Location = new System.Drawing.Point(13, 153);
            this.groupBox_PMPlot.Name = "groupBox_PMPlot";
            this.groupBox_PMPlot.Size = new System.Drawing.Size(141, 173);
            this.groupBox_PMPlot.TabIndex = 2;
            this.groupBox_PMPlot.TabStop = false;
            this.groupBox_PMPlot.Text = "PM Plot";
            // 
            // radioButton_Y
            // 
            this.radioButton_Y.AutoSize = true;
            this.radioButton_Y.Location = new System.Drawing.Point(17, 140);
            this.radioButton_Y.Name = "radioButton_Y";
            this.radioButton_Y.Size = new System.Drawing.Size(95, 17);
            this.radioButton_Y.TabIndex = 5;
            this.radioButton_Y.TabStop = true;
            this.radioButton_Y.Text = "Y Axis Moment";
            this.radioButton_Y.UseVisualStyleBackColor = true;
            this.radioButton_Y.CheckedChanged += new System.EventHandler(this.RadioButton_Y_CheckedChanged);
            // 
            // radioButton_X
            // 
            this.radioButton_X.AutoSize = true;
            this.radioButton_X.Location = new System.Drawing.Point(17, 117);
            this.radioButton_X.Name = "radioButton_X";
            this.radioButton_X.Size = new System.Drawing.Size(95, 17);
            this.radioButton_X.TabIndex = 4;
            this.radioButton_X.TabStop = true;
            this.radioButton_X.Text = "X Axis Moment";
            this.radioButton_X.UseVisualStyleBackColor = true;
            this.radioButton_X.CheckedChanged += new System.EventHandler(this.RadioButton_X_CheckedChanged);
            // 
            // radioButton_Minor
            // 
            this.radioButton_Minor.AutoSize = true;
            this.radioButton_Minor.Location = new System.Drawing.Point(17, 94);
            this.radioButton_Minor.Name = "radioButton_Minor";
            this.radioButton_Minor.Size = new System.Drawing.Size(114, 17);
            this.radioButton_Minor.TabIndex = 3;
            this.radioButton_Minor.TabStop = true;
            this.radioButton_Minor.Text = "Minor Axis Moment";
            this.radioButton_Minor.UseVisualStyleBackColor = true;
            this.radioButton_Minor.CheckedChanged += new System.EventHandler(this.RadioButton_Minor_CheckedChanged);
            // 
            // radioButton_Major
            // 
            this.radioButton_Major.AutoSize = true;
            this.radioButton_Major.Location = new System.Drawing.Point(17, 71);
            this.radioButton_Major.Name = "radioButton_Major";
            this.radioButton_Major.Size = new System.Drawing.Size(114, 17);
            this.radioButton_Major.TabIndex = 2;
            this.radioButton_Major.TabStop = true;
            this.radioButton_Major.Text = "Major Axis Moment";
            this.radioButton_Major.UseVisualStyleBackColor = true;
            this.radioButton_Major.CheckedChanged += new System.EventHandler(this.RadioButton_Major_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Analysis Angle";
            // 
            // comboBox_Angle
            // 
            this.comboBox_Angle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Angle.FormattingEnabled = true;
            this.comboBox_Angle.Location = new System.Drawing.Point(17, 41);
            this.comboBox_Angle.Name = "comboBox_Angle";
            this.comboBox_Angle.Size = new System.Drawing.Size(108, 21);
            this.comboBox_Angle.TabIndex = 0;
            this.comboBox_Angle.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Angle_SelectedIndexChanged);
            // 
            // groupBox_Options
            // 
            this.groupBox_Options.Controls.Add(this.checkBox_ReductionFactors);
            this.groupBox_Options.Location = new System.Drawing.Point(13, 94);
            this.groupBox_Options.Name = "groupBox_Options";
            this.groupBox_Options.Size = new System.Drawing.Size(137, 53);
            this.groupBox_Options.TabIndex = 1;
            this.groupBox_Options.TabStop = false;
            this.groupBox_Options.Text = "Options";
            // 
            // checkBox_ReductionFactors
            // 
            this.checkBox_ReductionFactors.AutoSize = true;
            this.checkBox_ReductionFactors.Location = new System.Drawing.Point(17, 21);
            this.checkBox_ReductionFactors.Name = "checkBox_ReductionFactors";
            this.checkBox_ReductionFactors.Size = new System.Drawing.Size(108, 17);
            this.checkBox_ReductionFactors.TabIndex = 0;
            this.checkBox_ReductionFactors.Text = "Apply Phi Factors";
            this.checkBox_ReductionFactors.UseVisualStyleBackColor = true;
            this.checkBox_ReductionFactors.CheckedChanged += new System.EventHandler(this.CheckBox_ReductionFactors_CheckedChanged);
            // 
            // groupBox_PlotType
            // 
            this.groupBox_PlotType.Controls.Add(this.radioButton_MM);
            this.groupBox_PlotType.Controls.Add(this.radioButton_PM);
            this.groupBox_PlotType.Location = new System.Drawing.Point(13, 15);
            this.groupBox_PlotType.Name = "groupBox_PlotType";
            this.groupBox_PlotType.Size = new System.Drawing.Size(137, 73);
            this.groupBox_PlotType.TabIndex = 0;
            this.groupBox_PlotType.TabStop = false;
            this.groupBox_PlotType.Text = "Type of Plot";
            // 
            // radioButton_MM
            // 
            this.radioButton_MM.AutoSize = true;
            this.radioButton_MM.Location = new System.Drawing.Point(17, 44);
            this.radioButton_MM.Name = "radioButton_MM";
            this.radioButton_MM.Size = new System.Drawing.Size(43, 17);
            this.radioButton_MM.TabIndex = 1;
            this.radioButton_MM.TabStop = true;
            this.radioButton_MM.Text = "MM";
            this.radioButton_MM.UseVisualStyleBackColor = true;
            this.radioButton_MM.CheckedChanged += new System.EventHandler(this.RadioButton_MM_CheckedChanged);
            // 
            // radioButton_PM
            // 
            this.radioButton_PM.AutoSize = true;
            this.radioButton_PM.Location = new System.Drawing.Point(17, 21);
            this.radioButton_PM.Name = "radioButton_PM";
            this.radioButton_PM.Size = new System.Drawing.Size(41, 17);
            this.radioButton_PM.TabIndex = 0;
            this.radioButton_PM.TabStop = true;
            this.radioButton_PM.Text = "PM";
            this.radioButton_PM.UseVisualStyleBackColor = true;
            this.radioButton_PM.CheckedChanged += new System.EventHandler(this.RadioButton_PM_CheckedChanged);
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(790, 607);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 8;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // AxialForceMomentAnalysisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 636);
            this.Controls.Add(this.button_Close);
            this.Controls.Add(this.splitContainer_PM);
            this.MaximumSize = new System.Drawing.Size(890, 675);
            this.MinimumSize = new System.Drawing.Size(890, 675);
            this.Name = "AxialForceMomentAnalysisForm";
            this.Text = "Axial Force - Moment Analysis";
            this.Load += new System.EventHandler(this.AxialForceMomentAnalysisForm_Load);
            this.splitContainer_PM.Panel1.ResumeLayout(false);
            this.splitContainer_PM.Panel1.PerformLayout();
            this.splitContainer_PM.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_PM)).EndInit();
            this.splitContainer_PM.ResumeLayout(false);
            this.groupBox_LimitStates.ResumeLayout(false);
            this.groupBox_LimitStates.PerformLayout();
            this.groupBox_Loading.ResumeLayout(false);
            this.groupBox_Loading.PerformLayout();
            this.groupBox_DesignOptions.ResumeLayout(false);
            this.groupBox_DesignOptions.PerformLayout();
            this.groupBox_AnalysisOptions.ResumeLayout(false);
            this.groupBox_AnalysisOptions.PerformLayout();
            this.groupBox_MMPlot.ResumeLayout(false);
            this.groupBox_MMPlot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PM)).EndInit();
            this.groupBox_PMPlot.ResumeLayout(false);
            this.groupBox_PMPlot.PerformLayout();
            this.groupBox_Options.ResumeLayout(false);
            this.groupBox_Options.PerformLayout();
            this.groupBox_PlotType.ResumeLayout(false);
            this.groupBox_PlotType.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer_PM;
        private System.Windows.Forms.GroupBox groupBox_Loading;
        private System.Windows.Forms.Label label_LoadingDefined;
        private System.Windows.Forms.Button button_DefineLoading;
        private System.Windows.Forms.GroupBox groupBox_DesignOptions;
        private System.Windows.Forms.RadioButton radioButton_Spirals;
        private System.Windows.Forms.RadioButton radioButton_ClosedHoops;
        private System.Windows.Forms.Button button_DeleteCase;
        private System.Windows.Forms.GroupBox groupBox_AnalysisOptions;
        private System.Windows.Forms.TextBox textBox_NumberOfPoints;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_AngleInterval;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_SingleAngle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton_RunSuite;
        private System.Windows.Forms.RadioButton radioButton_RunSingle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_MeshSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_NewAnalysis;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox_AnalysisCases;
        private System.Windows.Forms.Button button_Analyze;
        private System.Windows.Forms.CheckBox checkBox_IncludePT;
        private System.Windows.Forms.GroupBox groupBox_LimitStates;
        private System.Windows.Forms.Label label_LimitStates;
        private System.Windows.Forms.Button button_DefineLimitStates;
        private System.Windows.Forms.GroupBox groupBox_PMPlot;
        private System.Windows.Forms.RadioButton radioButton_Y;
        private System.Windows.Forms.RadioButton radioButton_X;
        private System.Windows.Forms.RadioButton radioButton_Minor;
        private System.Windows.Forms.RadioButton radioButton_Major;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_Angle;
        private System.Windows.Forms.GroupBox groupBox_Options;
        private System.Windows.Forms.CheckBox checkBox_ReductionFactors;
        private System.Windows.Forms.GroupBox groupBox_PlotType;
        private System.Windows.Forms.RadioButton radioButton_MM;
        private System.Windows.Forms.RadioButton radioButton_PM;
        private System.Windows.Forms.DataGridView dataGridView_PM;
        private System.Windows.Forms.DataGridViewTextBoxColumn Curvature;
        private System.Windows.Forms.DataGridViewTextBoxColumn P;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mmaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn Phi;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhiP;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhiMmaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhiMmin;
        private System.Windows.Forms.DataGridViewTextBoxColumn Limit;
        private System.Windows.Forms.GroupBox groupBox_MMPlot;
        private System.Windows.Forms.Label label_LoadMy;
        private System.Windows.Forms.Label label_LoadMx;
        private System.Windows.Forms.Label label_LoadP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox_Loads;
        private System.Windows.Forms.Button button_Close;
    }
}