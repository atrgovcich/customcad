﻿namespace CadPanel.Forms
{
    partial class LayerPropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_Layers = new System.Windows.Forms.DataGridView();
            this.LayerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LayerColor = new System.Windows.Forms.DataGridViewButtonColumn();
            this.LineThickness = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Visible = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Frozen = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Print = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Button_RemoveLayer = new System.Windows.Forms.Button();
            this.Button_AddLayer = new System.Windows.Forms.Button();
            this.Button_Cancel = new System.Windows.Forms.Button();
            this.Button_Apply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Layers)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_Layers
            // 
            this.dgv_Layers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Layers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LayerName,
            this.LayerColor,
            this.LineThickness,
            this.Visible,
            this.Frozen,
            this.Print});
            this.dgv_Layers.Location = new System.Drawing.Point(16, 101);
            this.dgv_Layers.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_Layers.Name = "dgv_Layers";
            this.dgv_Layers.RowHeadersWidth = 51;
            this.dgv_Layers.Size = new System.Drawing.Size(737, 426);
            this.dgv_Layers.TabIndex = 3;
            // 
            // LayerName
            // 
            this.LayerName.HeaderText = "Name";
            this.LayerName.MinimumWidth = 6;
            this.LayerName.Name = "LayerName";
            this.LayerName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LayerName.Width = 125;
            // 
            // LayerColor
            // 
            this.LayerColor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.LayerColor.HeaderText = "Color";
            this.LayerColor.MinimumWidth = 6;
            this.LayerColor.Name = "LayerColor";
            this.LayerColor.Width = 50;
            // 
            // LineThickness
            // 
            this.LineThickness.HeaderText = "Line Thickness";
            this.LineThickness.MinimumWidth = 6;
            this.LineThickness.Name = "LineThickness";
            this.LineThickness.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.LineThickness.Width = 110;
            // 
            // Visible
            // 
            this.Visible.HeaderText = "Visible";
            this.Visible.MinimumWidth = 6;
            this.Visible.Name = "Visible";
            this.Visible.Width = 50;
            // 
            // Frozen
            // 
            this.Frozen.HeaderText = "Frozen";
            this.Frozen.MinimumWidth = 6;
            this.Frozen.Name = "Frozen";
            this.Frozen.Width = 50;
            // 
            // Print
            // 
            this.Print.HeaderText = "Print";
            this.Print.MinimumWidth = 6;
            this.Print.Name = "Print";
            this.Print.Width = 50;
            // 
            // Button_RemoveLayer
            // 
            this.Button_RemoveLayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_RemoveLayer.Location = new System.Drawing.Point(83, 48);
            this.Button_RemoveLayer.Margin = new System.Windows.Forms.Padding(4);
            this.Button_RemoveLayer.Name = "Button_RemoveLayer";
            this.Button_RemoveLayer.Size = new System.Drawing.Size(59, 46);
            this.Button_RemoveLayer.TabIndex = 2;
            this.Button_RemoveLayer.Text = "-";
            this.Button_RemoveLayer.UseVisualStyleBackColor = true;
            this.Button_RemoveLayer.Click += new System.EventHandler(this.Button_RemoveLayer_Click);
            // 
            // Button_AddLayer
            // 
            this.Button_AddLayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_AddLayer.Location = new System.Drawing.Point(16, 48);
            this.Button_AddLayer.Margin = new System.Windows.Forms.Padding(4);
            this.Button_AddLayer.Name = "Button_AddLayer";
            this.Button_AddLayer.Size = new System.Drawing.Size(59, 46);
            this.Button_AddLayer.TabIndex = 1;
            this.Button_AddLayer.Text = "+";
            this.Button_AddLayer.UseVisualStyleBackColor = true;
            this.Button_AddLayer.Click += new System.EventHandler(this.Button_AddLayer_Click);
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(631, 602);
            this.Button_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(123, 28);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.UseVisualStyleBackColor = true;
            this.Button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // Button_Apply
            // 
            this.Button_Apply.Location = new System.Drawing.Point(500, 602);
            this.Button_Apply.Margin = new System.Windows.Forms.Padding(4);
            this.Button_Apply.Name = "Button_Apply";
            this.Button_Apply.Size = new System.Drawing.Size(123, 28);
            this.Button_Apply.TabIndex = 4;
            this.Button_Apply.Text = "Apply Changes";
            this.Button_Apply.UseVisualStyleBackColor = true;
            this.Button_Apply.Click += new System.EventHandler(this.Button_Apply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 25);
            this.label1.TabIndex = 22;
            this.label1.Text = "Layer Properties";
            // 
            // LayerPropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 645);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Apply);
            this.Controls.Add(this.Button_RemoveLayer);
            this.Controls.Add(this.Button_AddLayer);
            this.Controls.Add(this.dgv_Layers);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LayerPropertiesForm";
            this.Text = "Layer Properties";
            this.Load += new System.EventHandler(this.LayerPropertiesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Layers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_Layers;
        internal System.Windows.Forms.Button Button_RemoveLayer;
        internal System.Windows.Forms.Button Button_AddLayer;
        internal System.Windows.Forms.Button Button_Cancel;
        internal System.Windows.Forms.Button Button_Apply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LayerName;
        private System.Windows.Forms.DataGridViewButtonColumn LayerColor;
        private System.Windows.Forms.DataGridViewTextBoxColumn LineThickness;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Visible;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Frozen;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Print;
    }
}