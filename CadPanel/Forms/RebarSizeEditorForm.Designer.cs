﻿namespace CadPanel.Forms
{
    partial class RebarSizeEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_Rebar = new System.Windows.Forms.DataGridView();
            this.RebarSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Diameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Area = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_Reset = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Accept = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Rebar)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Rebar
            // 
            this.dataGridView_Rebar.AllowUserToAddRows = false;
            this.dataGridView_Rebar.AllowUserToDeleteRows = false;
            this.dataGridView_Rebar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Rebar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RebarSize,
            this.Diameter,
            this.Area});
            this.dataGridView_Rebar.Location = new System.Drawing.Point(12, 56);
            this.dataGridView_Rebar.Name = "dataGridView_Rebar";
            this.dataGridView_Rebar.RowHeadersWidth = 51;
            this.dataGridView_Rebar.RowTemplate.Height = 24;
            this.dataGridView_Rebar.Size = new System.Drawing.Size(460, 320);
            this.dataGridView_Rebar.TabIndex = 0;
            // 
            // RebarSize
            // 
            this.RebarSize.HeaderText = "Rebar Size";
            this.RebarSize.MinimumWidth = 6;
            this.RebarSize.Name = "RebarSize";
            this.RebarSize.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RebarSize.Width = 125;
            // 
            // Diameter
            // 
            this.Diameter.HeaderText = "Diameter (in)";
            this.Diameter.MinimumWidth = 6;
            this.Diameter.Name = "Diameter";
            this.Diameter.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Diameter.Width = 125;
            // 
            // Area
            // 
            this.Area.HeaderText = "Area (in^2)";
            this.Area.MinimumWidth = 6;
            this.Area.Name = "Area";
            this.Area.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Area.Width = 125;
            // 
            // button_Reset
            // 
            this.button_Reset.Location = new System.Drawing.Point(478, 56);
            this.button_Reset.Name = "button_Reset";
            this.button_Reset.Size = new System.Drawing.Size(134, 36);
            this.button_Reset.TabIndex = 1;
            this.button_Reset.Text = "Reset to Default";
            this.button_Reset.UseVisualStyleBackColor = true;
            this.button_Reset.Click += new System.EventHandler(this.Button_Reset_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(478, 140);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(134, 36);
            this.button_Delete.TabIndex = 2;
            this.button_Delete.Text = "Delete Selected";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.Button_Delete_Click);
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(478, 98);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(134, 36);
            this.button_Add.TabIndex = 3;
            this.button_Add.Text = "Add New";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(506, 439);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(106, 32);
            this.button_Cancel.TabIndex = 4;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_Accept
            // 
            this.button_Accept.Location = new System.Drawing.Point(394, 439);
            this.button_Accept.Name = "button_Accept";
            this.button_Accept.Size = new System.Drawing.Size(106, 32);
            this.button_Accept.TabIndex = 5;
            this.button_Accept.Text = "Accept";
            this.button_Accept.UseVisualStyleBackColor = true;
            this.button_Accept.Click += new System.EventHandler(this.Button_Accept_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Rebar Sizes";
            // 
            // RebarSizeEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 483);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Accept);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.button_Delete);
            this.Controls.Add(this.button_Reset);
            this.Controls.Add(this.dataGridView_Rebar);
            this.Name = "RebarSizeEditorForm";
            this.Text = "Edit Rebar Sizes";
            this.Load += new System.EventHandler(this.RebarSizeEditorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Rebar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Rebar;
        private System.Windows.Forms.Button button_Reset;
        private System.Windows.Forms.Button button_Delete;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_Accept;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RebarSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn Diameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Area;
    }
}