﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPanel.Snapping;

namespace CadPanel.Forms
{
    public partial class SnappingOptionsForm : Form
    {
        private SnapProperties _properties;

        public SnappingOptionsForm(SnapProperties properties)
        {
            InitializeComponent();

            _properties = properties;
        }

        private void SnappingOptions_Load(object sender, EventArgs e)
        {
            SetInitialCheckBoxStates();

            SetInitialTextBoxValues();

            if (_properties.PointSnapsOn == true)
            {
                groupBox_PointSnaps.Enabled = true;
            }
            else
            {
                groupBox_PointSnaps.Enabled = false;
            }

            if (_properties.SnapOrtho == true)
            {
                groupBox_OrthoSnaps.Enabled = true;
            }
            else
            {
                groupBox_OrthoSnaps.Enabled = false;
            }
        }

        private void SetInitialCheckBoxStates()
        {
            if (_properties.PointSnapsOn == true)
            {
                checkBox_PointSnapsOn.Checked = true;
            }

            if (_properties.SnapEndPoints == true)
            {
                checkBox_EndPoints.Checked = true;
            }

            if (_properties.SnapMidPoints == true)
            {
                checkBox_MidPoints.Checked = true;
            }

            if (_properties.SnapCenter == true)
            {
                checkBox_CenterPoints.Checked = true;
            }

            if (_properties.SnapQuadrant == true)
            {
                checkBox_QuadrantPoints.Checked = true;
            }

            if (_properties.SnapPerpendicular == true)
            {
                checkBox_Perpendicular.Checked = true;
            }

            if (_properties.SnapIntersection == true)
            {
                checkBox_Intersections.Checked = true;
            }

            if (_properties.SnapApparentOrthoIntersection == true)
            {
                checkBox_ApparentOrthoIntersections.Checked = true;
            }

            if (_properties.SnapOrtho == true)
            {
                checkBox_OrthoSnapsOn.Checked = true;
            }
        }

        private void SetInitialTextBoxValues()
        {
            textBox_SnapDistance.Text = Convert.ToString(_properties.BaseSnapDistance);

            textBox_SnapAngleFuzz.Text = Convert.ToString(_properties.OrthoSnapAngleTolerance);

            textBox_SnapAngleInterval.Text = Convert.ToString(_properties.OrthoSnapAngleInterval);
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            if (ValidateTextBoxes(out string message) == false)
            {
                MessageBox.Show(message, "Validation Error", MessageBoxButtons.OK);
            }
            else
            {
                _properties.BaseSnapDistance = Convert.ToDouble(textBox_SnapDistance.Text);

                _properties.OrthoSnapAngleInterval = Convert.ToDouble(textBox_SnapAngleInterval.Text);

                _properties.OrthoSnapAngleTolerance = Convert.ToDouble(textBox_SnapAngleFuzz.Text);

                _properties.PointSnapsOn = checkBox_PointSnapsOn.Checked;

                _properties.SnapOrtho = checkBox_OrthoSnapsOn.Checked;

                _properties.SnapEndPoints = checkBox_EndPoints.Checked;

                _properties.SnapMidPoints = checkBox_MidPoints.Checked;

                _properties.SnapCenter = checkBox_CenterPoints.Checked;

                _properties.SnapQuadrant = checkBox_QuadrantPoints.Checked;

                _properties.SnapPerpendicular = checkBox_Perpendicular.Checked;

                _properties.SnapIntersection = checkBox_Intersections.Checked;

                _properties.SnapApparentOrthoIntersection = checkBox_ApparentOrthoIntersections.Checked;

                this.Close();
            }
        }

        private bool ValidateTextBoxes(out string message)
        {
            if (double.TryParse(textBox_SnapDistance.Text, out double snapDistRes) == false)
            {
                message = "Snap distance must be numeric";

                return false;
            }

            if (double.TryParse(textBox_SnapAngleFuzz.Text, out double snapAngleFuzzRes) == false)
            {
                message = "Snap angle fuzz must be numeric";

                return false;
            }
            else
            {
                if (snapAngleFuzzRes >= 360)
                {
                    message = "Snap angle fuzz must be less than 360 degrees";

                    return false;
                }
            }

            if (double.TryParse(textBox_SnapAngleInterval.Text, out double snapAngleIntervalRes) == false)
            {
                message = "Snap angle interval must be numeric";

                return false;
            }
            else
            {
                if (snapAngleIntervalRes >= 360)
                {
                    message = "Snap angle interval must be less than 360 degrees";

                    return false;
                }
            }

            message = "Success";

            return true;
        }

        private void checkBox_PointSnapsOn_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_PointSnapsOn.Checked == true)
            {
                groupBox_PointSnaps.Enabled = true;
            }
            else
            {
                groupBox_PointSnaps.Enabled = false;
            }
        }

        private void checkBox_OrthoSnapsOn_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox_OrthoSnapsOn.Checked == true)
            {
                groupBox_OrthoSnaps.Enabled = true;
            }
            else
            {
                groupBox_OrthoSnaps.Enabled = false;
            }
        }
    }
}
