﻿namespace CadPanel.Forms
{
    partial class DefineCrossSectionLoadingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_Loads = new System.Windows.Forms.DataGridView();
            this.LoadCase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AxialForce = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MxMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyMax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MxTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MxBot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MyBot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_OK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Loads)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Loads
            // 
            this.dataGridView_Loads.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Loads.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LoadCase,
            this.AxialForce,
            this.MxMax,
            this.MyMax,
            this.MxTop,
            this.MyTop,
            this.MxBot,
            this.MyBot,
            this.Vx,
            this.Vy});
            this.dataGridView_Loads.Location = new System.Drawing.Point(12, 12);
            this.dataGridView_Loads.Name = "dataGridView_Loads";
            this.dataGridView_Loads.Size = new System.Drawing.Size(776, 348);
            this.dataGridView_Loads.TabIndex = 0;
            // 
            // LoadCase
            // 
            this.LoadCase.HeaderText = "Load Case";
            this.LoadCase.Name = "LoadCase";
            // 
            // AxialForce
            // 
            this.AxialForce.HeaderText = "Axial Force (kips)";
            this.AxialForce.Name = "AxialForce";
            // 
            // MxMax
            // 
            this.MxMax.HeaderText = "Mx Max (kip-in)";
            this.MxMax.Name = "MxMax";
            // 
            // MyMax
            // 
            this.MyMax.HeaderText = "My Max (kip-in)";
            this.MyMax.Name = "MyMax";
            // 
            // MxTop
            // 
            this.MxTop.HeaderText = "Mx Top (kip-in)";
            this.MxTop.Name = "MxTop";
            // 
            // MyTop
            // 
            this.MyTop.HeaderText = "My Top (kip-in)";
            this.MyTop.Name = "MyTop";
            // 
            // MxBot
            // 
            this.MxBot.HeaderText = "Mx Bot (kip-in)";
            this.MxBot.Name = "MxBot";
            // 
            // MyBot
            // 
            this.MyBot.HeaderText = "My Bot (kip-in)";
            this.MyBot.Name = "MyBot";
            // 
            // Vx
            // 
            this.Vx.HeaderText = "Vx (kips)";
            this.Vx.Name = "Vx";
            // 
            // Vy
            // 
            this.Vy.HeaderText = "Vy (kips)";
            this.Vy.Name = "Vy";
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(713, 415);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 1;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.Button_Cancel_Click);
            // 
            // button_OK
            // 
            this.button_OK.Location = new System.Drawing.Point(632, 415);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 2;
            this.button_OK.Text = "OK";
            this.button_OK.UseVisualStyleBackColor = true;
            this.button_OK.Click += new System.EventHandler(this.Button_OK_Click);
            // 
            // DefineCrossSectionLoadingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.dataGridView_Loads);
            this.Name = "DefineCrossSectionLoadingForm";
            this.Text = "Cross Section Loads";
            this.Load += new System.EventHandler(this.DefineCrossSectionLoadingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Loads)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Loads;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoadCase;
        private System.Windows.Forms.DataGridViewTextBoxColumn AxialForce;
        private System.Windows.Forms.DataGridViewTextBoxColumn MxMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn MyMax;
        private System.Windows.Forms.DataGridViewTextBoxColumn MxTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn MyTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn MxBot;
        private System.Windows.Forms.DataGridViewTextBoxColumn MyBot;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vx;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vy;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_OK;
    }
}