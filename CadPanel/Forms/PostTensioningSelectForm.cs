﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ReinforcedConcrete.PostTensioning;

namespace CadPanel.Forms
{
    public partial class PostTensioningSelectForm : Form
    {
        private List<PostTensioningDefinition> _postTensioningList = new List<PostTensioningDefinition>();
        public bool Cancel { get; set; } = true;
        public string SelectedName { get; set; } = null;

        public PostTensioningSelectForm(List<PostTensioningDefinition> postTensioningList)
        {
            InitializeComponent();

            _postTensioningList = postTensioningList;
        }

        private void PostTensioningSelectForm_Load(object sender, EventArgs e)
        {
            Size constSize = new Size(305, 175);
            this.MinimumSize = constSize;
            this.MaximumSize = constSize;
            this.Size = constSize;

            PopulatePostTensioningComboBox();
        }

        #region Private Methods
        private void PopulatePostTensioningComboBox()
        {
            comboBox_Name.Items.Clear();

            for (int i = 0; i < _postTensioningList.Count; i++)
            {
                comboBox_Name.Items.Add(_postTensioningList[i].Name);
            }

            if (_postTensioningList.Count > 0)
            {
                comboBox_Name.SelectedIndex = 0;
            }
        }
        #endregion

        private void Button_OK_Click(object sender, EventArgs e)
        {
            Cancel = false;
            SelectedName = comboBox_Name.Text;
            this.Close();
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            Cancel = true;
            SelectedName = null;
            this.Close();
        }
    }
}
