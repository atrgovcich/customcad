﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CadPanel.CommandList;
using CadPanel.Extensions;
using System.Linq;
using System.Text;

namespace CadPanel.Forms
{
    public partial class CommandListForm : Form
    {
        private List<CadCommand> _commandList;
        private int _minWidth = 300;
        private int _minHeight = 300;

        public CommandListForm(List<CadCommand> commandList)
        {
            InitializeComponent();

            _commandList = new List<CadCommand>(commandList);
        }

        private void CommandListForm_Load(object sender, EventArgs e)
        {
            CreateFormControls();

            this.MinimumSize = new Size(_minWidth, _minHeight);

            CommandListForm_Resize(this, new EventArgs());

            DisplayAllCommands();
        }

        private void DisplayAllCommands()
        {
            _commandList = _commandList.OrderBy(x => x.Name).ToList();

            StringBuilder sB = new StringBuilder();

            sB.AppendLine("<html>");
            sB.AppendLine("<head>");
            sB.AppendLine("<style>");
            sB.AppendLine("table {font-family: arial, sans-serif;");
            sB.AppendLine("border-collapse: collapse;");
            sB.AppendLine("width: 100%;}");
            sB.AppendLine("td, th {");
            sB.AppendLine("border: 1px solid #DDDDDD;");
            sB.AppendLine("text-align: left");
            sB.AppendLine("padding: 8px;}");
            sB.AppendLine("tr: nth-child (even) {");
            sB.AppendLine("background-color: #DDDDDD;}");
            sB.AppendLine("</style>");
            sB.AppendLine("</head>");
            sB.AppendLine("<body>");
            sB.AppendLine("<h2>CAD Commands</h2>");
            sB.AppendLine("<table>");
            sB.AppendLine("<tr>");
            sB.AppendLine("<th>Name</th>");
            sB.AppendLine("<th>Description</th>");
            sB.AppendLine("<th>Shortcuts</th>");
            sB.AppendLine("</tr>");

            for (int i = 0; i < _commandList.Count; i++)
            {
                CadCommand c = _commandList[i];

                sB.AppendLine("<tr>");

                string shortcuts = string.Empty;

                if (c.Shortcuts != null)
                {
                    for (int j = 0; j < c.Shortcuts.Count; j++)
                    {
                        shortcuts = shortcuts + c.Shortcuts[j];

                        if (j < c.Shortcuts.Count - 1)
                        {
                            shortcuts = shortcuts + ", ";
                        }
                    }
                }

                sB.AppendLine($"<td>{c.Name}</td><td>{c.Description}</td><td>{shortcuts}</td>");

                sB.AppendLine("</tr>");
            }

            sB.AppendLine("</table>");
            sB.AppendLine("</body>");
            sB.AppendLine("</html>");

            if (this.ContainsControl("WebBrowser_Commands", out Control wB) == true)
            {
                ((WebBrowser)wB).DocumentText = sB.ToString();
            }
        }

        private void CreateFormControls()
        {
            //Create the web browser panel
            Panel pan = new Panel();
            pan.Name = "Panel_WebBrowser";
            pan.Location = new Point(10, 20);
            pan.Size = new Size(this.Width - 40, this.Height - 80);
            pan.BorderStyle = BorderStyle.Fixed3D;

            WebBrowser wb = new WebBrowser();
            wb.Name = "WebBrowser_Commands";
            wb.Location = new Point(3, 3);
            wb.Size = new Size(pan.Width - 6, pan.Height - 6);

            pan.Controls.Add(wb);

            this.Controls.Add(pan);
        }

        private void CommandListForm_Resize(object sender, EventArgs e)
        {
            ResizeWebBrowser();
        }

        private void ResizeWebBrowser()
        {
            if (this.Width >= _minWidth && this.Height >= _minHeight)
            {
                if (this.ContainsControl("Panel_WebBrowser", out Control pan) == true)
                {
                    pan.Width = this.Width - 40;
                    pan.Height = this.Height - 80;

                    if (pan.ContainsControl("WebBrowser_Commands", out Control wb) == true)
                    {
                        wb.Width = pan.Width - 6;
                        wb.Height = pan.Height - 6;
                    }
                }
            }
        }

    }
}
