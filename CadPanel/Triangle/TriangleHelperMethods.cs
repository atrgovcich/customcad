﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.DrawingElements;
using TriangleNetHelper.Geometry;
using CadPanel.Common;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.Triangle
{
    public static class TriangleHelperMethods
    {
        public static PrePolygon ConvertPolygonGtoPrePolygon(PolygonG boundary)
        {
            List<PointD> polygonPointList = boundary.Nodes.Select(x => x.Point).ToList();
            polygonPointList.RemoveAt(polygonPointList.Count - 1);

            List<PreVertex> vertices = new List<PreVertex>(polygonPointList.Count);

            for (int i = 0; i < polygonPointList.Count; i++)
            {
                vertices.Add(polygonPointList[i].ConvertToPreVertex());
            }
            

            return new PrePolygon(vertices) { Hole = boundary.Hole };
        }

        public static List<PolygonG> ConvertFromPrePolygons(IEnumerable<PrePolygon> meshBoundaries)
        {
            var convertedMeshes = new List<PolygonG>();

            var boundaries = meshBoundaries.ToList();

            for (int i = 0; i < boundaries.Count; i++)
            {
                PrePolygon boundary = boundaries[i];

                convertedMeshes.Add(ConvertFromPrePolygon(boundary));
            }

            return convertedMeshes;
        }

        public static List<GenericPolygon> ConvertFromPrePolgyonsToGenericPolygons(IEnumerable<PrePolygon> meshBoundaries)
        {
            var convertedMeshes = new List<GenericPolygon>();

            var boundaries = meshBoundaries.ToList();

            for (int i = 0; i < boundaries.Count; i++)
            {
                PrePolygon boundary = boundaries[i];

                convertedMeshes.Add(ConvertFromPrePolygonToGenericPolygon(boundary));
            }

            return convertedMeshes;
        }

        public static PolygonG ConvertFromPrePolygon(PrePolygon meshBoundary)
        {
            List<PreVertex> vertices = meshBoundary.Vertices;

            List<DrawingNode> nodeList = new List<DrawingNode>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++)
            {
                PreVertex pV = vertices[i];

                PointD pD = new PointD(pV.X, pV.Y);

                DrawingNode n = new DrawingNode(pD, NodeType.EndPoint);

                nodeList.Add(n);
            }

            PolygonG poly = new PolygonG()
            {
                Nodes = nodeList
            };

            return poly;
        }

        public static GenericPolygon ConvertFromPrePolygonToGenericPolygon(PrePolygon meshBoundary)
        {
            List<PreVertex> vertices = meshBoundary.Vertices;

            List<PointD> nodeList = new List<PointD>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++)
            {
                PreVertex pV = vertices[i];

                PointD pD = new PointD(pV.X, pV.Y);

                nodeList.Add(pD);
            }

            if (nodeList[0] == nodeList[nodeList.Count - 1])
            {
                nodeList[nodeList.Count - 1] = nodeList[0];
            }

            return new GenericPolygon(nodeList);
        }

        public static PrePolyline ConvertPolylineGtoPrePolyline(PolylineG segment)
        {
            List<PointD> vertices = segment.Nodes.Select(x => x.Point).ToList();

            List<PreVertex> preVertices = new List<PreVertex>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++)
            {
                PreVertex v = new PreVertex(vertices[i].X, vertices[i].Y);

                preVertices.Add(v);
            }

            return new PrePolyline(preVertices);
        }

        public static PreVertex ConvertPointGtoPreVertex(PointG point)
        {
            return new PreVertex(point.Node().Point.X, point.Node().Point.Y);
        }
    }
}
