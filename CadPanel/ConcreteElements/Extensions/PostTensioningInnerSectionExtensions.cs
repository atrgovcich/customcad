﻿using System.Collections.Generic;
using CadPanel.DrawingElements;
using CadPanel.Common;
using Utilities.Geometry;
using ReinforcedConcrete.PostTensioning;

namespace CadPanel.ConcreteElements.Extensions
{
    public static class PostTensioningInnerSectionExtensions
    {
        public static List<CircleG> GetWireCirclesG(this PostTensioningInnerSection pt, PointD c)
        {
            List<GenericCircle> wireCircles = pt.GetWireCircles(c);

            List<CircleG> wireCirclesG = new List<CircleG>(wireCircles.Count);

            for (int i = 0; i < wireCircles.Count; i++)
            {
                CircleG cG = new CircleG() { Radius = wireCircles[i].Radius, Hole = false, Material = null, Selected = false, Layer = null };
                cG.Nodes.Add(new DrawingNode(wireCircles[i].Center, NodeType.CenterPoint));

                wireCirclesG.Add(cG);
            }

            return wireCirclesG;
        }
    }
}
