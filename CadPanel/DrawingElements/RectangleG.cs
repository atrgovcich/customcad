﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CadPanel.Common;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.DrawingElements
{
    public class RectangleG : PolygonG
    {
        #region Constructors
        public RectangleG() : base()
        {

        }
        #endregion
        #region Public Overriden Methods
        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            if (Nodes.Count == 1)
            {
                Pen myPen = new Pen(Layer.Color, Layer.Width);

                if (Selected == true)
                {
                    myPen.Color = instanceData.SelectedObjectLineColor;
                    myPen.Width = instanceData.SelectedObjectLineWidth;
                }

                if (IsRebar == true)
                {
                    if (Rebar != null)
                    {
                        myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                    }
                }

                Point[] pixelPoints = new Point[4];

                PointD p0d = Nodes[0].Point;

                double delta_x = currentPoint.X - p0d.X;

                double delta_y = currentPoint.Y - p0d.Y;

                PointD p1d = new PointD(p0d.X + delta_x, p0d.Y);

                PointD p2d = new PointD(p0d.X + delta_x, p0d.Y + delta_y);

                PointD p3d = new PointD(p0d.X, p0d.Y + delta_y);

                Point p0 = p0d.ConvertToPixelPoint(zeroPoint, drawingScale);

                Point p1 = p1d.ConvertToPixelPoint(zeroPoint, drawingScale);

                Point p2 = p2d.ConvertToPixelPoint(zeroPoint, drawingScale);

                Point p3 = p3d.ConvertToPixelPoint(zeroPoint, drawingScale);

                pixelPoints[0] = p0;

                pixelPoints[1] = p1;

                pixelPoints[2] = p2;

                pixelPoints[3] = p3;

                if (pixelPoints.GetUpperBound(0) >= 1)
                {
                    if (FillShape == false)
                    {
                        g.DrawPolygon(myPen, pixelPoints);
                    }
                    else
                    {
                        Color? fillBrushColor = FillColor;

                        if (fillBrushColor == null)
                        {
                            if (Selected == false)
                            {
                                fillBrushColor = Layer.Color;
                            }
                            else
                            {
                                fillBrushColor = instanceData.SelectedObjectLineColor;
                            }
                        }

                        SolidBrush fillBrush = new SolidBrush(fillBrushColor.Value);

                        g.FillPolygon(fillBrush, pixelPoints);
                        fillBrush.Dispose();
                    }

                    if (Selected == true)
                    {
                        SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                        for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                        {
                            Rectangle boundingRectangle = new Rectangle
                                (
                                    Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                                    Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                                    instanceData.SelectedObjectPointWidth,
                                    instanceData.SelectedObjectPointWidth
                                );

                            g.FillEllipse(myBrush, boundingRectangle);
                        }

                        myBrush.Dispose();
                    }
                }

                myPen.Dispose();
            }

        }
        #endregion

        #region Public Methods
        public static RectangleG CreateRectangleFromCornerPoints(PointD p0, PointD currentPoint, Layer currentLayer)
        {
            double delta_x = currentPoint.X - p0.X;

            double delta_y = currentPoint.Y - p0.Y;

            PointD p1 = new PointD(p0.X + delta_x, p0.Y);

            PointD p2 = new PointD(p0.X + delta_x, p0.Y + delta_y);

            PointD p3 = new PointD(p0.X, p0.Y + delta_y);

            PointD p4 = p0;

            List<PointD> points = new List<PointD>() { p0, p1, p2, p3, p4 };

            List<DrawingNode> nodes = new List<DrawingNode>(5);

            foreach (PointD p in points)
            {
                DrawingNode n = new DrawingNode(p, NodeType.EndPoint);

                nodes.Add(n);
            }

            return new RectangleG() { Layer = currentLayer, Nodes = nodes };
        }
        #endregion
    }
}
