﻿using System.Collections.Generic;
using static System.Math;
using CadPanel.Common;
using Utilities.Geometry;
using Utilities.DataStructures.QuadTree;

namespace CadPanel.DrawingElements
{
    public class LineSegment
    {

        public DrawingNode StartNode { get; set; }
        public DrawingNode EndNode { get; set; }

        protected int _precision = 6;

        #region Constructors
        public LineSegment(DrawingNode startNode, DrawingNode endNode)
        {
            StartNode = startNode;
            EndNode = endNode;
        }
        #endregion

        #region Public Methods
        public static LineSegment LineSegmentFromBasePointAndAngle(PointD basePoint, double angleInRadians)
        {
            DrawingNode n0 = new DrawingNode(basePoint, NodeType.EndPoint);

            double x1 = basePoint.X + Cos(angleInRadians);

            double y1 = basePoint.Y + Sin(angleInRadians);

            DrawingNode n1 = new DrawingNode(new PointD(x1, y1), NodeType.EndPoint);

            return new LineSegment(n0, n1);
        }

        /// <summary>
        /// Returns the length of the line segment
        /// </summary>
        /// <returns></returns>
        public double Length()
        {
            return Sqrt(Pow(EndNode.Point.X - StartNode.Point.X, 2) + Pow(EndNode.Point.Y - StartNode.Point.Y, 2));
        }

        /// <summary>
        /// Reverses the direction of the line segment
        /// </summary>
        public void Reverse()
        {
            DrawingNode temp = EndNode;
            EndNode = StartNode;
            StartNode = temp;
        }

        public PointD Midpoint()
        {
            return new PointD((StartNode.Point.X + EndNode.Point.X) / 2.0, (StartNode.Point.Y + EndNode.Point.Y) / 2.0);
        }

        public bool IntersectsWith(LineSegment other, out PointD intersection)
        {
            double verticalTolerance = 1.0 * Pow(10.0, -_precision);

            intersection = null;

            double minX = Min(StartNode.Point.X, EndNode.Point.X);
            double maxX = Max(StartNode.Point.X, EndNode.Point.X);

            double minY = Min(StartNode.Point.Y, EndNode.Point.Y);
            double maxY = Max(StartNode.Point.Y, EndNode.Point.Y);

            double minX_o = Min(other.StartNode.Point.X, other.EndNode.Point.X);
            double maxX_o = Max(other.StartNode.Point.X, other.EndNode.Point.X);

            double minY_o = Min(other.StartNode.Point.Y, other.EndNode.Point.Y);
            double maxY_o = Max(other.StartNode.Point.Y, other.EndNode.Point.Y);

            if (Abs(StartNode.Point.X - EndNode.Point.X) <= verticalTolerance)
            {
                if (Abs(other.StartNode.Point.X - other.EndNode.Point.X) <= verticalTolerance)
                {
                    //lines are either parallel or colinear
                    return false;
                }
                else
                {
                    double m_o = (other.EndNode.Point.Y - other.StartNode.Point.Y) / (other.EndNode.Point.X - other.StartNode.Point.X);

                    double b_o = other.StartNode.Point.Y - m_o * other.StartNode.Point.X;

                    double x_int = StartNode.Point.X;

                    double y_int = m_o * x_int + b_o;

                    if (y_int >= minY && y_int <= maxY)
                    {
                        if (x_int >= minX_o && x_int <= maxX_o)
                        {
                            intersection = new PointD(x_int, y_int);

                            return true;
                        }
                    }
                }
            }
            else
            {
                double m = (EndNode.Point.Y - StartNode.Point.Y) / (EndNode.Point.X - StartNode.Point.X);

                double b = StartNode.Point.Y - m * StartNode.Point.X;

                if (other.StartNode.Point.X == other.EndNode.Point.X)
                {
                    double x_int = other.StartNode.Point.X;

                    double y_int = m * x_int + b;

                    if (y_int >= minY_o && y_int <= maxY_o)
                    {
                        if (x_int >= minX && x_int <= maxX)
                        {
                            intersection = new PointD(x_int, y_int);

                            return true;
                        }
                    }
                }
                else
                {
                    double m_o = (other.EndNode.Point.Y - other.StartNode.Point.Y) / (other.EndNode.Point.X - other.StartNode.Point.X);

                    double b_o = other.StartNode.Point.Y - m_o * other.StartNode.Point.X;

                    if (m == m_o)
                    {
                        return false;
                    }
                    else
                    {
                        double x_int = (b_o - b) / (m - m_o);

                        double y_int = m * x_int + b;

                        if (x_int >= minX && x_int <= maxX)
                        {
                            if (x_int >= minX_o && x_int <= maxX_o)
                            {
                                intersection = new PointD(x_int, y_int);

                                return true;
                            }   
                        }
                    }
                }
            }

            return false;
        }

        public bool IntersectsWithInfiniteLine(LineSegment other, out PointD intersection)
        {
            intersection = null;

            double minX = Min(StartNode.Point.X, EndNode.Point.X);
            double maxX = Max(StartNode.Point.X, EndNode.Point.X);

            double minY = Min(StartNode.Point.Y, EndNode.Point.Y);
            double maxY = Max(StartNode.Point.Y, EndNode.Point.Y);

            if (StartNode.Point.X == EndNode.Point.X)
            {
                if (other.StartNode.Point.X == other.EndNode.Point.X)
                {
                    //lines are either parallel or colinear
                    return false;
                }
                else
                {
                    double m_o = (other.EndNode.Point.Y - other.StartNode.Point.Y) / (other.EndNode.Point.X - other.StartNode.Point.X);

                    double b_o = other.StartNode.Point.Y - m_o * other.StartNode.Point.X;

                    double x_int = StartNode.Point.X;

                    double y_int = m_o * x_int + b_o;

                    if (y_int >= minY && y_int <= maxY)
                    {
                        intersection = new PointD(x_int, y_int);

                        return true;
                    }
                }
            }
            else
            {
                double m = (EndNode.Point.Y - StartNode.Point.Y) / (EndNode.Point.X - StartNode.Point.X);

                double b = StartNode.Point.Y - m * StartNode.Point.X;

                if (other.StartNode.Point.X == other.EndNode.Point.X)
                {
                    double x_int = other.StartNode.Point.X;

                    double y_int = m * x_int + b;

                    if (x_int >= minX && x_int <= maxX)
                    {
                        intersection = new PointD(x_int, y_int);

                        return true;
                    }
                }
                else
                {
                    double m_o = (other.EndNode.Point.Y - other.StartNode.Point.Y) / (other.EndNode.Point.X - other.StartNode.Point.X);

                    double b_o = other.StartNode.Point.Y - m_o * other.StartNode.Point.X;

                    if (m == m_o)
                    {
                        return false;
                    }
                    else
                    {
                        double x_int = (b_o - b) / (m - m_o);

                        double y_int = m * x_int + b;

                        if (x_int >= minX && x_int <= maxX)
                        {
                            intersection = new PointD(x_int, y_int);

                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public PointD GetClosestPerpendicularPointToInfiniteExtension(PointD other)
        {
            PointD closestPoint = null;

            if (StartNode.Point.X == EndNode.Point.X)
            {
                //vertical line
                closestPoint = new PointD(StartNode.Point.X, other.Y);
            }
            else if (StartNode.Point.Y == EndNode.Point.Y)
            {
                //horizontal line
                closestPoint = new PointD(other.X, StartNode.Point.Y);
            }
            else
            {
                //determine slope of line
                double m = (EndNode.Point.Y - StartNode.Point.Y) / (EndNode.Point.X - StartNode.Point.X);

                double b = StartNode.Point.Y - m * StartNode.Point.X;

                double m_inv = -1.0 / m;

                double b_inv = other.Y - m_inv * other.X;

                double xInt = (b_inv - b) / (m - m_inv);

                double yInt = m * xInt + b;

                closestPoint = new PointD(xInt, yInt);
            }

            return closestPoint;
        }

        public void ExtendToFitWithinRectangle(Rect boundingRectangle)
        {
            if (boundingRectangle.Contains(StartNode.Point.X, StartNode.Point.Y) || boundingRectangle.Contains(EndNode.Point.X, EndNode.Point.Y))
            {
                PointD p0 = new PointD(boundingRectangle.X, boundingRectangle.Y);
                PointD p1 = new PointD(boundingRectangle.X + boundingRectangle.Width, boundingRectangle.Y + boundingRectangle.Height);

                RectangleG tempRectG = RectangleG.CreateRectangleFromCornerPoints(p0, p1, null);

                if (tempRectG.IntersectsWithInfiniteLine(this, out List<PointD> intersections) == true)
                {
                    if (intersections.Count == 2)
                    {
                        StartNode.Point = intersections[0];
                        EndNode.Point = intersections[1];
                    }
                }
            }
            
        }

        public Side WhichSide(PointD p)
        {
            double x1 = StartNode.Point.X;
            double y1 = StartNode.Point.Y;

            double x2 = EndNode.Point.X;
            double y2 = EndNode.Point.Y;

            double xp = p.X;
            double yp = p.Y;

            double result = ((yp - y1) * (x2 - x1)) - ((y2 - y1) * (xp - x1));

            if (result > 0)
            {
                return Side.Left;
            }
            else if (result < 0)
            {
                return Side.Right;
            }
            else
            {
                return Side.Colinear;
            }
        }
        #endregion
    }
}
