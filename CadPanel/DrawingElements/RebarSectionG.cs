﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CadPanel.Common;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using CadPanel.Extensions;
using ReinforcedConcrete.Reinforcement;
using FiberSectionAnalysis.Geometry;
using Newtonsoft.Json;

namespace CadPanel.DrawingElements
{
    public class RebarSectionG : CircleG
    {
        #region Public Properties
        public override double Radius
        {
            get
            {
                return _rebarDefinition.Diameter / 2.0;
            }
            set
            {
                _radius = value;
            }
        }

        private RebarDefinition _rebarDefinition;
        public override RebarDefinition Rebar
        {
            get
            {
                return _rebarDefinition;
            }
            set
            {
                _rebarDefinition = value;

                if (_rebarDefinition != null)
                {
                    Radius = _rebarDefinition.Diameter / 2.0;
                }
            }
        }
        #endregion

        #region Constructors
        public RebarSectionG(RebarDefinition barSize) : base("Rebar Section")
        {
            Rebar = barSize;
            Radius = (barSize != null) ? barSize.Diameter / 2.0 : 0;
            IsRebar = true;
            _elementType = DrawingElementType.RebarSection;
        }
        #endregion

        #region Public Overridden Methods
        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            DrawingNode cp = new DrawingNode(currentPoint, NodeType.CenterPoint);

            Point pixelCenterPoint = cp.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            int pixelRadius = Convert.ToInt32(Radius * drawingScale);

            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            g.DrawEllipse(myPen, boundingRectangle);

            if (Selected == true)
            {
                List<PointD> quadPoints = QuadrantPoints();

                List<Point> pixelQuadPoints = new List<Point>(5);

                pixelQuadPoints.Add(pixelCenterPoint);

                foreach (PointD p in quadPoints)
                {
                    pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                }

                SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                foreach (Point p in pixelQuadPoints)
                {
                    Rectangle quadBoundingRectangle = new Rectangle(
                        Convert.ToInt32(p.X - instanceData.SelectedObjectPointWidth / 2.0),
                        Convert.ToInt32(p.Y - instanceData.SelectedObjectPointWidth / 2.0),
                        instanceData.SelectedObjectPointWidth,
                        instanceData.SelectedObjectPointWidth);

                    g.FillEllipse(myBrush, quadBoundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            DrawingNode currentCenterNode = CenterPoint();

            PointD copiedCenterPoint = new PointD(currentCenterNode.Point.X + delta_X, currentCenterNode.Point.Y + delta_Y);

            DrawingNode centerNode = new DrawingNode(copiedCenterPoint, NodeType.CenterPoint);

            RebarSectionG copiedRebar = new RebarSectionG(Rebar)
            {
                Nodes = new List<DrawingNode>(1) { centerNode },
                Hole = Hole,
                IsRebar = IsRebar,
                Layer = Layer,
                Material = Material,
                Name = Name,
                Radius = Radius,
                Selected = false
            };

            return copiedRebar;
        }

        public override void ActionAfterReshape()
        {
            UpdateOpenGLGeometry();

            return;
        }

        public override List<PointD> QuadrantPoints()
        {
            return BuildInitialQuadrantPoints();
        }

        public override IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            //Cannot offset rebar (would create different size rebar)
            return null;
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            return null;
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            RebarSectionFS rbFS = new RebarSectionFS(this.CenterPoint().Point, this.Rebar, this.Material);

            return rbFS;
        }
        #endregion

        #region Protected Methods
        protected override void PopulateQuadrantNodes()
        {
            return;  //Do nothing
        }

        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.ClosedGeometry,
                ElementPropertyType.Rebar,
                ElementPropertyType.Material,
                ElementPropertyType.UserProperties
            };
        }
        #endregion
    }
}
