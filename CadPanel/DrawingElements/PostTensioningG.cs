﻿using System;
using System.Collections.Generic;
using static System.Math;
using System.Drawing;
using CadPanel.Common;
using ReinforcedConcrete.PostTensioning;
using CadPanel.ConcreteElements.Extensions;
using Materials;
using Newtonsoft.Json;
using Utilities.Extensions.DotNetNative;
using CadPanel.PropertyPanel;
using CadPanel.Extensions;
using FiberSectionAnalysis.General;
using Utilities.DataStructures.QuadTree;
using FiberSectionAnalysis.Geometry;

using OpenTK;

using Point = System.Drawing.Point;
using Color = System.Drawing.Color;
using Layer = CadPanel.Common.Layer;
using Utilities.Geometry;

namespace CadPanel.DrawingElements
{
    public class PostTensioningG : DrawingElement
    {
        #region Public Properties
        public PostTensioningDefinition TypeOfPT { get; set; }
        public JackingAndGroutingType JackingAndGrouting { get; set; } = JackingAndGroutingType.JackedGroutedReleased;
        public double MomentAtGrouting { get; set; } = 0;
        public double StrainAtGrouting { get; set; } = 0;
        public bool FillInnerStrands { get; set; } = false;

        public double Radius
        {
            get
            {
                return TypeOfPT.OuterDiameter / 2.0;
            }
        }
        #endregion

        #region Private Fields
        [JsonProperty]
        private double _initialPrestress = 162;
        [JsonProperty]
        private double _initialPrestrain = 0;
        #endregion

        #region Public Getters And Setters
        [JsonIgnore]
        public override IGenericMaterial Material
        {
            get
            {
                return TypeOfPT.Material;
            }
        }

        [JsonIgnore]
        public double OuterDiameter
        {
            get
            {
                return TypeOfPT.OuterDiameter;
            }
        }

        [JsonIgnore]
        public double InitialPrestrain
        {
            get
            {
                return _initialPrestrain;
            }
        }

        [JsonIgnore]
        public double InitialPrestress
        {
            get
            {
                return _initialPrestress;
            }
            set
            {
                _initialPrestress = value;

                if (Material != null)
                {
                    _initialPrestrain = GetInitialPrestrain(value);
                }
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public PostTensioningG() : base()
        {
            Name = "Post Tensioning";
            _elementType = DrawingElementType.PostTensioning;
        }

        public PostTensioningG(PointD center, PostTensioningDefinition ptType, Layer layer, double initialPrestress, JackingAndGroutingType installationType, double momentAtGrout) : this()
        {
            if (center != null)
            {
                Nodes.Add(new DrawingNode(center, NodeType.CenterPoint));
            }

            TypeOfPT = ptType;
            Layer = layer;
            InitialPrestress = initialPrestress;
            JackingAndGrouting = installationType;
            MomentAtGrouting = momentAtGrout;

            if (center != null)
            {
                UpdateOpenGLGeometry();
            }
        }

        public PostTensioningG(PointD center, PostTensioningDefinition ptType, Layer layer, double initialPrestress, JackingAndGroutingType installationType):
            this(center, ptType, layer, initialPrestress, installationType, 0)
        {

        }
        #endregion

        #region Public Implemented Methods
        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            //Draw the outer circle
            DrawingNode cp = CenterPoint();

            Point pixelCenterPoint = cp.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            int pixelRadius = Convert.ToInt32(TypeOfPT.OuterDiameter / 2.0 * drawingScale);

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            g.DrawEllipse(myPen, boundingRectangle);

            //Draw all of the inner circles
            List<CircleG> circleList = TypeOfPT.PTSection.GetWireCirclesG(cp.Point);

            foreach (CircleG circle in circleList)
            {
                Point circleCP = circle.CenterPoint().Point.ConvertToPixelPoint(zeroPoint, drawingScale);

                int circleRadius = Convert.ToInt32(circle.Radius * drawingScale);

                Rectangle circleBoundingRectangle = new Rectangle(
                    circleCP.X - circleRadius,
                    circleCP.Y - circleRadius,
                    circleRadius * 2,
                    circleRadius * 2);

                if (FillInnerStrands == false)
                {
                    g.DrawEllipse(myPen, circleBoundingRectangle);
                }
                else
                {
                    Color brushColor = Layer.Color;

                    if (Selected == true)
                    {
                        brushColor = instanceData.SelectedObjectLineColor;
                    }

                    SolidBrush fillBrush = new SolidBrush(brushColor);
                    g.FillEllipse(fillBrush, circleBoundingRectangle);
                    fillBrush.Dispose();
                }
            }

            if (Selected == true)
            {
                if (numSelectedElements == 1 || (numSelectedElements > 1 && instanceData.DrawVerticesOfMultipleObjects == true))
                {
                    List<PointD> quadPoints = QuadrantPoints();

                    List<Point> pixelQuadPoints = new List<Point>(5);

                    pixelQuadPoints.Add(pixelCenterPoint);

                    foreach (PointD p in quadPoints)
                    {
                        pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                    }

                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    foreach (Point p in pixelQuadPoints)
                    {
                        Rectangle quadBoundingRectangle = new Rectangle(
                            Convert.ToInt32(p.X - instanceData.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(p.Y - instanceData.SelectedObjectPointWidth / 2.0),
                            instanceData.SelectedObjectPointWidth,
                            instanceData.SelectedObjectPointWidth);

                        g.FillEllipse(myBrush, quadBoundingRectangle);
                    }

                    myBrush.Dispose();
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            //Draw the outer circle
            DrawingNode cp = new DrawingNode(currentPoint, NodeType.CenterPoint);

            Point pixelCenterPoint = cp.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            int pixelRadius = Convert.ToInt32(TypeOfPT.OuterDiameter / 2.0 * drawingScale);

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            g.DrawEllipse(myPen, boundingRectangle);

            //Draw all of the inner circles
            List<CircleG> circleList = TypeOfPT.PTSection.GetWireCirclesG(cp.Point);

            foreach (CircleG circle in circleList)
            {
                Point circleCP = circle.CenterPoint().Point.ConvertToPixelPoint(zeroPoint, drawingScale);

                int circleRadius = Convert.ToInt32(circle.Radius * drawingScale);

                Rectangle circleBoundingRectangle = new Rectangle(
                    circleCP.X - circleRadius,
                    circleCP.Y - circleRadius,
                    circleRadius * 2,
                    circleRadius * 2);

                if (FillInnerStrands == false)
                {
                    g.DrawEllipse(myPen, circleBoundingRectangle);
                }
                else
                {
                    Color brushColor = Layer.Color;

                    if (Selected == true)
                    {
                        brushColor = instanceData.SelectedObjectLineColor;
                    }

                    SolidBrush fillBrush = new SolidBrush(brushColor);
                    g.FillEllipse(fillBrush, circleBoundingRectangle);
                    fillBrush.Dispose();
                }
            }

            if (Selected == true)
            {
                List<PointD> quadPoints = QuadrantPoints();

                List<Point> pixelQuadPoints = new List<Point>(5);

                pixelQuadPoints.Add(pixelCenterPoint);

                foreach (PointD p in quadPoints)
                {
                    pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                }

                SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                foreach (Point p in pixelQuadPoints)
                {
                    Rectangle quadBoundingRectangle = new Rectangle(
                        Convert.ToInt32(p.X - instanceData.SelectedObjectPointWidth / 2.0),
                        Convert.ToInt32(p.Y - instanceData.SelectedObjectPointWidth / 2.0),
                        instanceData.SelectedObjectPointWidth,
                        instanceData.SelectedObjectPointWidth);

                    g.FillEllipse(myBrush, quadBoundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override Rect BoundingRectangle()
        {
            double outerRadius = TypeOfPT.OuterDiameter / 2.0;

            DrawingNode cp = CenterPoint();

            int x0 = Convert.ToInt32(Floor(cp.Point.X - outerRadius));
            int y0 = Convert.ToInt32(Floor(cp.Point.Y - outerRadius));

            int width = Convert.ToInt32(Ceiling(cp.Point.X + outerRadius - x0));
            int height = width;

            return new Rect(x0, y0, width, height);
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {
            double outerRadius = TypeOfPT.OuterDiameter / 2.0;

            PointD cp = CenterPoint().Point;

            if (nodeType.Equals(NodeType.CenterPoint) == true)
            {
                return cp;
            }
            else if (nodeType.Equals(NodeType.QuadrantPoint) == true)
            {
                double closestDist = 1.0E10;

                PointD closestPoint = null;

                List<PointD> quadrantPoints = new List<PointD>()
                {
                    new PointD(cp.X + outerRadius, cp.Y),
                    new PointD(cp.X, cp.Y + outerRadius),
                    new PointD(cp.X - outerRadius, cp.Y),
                    new PointD(cp.X, cp.Y - outerRadius)
                };

                foreach (PointD p in quadrantPoints)
                {
                    if (globalPoint.DistanceTo(p) <= closestDist)
                    {
                        closestDist = globalPoint.DistanceTo(p);
                        closestPoint = p;
                    }
                }

                return closestPoint;
            }
            else
            {
                return null;
            }
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (IntersectsWithInfiniteLine(line, out List<PointD> infIntersections) == true)
            {
                PointD p0 = line.StartNode.Point;
                PointD p1 = line.EndNode.Point;

                for (int i = 0; i < infIntersections.Count; i++)
                {
                    PointD p = infIntersections[i];

                    if (Round(p1.X - p0.X, _precision) == 0)
                    {
                        //vertical line
                        if (p.Y >= Min(p0.Y, p1.Y) && p.Y <= Max(p0.Y, p1.Y))
                        {
                            intersections.Add(p);
                        }
                    }
                    else
                    {
                        //horizontal line or line with slope
                        if (p.X >= Min(p0.X, p1.X) && p.X <= Max(p0.X, p1.X))
                        {
                            intersections.Add(p);
                        }
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            double r = TypeOfPT.OuterDiameter / 2.0;

            DrawingNode cN = CenterPoint();

            PointD cP = cN.Point;

            PointD p0 = line.StartNode.Point;

            PointD p1 = line.EndNode.Point;

            if (Round(p1.X - p0.X, _precision) == 0)
            {
                //line is vertical
                if (p0.X < cP.X - r || p0.X > cP.X + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double xInt = p0.X;

                    double yInt1 = Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    double yInt2 = -Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    intersections.Add(new PointD(xInt, yInt1));

                    intersections.Add(new PointD(xInt, yInt2));

                    return true;
                }
            }
            else if (Round(p1.Y - p0.Y, _precision) == 0)
            {
                //line is horizontal
                if (p0.Y < cP.Y - r || p0.Y > cP.Y + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double yInt = p0.Y;

                    double xInt1 = Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    double xInt2 = -Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    intersections.Add(new PointD(xInt1, yInt));

                    intersections.Add(new PointD(xInt2, yInt));

                    return true;
                }
            }
            else
            {
                double ms = (p1.Y - p0.Y) / (p1.X - p0.X);

                double bs = p0.Y - ms * p0.X;

                double a = cP.X;

                double b = cP.Y;

                double a1 = (-1.0 - Pow(ms, 2));

                double b1 = (2.0 * a - 2.0 * ms * bs + 2.0 * ms * b);

                double c1 = (Pow(r, 2) + 2.0 * bs * b - Pow(a, 2) - Pow(bs, 2) - Pow(b, 2));

                double xInt1 = (-b1 + Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double xInt2 = (-b1 - Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double yInt1 = ms * xInt1 + bs;

                double yInt2 = ms * xInt2 + bs;

                if (xInt1.IsNanOrInfinity() == false && yInt1.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt1, yInt1));
                }

                if (xInt2.IsNanOrInfinity() == false && yInt2.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt2, yInt2));
                }

                if (intersections.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override List<LineSegment> Edges()
        {
            return new List<LineSegment>();
        }

        public override List<PointD> QuadrantPoints()
        {
            double outerRadius = TypeOfPT.OuterDiameter / 2.0;

            List<PointD> pointList = new List<PointD>(4);

            DrawingNode centerPoint = CenterPoint();

            pointList.Add(new PointD(centerPoint.Point.X - outerRadius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y + outerRadius));
            pointList.Add(new PointD(centerPoint.Point.X + outerRadius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y - outerRadius));

            return pointList;
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //PT can't be joine with other elements
            newElement = null;
            return false;
        }

        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            DrawingNode currentCenterNode = CenterPoint();

            PointD copiedCenterPoint = new PointD(currentCenterNode.Point.X + delta_X, currentCenterNode.Point.Y + delta_Y);

            DrawingNode centerNode = new DrawingNode(copiedCenterPoint, NodeType.CenterPoint);

            PostTensioningG copiedPostTensioning = new PostTensioningG(copiedCenterPoint, TypeOfPT, Layer, InitialPrestress, JackingAndGrouting, MomentAtGrouting)
            {
                StrainAtGrouting = StrainAtGrouting,
                FillInnerStrands = FillInnerStrands,
            };

            return copiedPostTensioning;
        }

        /// <summary>
        /// Returns true if the drawing element is fully contained within the given bounding rectangle
        /// </summary>
        /// <param name="rectangle">Bounding rectangle</param>
        /// <returns></returns>
        public override bool IsContainedWithin(Rect rectangle)
        {
            bool containsAllPoints = true;

            List<PointD> pointsToCheck = QuadrantPoints();

            for (int i = 0; i < pointsToCheck.Count; i++)
            {
                PointD qp = pointsToCheck[i];

                if (rectangle.Contains(qp.X, qp.Y) == false)
                {
                    containsAllPoints = false;
                    break;
                }
            }

            return containsAllPoints;
        }

        public override List<IDrawingElement> Explode()
        {
            return new List<IDrawingElement>();
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            double r = TypeOfPT.OuterDiameter / 2.0;

            PointD cp = CenterPoint().Point;

            double distToCenter = p.DistanceTo(cp);

            if (Abs(distToCenter - r) <= orthogonalTolerance)
            {
                orthogonalDistance = Abs(distToCenter - r);

                return true;
            }
            else
            {
                orthogonalDistance = -1;

                return false;
            }
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                intersects = ((CircleG)other).IntersectsWithPostTensioning(this, out intersections);
            }
            if (other.GetType().Equals(typeof(PostTensioningG)) == true)
            {
                intersects = IntersectsWithPostTensioning((PostTensioningG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(ArcG)) == true)
            {
                intersects = ((ArcG)other).IntersectsWithPostTensioning(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                intersects = ((PolygonG)other).IntersectsWithPostTensioning(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                intersects = ((PolylineG)other).IntersectsWithPostTensioning(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PointG)) == true)
            {
                intersects = ((PointG)other).IntersectsWithElement(this, out intersections);
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            return intersects;
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            DrawingNode n0 = CenterPoint();

            DrawingNode n1 = new DrawingNode(basePoint, NodeType.EndPoint);

            LineSegment line = new LineSegment(n0, n1);

            if (IntersectsWithInfiniteLine(line, out intersections) == true)
            {
                return true;
            }

            return false;
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            PostTensioningSectionFS ptFS = new PostTensioningSectionFS(this.CenterPoint().Point, this.TypeOfPT, this.InitialPrestress, this.JackingAndGrouting);

            return ptFS;
        }
        #endregion

        #region Public Methods
        public DrawingNode CenterPoint()
        {
            foreach (DrawingNode node in Nodes)
            {
                if (node.TypeOfNode.Equals(NodeType.CenterPoint) == true)
                {
                    return node;
                }
            }

            throw new Exception("No center point has been ddefined.");
        }

        public double GetInitialPrestrain(double initialPrestress)
        {
            double prestrain = Material.MonotonicStrainStress(-1 * initialPrestress);

            return prestrain;

            //throw new NotImplementedException();
        }

        public bool IntersectsWithPostTensioning(PostTensioningG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Protected Methods
        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.PostTensioning,
                ElementPropertyType.UserProperties
            };
        }
        #endregion

        #region OpenGL
        public override List<Vector2> GenerateOpenGLVertices()
        {
            int numLineSegments = 50;

            int listSize = (numLineSegments + 1) * 2;

            DrawingNode cp = CenterPoint();

            List<CircleG> circleList = TypeOfPT.PTSection.GetWireCirclesG(cp.Point);

            List<Vector2> vbo = new List<Vector2>(listSize * (circleList.Count + 1));

            DrawingNode cP = CenterPoint();

            //Outer circle
            for (int i = 1; i <= numLineSegments; i++)
            {
                double theta = (2 * PI * (i - 1) / numLineSegments);

                float x = (float)(cP.Point.X + Radius * Cos(theta));
                float y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));

                theta = (2 * PI * i / numLineSegments);

                x = (float)(cP.Point.X + Radius * Cos(theta));
                y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));
            }

            //Inner circles
            for (int j = 0; j < circleList.Count; j++)
            {
                CircleG circle = circleList[j];

                cP = circle.CenterPoint();
                double cR = circle.Radius;

                for (int i = 1; i <= numLineSegments; i++)
                {
                    double theta = (2 * PI * (i - 1) / numLineSegments);

                    float x = (float)(cP.Point.X + cR * Cos(theta));
                    float y = (float)(cP.Point.Y + cR * Sin(theta));

                    vbo.Add(new Vector2(x, y));

                    theta = (2 * PI * i / numLineSegments);

                    x = (float)(cP.Point.X + cR * Cos(theta));
                    y = (float)(cP.Point.Y + cR * Sin(theta));

                    vbo.Add(new Vector2(x, y));
                }
            }

            return vbo;
        }

        public override List<Vector2> GenerateOpenGLSelectionVertices()
        {
            DrawingNode cp = CenterPoint();

            List<PointD> quadPoints = QuadrantPoints();

            List<Vector2> verts = new List<Vector2>(5);

            for(int i = 0; i < quadPoints.Count; i++)
            {
                PointD p = quadPoints[i];

                verts.Add(new Vector2((float)p.X, (float)p.Y));
            }

            verts.Add(new Vector2((float)cp.Point.X, (float)cp.Point.Y));

            return verts;
        }
        #endregion
    }
}
