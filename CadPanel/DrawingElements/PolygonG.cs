﻿using System;
using System.Collections.Generic;
using System.Linq;
using CadPanel.Common;
using static System.Math;
using System.Drawing;
using static CadPanel.Common.CommonMethods;
using PolyBoolCS;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using CadPanel.Extensions;
using Utilities.DataStructures.QuadTree;
using FiberSectionAnalysis.Geometry;
using CadPanel.OpenGL;

using OpenTK;

using Point = System.Drawing.Point;
using Color = System.Drawing.Color;

namespace CadPanel.DrawingElements
{
    public class PolygonG : ClosedDrawingElement
    {
        
        #region Constructors
        public PolygonG() : base()
        {
            Name = "Polygon";
            _elementType = DrawingElementType.Polygon;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Retunrs the area of the polygon
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return Abs(SignedArea());
        }

        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Point[] pixelPoints = new Point[Nodes.Count];

            for (int i = 0; i < Nodes.Count; i++)
            {
                pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
            }

            if (pixelPoints.GetUpperBound(0) >= 1)
            {
                if (FillShape == true)
                {
                    Color? fillBrushColor = (Selected == false) ? FillColor : instanceData.SelectedObjectLineColor;

                    if (fillBrushColor == null)
                    {
                        if (Selected == false)
                        {
                            fillBrushColor = Layer.Color;
                        }
                        else
                        {
                            fillBrushColor = instanceData.SelectedObjectLineColor;
                        }
                    }

                    SolidBrush fillBrush = new SolidBrush(fillBrushColor.Value);

                    g.FillPolygon(fillBrush, pixelPoints);

                    fillBrush.Dispose();
                }

                g.DrawPolygon(myPen, pixelPoints);

                if (Selected == true)
                {
                    if (numSelectedElements == 1 || (numSelectedElements > 1 && instanceData.DrawVerticesOfMultipleObjects == true))
                    {
                        SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                        for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                        {
                            Rectangle boundingRectangle = new Rectangle
                                (
                                    Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                                    Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                                    instanceData.SelectedObjectPointWidth,
                                    instanceData.SelectedObjectPointWidth
                                );

                            g.FillEllipse(myBrush, boundingRectangle);
                        }

                        myBrush.Dispose();
                    }                    
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            //Do nothing
        }

        /// <summary>
        /// Creates a copy of the element given a copy base point and a current paste point
        /// </summary>
        /// <param name="basePoint">The copy base point</param>
        /// <param name="currentPoint">The point at which to create the copy</param>
        /// <returns></returns>
        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>();

            for (int i = 0; i< Nodes.Count - 1; i++)
            {
                DrawingNode node = Nodes[i];

                PointD copiedPoint = new PointD(node.Point.X + delta_X, node.Point.Y + delta_Y);

                DrawingNode copiedNode = new DrawingNode(copiedPoint, node.TypeOfNode);

                copiedNodes.Add(copiedNode);
            }

            copiedNodes.Add(copiedNodes[0]);

            PolygonG copiedPolygon = new PolygonG()
            {
                Nodes = copiedNodes,
                FillColor = FillColor,
                FillShape = FillShape,
                Hole = Hole,
                IsRebar = IsRebar,
                Layer = Layer,
                Material = Material,
                Name = Name,
                Rebar = Rebar,
                Selected = false
            };

            return copiedPolygon;
        }

        /// <summary>
        /// Returns the bounding rectangle for the element, for use in the quad tree
        /// </summary>
        /// <returns></returns>
        public override Rect BoundingRectangle()
        {
            double minX = 0, minY = 0, maxX = 0, maxY = 0;

            int count = 0;

            foreach (DrawingNode node in Nodes)
            {
                if (count == 0)
                {
                    minX = node.Point.X;
                    minY = node.Point.Y;
                    maxX = node.Point.X;
                    maxY = node.Point.Y;
                }
                else
                {
                    if (node.Point.X < minX)
                    {
                        minX = node.Point.X;
                    }

                    if (node.Point.Y < minY)
                    {
                        minY = node.Point.Y;
                    }

                    if (node.Point.X > maxX)
                    {
                        maxX = node.Point.X;
                    }

                    if (node.Point.Y > maxY)
                    {
                        maxY = node.Point.Y;
                    }
                }

                count++;
            }

            int x0 = Convert.ToInt32(Floor(minX));
            int y0 = Convert.ToInt32(Floor(minY));

            int width = Convert.ToInt32(Ceiling(maxX - x0));
            int height = Convert.ToInt32(Ceiling(maxY - y0));

            return new Rect(x0, y0, width, height);
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {
            if (nodeType.Equals(NodeType.EndPoint) == true)
            {
                double closestDist = 1.0E10;

                PointD closestPoint = null;

                foreach (DrawingNode node in Nodes)
                {
                    if (globalPoint.DistanceTo(node.Point) <= closestDist)
                    {
                        closestDist = globalPoint.DistanceTo(node.Point);
                        closestPoint = node.Point;
                    }
                }

                return closestPoint;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Moves the drawing element
        /// </summary>
        /// <param name="basePoint">Base point of move command</param>
        /// <param name="currentPoint">Final point of move command</param>
        public override void Move(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            //Loop through all elements except for the last one
            //The last node is set equal to the first node (reference)
            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                DrawingNode node = Nodes[i];
                node.Point.X += delta_X;
                node.Point.Y += delta_Y;
            }

            UpdateOpenGLGeometry();
        }

        /// <summary>
        /// Returns a list of the edges that make up the element
        /// </summary>
        /// <returns></returns>
        public override List<LineSegment> Edges()
        {
            List<LineSegment> segments = new List<LineSegment>();

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                LineSegment seg = new LineSegment(Nodes[i], Nodes[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        public override void RotateThroughAngle(PointD basePoint, double angle)
        {
            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                DrawingNode node = Nodes[i];

                double distToBasePoint = node.Point.DistanceTo(basePoint);

                if (distToBasePoint > 0)
                {
                    double oldAngle = node.Point.GetAngleFromPoint(basePoint);

                    double newAngle = oldAngle + angle;

                    if (newAngle > 2.0 * PI)
                    {
                        newAngle -= 2.0 * PI;
                    }

                    double newX = basePoint.X + distToBasePoint * Cos(newAngle);
                    double newY = basePoint.Y + distToBasePoint * Sin(newAngle);

                    node.Point.X = newX;
                    node.Point.Y = newY;
                }
            }

            UpdateOpenGLGeometry();
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            List<LineSegment> edgeList = Edges();

            for (int i = 0; i < edgeList.Count; i++)
            {
                if (edgeList[i].IntersectsWith(line, out PointD inters) == true)
                {
                    bool unique = true;

                    for (int j = 0; j < intersections.Count; j++)
                    {
                        if (intersections[j] == inters)
                        {
                            unique = false;
                            break;
                        }
                    }

                    if (unique == true)
                    {
                        intersections.Add(inters);
                    }

                    intersects = true;
                }
            }

            return intersects;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                LineSegment seg = edges[i];

                if (seg.IntersectsWithInfiniteLine(line, out PointD infIntersection) == true)
                {
                    bool unique = true;

                    for (int j = 0; j < intersections.Count; j++)
                    {
                        if (intersections[j] == infIntersection)
                        {
                            unique = false;

                            break;
                        }
                    }

                    if (unique == true)
                    {
                        intersections.Add(infIntersection);
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //Polygons can't be joine with other elements (they are already closed)
            newElement = null;
            return false;
        }

        public override List<IDrawingElement> Explode()
        {
            List<IDrawingElement> explodedElements = new List<IDrawingElement>();

            List<LineSegment> edges = Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                DrawingNode n0 = edges[i].StartNode.CreateCopy();

                DrawingNode n1 = edges[i].EndNode.CreateCopy();

                PolylineG pl = new PolylineG()
                {
                    Nodes = new List<DrawingNode>(2) { n0, n1 },
                    Layer = Layer,
                    IsRebar = IsRebar,
                    Rebar = Rebar,
                    Selected = false
                };

                explodedElements.Add(pl);
            }

            return explodedElements;
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            List<LineSegment> edges = Edges();

            double minDist = orthogonalTolerance * 10.0;

            bool liesOnBoundary = false;

            for (int i = 0; i < edges.Count; i++)
            {
                if (p.LiesOnLine(edges[i], out double distToSeg, orthogonalTolerance) == true)
                {
                    liesOnBoundary = true;

                    minDist = Min(minDist, distToSeg);
                }
            }

            if (liesOnBoundary == false)
            {
                orthogonalDistance = -1;
            }
            else
            {
                orthogonalDistance = minDist;
            }

            return liesOnBoundary;
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                intersects = ((CircleG)other).IntersectsWithPolygon(this, out intersections);
            }
            if (other.GetType().Equals(typeof(PostTensioningG)) == true)
            {
                intersects = IntersectsWithPostTensioning((PostTensioningG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(ArcG)) == true)
            {
                intersects = ((ArcG)other).IntersectsWithPolygon(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                intersects = IntersectsWithPolygon((PolygonG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                intersects = IntersectsWithPolyline((PolylineG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PointG)) == true)
            {
                intersects = ((PointG)other).IntersectsWithElement(this, out intersections);
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            return intersects;
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                LineSegment edge = edges[i];

                PointD perpPoint = edge.GetClosestPerpendicularPointToInfiniteExtension(basePoint);

                DrawingNode n0 = new DrawingNode(basePoint, NodeType.EndPoint);

                DrawingNode n1 = new DrawingNode(perpPoint, NodeType.EndPoint);

                LineSegment perpLine = new LineSegment(n0, n1);

                if (edge.IntersectsWithInfiniteLine(perpLine, out PointD edgeIntersection) == true)
                {
                    intersections.Add(edgeIntersection);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Offsets the element by the offset distance
        /// </summary>
        /// <param name="offsetDistance">Offset distance</param>
        /// <param name="direction">For interior offset, specify 1; for exterior offset, specify -1</param>
        /// <returns></returns>
        public override IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            int direction = 1;

            if (PointLiesWithin(globalPoint) == false)
            {
                direction = -1;
            }

            double tolerance = 1.0 * Pow(10, -_precision);

            List<PointD> pointList = Nodes.Select(x => x.Point).ToList(); ;

            pointList.RemoveAt(pointList.Count - 1);    //remove the last point because this method was written
                                                        //for polygons that are not closed (i.e. the last point
                                                        //is not equal to the first point)

            List<PointD> offsetPointList = new List<PointD>(pointList.Count);

            for (int i = 0; i < pointList.Count; i++)
            {
                PointD prevPoint;
                PointD point;
                PointD nextPoint;

                int prevIndex = 0;
                int index = 0;
                int nextIndex = 0;

                if (i == 0)
                {
                    prevIndex = pointList.Count - 1;
                    index = i;
                    nextIndex = i + 1;
                }
                else if (i > 0 && i < pointList.Count - 1)
                {
                    prevIndex = i - 1;
                    index = i;
                    nextIndex = i + 1;
                }
                else
                {
                    prevIndex = i - 1;
                    index = i;
                    nextIndex = 0;
                }

                prevPoint = pointList[prevIndex];
                point = pointList[index];
                nextPoint = pointList[nextIndex];

                double angle1 = point.GetAngleTo(nextPoint);
                double angle2 = point.GetAngleTo(prevPoint);

                double avgAngle = (angle1 + angle2) / 2d;

                double angleDiff = angle2 - angle1;

                double halfTheta = angleDiff / 2d;

                double totalDist = Abs(offsetDistance / Sin(halfTheta));

                PointD newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));

                Vector pToNewP = new Vector(newPoint.X - point.X, newPoint.Y - point.Y);
                Vector pToNewPuv = pToNewP.UnitVector();

                PointD perturbedPoint = new PointD(point.X + pToNewPuv.X * 0.1, point.Y + pToNewPuv.Y * 0.1);

                if (direction == 1)
                {
                    //offset inside
                    if (WindingNumberInside(perturbedPoint, this) != 0)
                    {
                        //if the new point is inside, then the new point has been offset inside the original shape
                    }
                    else
                    {
                        //The point was accidentally offset to the outside, so flip the average angle (subtract 180 degrees, or PI in this case)
                        avgAngle = avgAngle - PI;
                        newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));
                    }
                }
                else if (direction == -1)
                {
                    //offset outside
                    if (WindingNumberInside(perturbedPoint, this) == 0)
                    {
                        //If the new point is outside, then the new point was offset outside correctly
                    }
                    else
                    {
                        //The point was accidentally offset to the outside, so flip the average angle (subtract 180 degrees, or PI in this case)
                        avgAngle = avgAngle - PI;
                        newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));
                    }
                }

                offsetPointList.Add(newPoint);
            }

            List<LineSegment> edgeList = new List<LineSegment>();

            for (int i = 0; i < offsetPointList.Count; i++)
            {
                PointD point;
                PointD nextPoint;

                int index = 0;
                int nextIndex = 0;

                if (i >= 0 && i < offsetPointList.Count - 1)
                {
                    index = i;
                    nextIndex = i + 1;
                }
                else
                {
                    index = i;
                    nextIndex = 0;
                }

                point = offsetPointList[index];
                nextPoint = offsetPointList[nextIndex];

                PointD origPoint = pointList[index];
                PointD origNextPoint = pointList[nextIndex];

                Vector unitVectorOrig = new Vector(origNextPoint.X - origPoint.X, origNextPoint.Y - origPoint.Y);
                unitVectorOrig = unitVectorOrig.UnitVector();

                Vector unitVectorOffset = new Vector(nextPoint.X - point.X, nextPoint.Y - point.Y);
                unitVectorOffset = unitVectorOffset.UnitVector();

                if (Sign(Round(unitVectorOrig.X, 4)) == Sign(Round(unitVectorOffset.X, 4)) && Sign(Round(unitVectorOrig.Y, 4)) == Sign(Round(unitVectorOffset.Y, 4)))
                {
                    edgeList.Add(new LineSegment(new DrawingNode(point, NodeType.EndPoint), new DrawingNode(nextPoint, NodeType.EndPoint)));
                }
            }

            for (int i = 0; i < edgeList.Count; i++)
            {
                LineSegment nextEdge;
                LineSegment prevEdge;

                int prevEdgeIndex = 0;
                int nextEdgeIndex = 0;

                if (i == 0)
                {
                    prevEdge = edgeList[edgeList.Count - 1];
                    nextEdge = edgeList[i + 1];

                    prevEdgeIndex = edgeList.Count - 1;
                    nextEdgeIndex = i + 1;
                }
                else if (i == edgeList.Count - 1)
                {
                    prevEdge = edgeList[i - 1];
                    nextEdge = edgeList[0];

                    prevEdgeIndex = i - 1;
                    nextEdgeIndex = 0;
                }
                else
                {
                    prevEdge = edgeList[i - 1];
                    nextEdge = edgeList[i + 1];

                    prevEdgeIndex = i - 1;
                    nextEdgeIndex = i + 1;
                }

                if (edgeList[i].IntersectsWith(nextEdge, out PointD intersectionNext) == true)
                {
                    if (edgeList[i].StartNode.Point.DistanceTo(intersectionNext) <= tolerance)
                    {
                        //The intersection is the first point of the current line, this shouldn't happen
                        //throw new Exception("Line intersection of the current edge and the next edge was found at the first point of the current edge.");
                    }
                    else if (edgeList[i].EndNode.Point.DistanceTo(intersectionNext) <= tolerance)
                    {
                        //This means we are good, do nothing
                    }
                    else
                    {
                        //the intersection is somewhere in the line
                        edgeList[i] = new LineSegment(edgeList[i].StartNode, new DrawingNode(intersectionNext, NodeType.EndPoint));
                        edgeList[nextEdgeIndex] = new LineSegment(new DrawingNode(intersectionNext, NodeType.EndPoint), edgeList[nextEdgeIndex].EndNode);
                    }
                }

                if (edgeList[i].IntersectsWith(prevEdge, out PointD intersectionPrev) == true)
                {
                    if (edgeList[i].StartNode.Point.DistanceTo(intersectionPrev) <= tolerance)
                    {
                        //This means we are good, do nothing
                    }
                    else if (edgeList[i].EndNode.Point.DistanceTo(intersectionPrev) <= tolerance)
                    {
                        //The intersection is the first point of the current line, this shouldn't happen
                        //throw new Exception("Line intersection of the current edge and the next edge was found at the first point of the current edge.");
                    }
                    else
                    {
                        //the intersection is somewhere in the line
                        edgeList[i] = new LineSegment(new DrawingNode(intersectionPrev, NodeType.EndPoint), edgeList[i].EndNode);
                        edgeList[prevEdgeIndex] = new LineSegment(edgeList[nextEdgeIndex].StartNode, new DrawingNode(intersectionPrev, NodeType.EndPoint));
                    }
                }
            }

            if (edgeList.Count > 0)
            {
                List<DrawingNode> checkedOffsetPolygon = new List<DrawingNode>();

                foreach (LineSegment seg in edgeList)
                {
                    checkedOffsetPolygon.Add(seg.StartNode);
                }

                checkedOffsetPolygon.Add(checkedOffsetPolygon[0]); //Add the last node as the first node

                PolygonG offsetPolygon = new PolygonG()
                {
                    Nodes = checkedOffsetPolygon,
                    Layer = Layer,
                    IsRebar = IsRebar,
                    Rebar = Rebar,
                    Hole = Hole,
                    FillColor = FillColor,
                    Material = Material,
                };

                return offsetPolygon;
            }
            else
            {
                return null;
            }
            
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            return this;
        }

        public override PointD Centroid()
        {
            double sumX = 0;
            double sumY = 0;

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                PointD p0 = Nodes[i].Point;
                PointD p1 = Nodes[i + 1].Point;

                sumX += (p0.X + p1.X) * (p0.X * p1.Y - p1.X * p0.Y);
                sumY += (p0.Y + p1.Y) * (p0.X * p1.Y - p1.X * p0.Y);
            }

            double area = SignedArea();

            double cx = 1.0 / (6.0 * area) * sumX;
            double cy = 1.0 / (6.0 * area) * sumY;

            return new PointD(cx, cy);
        }

        public override double Perimeter()
        {
            List<LineSegment> edges = Edges();

            double perimeter = 0;

            for (int i = 0; i < edges.Count; i++)
            {
                perimeter += edges[i].Length();
            }

            return perimeter;
        }

        public override double Ixx()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                DrawingNode n0 = Nodes[i];
                DrawingNode n1 = Nodes[i + 1];

                double x0 = n0.Point.X;
                double y0 = n0.Point.Y;

                double x1 = n1.Point.X;
                double y1 = n1.Point.Y;

                total += (x0 * y1 - x1 * y0) * (Pow(x0, 2) + x0 * x1 + Pow(x1, 2));
            }

            total /= 12.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            double d = centroid.X;

            total -= a * d * d;

            return Abs(total);
        }

        public override double Iyy()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                DrawingNode n0 = Nodes[i];
                DrawingNode n1 = Nodes[i + 1];

                double x0 = n0.Point.X;
                double y0 = n0.Point.Y;

                double x1 = n1.Point.X;
                double y1 = n1.Point.Y;

                total += (x0 * y1 - x1 * y0) * (Pow(y0, 2) + y0 * y1 + Pow(y1, 2));
            }

            total /= 12.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            double d = centroid.Y;

            total -= a * d * d;

            return Abs(total);
        }

        public override double Ixy()
        {
            //https://en.wikipedia.org/wiki/Second_moment_of_area
            double total = 0;

            //Loop over all of the nodes (0 -> n), where the node n + 1 is equal to the first node
            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                DrawingNode n0 = Nodes[i];
                DrawingNode n1 = Nodes[i + 1];

                double x0 = n0.Point.X;
                double y0 = n0.Point.Y;

                double x1 = n1.Point.X;
                double y1 = n1.Point.Y;

                total += (x0 * y1 - x1 * y0) * (x0 * y1 + 2.0 * x0 * y0 + 2.0 * x1 * y1 + x1 * y0);
            }

            total /= 24.0;

            //Now perform parallel axis theorem
            double a = Area();

            PointD centroid = Centroid();

            total -= a * centroid.X * centroid.Y;

            return Abs(total);
        }

        public override double Sxx()
        {
            //Get maximum and minimum y coordinate
            double minY = 0;
            double maxY = 0;

            for (int i = 0; i < Nodes.Count; i++)
            {
                if (i == 0)
                {
                    minY = Nodes[i].Point.Y;
                    maxY = Nodes[i].Point.Y;
                }
                else
                {
                    minY = Min(minY, Nodes[i].Point.Y);
                    maxY = Max(maxY, Nodes[i].Point.Y);
                }
            }

            double ix = Ixx();
            PointD centroid = Centroid();

            return ix / Max(Abs(centroid.Y - minY), Abs(centroid.Y - maxY));
        }

        public override double Syy()
        {
            //Get maximum and minimum x coordinate
            double minX = 0;
            double maxX = 0;

            for (int i = 0; i < Nodes.Count; i++)
            {
                if (i == 0)
                {
                    minX = Nodes[i].Point.X;
                    maxX = Nodes[i].Point.X;
                }
                else
                {
                    minX = Min(minX, Nodes[i].Point.X);
                    maxX = Max(maxX, Nodes[i].Point.X);
                }
            }

            double iy = Iyy();
            PointD centroid = Centroid();

            return iy / Max(Abs(centroid.X - minX), Abs(centroid.X - maxX));
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            PolygonFS pgFS = new PolygonFS(this.Nodes.Select(x => x.Point).ToList(), this.Material);
            pgFS.Hole = this.Hole;
            pgFS.IsRebar = this.IsRebar;

            return pgFS;
        }
        #endregion

        #region Public Methodds
        public double SignedArea()
        {
            double sum = 0;

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                PointD p0 = Nodes[i].Point;
                PointD p1 = Nodes[i + 1].Point;
                sum += p0.X * p1.Y - p1.X * p0.Y;
            }

            return sum / 2.0;
        }

        public static bool operator ==(PolygonG pg1, PolygonG pg2)
        {
            if (pg1.Equals(pg2) == true)
            {
                return true;
            }

            if (object.ReferenceEquals(pg1, null))
            {
                return object.ReferenceEquals(pg2, null);
            }

            if (object.ReferenceEquals(pg2, null))
            {
                return object.ReferenceEquals(pg1, null);
            }

            bool same = true;

            if (pg1.Nodes.Count == pg2.Nodes.Count)
            {
                for (int i = 0; i < pg1.Nodes.Count; i++)
                {
                    if (pg1.Nodes[i].Point != pg2.Nodes[i].Point)
                    {
                        same = false;
                        break;
                    }
                }
            }
            else
            {
                same = false;
            }

            if (same == false)
            {
                List<PointD> pg1_R = new List<PointD>(); //resampled
                List<PointD> pg2_R = new List<PointD>(); //resampled

                for (int i = 0; i < pg1.Nodes.Count; i++)
                {
                    pg1_R.Add(pg1.Nodes[i].Point);
                }

                for (int i = 0; i < pg2.Nodes.Count; i++)
                {
                    pg2_R.Add(pg2.Nodes[i].Point);
                }

                int count = 0;

                while (count <= pg1_R.Count - 3)
                {
                    PointD p1 = pg1_R[count];
                    PointD p2 = pg1_R[count + 1];
                    PointD p3 = pg1_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg1_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg1_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                count = 0;

                while (count <= pg2_R.Count - 3)
                {
                    PointD p1 = pg2_R[count];
                    PointD p2 = pg2_R[count + 1];
                    PointD p3 = pg2_R[count + 2];

                    double m12, m23;

                    if (p1.X == p2.X)
                    {
                        if (p2.X == p3.X)
                        {
                            //slopes are the same, remove point 2
                            pg2_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.Y - p1.Y) / (p2.X - p1.X);
                        if (p2.X != p3.X)
                        {
                            m23 = (p3.Y - p2.Y) / (p3.X - p2.X);
                            if (m12 == m23)
                            {
                                pg2_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                //Now both polygons are resampled

                if (pg1_R.Count != pg2_R.Count)
                {
                    return false;
                }
                else
                {
                    int numSame = 0;

                    foreach (PointD p1 in pg1_R)
                    {
                        foreach (PointD p2 in pg2_R)
                        {
                            if (p1 == p2)
                            {
                                numSame++;
                                break;
                            }
                        }
                    }

                    if (numSame == pg1_R.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                return true;
            }
        }

        public static bool operator !=(PolygonG pg1, PolygonG pg2)
        {
            if (pg1 == pg2)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IntersectsWithPostTensioning(PostTensioningG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = this.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (other.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool IntersectsWithPolygon(PolygonG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = this.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (other.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool IntersectsWithPolyline(PolylineG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = this.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (other.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool PointLiesWithin(PointD p)
        {
            if (WindingNumberInside(p, this) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool PointLiesWithin(DrawingNode n)
        {
            return PointLiesWithin(n.Point);
        }

        public PointD GetCentroid()
        {
            // use Archimedes.geometry formulation
            double sumX = 0;
            double sumY = 0;

            var vertices = Nodes.Select(x=>x.Point).ToList();
            vertices.RemoveAt(vertices.Count - 1);  //remove the last index (which is equal to the first index)
                                                    //because this formulation is developed for a polygon that
                                                    //is not closed (i.e. the last point is not equal to the first)

            for (int i = 0; i < vertices.Count; i++)
            {
                PointD p0 = vertices[i];
                PointD p1;

                if (i < vertices.Count - 1)
                {
                    p1 = vertices[i + 1];
                }
                else
                {
                    p1 = vertices[0];
                }

                sumX += (p0.X + p1.X) * (p0.X * p1.Y - p1.X * p0.Y);

                sumY += (p0.Y + p1.Y) * (p0.X * p1.Y - p1.X * p0.Y);
            }

            double area = SignedArea();

            double cx = 1.0 / (6.0 * area) * sumX;

            double cy = 1.0 / (6.0 * area) * sumY;

            return new PointD(cx, cy);
        }

        public Polygon ConvertToPolyBoolPolygon()
        {
            List<PointD> vertices = Nodes.Select(x => x.Point).ToList();
            vertices.RemoveAt(vertices.Count - 1); //The methods were written for polygons that are not closed

            var convertedVertices = PolyBool.PolyBoolHelperMethods.ConvertToPolyBoolPoints(vertices);

            return PolyBoolHelper.ConvertToPolygon(convertedVertices.ToList());
        }

        public void MatchProperties(PolygonG other)
        {
            Layer = other.Layer;
            IsRebar = other.IsRebar;
            Rebar = other.Rebar;
            Hole = other.Hole;
            FillColor = other.FillColor;
        }

        /// <summary>
        /// Determines whether the polygon lies completely within another closed shape
        /// </summary>
        /// <param name="other">Other closed shape</param>
        /// <returns></returns>
        public override bool IsFullyWithin(ClosedDrawingElement other)
        {
            bool inside = true;

            if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                PolygonG poly = (PolygonG)other;

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (WindingNumberInside(Nodes[i].Point, poly) == 0)
                    {
                        inside = false;

                        break;
                    }
                }
            }
            else if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                CircleG circle = (CircleG)other;

                DrawingNode cP = circle.CenterPoint();

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].DistanceTo(cP) > circle.Radius)
                    {
                        inside = false;

                        break;
                    }
                }
            }

            return inside;
        }

        public override bool ContainsPoint(PointD p)
        {
            if (WindingNumberInside(p, this) != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Protected Methods
        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.ClosedGeometry,
                ElementPropertyType.Rebar,
                ElementPropertyType.Material,
                ElementPropertyType.UserProperties
            };
        }


        #endregion

        #region OpenGL

        public override List<Vector2> GenerateOpenGLVertices()
        {
            if (Nodes.Count > 1)
            {
                int listSize = ((Nodes.Count - 2) * 2 + 2);

                List<Vector2> vbo = new List<Vector2>(listSize);
                List<float> cbo = new List<float>(listSize);

                for (int i = 0; i < Nodes.Count - 1; i++)
                {
                    float x = (float)Nodes[i].Point.X;
                    float y = (float)Nodes[i].Point.Y;

                    vbo.Add(new Vector2(x, y));

                    x = (float)Nodes[i + 1].Point.X;
                    y = (float)Nodes[i + 1].Point.Y;

                    vbo.Add(new Vector2(x, y));
                }

                return vbo;
            }
            else
            {
                return null;
            }
        }

        public override List<Vector2> GenerateOpenGLSelectionVertices()
        {
            if (Nodes != null)
            {
                List<Vector2> verts = new List<Vector2>(Nodes.Count);

                for (int i = 0; i < Nodes.Count - 1; i++)
                {
                    float x = (float)Nodes[i].Point.X;
                    float y = (float)Nodes[i].Point.Y;

                    Vector2 v = new Vector2(x, y);

                    verts.Add(v);
                }

                return verts;
            }
            else
            {
                return new List<Vector2>();
            }
        }

        public override void GetOpenGLBuffers(CadPanelInstanceData instanceData, out List<Vector2> vertexBuffer, out List<float> colorBuffer)
        {
            if (OpenGLVertices == null)
            {
                int listSize = ((Nodes.Count - 2) * 2 + 2);

                List<Vector2> vbo = new List<Vector2>(listSize);
                List<float> cbo = new List<float>(listSize);

                for (int i = 0; i < Nodes.Count - 1; i++)
                {
                    float x = (float)Nodes[i].Point.X;
                    float y = (float)Nodes[i].Point.Y;

                    vbo.Add(new Vector2(x, y));

                    x = (float)Nodes[i + 1].Point.X;
                    y = (float)Nodes[i + 1].Point.Y;

                    vbo.Add(new Vector2(x, y));

                    Color c = Layer.Color;

                    if (Selected == true)
                    {
                        c = instanceData.SelectedObjectLineColor;
                    }

                    float r = c.R / 255f;
                    float g = c.G / 255f;
                    float b = c.B / 255f;

                    cbo.Add(r);
                    cbo.Add(g);
                    cbo.Add(b);

                    cbo.Add(r);
                    cbo.Add(g);
                    cbo.Add(b);

                    OpenGLVertices = vbo;
                }
            }

            vertexBuffer = OpenGLVertices;
            colorBuffer = null;
            
        }
        #endregion
    }
}
