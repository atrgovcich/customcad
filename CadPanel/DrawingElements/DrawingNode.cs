﻿using Utilities.Geometry;
using Newtonsoft.Json;

namespace CadPanel.DrawingElements
{
    public class DrawingNode
    {
        public PointD Point { get; set; }
        public NodeType TypeOfNode { get; set; }

        #region Constructors
        [JsonConstructor]
        public DrawingNode()
        {

        }

        public DrawingNode(PointD p, NodeType type)
        {
            Point = p;
            TypeOfNode = type;
        }
        #endregion

        #region Public Methods
        public DrawingNode CreateCopy()
        {
            PointD copyPoint = new PointD(Point.X, Point.Y);

            return new DrawingNode(copyPoint, TypeOfNode);
        }

        public DrawingNode CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            PointD copyPoint = new PointD(Point.X + delta_X, Point.Y + delta_Y);

            return new DrawingNode(copyPoint, TypeOfNode);
        }

        public double DistanceTo(DrawingNode other)
        {
            return this.Point.DistanceTo(other.Point);
        }

        #endregion
    }
}
