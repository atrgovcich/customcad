﻿using System.ComponentModel;

namespace CadPanel.DrawingElements
{
    public enum NodeType
    {
        [Description("Start Point")]
        StartPoint,
        [Description("End Point")]
        EndPoint,
        [Description("Center Point")]
        CenterPoint,
        [Description("Third Point")]
        ThirdPoint,
        [Description("Quadrant Point")]
        QuadrantPoint
    }
}
