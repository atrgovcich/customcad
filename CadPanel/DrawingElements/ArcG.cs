﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CadPanel.Common;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using Utilities.Extensions.DotNetNative;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using CadPanel.Extensions;
using Utilities.DataStructures.QuadTree;
using Newtonsoft.Json;

using OpenTK;

using Layer = CadPanel.Common.Layer;
using Color = System.Drawing.Color;

namespace CadPanel.DrawingElements
{
    public class ArcG : OpenDrawingElement
    {
        #region Public Properties
        public DrawingNode StartPoint
        {
            get
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.StartPoint) == true)
                    {
                        return Nodes[i];
                    }
                }

                return null;
            }
            set
            {
                value.TypeOfNode = NodeType.StartPoint;

                bool startPointExists = false;

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.StartPoint) == true)
                    {
                        startPointExists = true;

                        Nodes[i] = value;

                        break;
                    }
                }
                
                if (startPointExists == false)
                {
                    Nodes.Add(value);
                }

                if (EndPoint != null && ThirdPoint != null)
                {
                    ComputeProperties();
                }
            }
        }

        public DrawingNode EndPoint
        {
            get
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.EndPoint) == true)
                    {
                        return Nodes[i];
                    }
                }

                return null;
            }
            set
            {
                value.TypeOfNode = NodeType.EndPoint;

                bool endPointExists = false;

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.EndPoint) == true)
                    {
                        endPointExists = true;

                        Nodes[i] = value;

                        break;
                    }
                }

                if (endPointExists == false)
                {
                    Nodes.Add(value);
                }

                if (StartPoint != null && ThirdPoint != null)
                {
                    ComputeProperties();
                }
            }
        }

        public DrawingNode ThirdPoint
        {
            get
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.ThirdPoint) == true)
                    {
                        return Nodes[i];
                    }
                }

                return null;
            }
            set
            {
                value.TypeOfNode = NodeType.ThirdPoint;

                bool thirdPointExists = false;

                for (int i = 0; i < Nodes.Count; i++)
                {
                    if (Nodes[i].TypeOfNode.Equals(NodeType.ThirdPoint) == true)
                    {
                        thirdPointExists = true;

                        Nodes[i] = value;

                        break;
                    }
                }

                if (thirdPointExists == false)
                {
                    Nodes.Add(value);
                }

                if (EndPoint != null && StartPoint != null)
                {
                    ComputeProperties();
                }
            }
        }

        public double Radius
        {
            get
            {
                return _radius;
            }
        }
        #endregion

        #region Protected Fields
        protected double _radius;
        protected double _startAngleRadians;
        protected double _endAngleRadians;
        protected DrawingNode _centerPoint;
        /// <summary>
        /// Angle through which the arc exists (in radians)
        /// </summary>
        protected double _sweepAngle; //in radians
        #endregion

        #region Constructors
        [JsonConstructor]
        public ArcG() : base()
        {
            Name = "Arc";
            _elementType = DrawingElementType.Arc;
        }

        public ArcG(DrawingNode startPoint, DrawingNode endPoint, DrawingNode thirdPoint) : base()
        {
            Nodes = new List<DrawingNode>(3) { startPoint, endPoint, thirdPoint };

            Name = "Arc";

            ComputeProperties();
        }

        public ArcG(List<DrawingNode> nodes, Layer layer) : base()
        {
            Layer = layer;

            Nodes = nodes;

            ComputeProperties();
        }
        #endregion

        #region Public Implemented Methods

        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            if (_centerPoint.Point.X.IsNanOrInfinity() || _centerPoint.Point.Y.IsNanOrInfinity())
            {
                return;
            }

            Point pixel_centerPoint = _centerPoint.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            double startAngleDegrees = _startAngleRadians * 360.0 / (2.0 * PI);

            double sweepAngleDegrees = _sweepAngle * 360.0 / (2.0 * PI);

            int pixel_radius = Convert.ToInt32(_radius * drawingScale);

            if (pixel_centerPoint != null && sweepAngleDegrees != 0)
            {
                g.DrawArc(myPen, pixel_centerPoint.X - pixel_radius, pixel_centerPoint.Y - pixel_radius, pixel_radius * 2, pixel_radius * 2, Convert.ToSingle(startAngleDegrees), Convert.ToSingle(sweepAngleDegrees));
            }

            if (Selected == true)
            {
                if (numSelectedElements == 1 || (numSelectedElements > 1 && instanceData.DrawVerticesOfMultipleObjects == true))
                {
                    Point[] pixelPoints = new Point[Nodes.Count];

                    for (int i = 0; i < Nodes.Count; i++)
                    {
                        pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
                    }

                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                    {
                        Rectangle boundingRectangle = new Rectangle
                            (
                                Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                                Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                                instanceData.SelectedObjectPointWidth,
                                instanceData.SelectedObjectPointWidth
                            );

                        g.FillEllipse(myBrush, boundingRectangle);
                    }

                    myBrush.Dispose();
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            try
            {
                if (StartPoint == null || EndPoint == null)
                {
                    return;
                }

                if (currentPoint == StartPoint.Point || currentPoint == EndPoint.Point)
                {
                    return;
                }

                ThirdPoint = new DrawingNode(currentPoint, NodeType.ThirdPoint);

                ComputeProperties();

                if (_radius.IsNanOrInfinity() || _startAngleRadians.IsNanOrInfinity() || _endAngleRadians.IsNanOrInfinity() || _sweepAngle.IsNanOrInfinity())
                {
                    return;
                }

                Point pixel_centerPoint = _centerPoint.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

                int pixel_radius = Convert.ToInt32(_radius * drawingScale);

                double startAngleDegrees = _startAngleRadians * 360.0 / (2.0 * PI);

                double sweepAngleDegrees = _sweepAngle * 360.0 / (2.0 * PI);

                if (Selected == true)
                {
                    myPen.Color = instanceData.SelectedObjectLineColor;
                    myPen.Width = instanceData.SelectedObjectLineWidth;
                }

                if (IsRebar == true)
                {
                    if (Rebar != null)
                    {
                        myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                    }
                }

                if (pixel_centerPoint != null && sweepAngleDegrees != 0)
                {
                    g.DrawArc(myPen, pixel_centerPoint.X - pixel_radius, pixel_centerPoint.Y - pixel_radius, pixel_radius * 2, pixel_radius * 2, Convert.ToSingle(startAngleDegrees), Convert.ToSingle(sweepAngleDegrees));
                }

                if (Selected == true)
                {
                    Point[] pixelPoints = new Point[Nodes.Count];

                    for (int i = 0; i < Nodes.Count; i++)
                    {
                        pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
                    }

                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                    {
                        Rectangle boundingRectangle = new Rectangle
                            (
                                Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                                Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                                instanceData.SelectedObjectPointWidth,
                                instanceData.SelectedObjectPointWidth
                            );

                        g.FillEllipse(myBrush, boundingRectangle);
                    }

                    myBrush.Dispose();
                }

                myPen.Dispose();
            }
            catch
            {
                if (myPen != null)
                {
                    myPen.Dispose();
                }

                return;
            }
        }

        /// <summary>
        /// Returns a copy of the element given a base point and a paste point
        /// </summary>
        /// <param name="basePoint">Base point for copying</param>
        /// <param name="currentPoint">Paste point</param>
        /// <returns></returns>
        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            DrawingNode copiedStartPoint = StartPoint.CreateCopy(basePoint, currentPoint);
            DrawingNode copiedEndPoint = EndPoint.CreateCopy(basePoint, currentPoint);
            DrawingNode copiedThirdPoint = ThirdPoint.CreateCopy(basePoint, currentPoint);

            ArcG copiedArc = new ArcG(copiedStartPoint, copiedEndPoint, copiedThirdPoint);

            copiedArc.Layer = Layer;
            copiedArc.Selected = false;
            copiedArc.Rebar = Rebar;
            copiedArc.IsRebar = IsRebar;

            return copiedArc;
        }

        public override Rect BoundingRectangle()
        {
            List<PointD> pointsWithinSweep = new List<PointD>();

            pointsWithinSweep.AddRange(GetQuadrantPointsOnArc());

            pointsWithinSweep.Add(StartPoint.Point);

            pointsWithinSweep.Add(EndPoint.Point);

            //Find the min and max x and y values

            double minX = 0;
            double maxX = 0;
            double minY = 0;
            double maxY = 0;

            for (int i = 0; i < pointsWithinSweep.Count; i++)
            {
                PointD p = pointsWithinSweep[i];

                if (i == 0)
                {
                    minX = p.X;
                    maxX = p.X;
                    minY = p.Y;
                    maxY = p.Y;
                }
                else
                {
                    minX = Min(minX, p.X);
                    maxX = Max(maxX, p.X);
                    minY = Min(minY, p.Y);
                    maxY = Max(maxY, p.Y);
                }
            }

            int x0 = Convert.ToInt32(Floor(minX));
            int y0 = Convert.ToInt32(Floor(minY));

            int width = Convert.ToInt32(Ceiling(maxX - x0));
            int height = Convert.ToInt32(Ceiling(maxY - y0));

            return new Rect(x0, y0, width, height);
        }

        public override List<PointD> QuadrantPoints()
        {
            return GetQuadrantPointsOnArc();
        }

        public override List<LineSegment> Edges()
        {
            return new List<LineSegment>();
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (IntersectsWithInfiniteLine(line, out List<PointD> infIntersections) == true)
            {
                PointD p0 = line.StartNode.Point;
                PointD p1 = line.EndNode.Point;

                for (int i = 0; i < infIntersections.Count; i++)
                {
                    PointD p = infIntersections[i];

                    if (Round(p1.X - p0.X, _precision) == 0)
                    {
                        //vertical line
                        if (p.Y >= Min(p0.Y, p1.Y) && p.Y <= Max(p0.Y, p1.Y))
                        {
                            intersections.Add(p);
                        }
                    }
                    else
                    {
                        //horizontal line or line with slope
                        if (p.X >= Min(p0.X, p1.X) && p.X <= Max(p0.X, p1.X))
                        {
                            intersections.Add(p);
                        }
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            double r = _radius;

            DrawingNode cN = _centerPoint;

            PointD cP = cN.Point;

            PointD p0 = line.StartNode.Point;

            PointD p1 = line.EndNode.Point;

            if (Round(p1.X - p0.X, _precision) == 0)
            {
                //line is vertical
                if (p0.X < cP.X - r || p0.X > cP.X + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double xInt = p0.X;

                    double yInt1 = Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    double yInt2 = -Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    PointD int1 = new PointD(xInt, yInt1);

                    PointD int2 = new PointD(xInt, yInt2);

                    if (PointLiesWithinArcSweep(int1) == true)
                    {
                        intersections.Add(int1);
                    }

                    if (PointLiesWithinArcSweep(int2) == true)
                    {
                        intersections.Add(int2);
                    }
                }
            }
            else if (Round(p1.Y - p0.Y, _precision) == 0)
            {
                //line is horizontal
                if (p0.Y < cP.Y - r || p0.Y > cP.Y + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double yInt = p0.Y;

                    double xInt1 = Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    double xInt2 = -Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    PointD int1 = new PointD(xInt1, yInt);

                    PointD int2 = new PointD(xInt2, yInt);

                    if (PointLiesWithinArcSweep(int1) == true)
                    {
                        intersections.Add(int1);
                    }

                    if (PointLiesWithinArcSweep(int2) == true)
                    {
                        intersections.Add(int2);
                    }
                }
            }
            else
            {
                double ms = (p1.Y - p0.Y) / (p1.X - p0.X);

                double bs = p0.Y - ms * p0.X;

                double a = cP.X;

                double b = cP.Y;

                double a1 = (-1.0 - Pow(ms, 2));

                double b1 = (2.0 * a - 2.0 * ms * bs + 2.0 * ms * b);

                double c1 = (Pow(r, 2) + 2.0 * bs * b - Pow(a, 2) - Pow(bs, 2) - Pow(b, 2));

                double xInt1 = (-b1 + Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double xInt2 = (-b1 - Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double yInt1 = ms * xInt1 + bs;

                double yInt2 = ms * xInt2 + bs;

                if (xInt1.IsNanOrInfinity() == false && yInt1.IsNanOrInfinity() == false)
                {
                    PointD int1 = new PointD(xInt1, yInt1);

                    if (PointLiesWithinArcSweep(int1) == true)
                    {
                        intersections.Add(int1);
                    }
                }

                if (xInt2.IsNanOrInfinity() == false && yInt2.IsNanOrInfinity() == false)
                {
                    PointD int2 = new PointD(xInt2, yInt2);

                    if (PointLiesWithinArcSweep(int2) == true)
                    {
                        intersections.Add(int2);
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {
            double closestDist = 1.0E10;

            PointD closestPoint = null;

            foreach (DrawingNode node in Nodes)
            {
                if (node.TypeOfNode.Equals(nodeType) == true)
                {
                    if (globalPoint.DistanceTo(node.Point) <= closestDist)
                    {
                        closestDist = globalPoint.DistanceTo(node.Point);
                        closestPoint = node.Point;
                    }
                }
                else if (nodeType.Equals(NodeType.EndPoint) == true)
                {
                    if (node.TypeOfNode.Equals(NodeType.StartPoint) == true)
                    {
                        if (globalPoint.DistanceTo(node.Point) <= closestDist)
                        {
                            closestDist = globalPoint.DistanceTo(node.Point);
                            closestPoint = node.Point;
                        }
                    }
                }
            }

            return closestPoint;
        }

        /// <summary>
        /// Returns true if the drawing element is fully contained within the given bounding rectangle
        /// </summary>
        /// <param name="rectangle">Bounding rectangle</param>
        /// <returns></returns>
        public override bool IsContainedWithin(Rect rectangle)
        {
            bool containsAllPoints = false;

            List<PointD> pointsWithinSweep = new List<PointD>();

            pointsWithinSweep.AddRange(GetQuadrantPointsOnArc());

            pointsWithinSweep.Add(StartPoint.Point);

            pointsWithinSweep.Add(EndPoint.Point);

            //Find the min and max x and y values

            double minX = 0;
            double maxX = 0;
            double minY = 0;
            double maxY = 0;

            for (int i = 0; i < pointsWithinSweep.Count; i++)
            {
                PointD p = pointsWithinSweep[i];

                if (i == 0)
                {
                    minX = p.X;
                    maxX = p.X;
                    minY = p.Y;
                    maxY = p.Y;
                }
                else
                {
                    minX = Min(minX, p.X);
                    maxX = Max(maxX, p.X);
                    minY = Min(minY, p.Y);
                    maxY = Max(maxY, p.Y);
                }
            }

            if (rectangle.Contains(minX, minY) && rectangle.Contains(maxX, maxY))
            {
                containsAllPoints = true;
            }

            return containsAllPoints;
        }

        /// <summary>
        /// Moves the drawing element
        /// </summary>
        /// <param name="basePoint">Base point of move command</param>
        /// <param name="currentPoint">Final point of move command</param>
        public override void Move(PointD basePoint, PointD currentPoint)
        {
            base.Move(basePoint, currentPoint);

            ComputeProperties();
        }

        /// <summary>
        /// Rotates the object through the specified angle
        /// </summary>
        /// <param name="basePoint">Rotation base point</param>
        /// <param name="angle">Angle, in radians, to rotate through</param>
        public override void RotateThroughAngle(PointD basePoint, double angle)
        {
            base.RotateThroughAngle(basePoint, angle);

            ComputeProperties();
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //Arcs can't be joine with other elements (yet)
            newElement = null;
            return false;
        }

        public override List<IDrawingElement> Explode()
        {
            return new List<IDrawingElement>();
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            double r = _radius;

            PointD cp = _centerPoint.Point;

            double distToCenter = p.DistanceTo(cp);

            if (Abs(distToCenter - r) <= orthogonalTolerance)
            {
                if (PointLiesWithinArcSweep(p) == true)
                {
                    orthogonalDistance = Abs(distToCenter - r);

                    return true;
                }
                else
                {
                    orthogonalDistance = -1;

                    return false;
                }
            }
            else
            {
                orthogonalDistance = -1;

                return false;
            }
        }

        public override void ActionAfterReshape()
        {
            ComputeProperties();

            base.ActionAfterReshape();
        }

        public override bool ValidateElement()
        {
            if (_centerPoint.Point.X.IsNanOrInfinity() || _centerPoint.Point.Y.IsNanOrInfinity())
            {
                return false;
            }

            if (_radius.IsNanOrInfinity() || _radius <= 0)
            {
                return false;
            }

            if (_sweepAngle.IsNanOrInfinity() || _sweepAngle == 0)
            {
                return false;
            }

            return true;
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                intersects = ((CircleG)other).IntersectsWithArc(this, out intersections);
            }
            if (other.GetType().Equals(typeof(PostTensioningG)) == true)
            {
                intersects = IntersectsWithPostTensioning((PostTensioningG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(ArcG)) == true)
            {
                intersects = IntersectsWithArc((ArcG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                intersects = IntersectsWithPolygon((PolygonG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                intersects = IntersectsWithPolyline((PolylineG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PointG)) == true)
            {
                intersects = IntersectsWithPoint((PointG)other, out intersections);
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            return intersects;
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            DrawingNode n0 = CenterPoint();

            DrawingNode n1 = new DrawingNode(basePoint, NodeType.EndPoint);

            LineSegment line = new LineSegment(n0, n1);

            if (IntersectsWithInfiniteLine(line, out intersections) == true)
            {
                return true;
            }

            return false;
        }

        public override IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            //Create a dummy circle to work with
            CircleG dummyCircle = new CircleG()
            {
                Nodes = new List<DrawingNode>() { _centerPoint },
                Radius = _radius
            };

            int direction = 1;

            if (dummyCircle.PointLiesWithin(globalPoint) == false)
            {
                direction = -1;
            }

            CircleG offsetCircle = (CircleG)dummyCircle.CreateCopy(globalPoint, globalPoint);

            double newRadius = _radius;

            if (direction == 1)
            {
                if (offsetDistance < Radius)
                {
                    newRadius = _radius - offsetDistance;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                newRadius = _radius + offsetDistance;
            }

            offsetCircle.Radius = newRadius;

            //Get line from center to start point
            Vector centerToStartPoint = new Vector(StartPoint.Point.X - _centerPoint.Point.X, StartPoint.Point.Y - _centerPoint.Point.Y);

            centerToStartPoint = centerToStartPoint.UnitVector();

            PointD newStartPoint = new PointD(_centerPoint.Point.X + newRadius * centerToStartPoint.X, _centerPoint.Point.Y + newRadius * centerToStartPoint.Y);

            DrawingNode newStartNode = new DrawingNode(newStartPoint, NodeType.StartPoint);

            //Get line from center to end point
            Vector centerToEndPoint = new Vector(EndPoint.Point.X - _centerPoint.Point.X, EndPoint.Point.Y - _centerPoint.Point.Y);

            centerToEndPoint = centerToEndPoint.UnitVector();

            PointD newEndPoint = new PointD(_centerPoint.Point.X + newRadius * centerToEndPoint.X, _centerPoint.Point.Y + newRadius * centerToEndPoint.Y);

            DrawingNode newEndNode = new DrawingNode(newEndPoint, NodeType.EndPoint);

            //Get line from center to third point
            Vector centerToThirdPoint = new Vector(ThirdPoint.Point.X - _centerPoint.Point.X, ThirdPoint.Point.Y - _centerPoint.Point.Y);

            centerToThirdPoint = centerToThirdPoint.UnitVector();

            PointD newThirdPoint = new PointD(_centerPoint.Point.X + newRadius * centerToThirdPoint.X, _centerPoint.Point.Y + newRadius * centerToThirdPoint.Y);

            DrawingNode newThirdNode = new DrawingNode(newThirdPoint, NodeType.ThirdPoint);

            //Create new Arc
            List<DrawingNode> allNodes = new List<DrawingNode>(3) { newStartNode, newEndNode, newThirdNode };

            ArcG offsetArc = new ArcG(allNodes, Layer)
            {
                IsRebar = IsRebar,
                Rebar = Rebar
            };


            return offsetArc;
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            List<DrawingNode> nodeList = new List<DrawingNode>();

            double theta_1 = 2.0 * Asin((0.5 * dist) / Radius);

            int minAnglePoints = (int)Ceiling(8 * _sweepAngle / (2.0 * PI));

            int numAnglePoints = Max(minAnglePoints, Convert.ToInt32(Ceiling(Abs(_sweepAngle / theta_1))));

            double thetaInterval = -_sweepAngle / numAnglePoints;

            double standardConventionStartAngleRadians = 2.0 * PI - _startAngleRadians;

            for (int i = 0; i <= numAnglePoints; i++)
            {
                double theta = standardConventionStartAngleRadians + i * thetaInterval;

                PointD edgePoint = GetPointAtAngle(theta);

                DrawingNode edgeNode = new DrawingNode(edgePoint, NodeType.EndPoint);

                nodeList.Add(edgeNode);
            }

            PolylineG pl = new PolylineG()
            {
                Nodes = nodeList,
                Layer = Layer,
                IsRebar = IsRebar,
                Rebar = Rebar,
            };

            return pl;
        }

        public override double Length()
        {
            return 2.0 * PI * Radius * _sweepAngle / (2.0 * PI);
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            return null;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Gets the quadrant of the point relative to the arc's center point
        /// </summary>
        /// <param name="node">Node to determine quadrant of</param>
        /// <returns></returns>
        protected int GetQuadrant(DrawingNode node)
        {
            if (node.Point.X >= _centerPoint.Point.X)
            {
                if (node.Point.Y >= _centerPoint.Point.Y)
                {
                    return 1;
                }
                else
                {
                    return 4;
                }
            }
            else
            {
                if (node.Point.Y >= _centerPoint.Point.Y)
                {
                    return 2;
                }
                else
                {
                    return 3;
                }
            }
        }

        /// <summary>
        /// Returns the angle from the vector (1,0) that the node lies relative to the center point of the arc.
        /// Returns the angle in radians.  Angle is measured positive clockwise from the vector (1,0).  This differs
        /// from standard convention.  Windows GDI defines a positive angle as counterclockwise.
        /// </summary>
        /// <param name="node">Node to determine angle of</param>
        /// <returns></returns>
        protected double GetArcPointAngleRadians(DrawingNode node)
        {
            return GetArcPointAngleRadians(node.Point);
        }

        /// <summary>
        /// Returns the angle from the vector (1,0) that the node lies relative to the center point of the arc.
        /// Returns the angle in radians.  Angle is measured positive clockwise from the vector (1,0).  This differs
        /// from standard convention.  Windows GDI defines a positive angle as counterclockwise.
        /// </summary>
        /// <param name="p">Point to determine angle of</param>
        /// <returns></returns>
        protected double GetArcPointAngleRadians(PointD p)
        {
            double arcPointAngle = 0;

            if (_centerPoint.Point.X == p.X)
            {
                if (p.Y > _centerPoint.Point.Y)
                {
                    arcPointAngle = 3.0 * PI / 2.0;
                }
                else if (p.Y < _centerPoint.Point.Y)
                {
                    arcPointAngle = PI / 2.0;
                }
                else
                {
                    arcPointAngle = 0.0;
                }
            }
            else if (_centerPoint.Point.Y == p.Y)
            {
                if (p.X > _centerPoint.Point.X)
                {
                    arcPointAngle = 0.0;
                }
                else if (p.X < _centerPoint.Point.X)
                {
                    arcPointAngle = PI;
                }
                else
                {
                    arcPointAngle = 0;
                }
            }
            else if (p.X > _centerPoint.Point.X && p.Y > _centerPoint.Point.Y)
            {
                //Quadrant 1, Atan returns positive
                arcPointAngle = 2.0 * PI - Atan((p.Y - _centerPoint.Point.Y) / (p.X - _centerPoint.Point.X));
            }
            else if (p.X < _centerPoint.Point.X && p.Y > _centerPoint.Point.Y)
            {
                //Quadrant 2, Atan returns negative
                arcPointAngle = PI - Atan((p.Y - _centerPoint.Point.Y) / (p.X - _centerPoint.Point.X));
            }
            else if (p.X < _centerPoint.Point.X && p.Y < _centerPoint.Point.Y)
            {
                //Quadrant 3, Atan returns positive
                arcPointAngle = PI - Atan((p.Y - _centerPoint.Point.Y) / (p.X - _centerPoint.Point.X));
            }
            else
            {
                //Quadrant 4, Atan returns negative
                arcPointAngle = -1.0 * Atan((p.Y - _centerPoint.Point.Y) / (p.X - _centerPoint.Point.X));
            }

            return arcPointAngle;
        }

        /// <summary>
        /// Returns the sweep angle of the arc (between start and end points).
        /// Returns the angle in radians.  Angle is measured positive clockwise.
        /// </summary>
        /// <param name="start">Start point</param>
        /// <param name="end">End point</param>
        /// <param name="third">Third point</param>
        /// <returns></returns>
        protected double GetAngleSweepRadians(DrawingNode start, DrawingNode end, DrawingNode third)
        {
            double startPointAngle = GetArcPointAngleRadians(start);

            double endPointAngle = GetArcPointAngleRadians(end);

            double sweepAngle = 0;

            double side = IsLeft(start.Point, end.Point, third.Point);

            if (side > 0)
            {
                //The third point is left of the line formed by the two end points
                if (endPointAngle > startPointAngle)
                {
                    sweepAngle = endPointAngle - startPointAngle;
                }
                else
                {
                    sweepAngle = 2.0 * PI + endPointAngle - startPointAngle;
                }
            }
            else if (side < 0)
            {
                //The third point is right of the line formed by the two end points
                if (endPointAngle < startPointAngle)
                {
                    sweepAngle = endPointAngle - startPointAngle;
                }
                else
                {
                    sweepAngle = -2.0 * PI + (endPointAngle - startPointAngle);
                }
            }

            return sweepAngle;
        }

        protected double GetAngleSweepRadians(PointD start, PointD end, PointD third)
        {
            double startPointAngle = GetArcPointAngleRadians(start);

            double endPointAngle = GetArcPointAngleRadians(end);

            double sweepAngle = 0;

            double side = IsLeft(start, end, third);

            if (side > 0)
            {
                //The third point is left of the line formed by the two end points
                if (endPointAngle > startPointAngle)
                {
                    sweepAngle = endPointAngle - startPointAngle;
                }
                else
                {
                    sweepAngle = 2.0 * PI + endPointAngle - startPointAngle;
                }
            }
            else if (side < 0)
            {
                //The third point is right of the line formed by the two end points
                if (endPointAngle < startPointAngle)
                {
                    sweepAngle = endPointAngle - startPointAngle;
                }
                else
                {
                    sweepAngle = -2.0 * PI + (endPointAngle - startPointAngle);
                }
            }

            return sweepAngle;
        }

        /// <summary>
        /// Gets a list of the quadrant points that fall within the arc sweep
        /// </summary>
        /// <returns></returns>
        protected List<PointD> GetQuadrantPointsOnArc()
        {
            List<PointD> quadrantPoints = GetAllQuadrantPoints();

            List<PointD> pointsOnArc = new List<PointD>();

            for (int i = 0; i < quadrantPoints.Count; i++)
            {
                if (PointLiesWithinArcSweep(quadrantPoints[i]) == true)
                {
                    pointsOnArc.Add(quadrantPoints[i]);
                }
            }

            return pointsOnArc;
        }

        protected List<PointD> GetAllQuadrantPoints()
        {
            List<PointD> pointList = new List<PointD>(4);

            DrawingNode centerPoint = _centerPoint;

            pointList.Add(new PointD(centerPoint.Point.X - _radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y + _radius));
            pointList.Add(new PointD(centerPoint.Point.X + _radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y - _radius));

            return pointList;
        }

        protected void ComputeProperties()
        {
            CircleDefinition def = GetThreePointCircleEquation(StartPoint.Point, EndPoint.Point, ThirdPoint.Point);

            _centerPoint = new DrawingNode(def.Centroid, NodeType.CenterPoint);

            _radius = def.Radius;

            _startAngleRadians = GetArcPointAngleRadians(StartPoint);

            _endAngleRadians = GetArcPointAngleRadians(EndPoint);

            _sweepAngle = GetAngleSweepRadians(StartPoint, EndPoint, ThirdPoint);

            UpdateOpenGLGeometry();
        }

        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.OpenGeometry,
                ElementPropertyType.Rebar,
                ElementPropertyType.UserProperties
            };
        }
        #endregion

        #region Public Methods
        public DrawingNode CenterPoint()
        {
            return _centerPoint;
        }

        /// <summary>
        /// Checks if a point lies within the arc sweep.
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public bool PointLiesWithinArcSweep(PointD p)
        {
            double angleP = GetArcPointAngleRadians(p);

            if (_sweepAngle < 0)
            {
                if (_startAngleRadians > _endAngleRadians)
                {
                    if (angleP >= _endAngleRadians && angleP <= _startAngleRadians)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (angleP <= _startAngleRadians || angleP >= _endAngleRadians)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                if (_startAngleRadians > _endAngleRadians)
                {
                    if (angleP >= _startAngleRadians || angleP <= _endAngleRadians)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (angleP >= _startAngleRadians && angleP <= _endAngleRadians)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

        }

        public bool IntersectsWithPostTensioning(PostTensioningG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                int count = 0;

                int numInt = intersections.Count;

                while (count < numInt)
                {
                    if (this.PointLiesWithinArcSweep(intersections[count]) == false)
                    {
                        intersections.RemoveAt(count);

                        count--;

                        numInt--;
                    }

                    count++;
                }

                if (numInt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IntersectsWithArc(ArcG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                int count = 0;

                int numInt = intersections.Count;

                while (count < numInt)
                {
                    if (this.PointLiesWithinArcSweep(intersections[count]) == false || other.PointLiesWithinArcSweep(intersections[count]) == false)
                    {
                        intersections.RemoveAt(count);

                        count--;

                        numInt--;
                    }

                    count++;
                }

                if (numInt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IntersectsWithPolygon(PolygonG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = other.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (this.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                int count = 0;

                int numInt = intersections.Count;

                while (count < numInt)
                {
                    if (this.PointLiesWithinArcSweep(intersections[count]) == false)
                    {
                        intersections.RemoveAt(count);

                        count--;

                        numInt--;
                    }

                    count++;
                }

                if (numInt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IntersectsWithPolyline(PolylineG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = other.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (this.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                int count = 0;

                int numInt = intersections.Count;

                while (count < numInt)
                {
                    if (this.PointLiesWithinArcSweep(intersections[count]) == false)
                    {
                        intersections.RemoveAt(count);

                        count--;

                        numInt--;
                    }

                    count++;
                }

                if (numInt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IntersectsWithPoint(PointG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (this.PointLiesOnElementBoundary(other.Node().Point, 1.0 * Pow(10.0, -_precision), out _) == true)
            {
                intersections.Add(other.Node().Point);

                return true;
            }

            return false;
        }

        public PointD GetPointAtAngle(double theta)
        {
            double r = Radius;

            DrawingNode cN = CenterPoint();

            PointD cP = cN.Point;

            PointD p = new PointD(cP.X + r * Cos(theta), cP.Y + r * Sin(theta));

            return p;
        }
        #endregion

        #region OpenGL
        public override List<Vector2> GenerateOpenGLVertices()
        {
            int numLineSegments = Convert.ToInt32(Math.Ceiling(50 * (Math.Abs(_sweepAngle) / (2.0 * PI))));

            int listSize = (numLineSegments + 1) * 2;

            List<Vector2> vbo = new List<Vector2>(listSize);

            DrawingNode cP = CenterPoint();

            double angleInterval = -_sweepAngle / numLineSegments; //OpenGL draws arcs backwards relative to GDI+, so put negative in front of sweep angle

            double openGLstartAngle = 2.0 * PI - _startAngleRadians;

            for (int i = 1; i <= numLineSegments; i++)
            {
                double theta = openGLstartAngle + (i - 1) * angleInterval;

                float x = (float)(cP.Point.X + Radius * Cos(theta));
                float y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));

                theta = openGLstartAngle + (i) * angleInterval;

                x = (float)(cP.Point.X + Radius * Cos(theta));
                y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));
            }

            return vbo;
        }
        #endregion
    }
}
