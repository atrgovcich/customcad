﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CadPanel.Common;
using static System.Math;
using Utilities.Extensions.DotNetNative;
using System.Linq;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using CadPanel.Extensions;
using Utilities.DataStructures.QuadTree;
using FiberSectionAnalysis.Geometry;
using Newtonsoft.Json;

using OpenTK;

using Color = System.Drawing.Color;
using Point = System.Drawing.Point;

namespace CadPanel.DrawingElements
{
    public class CircleG : ClosedDrawingElement
    {
        #region Private Backing Fields
        protected double _radius;
        #endregion

        #region Public Properties
        public virtual double Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                _radius = value;

                PopulateQuadrantNodes();
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public CircleG() : base()
        {
            Name = "Circle";
            _elementType = DrawingElementType.Circle;
        }

        protected CircleG(string name) : base()
        {
            Name = name;
        }
        #endregion

        #region Public Implemented Methods
        public override double Area()
        {
            return PI * Pow(Radius, 2);
        }

        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Point pixelCenterPoint = CenterPoint().Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            int pixelRadius = Convert.ToInt32(Radius * drawingScale);

            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            if (String.Compare(Name, "Rebar Section", ignoreCase:true) != 0)
            {
                if (IsRebar == true)
                {
                    if (Rebar != null)
                    {
                        myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                    }
                }
            }

            Rectangle boundingRectangle = new Rectangle(
                pixelCenterPoint.X - pixelRadius,
                pixelCenterPoint.Y - pixelRadius,
                pixelRadius * 2,
                pixelRadius * 2);

            if (FillShape == true)
            {
                Color? fillBrushColor = (Selected == false) ? FillColor : instanceData.SelectedObjectLineColor;

                if (fillBrushColor == null)
                {
                    if (Selected == false)
                    {
                        fillBrushColor = Layer.Color;
                    }
                    else
                    {
                        fillBrushColor = instanceData.SelectedObjectLineColor;
                    }
                }

                SolidBrush fillBrush = new SolidBrush(fillBrushColor.Value);

                g.FillEllipse(fillBrush, boundingRectangle);

                fillBrush.Dispose();
            }

            g.DrawEllipse(myPen, boundingRectangle);

            if (Selected == true)
            {
                if (numSelectedElements == 1 || (numSelectedElements > 1 && instanceData.DrawVerticesOfMultipleObjects == true))
                {
                    List<PointD> pointList = Nodes.Select(x => x.Point).ToList();

                    List<Point> pixelPointList = new List<Point>(5);

                    foreach (PointD p in pointList)
                    {
                        pixelPointList.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                    }

                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    foreach (Point p in pixelPointList)
                    {
                        Rectangle quadBoundingRectangle = new Rectangle(
                            Convert.ToInt32(p.X - instanceData.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(p.Y - instanceData.SelectedObjectPointWidth / 2.0),
                            instanceData.SelectedObjectPointWidth,
                            instanceData.SelectedObjectPointWidth);

                        g.FillEllipse(myBrush, quadBoundingRectangle);
                    }

                    myBrush.Dispose();
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            DrawingNode cp = CenterPoint();

            Point pixelCenterPoint = cp.Point.ConvertToPixelPoint(zeroPoint, drawingScale);

            double r = Sqrt(Pow(cp.Point.X - currentPoint.X, 2) + Pow(cp.Point.Y - currentPoint.Y, 2));

            if (r > 0)
            {
                Radius = Sqrt(Pow(cp.Point.X - currentPoint.X, 2) + Pow(cp.Point.Y - currentPoint.Y, 2));

                int pixelRadius = Convert.ToInt32(Radius * drawingScale);

                Pen myPen = new Pen(Layer.Color, Layer.Width);

                if (Selected == true)
                {
                    myPen.Color = instanceData.SelectedObjectLineColor;
                    myPen.Width = instanceData.SelectedObjectLineWidth;
                }

                if (IsRebar == true)
                {
                    if (Rebar != null)
                    {
                        myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                    }
                }

                Rectangle boundingRectangle = new Rectangle(
                    pixelCenterPoint.X - pixelRadius,
                    pixelCenterPoint.Y - pixelRadius,
                    pixelRadius * 2,
                    pixelRadius * 2);

                g.DrawEllipse(myPen, boundingRectangle);

                if (Selected == true)
                {
                    List<PointD> quadPoints = QuadrantPoints();

                    List<Point> pixelQuadPoints = new List<Point>(5);

                    pixelQuadPoints.Add(pixelCenterPoint);

                    foreach (PointD p in quadPoints)
                    {
                        pixelQuadPoints.Add(p.ConvertToPixelPoint(zeroPoint, drawingScale));
                    }

                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    foreach (Point p in pixelQuadPoints)
                    {
                        Rectangle quadBoundingRectangle = new Rectangle(
                            Convert.ToInt32(p.X - instanceData.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(p.Y - instanceData.SelectedObjectPointWidth / 2.0),
                            instanceData.SelectedObjectPointWidth,
                            instanceData.SelectedObjectPointWidth);

                        g.FillEllipse(myBrush, quadBoundingRectangle);
                    }

                    myBrush.Dispose();
                }

                myPen.Dispose();
            }   
        }

        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>(Nodes.Count);

            for (int i = 0; i < Nodes.Count; i++)
            {
                copiedNodes.Add(Nodes[i].CreateCopy(basePoint, currentPoint));
            }

            //DrawingNode currentCenterNode = CenterPoint();

            //PointD copiedCenterPoint = new PointD(currentCenterNode.Point.X + delta_X, currentCenterNode.Point.Y + delta_Y);

            //DrawingNode centerNode = new DrawingNode(copiedCenterPoint, NodeType.CenterPoint);

            CircleG copiedCircle = new CircleG()
            {
                Nodes = new List<DrawingNode>(copiedNodes),
                Radius = Radius,
                Hole = Hole,
                IsRebar = IsRebar,
                Layer = Layer,
                Material = Material,
                Name = Name,
                Rebar = Rebar,
                Selected = false
            };

            copiedCircle.Nodes = copiedNodes;

            return copiedCircle;
        }

        public override Rect BoundingRectangle()
        {
            DrawingNode cp = CenterPoint();

            int x0 = Convert.ToInt32(Floor(cp.Point.X - Radius));
            int y0 = Convert.ToInt32(Floor(cp.Point.Y - Radius));

            int width = Convert.ToInt32(Ceiling(cp.Point.X + Radius - x0));
            int height = width;

            return new Rect(x0, y0, width, height);
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {
            PointD cp = CenterPoint().Point;

            if (nodeType.Equals(NodeType.CenterPoint) == true)
            {
                return cp;
            }
            else if (nodeType.Equals(NodeType.QuadrantPoint) == true)
            {
                double closestDist = 1.0E10;

                PointD closestPoint = null;

                List<PointD> quadrantPoints = new List<PointD>()
                {
                    new PointD(cp.X + Radius, cp.Y),
                    new PointD(cp.X, cp.Y + Radius),
                    new PointD(cp.X - Radius, cp.Y),
                    new PointD(cp.X, cp.Y - Radius)
                };

                foreach (PointD p in quadrantPoints)
                {
                    if (globalPoint.DistanceTo(p) <= closestDist)
                    {
                        closestDist = globalPoint.DistanceTo(p);
                        closestPoint = p;
                    }
                }

                return closestPoint;
            }
            else
            {
                return null;
            }
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (IntersectsWithInfiniteLine(line, out List<PointD> infIntersections) == true)
            {
                PointD p0 = line.StartNode.Point;
                PointD p1 = line.EndNode.Point;

                for (int i = 0; i < infIntersections.Count; i++)
                {
                    PointD p = infIntersections[i];

                    if (Round(p1.X - p0.X, _precision) == 0)
                    {
                        //vertical line
                        if(p.Y >= Min(p0.Y, p1.Y) && p.Y <= Max(p0.Y, p1.Y))
                        {
                            intersections.Add(p);
                        }
                    }
                    else
                    {
                        //horizontal line or line with slope
                        if (p.X >= Min(p0.X, p1.X) && p.X <= Max(p0.X, p1.X))
                        {
                            intersections.Add(p);
                        }
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            double r = Radius;

            DrawingNode cN = CenterPoint();

            PointD cP = cN.Point;

            PointD p0 = line.StartNode.Point;

            PointD p1 = line.EndNode.Point;

            if (Round(p1.X - p0.X, _precision) == 0)
            {
                //line is vertical
                if (p0.X < cP.X - r || p0.X > cP.X + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double xInt = p0.X;

                    double yInt1 = Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    double yInt2 = -Sqrt(Pow(r, 2) - Pow(xInt - cP.X, 2)) + cP.Y;

                    intersections.Add(new PointD(xInt, yInt1));

                    intersections.Add(new PointD(xInt, yInt2));

                    return true;
                }
            }
            else if (Round(p1.Y - p0.Y, _precision) == 0)
            {
                //line is horizontal
                if (p0.Y < cP.Y - r || p0.Y > cP.Y + r)
                {
                    //line is outside of circle
                    return false;
                }
                else
                {
                    double yInt = p0.Y;

                    double xInt1 = Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    double xInt2 = -Sqrt(Pow(r, 2) - Pow(yInt - cP.Y, 2)) + cP.X;

                    intersections.Add(new PointD(xInt1, yInt));

                    intersections.Add(new PointD(xInt2, yInt));

                    return true;
                }
            }
            else
            {
                double ms = (p1.Y - p0.Y) / (p1.X - p0.X);

                double bs = p0.Y - ms * p0.X;

                double a = cP.X;

                double b = cP.Y;

                double a1 = (-1.0 - Pow(ms, 2));

                double b1 = (2.0 * a - 2.0 * ms * bs + 2.0 * ms * b);

                double c1 = (Pow(r, 2) + 2.0 * bs * b - Pow(a, 2) - Pow(bs, 2) - Pow(b, 2));

                double xInt1 = (-b1 + Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double xInt2 = (-b1 - Sqrt(Pow(b1, 2) - 4.0 * a1 * c1)) / (2.0 * a1);

                double yInt1 = ms * xInt1 + bs;

                double yInt2 = ms * xInt2 + bs;

                if (xInt1.IsNanOrInfinity() == false && yInt1.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt1, yInt1));
                }

                if (xInt2.IsNanOrInfinity() == false && yInt2.IsNanOrInfinity() == false)
                {
                    intersections.Add(new PointD(xInt2, yInt2));
                }

                if (intersections.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                intersects = IntersectsWithCircle((CircleG)other, out intersections);
            }
            if (other.GetType().Equals(typeof(PostTensioningG)) == true)
            {
                intersects = IntersectsWithPostTensioning((PostTensioningG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(ArcG)) == true)
            {
                intersects = IntersectsWithArc((ArcG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                intersects = IntersectsWithPolygon((PolygonG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                intersects = IntersectsWithPolyline((PolylineG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PointG)) == true)
            {
                intersects = IntersectsWithPoint((PointG)other, out intersections);
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            return intersects;
        }

        public override List<LineSegment> Edges()
        {
            return new List<LineSegment>();
        }

        public override List<PointD> QuadrantPoints()
        {
            List<PointD> pointList = new List<PointD>(4);

            DrawingNode centerPoint = CenterPoint();

            List<DrawingNode> qNs = QuadrantNodes();

            for (int i = 0; i < qNs.Count; i++)
            {
                if (qNs[i].TypeOfNode.Equals(NodeType.QuadrantPoint) == true)
                {
                    pointList.Add(qNs[i].Point);
                }
            }

            //pointList.Add(new PointD(centerPoint.Point.X - Radius, centerPoint.Point.Y));
            //pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y + Radius));
            //pointList.Add(new PointD(centerPoint.Point.X + Radius, centerPoint.Point.Y));
            //pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y - Radius));

            return pointList;
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //Circles can't be joine with other elements
            newElement = null;
            return false;
        }

        /// <summary>
        /// Returns true if the drawing element is fully contained within the given bounding rectangle
        /// </summary>
        /// <param name="rectangle">Bounding rectangle</param>
        /// <returns></returns>
        public override bool IsContainedWithin(Rect rectangle)
        {
            bool containsAllPoints = true;

            List<PointD> pointsToCheck = QuadrantPoints();

            for (int i = 0; i < pointsToCheck.Count; i++)
            {
                PointD qp = pointsToCheck[i];

                if (rectangle.Contains(qp.X, qp.Y) == false)
                {
                    containsAllPoints = false;
                    break;
                }
            }

            return containsAllPoints;
        }

        public override List<IDrawingElement> Explode()
        {
            return new List<IDrawingElement>();
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            double r = Radius;

            PointD cp = CenterPoint().Point;

            double distToCenter = p.DistanceTo(cp);

            if (Abs(distToCenter - r) <= orthogonalTolerance)
            {
                orthogonalDistance = Abs(distToCenter - r);

                return true;
            }
            else
            {
                orthogonalDistance = -1;

                return false;
            }
        }

        /// <summary>
        /// Rotates the object through the specified angle
        /// </summary>
        /// <param name="basePoint">Rotation base point</param>
        /// <param name="angle">Angle, in radians, to rotate through</param>
        public override void RotateThroughAngle(PointD basePoint, double angle)
        {
            base.RotateThroughAngle(basePoint, angle);

            PopulateQuadrantNodes();
        }

        public override void ActionAfterReshape()
        {
            //Find the new radius

            //Get the center point
            DrawingNode cP = CenterPoint();

            //Get the quadrant nodes
            List<DrawingNode> qNs = QuadrantNodes();

            //Sort the list by distance to the center point
            qNs = qNs.OrderBy(x => x.DistanceTo(cP)).ToList();

            //Check to see if the distances from quadrant points to center point 
            //are different.  If any more than one is different, it means the center
            //point was modified.
            int numDifferent = 0;

            for (int i = 0; i < qNs.Count; i++)
            {
                if (Abs(Round(qNs[i].DistanceTo(cP) - Radius, _precision)) > 0)
                {
                    numDifferent++;
                }
            }
            
            if (numDifferent > 1)
            {
                if (Radius == 0)
                {
                    //This is a catch.  If the radius was 0, validation will return false, and the 
                    //radius will need to be re-calculated from the re-set nodal points
                    _radius = ComputeRadiusFromNodes();
                }
                else
                {
                    //The center point was modified.  Don't adjust the radius.
                    //Simply re-populate the new quadrant nodes.
                    PopulateQuadrantNodes();
                }
            }
            else if (numDifferent == 1)
            {
                //The modified point is either the first or the last in the ordered list
                //It will be the point that is most different from the current radius

                List<DrawingNode> firstAndLast = new List<DrawingNode>(2) { qNs[0], qNs[qNs.Count - 1] };

                firstAndLast = firstAndLast.OrderBy(x => (Abs(x.DistanceTo(cP) - Radius))).ToList();

                DrawingNode modifiedNode = firstAndLast[firstAndLast.Count - 1];

                Radius = modifiedNode.DistanceTo(cP);

                //AddNode(modifiedNode);
            }

            UpdateOpenGLGeometry();
        }

        public override bool ValidateElement()
        {
            if (Radius <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            DrawingNode n0 = CenterPoint();

            DrawingNode n1 = new DrawingNode(basePoint, NodeType.EndPoint);

            LineSegment line = new LineSegment(n0, n1);

            if (IntersectsWithInfiniteLine(line, out intersections) == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Offsets the element by the offset distance
        /// </summary>
        /// <param name="offsetDistance">Offset distance</param>
        /// <param name="direction">For interior offset, specify 1; for exterior offset, specify -1</param>
        /// <returns></returns>
        public override IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            int direction = 1;

            if (PointLiesWithin(globalPoint) == false)
            {
                direction = -1;
            }

            if (direction == 1)
            {
                if (offsetDistance < Radius)
                {
                    CircleG offsetCircle = (CircleG)CreateCopy(globalPoint, globalPoint);

                    double newRadius = Radius - offsetDistance;

                    offsetCircle.Radius = newRadius;

                    return offsetCircle;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                CircleG offsetCircle = (CircleG)CreateCopy(globalPoint, globalPoint);

                double newRadius = Radius + offsetDistance;

                offsetCircle.Radius = newRadius;

                return offsetCircle;
            }
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            List<DrawingNode> nodeList = new List<DrawingNode>();

            double theta_1 = 2.0 * Asin((0.5 * dist) / Radius);

            int minAnglePoints = 8;

            int numAnglePoints = Max(minAnglePoints, Convert.ToInt32(Ceiling(2.0 * PI / theta_1)));

            double thetaInterval = 2.0 * PI / numAnglePoints;

            for (int i = 0; i < numAnglePoints; i++)
            {
                double theta = -0.5 * thetaInterval + i * thetaInterval;

                PointD edgePoint = GetPointAtAngle(theta);

                DrawingNode edgeNode = new DrawingNode(edgePoint, NodeType.EndPoint);

                nodeList.Add(edgeNode);
            }

            nodeList.Add(nodeList[0]);

            PolygonG pg = new PolygonG()
            {
                Nodes = nodeList,
                Layer = Layer,
                IsRebar = IsRebar,
                Rebar = Rebar,
                Material =Material,
                Hole = Hole,
                FillColor = FillColor
            };

            return pg;
        }

        public override PointD Centroid()
        {
            return CenterPoint().Point;
        }

        public override double Perimeter()
        {
            return 2.0 * PI * Radius;
        }

        public override double Ixx()
        {
            return (PI / 4.0) * Pow(Radius, 4);
        }

        public override double Iyy()
        {
            return (PI / 4.0) * Pow(Radius, 4);
        }

        public override double Ixy()
        {
            return 0;
        }

        public override double Sxx()
        {
            return (PI / 4.0) * Pow(Radius, 3);
        }

        public override double Syy()
        {
            return (PI / 4.0) * Pow(Radius, 3);
        }

        public override bool IsFullyWithin(ClosedDrawingElement other)
        {
            DrawingNode cP = CenterPoint();
            
            bool inside = true;

            if (other.GetType().Equals(typeof(PolygonG)) == true)
            {
                PolygonG poly = (PolygonG)other;

                List<DrawingNode> otherNodes = poly.Nodes;

                for (int i = 0; i < otherNodes.Count; i++)
                {
                    if (otherNodes[i].DistanceTo(cP) < Radius)
                    {
                        inside = false;

                        break;
                    }
                }

                //Now check if any edges intersect the circle
                if (inside == true)
                {
                    List<LineSegment> edges = poly.Edges();

                    for (int i = 0; i < edges.Count; i++)
                    {
                        if (this.IntersectsWithLine(edges[i], out List<PointD> intersections) == true)
                        {
                            if (edges[i].StartNode.DistanceTo(cP) != Radius || edges[i].EndNode.DistanceTo(cP) != Radius)
                            {
                                inside = false;

                                break;
                            }
                        }
                    }
                }
                
            }
            else if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                CircleG circle = (CircleG)other;

                List<DrawingNode> otherNodes = circle.Nodes;

                for (int i = 0; i < otherNodes.Count; i++)
                {
                    if (otherNodes[i].DistanceTo(cP) < Radius)
                    {
                        inside = false;

                        break;
                    }
                }
            }

            return inside;
        }

        public override bool ContainsPoint(PointD p)
        {
            DrawingNode n = CenterPoint();

            if (n.Point.DistanceTo(p) <= Radius)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            CircleFS cFS = new CircleFS(this.CenterPoint().Point, this.Radius, this.Material);
            cFS.Hole = this.Hole;
            cFS.IsRebar = this.IsRebar;

            return cFS;
        }
        #endregion

        #region Public Methods
        public PointD GetPointAtAngle(double theta)
        {
            double r = Radius;

            DrawingNode cN = CenterPoint();

            PointD cP = cN.Point;

            PointD p = new PointD(cP.X + r * Cos(theta), cP.Y + r * Sin(theta));

            return p;
        }
        public bool PointLiesWithin(PointD p)
        {
            DrawingNode cP = CenterPoint();

            if (cP.Point.DistanceTo(p) <= Radius)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PointLiesWithin(DrawingNode n)
        {
            return PointLiesWithin(n.Point);
        }

        public DrawingNode CenterPoint()
        {
            foreach (DrawingNode node in Nodes)
            {
                if (node.TypeOfNode.Equals(NodeType.CenterPoint) == true)
                {
                    return node;
                }
            }

            throw new Exception("No center point has been defined.");
        }

        public List<DrawingNode> QuadrantNodes()
        {
            List<DrawingNode> retList = new List<DrawingNode>();

            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].TypeOfNode.Equals(NodeType.QuadrantPoint) == true)
                {
                    retList.Add(Nodes[i]);
                }
            }

            return retList;
        }

        public bool IntersectsWithCircle(CircleG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IntersectsWithPostTensioning(PostTensioningG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IntersectsWithArc(ArcG other, out List<PointD> intersections)
        {
            intersections = CommonMethods.GetIntersectionsOfTwoCircles(this.CenterPoint().Point, this.Radius, other.CenterPoint().Point, other.Radius);

            if (intersections.Count > 0)
            {
                int count = 0;

                int numInt = intersections.Count;

                while (count < numInt)
                {
                    if (other.PointLiesWithinArcSweep(intersections[count]) == false)
                    {
                        intersections.RemoveAt(count);

                        count--;

                        numInt--;
                    }

                    count++;
                }

                if (numInt > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IntersectsWithPolygon(PolygonG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = other.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (this.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool IntersectsWithPolyline(PolylineG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = other.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (this.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool IntersectsWithPoint(PointG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            if (this.PointLiesOnElementBoundary(other.Node().Point, 1.0 * Pow(10.0, -_precision), out _) == true)
            {
                intersections.Add(other.Node().Point);

                return true;
            }

            return false;
        }
        #endregion

        #region Protected Methods
        protected virtual void PopulateQuadrantNodes()
        {
            if (Nodes != null && Nodes.Count > 0)
            {
                List<DrawingNode> exQns = QuadrantNodes();

                List<PointD> qPs = BuildInitialQuadrantPoints();

                if (exQns.Count >= 4)
                {
                    DrawingNode cP = CenterPoint();

                    List<MutableTuple<PointD, bool>> unitVectorFound = new List<MutableTuple<PointD, bool>>()
                    {
                        new MutableTuple<PointD, bool>(new PointD(1,0), false),
                        new MutableTuple<PointD, bool>(new PointD(0,1), false),
                        new MutableTuple<PointD, bool>(new PointD(-1,0), false),
                        new MutableTuple<PointD, bool>(new PointD(0,-1), false)
                    };

                    for (int i = 0; i < exQns.Count; i++)
                    {
                        PointD uv = new PointD((exQns[i].Point.X - cP.Point.X) / exQns[i].DistanceTo(cP), (exQns[i].Point.Y - cP.Point.Y) / exQns[i].DistanceTo(cP));

                        double tt = 2;
                        for (int j = 0; j < unitVectorFound.Count; j++)
                        {
                            if (uv == unitVectorFound[j].Item1)
                            {
                                unitVectorFound[j].Item2 = true;
                            }
                        }
                    }

                    List<PointD> missingUnitVectors = new List<PointD>();

                    for (int i = 0; i < unitVectorFound.Count; i++)
                    {
                        if (unitVectorFound[i].Item2 == false)
                        {
                            missingUnitVectors.Add(unitVectorFound[i].Item1);
                        }
                    }

                    int missingUvIndex = 0;

                    for (int i = 0; i < exQns.Count; i++)
                    {
                        PointD uv = new PointD((exQns[i].Point.X - cP.Point.X) / exQns[i].DistanceTo(cP), (exQns[i].Point.Y - cP.Point.Y) / exQns[i].DistanceTo(cP));

                        bool uvFound = false;

                        for (int j = 0; j < unitVectorFound.Count; j++)
                        {
                            if (uv == unitVectorFound[j].Item1)
                            {
                                uvFound = true;
                            }
                        }

                        if (uvFound == false)
                        {
                            uv = missingUnitVectors[missingUvIndex];

                            missingUvIndex++;
                        }

                        exQns[i].Point.X = cP.Point.X + uv.X * _radius;

                        exQns[i].Point.Y = cP.Point.Y + uv.Y * _radius;
                    }
                }
                else
                {
                    int counter = 0;

                    int nodeCount = Nodes.Count;

                    while (counter < nodeCount)
                    {
                        if (Nodes[counter].TypeOfNode.Equals(NodeType.QuadrantPoint) == true)
                        {
                            Nodes.RemoveAt(counter);

                            counter--;

                            nodeCount--;
                        }

                        counter++;
                    }

                    for (int i = 0; i < qPs.Count; i++)
                    {
                        Nodes.Add(new DrawingNode(qPs[i], NodeType.QuadrantPoint));
                    }
                }              
            }
        }

        protected List<PointD> BuildInitialQuadrantPoints()
        {
            List<PointD> pointList = new List<PointD>(4);

            DrawingNode centerPoint = CenterPoint();

            pointList.Add(new PointD(centerPoint.Point.X - Radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y + Radius));
            pointList.Add(new PointD(centerPoint.Point.X + Radius, centerPoint.Point.Y));
            pointList.Add(new PointD(centerPoint.Point.X, centerPoint.Point.Y - Radius));

            return pointList;
        }

        protected Tuple<double, double> GetYCoordinate(double x)
        {
            DrawingNode cP = CenterPoint();

            if (x < cP.Point.X - Radius || x > cP.Point.X + Radius)
            {
                return null;
            }
            else
            {
                double y1 = cP.Point.Y + Sqrt(Radius * Radius - (x - cP.Point.X) * (x - cP.Point.X));

                double y2 = cP.Point.Y - Sqrt(Radius * Radius - (x - cP.Point.X) * (x - cP.Point.X));

                return new Tuple<double, double>(y1, y2);
            }
        }

        protected double GetValueOfDerivateAt(double x, double y)
        {
            DrawingNode cP = CenterPoint();

            if (y == cP.Point.Y)
            {
                //Vertical line
                return double.PositiveInfinity;
            }
            else
            {
                return -1.0 * ((x - cP.Point.X) / (y - cP.Point.Y));
            }
        }

        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.ClosedGeometry,
                ElementPropertyType.Rebar,
                ElementPropertyType.Material,
                ElementPropertyType.UserProperties
            };
        }
        #endregion

        #region Private Methods
        private void AddNode(DrawingNode node)
        {
            List<int> removeIndeces = new List<int>();

            for (int i = 0; i < Nodes.Count;i++)
            {
                if (Nodes[i].TypeOfNode.Equals(node.TypeOfNode) == true)
                {
                    if (Nodes[i].Point == node.Point)
                    {
                        removeIndeces.Add(i);
                    }
                }
            }

            for (int i = 0; i < removeIndeces.Count; i++)
            {
                Nodes.RemoveAt(removeIndeces[i] - i);
            }

            Nodes.Add(node);
        }

        private double ComputeRadiusFromNodes()
        {
            DrawingNode cP = CenterPoint();

            DrawingNode qP = null;

            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].TypeOfNode.Equals(NodeType.QuadrantPoint) == true)
                {
                    qP = Nodes[i];

                    break;
                }
            }

            double r = qP.DistanceTo(cP);

            return r;
        }

        #endregion

        #region Private Structs

        #endregion

        #region OpenGL
        public override List<Vector2> GenerateOpenGLVertices()
        {
            int numLineSegments = 50;

            int listSize = (numLineSegments + 1) * 2;

            List<Vector2> vbo = new List<Vector2>(listSize);

            DrawingNode cP = CenterPoint();

            for (int i = 1; i <= numLineSegments; i++)
            {
                double theta = (2 * PI * (i - 1) / numLineSegments);

                float x = (float)(cP.Point.X + Radius * Cos(theta));
                float y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));

                theta = (2 * PI * i / numLineSegments);

                x = (float)(cP.Point.X + Radius * Cos(theta));
                y = (float)(cP.Point.Y + Radius * Sin(theta));

                vbo.Add(new Vector2(x, y));
            }
            
            return vbo;
        }
        #endregion
    }
}
