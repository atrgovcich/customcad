﻿using System;
using System.Collections.Generic;
using Materials;
using CadPanel.Common;
using System.Drawing;
using static System.Math;
using ReinforcedConcrete.Reinforcement;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using Utilities.DataStructures.QuadTree;
using Newtonsoft.Json;
using OpenTK;

using Point = System.Drawing.Point;
using Utilities.Containers;
using CadPanel.UserProperties;

namespace CadPanel.DrawingElements
{
    public abstract class DrawingElement : IDrawingElement
    {
        #region Implemented Public Properties
        public List<DrawingNode> Nodes { get; set; } = new List<DrawingNode>();
        public CadPanel.Common.Layer Layer { get; set; }
        public bool Selected { get; set; } = false;
        public string Name { get; set; }

        [JsonIgnore]
        public DrawingElementType ElementType
        {
            get
            {
                return _elementType;
            }
        }

        [JsonIgnore]
        public List<ElementPropertyType> PropertyTypeList { get; protected set; }

        public virtual IGenericMaterial Material { get; set; }

        protected List<Vector2> _openGLvertices;

        [JsonIgnore]
        public List<Vector2> OpenGLVertices
        {
            get
            {
                if (_openGLvertices == null)
                {
                    _openGLvertices = GenerateOpenGLVertices();
                }

                return _openGLvertices;
            }
            protected set
            {
                _openGLvertices = value;
            }
        }

        protected List<Vector2> _openGLselectionVertices;
        [JsonIgnore]
        public List<Vector2> OpenGLSelectionVertices
        {
            get
            {
                if (_openGLselectionVertices == null)
                {
                    _openGLselectionVertices = GenerateOpenGLSelectionVertices();
                }

                return _openGLselectionVertices;
            }
            protected set
            {
                _openGLselectionVertices = value;
            }
        }
        public ObservableList<UserProperty> UserProperties { get; set; }
        #endregion

        #region Public Properties
        public bool IsRebar { get; set; } = false;
        public virtual RebarDefinition Rebar { get; set; } = null;

        #endregion

        #region Protected Properties
        protected int _precision = 4;

        [JsonProperty]
        protected DrawingElementType _elementType;
        #endregion

        #region Private Variables
        private const int ROUND = 4;
        #endregion

        #region Constructors
        public DrawingElement()
        {
            PopulatePropertyTypesList();
        }
        #endregion

        #region Abstract Methods
        public abstract void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements);

        public abstract void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData);

        public abstract IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint);

        public abstract Rect BoundingRectangle();

        public abstract PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType);

        public abstract List<LineSegment> Edges();

        public abstract bool IntersectsWithLine(LineSegment line, out List<PointD> intersections);

        public abstract bool JoinWith(IDrawingElement other, out IDrawingElement newElement);

        public abstract bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections);

        public abstract List<IDrawingElement> Explode();

        public abstract bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance);

        public abstract bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections);

        public abstract bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections);

        protected abstract void PopulatePropertyTypesList();

        public abstract GenericShape ConvertToFiberSectionElement();
        #endregion

        #region Implemented Public Interface Methods
        /// <summary>
        /// Moves the drawing element
        /// </summary>
        /// <param name="basePoint">Base point of move command</param>
        /// <param name="currentPoint">Final point of move command</param>
        public virtual void Move(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            foreach (DrawingNode node in Nodes)
            {
                node.Point.X += delta_X;
                node.Point.Y += delta_Y;
            }

            UpdateOpenGLGeometry();
        }

        /// <summary>
        /// Rotates the drawing element about a base point
        /// </summary>
        /// <param name="basePoint">Rotate base point</param>
        /// <param name="currentPoint">Final rotate point</param>
        public void Rotate(PointD basePoint, PointD currentPoint)
        {
            double rotationAngle = currentPoint.GetAngleFromPoint(basePoint);

            RotateThroughAngle(basePoint, rotationAngle);
        }

        /// <summary>
        /// Rotates the object through the specified angle
        /// </summary>
        /// <param name="basePoint">Rotation base point</param>
        /// <param name="angle">Angle, in radians, to rotate through</param>
        public virtual void RotateThroughAngle(PointD basePoint, double angle)
        {
            foreach (DrawingNode node in Nodes)
            {
                double distToBasePoint = node.Point.DistanceTo(basePoint);

                if (distToBasePoint > 0)
                {
                    double oldAngle = node.Point.GetAngleFromPoint(basePoint);

                    double newAngle = oldAngle + angle;

                    if (newAngle > 2.0 * PI)
                    {
                        newAngle -= 2.0 * PI;
                    }

                    double newX = basePoint.X + distToBasePoint * Cos(newAngle);
                    double newY = basePoint.Y + distToBasePoint * Sin(newAngle);

                    node.Point.X = newX;
                    node.Point.Y = newY;
                }
            }

            UpdateOpenGLGeometry();
        }

        public virtual List<PointD> QuadrantPoints()
        {
            return new List<PointD>();
        }

        public virtual PointD BasePoint()
        {
            if (Nodes != null)
            {
                if (Nodes.Count >= 1)
                {
                    if (Nodes[0].TypeOfNode.Equals(NodeType.CenterPoint)==true ||
                        Nodes[0].TypeOfNode.Equals(NodeType.EndPoint) == true)
                    {
                        return Nodes[0].Point;
                    }
                }
            }

            throw new Exception($"No base point found for the current {Name} element.  Every element must have a base point.");
        }

        /// <summary>
        /// Returns true if the drawing element is fully contained within the given bounding rectangle
        /// </summary>
        /// <param name="rectangle">Bounding rectangle</param>
        /// <returns></returns>
        public virtual bool IsContainedWithin(Rect rectangle)
        {
            bool containsAllPoints = true;

            for (int i = 0; i < Nodes.Count; i++)
            {
                PointD qp = Nodes[i].Point;

                if (rectangle.Contains(qp.X, qp.Y) == false)
                {
                    containsAllPoints = false;
                    break;
                }
            }

            return containsAllPoints;
        }

        public virtual void ActionAfterReshape()
        {
            UpdateOpenGLGeometry();

            return;
        }

        public virtual bool ValidateElement()
        {
            return true;
        }

        public virtual bool CloseShape(out IDrawingElement newClosedElement)
        {
            newClosedElement = null;

            return false;
        }

        public virtual IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            return null;
        }

        public virtual IDrawingElement GetDiscretizedPoly(double dist)
        {
            return null;
        }
        #endregion

        #region Protected Methods

        #endregion

        #region Virtual Methods      
        public virtual void GetOpenGLBuffers(CadPanelInstanceData instanceData, out List<Vector2> vertexBuffer, out List<float> colorBuffer)
        {
            vertexBuffer = new List<Vector2>();
            colorBuffer = new List<float>();
        }

        public virtual List<Vector2> GenerateOpenGLVertices()
        {
            return new List<Vector2>();
        }

        public virtual List<Vector2> GenerateOpenGLSelectionVertices()
        {
            if (Nodes != null)
            {
                List<Vector2> verts = new List<Vector2>(Nodes.Count);

                for (int i = 0; i < Nodes.Count; i++)
                {
                    float x = (float)Nodes[i].Point.X;
                    float y = (float)Nodes[i].Point.Y;

                    Vector2 v = new Vector2(x, y);

                    verts.Add(v);
                }

                return verts;
            }
            else
            {
                return new List<Vector2>();
            }
        }

        public void UpdateOpenGLGeometry()
        {
            _openGLvertices = GenerateOpenGLVertices();

            _openGLselectionVertices = GenerateOpenGLSelectionVertices();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Adds a user property to the object, checking for uniqueness.
        /// </summary>
        /// <param name="p">User property to add</param>
        public void AddUserProperty(UserProperty p)
        {
            if (UserProperties != null)
            {
                bool unique = true;

                //Check for uniqueness
                for (int i = 0; i < UserProperties.Count; i++)
                {
                    if (string.Compare(UserProperties[i].PropertyName, p.PropertyName, ignoreCase: true) == 0)
                    {
                        unique = false;
                        break;
                    }
                }

                if (unique == false)
                {
                    throw new Exception($"User property name must be unique.  Another property is already defined with the name '{p.PropertyName}'.");
                }
                else
                {
                    UserProperties.Add(p);
                }
            }
            else
            {
                UserProperties = new ObservableList<UserProperty>();

                UserProperties.Add(p);
            }
        }

        /// <summary>
        /// Clears the user properties from the object
        /// </summary>
        public void ClearUserProperties()
        {
            if (UserProperties != null)
            {
                UserProperties.Clear();
            }
            else
            {
                UserProperties = new ObservableList<UserProperty>();
            }
        }

        /// <summary>
        /// Removes a user property, if it exists
        /// </summary>
        /// <param name="p">User property to remove</param>
        public void RemoveUserProperty(UserProperty p)
        {
            if (UserProperties != null)
            {
                UserProperties.Remove(p);
            }
        }

        /// <summary>
        /// Removes the user property with the specified name, if it exists
        /// </summary>
        /// <param name="name">Name of user property to remove</param>
        public void RemoveUserPoperty(string name)
        {
            if (UserProperties != null)
            {
                int index = -1;

                for (int i = 0; i < UserProperties.Count; i++)
                {
                    if (string.Compare(UserProperties[i].PropertyName, name, ignoreCase:true) == 0)
                    {
                        index = i;
                        break;
                    }
                }

                if (index >= 0)
                {
                    UserProperties.RemoveAt(index);
                }
            }
        }
        #endregion
    }
}
