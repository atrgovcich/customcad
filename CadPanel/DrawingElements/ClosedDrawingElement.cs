﻿using System.Drawing;
using Utilities.Geometry;

namespace CadPanel.DrawingElements
{
    public abstract class ClosedDrawingElement : DrawingElement
    { 
        #region Public Properties
        public bool Hole { get; set; } = false;
        public bool FillShape { get; set; }
        public Color? FillColor { get; set; } = null;
        #endregion

        #region Constructors
        public ClosedDrawingElement() : base()
        {

        }
        #endregion

        #region Public Abstract Methods
        public abstract double Area();

        public abstract PointD Centroid();

        public abstract double Perimeter();
        public abstract double Ixx();
        public abstract double Iyy();
        public abstract double Ixy();
        public abstract double Sxx();
        public abstract double Syy();
        public abstract bool IsFullyWithin(ClosedDrawingElement other);
        public abstract bool ContainsPoint(PointD p);

        #endregion
    }
}
