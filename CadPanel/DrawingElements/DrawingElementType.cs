﻿using System.ComponentModel;

namespace CadPanel.DrawingElements
{
    public enum DrawingElementType
    {
        [Description("Circle")]
        Circle,
        [Description("Arc")]
        Arc,
        [Description("Point")]
        Point,
        [Description("Polygon")]
        Polygon,
        [Description("Polyline")]
        Polyline,
        [Description("Post Tensioning")]
        PostTensioning,
        [Description("Rebar Section")]
        RebarSection
    }
}
