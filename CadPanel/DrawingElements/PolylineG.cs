﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CadPanel.Common;
using static System.Math;
using System.Linq;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using CadPanel.Extensions;
using Utilities.DataStructures.QuadTree;
using FiberSectionAnalysis.Geometry;

using OpenTK;

using Color = System.Drawing.Color;

namespace CadPanel.DrawingElements
{
    public class PolylineG : OpenDrawingElement
    {
        #region Public Properties

        #endregion

        #region Constructors
        public PolylineG() : base()
        {
            Name = "Polyline";
            _elementType = DrawingElementType.Polyline;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns a copy of the polyline element
        /// </summary>
        /// <param name="basePoint">Copy base point</param>
        /// <param name="currentPoint">Current point</param>
        /// <returns></returns>
        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>();

            foreach (DrawingNode node in Nodes)
            {
                DrawingNode copiedNode = new DrawingNode()
                {
                    TypeOfNode = node.TypeOfNode,
                    Point = new PointD(node.Point.X + delta_X, node.Point.Y + delta_Y)
                };

                copiedNodes.Add(copiedNode);
            }

            PolylineG copiedPolyline = new PolylineG()
            {
                Nodes = copiedNodes,
                Layer = Layer,
                Name = Name,
                Selected = false
            };

            return copiedPolyline;
        }

        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            Point[] pixelPoints = new Point[Nodes.Count];

            for (int i = 0; i < Nodes.Count; i++)
            {
                pixelPoints[i] = Nodes[i].Point.ConvertToPixelPoint(zeroPoint, drawingScale);
            }

            if (pixelPoints.GetUpperBound(0) > 0)
            {
                g.DrawLines(myPen, pixelPoints);
            }

            if (Selected == true)
            {
                if (numSelectedElements == 1 || (numSelectedElements > 1 && instanceData.DrawVerticesOfMultipleObjects == true))
                {
                    SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                    for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                    {
                        Rectangle boundingRectangle = new Rectangle
                            (
                                Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                                Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                                instanceData.SelectedObjectPointWidth,
                                instanceData.SelectedObjectPointWidth
                            );

                        g.FillEllipse(myBrush, boundingRectangle);
                    }

                    myBrush.Dispose();
                }
            }

            myPen.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;
                myPen.Width = instanceData.SelectedObjectLineWidth;
            }

            if (IsRebar == true)
            {
                if (Rebar != null)
                {
                    myPen.Width = Convert.ToInt32(Rebar.Diameter * drawingScale);
                }
            }

            List<Point> pixelPointList = new List<Point>();

            foreach (DrawingNode node in Nodes)
            {
                pixelPointList.Add(node.Point.ConvertToPixelPoint(zeroPoint, drawingScale));
            }

            if (currentPoint != null)
            {
                if (currentPoint != Nodes[Nodes.Count - 1].Point)
                {
                    pixelPointList.Add(currentPoint.ConvertToPixelPoint(zeroPoint, drawingScale));
                }
            }

            Point[] pixelPoints = pixelPointList.ToArray();

            if (pixelPoints.GetUpperBound(0) > 0)
            {
                g.DrawLines(myPen, pixelPoints);
            }

            if (Selected == true)
            {
                SolidBrush myBrush = new SolidBrush(instanceData.SelectedObjectPointColor);

                for (int i = 0; i <= pixelPoints.GetUpperBound(0); i++)
                {
                    Rectangle boundingRectangle = new Rectangle
                        (
                            Convert.ToInt32(pixelPoints[i].X - instanceData.SelectedObjectPointWidth / 2.0),
                            Convert.ToInt32(pixelPoints[i].Y - instanceData.SelectedObjectPointWidth / 2.0),
                            instanceData.SelectedObjectPointWidth,
                            instanceData.SelectedObjectPointWidth
                        );

                    g.FillEllipse(myBrush, boundingRectangle);
                }

                myBrush.Dispose();
            }

            myPen.Dispose();
        }

        public override Rect BoundingRectangle()
        {
            double minX = 0, minY = 0, maxX = 0, maxY = 0;

            int count = 0;

            foreach (DrawingNode node in Nodes)
            {
                if (count == 0)
                {
                    minX = node.Point.X;
                    minY = node.Point.Y;
                    maxX = node.Point.X;
                    maxY = node.Point.Y;
                }
                else
                {
                    if (node.Point.X < minX)
                    {
                        minX = node.Point.X;
                    }

                    if (node.Point.Y < minY)
                    {
                        minY = node.Point.Y;
                    }

                    if (node.Point.X > maxX)
                    {
                        maxX = node.Point.X;
                    }

                    if (node.Point.Y > maxY)
                    {
                        maxY = node.Point.Y;
                    }
                }

                count++;
            }

            double tolerance = 0.1; //This is a tolerance value that is added to or subtracted from the max / min values, respectively.  This prevents a 0 width or height value.

            int x0 = Convert.ToInt32(Floor(minX - tolerance));
            int y0 = Convert.ToInt32(Floor(minY - tolerance));

            int width = Convert.ToInt32(Ceiling((maxX + tolerance) - x0));
            int height = Convert.ToInt32(Ceiling((maxY + tolerance) - y0));

            return new Rect(x0, y0, width, height);
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {
            if (nodeType.Equals(NodeType.EndPoint) == true)
            {
                double closestDist = 1.0E10;

                PointD closestPoint = null;

                foreach (DrawingNode node in Nodes)
                {
                    if (globalPoint.DistanceTo(node.Point) <= closestDist)
                    {
                        closestDist = globalPoint.DistanceTo(node.Point);
                        closestPoint = node.Point;
                    }
                }

                return closestPoint;
            }
            else
            {
                return null;
            }
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            List<LineSegment> edgeList = Edges();

            for (int i = 0; i < edgeList.Count; i++)
            {
                if (edgeList[i].IntersectsWith(line, out PointD inters) == true)
                {
                    bool unique = true;

                    for (int j = 0; j < intersections.Count; j++)
                    {
                        if (intersections[j] == inters)
                        {
                            unique = false;
                            break;
                        }
                    }

                    if (unique == true)
                    {
                        intersections.Add(inters);
                    }

                    intersects = true;
                }
            }

            return intersects;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                LineSegment seg = edges[i];

                if (seg.IntersectsWithInfiniteLine(line, out PointD infIntersection) == true)
                {
                    bool unique = true;

                    for (int j = 0; j < intersections.Count; j++)
                    {
                        if (intersections[j] == infIntersection)
                        {
                            unique = false;

                            break;
                        }
                    }

                    if (unique == true)
                    {
                        intersections.Add(infIntersection);
                    }
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                return JoinWithOtherPolyline((PolylineG)other, out newElement);
            }
            else
            {
                newElement = null;

                return false;
            }
        }

        public override List<LineSegment> Edges()
        {
            List<LineSegment> segments = new List<LineSegment>();

            for (int i = 0; i < Nodes.Count - 1; i++)
            {
                LineSegment seg = new LineSegment(Nodes[i], Nodes[i + 1]);

                segments.Add(seg);
            }

            return segments;
        }

        public override List<IDrawingElement> Explode()
        {
            List<IDrawingElement> explodedElements = new List<IDrawingElement>();

            List<LineSegment> edges = Edges();

            if (edges.Count > 1)
            {
                for (int i = 0; i < edges.Count; i++)
                {
                    DrawingNode n0 = edges[i].StartNode.CreateCopy();

                    DrawingNode n1 = edges[i].EndNode.CreateCopy();

                    PolylineG pl = new PolylineG()
                    {
                        Nodes = new List<DrawingNode>(2) { n0, n1 },
                        Layer = Layer,
                        IsRebar = IsRebar,
                        Rebar = Rebar,
                        Selected = false
                    };

                    explodedElements.Add(pl);
                }
            }

            return explodedElements;
        }

        public override bool CloseShape(out IDrawingElement newClosedElement)
        {
            newClosedElement = null;

            if (Nodes != null && Nodes.Count > 2)
            {
                //There must be at least 3 nodes to close the shape

                List<DrawingNode> copiedNodes = null; 

                //First check if the first point equals the last point
                if (Nodes[0].Point == Nodes[Nodes.Count - 1].Point)
                {
                    copiedNodes = new List<DrawingNode>(Nodes.Count);

                    //The polyline first and last points are the same.
                    //No need to add another segment
                    for (int i = 0;i < Nodes.Count - 1; i++)
                    {
                        copiedNodes.Add(Nodes[i].CreateCopy());
                    }

                    //Add the first node as the last node
                    copiedNodes.Add(copiedNodes[0]);
                }
                else
                {
                    copiedNodes = new List<DrawingNode>(Nodes.Count + 1);

                    //The polyline first and last points are not the same
                    //Need to add the last segment
                    for (int i = 0; i < Nodes.Count; i++)
                    {
                        copiedNodes.Add(Nodes[i].CreateCopy());
                    }

                    //Add the first node as the last node
                    copiedNodes.Add(copiedNodes[0]);
                }

                newClosedElement = new PolygonG()
                {
                    Nodes = copiedNodes,
                    Layer = Layer,
                    Rebar = Rebar,
                    IsRebar = IsRebar,
                };

                return true;
            }

            return false;
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            List<LineSegment> edges = Edges();

            double minDist = orthogonalTolerance * 10.0;

            bool liesOnBoundary = false;

            for (int i = 0; i < edges.Count; i++)
            {
                if (p.LiesOnLine(edges[i], out double distToSeg, orthogonalTolerance) == true)
                {
                    liesOnBoundary = true;

                    minDist = Min(minDist, distToSeg);
                }
            }

            if (liesOnBoundary == false)
            {
                orthogonalDistance = -1;
            }
            else
            {
                orthogonalDistance = minDist;
            }

            return liesOnBoundary;
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.GetType().Equals(typeof(CircleG)) == true || other.GetType().IsSubclassOf(typeof(CircleG)) == true)
            {
                intersects = ((CircleG)other).IntersectsWithPolyline(this, out intersections);
            }
            if (other.GetType().Equals(typeof(PostTensioningG)) == true)
            {
                intersects = IntersectsWithPostTensioning((PostTensioningG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(ArcG)) == true)
            {
                intersects = ((ArcG)other).IntersectsWithPolyline(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolygonG)) == true || other.GetType().IsSubclassOf(typeof(PolygonG)) == true)
            {
                intersects = ((PolygonG)other).IntersectsWithPolyline(this, out intersections);
            }
            else if (other.GetType().Equals(typeof(PolylineG)) == true)
            {
                intersects = IntersectsWithPolyline((PolylineG)other, out intersections);
            }
            else if (other.GetType().Equals(typeof(PointG)) == true)
            {
                intersects = ((PointG)other).IntersectsWithElement(this, out intersections);
            }

            //Eliminate any duplicates
            int count = 0;

            int numInt = intersections.Count;

            while (count < numInt - 1)
            {
                int innerCount = count + 1;

                while (innerCount < numInt)
                {
                    if (intersections[count] == intersections[innerCount])
                    {
                        intersections.RemoveAt(innerCount);

                        innerCount--;

                        numInt--;
                    }

                    innerCount++;
                }

                count++;
            }

            return intersects;
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                LineSegment edge = edges[i];

                PointD perpPoint = edge.GetClosestPerpendicularPointToInfiniteExtension(basePoint);

                DrawingNode n0 = new DrawingNode(basePoint, NodeType.EndPoint);

                DrawingNode n1 = new DrawingNode(perpPoint, NodeType.EndPoint);

                LineSegment perpLine = new LineSegment(n0, n1);

                if (edge.IntersectsWithInfiniteLine(perpLine, out PointD edgeIntersection) == true)
                {
                    intersections.Add(edgeIntersection);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Offsets the element by the offset distance
        /// </summary>
        /// <param name="offsetDistance">Offset distance</param>
        /// <param name="direction">For left offset, specify 1; for right offset, specify -1</param>
        /// <returns></returns>
        public override IDrawingElement Offset(double offsetDistance, PointD globalPoint)
        {
            LineSegment closestSegment = GetClosestSegmentTo(globalPoint);

            int direction = 1;

            if (closestSegment.WhichSide(globalPoint) == Side.Right)
            {
                direction = -1;
            }
            else if (closestSegment.WhichSide(globalPoint) == Side.Colinear)
            {
                return null;
            }

            double tolerance = 1.0 * Pow(10, -_precision);

            List<PointD> pointList = Nodes.Select(x => x.Point).ToList(); ;

            List<PointD> offsetPointList = new List<PointD>(pointList.Count + 2);

            Vector firstSegVector = new Vector(pointList[1].X - pointList[0].X, pointList[1].Y - pointList[0].Y);
            firstSegVector = firstSegVector.UnitVector();
            firstSegVector = firstSegVector.Reverse();

            Vector lastSegVector = new Vector(pointList[pointList.Count - 1].X - pointList[pointList.Count - 2].X, pointList[pointList.Count - 1].Y - pointList[pointList.Count - 2].Y);
            lastSegVector = lastSegVector.UnitVector();

            pointList.Insert(0, new PointD(pointList[0].X + firstSegVector.X * 1.0, pointList[0].Y + firstSegVector.Y * 1.0));
            pointList.Add(new PointD(pointList[pointList.Count - 1].X + lastSegVector.X * 1.0, pointList[pointList.Count - 1].Y + lastSegVector.Y * 1.0));

            for (int i = 1; i < pointList.Count - 1; i++)
            {
                PointD prevPoint;
                PointD point;
                PointD nextPoint;

                int prevIndex = 0;
                int index = 0;
                int nextIndex = 0;

                prevIndex = i - 1;
                index = i;
                nextIndex = i + 1;

                prevPoint = pointList[prevIndex];
                point = pointList[index];
                nextPoint = pointList[nextIndex];

                DrawingNode prevNode = new DrawingNode(prevPoint, NodeType.EndPoint);
                DrawingNode currentNode = new DrawingNode(point, NodeType.EndPoint);
                DrawingNode nextNode = new DrawingNode(nextPoint, NodeType.EndPoint);

                LineSegment prevSeg = new LineSegment(prevNode, currentNode);
                LineSegment nextSeg = new LineSegment(currentNode, nextNode);

                double angle1 = point.GetAngleTo(nextPoint);
                double angle2 = point.GetAngleTo(prevPoint);

                double avgAngle = (angle1 + angle2) / 2d;

                double angleDiff = angle2 - angle1;

                double halfTheta = angleDiff / 2d;

                double totalDist = Abs(offsetDistance / Sin(halfTheta));

                PointD newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));

                PointD newPointOrig = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));

                if (direction == 1)
                {
                    //offset inside
                    if (prevSeg.WhichSide(newPoint) == Side.Left)
                    {
                        //if the new point is on the correct side
                    }
                    else
                    {
                        //The point was accidentally offset to the opposite side, so flip the average angle (subtract 180 degrees, or PI in this case)
                        avgAngle = avgAngle - PI;
                        newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));
                    }
                }
                else if (direction == -1)
                {
                    //offset outside
                    if (prevSeg.WhichSide(newPoint) == Side.Right)
                    {
                        //If the new point is on the correct side
                    }
                    else
                    {
                        //The point was accidentally offset to the outside, so flip the average angle (subtract 180 degrees, or PI in this case)
                        avgAngle = avgAngle - PI;
                        newPoint = new PointD(point.X + totalDist * Cos(avgAngle), point.Y + totalDist * Sin(avgAngle));
                    }
                }

                offsetPointList.Add(newPoint);
            }

            List<LineSegment> edgeList = new List<LineSegment>();

            for (int i = 0; i < offsetPointList.Count - 1; i++)
            {
                PointD point;
                PointD nextPoint;

                int index = 0;
                int nextIndex = 0;

                index = i;
                nextIndex = i + 1;

                point = offsetPointList[index];
                nextPoint = offsetPointList[nextIndex];

                PointD origPoint = pointList[index + 1];
                PointD origNextPoint = pointList[nextIndex + 1];

                Vector unitVectorOrig = new Vector(origNextPoint.X - origPoint.X, origNextPoint.Y - origPoint.Y);
                unitVectorOrig = unitVectorOrig.UnitVector();

                Vector unitVectorOffset = new Vector(nextPoint.X - point.X, nextPoint.Y - point.Y);
                unitVectorOffset = unitVectorOffset.UnitVector();

                if (Sign(Round(unitVectorOrig.X, 4)) == Sign(Round(unitVectorOffset.X, 4)) && Sign(Round(unitVectorOrig.Y, 4)) == Sign(Round(unitVectorOffset.Y, 4)))
                {
                    edgeList.Add(new LineSegment(new DrawingNode(point, NodeType.EndPoint), new DrawingNode(nextPoint, NodeType.EndPoint)));
                }
            }

            Vector firstSegVectorOffset = new Vector(offsetPointList[1].X - offsetPointList[0].X, offsetPointList[1].Y - offsetPointList[0].Y);
            firstSegVectorOffset = firstSegVectorOffset.UnitVector();
            firstSegVectorOffset = firstSegVectorOffset.Reverse();

            Vector lastSegVectorOffset = new Vector(offsetPointList[offsetPointList.Count - 1].X - offsetPointList[offsetPointList.Count - 2].X, offsetPointList[offsetPointList.Count - 1].Y - offsetPointList[offsetPointList.Count - 2].Y);
            lastSegVectorOffset = lastSegVectorOffset.UnitVector();

            DrawingNode firstOffsetNode = new DrawingNode(new PointD(offsetPointList[0].X + firstSegVectorOffset.X * 1.0, offsetPointList[0].Y + firstSegVectorOffset.Y * 1.0), NodeType.EndPoint);
            DrawingNode lastOffsetNode = new DrawingNode(new PointD(offsetPointList[offsetPointList.Count - 1].X + lastSegVectorOffset.X * 1.0, offsetPointList[offsetPointList.Count - 1].Y + lastSegVectorOffset.Y * 1.0), NodeType.EndPoint);

            LineSegment firstOffsetSeg = new LineSegment(firstOffsetNode, edgeList[0].StartNode);
            LineSegment lastOffsetSeg = new LineSegment(edgeList[edgeList.Count - 1].EndNode, lastOffsetNode);

            edgeList.Insert(0, firstOffsetSeg);
            edgeList.Add(lastOffsetSeg);

            for (int i = 1; i < edgeList.Count - 1; i++)
            {
                LineSegment nextEdge;
                LineSegment prevEdge;

                int prevEdgeIndex = 0;
                int nextEdgeIndex = 0;

                prevEdge = edgeList[i - 1];
                nextEdge = edgeList[i + 1];

                prevEdgeIndex = i - 1;
                nextEdgeIndex = i + 1;

                if (edgeList[i].IntersectsWith(nextEdge, out PointD intersectionNext) == true)
                {
                    if (edgeList[i].StartNode.Point.DistanceTo(intersectionNext) <= tolerance)
                    {
                        //The intersection is the first point of the current line, this shouldn't happen
                        throw new Exception("Line intersection of the current edge and the next edge was found at the first point of the current edge.");
                    }
                    else if (edgeList[i].EndNode.Point.DistanceTo(intersectionNext) <= tolerance)
                    {
                        //This means we are good, do nothing
                    }
                    else
                    {
                        //the intersection is somewhere in the line
                        edgeList[i] = new LineSegment(edgeList[i].StartNode, new DrawingNode(intersectionNext, NodeType.EndPoint));
                        edgeList[nextEdgeIndex] = new LineSegment(new DrawingNode(intersectionNext, NodeType.EndPoint), edgeList[nextEdgeIndex].EndNode);
                    }
                }

                if (edgeList[i].IntersectsWith(prevEdge, out PointD intersectionPrev) == true)
                {
                    if (edgeList[i].StartNode.Point.DistanceTo(intersectionPrev) <= tolerance)
                    {
                        //This means we are good, do nothing
                    }
                    else if (edgeList[i].EndNode.Point.DistanceTo(intersectionPrev) <= tolerance)
                    {
                        //The intersection is the first point of the current line, this shouldn't happen
                        throw new Exception("Line intersection of the current edge and the next edge was found at the first point of the current edge.");
                    }
                    else
                    {
                        //the intersection is somewhere in the line
                        edgeList[i] = new LineSegment(new DrawingNode(intersectionPrev, NodeType.EndPoint), edgeList[i].EndNode);
                        edgeList[prevEdgeIndex] = new LineSegment(edgeList[nextEdgeIndex].StartNode, new DrawingNode(intersectionPrev, NodeType.EndPoint));
                    }
                }
            }

            List<DrawingNode> checkedOffsetPolygon = new List<DrawingNode>();

            for (int i = 1; i < edgeList.Count; i++)
            {
                LineSegment seg = edgeList[i];

                checkedOffsetPolygon.Add(seg.StartNode);
            }

            PolylineG offsetPolyline = new PolylineG()
            {
                Nodes = checkedOffsetPolygon,
                Layer = Layer,
                IsRebar = IsRebar,
                Rebar = Rebar,
            };

            return offsetPolyline;
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            return this;
        }

        public override double Length()
        {
            List<LineSegment> edges = Edges();

            double total = 0;

            for (int i = 0; i < edges.Count; i++)
            {
                total += edges[i].Length();
            }

            return total;
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            PolylineFS pFS = new PolylineFS(this.Nodes.Select(x => x.Point).ToList());
            pFS.IsRebar = this.IsRebar;

            return pFS;
        }
        #endregion

        #region Public Methods


        public List<PointD> GetIntersectionWithInfiniteVector(PointD pA, Vector v)
        {
            PointD pB = new PointD(pA.X + v.X, pA.Y + v.Y);

            LineSegment line = new LineSegment(new DrawingNode(pA, NodeType.EndPoint), new DrawingNode(pB, NodeType.EndPoint));

            return GetIntersectionWithInfiniteLine(line);
        }

        public List<PointD> GetIntersectionWithInfiniteLine(LineSegment line)
        {
            PointD pA = line.StartNode.Point;
            PointD pB = line.EndNode.Point;

            List<PointD> intersections = new List<PointD>();

            foreach (LineSegment edge in this.Edges())
            {
                PointD p0 = edge.StartNode.Point;
                PointD p1 = edge.EndNode.Point;

                if (p0.X == p1.X)
                {
                    //the edge is a vertical line
                    if (pA.X != pB.X)
                    {
                        //the input line is NOT a vertical line
                        double m2 = (pB.Y - pA.Y) / (pB.X - pA.X);
                        double b2 = pA.Y - m2 * pA.X;

                        double intX = p0.X;
                        double intY = m2 * intX + b2;

                        PointD intersection = new PointD(intX, intY);

                        if (intersection.Y >= Min(p0.Y, p1.Y) && intersection.Y <= Max(p0.Y, p1.Y))
                        {
                            bool unique = true;

                            foreach (PointD p in intersections)
                            {
                                if (p.X == intersection.X && p.Y == intersection.Y)
                                {
                                    unique = false;
                                    break;
                                }
                            }

                            if (unique == true)
                            {
                                intersections.Add(intersection);
                            }
                        }
                    }
                }
                else
                {
                    //the edge is not a vertical line
                    double m1 = (p1.Y - p0.Y) / (p1.X - p0.X);
                    double b1 = p0.Y - m1 * p0.X;

                    if (pA.X == pB.X)
                    {
                        //the input line is a vertical line
                        double intX = pA.X;
                        double intY = m1 * intX + b1;

                        PointD intersection = new PointD(intX, intY);

                        if (intersection.X >= Min(p0.X, p1.X) && intersection.X <= Max(p0.X, p1.X))
                        {
                            bool unique = true;

                            foreach (PointD p in intersections)
                            {
                                if (p.X == intersection.X && p.Y == intersection.Y)
                                {
                                    unique = false;
                                    break;
                                }
                            }

                            if (unique == true)
                            {
                                intersections.Add(intersection);
                            }
                        }
                    }
                    else
                    {
                        double m2 = (pB.Y - pA.Y) / (pB.X - pA.X);
                        double b2 = pA.Y - m2 * pA.X;

                        if (m1 != m2)
                        {
                            double intX = (b2 - b1) / (m1 - m2);
                            double intY = m1 * intX + b1;

                            PointD intersection = new PointD(intX, intY);

                            if (intersection.X >= Min(p0.X, p1.X) && intersection.X <= Max(p0.X, p1.X))
                            {
                                //intersection piont lies within the edge bounds
                                bool unique = true;

                                foreach (PointD p in intersections)
                                {
                                    if (p.X == intersection.X && p.Y == intersection.Y)
                                    {
                                        unique = false;
                                        break;
                                    }
                                }

                                if (unique == true)
                                {
                                    intersections.Add(intersection);
                                }
                            }
                        }
                    }
                }
            }

            return intersections;
        }

        public bool IntersectsWithPostTensioning(PostTensioningG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = this.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (other.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }

        public bool IntersectsWithPolyline(PolylineG other, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            List<LineSegment> edges = this.Edges();

            for (int i = 0; i < edges.Count; i++)
            {
                if (other.IntersectsWithLine(edges[i], out List<PointD> edgeIntersections) == true)
                {
                    intersections.AddRange(edgeIntersections);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region Protected Methods
        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.OpenGeometry,
                ElementPropertyType.Rebar,
                ElementPropertyType.UserProperties
            };
        }

        protected bool JoinWithOtherPolyline(PolylineG other, out IDrawingElement joinedElement)
        {
            List<DrawingNode> pL1 = Nodes;
            List<DrawingNode> pL2 = other.Nodes;

            DrawingNode sP1 = pL1[0];
            DrawingNode eP1 = pL1[pL1.Count - 1];

            DrawingNode sP2 = pL2[0];
            DrawingNode eP2 = pL2[pL2.Count - 1];

            List<DrawingNode> newNodeList = new List<DrawingNode>();

            joinedElement = null;

            bool joined = false;

            if (sP1.Point == sP2.Point)
            {
                for (int i = pL1.Count - 1; i >= 0; i--)
                {
                    newNodeList.Add(pL1[i]);
                }

                for (int i = 1; i < pL2.Count; i++)
                {
                    newNodeList.Add(pL2[i]);
                }

                joined = true;
            }
            else if (sP1.Point == eP2.Point)
            {
                for (int i = 0; i < pL2.Count; i++)
                {
                    newNodeList.Add(pL2[i]);
                }

                for (int i = 1; i < pL1.Count; i++)
                {
                    newNodeList.Add(pL1[i]);
                }

                joined = true;
            }
            else if (sP2.Point == eP1.Point)
            {
                for (int i = 0; i < pL1.Count; i++)
                {
                    newNodeList.Add(pL1[i]);
                }

                for (int i = 1; i < pL2.Count; i++)
                {
                    newNodeList.Add(pL2[i]);
                }

                joined = true;
            }
            else if (eP1.Point == eP2.Point)
            {
                for (int i = 0; i < pL1.Count; i++)
                {
                    newNodeList.Add(pL1[i]);
                }

                for (int i = pL2.Count - 2; i >= 0; i--)
                {
                    newNodeList.Add(pL2[i]);
                }

                joined = true;
            }

            if (joined == true)
            {
                if (newNodeList[0].Point == newNodeList[newNodeList.Count - 1].Point)
                {
                    newNodeList[newNodeList.Count - 1] = newNodeList[0];

                    joinedElement = new PolygonG() { Nodes = newNodeList, Layer = Layer, Rebar = Rebar, IsRebar = IsRebar };
                }
                else
                {
                    joinedElement = new PolylineG() { Nodes = newNodeList, Layer = Layer, Rebar = Rebar, IsRebar = IsRebar };
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Public Methods
        public LineSegment GetClosestSegmentTo(PointD p)
        {
            List<LineSegment> edges = Edges();

            LineSegment closestSegment = null;

            double closestDist = 0;

            for (int i = 0; i < edges.Count; i++)
            {
                PointD perpPoint = edges[i].GetClosestPerpendicularPointToInfiniteExtension(p);

                if (i == 0)
                {
                    closestDist = p.DistanceTo(perpPoint);

                    closestSegment = edges[i];
                }
                else
                {
                    double dist = p.DistanceTo(perpPoint);

                    if (dist< closestDist)
                    {
                        closestSegment = edges[i];
                    }
                }
            }

            return closestSegment;
        }
        #endregion

        #region OpenGL
        public override List<Vector2> GenerateOpenGLVertices()
        {
            if (Nodes.Count > 1)
            {
                int listSize = ((Nodes.Count - 2) * 2 + 2);

                List<Vector2> vbo = new List<Vector2>(listSize);
                List<float> cbo = new List<float>(listSize);

                for (int i = 0; i < Nodes.Count - 1; i++)
                {
                    float x = (float)Nodes[i].Point.X;
                    float y = (float)Nodes[i].Point.Y;

                    vbo.Add(new Vector2(x, y));

                    x = (float)Nodes[i + 1].Point.X;
                    y = (float)Nodes[i + 1].Point.Y;

                    vbo.Add(new Vector2(x, y));
                }

                return vbo;
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
