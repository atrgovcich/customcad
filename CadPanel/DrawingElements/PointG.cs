﻿using System;
using System.Collections.Generic;
using CadPanel.Common;
using System.Drawing;
using static System.Math;
using static CadPanel.Common.CommonMethods;
using Utilities.Geometry;
using CadPanel.Extensions;
using Utilities.DataStructures.QuadTree;

using Color = System.Drawing.Color;
using CadPanel.PropertyPanel;

namespace CadPanel.DrawingElements
{
    public class PointG : DrawingElement
    {
        #region Public Properties

        #endregion

        #region Constructors
        public PointG() : base()
        {
            Name = "Point";
            _elementType = DrawingElementType.Point;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns a copy of the polyline element
        /// </summary>
        /// <param name="basePoint">Copy base point</param>
        /// <param name="currentPoint">Current point</param>
        /// <returns></returns>
        public override IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint)
        {
            double delta_X = currentPoint.X - basePoint.X;
            double delta_Y = currentPoint.Y - basePoint.Y;

            List<DrawingNode> copiedNodes = new List<DrawingNode>();

            foreach (DrawingNode node in Nodes)
            {
                DrawingNode copiedNode = new DrawingNode()
                {
                    TypeOfNode = node.TypeOfNode,
                    Point = new PointD(node.Point.X + delta_X, node.Point.Y + delta_Y)
                };

                copiedNodes.Add(copiedNode);
            }

            PointG copiedPoint = new PointG()
            {
                Nodes = copiedNodes,
                Layer = Layer,
                Name = Name,
                Selected = false
            };

            return copiedPoint;
        }

        public override void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            Color brushColor = Color.FromArgb(200, Layer.Color.R, Layer.Color.G, Layer.Color.B);

            SolidBrush myBrush = new SolidBrush(brushColor);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;

                myPen.Width = instanceData.SelectedObjectLineWidth;

                brushColor = Color.FromArgb(200, instanceData.SelectedObjectPointColor.R, instanceData.SelectedObjectPointColor.G, instanceData.SelectedObjectPointColor.B);

                myBrush.Color = brushColor;
            }

            Point cp = ConvertToPixelCoord(Node().Point, zeroPoint, drawingScale);

            int r = Convert.ToInt32(instanceData.PointObjectSize / 2.0);

            Point topLeft = new Point(cp.X - r, cp.Y - r);

            Rectangle rec = new Rectangle(topLeft, new Size(Convert.ToInt32(r), Convert.ToInt32(r)));

            if (topLeft != null)
            {

                g.FillEllipse(myBrush, rec);

                g.DrawEllipse(myPen, rec);
            }

            myPen.Dispose();

            myBrush.Dispose();
        }

        public override void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData)
        {
            Pen myPen = new Pen(Layer.Color, Layer.Width);

            Color brushColor = Color.FromArgb(200, Layer.Color.R, Layer.Color.G, Layer.Color.B);

            SolidBrush myBrush = new SolidBrush(brushColor);

            if (Selected == true)
            {
                myPen.Color = instanceData.SelectedObjectLineColor;

                myPen.Width = instanceData.SelectedObjectLineWidth;

                brushColor = Color.FromArgb(200, instanceData.SelectedObjectPointColor.R, instanceData.SelectedObjectPointColor.G, instanceData.SelectedObjectPointColor.B);

                myBrush.Color = brushColor;
            }

            Point cp = ConvertToPixelCoord(currentPoint, zeroPoint, drawingScale);

            int r = Convert.ToInt32(instanceData.PointObjectSize / 2.0);

            Point topLeft = new Point(cp.X - r, cp.Y - r);

            Rectangle rec = new Rectangle(topLeft, new Size(Convert.ToInt32(r), Convert.ToInt32(r)));

            if (topLeft != null)
            {

                g.FillEllipse(myBrush, rec);

                g.DrawEllipse(myPen, rec);
            }

            myPen.Dispose();

            myBrush.Dispose();
        }

        public override Rect BoundingRectangle()
        {
            double x = Node().Point.X;
            double y = Node().Point.Y;
            
            int x0 = Convert.ToInt32(Floor(x - 0.25));
            int y0 = Convert.ToInt32(Floor(y - 0.25));

            int width = Convert.ToInt32(Ceiling((x + 0.25) - x0));
            int height = Convert.ToInt32(Ceiling((y + 0.25) - y0));

            return new Rect(x0, y0, width, height);
        }

        public override PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType)
        {

            if (nodeType.Equals(NodeType.CenterPoint) == true)
            {
                return Node().Point;
            }
            else if (nodeType.Equals(NodeType.EndPoint) == true)
            {
                return Node().Point;
            }
            else
            {
                return null;
            }
        }

        public override bool IntersectsWithLine(LineSegment line, out List<PointD> intersections)
        {
            bool intersects = this.LiesOnLine(line);

            if (intersects == true)
            {
                intersections = new List<PointD>(1) { Nodes[0].Point };
            }
            else
            {
                intersections = null;
            }

            return intersects;
        }

        public override bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections)
        {
            intersections = new List<PointD>();

            PointD v0 = line.StartNode.Point;
            PointD v1 = line.EndNode.Point;

            PointD v2 = Nodes[0].Point;

            if (Round(v1.X - v0.X, _precision) == 0)
            {
                if (Round(v2.X - v0.X, _precision) == 0)
                {
                    intersections.Add(v2);
                }
            }
            else
            {
                double m = (v1.Y - v0.Y) / (v1.X - v0.X);
                double b = v0.Y - m * v0.X;

                if (Round(v2.Y - (m * v2.X + b), _precision) == 0)
                {
                    intersections.Add(v2);
                }
                else if (Round(v2.Y - v0.Y, _precision) == 0 && Round(v2.X - v0.X, _precision) == 0)
                {
                    intersections.Add(v2);
                }
                else if (Round(v2.Y - v1.Y, _precision) == 0 && Round(v2.X - v1.X, _precision) == 0)
                {
                    intersections.Add(v2);
                }
            }

            if (intersections.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool JoinWith(IDrawingElement other, out IDrawingElement newElement)
        {
            //Points can't be joine with other elements
            newElement = null;
            return false;
        }

        public override List<IDrawingElement> Explode()
        {
            return new List<IDrawingElement>();
        }

        public override bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance)
        {
            if (Nodes[0].Point.IsSameAsOtherPointWithinTolerance(p, orthogonalTolerance, out double distToOther) == true)
            {
                orthogonalDistance = distToOther;

                return true;
            }
            else
            {
                orthogonalDistance = -1;

                return false;
            }
        }

        public override bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections)
        {
            bool intersects = false;

            intersections = new List<PointD>();

            if (other.PointLiesOnElementBoundary(this.Node().Point, 1.0 * Pow(10.0, -_precision), out _) == true)
            {
                intersects = true;

                intersections.Add(this.Node().Point);
            }

            return intersects;
        }

        public override bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections)
        {
            intersections = new List<PointD>() { this.Node().Point };

            return true;
        }

        public override IDrawingElement GetDiscretizedPoly(double dist)
        {
            return this;
        }

        public override GenericShape ConvertToFiberSectionElement()
        {
            return null;
        }
        #endregion

        #region Public Methods
        public DrawingNode Node()
        {
            if (Nodes.Count == 1)
            {
                return Nodes[0];
            }
            else
            {
                throw new Exception("Point found with more than one node");
            }
        }

        public override List<LineSegment> Edges()
        {
            return new List<LineSegment>();
        }

        /// <summary>
        /// Returns true if the point v2 lies on the lined defined by v0 and v1
        /// </summary>
        /// <param name="line">Start point of line</param>
        /// <param name="orthogonalTolerance">Optional: Tolerance when comparing x and/or y coordinates</param>
        /// <returns></returns>
        public bool LiesOnLine(LineSegment line, double orthogonalTolerance = 0.0001)
        {
            PointD v2 = Nodes[0].Point;

            return v2.LiesOnLine(line, orthogonalTolerance);
        }


        #endregion

        #region Protected Methods
        protected override void PopulatePropertyTypesList()
        {
            PropertyTypeList = new List<ElementPropertyType>()
            {
                ElementPropertyType.General,
                ElementPropertyType.Nodes,
                ElementPropertyType.UserProperties
            };
        }
        #endregion
    }
}
