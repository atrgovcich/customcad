﻿using System.Collections.Generic;
using Materials;
using CadPanel.Common;
using System.Drawing;
using CadPanel.PropertyPanel;
using Utilities.Geometry;
using Utilities.DataStructures.QuadTree;
using ReinforcedConcrete.Reinforcement;
using OpenTK;
using Utilities.Containers;
using CadPanel.UserProperties;

using Point = System.Drawing.Point;

namespace CadPanel.DrawingElements
{
    public interface IDrawingElement
    {
        List<DrawingNode> Nodes { get; set; }
        CadPanel.Common.Layer Layer { get; set; }
        bool Selected { get; set; }
        string Name { get; set; }
        List<ElementPropertyType> PropertyTypeList { get; }
        DrawingElementType ElementType { get; }
        bool IsRebar { get; set; }
        RebarDefinition Rebar { get; set; }
        IGenericMaterial Material { get; set; }

        void Draw(ref Graphics g, Point zeroPoint, double drawingScale, CadPanelInstanceData instanceData, int numSelectedElements);
        void DrawTemp(ref Graphics g, Point zeroPoint, PointD currentPoint, double drawingScale, CadPanelInstanceData instanceData);
        void Move(PointD basePoint, PointD currentPoint);
        void Rotate(PointD basePoint, PointD currentPoint);
        void RotateThroughAngle(PointD basePoint, double angle);
        IDrawingElement CreateCopy(PointD basePoint, PointD currentPoint);
        Rect BoundingRectangle();
        PointD ClosestNodalPointToPoint(PointD globalPoint, NodeType nodeType);
        List<LineSegment> Edges();
        List<PointD> QuadrantPoints();
        PointD BasePoint();
        ObservableList<UserProperty> UserProperties { get; set; }
        bool IsContainedWithin(Rect rectangle);
        bool IntersectsWithLine(LineSegment line, out List<PointD> intersections);
        bool JoinWith(IDrawingElement other, out IDrawingElement newElement);
        bool IntersectsWithInfiniteLine(LineSegment line, out List<PointD> intersections);
        bool PointLiesOnElementBoundary(PointD p, double orthogonalTolerance, out double orthogonalDistance);
        List<IDrawingElement> Explode();
        void ActionAfterReshape();
        bool ValidateElement();
        bool CloseShape(out IDrawingElement newClosedElement);
        bool IntersectsWithElement(IDrawingElement other, out List<PointD> intersections);
        bool HasPerpendicularIntersection(PointD basePoint, out List<PointD> intersections);
        IDrawingElement Offset(double offsetDistance, PointD globalPoint);
        IDrawingElement GetDiscretizedPoly(double dist);

        GenericShape ConvertToFiberSectionElement();

        void GetOpenGLBuffers(CadPanelInstanceData instanceData, out List<Vector2> vertexBuffer, out List<float> colorBuffer);
        List<Vector2> OpenGLVertices { get; }
        List<Vector2> OpenGLSelectionVertices { get; }

    }
}
