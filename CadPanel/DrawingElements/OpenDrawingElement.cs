﻿namespace CadPanel.DrawingElements
{
    public abstract class OpenDrawingElement : DrawingElement
    {
        public abstract double Length();

    }
}
