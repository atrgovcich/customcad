﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.Common;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.Snapping
{
    public class SnapDefinition
    {
        public PointD SnapPoint { get; set; } = null;
        public SnapType SnapType { get; set; } = SnapType.EndPoint;

        public SnapDefinition()
        {

        }

        public SnapDefinition(PointD point, SnapType nodeType)
        {
            SnapPoint = point;
            SnapType = nodeType;
        }
    }
}
