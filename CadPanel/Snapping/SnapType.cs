﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPanel.Snapping
{
    public enum SnapType
    {
        EndPoint,
        CenterPoint,
        MidPoint,
        QuadrantPoint,
        Intersection,
        Perpendicular,
        ApparentOrthoIntersection,
        Ortho
    }
}
