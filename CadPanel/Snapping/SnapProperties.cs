﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPanel.Snapping
{
    public class SnapProperties
    {
        public bool PointSnapsOn { get; set; } = true;

        public bool SnapEndPoints { get; set; } = true;
        public bool SnapMidPoints { get; set; } = true;
        public bool SnapOrtho { get; set; } = true;
        public bool SnapCenter { get; set; } = true;
        public bool SnapQuadrant { get; set; } = true;
        public bool SnapIntersection { get; set; } = true;
        public bool SnapPerpendicular { get; set; } = true;
        public bool SnapApparentOrthoIntersection { get; set; } = true;

        public double BaseSnapDistance { get; set; } = 1.0;
        public double SnapDistance { get; set; } = 1.0;
        public double OrthoSnapAngleTolerance { get; set; } = 2.0; //degrees
        public double OrthoSnapAngleInterval { get; set; } = 15.0; //degrees
    }
}
