﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadPanel.PropertyPanel.PropertyGroups;
using CadPanel.Extensions;
using static System.Math;
using CadPanel.PropertyPanel;

namespace CadPanel.UserControls
{
    public partial class CollapsiblePanel : Panel
    {
        private Button _expandContractButton = null;
        private Size _buttonSize = new Size(15, 15);
        private Point _buttonLocation = new Point(5, 5);
        private Point _currentInnerControlBasePoint = new Point(5, 30);
        private int _collapsedHeight = 25;
        private int _expandedHeight = 30;
        private float _labelFontSize = 8;
        private float _headerLabelFontSize = 9;
        private int _gapBetweenInnerControls = 5;
        private int _gapAfterLastControl = 30;
        private int _gapAfterHeaderLabel = 10;

        private Font _headerLabelFont;
        private Font _labelFont;

        public bool Expanded { get; set; } = false;
        public bool ShowPanel { get; set; } = true;

        protected bool _contentEnabled = true;
        public bool ContentEnabled
        {
            get
            {
                return _contentEnabled;
            }
            set
            {
                _contentEnabled = value;

                if (_contentEnabled == true)
                {
                    EnableChildControls();
                }
                else
                {
                    DisableChildControls();
                }
            }
        }

        private string _header = string.Empty;
        public string Header
        {
            get
            {
                return _header;
            }
            set
            {
                _header = value;

                CreateHeaderText();
            }
        }
        

        public List<PropertyGroup> AllProperties { get; set; }

        public CollapsiblePanel()
        {
            InitializeComponent();

            _labelFont = new Font("Arial", _labelFontSize, FontStyle.Regular);

            _headerLabelFont = new Font("Arial", _headerLabelFontSize, FontStyle.Bold);

            this.MinimumSize = new Size(20, 20);
            
            this.BorderStyle = BorderStyle.FixedSingle;

            CreateExpandContractButton();

            _currentInnerControlBasePoint = new Point(_buttonLocation.X, _buttonLocation.Y + _expandContractButton.Height + _gapAfterHeaderLabel);

            _collapsedHeight = _buttonLocation.Y + _expandContractButton.Height + _buttonLocation.Y;

            CreateHeaderText();

            CreateAllProperties();

        }

        private void CollapsiblePanel_Load(object sender, EventArgs e)
        {
            ResizePanelHeight();
        }

        private void CreateExpandContractButton()
        {
            _expandContractButton = new Button();
            _expandContractButton.Size = _buttonSize;
            _expandContractButton.Text = "+";
            _expandContractButton.Font = new Font("Arial", 10, FontStyle.Regular);
            _expandContractButton.Location = _buttonLocation;
            _expandContractButton.BackColor = Color.LightGray;
            _expandContractButton.Click += new EventHandler(ExpandContractButton_Click);
            _expandContractButton.Padding = new Padding(0, 0, 0, 0);
            _expandContractButton.TextAlign = ContentAlignment.MiddleCenter;
            _expandContractButton.FlatStyle = FlatStyle.System;
            this.Controls.Add(_expandContractButton);
        }

        private void CreateHeaderText()
        {
            string headerText = Header;

            if (this.ContainsControl("label_Header", out Control matchingControl) == true)
            {
                this.Controls.Remove(matchingControl);
            }

            if (headerText != null && headerText != string.Empty)
            {
                Label headerLabel = new Label() { Text = headerText, UseCompatibleTextRendering = true };

                headerLabel.Font = _headerLabelFont;

                headerLabel.Width = headerLabel.TextWidth();

                headerLabel.Height = headerLabel.TextHeight();

                int headerVerticalLocation = (int)Ceiling(Math.Max(_buttonLocation.Y, _buttonLocation.Y + 0.5 * _expandContractButton.Height - 0.5 * headerLabel.TextHeight()));

                headerLabel.Location = new Point(_buttonLocation.X + _expandContractButton.Width + 5, headerVerticalLocation);

                headerLabel.Name = "label_Header";

                this.Controls.Add(headerLabel);

                
            }
        }

        private void ExpandContractButton_Click(object sender, EventArgs e)
        {
            if (Expanded == false)
            {
                Expanded = true;

                _expandContractButton.Text = "-";

                ResizePanelHeight();
            }
            else
            {
                Expanded = false;

                _expandContractButton.Text = "+";

                ResizePanelHeight();
            }
        }

        private void CreateAllProperties()
        {
            if (AllProperties != null)
            {
                for (int i = 0; i < AllProperties.Count; i++)
                {
                    int newY = AllProperties[i].AddToCollapsiblePanel(this, _currentInnerControlBasePoint, _labelFont);

                    _currentInnerControlBasePoint = new Point(_currentInnerControlBasePoint.X, newY + _gapBetweenInnerControls);

                    _expandedHeight = _currentInnerControlBasePoint.Y + _gapBetweenInnerControls;

                    if (i == AllProperties.Count - 1)
                    {
                        _expandedHeight = _currentInnerControlBasePoint.Y + _gapAfterLastControl;
                    }
                }

                ResizePanelHeight();
            }          
        }

        private void ResizePanelHeight()
        {
            if (Expanded == true)
            {
                this.Size = new Size(this.Width, _expandedHeight);

                this.VerticalScroll.Maximum = 0;
                this.AutoScroll = false;
                this.HorizontalScroll.Visible = false;
                this.AutoScroll = true;
            }
            else
            {
                this.Size = new Size(this.Width, _collapsedHeight);
                this.AutoScroll = false;
            }
        }

        #region Public Methods
        public void AddProperty(PropertyGroup prop)
        {
            if (prop != null)
            {
                if (prop.Name != null && prop.Name != string.Empty)
                {
                    bool unique = true;

                    if (AllProperties != null)
                    {
                        for (int i = 0; i < AllProperties.Count; i++)
                        {
                            if (string.Compare(AllProperties[i].Name, prop.Name, ignoreCase: true) == 0)
                            {
                                unique = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        AllProperties = new List<PropertyGroup>();
                    }
                    
                    if (unique == true)
                    {
                        AllProperties.Add(prop);

                        int newY = prop.AddToCollapsiblePanel(this, _currentInnerControlBasePoint, _labelFont);

                        _currentInnerControlBasePoint = new Point(_currentInnerControlBasePoint.X, newY + _gapBetweenInnerControls);

                        _expandedHeight = _currentInnerControlBasePoint.Y + _gapAfterLastControl;

                        ResizePanelHeight();
                    }
                }
            }
        }


        #endregion

        #region Protected Methods
        protected void EnableChildControls()
        {
            foreach (Control c in this.GetAllChildControls())
            {
                c.Enabled = true;
            }
        }

        protected void DisableChildControls()
        {
            foreach (Control c in this.GetAllChildControls())
            {
                c.Enabled = false;
            }
        }
        #endregion
    }
}

