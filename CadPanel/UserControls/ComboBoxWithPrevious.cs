﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace CadPanel.UserControls
{
    //https://stackoverflow.com/questions/425241/is-there-a-beforeupdate-for-a-c-sharp-combobox-on-a-winform/425323#425323
    public partial class ComboBoxWithPrevious : ComboBox
    {
        public event CancelEventHandler BeforeUpdate;

        /// <summary>
        /// Previously selected index
        /// </summary>
        public int PreviousIndex
        {
            get
            {
                return mPrevIndex;
            }
        }

        public ComboBoxWithPrevious()
        {
            this.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private bool mBusy;
        private int mPrevIndex = -1;

        protected virtual void OnBeforeUpdate(CancelEventArgs cea)
        {
            if (BeforeUpdate != null) BeforeUpdate(this, cea);
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (mBusy) return;
            mBusy = true;
            try
            {
                CancelEventArgs cea = new CancelEventArgs();
                OnBeforeUpdate(cea);
                if (cea.Cancel)
                {
                    // Restore previous index
                    this.SelectedIndex = mPrevIndex;
                    return;
                }
                mPrevIndex = this.SelectedIndex;
                base.OnSelectedIndexChanged(e);
            }
            finally
            {
                mBusy = false;
            }
        }
    }
}
