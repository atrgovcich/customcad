﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

using CadPanel.OpenGL;
using CadPanel.CustomEventArgs;
using CadPanel.DrawingElements;
using CadPanel.Common;

using static CadPanel.Common.CommonMethods;

using Utilities.Geometry;

namespace CadPanel
{
    public partial class CadPanelControl
    {
        public bool UseOpenGL
        {
            get
            {
                return _renderWithOpenGL;
            }
            set
            {
                if (_renderWithOpenGL != value)
                {
                    if (value == true)
                    {
                        _useGDI = false;
                    }

                    //only do a set if the value is different
                    _renderWithOpenGL = value;
                    ChangeRenderingModes();
                }
            }
        }

        #region OpenTK
        private OpenGLControl _openGLcontrol;
        private Point _draftingWindowTopLeft = new Point(10, 8);
        private bool _renderWithOpenGL = false;
        private int _vboPointer = 0;
        private int _cboPointer = 0;
        private int _vaoPointer = 0;
        #endregion

        #region OpenGL
        private void InitializeOpenGL()
        {
            Point location = new Point();
            location.X = _topLeft.X;

            if (_showPanelToolStrip == true)
            {
                location.Y = _panelToolStrip.Location.Y + _panelToolStrip.Height;
            }
            else
            {
                location.Y = _topLeft.Y;
            }

            _openGLcontrol = new OpenGLControl(this);
            _openGLcontrol.Location = location;
            _openGLcontrol.BackColor = Color.White;
            _openGLcontrol.KeysPressed += new KeysPressedEventHandler(DraftingPictureBoxHasFocus_KeyPress);
            _openGLcontrol.KeysUp += new KeysPressedEventHandler(DraftingPictureBoxHasFocus_KeyUp);
            _openGLcontrol.MouseMove += new MouseEventHandler(DraftingPictureBox_MouseMove);
            _openGLcontrol.MouseDown += new MouseEventHandler(DraftingPictureBox_MouseDown);
            _openGLcontrol.MouseWheel += new MouseEventHandler(DraftingPictureBox_MouseWheel);
            _openGLcontrol.MouseUp += new MouseEventHandler(DraftingPictureBox_MouseUp);
            _openGLcontrol.MouseEnter += new EventHandler(DraftingPictureBox_MouseEnter);
            _openGLcontrol.MouseLeave += new EventHandler(DraftingPictureBox_MouseLeave);
            _openGLcontrol.Paint += new PaintEventHandler(openGLcontrol_Paint);
            _openGLcontrol.Resize += new EventHandler(openGLcontrol_Resize);

            _openGLcontrol.MakeCurrent();
            _vboPointer = GL.GenBuffer();
            _cboPointer = GL.GenBuffer();
            _vaoPointer = GL.GenVertexArray();
        }
        #endregion

        #region Paint
        private void openGLcontrol_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            OpenGLControl glControl = ((OpenGLControl)sender);

            glControl.MakeCurrent();

            RenderWithOpenGL();

            glControl.SwapBuffers();
        }

        private void openGLcontrol_Resize(object sender, EventArgs e)
        {
            OpenGLControl glControl = ((OpenGLControl)sender);

            glControl.Invalidate();
        }
        #endregion

        #region IO
        
        #endregion

        #region Converters
        private Keys ConvertOpenTKKeyToWindowsKeys(OpenTK.Input.Key k)
        {
            Keys e = Keys.A;

            switch (k)
            {
                case OpenTK.Input.Key.A:
                    e = Keys.A;
                    break;
                case OpenTK.Input.Key.AltLeft:
                case OpenTK.Input.Key.AltRight:
                    e = Keys.Alt;
                    break;
                case OpenTK.Input.Key.B:
                    e = Keys.B;
                    break;
                case OpenTK.Input.Key.Back:
                    e = Keys.Back;
                    break;
                case OpenTK.Input.Key.BackSlash:
                    e = Keys.OemBackslash;
                    break;
                case OpenTK.Input.Key.BracketLeft:
                    e = Keys.OemOpenBrackets;
                    break;
                case OpenTK.Input.Key.BracketRight:
                    e = Keys.OemCloseBrackets;
                    break;
                case OpenTK.Input.Key.C:
                    e = Keys.C;
                    break;
                case OpenTK.Input.Key.CapsLock:
                    e = Keys.CapsLock;
                    break;
                case OpenTK.Input.Key.Comma:
                    e = Keys.Oemcomma;
                    break;
                case OpenTK.Input.Key.ControlLeft:
                    e = Keys.LControlKey;
                    break;
                case OpenTK.Input.Key.ControlRight:
                    e = Keys.RControlKey;
                    break;
                case OpenTK.Input.Key.D:
                    e = Keys.D;
                    break;
                case OpenTK.Input.Key.Delete:
                    e = Keys.Delete;
                    break;
                case OpenTK.Input.Key.Down:
                    e = Keys.Down;
                    break;
                case OpenTK.Input.Key.E:
                    e = Keys.E;
                    break;
                case OpenTK.Input.Key.End:
                    e = Keys.End;
                    break;
                case OpenTK.Input.Key.Enter:
                    e = Keys.Enter;
                    break;
                case OpenTK.Input.Key.Escape:
                    e = Keys.Escape;
                    break;
                case OpenTK.Input.Key.F:
                    e = Keys.F;
                    break;
                case OpenTK.Input.Key.F1:
                    e = Keys.F1;
                    break;
                case OpenTK.Input.Key.F10:
                    e = Keys.F10;
                    break;
                case OpenTK.Input.Key.F11:
                    e = Keys.F11;
                    break;
                case OpenTK.Input.Key.F12:
                    e = Keys.F12;
                    break;
                case OpenTK.Input.Key.F13:
                    e = Keys.F13;
                    break;
                case OpenTK.Input.Key.F14:
                    e = Keys.F14;
                    break;
                case OpenTK.Input.Key.F15:
                    e = Keys.F15;
                    break;
                case OpenTK.Input.Key.F16:
                    e = Keys.F16;
                    break;
                case OpenTK.Input.Key.F17:
                    e = Keys.F17;
                    break;
                case OpenTK.Input.Key.F18:
                    e = Keys.F18;
                    break;
                case OpenTK.Input.Key.F19:
                    e = Keys.F19;
                    break;
                case OpenTK.Input.Key.F2:
                    e = Keys.F2;
                    break;
                case OpenTK.Input.Key.F20:
                    e = Keys.F20;
                    break;
                case OpenTK.Input.Key.F21:
                    e = Keys.F21;
                    break;
                case OpenTK.Input.Key.F22:
                    e = Keys.F22;
                    break;
                case OpenTK.Input.Key.F23:
                    e = Keys.F23;
                    break;
                case OpenTK.Input.Key.F24:
                    e = Keys.F24;
                    break;
                case OpenTK.Input.Key.F3:
                    e = Keys.F3;
                    break;
                case OpenTK.Input.Key.F4:
                    e = Keys.F4;
                    break;
                case OpenTK.Input.Key.F5:
                    e = Keys.F5;
                    break;
                case OpenTK.Input.Key.F6:
                    e = Keys.F6;
                    break;
                case OpenTK.Input.Key.F7:
                    e = Keys.F7;
                    break;
                case OpenTK.Input.Key.F8:
                    e = Keys.F8;
                    break;
                case OpenTK.Input.Key.F9:
                    e = Keys.F9;
                    break;
                case OpenTK.Input.Key.G:
                    e = Keys.G;
                    break;
                case OpenTK.Input.Key.H:
                    e = Keys.H;
                    break;
                case OpenTK.Input.Key.Home:
                    e = Keys.Home;
                    break;
                case OpenTK.Input.Key.I:
                    e = Keys.I;
                    break;
                case OpenTK.Input.Key.Insert:
                    e = Keys.Insert;
                    break;
                case OpenTK.Input.Key.J:
                    e = Keys.J;
                    break;
                case OpenTK.Input.Key.K:
                    e = Keys.K;
                    break;
                case OpenTK.Input.Key.Keypad0:
                    e = Keys.NumPad0;
                    break;
                case OpenTK.Input.Key.Keypad1:
                    e = Keys.NumPad1;
                    break;
                case OpenTK.Input.Key.Keypad2:
                    e = Keys.NumPad2;
                    break;
                case OpenTK.Input.Key.Keypad3:
                    e = Keys.NumPad3;
                    break;
                case OpenTK.Input.Key.Keypad4:
                    e = Keys.NumPad4;
                    break;
                case OpenTK.Input.Key.Keypad5:
                    e = Keys.NumPad5;
                    break;
                case OpenTK.Input.Key.Keypad6:
                    e = Keys.NumPad6;
                    break;
                case OpenTK.Input.Key.Keypad7:
                    e = Keys.NumPad7;
                    break;
                case OpenTK.Input.Key.Keypad8:
                    e = Keys.NumPad8;
                    break;
                case OpenTK.Input.Key.Keypad9:
                    e = Keys.NumPad9;
                    break;
                case OpenTK.Input.Key.KeypadAdd:
                    e = Keys.Add;
                    break;
                case OpenTK.Input.Key.KeypadDecimal:
                    e = Keys.Decimal;
                    break;
                case OpenTK.Input.Key.KeypadDivide:
                    e = Keys.Divide;
                    break;
                case OpenTK.Input.Key.KeypadEnter:
                    e = Keys.Enter;
                    break;
                case OpenTK.Input.Key.KeypadMinus:
                    e = Keys.Subtract;
                    break;
                case OpenTK.Input.Key.KeypadMultiply:
                    e = Keys.Multiply;
                    break;
                case OpenTK.Input.Key.L:
                    e = Keys.L;
                    break;
                case OpenTK.Input.Key.Left:
                    e = Keys.Left;
                    break;
                case OpenTK.Input.Key.LShift:
                    e = Keys.LShiftKey;
                    break;
                case OpenTK.Input.Key.M:
                    e = Keys.M;
                    break;
                case OpenTK.Input.Key.Minus:
                    e = Keys.OemMinus;
                    break;
                case OpenTK.Input.Key.N:
                    e = Keys.N;
                    break;
                case OpenTK.Input.Key.Number0:
                    e = Keys.D0;
                    break;
                case OpenTK.Input.Key.Number1:
                    e = Keys.D1;
                    break;
                case OpenTK.Input.Key.Number2:
                    e = Keys.D2;
                    break;
                case OpenTK.Input.Key.Number3:
                    e = Keys.D3;
                    break;
                case OpenTK.Input.Key.Number4:
                    e = Keys.D4;
                    break;
                case OpenTK.Input.Key.Number5:
                    e = Keys.D5;
                    break;
                case OpenTK.Input.Key.Number6:
                    e = Keys.D6;
                    break;
                case OpenTK.Input.Key.Number7:
                    e = Keys.D7;
                    break;
                case OpenTK.Input.Key.Number8:
                    e = Keys.D8;
                    break;
                case OpenTK.Input.Key.Number9:
                    e = Keys.D9;
                    break;
                case OpenTK.Input.Key.NumLock:
                    e = Keys.NumLock;
                    break;
                case OpenTK.Input.Key.O:
                    e = Keys.O;
                    break;
                case OpenTK.Input.Key.P:
                    e = Keys.P;
                    break;
                case OpenTK.Input.Key.PageDown:
                    e = Keys.PageDown;
                    break;
                case OpenTK.Input.Key.PageUp:
                    e = Keys.PageUp;
                    break;
                case OpenTK.Input.Key.Pause:
                    e = Keys.Pause;
                    break;
                case OpenTK.Input.Key.Period:
                    e = Keys.OemPeriod;
                    break;
                case OpenTK.Input.Key.Plus:
                    e = Keys.Oemplus;
                    break;
                case OpenTK.Input.Key.PrintScreen:
                    e = Keys.PrintScreen;
                    break;
                case OpenTK.Input.Key.Q:
                    e = Keys.Q;
                    break;
                case OpenTK.Input.Key.Quote:
                    e = Keys.OemQuotes;
                    break;
                case OpenTK.Input.Key.R:
                    e = Keys.R;
                    break;
                case OpenTK.Input.Key.Right:
                    e = Keys.Right;
                    break;
                case OpenTK.Input.Key.RShift:
                    e = Keys.RShiftKey;
                    break;
                case OpenTK.Input.Key.S:
                    e = Keys.S;
                    break;
                case OpenTK.Input.Key.ScrollLock:
                    e = Keys.Scroll;
                    break;
                case OpenTK.Input.Key.Semicolon:
                    e = Keys.OemSemicolon;
                    break;
                case OpenTK.Input.Key.Slash:
                    break;
                case OpenTK.Input.Key.Space:
                    e = Keys.Space;
                    break;
                case OpenTK.Input.Key.T:
                    e = Keys.T;
                    break;
                case OpenTK.Input.Key.Tab:
                    e = Keys.Tab;
                    break;
                case OpenTK.Input.Key.Tilde:
                    e = Keys.Oemtilde;
                    break;
                case OpenTK.Input.Key.U:
                    e = Keys.U;
                    break;
                case OpenTK.Input.Key.Up:
                    e = Keys.Up;
                    break;
                case OpenTK.Input.Key.V:
                    e = Keys.V;
                    break;
                case OpenTK.Input.Key.W:
                    e = Keys.W;
                    break;
                case OpenTK.Input.Key.X:
                    e = Keys.X;
                    break;
                case OpenTK.Input.Key.Y:
                    e = Keys.Y;
                    break;
                case OpenTK.Input.Key.Z:
                    e = Keys.Z;
                    break;
            }

            return e;
        }
        #endregion

        private List<List<Vector2>> vboList = new List<List<Vector2>>();

        private List<List<Vector4>> cboList = new List<List<Vector4>>();

        private List<Vector2> vertexBuffer = new List<Vector2>();

        private List<Vector4> colorBuffer = new List<Vector4>();

        private class OpenGLLayerVertices
        {
            public List<List<Vector2>> Vertices { get; set; }
            public int ListSize { get; set; }
        }

        private class OpenGLAttribute
        {
            public int StartIndex { get; set; }
            public int EndIndex { get; set; }
            public Layer Layer { get; set; }
            public  PrimitiveType PrimitiveType { get; set; } = PrimitiveType.Lines;
        }

        private void RenderWithOpenGL()
        {
            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                t.Start();

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                Point p0 = new Point(0, _draftingControl.Height); // bottom left
                Point p1 = new Point(_draftingControl.Width, 0); // top right

                PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
                PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                GL.ClearColor(Color.White);
                GL.Clear(ClearBufferMask.ColorBufferBit);

                GL.Viewport(0, 0, _openGLcontrol.ClientSize.Width, _openGLcontrol.ClientSize.Height);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadIdentity();
                GL.Ortho(p0d.X, p1d.X, p0d.Y, p1d.Y, -1, 1);

                int totalListSize = 0;

                //for (int i = 0; i < InstanceData.DrawingElementList.Count; i++)
                //{
                //    totalListSize += InstanceData.DrawingElementList[i].OpenGLVertices.Count;
                //}

                //vertexBuffer.Capacity = totalListSize;

                //t.Stop();
                //Debug.WriteLine($"Getting total number of vertices took {t.ElapsedMilliseconds} ms");
                //t.Reset();
                //t.Start();

                List<OpenGLAttribute> attributeList = new List<OpenGLAttribute>(1000);

                int listSize = 0;

                OpenGLAttribute currentAttribute = null;

                IList<IDrawingElement> orderedList;

                if (InstanceData.UseQuadTreeWithOpenGL == true)
                {
                    orderedList = GetElementsInDraftingWindow();
                }
                else
                {
                    orderedList = InstanceData.DrawingElementList;
                }

                if (orderedList != null)
                {
                    int numDrawingElements = orderedList.Count;

                    if (numDrawingElements > 0)
                    {
                        for (int i = 0; i < numDrawingElements; i++)
                        {
                            IDrawingElement elem = orderedList[i];

                            if (elem.Layer.Visible == false)
                            {
                                continue;
                            }

                            if (elem.Selected == false)
                            {
                                List<Vector2> vbo = elem.OpenGLVertices;

                                totalListSize += vbo.Count;

                                vertexBuffer.AddRange(vbo);

                                int vboCount = vbo.Count;

                                if (currentAttribute == null)
                                {
                                    currentAttribute = new OpenGLAttribute();

                                    currentAttribute.StartIndex = listSize;

                                    currentAttribute.Layer = elem.Layer;
                                }
                                else
                                {
                                    if (currentAttribute.Layer.Equals(elem.Layer) == false)
                                    {
                                        currentAttribute.EndIndex = listSize - 1;

                                        attributeList.Add(currentAttribute);

                                        currentAttribute = new OpenGLAttribute();

                                        currentAttribute.Layer = elem.Layer;

                                        currentAttribute.StartIndex = listSize;
                                    }
                                }

                                listSize += vboCount;
                            }
                            else
                            {
                                selectedElements.Add(orderedList[i]);
                            }
                        }

                        if (currentAttribute != null)
                        {
                            currentAttribute.EndIndex = listSize - 1;

                            attributeList.Add(currentAttribute);
                        }
                    }

                    if (selectedElements.Count > 0)
                    {
                        List<Vector2> pList = new List<Vector2>();
                      
                        currentAttribute = new OpenGLAttribute();

                        currentAttribute.StartIndex = listSize;

                        currentAttribute.Layer = new Layer("selected000x001", -1, InstanceData.SelectedObjectLineWidth, InstanceData.SelectedObjectLineColor);

                        bool drawSelectedVertices = false;

                        if (selectedElements.Count == 1)
                        {
                            drawSelectedVertices = true;
                        }
                        else if (selectedElements.Count > 1 && InstanceData.DrawVerticesOfMultipleObjects == true)
                        {
                            drawSelectedVertices = true;
                        }
                        
                        for (int i = 0; i < selectedElements.Count; i++)
                        {
                            IDrawingElement elem = selectedElements[i];

                            List<Vector2> vbo = elem.OpenGLVertices;

                            totalListSize += vbo.Count;

                            vertexBuffer.AddRange(vbo);

                            int vboCount = vbo.Count;

                            listSize += vboCount;

                            if (drawSelectedVertices == true)
                            {
                                pList.AddRange(elem.OpenGLSelectionVertices);
                            }
                        }

                        currentAttribute.EndIndex = listSize - 1;

                        attributeList.Add(currentAttribute);

                        //Selected elmeent vertices
                        if (pList.Count > 0)
                        {
                            currentAttribute = new OpenGLAttribute();

                            currentAttribute.StartIndex = listSize;

                            currentAttribute.Layer = new Layer("selected000x002", -1, InstanceData.SelectedObjectPointWidth, InstanceData.SelectedObjectPointColor);

                            currentAttribute.PrimitiveType = PrimitiveType.Points;

                            totalListSize += pList.Count;

                            vertexBuffer.AddRange(pList);

                            listSize += pList.Count;

                            currentAttribute.EndIndex = listSize - 1;

                            attributeList.Add(currentAttribute);
                        }
                    }

                    //t.Stop();
                    //Debug.WriteLine($"OpenGL vbo getting took {t.ElapsedMilliseconds} ms");
                    //t.Reset();
                    //t.Start();

                    Vector2[] vertices = (Vector2[])typeof(List<Vector2>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(vertexBuffer);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPointer);
                    GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(Vector2.SizeInBytes * totalListSize), vertices, BufferUsageHint.StreamDraw);
                    GL.VertexPointer(2, VertexPointerType.Float, Vector2.SizeInBytes, 0);
                    GL.EnableClientState(ArrayCap.VertexArray);

                    ClearVertexBufferCaller();

                    for (int i = 0; i < attributeList.Count; i++)
                    {
                        OpenGLAttribute a = attributeList[i];

                        GL.Color3(a.Layer.Color);
                        if (a.PrimitiveType.Equals(PrimitiveType.Lines) == true)
                        {
                            GL.LineWidth(a.Layer.Width);
                        }
                        else if (a.PrimitiveType.Equals(PrimitiveType.Points) == true)
                        {
                            GL.PointSize((float)a.Layer.Width);
                        }

                        GL.DrawArrays(a.PrimitiveType, a.StartIndex, a.EndIndex - a.StartIndex + 1);
                    }

                    //t.Stop();
                    //Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");
                    //t.Reset();
                    //t.Start();

                    //var ts = Task.Run(() => vertexBuffer.Clear());
                    //vertexBuffer.Clear();

                    //t.Stop();
                    //Debug.WriteLine($"Clearing vertex buffer took {t.ElapsedMilliseconds} ms");
                }

                t.Stop();
                Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");
            }
        }

        private async void ClearVertexBufferCaller()
        {
            await ClearVertexBuffer();
        }

        private Task ClearVertexBuffer()
        {
            return Task.Run(()=>vertexBuffer.Clear());
        }

        private void RenderSelectionBoxWithOpenGL(PointD[] cornerPoints, SelectionBoxDirection dir)
        {
            RenderWithOpenGL();

            if (dir.Equals(SelectionBoxDirection.Left) == true)
            {
                GL.LineStipple(1, 0xAAAA);

                GL.Enable(EnableCap.LineStipple);
            }

            GL.Color3(Color.Gray);

            GL.LineWidth(1f);

            GL.Begin(PrimitiveType.LineStrip);

            for (int i = 0; i <= cornerPoints.GetUpperBound(0); i++)
            {
                GL.Vertex2((float)cornerPoints[i].X, (float)cornerPoints[i].Y);
            }

            GL.End();

            if (dir.Equals(SelectionBoxDirection.Left) == true)
            {
                GL.Disable(EnableCap.LineStipple);
            }

            _openGLcontrol.SwapBuffers();
        }

        private void RenderWithOpenGL4()
        {
            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                t.Start();

                List<IDrawingElement> elementsInBounds = GetElementsInDraftingWindow();

                //elementsInBounds = elementsInBounds.OrderBy(x => InstanceData.DrawingElementList.GetDrawOrder(x)).ToList();

                t.Stop();
                Debug.WriteLine($"Getting elemetns in window took {t.ElapsedMilliseconds} ms");
                t.Reset();
                t.Start();

                Dictionary<Layer, OpenGLLayerVertices> layerVertices = new Dictionary<Layer, OpenGLLayerVertices>();

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                Point p0 = new Point(0, _draftingControl.Height); // bottom left
                Point p1 = new Point(_draftingControl.Width, 0); // top right

                PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
                PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                GL.Viewport(0, 0, _openGLcontrol.ClientSize.Width, _openGLcontrol.ClientSize.Height);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadIdentity();
                GL.Ortho(p0d.X, p1d.X, p0d.Y, p1d.Y, -1, 1);

                t.Start();

                int totalListSize = 0;

                for (int i = 0; i < elementsInBounds.Count; i++)
                {
                    totalListSize += elementsInBounds[i].OpenGLVertices.Count;
                }

                //vboList.Capacity = InstanceData.DrawingElementList.Count;
                vertexBuffer.Capacity = totalListSize;

                t.Stop();
                Debug.WriteLine($"Getting total number of vertices took {t.ElapsedMilliseconds} ms");
                t.Reset();
                t.Start();

                List<OpenGLAttribute> attributeList = new List<OpenGLAttribute>(1000);

                int listSize = 0;

                OpenGLAttribute currentAttribute = null;

                if (elementsInBounds != null)
                {
                    int numDrawingElements = elementsInBounds.Count;

                    if (numDrawingElements > 0)
                    {
                        for (int i = 0; i < numDrawingElements; i++)
                        {
                            IDrawingElement elem = elementsInBounds[i];

                            if (elem.Selected == false)
                            {
                                List<Vector2> vbo = elem.OpenGLVertices;

                                vertexBuffer.AddRange(vbo);

                                int vboCount = vbo.Count;

                                if (currentAttribute == null)
                                {
                                    currentAttribute = new OpenGLAttribute();

                                    currentAttribute.StartIndex = listSize;

                                    currentAttribute.Layer = elem.Layer;
                                }
                                else
                                {
                                    if (currentAttribute.Layer.Equals(elem.Layer) == false)
                                    {
                                        currentAttribute.EndIndex = listSize - 1;

                                        attributeList.Add(currentAttribute);

                                        currentAttribute = new OpenGLAttribute();

                                        currentAttribute.Layer = elem.Layer;

                                        currentAttribute.StartIndex = listSize;
                                    }
                                }

                                listSize += vboCount;
                            }
                            else
                            {
                                selectedElements.Add(elementsInBounds[i]);
                            }
                        }

                        if (currentAttribute != null)
                        {
                            currentAttribute.EndIndex = listSize - 1;

                            attributeList.Add(currentAttribute);
                        }
                    }

                    if (selectedElements.Count > 0)
                    {
                        currentAttribute = new OpenGLAttribute();

                        currentAttribute.StartIndex = listSize;

                        currentAttribute.Layer = new Layer("selected000x001", -1, InstanceData.SelectedObjectLineWidth, InstanceData.SelectedObjectLineColor);

                        for (int i = 0; i < selectedElements.Count; i++)
                        {
                            IDrawingElement elem = selectedElements[i];

                            List<Vector2> vbo = elem.OpenGLVertices;

                            vertexBuffer.AddRange(vbo);

                            int vboCount = vbo.Count;

                            listSize += vboCount;
                        }

                        currentAttribute.EndIndex = listSize - 1;

                        attributeList.Add(currentAttribute);
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL vbo getting took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    Vector2[] vertices = (Vector2[])typeof(List<Vector2>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(vertexBuffer);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPointer);
                    GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(Vector2.SizeInBytes * totalListSize), vertices, BufferUsageHint.DynamicDraw);
                    GL.VertexPointer(2, VertexPointerType.Float, Vector2.SizeInBytes, 0);
                    GL.EnableClientState(ArrayCap.VertexArray);

                    for (int i = 0; i < attributeList.Count; i++)
                    {
                        OpenGLAttribute a = attributeList[i];

                        GL.Color3(a.Layer.Color);
                        GL.LineWidth(a.Layer.Width);
                        GL.DrawArrays(PrimitiveType.Lines, a.StartIndex, a.EndIndex - a.StartIndex + 1);
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    //var ts = Task.Run(() => vertexBuffer.Clear());
                    vertexBuffer.Clear();

                    t.Stop();
                    Debug.WriteLine($"Clearing vertex buffer took {t.ElapsedMilliseconds} ms");
                }
            }
        }

        private void RenderWithOpenGL3()
        {
            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                Dictionary<Layer, OpenGLLayerVertices> layerVertices = new Dictionary<Layer, OpenGLLayerVertices>();

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                Point p0 = new Point(0, _draftingControl.Height); // bottom left
                Point p1 = new Point(_draftingControl.Width, 0); // top right

                PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
                PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                GL.Viewport(0, 0, _openGLcontrol.ClientSize.Width, _openGLcontrol.ClientSize.Height);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadIdentity();
                GL.Ortho(p0d.X, p1d.X, p0d.Y, p1d.Y, -1, 1);

                t.Start();

                if (InstanceData.DrawingElementList != null)
                {
                    int numDrawingElements = InstanceData.DrawingElementList.Count;

                    if (numDrawingElements > 0)
                    {
                        for (int i = 0; i < numDrawingElements; i++)
                        {
                            IDrawingElement elem = InstanceData.DrawingElementList[i];

                            if (elem.Selected == false)
                            {
                                List<Vector2> vbo = elem.OpenGLVertices;

                                if (layerVertices.TryGetValue(elem.Layer, out OpenGLLayerVertices lVerts) == true)
                                {
                                    lVerts.Vertices.Add(vbo);

                                    lVerts.ListSize += vbo.Count;
                                }
                                else
                                {
                                    OpenGLLayerVertices nvList = new OpenGLLayerVertices();

                                    nvList.Vertices = new List<List<Vector2>>(1000);

                                    nvList.Vertices.Add(vbo);

                                    nvList.ListSize += vbo.Count;

                                    layerVertices.Add(elem.Layer, nvList);
                                }
                            }
                            else
                            {
                                selectedElements.Add(InstanceData.DrawingElementList[i]);
                            }
                        }
                    }

                    //if (selectedElements.Count > 0)
                    //{
                    //    for (int i = 0; i < selectedElements.Count; i++)
                    //    {
                    //        List<Vector2> vbo = selectedElements[i].OpenGLVertices;
                    //        List<Vector4> cbo = selectedElements[i].OpenGLColors;

                    //        vboList.Add(vbo);
                    //        cboList.Add(cbo);

                    //        int vboCount = vbo.Count;

                    //        listSize += vboCount;
                    //    }
                    //}

                    t.Stop();
                    Debug.WriteLine($"OpenGL vbo getting took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    foreach (KeyValuePair<Layer, OpenGLLayerVertices> kvp in layerVertices)
                    {
                        List<List<Vector2>> elemVertexList = kvp.Value.Vertices;

                        int numElements = elemVertexList.Count;

                        int numVertices = kvp.Value.ListSize;

                        List<Vector2> vb = new List<Vector2>(numVertices);

                        if (numElements > 0)
                        {
                            for (int i = 0; i < numElements; i++)
                            {
                                List<Vector2> v = elemVertexList[i];

                                vb.AddRange(v);
                            }

                            Vector2[] vertices = (Vector2[])typeof(List<Vector2>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(vb);

                            GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPointer);
                            GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(Vector2.SizeInBytes * numVertices), vertices, BufferUsageHint.DynamicDraw);
                            GL.VertexPointer(2, VertexPointerType.Float, Vector2.SizeInBytes, 0);
                            GL.EnableClientState(ArrayCap.VertexArray);

                            GL.Color3(kvp.Key.Color);
                            GL.LineWidth(kvp.Key.Width);
                            GL.DrawArrays(PrimitiveType.Lines, 0, numVertices);
                        }
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");
                }
            }
        }

        private void RenderWithOpenGL2()
        {
            int listSize = 0;

            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                Point p0 = new Point(0, _draftingControl.Height); // bottom left
                Point p1 = new Point(_draftingControl.Width, 0); // top right

                PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
                PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                GL.Viewport(0, 0, _openGLcontrol.ClientSize.Width, _openGLcontrol.ClientSize.Height);
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadIdentity();
                GL.Ortho(p0d.X, p1d.X, p0d.Y, p1d.Y, -1, 1);

                t.Start();

                vboList.Capacity = InstanceData.DrawingElementList.Count;
                cboList.Capacity = InstanceData.DrawingElementList.Count;

                t.Stop();
                Debug.WriteLine($"Time to create list of Vector2 = {t.ElapsedMilliseconds} ms");
                t.Reset();
                t.Start();

                if (InstanceData.DrawingElementList != null)
                {
                    int numDrawingElements = InstanceData.DrawingElementList.Count;

                    if (numDrawingElements > 0)
                    {
                        for (int i = 0; i < numDrawingElements; i++)
                        {
                            IDrawingElement elem = InstanceData.DrawingElementList[i];

                            if (elem.Selected == false)
                            {
                                List<Vector2> vbo = elem.OpenGLVertices;

                                vboList.Add(vbo);

                                int vboCount = vbo.Count;

                                listSize += vboCount;
                            }
                            else
                            {
                                selectedElements.Add(InstanceData.DrawingElementList[i]);
                            }
                        }
                    }

                    if (selectedElements.Count > 0)
                    {
                        for (int i = 0; i < selectedElements.Count; i++)
                        {
                            List<Vector2> vbo = selectedElements[i].OpenGLVertices;

                            vboList.Add(vbo);

                            int vboCount = vbo.Count;

                            listSize += vboCount;
                        }
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL vbo getting took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    vertexBuffer.Capacity = listSize;
                    colorBuffer.Capacity = listSize;

                    t.Stop();
                    Debug.WriteLine($"Time to set capacity of vertexBuffer = {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    int vboListCount = vboList.Count;

                    if (vboListCount > 0 && listSize > 0)
                    {
                        for (int i = 0; i < vboListCount; i++)
                        {
                            List<Vector2> v = vboList[i];

                            vertexBuffer.AddRange(v);

                            List<Vector4> c = cboList[i];

                            colorBuffer.AddRange(c);
                        }

                        t.Stop();
                        Debug.WriteLine($"OpenGL pre-processing took {t.ElapsedMilliseconds} ms");
                        t.Reset();
                        t.Start();

                        Vector2[] vertices = (Vector2[])typeof(List<Vector2>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(vertexBuffer);
                        Vector4[] colors = (Vector4[])typeof(List<Vector4>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(colorBuffer);

                        t.Stop();
                        Debug.WriteLine($"Time to get backing array = {t.ElapsedMilliseconds} ms");
                        t.Reset();
                        t.Start();

                        GL.BindBuffer(BufferTarget.ArrayBuffer, _cboPointer);
                        GL.BufferData<Vector4>(BufferTarget.ArrayBuffer, (IntPtr)(Vector4.SizeInBytes * listSize), colors, BufferUsageHint.DynamicDraw);
                        GL.ColorPointer(4, ColorPointerType.Float, Vector4.SizeInBytes, 0);
                        GL.EnableClientState(ArrayCap.ColorArray);

                        GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPointer);
                        GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(Vector2.SizeInBytes * listSize), vertices, BufferUsageHint.DynamicDraw);
                        GL.VertexPointer(2, VertexPointerType.Float, Vector2.SizeInBytes, 0);
                        GL.EnableClientState(ArrayCap.VertexArray);

                        GL.DrawArrays(PrimitiveType.Lines, 0, listSize);
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");

                    t.Reset();
                    t.Start();

                    vboList.Clear();
                    cboList.Clear();

                    t.Stop();
                    Debug.WriteLine($"Clearing vboList took {t.ElapsedMilliseconds} ms");

                    t.Reset();
                    t.Start();

                    vertexBuffer.Clear();
                    colorBuffer.Clear();
                    //var ts = Task.Run(() => vertexBuffer.Clear()); 

                    t.Stop();
                    Debug.WriteLine($"Clearing vertexBuffer took {t.ElapsedMilliseconds} ms");

                }
            }
        }

        private void RenderWithOpenGL1()
        {
            int listSize = 0;

            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                t.Start();

                List<IDrawingElement> elementsInBounds = GetElementsInDraftingWindow().ToList();

                double tt = 2;

                elementsInBounds = elementsInBounds.OrderBy(x => InstanceData.DrawingElementList.GetDrawOrder(x)).ToList();

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                t.Stop();
                Debug.WriteLine($"OpenGL getting elements in bounds took {t.ElapsedMilliseconds} ms");
                t.Reset();
                t.Start();

                if (elementsInBounds != null)
                {
                    List<List<Vector2>> vboList = new List<List<Vector2>>(elementsInBounds.Count + 1);
                    List<List<float>> cboList = new List<List<float>>(elementsInBounds.Count + 1);

                    if (elementsInBounds.Count > 0)
                    {
                        for (int i = 0; i < elementsInBounds.Count; i++)
                        {
                            if (elementsInBounds[i].Selected == false)
                            {
                                //elementsInBounds[i].GetOpenGLBuffers(InstanceData, out List<Vector2> vbo, out List<float> cbo);

                                List<Vector2> vbo = elementsInBounds[i].OpenGLVertices;

                                vboList.Add(vbo);

                                //cboList.Add(cbo);

                                listSize += vbo.Count;
                            }
                            else
                            {
                                selectedElements.Add(elementsInBounds[i]);
                            }
                        }
                    }

                    if (selectedElements.Count > 0)
                    {
                        for (int i = 0; i < selectedElements.Count; i++)
                        {
                            selectedElements[i].GetOpenGLBuffers(InstanceData, out List<Vector2> vbo, out List<float> cbo);

                            vboList.Add(vbo);

                            //cboList.Add(cbo);

                            listSize += vbo.Count;
                        }
                    }

                    Vector2[] vertexBuffer = new Vector2[listSize];
                    //float[] colorBuffer = new float[listSize];

                    int count = 0;

                    t.Stop();
                    Debug.WriteLine($"OpenGL vbo getting took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    for (int i = 0; i < vboList.Count; i++)
                    {
                        for (int j = 0; j < vboList[i].Count; j++)
                        {
                            vertexBuffer[count] = vboList[i][j];
                            //colorBuffer[count] = cboList[i][j];
                            count++;
                        }
                    }

                    t.Stop();
                    Debug.WriteLine($"OpenGL pre-processing took {t.ElapsedMilliseconds} ms");
                    t.Reset();
                    t.Start();

                    Point p0 = new Point(0, _draftingControl.Height); // bottom left
                    Point p1 = new Point(_draftingControl.Width, 0); // top right

                    PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
                    PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                    GL.Viewport(0, 0, _openGLcontrol.ClientSize.Width, _openGLcontrol.ClientSize.Height);
                    GL.MatrixMode(MatrixMode.Projection);
                    //Matrix4 m = Matrix4.CreateOrthographicOffCenter(0, _openGLcontrol.ClientSize.Width, 0, _openGLcontrol.ClientSize.Height, 0, 1);
                    //GL.LoadMatrix(ref m);
                    GL.LoadIdentity();
                    GL.Ortho(p0d.X, p1d.X, p0d.Y, p1d.Y, -1, 1);
                    GL.BufferData<Vector2>(BufferTarget.ArrayBuffer, (IntPtr)(Vector2.SizeInBytes * count), vertexBuffer, BufferUsageHint.DynamicDraw);

                    GL.EnableClientState(ArrayCap.VertexArray);
                    //GL.EnableClientState(ArrayCap.ColorArray);
                    GL.VertexPointer(2, VertexPointerType.Float, Vector2.SizeInBytes, 0);

                    GL.BindBuffer(BufferTarget.ArrayBuffer, _vboPointer);
                    GL.Color3(Color.Blue);
                    GL.DrawArrays(PrimitiveType.Lines, 0, vertexBuffer.Length);
                }

                t.Stop();
                Debug.WriteLine($"OpenGL rendering took {t.ElapsedMilliseconds} ms");
            }
        }
    }
}
