﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPanel.UserControls
{
    public partial class CheckBoxTS : CheckBox
    {
        public CheckBoxTS()
        {
            InitializeComponent();
        }

        protected override void OnClick(EventArgs e)
        {
            if (this.ThreeState == true)
            {
                if (this.CheckState.Equals(CheckState.Checked) == true)
                {
                    this.CheckState = CheckState.Unchecked;
                }
                else if (this.CheckState.Equals(CheckState.Unchecked) == true)
                {
                    this.CheckState = CheckState.Checked;
                }
                else
                {
                    //Indeterminate
                    this.CheckState = CheckState.Unchecked;
                }
            }
            else
            {
                base.OnClick(e);
            }
        }

    }
}
