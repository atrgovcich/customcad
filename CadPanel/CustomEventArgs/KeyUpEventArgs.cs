﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPanel.CustomEventArgs
{
    public class KeyUpEventArgs : EventArgs
    {
        public Keys KeyCode { get; set; }
        public bool Handled { get; set; }
        public bool CapsLock { get; set; }
        public bool Shift { get; set; }
        public bool Control { get; set; }
        public bool Alt { get; set; }

        public KeyUpEventArgs()
        {

        }
        public KeyUpEventArgs(Keys keyCode)
        {
            KeyCode = keyCode;
        }

    }
}
