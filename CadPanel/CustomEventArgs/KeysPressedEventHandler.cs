﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadPanel.CustomEventArgs
{
    public delegate void KeysPressedEventHandler(object sender, KeysPressedEventArgs e);

    public delegate void Direct2DPaintEventHandler(object sender, Direct2DPaintEventArgs e);
}
