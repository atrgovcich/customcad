﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PolyBoolCS;
using CadPanel.DrawingElements;
using CadPanel.Common;
using TriangleNetHelper;
using Utilities.Geometry;
using CadPanel.Extensions;

using Point = System.Drawing.Point;
using PointPB = PolyBoolCS.Point;

namespace CadPanel.PolyBool
{
    public static class PolyBoolHelperMethods
    {
        public static List<PolygonG> ConvertFromPolyBoolPolygon(PointList region)
        {
            List<PointPB> vertices = PolyBoolHelper.ConvertRegionToPointList(region);

            var convertedVertices = ConvertFromPolyBoolPoints(vertices).ToList();

            var convertedBoundaries = new List<PolygonG>();

            List<DrawingNode> nodeList = new List<DrawingNode>(convertedVertices.Count + 1);

            for (int i = 0; i < convertedVertices.Count; i++)
            {
                DrawingNode n = new DrawingNode(convertedVertices[i], NodeType.EndPoint);

                nodeList.Add(n);
            }

            nodeList.Add(nodeList[0]); //Close the polygon

            PolygonG poly = new PolygonG()
            {
                Nodes = nodeList
            };

            convertedBoundaries.Add(poly);

            return convertedBoundaries;
        }

        public static IEnumerable<PointPB> ConvertToPolyBoolPoints(IEnumerable<PointD> points)
        {
            var convertedPoints = new List<PointPB>();

            foreach (var point in points)
            {
                convertedPoints.Add(point.ConvertToPolyBoolPoint());
            }

            return convertedPoints;
        }

        public static IEnumerable<PointD> ConvertFromPolyBoolPoints(IEnumerable<PointPB> points)
        {
            var convertedPoints = new List<PointD>();

            foreach (var point in points)
            {
                convertedPoints.Add(ConvertFromPolyBoolPoint(point));
            }

            return convertedPoints;
        }

        public static PointD ConvertFromPolyBoolPoint(PointPB point)
        {
            return new PointD(point.x, point.y);
        }

        private static List<PolygonG> ConvertPolyBoolRegionsToPolygonG(Polygon poly)
        {
            if (poly == null)
            {
                return new List<PolygonG>();
            }

            List<PolygonG> shapeList = new List<PolygonG>(poly.regions.Count);

            for (int i = 0; i < poly.regions.Count; i++)
            {
                PointList region = poly.regions[i];

                List<PointPB> pointList = PolyBoolHelper.ConvertRegionToPointList(region);

                List<DrawingNode> nodeList = new List<DrawingNode>(pointList.Count + 1);

                for (int j = 0; j < pointList.Count; j++)
                {
                    PointPB p = pointList[j];

                    PointD pD = ConvertFromPolyBoolPoint(p);

                    DrawingNode n = new DrawingNode(pD, NodeType.EndPoint);

                    nodeList.Add(n);
                }

                nodeList.Add(nodeList[0]); //Close the polygon

                PolygonG convertedPoly = new PolygonG()
                {
                    Nodes = nodeList
                };

                shapeList.Add(convertedPoly);
            }

            return shapeList;
        }

        public static List<PolygonG> BooleanUnion(List<PolygonG> polygons, double orthogonalTolerance)
        {
            List<PolyBoolCS.Polygon> polyBoolPolygons = new List<PolyBoolCS.Polygon>();

            for (int i = 0; i < polygons.Count(); i++)
            {
                PolygonG polygon = polygons[i];

                List<PointPB> pointList = new List<PointPB>();

                List<PointD> polygonPoints = polygon.Nodes.Select(x=>x.Point).ToList();
                polygonPoints.RemoveAt(polygonPoints.Count - 1);

                for (int j = 0; j < polygonPoints.Count; j++)
                {
                    PointD point = polygonPoints[j];

                    pointList.Add(point.ConvertToPolyBoolPoint());
                }

                polyBoolPolygons.Add(PolyBoolHelper.ConvertToPolygon(pointList));
            }

            PreProcessor helper = new PreProcessor(orthogonalTolerance);

            Polygon unionedPolygon = helper.GetUnionOfPolygons(polyBoolPolygons);

            List<PolygonG> unionedPolygonG = ConvertPolyBoolRegionsToPolygonG(unionedPolygon);

            return unionedPolygonG;
        }

        public static List<PolygonG> BooleanDifference(PolygonG poly1, PolygonG poly2)
        {
            var convertedBoundary = poly1.ConvertToPolyBoolPolygon();
            var convertedOther = poly2.ConvertToPolyBoolPolygon();

            var polybool = new PolyBoolCS.PolyBool();

            Polygon difference = polybool.difference(convertedBoundary, convertedOther);

            if (difference.regions.Any())
            {
                var differencePolygons = new List<PolygonG>();

                foreach (var region in difference.regions)
                {
                    differencePolygons.AddRange(ConvertFromPolyBoolPolygon(region));
                }

                return differencePolygons;
            }

            return new List<PolygonG>();
        }

        public static List<PolygonG> BooleanIntersection(PolygonG poly1, PolygonG poly2)
        {
            var convertedBoundary = poly1.ConvertToPolyBoolPolygon();
            var convertedOther = poly2.ConvertToPolyBoolPolygon();

            var polybool = new PolyBoolCS.PolyBool();

            Polygon intersection = polybool.intersect(convertedBoundary, convertedOther);

            if (intersection.regions.Any())
            {
                var overlappingPolygons = new List<PolygonG>();

                foreach (var region in intersection.regions)
                {
                    if (region.Count != 0)
                    {
                        overlappingPolygons.AddRange(ConvertFromPolyBoolPolygon(region));
                    }
                }

                return overlappingPolygons;
            }
            else
            {
                if (convertedBoundary.regions.Count == 1)
                {
                    if (convertedOther.regions.Count == 1)
                    {
                        if (AreRegionsExactlyTheSame(convertedBoundary.regions[0], convertedOther.regions[0]) == true)
                        {
                            var overlappingPolygons = new List<PolygonG>();

                            overlappingPolygons.AddRange(ConvertFromPolyBoolPolygon(convertedBoundary.regions[0]));

                            return overlappingPolygons;
                        }
                    }
                }
            }

            return new List<PolygonG>();
        }

        private static bool AreRegionsExactlyTheSame(PointList region1, PointList region2)
        {
            bool same = true;

            if (region1.Count == region2.Count)
            {
                for (int i = 0; i < region1.Count; i++)
                {
                    if (region1[i] != region2[i])
                    {
                        same = false;
                        break;
                    }
                }
            }
            else
            {
                same = false;
            }

            if (same == false)
            {
                PointList region1_R = new PointList(); //resampled
                PointList region2_R = new PointList(); //resampled

                for (int i = 0; i < region1.Count; i++)
                {
                    region1_R.Add(region1[i]);
                }

                for (int i = 0; i < region2.Count; i++)
                {
                    region2_R.Add(region2[i]);
                }

                int count = 0;

                while (count <= region1_R.Count - 3)
                {
                    PointPB p1 = region1_R[count];
                    PointPB p2 = region1_R[count + 1];
                    PointPB p3 = region1_R[count + 2];

                    double m12, m23;

                    if (p1.x == p2.x)
                    {
                        if (p2.x == p3.x)
                        {
                            //slopes are the same, remove point 2
                            region1_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.y - p1.y) / (p2.x - p1.x);
                        if (p2.x != p3.x)
                        {
                            m23 = (p3.y - p2.y) / (p3.x - p2.x);
                            if (m12 == m23)
                            {
                                region1_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                count = 0;

                while (count <= region2_R.Count - 3)
                {
                    PointPB p1 = region2_R[count];
                    PointPB p2 = region2_R[count + 1];
                    PointPB p3 = region2_R[count + 2];

                    double m12, m23;

                    if (p1.x == p2.x)
                    {
                        if (p2.x == p3.x)
                        {
                            //slopes are the same, remove point 2
                            region2_R.RemoveAt(count + 1);
                            count--;
                        }
                    }
                    else
                    {
                        m12 = (p2.y - p1.y) / (p2.x - p1.x);
                        if (p2.x != p3.x)
                        {
                            m23 = (p3.y - p2.y) / (p3.x - p2.x);
                            if (m12 == m23)
                            {
                                region2_R.RemoveAt(count + 1);
                                count--;
                            }
                        }
                    }

                    count++;
                }

                //Now both polygons are resampled

                if (region1_R.Count != region2_R.Count)
                {
                    return false;
                }
                else
                {
                    int numSame = 0;

                    for (int i = 0; i < region1_R.Count; i++)
                    {
                        PointPB p1 = region1_R[i];

                        for (int j = 0; j < region2_R.Count; j++)
                        {
                            PointPB p2 = region2_R[j];

                            if (p1 == p2)
                            {
                                numSame++;
                                break;
                            }
                        }
                    }

                    if (numSame == region1_R.Count)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else
            {
                return true;
            }
        }
    }

    
}
