﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CadPanel.OpenGL
{
    public class OpenGLDrawInfo
    {
        public List<Vector2> Coordinates { get; set; }
        public Color Color { get; set; }
        public int Width { get; set; }
        public PrimitiveType PrimitiveType { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
    }
}
