﻿using System;
using System.Windows.Forms;
using System.Reflection;
using System.Windows.Input;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using CadPanel.CustomEventArgs;

namespace CadPanel.OpenGL
{
    public class OpenGLControl : GLControl
    {
        public event KeysPressedEventHandler KeysPressed;

        public event KeysPressedEventHandler KeysUp;

        private CadPanelControl _host;

        private Array _allPossibleKeys = Enum.GetValues(typeof(System.Windows.Input.Key));

        private bool _capsLock;

        public OpenGLControl(CadPanelControl host) : base(GraphicsMode.Default)
        {
            _host = host;
        }

        public bool PreFilterMessage(ref Message m)
        {
            const int WM_KEYUP = 0x0101;
            const int WM_SYSKEYUP = 0x105;

            if (this.ContainsFocus)
            {
                if (m.Msg == WM_KEYUP || m.Msg == WM_SYSKEYUP)
                {
                    Keys keyData = (Keys)m.WParam;

                    KeysPressedEventArgs e = GetKeysPressedEventArgsFromKeys(keyData);

                    foreach (Delegate d in KeysUp.GetInvocationList())
                    {
                        ParameterInfo[] paramList = d.Method.GetParameters();
                        object[] paramObjects = new object[paramList.Length];

                        bool allParamsFound = true;

                        for (int i = 0; i <= paramList.GetUpperBound(0); i++)
                        {
                            if (paramList[i].ParameterType.Equals(typeof(KeysPressedEventArgs)) == true)
                            {
                                paramObjects[i] = e;

                            }
                            else if (paramList[i].ParameterType.Equals(typeof(object)) == true)
                            {
                                if (paramList[i].Name == "sender")
                                {
                                    paramObjects[i] = this;
                                }
                            }
                            else
                            {
                                allParamsFound = false;
                                break;
                            }

                        }

                        if (allParamsFound == true)
                        {
                            d.Method.Invoke(_host, paramObjects);
                        }
                    }

                    return true;
                }
            }
            return false;
        }

        protected KeysPressedEventArgs GetKeysPressedEventArgsFromKeys(Keys keyData)
        {
            int intKey = (int)keyData;

            KeysPressedEventArgs e = new KeysPressedEventArgs() { KeyCode = Keys.None };

            e.CapsLock = _capsLock;

            if (intKey >= 65 && intKey <= 90)
            {
                e.KeyCode = keyData;
            }
            else if (intKey >= 48 && intKey <= 57)
            {
                e.KeyCode = keyData;
            }
            else if (intKey >= 96 && intKey <= 105)
            {
                //Numpad
                e.KeyCode = keyData;
            }
            else if (intKey == 110)
            {
                //Numpad decimal
                e.KeyCode = keyData;
            }
            else if (intKey == 13)
            {
                //Numpad enter
                e.KeyCode = keyData;
            }
            else if (intKey >= 112 && intKey <= 123)
            {
                //Function keys F1 - F12
                e.KeyCode = keyData;
            }
            else if (intKey >= 186 && intKey <= 192)
            {
                e.KeyCode = keyData;
            }
            else if (intKey >= 219 && intKey <= 223)
            {
                e.KeyCode = keyData;
            }
            else
            {
                switch (keyData)
                {
                    case Keys.CapsLock:
                        _capsLock = !_capsLock;
                        break;
                    case Keys.Back:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Down:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Up:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Tab:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Space:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Escape:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Delete:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Return:
                        e.KeyCode = keyData;
                        break;
                    case Keys.Control | Keys.M:
                        e.KeyCode = Keys.M;
                        e.Control = true;
                        break;
                    case Keys.Alt | Keys.Z:
                        e.KeyCode = Keys.Z;
                        e.Alt = true;
                        break;
                    case Keys.Control | Keys.C:
                        e.KeyCode = Keys.C;
                        e.Control = true;
                        break;
                    case Keys.Control | Keys.Shift | Keys.C:
                        e.KeyCode = Keys.C;
                        e.Control = true;
                        e.Shift = true;
                        break;
                    case Keys.Control | Keys.V:
                        e.KeyCode = Keys.V;
                        e.Control = true;
                        break;
                    case Keys.Control | Keys.Z:
                        e.KeyCode = Keys.Z;
                        e.Control = true;
                        break;
                    case Keys.Shift:
                        e.Shift = true;
                        break;
                    case Keys.ShiftKey:
                        e.Shift = true;
                        break;
                    case Keys.LShiftKey:
                        e.Shift = true;
                        break;
                    case Keys.RShiftKey:
                        e.Shift = true;
                        break;
                    case Keys.ShiftKey | Keys.Shift:
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.A:
                        e.KeyCode = Keys.A;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.B:
                        e.KeyCode = Keys.B;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.C:
                        e.KeyCode = Keys.C;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D:
                        e.KeyCode = Keys.D;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.E:
                        e.KeyCode = Keys.E;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.F:
                        e.KeyCode = Keys.F;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.G:
                        e.KeyCode = Keys.G;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.H:
                        e.KeyCode = Keys.H;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.I:
                        e.KeyCode = Keys.I;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.J:
                        e.KeyCode = Keys.J;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.K:
                        e.KeyCode = Keys.K;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.L:
                        e.KeyCode = Keys.L;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.M:
                        e.KeyCode = Keys.M;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.N:
                        e.KeyCode = Keys.N;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.O:
                        e.KeyCode = Keys.O;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.P:
                        e.KeyCode = Keys.P;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Q:
                        e.KeyCode = Keys.Q;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.R:
                        e.KeyCode = Keys.R;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.S:
                        e.KeyCode = Keys.S;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.T:
                        e.KeyCode = Keys.T;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.U:
                        e.KeyCode = Keys.U;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.V:
                        e.KeyCode = Keys.V;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.W:
                        e.KeyCode = Keys.W;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.X:
                        e.KeyCode = Keys.X;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Y:
                        e.KeyCode = Keys.Y;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Z:
                        e.KeyCode = Keys.Z;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D0:
                        e.KeyCode = Keys.D0;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D1:
                        e.KeyCode = Keys.D1;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D2:
                        e.KeyCode = Keys.D2;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D3:
                        e.KeyCode = Keys.D3;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D4:
                        e.KeyCode = Keys.D4;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D5:
                        e.KeyCode = Keys.D5;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D6:
                        e.KeyCode = Keys.D6;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D7:
                        e.KeyCode = Keys.D7;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D8:
                        e.KeyCode = Keys.D8;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.D9:
                        e.KeyCode = Keys.D9;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oemplus:
                        e.KeyCode = Keys.Oemplus;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.OemMinus:
                        e.KeyCode = Keys.OemMinus;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oemcomma:
                        e.KeyCode = Keys.Oemcomma;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.OemPeriod:
                        e.KeyCode = Keys.OemPeriod;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.OemQuestion:
                        e.KeyCode = Keys.OemQuestion;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oem1:
                        e.KeyCode = Keys.Oem1;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oem4:
                        e.KeyCode = Keys.Oem4;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oem6:
                        e.KeyCode = Keys.Oem6;
                        e.Shift = true;
                        break;
                    case Keys.Shift | Keys.Oem7:
                        e.KeyCode = Keys.Oem7;
                        e.Shift = true;
                        break;
                    default:
                        e.KeyCode = Keys.None;
                        break;
                }
            }

            return e;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            KeysPressedEventArgs e = GetKeysPressedEventArgsFromKeys(keyData);

            foreach (Delegate d in KeysPressed.GetInvocationList())
            {
                ParameterInfo[] paramList = d.Method.GetParameters();
                object[] paramObjects = new object[paramList.Length];

                bool allParamsFound = true;

                for (int i = 0; i <= paramList.GetUpperBound(0); i++)
                {
                    if (paramList[i].ParameterType.Equals(typeof(KeysPressedEventArgs)) == true)
                    {
                        paramObjects[i] = e;

                    }
                    else if (paramList[i].ParameterType.Equals(typeof(object)) == true)
                    {
                        if (paramList[i].Name == "sender")
                        {
                            paramObjects[i] = this;
                        }
                    }
                    else
                    {
                        allParamsFound = false;
                        break;
                    }

                }

                if (allParamsFound == true)
                {
                    d.Method.Invoke(_host, paramObjects);
                }
            }

            //List<Keys> depressedKeys = GetAllKeysPressed();

            //var isCapsLocked = Keyboard.IsKeyToggled(Key.CapsLock);



            return true;
        }

    }
}
