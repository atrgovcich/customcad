﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;


namespace CadPanel.OpenGL
{
    public struct OpenGLVertex
    {
        public Vector2 Coordinate;
        public Vector4 Color;
        public static readonly int SizeInBytes = 24;
    }
}
