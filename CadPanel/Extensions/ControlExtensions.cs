﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace CadPanel.Extensions
{
    public static class ControlExtensions
    {
        public static bool ContainsControl(this Control parent, string controlName, out Control matchingControl)
        {
            List<Control> controlList = GetAllChildControls(parent);

            matchingControl = null;

            for (int i = 0; i < controlList.Count; i++)
            {
                Control c = controlList[i];

                if (string.Compare(c.Name, controlName, ignoreCase: true) == 0)
                {
                    matchingControl = c;

                    return true;
                }
            }

            return false;
        }

        public static List<Control> GetAllChildControls(this Control parent)
        {
            List<Control> controlList = new List<Control>();

            foreach (Control c in parent.Controls)
            {
                controlList.AddRange(GetAllChildControls(c));
                controlList.Add(c);
            }

            return controlList;
        }

        public static Control GetControlByName(this Control parent, string controlName)
        {
            List<Control> controlList = GetAllChildControls(parent);

            for (int i = 0; i < controlList.Count; i++)
            {
                Control c = controlList[i];

                if (string.Compare(c.Name, controlName, ignoreCase: true) == 0)
                {
                    return c;
                }
            }

            return null;
        }
    }
}
