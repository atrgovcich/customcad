﻿using System;
using static System.Math;
using CadPanel.DrawingElements;
using TriangleNetHelper.Geometry;
using Utilities.Geometry;

using Point = System.Drawing.Point;
using PointPB = PolyBoolCS.Point;

namespace CadPanel.Extensions
{
    public static class PointDExtensions
    {
        public static bool LiesOnLine(this PointD p, LineSegment line, double orthogonalTolerance = 0.0001)
        {
            PointD v0 = line.StartNode.Point;
            PointD v1 = line.EndNode.Point;

            double maxX = Max(v0.X, v1.X);
            double minX = Min(v0.X, v1.X);

            double maxY = Max(v0.Y, v1.Y);
            double minY = Min(v0.Y, v1.Y);

            if (p.LiesOnInfiniteLine(line, out _, orthogonalTolerance) == true)
            {
                if (Abs(v0.X - v1.X) <= PointD.COINCIDENCE_TOLERANCE)
                {
                    //vertical line
                    if (p.Y >= minY && p.Y <= maxY)
                    {
                        return true;
                    }
                }
                else
                {
                    if (p.X >= minX && p.X <= maxX)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool LiesOnLine(this PointD p, LineSegment line, out double orthogonalDistance, double orthogonalTolerance = 0.0001)
        {
            PointD v0 = line.StartNode.Point;
            PointD v1 = line.EndNode.Point;

            double maxX = Max(v0.X, v1.X);
            double minX = Min(v0.X, v1.X);

            double maxY = Max(v0.Y, v1.Y);
            double minY = Min(v0.Y, v1.Y);

            if (p.LiesOnInfiniteLine(line, out orthogonalDistance, orthogonalTolerance) == true)
            {
                if (Abs(v0.X - v1.X) <= PointD.COINCIDENCE_TOLERANCE)
                {
                    //vertical line
                    if (p.Y >= minY && p.Y <= maxY)
                    {
                        return true;
                    }
                }
                else
                {
                    if (p.X >= minX && p.X <= maxX)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool LiesOnInfiniteLine(this PointD p, LineSegment line, out double orthogonalDistance, double orthogonalTolerance = 0.0001)
        {
            PointD v0 = line.StartNode.Point;
            PointD v1 = line.EndNode.Point;

            if (Abs(v0.X - v1.X) <= PointD.COINCIDENCE_TOLERANCE)
            {
                if (Abs(p.X - v0.X) <= orthogonalTolerance)
                {
                    orthogonalDistance = Abs(p.X - v0.X);

                    return true;
                }
                else
                {
                    orthogonalDistance = -1;

                    return false;
                }
            }
            else if (Abs(v0.Y - v1.Y) <= PointD.COINCIDENCE_TOLERANCE)
            {
                if (Abs(p.Y - v0.Y) <= orthogonalTolerance)
                {
                    orthogonalDistance = Abs(p.Y - v0.Y);

                    return true;
                }
                else
                {
                    orthogonalDistance = -1;

                    return false;
                }
            }
            else
            {
                double m = (v1.Y - v0.Y) / (v1.X - v0.X);
                double b = v0.Y - m * v0.X;

                double m_inv = -1.0 / m;
                double b_inv = p.Y - m_inv * p.X;

                double xInt = (b_inv - b) / (m - m_inv);
                double yInt = m * xInt + b;

                double d = Sqrt(Pow(xInt - p.X, 2) + Pow(yInt - p.Y, 2));

                if (d <= orthogonalTolerance)
                {
                    orthogonalDistance = d;

                    return true;
                }
                else
                {
                    orthogonalDistance = -1;

                    return false;
                }
            }
        }

        public static Point ConvertToPixelPoint(this PointD p, Point zeroPoint, double drawingScale)
        {
            try
            {
                int px = Convert.ToInt32(zeroPoint.X + p.X * drawingScale);
                int py = Convert.ToInt32(zeroPoint.Y - p.Y * drawingScale);

                Point pixelPoint = new Point(px, py);

                return pixelPoint;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not convert to pixel point");
            }
        }

        //public static PointPB ConvertToPolyBoolPoint(this PointD p)
        //{
        //    //return new Point(point.X, point.Y);
        //    return new PointPB(Math.Round(p.X, PointD.PRECISION), Math.Round(p.Y, PointD.PRECISION));
        //}

        public static PreVertex ConvertToPreVertex(this PointD p)
        {
            return new PreVertex(p.X, p.Y);
        }
    }
}
