﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.Common;
using CadPanel.DrawingElements;
using Utilities.Geometry;
using Utilities.DataStructures.QuadTree;

namespace CadPanel.Extensions
{
    public static class RectExtensions
    {
        public static List<LineSegment> Edges(this Rect rect)
        {
            double x0 = rect.X;
            double y0 = rect.Y;
            DrawingNode n0 = new DrawingNode(new PointD(x0, y0), NodeType.EndPoint);

            double x1 = x0;
            double y1 = y0 + rect.Height;
            DrawingNode n1 = new DrawingNode(new PointD(x1, y1), NodeType.EndPoint);

            double x2 = x0 + rect.Width;
            double y2 = y1;
            DrawingNode n2 = new DrawingNode(new PointD(x2, y2), NodeType.EndPoint);

            double x3 = x2;
            double y3 = y0;
            DrawingNode n3 = new DrawingNode(new PointD(x3, y3), NodeType.EndPoint);

            LineSegment e0 = new LineSegment(n0, n1);
            LineSegment e1 = new LineSegment(n1, n2);
            LineSegment e2 = new LineSegment(n2, n3);
            LineSegment e3 = new LineSegment(n3, n0);

            List<LineSegment> edgeList = new List<LineSegment>(4);
            edgeList.Add(e0);
            edgeList.Add(e1);
            edgeList.Add(e2);
            edgeList.Add(e3);

            return edgeList;
        }
    }
}
