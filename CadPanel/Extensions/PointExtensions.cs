﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.Common;
using System.Drawing;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.Extensions
{
    public static class PointExtensions
    {
        public static PointD ConvertToGlobalCoord(this Point p, Point zeroPoint, double drawingScale)
        {
            double x = (p.X - zeroPoint.X) / drawingScale;
            double y = (zeroPoint.Y - p.Y) / drawingScale;

            return new PointD(x, y);
        }
    }
}
