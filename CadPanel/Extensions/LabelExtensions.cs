﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace CadPanel.Extensions
{
    public static class LabelExtensions
    {
        public static int TextWidth(this Label label)
        {
            int h = 0;

            if (label.Text != null && label.Text != string.Empty)
            {
                using (Graphics g = label.CreateGraphics())
                {
                    SizeF size = g.MeasureString(label.Text, label.Font, 495);
                    h = (int)Math.Ceiling(size.Width);
                }
            }

            return h;
        }

        public static int TextHeight(this Label label)
        {
            int h = 0;

            if (label.Text != null && label.Text != string.Empty)
            {
                using (Graphics g = label.CreateGraphics())
                {
                    SizeF size = g.MeasureString(label.Text, label.Font, 495);
                    h = (int)Math.Ceiling(size.Height);
                }
            }
            
            return h;
        }
    }
}
