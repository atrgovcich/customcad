﻿using CadPanel.CommandList;
using CadPanel.Common;
using ReinforcedConcrete.Reinforcement;
using ReinforcedConcrete.PostTensioning;
using CadPanel.CustomEventArgs;
using CadPanel.DrawingElements;
using CadPanel.Extensions;
using CadPanel.Forms;
using CadPanel.PropertyPanel;
using CadPanel.PropertyPanel.ControlContainers;
using CadPanel.PropertyPanel.PropertyGroups;
using CadPanel.Snapping;
using CadPanel.UndoSupport;
using CadPanel.UndoSupport.Actions;
using CadPanel.UserControls;
using Materials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using TriangleNetHelper.Geometry;
using Utilities.Extensions.DotNetNative;
using Utilities.Geometry;
using FiberSectionAnalysis.General;
using Utilities.DataStructures.QuadTree;
using static CadPanel.Common.CommonMethods;
using static System.Math;
using Utilities.Containers;
using CadPanel.Forms.MaterialForms;
using FiberSectionAnalysis.LimitStates;
using FiberSectionAnalysis.AxialForceMomentAnalysis;
using FiberSectionAnalysis.MomentCurvatureAnalysis;
using System.Drawing.Imaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utilities.Compression;
using System.Resources;
using System.Threading.Tasks;

using Bitmap = System.Drawing.Bitmap;
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;
using Layer = CadPanel.Common.Layer;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;
using CadPanel.UserProperties;

namespace CadPanel
{
    public partial class CadPanelControl : Panel
    {
        #region Public Properties
        public CadPanelInstanceData InstanceData { get; set; } = new CadPanelInstanceData();
        public ObservableList<IDrawingElement> SelectedElements { get; set; } = new ObservableList<IDrawingElement>();

        /// <summary>
        /// The number of pixels to add as a buffer below the drawing region
        /// </summary>
        public int BottomBuffer { get; set; } = 20;

        private Color _propertyPanelColor = Color.LightGray;
        public Color PropertyPanelColor
        {
            get
            {
                return _propertyPanelColor;
            }
            set
            {
                _propertyPanelColor = value;

                if (PropertyPanel != null)
                {
                    PropertyPanel.BackColor = value;

                    //this.Invalidate();
                }
            }
        }

        private Color _panelToolStripColor = Color.LightGray;
        public Color PanelToolStripColor
        {
            get
            {
                return _panelToolStripColor;
            }
            set
            {
                _panelToolStripColor = value;

                if (_panelToolStrip != null)
                {
                    _panelToolStrip.BackColor = value;
                }
            }
        }
       
        private bool _useGDI = true;
        public bool UseGDI
        {
            get
            {
                return _useGDI;
            }
            set
            {
                if (_useGDI != value)
                {
                    if (value == true)
                    {
                        _renderWithOpenGL = false;
                    }

                    //only do a set if the value is different
                    _useGDI = value;

                    ChangeRenderingModes();
                }
            }
        }

        private bool _showPropertyPanel = true;
        public bool ShowPropertyPanel
        {
            get
            {
                return _showPropertyPanel;
            }
            set
            {
                _showPropertyPanel = value;

                if (value == true)
                {
                    PropertyPanel.Show();
                }
                else
                {
                    PropertyPanel.Hide();
                }

                ResizeAll();
                Invalidate();
            }
        }
        public bool AllowDrawing { get; set; } = true;

        private bool _allowKeyboardCommands = true;
        public bool AllowKeyboardCommands
        {
            get
            {
                return _allowKeyboardCommands;
            }
            set
            {
                _allowKeyboardCommands = value;

                if (value == true)
                {
                    InputTextBox.Show();
                }
                else
                {
                    InputTextBox.Hide();
                }

                ResizeAll();
                Invalidate();
            }
        }
        public bool ShowPanelToolStrip
        {
            get
            {
                return _showPanelToolStrip;
            }
            set
            {
                _showPanelToolStrip = value;

                Point location = new Point();
                location.X = _topLeft.X;

                if (value == false)
                {
                    location.Y = _topLeft.Y;
                    _panelToolStrip.Hide();
                }
                else
                {
                    location.Y = _panelToolStrip.Location.Y + _panelToolStrip.Height;
                    _panelToolStrip.Show();
                }

                _openGLcontrol.Location = location;
                DraftingPictureBox.Location = location;

                ResizeAll();
                Invalidate();
            }
        }
        #endregion

        #region Private Fields
        Timer CheckTimer = new Timer() { Interval = Convert.ToInt32(1.0 / 60.0 * 1000.0), Enabled = false };

        private Bitmap DraftingBitmap = null;
        private PictureBoxWithKeyEvents DraftingPictureBox;
        private TextBox InputTextBox;
        private Label CoordLabel;
        private Panel PropertyPanel;
        private VerticalButton ExpandPropertyPanelButton;
        private bool _propertyPanelOpen;
        private int _propertyPanelOpenWidth = 200;
        private int _propertyPanelClosedWidth = 35;
        private Dictionary<ElementPropertyType, CollapsiblePanel> _propertyPanelDictionary = new Dictionary<ElementPropertyType, CollapsiblePanel>();
        private AccordianList _propertyPanelAccordianList;
        private PropertyPanelControlContainer _propertyPanelControlContainer;
        private ContextMenu _cadPanelRightClickContextMenu;
        private Control _draftingControl;

        //private QuadTree<IDrawingElement> InstanceData.DrawingElementQuadTree;
        Cursor _magnifyingCursor = null;

        private List<CadCommand> _commandList;

        #region Private Form Properties
        private int _minPanelWidth = 300;
        private int _minPanelHeight = 300;
        private Point _topLeft = new Point(10, 8);
        #endregion

        #region Private Mouse Event Variables
        private Point PreviousMousePoint = new Point(0, 0);
        private Point MousePoint = new Point(0, 0);
        #endregion

        #region Current Drawing States
        private DrawingType _currentDrawingType = DrawingType.None;
        private bool _drawingInProgress;
        private List<IDrawingElement> _tempDrawingElements = new List<IDrawingElement>();
        private List<PointD> _tempPoints = new List<PointD>();
        private bool _panning;
        private Point _panStartPoint;
        private Point _panZeroPoint;
        private PointD _currentGlobalPoint;
        #endregion

        #region Undo and Redo
        private int _maxActionsToStore = 20;
        LinkedList<ActionOnElements> _previousActions = new LinkedList<ActionOnElements>();
        #endregion

        #region Selection Box
        private PointD _selectionBoxStart = null;
        private PointD _selectionBoxEnd = null;
        private bool _selectionBoxInitiated = false;
        private KeysPressedEventArgs _currentKeysDown = new KeysPressedEventArgs();
        List<IDrawingElement> _singleClickSelectionPossibilities = new List<IDrawingElement>();
        int _singleClickSelectionIndex = 0;
        #endregion

        #region Input Text Box
        private int _readOnlyChars = 0;
        private string _lastCommand = "";
        #endregion

        #region MenuStrip
        private ToolStrip _panelToolStrip;
        private ToolStripComboBox _currentLayerDropdown;
        private bool _showPanelToolStrip = true;
        #endregion

        #region Snapping
        private bool _isSnapped = false;
        private SnapDefinition _snapPoint = null;
        private bool _previouslySnapped = false;
        #endregion

        #region Base Points
        private PointD _moveBasePoint = null;
        private PointD _copyBasePoint = null;
        private PointD _rotateBasePoint = null;
        private PointD _arrayBasePoint = null;
        private PointD _arrayDistBasePoint = null;
        private PointD _perpBasePoint = null;
        private double _linearArrayDistance = 0;
        private bool _arrayDistanceSet = false;
        #endregion

        #region Moving
        List<IDrawingElement> _tempMovedObjects = new List<IDrawingElement>();
        #endregion

        #region Reshaping
        private List<DrawingNode> _tempNodesToReshape = new List<DrawingNode>();
        private List<DrawingNode> _origNodesToReshape = new List<DrawingNode>();
        private List<IDrawingElement> _origElementsToReshape = new List<IDrawingElement>();
        #endregion

        #region Copying
        private List<IDrawingElement> _tempCopiedElements = new List<IDrawingElement>();
        private bool _copyWithBasePoint = false;
        #endregion

        #region Rebar Section Drawing
        RebarDefinition _selectedRebarSize = null;
        #endregion

        #region Post Tensioning Section Drawing
        private PostTensioningDefinition _selectedPostTensioningDefinition = null;
        #endregion

        #region Ortho Snapping
        private PointD _orthoBasePoint;
        #endregion

        #region Offsetting
        private double _offsetDistance = 0;
        private bool _offsetDistanceSet = false;
        #endregion
        #endregion

        #region Constructors
        public CadPanelControl()
        {
            InitializeComponent();

            DraftingBitmap = new Bitmap(InstanceData.CanvasWidth, InstanceData.CanvasHeight, PixelFormat.Format32bppRgb);

            ReadCommandListFile();

            this.BackColor = System.Drawing.Color.Transparent;
            this.Resize += new EventHandler(ResizeAllControls);
            //this.Paint += new PaintEventHandler(CadPanel_Paint);

            CheckTimer.Tick += new EventHandler(CheckTimer_Tick);

            RunInitializationRoutines();

            ExpandPropertyPanelButton.Click += new EventHandler(ExpandPropertyPanelButton_Click);

            this.MinimumSize = new Size(_minPanelWidth, _minPanelHeight);

            this.Size = new Size(900, 600);

            this.BackColor = Color.LightSkyBlue;

            CreateDraftingGraphics();

            ResizeAll();
        }
        #endregion

        #region Reading From Resource Files
        private void ReadCommandListFile()
        {
            _commandList = new List<CadCommand>();

            ResourceManager rm = new ResourceManager("CadPanel.Properties.Resources", Assembly.GetExecutingAssembly());

            string result = rm.GetString("CadCommandList");

            string[] lines = result.Split(new[] { Environment.NewLine }, StringSplitOptions.None);

            for (int i = 1; i <= lines.GetUpperBound(0); i++)
            {
                string line = lines[i];

                if (string.IsNullOrEmpty(line) == true)
                {
                    continue;
                }

                string[] cols = line.Split(',');

                CadCommand com = new CadCommand()
                {
                    Name = cols[0],
                    Description = cols[1]
                };

                if (cols.GetUpperBound(0) >= 2)
                {
                    if (String.IsNullOrEmpty(cols[2]) == false)
                    {
                        string[] shortcuts = cols[2].Split(';');

                        List<string> shortcutList = new List<string>();

                        for (int j = 0; j <= shortcuts.GetUpperBound(0); j++)
                        {
                            shortcutList.Add(shortcuts[j]);
                        }

                        com.Shortcuts = shortcutList;
                    }
                }

                _commandList.Add(com);
            }

            return;
        }
        #endregion

        #region Control Initialization

        private void RunInitializationRoutines()
        {
            InstanceData.InitializeLayers();

            _magnifyingCursor = CreatMagnifyingGlassCursor();
            InitializePanelMenuStrip();
            InitializeQuadTree();
            InitializePropertyPanel();
            InitializeDraftingPictureBox();
            InitializeOpenGL();



            BuildCollapsiblePropertyPanels();
            InitializeInputTextBox();
            InitializeCoordinateLabel();
            InitializeCadPanelContextMenu();

            InputTextBox.BringToFront();
            CoordLabel.BringToFront();
        }

        private void InitializeCadPanelContextMenu()
        {
            EventHandler handler = new EventHandler(contextMenu_ItemClicked);

            _cadPanelRightClickContextMenu = new ContextMenu();

            MenuItem filterSelectionItem = new MenuItem("Filter Selection", handler, Shortcut.None);
            //======
            MenuItem selectInstancesItem = new MenuItem("Select All Instances", handler, Shortcut.None);

            MenuItem selectInstancesInViewItem = new MenuItem("Visible in View", handler, Shortcut.None);

            MenuItem selectInstancesInSheet = new MenuItem("In Entire Drawing", handler, Shortcut.None);

            selectInstancesItem.MenuItems.Add(selectInstancesInViewItem);

            selectInstancesItem.MenuItems.Add(selectInstancesInSheet);
            //======
            MenuItem drawOrderItem = new MenuItem("DrawOrder", handler, Shortcut.None);

            MenuItem bringToFrontItem = new MenuItem("Bring to Front", handler, Shortcut.None);

            MenuItem sendToBackItem = new MenuItem("Send to Back", handler, Shortcut.None);

            drawOrderItem.MenuItems.Add(bringToFrontItem);

            drawOrderItem.MenuItems.Add(sendToBackItem);
            //=====

            _cadPanelRightClickContextMenu.MenuItems.Add(filterSelectionItem);

            _cadPanelRightClickContextMenu.MenuItems.Add(selectInstancesItem);

            _cadPanelRightClickContextMenu.MenuItems.Add(drawOrderItem);

            DraftingPictureBox.ContextMenu = _cadPanelRightClickContextMenu;
        }

        private void contextMenu_ItemClicked(object sender, EventArgs e)
        {
            MenuItem item = sender as MenuItem;

            if (String.Compare(item.Text, "Filter Selection", ignoreCase: true) == 0)
            {
                FilterSelectedObjects();
            }
            else if (String.Compare(item.Text,"Bring to Front", ignoreCase: true) == 0)
            {
                BringSelectedObjectsToFront();
            }
            else if (String.Compare(item.Text, "Send to Back", ignoreCase: true) == 0)
            {
                SendSelectedObjectsToBack();
            }
        }

        private void InitializeGDI()
        {
            InitializeDraftingPictureBox();
            ResizeDraftingControl();
        }

        private void InitializeQuadTree()
        {
            InstanceData.DrawingElementQuadTree = new QuadTree<IDrawingElement>();

            Rect rec = new Rect(InstanceData.MinDraftingBound, InstanceData.MinDraftingBound, Abs(2 * InstanceData.MinDraftingBound), Abs(2 * InstanceData.MinDraftingBound));

            InstanceData.DrawingElementQuadTree.Bounds = rec;

        }

        private void InitializeDraftingPictureBox()
        {
            Point location = new Point();
            location.X = _topLeft.X;

            if (_showPanelToolStrip == true)
            {
                location.Y = _panelToolStrip.Location.Y + _panelToolStrip.Height;
            }
            else
            {
                location.Y = _topLeft.Y;
            }

            DraftingPictureBox = new PictureBoxWithKeyEvents(this)
            {
                BackColor = System.Drawing.Color.Blue,
                Location = location,
                Width = InstanceData.CanvasWidth,
                Height = InstanceData.CanvasHeight,
                Name = "DraftingPictureBox"
            };

            DraftingPictureBox.KeysPressed += new KeysPressedEventHandler(DraftingPictureBoxHasFocus_KeyPress);
            DraftingPictureBox.Paint += new PaintEventHandler(DraftingPictureBox_Paint);
            DraftingPictureBox.MouseMove += new MouseEventHandler(DraftingPictureBox_MouseMove);
            DraftingPictureBox.MouseEnter += new EventHandler(DraftingPictureBox_MouseEnter);
            DraftingPictureBox.MouseLeave += new EventHandler(DraftingPictureBox_MouseLeave);
            DraftingPictureBox.MouseDown += new MouseEventHandler(DraftingPictureBox_MouseDown);
            DraftingPictureBox.MouseUp += new MouseEventHandler(DraftingPictureBox_MouseUp);
            DraftingPictureBox.KeysUp += new KeysPressedEventHandler(DraftingPictureBoxHasFocus_KeyUp);
            DraftingPictureBox.MouseWheel += new MouseEventHandler(DraftingPictureBox_MouseWheel);

            _draftingControl = DraftingPictureBox;

            this.Controls.Add(DraftingPictureBox);
        }

        private void InitializeInputTextBox()
        {
            int boxWidth = DraftingPictureBox.Width - 4;
            int boxHeight = 15;

            InputTextBox = new TextBox()
            {
                Name = "InputTextBox",
                Multiline = true,
                Size = new Size(boxWidth, boxHeight),
                Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - boxHeight - 2),
                BackColor = Color.LightGray,
                BorderStyle = BorderStyle.None,
                Text = "",
                Enabled = true,
                ReadOnly = false
            };

            this.Controls.Add(InputTextBox);
            InputTextBox.BringToFront();
        }

        private void InitializeCoordinateLabel()
        {
            CoordLabel = new Label()
            {
                Name = "CoordinateLabel",
                Location = new Point(DraftingPictureBox.Location.X + 2, DraftingPictureBox.Location.Y + DraftingPictureBox.Height - 50),
                Text = "test",
                BackColor = Color.LightGray,
                Height = 27
            };

            this.Controls.Add(CoordLabel);
            CoordLabel.BringToFront();
        }

        private void InitializePropertyPanel()
        {
            PropertyPanel = new Panel();
            PropertyPanel.Name = "PropertyPanel";
            PropertyPanel.Width = 200;
            PropertyPanel.BorderStyle = BorderStyle.Fixed3D;
            PropertyPanel.Height = this.Height - 60;
            PropertyPanel.Location = new Point(this.Width - PropertyPanel.Width - 20, _topLeft.Y);
            PropertyPanel.BackColor = _propertyPanelColor;
            _propertyPanelOpen = true;

            ExpandPropertyPanelButton = new VerticalButton();
            ExpandPropertyPanelButton.Name = "ExpandPropertyPanelButton";
            ExpandPropertyPanelButton.VerticalText = "Properties";
            ExpandPropertyPanelButton.Width = 25;
            ExpandPropertyPanelButton.Height = 100;
            ExpandPropertyPanelButton.Location = new Point(6, PropertyPanel.Height / 2 - ExpandPropertyPanelButton.Height / 2);
            ExpandPropertyPanelButton.FlatStyle = FlatStyle.Flat;

            PropertyPanel.Controls.Add(ExpandPropertyPanelButton);

            this.Controls.Add(PropertyPanel);

        }

        private void InitializePanelMenuStrip()
        {
            _panelToolStrip = new ToolStrip();
            _panelToolStrip.Name = "PanelMenuStrip";
            _panelToolStrip.AutoSize = false;
            _panelToolStrip.Width = 400;
            _panelToolStrip.Height = 30;
            _panelToolStrip.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            _panelToolStrip.Dock = DockStyle.None;
            _panelToolStrip.Stretch = false;
            _panelToolStrip.BackColor = PanelToolStripColor;
            _panelToolStrip.GripStyle = ToolStripGripStyle.Hidden;

            ToolStripLabel tsLabel = new ToolStripLabel("Current Layer");
            _panelToolStrip.Items.Add(tsLabel);

            _currentLayerDropdown = new ToolStripComboBox();
            _currentLayerDropdown.Name = "CurrentLayerDropdown";
            _currentLayerDropdown.AutoSize = false;
            _currentLayerDropdown.Width = 100;
            _currentLayerDropdown.DropDownStyle = ComboBoxStyle.DropDownList;
            _currentLayerDropdown.SelectedIndexChanged += new EventHandler(CurrentLayerDropdown_SelectedIndexChanged);
            PopulatePanelToolStripLayersDropdown();
            _currentLayerDropdown.SelectedIndex = 0;
            _panelToolStrip.Items.Add(_currentLayerDropdown);

            ToolStripButton tsHelpButton = new ToolStripButton();
            tsHelpButton.Name = "tsHelpButton";
            tsHelpButton.BackgroundImage = Properties.Resources.help_icon;
            tsHelpButton.BackgroundImageLayout = ImageLayout.Stretch;
            tsHelpButton.Click += new EventHandler(ToolStripHelpButton_Click);
            tsHelpButton.Margin = new Padding(10, 0, 10, 0);
            tsHelpButton.AutoSize = false;
            tsHelpButton.Size = new Size(25, 25);
            tsHelpButton.ToolTipText = "Command List";

            _panelToolStrip.Items.Add(tsHelpButton);
            _panelToolStrip.Renderer = new CustomToolStripRenderer();

            this.Controls.Add(_panelToolStrip);

            _panelToolStrip.Location = _topLeft;
            _panelToolStrip.Left = _topLeft.X;
        }

        private void BuildCollapsiblePropertyPanels()
        {
            _propertyPanelControlContainer = new PropertyPanelControlContainer();

            _propertyPanelDictionary.Add(ElementPropertyType.General, BuildCollapsiblePanel_General());
            _propertyPanelDictionary.Add(ElementPropertyType.ClosedGeometry, BuildCollapsiblePanel_ClosedGeometry());
            _propertyPanelDictionary.Add(ElementPropertyType.OpenGeometry, BuildCollapsiblePanel_OpenGeometry());
            _propertyPanelDictionary.Add(ElementPropertyType.Nodes, BuildCollapsiblePanel_Nodes());
            _propertyPanelDictionary.Add(ElementPropertyType.Material, BuildCollapsiblePanel_Material());
            _propertyPanelDictionary.Add(ElementPropertyType.Rebar, BuildCollapsiblePanel_Rebar());
            _propertyPanelDictionary.Add(ElementPropertyType.PostTensioning, BuildCollapsiblePanel_PostTensioning());
            _propertyPanelDictionary.Add(ElementPropertyType.UserProperties, BuildCollapsiblePanel_UserProperty());

            _propertyPanelAccordianList = new AccordianList();

            foreach (KeyValuePair<ElementPropertyType, CollapsiblePanel> kvp in _propertyPanelDictionary)
            {
                _propertyPanelAccordianList.AddCollapsiblePanel(kvp.Value);
            }

            _propertyPanelAccordianList.Location = new Point(_propertyPanelClosedWidth, 5);
            _propertyPanelAccordianList.AddAccordianListToControl(PropertyPanel);

            SetPropertyPanelAccordianVisibility();
        }

        private CollapsiblePanel BuildCollapsiblePanel_General()
        {
            TextBoxWithLabel elementTypeTextBox = new TextBoxWithLabel()
            {
                Name = "Element Type",
                LabelText = "Element Type",
                TextBoxWidth = 100,
                ReadOnly = true,
            };

            ComboBoxWithLabel layerComboBox = new ComboBoxWithLabel()
            {
                Name = "Layer",
                LabelText = "Layer",
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "General",
                Header = "General",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(elementTypeTextBox);
            panel.AddProperty(layerComboBox);

            layerComboBox.ComboBox.SelectedIndexChanged += new EventHandler(PropertyPanelLayersComboBox_SelectedIndexChanged);

            for (int i = 0; i < InstanceData.Layers.Count; i++)
            {
                layerComboBox.AddItemToComboBox(InstanceData.Layers[i].Name);
            }

            _propertyPanelControlContainer.GeneralControls = new GeneralPanelControls();

            _propertyPanelControlContainer.GeneralControls.ElementNameTextBox = elementTypeTextBox.TextBox;
            _propertyPanelControlContainer.GeneralControls.LayerComboBox = layerComboBox.ComboBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_Rebar()
        {
            BasicCheckBox isRebarCheckBox = new BasicCheckBox()
            {
                Name = "Treat as Rebar",
                CheckBoxText = "Treat as Rebar",
            };

            ComboBoxWithLabel rebarSize = new ComboBoxWithLabel()
            {
                Name = "Rebar Size",
                LabelText = "Rebar Size",
                ComboBoxWidth = 60,
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Rebar",
                Header = "Rebar",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(isRebarCheckBox);
            panel.AddProperty(rebarSize);

            foreach (KeyValuePair<string, RebarDefinition> bar in InstanceData.AvailableRebarSizes)
            {
                rebarSize.AddItemToComboBox(bar.Key);
            }

            rebarSize.ComboBox.SelectedIndexChanged += new EventHandler(PropertyPanelRebarSizeComboBox_SelectedIndexChanged);
            isRebarCheckBox.CheckBox.CheckStateChanged += new EventHandler(PropertyPanelTreatAsRebarCheckBox_CheckStateChanged);

            _propertyPanelControlContainer.RebarControls = new RebarPanelControls();

            _propertyPanelControlContainer.RebarControls.TreatAsRebarCheckBox = isRebarCheckBox.CheckBox;
            _propertyPanelControlContainer.RebarControls.RebarSizeComboBox = rebarSize.ComboBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_Material()
        {

            ComboBoxWithLabel materialComboBox = new ComboBoxWithLabel()
            {
                Name = "Material",
                LabelText = "Material",
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Material",
                Header = "Material",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(materialComboBox);

            materialComboBox.ComboBox.SelectedIndexChanged += new EventHandler(PropertyPanelMaterialsComboBox_SelectedIndexChanged);

            for (int i = 0; i < InstanceData.MaterialList.Count; i++)
            {
                materialComboBox.ComboBox.Items.Add(InstanceData.MaterialList[i].Name);
            }

            _propertyPanelControlContainer.MaterialControls = new MaterialPanelControls();

            _propertyPanelControlContainer.MaterialControls.MaterialComboBox = materialComboBox.ComboBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_ClosedGeometry()
        {
            BasicCheckBox treatAsHoleCheckBox = new BasicCheckBox()
            {
                Name = "Treat as Hole",
                CheckBoxText = "Treat as Hole",
            };

            TextBoxWithLabel areaTextBox = new TextBoxWithLabel()
            {
                Name = "Closed Area",
                LabelText = "Area",
                PostText = "in^2",
                ReadOnly = true,
            };

            TextBoxWithLabel perimeterTextBox = new TextBoxWithLabel()
            {
                Name = "Closed Perimeter",
                LabelText = "Perimeter",
                PostText = "in.",
                ReadOnly = true,
            };

            TextBoxWithLabel centroidTextBox = new TextBoxWithLabel()
            {
                Name = "Centroid",
                LabelText = "Centroid",
                TextBoxWidth = 100,
                ReadOnly = true,
            };

            TextBoxWithLabel ixTextBox = new TextBoxWithLabel()
            {
                Name = "Ixx",
                LabelText = "Ixx",
                PostText = "in^4",
                ReadOnly = true,
            };

            TextBoxWithLabel iyTextBox = new TextBoxWithLabel()
            {
                Name = "Iyy",
                LabelText = "Iyy",
                PostText = "in^4",
                ReadOnly = true,
            };

            TextBoxWithLabel ixyTextBox = new TextBoxWithLabel()
            {
                Name = "Ixy",
                LabelText = "Ixy",
                PostText = "in^4",
                ReadOnly = true,
            };

            TextBoxWithLabel sxTextBox = new TextBoxWithLabel()
            {
                Name = "Sxx",
                LabelText = "Sxx",
                PostText = "in^3",
                ReadOnly = true,
            };

            TextBoxWithLabel syTextBox = new TextBoxWithLabel()
            {
                Name = "Syy",
                LabelText = "Syy",
                PostText = "in^3",
                ReadOnly = true,
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Closed Geometry",
                Header = "Closed Geometry",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(treatAsHoleCheckBox);
            panel.AddProperty(areaTextBox);
            panel.AddProperty(perimeterTextBox);
            panel.AddProperty(centroidTextBox);
            panel.AddProperty(ixTextBox);
            panel.AddProperty(iyTextBox);
            panel.AddProperty(ixyTextBox);
            panel.AddProperty(sxTextBox);
            panel.AddProperty(syTextBox);

            treatAsHoleCheckBox.CheckBox.CheckStateChanged += new EventHandler(PropertyPanelHoleCheckBox_CheckStateChanged);

            _propertyPanelControlContainer.ClosedGeometryControls = new ClosedGeometryPanelControls();

            _propertyPanelControlContainer.ClosedGeometryControls.AreaTextBox = areaTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.CentroidTextBox = centroidTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.IxxTextBox = ixTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.IyyTextBox = iyTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.IxyTextBox = ixyTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.PerimeterTextBox = perimeterTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.SxxTextBox = sxTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.SyyTextBox = syTextBox.TextBox;
            _propertyPanelControlContainer.ClosedGeometryControls.TreatAsHoleCheckBox = treatAsHoleCheckBox.CheckBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_Nodes()
        {
            RichTextBoxWithLabel nodesRichTextBox = new RichTextBoxWithLabel()
            {
                Name = "Nodes",
                LabelText = "Element Nodes",
                ReadOnly = true,
                RichTextBoxWidth = 140,
                RichTextBoxHeight = 100,
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Nodes",
                Header = "Nodes",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(nodesRichTextBox);

            _propertyPanelControlContainer.NodesControls = new NodesPanelControls();

            _propertyPanelControlContainer.NodesControls.NodesRichTextBox = nodesRichTextBox.RichTextBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_OpenGeometry()
        {
            TextBoxWithLabel lengthTextBox = new TextBoxWithLabel()
            {
                Name = "Length",
                LabelText = "Length",
                PostText = "in.",
                ReadOnly = true,
                TextBoxWidth = 60
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Open Geometry",
                Header = "OpenGeometry",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(lengthTextBox);

            _propertyPanelControlContainer.OpenGeometryControls = new OpenGeometryPanelControls();

            _propertyPanelControlContainer.OpenGeometryControls.LengthTextBox = lengthTextBox.TextBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_PostTensioning()
        {
            ComboBoxWithLabel postTensioningTypeComboBox = new ComboBoxWithLabel()
            {
                Name = "Post Tensioning Type",
                LabelText = "PT Type",
                ComboBoxWidth = 140,
            };

            ComboBoxWithLabel jackingAndGroutingComboBox = new ComboBoxWithLabel()
            {
                Name = "Jacking and Grouting Type",
                LabelText = "Jacking/Grouting",
                ComboBoxWidth = 100,
            };

            TextBoxWithLabel preTensionTextBox = new TextBoxWithLabel()
            {
                Name = "Pretension",
                LabelText = "Prestress",
                PostText = "ksi",
                ReadOnly = false,
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "Post Tensioning",
                Header = "Post Tensioning",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(postTensioningTypeComboBox);
            panel.AddProperty(jackingAndGroutingComboBox);
            panel.AddProperty(preTensionTextBox);

            postTensioningTypeComboBox.ComboBox.SelectedIndexChanged += new EventHandler(PropertyPanelPostTensioningDefinitionComboBox_SelectedIndexChanged);
            jackingAndGroutingComboBox.ComboBox.SelectedIndexChanged += new EventHandler(PropertyPanelJackingAndGroutingComboBox_SelectedIndexChanged);
            preTensionTextBox.TextBox.TextChanged += new EventHandler(PropertyPanelInitialPrestressTextBox_TextChanged);

            foreach (PostTensioningDefinition pt in InstanceData.PostTensioningDefinitionList)
            {
                postTensioningTypeComboBox.AddItemToComboBox(pt.Name);
            }

            foreach (JackingAndGroutingType jg in Enum.GetValues(typeof(JackingAndGroutingType)))
            {
                jackingAndGroutingComboBox.AddItemToComboBox(jg.GetDescription());
            }

            _propertyPanelControlContainer.PostTensioningControls = new PostTensioningPanelControls();

            _propertyPanelControlContainer.PostTensioningControls.JackingAndGroutingComboBox = jackingAndGroutingComboBox.ComboBox;
            _propertyPanelControlContainer.PostTensioningControls.PostTensioningTypeComboBox = postTensioningTypeComboBox.ComboBox;
            _propertyPanelControlContainer.PostTensioningControls.PretensionTextBox = preTensionTextBox.TextBox;

            return panel;
        }

        private CollapsiblePanel BuildCollapsiblePanel_UserProperty()
        {
            UserPropertyDataGrid dgv = new UserPropertyDataGrid()
            {
                Name = "User Properties",
                LabelText = "User Properties",
                DataGridWidth = 200,
                DataGridHeight = 150,
                ButtonHeight = 25,
                ButtonWidth = 60,
            };

            CollapsiblePanel panel = new CollapsiblePanel()
            {
                Name = "User Properties",
                Header = "User Properties",
                Width = PropertyPanel.Width - (_propertyPanelClosedWidth + 5) - 5
            };

            panel.AddProperty(dgv);

            _propertyPanelControlContainer.UserPropertyControls = new UserPropertyPanelControls();

            _propertyPanelControlContainer.UserPropertyControls.DataGrid = dgv.DataGrid;
            _propertyPanelControlContainer.UserPropertyControls.AddButton = dgv.AddButton;
            _propertyPanelControlContainer.UserPropertyControls.RemoveButton = dgv.RemoveButton;

            _propertyPanelControlContainer.UserPropertyControls.DataGrid.CellValueChanged += new DataGridViewCellEventHandler(PropertyPanelUserPropertyDGV_CellContentChanged);
            _propertyPanelControlContainer.UserPropertyControls.DataGrid.CellClick += new DataGridViewCellEventHandler(PropertyPanelUserPropertyDGV_CellClick);
            return panel;
        }
        #endregion

        #region Panel Tool Strip
        private void PopulatePanelToolStripLayersDropdown()
        {
            if (_currentLayerDropdown != null)
            {
                _currentLayerDropdown.Items.Clear();

                if (InstanceData.Layers != null)
                {
                    for (int i = 0; i < InstanceData.Layers.Count; i++)
                    {
                        _currentLayerDropdown.Items.Add(InstanceData.Layers[i].Name);
                    }
                }
            }
        }

        private class CustomToolStripRenderer: ToolStripSystemRenderer
        {
            public CustomToolStripRenderer()
            {

            }

            protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
            {
                base.OnRenderToolStripBorder(e);

                ControlPaint.DrawBorder3D(e.Graphics, e.AffectedBounds);
                //ControlPaint.DrawFocusRectangle(e.Graphics, e.AffectedBounds, SystemColors.ControlDarkDark, SystemColors.ControlDarkDark);
            }
        }

        private void CurrentLayerDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            InstanceData.CurrentDrawingLayer = InstanceData.GetLayerFromName(_currentLayerDropdown.Text);
        }

        private void ToolStripHelpButton_Click(object sender, EventArgs e)
        {
            LoadCommandListForm(_commandList);
        }
        #endregion

        #region Changing Rendering Modes
        private void ChangeRenderingModes()
        {
            if (_draftingControl != null)
            {
                if (this.Controls.Contains(_draftingControl) == true)
                {
                    this.Controls.Remove(_draftingControl);
                }
            }

            if (_renderWithOpenGL == true)
            {
                _draftingControl = _openGLcontrol;
            }
            else if (_useGDI == true)
            {
                _draftingControl = DraftingPictureBox;
            }

            this.Controls.Add(_draftingControl);

            this.Invalidate();

            ResizeDraftingControl();
            
            _draftingControl.BringToFront();
            CoordLabel.BringToFront();
            InputTextBox.BringToFront();

            CreateDraftingGraphics();
        }
        #endregion

        #region Button Events
        private void ExpandPropertyPanelButton_Click(object sender, EventArgs e)
        {
            if (_propertyPanelOpen == true)
            {
                PropertyPanel.Width = _propertyPanelClosedWidth;
                _propertyPanelOpen = false;
            }
            else
            {
                PropertyPanel.Width = _propertyPanelOpenWidth;
                _propertyPanelOpen = true;
            }

            ResizeAll();
        }
        #endregion

        #region Graphics and Paitning
        public void CreateDraftingGraphics()
        {
            if (_renderWithOpenGL == true)
            {
                _openGLcontrol.Invalidate();
            }
            else if (_useGDI == true)
            {
                RenderWithGDI();
            }
        }

        private List<IDrawingElement> GetElementsInDraftingWindow()
        {
            Point p0 = new Point(0, _draftingControl.Height); // bottom left
            Point p1 = new Point(_draftingControl.Width, 0); // top right

            PointD p0d = ConvertToGlobalCoord(p0, InstanceData.ZeroPoint, InstanceData.DrawingScale);
            PointD p1d = ConvertToGlobalCoord(p1, InstanceData.ZeroPoint, InstanceData.DrawingScale);

            Rect rec = new Rect(
                Convert.ToInt32(Floor(p0d.X)),
                Convert.ToInt32(Floor(p0d.Y)),
                Convert.ToInt32(Ceiling(p1d.X - Floor(p0d.X))),
                Convert.ToInt32(Ceiling(p1d.Y - Floor(p0d.Y)))
                );

            List<IDrawingElement> elementsInBounds = InstanceData.DrawingElementQuadTree.GetNodesInside(rec).ToList();

            return elementsInBounds;
        }

        private void RenderWithGDI()
        {
            if (DraftingPictureBox == null)
            {
                throw new Exception("The drafting picture box is null.");
            }
            else
            {
                Stopwatch t = new Stopwatch();

                t.Start();

                //IDrawingElement[] elementsInBounds = GetElementsInDraftingWindow();

                List<IDrawingElement> elementsInBounds = GetElementsInDraftingWindow();

                elementsInBounds = elementsInBounds.OrderBy(x => InstanceData.DrawingElementList.GetDrawOrder(x)).ToList();

                Graphics g = Graphics.FromImage(DraftingBitmap);

                g.Clear(Color.White);

                List<IDrawingElement> selectedElements = new List<IDrawingElement>();

                //if (elementsInBounds != null)
                //{
                //    int arrayLen = elementsInBounds.Length;

                //    if (arrayLen > 0)
                //    {
                //        for (int i = 0; i < arrayLen; i++)
                //        {
                //            if (elementsInBounds[i].Selected == false)
                //            {
                //                elementsInBounds[i].Draw(ref g, InstanceData.ZeroPoint, InstanceData.DrawingScale, InstanceData);
                //            }
                //            else
                //            {
                //                selectedElements.Add(elementsInBounds[i]);
                //            }
                //        }
                //    }
                //}

                if (elementsInBounds != null)
                {
                    if (elementsInBounds.Count > 0)
                    {
                        for (int i = 0; i < elementsInBounds.Count; i++)
                        {
                            IDrawingElement elem = elementsInBounds[i];

                            if (elem.Layer.Visible == false)
                            {
                                continue;
                            }

                            if (elem.Selected == false)
                            {
                                elem.Draw(ref g, InstanceData.ZeroPoint, InstanceData.DrawingScale, InstanceData, SelectedElements.Count);
                            }
                            else
                            {
                                selectedElements.Add(elem);
                            }
                        }
                    }
                }

                if (selectedElements.Count > 0)
                {
                    for (int i = 0; i < selectedElements.Count; i++)
                    {
                        selectedElements[i].Draw(ref g, InstanceData.ZeroPoint, InstanceData.DrawingScale, InstanceData, SelectedElements.Count);
                    }
                }

                if (_tempMovedObjects != null)
                {
                    foreach (IDrawingElement elem in _tempMovedObjects)
                    {
                        elem.Draw(ref g, InstanceData.ZeroPoint, InstanceData.DrawingScale, InstanceData, SelectedElements.Count);
                    }
                }

                if (_drawingInProgress == true)
                {
                    if (_isSnapped == true && _snapPoint.SnapPoint != null)
                    {
                        DrawSnap(ref g);
                    }
                }

                g.Dispose();

                DraftingPictureBox.Invalidate();

                t.Stop();
                Debug.WriteLine($"GDI rendering took {t.ElapsedMilliseconds} ms");
            }
        }

        private void CreateDraftingGraphicsWithTempElements(PointD currentPoint)
        {
            if (_useGDI == true)
            {
                RenderTemporaryStateWithGDI(currentPoint);
            }
            else if (_renderWithOpenGL == true)
            {
                _draftingControl.Invalidate();
            }
        }

        private void RenderTemporaryStateWithGDI(PointD currentPoint)
        {
            Graphics g = Graphics.FromImage(DraftingBitmap);

            bool invalidateAtEnd = false;

            bool graphicsReCreated = false;

            if (_tempDrawingElements != null)
            {
                if (_tempDrawingElements.Count > 0)
                {
                    if (currentPoint != null)
                    {
                        CreateDraftingGraphics();

                        graphicsReCreated = true;

                        foreach (IDrawingElement ele in _tempDrawingElements)
                        {
                            ele.DrawTemp(ref g, InstanceData.ZeroPoint, currentPoint, InstanceData.DrawingScale, InstanceData);
                        }

                        invalidateAtEnd = true;
                    }
                }
            }

            if (_isSnapped == true && _snapPoint.SnapPoint != null)
            {
                if (graphicsReCreated == false)
                {
                    CreateDraftingGraphics();
                }

                DrawSnap(ref g);

                invalidateAtEnd = true;
            }
            else
            {
                if (_isSnapped == false && _previouslySnapped == true)
                {
                    if (graphicsReCreated == false)
                    {
                        CreateDraftingGraphics();
                    }

                    invalidateAtEnd = true;
                }
            }

            g.Dispose();

            if (invalidateAtEnd == true)
            {
                DraftingPictureBox.Invalidate();
            }
        }

        private void DraftingPictureBox_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            e.Graphics.DrawImage(DraftingBitmap, InstanceData.BitmapInsertionPoint.X, InstanceData.BitmapInsertionPoint.Y);
            watch.Stop();
            double t1 = watch.ElapsedMilliseconds;

            watch.Reset();

        }

        #endregion

        #region Control Resizing
        private void ResizeAllControls(object sender, EventArgs e)
        {
            ResizeAll();
            Invalidate();
        }

        private void ResizeAll()
        {
            if(ShowPropertyPanel == true)
            {
                ResizePropertyPanel();
            }

            if (ShowPanelToolStrip == true)
            {
                ResizePanelToolStrip();
            }

            ResizeDraftingControl();

            if(AllowKeyboardCommands == true)
            {
                ResizeInputTextBox();
            }

            ResizeCoordinateLabel();
        }

        private void ResizeDraftingControl()
        {
            int newWidth = (ShowPropertyPanel == true) ? this.Width - 40 - PropertyPanel.Width : this.Width - 20;

            int newHeight = (ShowPanelToolStrip == true) ? (this.Height - _topLeft.Y - _panelToolStrip.Height - BottomBuffer) : (this.Height - _topLeft.Y - BottomBuffer);

            Size newSize = new Size(newWidth, newHeight);

            _draftingControl.Size = newSize;

            if (_renderWithOpenGL == true)
            {

            }
            else if (_useGDI == true)
            {
                SetGDIValues();
            }

            CreateDraftingGraphics();
        }

        private void SetGDIValues()
        {
            DraftingBitmap.Dispose();
            DraftingBitmap = new Bitmap(DraftingPictureBox.Width, DraftingPictureBox.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);

            InstanceData.DrawingScale = InstanceData.BaseDrawingScale * InstanceData.MagnificationLevel;
            InstanceData.SelectionDistance = InstanceData.BaseSelectionDistance / InstanceData.MagnificationLevel;
            InstanceData.PanelSnapProperties.BaseSnapDistance = 1.0;
            InstanceData.PanelSnapProperties.SnapDistance = InstanceData.PanelSnapProperties.BaseSnapDistance / InstanceData.MagnificationLevel;
        }

        private void ResizeInputTextBox()
        {
            InputTextBox.Size = new Size(_draftingControl.Width - 4, InputTextBox.Height); // = DraftingPictureBox.Width - 4;
            InputTextBox.Location = new Point(_draftingControl.Location.X + 2, _draftingControl.Location.Y + _draftingControl.Height - InputTextBox.Height - 2);
        }

        private void ResizeCoordinateLabel()
        {
            int yLoc = (AllowKeyboardCommands == true) ? _draftingControl.Location.Y + _draftingControl.Height - 50 : _draftingControl.Location.Y + _draftingControl.Height - 30;

            CoordLabel.Location = new Point(_draftingControl.Location.X + 2, yLoc);
        }

        private void ResizePropertyPanel()
        {
            if (PropertyPanel != null)
            {
                PropertyPanel.Height = this.Height - _topLeft.Y - BottomBuffer;
                PropertyPanel.Location = new Point(this.Width - PropertyPanel.Width - 20, _topLeft.Y);

                if (ExpandPropertyPanelButton != null)
                {
                    ExpandPropertyPanelButton.Location = new Point(6, PropertyPanel.Height / 2 - ExpandPropertyPanelButton.Height / 2);
                }
            }
        }

        private void ResizePanelToolStrip()
        {
            int newWidth = (ShowPropertyPanel == true) ? this.Width - 40 - PropertyPanel.Width : this.Width - 20;

            _panelToolStrip.Width = newWidth;
            //_panelToolStrip.Size = new Size(newWidth, _panelToolStrip.Height);
        }
        #endregion

        #region Mouse Events
        private void DraftingPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            PreviousMousePoint = MousePoint;
            MousePoint = new Point(e.X, e.Y);
        }

        private void DraftingPictureBox_MouseEnter(object sender, EventArgs e)
        {
            CheckTimer.Enabled = true;

            _draftingControl.Focus();

            if (_useGDI == true)
            {
                Application.AddMessageFilter(DraftingPictureBox);
            }
        }

        private void DraftingPictureBox_MouseLeave(object sender, EventArgs e)
        {
            CoordLabel.Focus();
            CheckTimer.Enabled = false;
        }

        private void PingMouseMoveInDraftingPictureBox()
        {
            _currentGlobalPoint = ConvertToGlobalCoord(MousePoint, InstanceData.ZeroPoint, InstanceData.DrawingScale);

            CoordLabel.Text = "X = " + Convert.ToString(Round(_currentGlobalPoint.X, InstanceData.DisplayPrecision)) + " in." + Environment.NewLine + "Y = " + Convert.ToString(Round(_currentGlobalPoint.Y, InstanceData.DisplayPrecision)) + " in.";

            PointD snapPoint = null;

            switch (_panning)
            {
                case true:
                    InstanceData.ZeroPoint = new Point(_panZeroPoint.X + (MousePoint.X - _panStartPoint.X), _panZeroPoint.Y + (MousePoint.Y - _panStartPoint.Y));
                    break;
                case false:
                    break;
            }

            switch (_drawingInProgress)
            {
                case true:
                    snapPoint = GetSnappedPoint(_currentGlobalPoint);

                    if (snapPoint != null)
                    {
                        _currentGlobalPoint = new PointD(snapPoint.X, snapPoint.Y);
                    }

                    switch (_currentDrawingType)
                    {
                        case DrawingType.Move:
                            MoveTempMovedObjects(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        case DrawingType.Rotate:
                            RotateTempRotatedElements(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        case DrawingType.LinearArray:
                            LinearArrayTempElements(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        case DrawingType.Paste:
                            MoveTempCopiedObjects(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        case DrawingType.Reshape:
                            ReshapeTempElements(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        case DrawingType.Offset:
                            DrawTempOffsetElements(_currentGlobalPoint);
                            CreateDraftingGraphics();
                            break;
                        default:
                            CreateDraftingGraphicsWithTempElements(_currentGlobalPoint);
                            break;
                    }
                    break;
                case false:
                    switch (_currentDrawingType)
                    {
                        case DrawingType.None:
                        case DrawingType.ZoomWindow:
                            switch (_selectionBoxInitiated)
                            {
                                case true:
                                    DrawSelectionBox(_currentGlobalPoint);
                                    break;
                                case false:
                                    switch (_panning)
                                    {
                                        case true:
                                            CreateDraftingGraphics();
                                            break;
                                        case false:
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (_panning)
                            {
                                case true:
                                    CreateDraftingGraphics();
                                    break;
                                case false:
                                    snapPoint = GetSnappedPoint(_currentGlobalPoint);

                                    if (snapPoint != null)
                                    {
                                        _currentGlobalPoint = new PointD(snapPoint.X, snapPoint.Y);
                                    }

                                    CreateDraftingGraphicsWithTempElements(_currentGlobalPoint);

                                    break;
                            }
                            
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        private void DraftingPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            PreviousMousePoint = MousePoint;
            MousePoint = new Point(e.X, e.Y);

            PointD globalPoint = ConvertToGlobalCoord(MousePoint, InstanceData.ZeroPoint, InstanceData.DrawingScale);

            switch (e.Button)
            {
                case MouseButtons.Left:
                    PointD snapPoint = null;
                    
                    if (!_currentDrawingType.Equals(DrawingType.None))
                    {
                        snapPoint = GetSnappedPoint(globalPoint);
                    }

                    if (snapPoint != null)
                    {
                        globalPoint = new PointD(snapPoint.X, snapPoint.Y);
                    }

                    switch (_drawingInProgress)
                    {
                        case false:
                            switch (_currentDrawingType)
                            {
                                case DrawingType.Polyline:
                                    BeginDrawingPolylineViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Line:
                                    BeginDrawingLineViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Circle:
                                    BeginDrawingCircleViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Point:
                                    DrawPointViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Rectangle:
                                    BeginDrawingRectangleViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Arc:
                                    if (_tempPoints.Count <= 0)
                                    {
                                        BeginDrawingArcViaMouseClick(globalPoint);
                                    }
                                    else
                                    {
                                        ContinueDrawingArcViaMouseClick(globalPoint);
                                    }
                                    break;
                                case DrawingType.Move:
                                    BeginMoveViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Rotate:
                                    BeginRotateViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.LinearArray:
                                    BeginLinearArrayViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.CopyWithBasePoint:
                                    CopyElementsWithBasePointViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.None:
                                case DrawingType.Offset:
                                case DrawingType.ZoomWindow:
                                    BeginSelectionBox(globalPoint);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case true:
                            switch (_currentDrawingType)
                            {
                                case DrawingType.Polyline:
                                    ContinueDrawingPolylineViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Line:
                                    EndDrawingLineViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Circle:
                                    EndDrawingCircleViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Rectangle:
                                    EndDrawingRectangleViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Arc:
                                    ContinueDrawingArcViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.RebarSection:
                                    EndDrawingRebarSectionViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.PostTensioning:
                                    EndDrawingPostTensioningSectionViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Move:
                                    EndMoveViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Rotate:
                                    EndRotateViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.LinearArray:
                                    EndLinearArrayViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Paste:
                                    EndPasteViaMouseClick(globalPoint);
                                    break;
                                case DrawingType.Reshape:
                                    ReshapeOriginalElements(globalPoint);
                                    break;
                                case DrawingType.Offset:
                                    MouseClickedWhileOffsetting(globalPoint);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case MouseButtons.Right:

                    break;
                case MouseButtons.Middle:
                    _panStartPoint = new Point(MousePoint.X, MousePoint.Y);
                    _panZeroPoint = new Point(InstanceData.ZeroPoint.X, InstanceData.ZeroPoint.Y);
                    _panning = true;
                    this.Cursor = Cursors.Hand;
                    break;
                default:
                    break;
            }
        }

        private void DraftingPictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            PreviousMousePoint = MousePoint;
            MousePoint = new Point(e.X, e.Y);

            PointD globalPoint = ConvertToGlobalCoord(MousePoint, InstanceData.ZeroPoint, InstanceData.DrawingScale);

            switch (e.Button)
            {
                case MouseButtons.Left:
                    switch (_currentDrawingType)
                    {
                        case DrawingType.None:
                        case DrawingType.Offset:
                            switch (_drawingInProgress)
                            {
                                case false:
                                    switch (_selectionBoxInitiated)
                                    {
                                        case true:
                                            if (_selectionBoxStart == globalPoint)
                                            {
                                                //Single click selection or de-selection, or reshaping
                                                if (BeginReshapeElements(globalPoint, out List<IDrawingElement> elementsToReshape, out List<DrawingNode> nodesToReshape) == true)
                                                {
                                                    if (AllowDrawing == true)
                                                    {
                                                        _origElementsToReshape = elementsToReshape;
                                                        _origNodesToReshape = nodesToReshape;

                                                        SetDrawingStateToReshape();

                                                        BuildTempReshapedElements(globalPoint, elementsToReshape, nodesToReshape);
                                                    }
                                                }
                                                else
                                                {
                                                    SingleClickSelection(globalPoint);
                                                }
                                            }
                                            else
                                            {
                                                EndSelectionBox(globalPoint);
                                            }

                                            if (_currentDrawingType.Equals(DrawingType.Offset) == true)
                                            {
                                                MouseClickedWhileOffsetting(globalPoint);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case true:
                                    break;
                            }
                            
                            break;
                        case DrawingType.ZoomWindow:
                            switch (_selectionBoxInitiated)
                            {
                                case true:
                                    if (_selectionBoxStart == globalPoint)
                                    {
                                        ResetDrawingState();

                                        _selectionBoxInitiated = false;
                                    }
                                    else
                                    {
                                        ZoomWindow(_selectionBoxStart, globalPoint);
                                    }
                                    break;
                                default:
                                    break;
                            }

                            break;
                        default:
                            break;
                    }
                    break;
                case MouseButtons.Middle:
                    _panning = false;
                    switch (_currentDrawingType)
                    {
                        case DrawingType.TriangleNet:
                        case DrawingType.None:
                            this.Cursor = Cursors.Arrow;
                            break;
                        default:
                            this.Cursor = Cursors.Cross;
                            break;
                    }
                    break;
                default:
                    break;
            }

        }

        private void DraftingPictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
            try
            {
                InstanceData.PreviousMagnificationLevel = InstanceData.MagnificationLevel;
               
                Point focusPoint = new Point(e.X, e.Y);

                PointD globalFocusPoint = ConvertToGlobalCoord(MousePoint, InstanceData.ZeroPoint, InstanceData.DrawingScale);

                if (e.Delta > 0)
                {
                    InstanceData.WheelClicks += 1;
                }
                else if (e.Delta < 0)
                {
                    InstanceData.WheelClicks -= 1;
                }

                if (InstanceData.WheelClicks >= 0)
                {
                    InstanceData.MagnificationLevel = Sqrt(3.0 * InstanceData.WheelClicks + 1.0);
                }
                else
                {
                    InstanceData.MagnificationLevel = 1.0 - 1.0 / (Pow(5.0, 1.0 / Abs(InstanceData.WheelClicks)));
                }

                InstanceData.DrawingScale = InstanceData.BaseDrawingScale * InstanceData.MagnificationLevel;

                int zpX = Convert.ToInt32(focusPoint.X - globalFocusPoint.X * InstanceData.DrawingScale);
                int zpY = Convert.ToInt32(focusPoint.Y + globalFocusPoint.Y * InstanceData.DrawingScale);

                InstanceData.ZeroPoint = new Point(zpX, zpY);

                InstanceData.PanelSnapProperties.SnapDistance = InstanceData.PanelSnapProperties.BaseSnapDistance / InstanceData.MagnificationLevel;

                InstanceData.SelectionDistance = InstanceData.BaseSelectionDistance / InstanceData.MagnificationLevel;

                CreateDraftingGraphics();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion

        #region Keyboard Events
        private void DraftingPictureBoxHasFocus_KeyPress(object sender, KeysPressedEventArgs e)
        {

            if (_draftingControl.ContainsFocus == false)
            {
                return;
            }

            Keys key = e.KeyCode;
            switch (key)
            {
                case Keys.Return:
                    if (AllowKeyboardCommands == true)
                    {
                        switch (_drawingInProgress)
                        {
                            case true:
                                switch (_currentDrawingType)
                                {
                                    case DrawingType.Polyline:
                                        ReturnKeyPressedWhileDrawingPolyline();
                                        break;
                                    case DrawingType.Line:
                                        ReturnKeyPressedWhileDrawingLine();
                                        break;
                                    case DrawingType.Circle:
                                        ReturnKeyPressedWhileDrawingCircle();
                                        break;
                                    case DrawingType.Point:
                                        ReturnKeyPressedWhileDrawingPoint();
                                        break;
                                    case DrawingType.Rectangle:
                                        ReturnKeyPressedWhileDrawingRectangle();
                                        break;
                                    case DrawingType.Arc:
                                        ReturnKeyPressedWhileDrawingArc();
                                        break;
                                    case DrawingType.RebarSection:
                                        ReturnKeyPressedWhileDrawingRebarSection();
                                        break;
                                    case DrawingType.PostTensioning:
                                        ReturnKeyPressedWhileDrawingPostTensioningSection();
                                        break;
                                    case DrawingType.Move:
                                        ReturnKeyPressedWhileMoving();
                                        break;
                                    case DrawingType.Rotate:
                                        ReturnKeyPressedWhileRotating();
                                        break;
                                    case DrawingType.LinearArray:
                                        ReturnKeyPressedWhileLinearArraying();
                                        break;
                                    case DrawingType.Paste:
                                        ReturnKeyPressedWhilePasting();
                                        break;
                                    case DrawingType.Offset:
                                        ReturnKeyPressedWhileOffsetting();
                                        break;
                                    default:
                                        break;
                                }

                                break;
                            case false:
                                switch (_currentDrawingType)
                                {
                                    case DrawingType.None:
                                        if (String.Compare(InputTextBox.Text, "pl", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "polyline", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToPolyline();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "c", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "circle", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToCircle();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "l", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "line", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToLine();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "p", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "point", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToPoint();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "rec", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "rectangle", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToRectangle();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "arc", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToArc();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "m", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "move", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToMove();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "ro", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "rotate", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToRotate();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "arr", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "array", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToLinearArray();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "rbs", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToRebarSection();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "pts", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToPostTensioningSection();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "zw", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToZoomWindow();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "ze", ignoreCase: true) == 0)
                                        {
                                            ZoomExtents();

                                            InputTextBox.Text = "";

                                            _readOnlyChars = InputTextBox.Text.Length;
                                        }
                                        else if (String.Compare(InputTextBox.Text, "j", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "join", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 1)
                                            {
                                                JoinSelectedElements();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "ex", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "explode", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 0)
                                            {
                                                Explode();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "cs", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "close", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 0)
                                            {
                                                CloseShape();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "o", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "offset", ignoreCase: true) == 0)
                                        {
                                            SetDrawingStateToOffset();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "bu", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "booleanunion", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 0)
                                            {
                                                SetDrawingStateToBooleanUnion();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "bi", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "booleanintersection", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 0)
                                            {
                                                SetDrawingStateToBooleanIntersection();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "bd", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "booleandifference", ignoreCase: true) == 0)
                                        {
                                            if (SelectedElements.Count > 0)
                                            {
                                                SetDrawingStateToBooleanDifference();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "commands", ignoreCase: true) == 0)
                                        {
                                            _lastCommand = "commands";

                                            LoadCommandListForm(_commandList);
                                        }
                                        else if (String.Compare(InputTextBox.Text, "layers", ignoreCase: true) == 0)
                                        {
                                            LayerProperties();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "opt", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "options", ignoreCase: true) == 0)
                                        {
                                            ShowProgramOptionsDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "materials", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "mat", ignoreCase: true) == 0)
                                        {
                                            ShowMaterialsDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "rbp", ignoreCase: true) == 0)
                                        {
                                            ShowRebarPropertiesDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "ptp", ignoreCase: true) == 0)
                                        {
                                            ShowPostTensioningPropertiesDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "snap", ignoreCase: true) == 0)
                                        {
                                            ShowSnapOptionsDialog(InstanceData.PanelSnapProperties);
                                        }
                                        else if (String.Compare(InputTextBox.Text, "trimesh", ignoreCase: true) == 0)
                                        {
                                            if (ElementsSelected() == true)
                                            {
                                                SetDrawingStateToTriangleNet();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "quadmesh", ignoreCase: true) == 0)
                                        {
                                            if (ElementsSelected() == true)
                                            {
                                                SetDrawingStateToQuadMesh();
                                            }
                                        }
                                        else if (String.Compare(InputTextBox.Text, "pm", ignoreCase: true) == 0)
                                        {
                                            ShowAxialForceMomentDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "mc", ignoreCase: true) == 0)
                                        {
                                            ShowMomentCurvatureDialog();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "tmo", ignoreCase: true) == 0 || String.Compare(InputTextBox.Text, "triop", ignoreCase: true) == 0)
                                        {
                                            DisplayTriangleMeshOptions();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "makehole", ignoreCase: true) == 0)
                                        {
                                            MakeHole();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "opengl", ignoreCase:true) == 0)
                                        {
                                            _lastCommand = "opengl";
                                            UseOpenGL = true;
                                            ResetDrawingState();
                                        }
                                        else if (String.Compare(InputTextBox.Text, "gdi", ignoreCase: true) == 0)
                                        {
                                            _lastCommand = "gdi";
                                            UseGDI = true;
                                            ResetDrawingState();
                                        }
                                        else if (InputTextBox.Text.Equals(string.Empty) == true)
                                        {
                                            InterpretLastDrawingCommand();
                                        }
                                        break;
                                    case DrawingType.Polyline:
                                        ReturnKeyPressedWhileDrawingPolyline();
                                        break;
                                    case DrawingType.Line:
                                        ReturnKeyPressedWhileDrawingLine();
                                        break;
                                    case DrawingType.Circle:
                                        ReturnKeyPressedWhileDrawingCircle();
                                        break;
                                    case DrawingType.Point:
                                        ReturnKeyPressedWhileDrawingPoint();
                                        break;
                                    case DrawingType.Rectangle:
                                        ReturnKeyPressedWhileDrawingRectangle();
                                        break;
                                    case DrawingType.Arc:
                                        ReturnKeyPressedWhileDrawingArc();
                                        break;
                                    case DrawingType.RebarSection:
                                        ReturnKeyPressedWhileDrawingRebarSection();
                                        break;
                                    case DrawingType.PostTensioning:
                                        ReturnKeyPressedWhileDrawingPostTensioningSection();
                                        break;
                                    case DrawingType.Move:
                                        ReturnKeyPressedWhileMoving();
                                        break;
                                    case DrawingType.Rotate:
                                        ReturnKeyPressedWhileRotating();
                                        break;
                                    case DrawingType.LinearArray:
                                        ReturnKeyPressedWhileLinearArraying();
                                        break;
                                    case DrawingType.CopyWithBasePoint:
                                        ReturnKeyPressedWhileCopying();
                                        break;
                                    case DrawingType.Offset:
                                        ReturnKeyPressedWhileOffsetting();
                                        break;
                                    case DrawingType.BooleanUnion:
                                        ReturnKeyPressedWhileBooleanUnion();
                                        break;
                                    case DrawingType.BooleanIntersection:
                                        ReturnKeyPressedWhileBooleanIntersection();
                                        break;
                                    case DrawingType.BooleanDifference:
                                        ReturnKeyPressedWhileBooleanDifference();
                                        break;
                                    case DrawingType.TriangleNet:
                                    case DrawingType.QuadMesh:
                                        ReturnKeyPressedWhileTriangleNet();
                                        break;
                                    default:
                                        break;
                                }

                                break;
                            default:
                                break;
                        }
                    }

                    break;
                case Keys.Escape:
                    switch (_drawingInProgress)
                    {
                        case true:
                            switch (_currentDrawingType)
                            {
                                case DrawingType.Reshape:
                                    ResetReshapeOnly();
                                    CreateDraftingGraphics();
                                    break;
                                default:
                                    ResetDrawingState();
                                    CreateDraftingGraphics();
                                    break;
                            }
                            break;
                        case false:
                            ClearSelectedElements();

                            switch (_currentDrawingType)
                            {
                                case DrawingType.None:
                                    break;
                                default:
                                    if (_snapPoint != null)
                                    {
                                        CreateDraftingGraphics();
                                    }
                                    break;
                            }

                            ResetDrawingState();

                            break;
                        default:
                            break;
                    }
                    break;
                case Keys.Space:
                    InputTextBox.AppendText(" ");
                    break;
                case Keys.Back:
                    string myStr = InputTextBox.Text;

                    if (myStr.Length > 1)
                    {
                        if (myStr.Length > _readOnlyChars)
                        {
                            InputTextBox.Text = myStr.Left(myStr.Length - 1);
                        }
                    }
                    else
                    {
                        if (myStr.Length > _readOnlyChars)
                        {
                            InputTextBox.Text = "";
                        }
                    }
                    break;
                case Keys.Delete:
                    switch (_currentDrawingType)
                    {
                        case DrawingType.None:
                            if (AllowKeyboardCommands == true)
                            {
                                DeleteSelectedElements();
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case Keys.C:
                    if (e.Control == true && e.Alt == false && e.Shift == false)
                    {
                        _copyWithBasePoint = false;
                        SetDrawingStateToCopy();
                    }
                    else if (e.Control == true && e.Alt == false && e.Shift == true)
                    {
                        _copyWithBasePoint = true;
                        SetDrawingStateToCopy();
                    }
                    else
                    {
                        InputTextBox.AppendText(ConvertKeysPressedEventArgsToString(e));
                    }
                    break;
                case Keys.V:
                    if (e.Control == true && e.Alt == false && e.Shift == false)
                    {
                        SetDrawingStateToPaste();
                    }
                    else
                    {
                        InputTextBox.AppendText(ConvertKeysPressedEventArgsToString(e));
                    }
                    break;
                case Keys.Z:
                    if (e.Control == true && e.Alt == false && e.Shift == false)
                    {
                        UndoLastAction();
                    }
                    else
                    {
                        InputTextBox.AppendText(ConvertKeysPressedEventArgsToString(e));
                    }
                    break;
                case Keys.Tab:
                    switch (_drawingInProgress)
                    {
                        case true:
                            break;
                        case false:
                            CycleNextSelectionPossibility();
                            break;
                    }

                    break;
                case Keys.F3:
                    InstanceData.PanelSnapProperties.PointSnapsOn = !InstanceData.PanelSnapProperties.PointSnapsOn;
                    break;
                case Keys.F9:
                    InstanceData.PanelSnapProperties.SnapOrtho = !InstanceData.PanelSnapProperties.SnapOrtho;
                    break;
                default:
                    if (AllowKeyboardCommands == true)
                    {
                        InputTextBox.AppendText(ConvertKeysPressedEventArgsToString(e));
                    }
                    break;
            }

            _currentKeysDown = e;
        }

        private void DraftingPictureBoxHasFocus_KeyUp(object sender, KeysPressedEventArgs e)
        {
            if (_draftingControl.ContainsFocus == true)
            {
                if (e.Shift == true)
                {
                    _currentKeysDown.Shift = false;
                }
                else if (e.Control == true)
                {
                    _currentKeysDown.Control = false;
                }
                else if (e.Alt == true)
                {
                    _currentKeysDown.Alt = false;
                }
            }
        }

        public static char ChrW(int code)
        {
            return (char)code;
        }

        public string ConvertKeysPressedEventArgsToString(KeysPressedEventArgs e)
        {
            if (e.Shift == true && e.Control == false && e.Alt == false)
            {
                return ConvertKeysPlusShiftToString(e.KeyCode, e.CapsLock);
            }
            else if (e.Shift == false && e.Control == false && e.Alt == false)
            {
                return ConvertKeysToString(e.KeyCode, e.CapsLock);
            }
            else
            {
                return String.Empty;
            }
        }

        public string ConvertKeysToString(Keys key, bool capsLock)
        {
            string s = String.Empty;

            int intKey = (int)key;

            if (intKey >= 65 && intKey <= 90)
            {
                s = Convert.ToString(ChrW((int)key));
            }
            else if (intKey >=96 && intKey <= 105)
            {
                //Numpad
                switch (key)
                {
                    case Keys.NumPad0:
                        s = "0";
                        break;
                    case Keys.NumPad1:
                        s = "1";
                        break;
                    case Keys.NumPad2:
                        s = "2";
                        break;
                    case Keys.NumPad3:
                        s = "3";
                        break;
                    case Keys.NumPad4:
                        s = "4";
                        break;
                    case Keys.NumPad5:
                        s = "5";
                        break;
                    case Keys.NumPad6:
                        s = "6";
                        break;
                    case Keys.NumPad7:
                        s = "7";
                        break;
                    case Keys.NumPad8:
                        s = "8";
                        break;
                    case Keys.NumPad9:
                        s = "9";
                        break;
                    default:
                        s = "";
                        break;
                }
            }
            else if (key.Equals(Keys.Decimal))
            {
                s = ".";
            }
            else if (intKey >= 48 && intKey <= 57)
            {
                s = Convert.ToString(ChrW((int)key));
            }
            else if (key.Equals(Keys.Oemplus))
            {
                s = "=";
            }
            else if (key.Equals(Keys.OemMinus))
            {
                s = "-";
            }
            else if (key.Equals(Keys.Oemcomma))
            {
                s = ",";
            }
            else if (key.Equals(Keys.OemPeriod))
            {
                s = ".";
            }
            else if (key.Equals(Keys.OemQuestion))
            {
                s = "/";
            }
            else if (key.Equals(Keys.Oem1))
            {
                s = ";";
            }
            else if (key.Equals(Keys.Oem7))
            {
                s = "'";
            }
            else if (key.Equals(Keys.Oem4))
            {
                s = "[";
            }
            else if (key.Equals(Keys.Oem6))
            {
                s = "]";
            }
            if (capsLock == true)
            {
                return s;
            }
            else
            {
                return s.ToLower();
            }
        }

        public string ConvertKeysPlusShiftToString(Keys key, bool capsLock)
        {
            if (key.Equals(Keys.D0))
            {
                return ")";
            }
            else if (key.Equals(Keys.D1))
            {
                return "!";
            }
            else if (key.Equals(Keys.D2))
            {
                return "@";
            }
            else if (key.Equals(Keys.D3))
            {
                return "#";
            }
            else if (key.Equals(Keys.D4))
            {
                return "$";
            }
            else if (key.Equals(Keys.D5))
            {
                return "%";
            }
            else if (key.Equals(Keys.D6))
            {
                return "^";
            }
            else if (key.Equals(Keys.D8))
            {
                return "*";
            }
            else if (key.Equals(Keys.D7))
            {
                return "&";
            }
            else if (key.Equals(Keys.D9))
            {
                return "(";
            }
            else if (key.Equals(Keys.OemMinus))
            {
                return "_";
            }
            else if (key.Equals(Keys.Oemcomma))
            {
                return "<";
            }
            else if (key.Equals(Keys.OemPeriod))
            {
                return ">";
            }
            else if (key.Equals(Keys.Oemplus))
            {
                return "+";
            }
            else if (key.Equals(Keys.OemQuestion))
            {
                return "?";
            }
            else if (key.Equals(Keys.Oem1))
            {
                return ":";
            }
            else if (key.Equals(Keys.Oem7))
            {
                return @"""";
            }
            else if (key.Equals(Keys.Oem4))
            {
                return "{";
            }
            else if (key.Equals(Keys.Oem6))
            {
                return "}";
            }
            else if ((int)key >= 65 && (int)key <= 90)
            {
                if (capsLock == true)
                {
                    return Convert.ToString(ChrW((int)key)).ToLower();
                }
                else
                {
                    return Convert.ToString(ChrW((int)key));
                }
            }

            return string.Empty;
        }

        private void InterpretLastDrawingCommand()
        {
            if (String.Compare(_lastCommand, "pl", ignoreCase: true) == 0)
            {
                SetDrawingStateToPolyline();
            }
            else if (String.Compare(_lastCommand, "l", ignoreCase: true) == 0)
            {
                SetDrawingStateToLine();
            }
            else if (String.Compare(_lastCommand, "line", ignoreCase: true) == 0)
            {
                SetDrawingStateToLine();
            }
            else if (String.Compare(_lastCommand, "c", ignoreCase: true) == 0)
            {
                SetDrawingStateToCircle();
            }
            else if (String.Compare(_lastCommand, "circle", ignoreCase: true) == 0)
            {
                SetDrawingStateToCircle();
            }
            else if (String.Compare(_lastCommand, "rec", ignoreCase: true) == 0)
            {
                SetDrawingStateToRectangle();
            }
            else if (String.Compare(_lastCommand, "rectangle", ignoreCase: true) == 0)
            {
                SetDrawingStateToRectangle();
            }
            else if (String.Compare(_lastCommand, "m", ignoreCase: true) == 0)
            {
                SetDrawingStateToMove();
            }
            else if (String.Compare(_lastCommand, "move", ignoreCase: true) == 0)
            {
                SetDrawingStateToMove();
            }
            else if (String.Compare(_lastCommand, "paste", ignoreCase: true) == 0)
            {
                SetDrawingStateToPaste();
            }
            else if (String.Compare(_lastCommand, "ro", ignoreCase: true) == 0)
            {
                SetDrawingStateToRotate();
            }
            else if (String.Compare(_lastCommand, "arr", ignoreCase: true) == 0)
            {
                SetDrawingStateToLinearArray();
            }
            else if (String.Compare(_lastCommand, "arc", ignoreCase: true) == 0)
            {
                SetDrawingStateToArc();
            }
            else if (String.Compare(_lastCommand, "rbs", ignoreCase: true) == 0)
            {
                SetDrawingStateToRebarSection();
            }
            else if (String.Compare(_lastCommand, "zw", ignoreCase: true) == 0)
            {
                SetDrawingStateToZoomWindow();
            }
            else if (String.Compare(_lastCommand, "pts", ignoreCase: true) == 0)
            {
                SetDrawingStateToPostTensioningSection();
            }
            else if (String.Compare(_lastCommand, "gdi", ignoreCase: true) == 0)
            {
                UseGDI = true;
                ResetDrawingState();
            }
            else if (String.Compare(_lastCommand, "opengl", ignoreCase:true) == 0)
            {
                UseOpenGL = true;
                ResetDrawingState();
            }
            else if (String.Compare(_lastCommand, "j", ignoreCase: true) == 0 || String.Compare(_lastCommand, "join", ignoreCase: true) == 0)
            {
                if (SelectedElements != null && SelectedElements.Count > 0)
                {
                    JoinSelectedElements();
                }
            }
            else if (String.Compare(_lastCommand, "ex", ignoreCase: true) == 0 || String.Compare(_lastCommand, "explode", ignoreCase: true) == 0)
            {
                if (SelectedElements != null && SelectedElements.Count > 0)
                {
                    Explode();
                }
            }
            else if (String.Compare(_lastCommand, "cs", ignoreCase: true) == 0 || String.Compare(_lastCommand, "close", ignoreCase: true) == 0)
            {
                if (SelectedElements != null && SelectedElements.Count > 0)
                {
                    CloseShape();
                }
            }
            else if (String.Compare(_lastCommand, "o", ignoreCase: true) == 0 || String.Compare(_lastCommand, "offset", ignoreCase: true) == 0)
            {
                SetDrawingStateToOffset();
            }
            else if (String.Compare(_lastCommand, "commands", ignoreCase: true) == 0)
            {
                LoadCommandListForm(_commandList);
            }
            else if (String.Compare(_lastCommand, "layers", ignoreCase: true) == 0)
            {
                LayerProperties();
            }
            else if (String.Compare(_lastCommand, "mat", ignoreCase: true) == 0)
            {
                ShowMaterialsDialog();
            }
            else if (String.Compare(_lastCommand, "opt", ignoreCase: true) == 0)
            {
                ShowProgramOptionsDialog();
            }
            else if (String.Compare(_lastCommand, "snap", ignoreCase: true) == 0)
            {
                ShowSnapOptionsDialog(InstanceData.PanelSnapProperties);
            }
            else if (String.Compare(_lastCommand, "booleanunion", ignoreCase: true) == 0)
            {
                if (ElementsSelected() == true)
                {
                    SetDrawingStateToBooleanUnion();
                }
            }
            else if (String.Compare(_lastCommand, "booleanintersection", ignoreCase: true) == 0)
            {
                if (ElementsSelected() == true)
                {
                    SetDrawingStateToBooleanIntersection();
                }
            }
            else if (String.Compare(_lastCommand, "booleandifference", ignoreCase: true) == 0)
            {
                if (ElementsSelected() == true)
                {
                    SetDrawingStateToBooleanDifference();
                }
            }
            else if (String.Compare(_lastCommand, "trimesh", ignoreCase: true) == 0)
            {
                if (ElementsSelected() == true)
                {
                    SetDrawingStateToTriangleNet();
                }
            }
            else if (String.Compare(_lastCommand, "pm", ignoreCase: true) == 0)
            {
                ShowAxialForceMomentDialog();
            }
            else if (String.Compare(_lastCommand, "mc", ignoreCase: true) == 0)
            {
                ShowAxialForceMomentDialog();
            }
            else if (String.Compare(_lastCommand, "tmo", ignoreCase: true) == 0)
            {
                DisplayTriangleMeshOptions();
            }
            else if (String.Compare(_lastCommand, "makehole", ignoreCase: true) == 0)
            {
                MakeHole();
            }
            else if (String.Compare(_lastCommand, "rbp", ignoreCase: true) == 0)
            {
                ShowRebarPropertiesDialog();
            }
            else if (String.Compare(_lastCommand, "ptp", ignoreCase: true) == 0)
            {
                ShowPostTensioningPropertiesDialog();
            }
        }
        #endregion

        #region Selection Box
        private void BeginSelectionBox(PointD globalPoint)
        {
            _selectionBoxStart = globalPoint;

            _selectionBoxInitiated = true;
        }

        private void EndSelectionBox(PointD globalPoint)
        {
            _selectionBoxEnd = globalPoint;

            double x0 = Min(_selectionBoxStart.X, _selectionBoxEnd.X);
            double y0 = Min(_selectionBoxStart.Y, _selectionBoxEnd.Y);

            double width = Max(_selectionBoxStart.X, _selectionBoxEnd.X) - x0;
            double height = Max(_selectionBoxStart.Y, _selectionBoxEnd.Y) - y0;
            
            SelectionBoxDirection dir = SelectionBoxDirection.Right;

            if (globalPoint.X < _selectionBoxStart.X)
            {
                dir = SelectionBoxDirection.Left;
            }

            Rect recForQuadTree = new Rect(x0 - width * 0.01, y0 - height * 0.01, width * 1.02, height * 1.02);

            Rect recForContainment = new Rect(x0, y0, width, height);

            List<LineSegment> rectEdges = recForContainment.Edges();

            List<IDrawingElement> nodesInsideAll = InstanceData.DrawingElementQuadTree.GetNodesInside(recForQuadTree).ToList();

            List<IDrawingElement> nodesInside = new List<IDrawingElement>(nodesInsideAll.Count);

            for (int i = 0; i < nodesInsideAll.Count; i++)
            {
                IDrawingElement elem = nodesInsideAll[i];

                if (elem.Layer.Visible == true)
                {
                    nodesInside.Add(elem);
                }
            }

            for (int i = 0; i < nodesInside.Count; i++)
            {
                IDrawingElement elem = nodesInside[i];
                if (dir.Equals(SelectionBoxDirection.Right) == true)
                {
                    if (elem.IsContainedWithin(recForContainment) == false)
                    {
                        continue;
                    }
                }
                else
                {
                    if (elem.IsContainedWithin(recForContainment) == false)
                    {
                        bool crosses = false;

                        //check if the selection box crosses the element
                        for (int j = 0; j < rectEdges.Count; j++)
                        {
                            if (elem.IntersectsWithLine(rectEdges[j], out _) == true)
                            {
                                crosses = true;
                                break;
                            }
                        }
                        
                        if (crosses == false)
                        {
                            continue;
                        }
                    }
                }

                if (_currentKeysDown.Shift == false)
                {
                    if (elem.Selected == false)
                    {
                        elem.Selected = true;
                        SelectedElements.Add(elem);
                    }
                }
                else
                {
                    if (elem.Selected == true)
                    {
                        elem.Selected = false;

                        try
                        {
                            SelectedElements.Remove(elem);
                        }
                        catch
                        {
                            Console.WriteLine("Element not found in selected elements list.");
                        }
                    }
                }
            }

            _selectionBoxInitiated = false;

            CreateDraftingGraphics();

            SetPropertyPanelAccordianVisibility();
        }

        private void DrawSelectionBox(PointD globalPoint)
        {
            if (_selectionBoxInitiated == true)
            {
                PointD[] cornerPointsGlobal = new PointD[5];

                PointD p0 = _selectionBoxStart;

                PointD p4 = globalPoint;

                cornerPointsGlobal[0] = p0;

                cornerPointsGlobal[1] = new PointD(p0.X, p4.Y);

                cornerPointsGlobal[2] = p4;

                cornerPointsGlobal[3] = new PointD(p4.X, p0.Y);

                cornerPointsGlobal[4] = p0;

                SelectionBoxDirection dir = SelectionBoxDirection.Right;

                if (globalPoint.X < _selectionBoxStart.X)
                {
                    dir = SelectionBoxDirection.Left;
                }

                RenderSelectionBox(cornerPointsGlobal, dir);
            }            
        }

        private void RenderSelectionBox(PointD[] cornerPoints, SelectionBoxDirection dir)
        {
            if (_useGDI == true)
            {
                Point[] cornerPointsPixel = new Point[cornerPoints.Length];

                for (int i = 0; i <= cornerPoints.GetUpperBound(0); i++)
                {
                    cornerPointsPixel[i] = ConvertToPixelCoord(cornerPoints[i], InstanceData.ZeroPoint, InstanceData.DrawingScale);
                }

                RenderSelectionBoxWithGDI(cornerPointsPixel, dir);
            }
            else if (_renderWithOpenGL == true)
            {
                RenderSelectionBoxWithOpenGL(cornerPoints, dir);
            }
        }

        private void RenderSelectionBoxWithGDI(Point[] cornerPoints, SelectionBoxDirection dir)
        {
            Graphics g = Graphics.FromImage(DraftingBitmap);

            g.Clear(Color.White);

            CreateDraftingGraphics();

            Pen myPen = new Pen(Color.Gray);

            myPen.Width = 1;

            if (dir.Equals(SelectionBoxDirection.Left) == true)
            {
                myPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            }

            g.DrawPolygon(myPen, cornerPoints);

            myPen.Dispose();

            g.Dispose();

            DraftingPictureBox.Invalidate();
        }
      
        private void ClearSelectedElements()
        {
            if (SelectedElements.Count >= 1)
            {
                foreach (IDrawingElement elem in SelectedElements)
                {
                    elem.Selected = false;
                }

                SelectedElements.Clear();

                _singleClickSelectionPossibilities.Clear();

                _singleClickSelectionIndex = 0;

                CreateDraftingGraphics();

                SetPropertyPanelAccordianVisibility();
            }  
        }
        #endregion

        #region Single Click Selection
        private void SingleClickSelection(PointD globalPoint)
        {
            Rect boundingRect = GetBufferedBoundingRectangle(globalPoint, InstanceData.SelectionDistance);

            List<IDrawingElement> nearbyElementsAll = InstanceData.DrawingElementQuadTree.GetNodesInside(boundingRect).ToList();

            List<IDrawingElement> nearbyElements = new List<IDrawingElement>(nearbyElementsAll.Count);

            for (int i = 0; i < nearbyElementsAll.Count; i++)
            {
                IDrawingElement elem = nearbyElementsAll[i];

                if (elem.Layer.Visible == true)
                {
                    nearbyElements.Add(elem);
                }
            }

            List<Tuple<IDrawingElement, double>> elementsAndDistances = new List<Tuple<IDrawingElement, double>>();

            for (int i = 0; i < nearbyElements.Count; i++)
            {
                if (nearbyElements[i].PointLiesOnElementBoundary(globalPoint, InstanceData.SelectionDistance, out double distToElement) == true)
                {
                    elementsAndDistances.Add(new Tuple<IDrawingElement, double>(nearbyElements[i], distToElement));
                }
            }

            if (elementsAndDistances.Count > 0)
            {
                _singleClickSelectionPossibilities.Clear();

                _singleClickSelectionIndex = 0;

                double minDist = elementsAndDistances.Min(x => x.Item2);

                for (int i = 0; i < elementsAndDistances.Count; i++)
                {
                    if (Abs(elementsAndDistances[i].Item2 - minDist) <= InstanceData.SelectionToleranceForCoincidentElements)
                    {
                        _singleClickSelectionPossibilities.Add(elementsAndDistances[i].Item1);
                    }
                }

                if (_currentKeysDown.Shift == true)
                {
                    for (int i = 0; i < _singleClickSelectionPossibilities.Count; i++)
                    {
                        if (SelectedElements.Contains(_singleClickSelectionPossibilities[i]))
                        {
                            _singleClickSelectionPossibilities[i].Selected = false;

                            SelectedElements.Remove(_singleClickSelectionPossibilities[i]);

                            _singleClickSelectionPossibilities.Clear();

                            break;
                        }
                    }
                }
                else
                {
                    if (SelectedElements.Contains(_singleClickSelectionPossibilities[0]) == false)
                    {
                        _singleClickSelectionPossibilities[0].Selected = true;

                        SelectedElements.Add(_singleClickSelectionPossibilities[0]);
                    }
                }

                CreateDraftingGraphics();
            }

            _selectionBoxInitiated = false;

            SetPropertyPanelAccordianVisibility();
        }

        private void CycleNextSelectionPossibility()
        {
            if (SelectedElements.Count > 0)
            {
                if (_singleClickSelectionPossibilities.Count > 1)
                {
                    bool otherVisibleInList = false;

                    for (int i = 0; i < _singleClickSelectionPossibilities.Count; i++)
                    {
                        if (i != _singleClickSelectionIndex)
                        {
                            if (_singleClickSelectionPossibilities[i].Layer.Visible == true)
                            {
                                otherVisibleInList = true;
                                break;
                            }
                        }
                    }

                    if (otherVisibleInList == false)
                    {
                        return;
                    }

                    int originalIndex = _singleClickSelectionIndex;

                    IDrawingElement currentSelection = _singleClickSelectionPossibilities[_singleClickSelectionIndex];

                    currentSelection.Selected = false;

                    SelectedElements.Remove(currentSelection);
                    
                    while (true)
                    {
                        if (_singleClickSelectionIndex == _singleClickSelectionPossibilities.Count - 1)
                        {
                            _singleClickSelectionIndex = 0;
                        }
                        else
                        {
                            _singleClickSelectionIndex++;
                        }

                        if (_singleClickSelectionPossibilities[_singleClickSelectionIndex].Layer.Visible == true)
                        {
                            break;
                        }
                    }                

                    IDrawingElement newSelection = _singleClickSelectionPossibilities[_singleClickSelectionIndex];

                    newSelection.Selected = true;

                    SelectedElements.Add(newSelection);

                    CreateDraftingGraphics();

                    SetPropertyPanelAccordianVisibility();
                }
            }
        }
        #endregion

        #region Reshape Elements
        private void SetDrawingStateToReshape()
        {
            _currentDrawingType = DrawingType.Reshape;

            _selectionBoxInitiated = false;

            this.Cursor = Cursors.Cross;
        }

        private bool BeginReshapeElements(PointD globalPoint, out List<IDrawingElement> elementsToReshape, out List<DrawingNode> nodesToReshape)
        {
            elementsToReshape = new List<IDrawingElement>();

            nodesToReshape = new List<DrawingNode>();

            List<Tuple<DrawingNode, IDrawingElement>> possibleNodesAndElements = new List<Tuple<DrawingNode, IDrawingElement>>();

            if(SelectedElements != null && SelectedElements.Count > 0)
            {
                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    IDrawingElement elem = SelectedElements[i];

                    for (int j = 0; j < elem.Nodes.Count; j++)
                    {
                        if (globalPoint.DistanceTo(elem.Nodes[j].Point) <= InstanceData.SelectionDistance)
                        {
                            possibleNodesAndElements.Add(new Tuple<DrawingNode, IDrawingElement>(elem.Nodes[j], elem));
                        }
                    }
                }
            }

            possibleNodesAndElements = possibleNodesAndElements.OrderBy(x => x.Item1.Point.DistanceTo(globalPoint)).ToList();

            for (int i = 0; i < possibleNodesAndElements.Count; i++)
            {
                if (i == 0)
                {
                    nodesToReshape.Add(possibleNodesAndElements[i].Item1);
                    elementsToReshape.Add(possibleNodesAndElements[i].Item2);
                }
                else
                {
                    if (possibleNodesAndElements[i].Item1.Point.DistanceTo(globalPoint) == possibleNodesAndElements[0].Item1.Point.DistanceTo(globalPoint))
                    {
                        if (elementsToReshape.Contains(possibleNodesAndElements[i].Item2) == false)
                        {
                            elementsToReshape.Add(possibleNodesAndElements[i].Item2);

                            if (nodesToReshape.Contains(possibleNodesAndElements[i].Item1) == false)
                            {
                                nodesToReshape.Add(possibleNodesAndElements[i].Item1);
                            }
                        }
                    }
                }
            }

            if (elementsToReshape.Count > 0)
            {
                _orthoBasePoint = nodesToReshape[0].Point;

                return true;
            }
            else
            {
                return false;
            }
        }

        private void BuildTempReshapedElements(PointD globalPoint, List<IDrawingElement> elementsToReshape, List<DrawingNode> nodesToReshape)
        {
            _tempNodesToReshape.Clear();
            _tempMovedObjects.Clear();

            for (int i = 0; i < elementsToReshape.Count; i++)
            {
                IDrawingElement elemCopy = elementsToReshape[i].CreateCopy(globalPoint, globalPoint);
                elemCopy.Selected = false;

                for (int j = 0; j < elemCopy.Nodes.Count; j++)
                {
                    if (elemCopy.Nodes[j].Point == nodesToReshape[0].Point)
                    {
                        _tempNodesToReshape.Add(elemCopy.Nodes[j]);

                        break; //Only allow one node from a particular element to be reshaped at a time
                    }
                }

                _tempMovedObjects.Add(elemCopy);
            }

            _drawingInProgress = true;
        }

        private void ReshapeTempElements(PointD globalPoint)
        {
            if (_tempNodesToReshape != null && _tempNodesToReshape.Count > 0)
            {
                for (int i = 0; i < _tempNodesToReshape.Count; i++)
                {
                    _tempNodesToReshape[i].Point.X = globalPoint.X;
                    _tempNodesToReshape[i].Point.Y = globalPoint.Y;
                }

                for (int i = 0; i < _tempMovedObjects.Count; i++)
                {
                    _tempMovedObjects[i].ActionAfterReshape();
                }
            }
        }

        private void ReshapeOriginalElements(PointD globalPoint)
        {
            if (_origNodesToReshape!= null && _origNodesToReshape.Count > 0)
            {
                ActionOnElements action = new ActionOnElements();

                //Even though there may be multiple original nodes, they should all
                //share the same coordinate, so we will look for the point that is first
                //in the list
                PointD pointOfInterest = new PointD(_origNodesToReshape[0].Point.X, _origNodesToReshape[0].Point.Y);

                for (int i = 0; i < _origElementsToReshape.Count; i++)
                {
                    //Create a copy of the element for validation
                    IDrawingElement elemCopy = _origElementsToReshape[i].CreateCopy(new PointD(0, 0), new PointD(0, 0));

                    DrawingNode nodeToReshape = null;

                    //Find and move the point

                    for (int k = 0; k < elemCopy.Nodes.Count; k++)
                    {
                        if (elemCopy.Nodes[k].Point == pointOfInterest)
                        {
                            elemCopy.Nodes[k].Point.X = globalPoint.X;
                            elemCopy.Nodes[k].Point.Y = globalPoint.Y;

                            elemCopy.ActionAfterReshape();

                            nodeToReshape = _origElementsToReshape[i].Nodes[k];

                            break;
                        }
                    }
                    
                    //Now validate the element
                    if(elemCopy.ValidateElement() == false)
                    {
                        //We do nothing to the original element
                    }
                    else
                    {
                        if (nodeToReshape != null)
                        {
                            PointD origPoint = new PointD(nodeToReshape.Point.X, nodeToReshape.Point.Y);

                            nodeToReshape.Point.X = globalPoint.X;
                            nodeToReshape.Point.Y = globalPoint.Y;

                            _origElementsToReshape[i].ActionAfterReshape();

                            ReshapeAction rA = new ReshapeAction(_origNodesToReshape[i], origPoint, globalPoint);

                            action.AddAction(rA, new List<IDrawingElement>(1) { _origElementsToReshape[i] });

                            InstanceData.DrawingElementQuadTree.Remove(_origElementsToReshape[i]);

                            InstanceData.DrawingElementQuadTree.Insert(_origElementsToReshape[i], _origElementsToReshape[i].BoundingRectangle());
                        }
                    }
                }

                ResetReshapeOnly();

                if (action.Actions.Count > 0)
                {
                    AddActionToList(action);
                }

                this.Cursor = Cursors.Default;

                CreateDraftingGraphics();

                UpdateAllPropertyPanels();
            }
        }

        private void ResetReshapeOnly()
        {
            _origNodesToReshape.Clear();
            _origElementsToReshape.Clear();
            _tempNodesToReshape.Clear();
            _tempMovedObjects.Clear();

            _drawingInProgress = false;

            _currentDrawingType = DrawingType.None;

            this.Cursor = Cursors.Default;

            _orthoBasePoint = null;
        }
        #endregion

        #region Element Drawing Methods

        #region Polyline Drawing Methods
        private void SetDrawingStateToPolyline()
        {
            _currentDrawingType = DrawingType.Polyline;

            InputTextBox.Text = "Enter first point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "pl";
        }

        private void BeginDrawingPolylineViaMouseClick(PointD globalPoint)
        {
            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            PolylineG tempLine = new PolylineG();

            tempLine.Layer = InstanceData.CurrentDrawingLayer;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.EndPoint };

            tempLine.Nodes.Add(newNode);

            _tempDrawingElements.Add(tempLine);

            _drawingInProgress = true;

            InputTextBox.Text = "Enter next point:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ContinueDrawingPolylineViaMouseClick(PointD globalPoint)
        {
            if (_tempPoints.Count >= 1)
            {
                if (_tempPoints[_tempPoints.Count-1] == globalPoint)
                {
                    //Don't do anything if the newest point is the same as the previous point
                    return;
                }
            }

            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.EndPoint };

            _tempDrawingElements[0].Nodes.Add(newNode);

            CreateDraftingGraphicsWithTempElements(globalPoint);
        }

        private void ReturnKeyPressedWhileDrawingPolyline()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (double.TryParse(lineSplit[0], out double x) == true)
                {
                    if (double.TryParse(lineSplit[1], out double y) == true)
                    {
                        PointD p = new PointD(x, y);

                        _tempPoints.Add(p);

                        _orthoBasePoint = p;

                        DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                        PolylineG newPolyline = new PolylineG() { Layer = InstanceData.CurrentDrawingLayer };

                        newPolyline.Nodes.Add(newNode);

                        _tempDrawingElements.Add(newPolyline);

                        InputTextBox.Text = "Enter next point:";

                        _readOnlyChars = InputTextBox.Text.Length;

                        _drawingInProgress = true;
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _tempPoints.Add(p);

                            _orthoBasePoint = p;

                            DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                            _tempDrawingElements[0].Nodes.Add(newNode);

                            InputTextBox.Text = "Enter next point:";

                            _readOnlyChars = InputTextBox.Text.Length;

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD prevPoint = _tempPoints[_tempPoints.Count - 1];

                                    PointD p = new PointD(prevPoint.X + xRel, prevPoint.Y + yRel);

                                    DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                                    _tempPoints.Add(p);

                                    _orthoBasePoint = p;

                                    _tempDrawingElements[0].Nodes.Add(newNode);

                                    InputTextBox.Text = "Enter next point:";

                                    _readOnlyChars = InputTextBox.Text.Length;

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (_tempPoints.Count >= 1)
                    {
                        List<DrawingNode> nodes = new List<DrawingNode>();

                        foreach (PointD p in _tempPoints)
                        {
                            DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                            nodes.Add(newNode);
                        }

                        if (nodes[0].Point == nodes[nodes.Count - 1].Point)
                        {
                            //the first point is the same as the last point, hence a polygon
                            nodes[nodes.Count - 1] = nodes[0];

                            PolygonG pg = new PolygonG()
                            {
                                Layer = InstanceData.CurrentDrawingLayer,
                                Nodes = nodes
                            };

                            AddDrawingElement(pg);
                        }
                        else
                        {
                            //polyline
                            PolylineG pl = new PolylineG()
                            {
                                Layer = InstanceData.CurrentDrawingLayer,
                                Nodes = nodes
                            };

                            AddDrawingElement(pl);
                        }
                        
                        ResetDrawingState();

                        CreateDraftingGraphics();
                    }
                }
            } 
        }
        #endregion

        #region Line Drawing Methods
        private void SetDrawingStateToLine()
        {
            _currentDrawingType = DrawingType.Line;

            InputTextBox.Text = "Enter first point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "l";
        }

        private void BeginDrawingLineViaMouseClick(PointD globalPoint)
        {
            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            PolylineG tempLine = new PolylineG();

            tempLine.Layer = InstanceData.CurrentDrawingLayer;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.EndPoint };

            tempLine.Nodes.Add(newNode);

            _tempDrawingElements.Add(tempLine);

            _drawingInProgress = true;

            InputTextBox.Text = "Enter Next Point:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void EndDrawingLineViaMouseClick(PointD globalPoint)
        {
            if (_tempPoints.Count >= 1)
            {
                if (_tempPoints[_tempPoints.Count - 1] == globalPoint)
                {
                    //Don't do anything if the newest point is the same as the previous point
                    return;
                }
            }

            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.EndPoint };

            _tempDrawingElements[0].Nodes.Add(newNode);

            AddDrawingElement(_tempDrawingElements[0]);

            ResetDrawingState();

            CreateDraftingGraphics();

        }

        private void ReturnKeyPressedWhileDrawingLine()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (double.TryParse(lineSplit[0], out double x) == true)
                {
                    if (double.TryParse(lineSplit[1], out double y) == true)
                    {
                        PointD p = new PointD(x, y);

                        _tempPoints.Add(p);

                        _orthoBasePoint = p;

                        DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                        PolylineG newPolyline = new PolylineG() { Layer = InstanceData.CurrentDrawingLayer };

                        newPolyline.Nodes.Add(newNode);

                        _tempDrawingElements.Add(newPolyline);

                        InputTextBox.Text = "Enter next point:";

                        _readOnlyChars = InputTextBox.Text.Length;

                        _drawingInProgress = true;
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _tempPoints.Add(p);

                            _orthoBasePoint = p;

                            DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                            _tempDrawingElements[0].Nodes.Add(newNode);

                            AddDrawingElement(_tempDrawingElements[0]);

                            CreateDraftingGraphics();

                            ResetDrawingState();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD prevPoint = _tempPoints[_tempPoints.Count - 1];

                                    PointD p = new PointD(prevPoint.X + xRel, prevPoint.Y + yRel);

                                    DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                                    _tempPoints.Add(p);

                                    _orthoBasePoint = p;

                                    _tempDrawingElements[0].Nodes.Add(newNode);

                                    AddDrawingElement(_tempDrawingElements[0]);

                                    CreateDraftingGraphics();

                                    ResetDrawingState();
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Circle Drawing Methods
        private void SetDrawingStateToCircle()
        {
            _currentDrawingType = DrawingType.Circle;

            InputTextBox.Text = "Enter center point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "c";
        }

        private void BeginDrawingCircleViaMouseClick(PointD globalPoint)
        {
            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            CircleG tempCircle = new CircleG();

            tempCircle.Layer = InstanceData.CurrentDrawingLayer;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.CenterPoint };

            tempCircle.Nodes.Add(newNode);

            _tempDrawingElements.Add(tempCircle);

            _drawingInProgress = true;

            InputTextBox.Text = "Enter radius:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileDrawingCircle()
        {
            if (_drawingInProgress == false)
            {
                string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _tempPoints.Add(p);

                            DrawingNode newNode = new DrawingNode(p, NodeType.CenterPoint);

                            _tempDrawingElements.Add(new CircleG() { Layer = InstanceData.CurrentDrawingLayer, Nodes = new List<DrawingNode>() { newNode } });

                            InputTextBox.Text = "Enter radius:";

                            _readOnlyChars = InputTextBox.Text.Length;

                            _drawingInProgress = true;
                        }
                    }
                }
            }
            else
            {
                string inputString = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

                if (double.TryParse(inputString, out double r) == true)
                {
                    ((CircleG)_tempDrawingElements[0]).Radius = r;

                    AddDrawingElement(_tempDrawingElements[0]);

                    ResetDrawingState();

                    CreateDraftingGraphics();
                }
            }
        }

        private void EndDrawingCircleViaMouseClick(PointD globalPoint)
        {
            CircleG newCircle = ((CircleG)_tempDrawingElements[0]);
            PointD center = newCircle.CenterPoint().Point;

            double r = globalPoint.DistanceTo(center);

            newCircle.Radius = r;

            AddDrawingElement(newCircle);

            ResetDrawingState();

            CreateDraftingGraphics();
        }
        #endregion

        #region Rebar Section Drawing Methods
        private void SetDrawingStateToRebarSection()
        {
            _currentDrawingType = DrawingType.RebarSection;

            string promptText = "";

            if (_selectedRebarSize == null)
            {
                promptText = "Enter rebar size:";
            }
            else
            {
                if (InstanceData.AvailableRebarSizes.TryGetValue(_selectedRebarSize.Name, out RebarDefinition barSize) == true)
                {
                    promptText = "Enter rebar size (" + _selectedRebarSize.Name + "):";
                }
                else
                {
                    promptText = "Enter rebar size:";
                }
            }

            InputTextBox.Text = promptText;

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "rbs";
        }

        private void EndDrawingRebarSectionViaMouseClick(PointD globalPoint)
        {
            if (_selectedRebarSize != null)
            {
                RebarSectionG newRebarSection = new RebarSectionG(_selectedRebarSize);
                newRebarSection.Nodes.Add(new DrawingNode(globalPoint, NodeType.CenterPoint));
                newRebarSection.Layer = InstanceData.CurrentDrawingLayer;
                AddDrawingElement(newRebarSection);
            }
        }

        private void ReturnKeyPressedWhileDrawingRebarSection()
        {
            if (_drawingInProgress == false)
            {
                string inputRebarSize = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

                if (String.Compare(inputRebarSize, "", ignoreCase: true) == 0)
                {
                    if (_selectedRebarSize != null)
                    {
                        if (InstanceData.AvailableRebarSizes.TryGetValue(_selectedRebarSize.Name, out RebarDefinition prevRebarSize) == true)
                        {
                            _tempDrawingElements.Add(new RebarSectionG(_selectedRebarSize) { Layer = InstanceData.CurrentDrawingLayer });

                            InputTextBox.Text = "Enter center point:";

                            _readOnlyChars = InputTextBox.Text.Length;

                            _drawingInProgress = true;
                        }
                        else
                        {
                            InputTextBox.Text = "Invalid rebar size, enter valid rebar size:";

                            _readOnlyChars = InputTextBox.Text.Length;
                        }
                    }
                    else
                    {
                        InputTextBox.Text = "Invalid rebar size, enter valid rebar size:";

                        _readOnlyChars = InputTextBox.Text.Length;
                    }
                }
                else if (InstanceData.AvailableRebarSizes.TryGetValue(inputRebarSize, out RebarDefinition rebarSize) == true)
                {
                    _selectedRebarSize = rebarSize;

                    _tempDrawingElements.Add(new RebarSectionG(_selectedRebarSize) { Layer = InstanceData.CurrentDrawingLayer });

                    InputTextBox.Text = "Enter center point:";

                    _readOnlyChars = InputTextBox.Text.Length;

                    _drawingInProgress = true;
                }
                else
                {
                    InputTextBox.Text = "Invalid rebar size, enter valid rebar size:";

                    _readOnlyChars = InputTextBox.Text.Length;
                }
            }
            else
            {
                string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);
                            
                            DrawingNode newNode = new DrawingNode(p, NodeType.CenterPoint);

                            RebarSectionG newRebarSection = new RebarSectionG(_selectedRebarSize);
                            newRebarSection.Nodes.Add(newNode);
                            newRebarSection.Layer = InstanceData.CurrentDrawingLayer;

                            AddDrawingElement(newRebarSection);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }
        #endregion

        #region Post Tensioning Section Drawing Methods
        private void SetDrawingStateToPostTensioningSection()
        {
            _lastCommand = "pts";

            PostTensioningSelectForm frm = new PostTensioningSelectForm(InstanceData.PostTensioningDefinitionList);

            frm.ShowDialog();

            if (frm.Cancel == true)
            {
                ResetDrawingState();
            }
            else
            {
                _selectedPostTensioningDefinition = PostTensioningMethods.GetPostTensioningDefinitionFromName(frm.SelectedName, InstanceData.PostTensioningDefinitionList);

                if (_selectedPostTensioningDefinition == null)
                {
                    MessageBox.Show("No post tensioning type was selected","Post Tensioning", MessageBoxButtons.OK);
                    ResetDrawingState();
                }
                else
                {
                    InputTextBox.Text = "Enter center point:";

                    _readOnlyChars = InputTextBox.Text.Length;

                    this.Cursor = Cursors.Cross;

                    _currentDrawingType = DrawingType.PostTensioning;

                    _drawingInProgress = true;

                    _tempDrawingElements.Add(new PostTensioningG(null, _selectedPostTensioningDefinition, InstanceData.CurrentDrawingLayer, 0, JackingAndGroutingType.JackedGroutedReleased, 0));
                }
            }

            frm.Dispose();
        }

        private void EndDrawingPostTensioningSectionViaMouseClick(PointD globalPoint)
        {
            if (_selectedPostTensioningDefinition != null)
            {
                PostTensioningG newPostTensioningSection = new PostTensioningG(globalPoint, _selectedPostTensioningDefinition, InstanceData.CurrentDrawingLayer, 0,JackingAndGroutingType.JackedGroutedReleased, 0);
                AddDrawingElement(newPostTensioningSection);
            }
        }

        private void ReturnKeyPressedWhileDrawingPostTensioningSection()
        {
            if (_drawingInProgress == true)
            {
                string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            PostTensioningG newPostTensioningSection = new PostTensioningG(p, _selectedPostTensioningDefinition, InstanceData.CurrentDrawingLayer, 0, JackingAndGroutingType.JackedGroutedReleased, 0);

                            AddDrawingElement(newPostTensioningSection);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }
        #endregion

        #region Point Drawing Methods
        private void SetDrawingStateToPoint()
        {
            _currentDrawingType = DrawingType.Point;

            InputTextBox.Text = "Enter point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "p";
        }

        private void ReturnKeyPressedWhileDrawingPoint()
        {
            if (_drawingInProgress == false)
            {
                string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _tempPoints.Add(p);

                            DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                            _tempDrawingElements.Add(new PointG() { Layer = InstanceData.CurrentDrawingLayer, Nodes = new List<DrawingNode>() { newNode } });

                            AddDrawingElement(_tempDrawingElements[0]);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }

        private void DrawPointViaMouseClick(PointD globalPoint)
        {
            DrawingNode newNode = new DrawingNode(globalPoint, NodeType.EndPoint);

            PointG newPoint = new PointG() { Layer = InstanceData.CurrentDrawingLayer, Nodes = new List<DrawingNode>() { newNode } };

            AddDrawingElement(newPoint);

            CreateDraftingGraphics();
        }
        #endregion

        #region Rectangle Drawing Methods
        private void SetDrawingStateToRectangle()
        {
            _currentDrawingType = DrawingType.Rectangle;

            InputTextBox.Text = "Enter first corner point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "rec";
        }

        private void BeginDrawingRectangleViaMouseClick(PointD globalPoint)
        {
            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            RectangleG tempRect = new RectangleG();

            tempRect.Layer = InstanceData.CurrentDrawingLayer;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.EndPoint };

            tempRect.Nodes.Add(newNode);

            _tempDrawingElements.Add(tempRect);

            _drawingInProgress = true;

            InputTextBox.Text = "Enter opposite corner point:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileDrawingRectangle()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _tempPoints.Add(p);

                            DrawingNode newNode = new DrawingNode(p, NodeType.EndPoint);

                            _tempDrawingElements.Add(new RectangleG() { Layer = InstanceData.CurrentDrawingLayer, Nodes = new List<DrawingNode>() { newNode } });

                            InputTextBox.Text = "Enter opposite corner point:";

                            _readOnlyChars = InputTextBox.Text.Length;

                            _drawingInProgress = true;
                        }
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            RectangleG newRect = RectangleG.CreateRectangleFromCornerPoints(_tempPoints[0], p, InstanceData.CurrentDrawingLayer);

                            AddDrawingElement(newRect);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD prevPoint = _tempPoints[_tempPoints.Count - 1];

                                    PointD p = new PointD(prevPoint.X + xRel, prevPoint.Y + yRel);

                                    RectangleG newRect = RectangleG.CreateRectangleFromCornerPoints(_tempPoints[0], p, InstanceData.CurrentDrawingLayer);

                                    AddDrawingElement(newRect);

                                    ResetDrawingState();

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void EndDrawingRectangleViaMouseClick(PointD globalPoint)
        {
            RectangleG newRect = RectangleG.CreateRectangleFromCornerPoints(_tempPoints[0], globalPoint, InstanceData.CurrentDrawingLayer);

            AddDrawingElement(newRect);

            ResetDrawingState();

            CreateDraftingGraphics();
        }
        #endregion

        #region Arc Drawing Methods
        private void SetDrawingStateToArc()
        {
            _currentDrawingType = DrawingType.Arc;

            InputTextBox.Text = "Enter start point:";

            _readOnlyChars = InputTextBox.Text.Length;

            this.Cursor = Cursors.Cross;

            _lastCommand = "arc";
        }

        private void BeginDrawingArcViaMouseClick(PointD globalPoint)
        {
            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            ArcG tempArc = new ArcG();

            tempArc.Layer = InstanceData.CurrentDrawingLayer;

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.StartPoint };

            tempArc.StartPoint = newNode;

            _tempDrawingElements.Add(tempArc);

            InputTextBox.Text = "Enter end point:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileDrawingArc()
        {
            if (_drawingInProgress == false)
            {
                string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            DrawingNode newNode = new DrawingNode(p, NodeType.StartPoint);

                            if (_tempPoints.Count == 0)
                            {
                                newNode.TypeOfNode = NodeType.StartPoint;

                                _tempDrawingElements.Add(new ArcG() { Layer = InstanceData.CurrentDrawingLayer, StartPoint = newNode });

                                InputTextBox.Text = "Enter end point:";
                            }
                            else if (_tempPoints.Count == 1)
                            {
                                newNode.TypeOfNode = NodeType.EndPoint;

                                ((ArcG)_tempDrawingElements[0]).EndPoint = newNode;

                                InputTextBox.Text = "Enter third point:";

                                _drawingInProgress = true;
                            }

                            _tempPoints.Add(p);

                            _orthoBasePoint = p;

                            _readOnlyChars = InputTextBox.Text.Length;
                        }
                    }
                }
            }
            else
            {
                string inputString = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

                string[] lineSplit = Regex.Split(inputString, ",");

                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            DrawingNode newNode = new DrawingNode(p, NodeType.ThirdPoint);

                            ((ArcG)_tempDrawingElements[0]).ThirdPoint = newNode;

                            _tempPoints.Add(p);

                            _orthoBasePoint = p;

                            AddDrawingElement(_tempDrawingElements[0]);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }

        private void ContinueDrawingArcViaMouseClick(PointD globalPoint)
        {
            if (_tempPoints.Count >= 1)
            {
                for (int i = 0; i < _tempPoints.Count; i++)
                {
                    if (_tempPoints[i] == globalPoint)
                    {
                        //Don't do anything if the newest point is the same as the previous point
                        return;
                    }
                }
            }

            DrawingNode newNode = new DrawingNode() { Point = globalPoint, TypeOfNode = NodeType.StartPoint };

            if (_tempPoints.Count == 1)
            {
                newNode.TypeOfNode = NodeType.EndPoint;

                ((ArcG)_tempDrawingElements[0]).EndPoint = newNode;

                InputTextBox.Text = "Enter third point:";

                _readOnlyChars = InputTextBox.Text.Length;

                _drawingInProgress = true;
            }
            else if (_tempPoints.Count == 2)
            {
                newNode.TypeOfNode = NodeType.ThirdPoint;

                ((ArcG)_tempDrawingElements[0]).ThirdPoint = newNode;
            }

            _tempPoints.Add(globalPoint);

            _orthoBasePoint = globalPoint;

            if (_tempPoints.Count == 3)
            {
                AddDrawingElement(_tempDrawingElements[0]);

                ResetDrawingState();

                CreateDraftingGraphics();
            }
        }
        #endregion

        private void ResetDrawingState()
        {
            _drawingInProgress = false;

            _currentDrawingType = DrawingType.None;

            _tempPoints.Clear();

            _tempDrawingElements.Clear();

            this.Cursor = Cursors.Arrow;

            InputTextBox.Clear();

            _readOnlyChars = 0;

            _previouslySnapped = _isSnapped;

            _isSnapped = false;

            _snapPoint = null;

            _tempMovedObjects.Clear();

            _arrayDistanceSet = false;

            _arrayBasePoint = null;

            _arrayDistBasePoint = null;

            _orthoBasePoint = null;

            _offsetDistanceSet = false;

            _meshSizeSet = false;

            SetPropertyPanelAccordianVisibility();
        }
        #endregion

        #region Add and Delete Elements
        public void AddDrawingElement(IDrawingElement elem)
        {
            InstanceData.DrawingElementList.Add(elem);

            InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());

            ActionOnElements action = new ActionOnElements();

            action.AddAction(new AddAction(), new List<IDrawingElement>(1) { elem });

            AddActionToList(action);
        }

        public void AddDrawingElement(List<IDrawingElement> elemList)
        {
            Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

            actions.Add(new AddAction(), new List<IDrawingElement>(elemList));

            foreach (IDrawingElement elem in elemList)
            {
                InstanceData.DrawingElementList.Add(elem);

                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
            }
            
            ActionOnElements action = new ActionOnElements(actions);

            AddActionToList(action);
        }

        private void DeleteSelectedElements()
        {
            if (SelectedElements.Count >= 1)
            {
                try
                {
                    Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

                    actions.Add(new DeleteAction(), new List<IDrawingElement>(SelectedElements));

                    InstanceData.DrawingElementList.RemoveRange(SelectedElements);

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        InstanceData.DrawingElementQuadTree.Remove(SelectedElements[i]);
                    }

                    ActionOnElements action = new ActionOnElements(actions);

                    AddActionToList(action);

                    ClearSelectedElements();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public void DeleteAllElements()
        {
            if (InstanceData.DrawingElementList != null)
            {
                int count = InstanceData.DrawingElementList.Count;

                while(count > 0)
                {
                    InstanceData.DrawingElementQuadTree.Remove(InstanceData.DrawingElementList[0]);

                    InstanceData.DrawingElementList.RemoveAt(0);

                    count--;
                }

                CreateDraftingGraphics();
            }
        }
        #endregion

        #region Undo and Redo

        private void AddActionToList(ActionOnElements action)
        {
            if (_previousActions.Count < _maxActionsToStore)
            {
                _previousActions.AddLast(action);
            }
            else
            {
                _previousActions.RemoveFirst();
                _previousActions.AddLast(action);
            }
        }

        private void UndoLastAction()
        {
            if (_previousActions.Count >= 1)
            {
                _previousActions.Last.Value.ReverseAction(InstanceData.DrawingElementList, InstanceData.DrawingElementQuadTree);

                if (UndoWarrantsCreatingGraphics(_previousActions.Last.Value) == true)
                {
                    CreateDraftingGraphics();
                }

                _previousActions.RemoveLast();
            }
        }

        private bool UndoWarrantsCreatingGraphics(ActionOnElements action)
        {
            List<DrawingAction> drawingActions = action.Actions.Keys.ToList();

            for (int i = 0; i < drawingActions.Count; i++)
            {
                DrawingAction drawingAction = drawingActions[i];

                if (drawingAction.GetType().Equals(typeof(CadPanel.UndoSupport.Actions.AddAction)) == true)
                {
                    return true;
                }
                else if (drawingAction.GetType().Equals(typeof(CadPanel.UndoSupport.Actions.DeleteAction)) == true)
                {
                    return true;
                }
                else if (drawingAction.GetType().Equals(typeof(CadPanel.UndoSupport.Actions.MoveAction)) == true)
                {
                    return true;
                }
                else if (drawingAction.GetType().Equals(typeof(CadPanel.UndoSupport.Actions.RotateAction)) == true)
                {
                    return true;
                }
                else if (drawingAction.GetType().Equals(typeof(CadPanel.UndoSupport.Actions.ReshapeAction)) == true)
                {
                    return true;
                }
            }
            
            return false;
        }
        #endregion

        #region Refresh Timer Events
        private void CheckTimer_Tick(object sender, EventArgs e)
        {
            if (_draftingControl.ContainsFocus == true)
            {
                if (MousePoint != PreviousMousePoint)
                {
                    PingMouseMoveInDraftingPictureBox();
                }
            }
        }
        #endregion

        #region Snapping Methods
        private void ShowSnapOptionsDialog(SnapProperties properties)
        {
            _lastCommand = "snap";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            SnappingOptionsForm frm = new SnappingOptionsForm(properties);

            frm.ShowDialog();

            frm.Dispose();

            InstanceData.PanelSnapProperties.SnapDistance = InstanceData.PanelSnapProperties.BaseSnapDistance / InstanceData.MagnificationLevel;
        }

        private PointD GetSnappedPoint(PointD globalPoint)
        {
            Rect boundingRect = GetBufferedBoundingRectangle(globalPoint, InstanceData.PanelSnapProperties.SnapDistance);

            List<IDrawingElement> nearbyElements = InstanceData.DrawingElementQuadTree.GetNodesInside(boundingRect).ToList();

            //Add temp elements to the nearby elements list
            //Only add temp elements that are polylines.  All other temp
            //elements should not be snapped to.
            nearbyElements.AddRange(_tempDrawingElements.Where(x=>x.GetType().Equals(typeof(PolylineG))==true).ToList());

            SnapDefinition closestSnapPoint = null;

            double closestDist = InstanceData.MaxDraftingBound;

            List<SnapDefinition> snapPoints = new List<SnapDefinition>();

            if (InstanceData.PanelSnapProperties.PointSnapsOn == true)
            {
                if (InstanceData.PanelSnapProperties.SnapEndPoints == true)
                {
                    snapPoints.Add(GetEndPointSnap(globalPoint, nearbyElements));
                }

                if (InstanceData.PanelSnapProperties.SnapMidPoints == true)
                {
                    snapPoints.Add(GetMidPointSnap(globalPoint, nearbyElements));
                }

                if (InstanceData.PanelSnapProperties.SnapCenter == true)
                {
                    snapPoints.Add(GetCenterPointSnap(globalPoint, nearbyElements));
                }

                if (InstanceData.PanelSnapProperties.SnapQuadrant == true)
                {
                    snapPoints.Add(GetQuadrantPointSnap(globalPoint, nearbyElements));
                }

                if (InstanceData.PanelSnapProperties.SnapIntersection == true)
                {
                    snapPoints.Add(GetIntersectionSnap(globalPoint, nearbyElements));
                }

                if (InstanceData.PanelSnapProperties.SnapPerpendicular == true)
                {
                    if (_orthoBasePoint != null)
                    {
                        snapPoints.Add(GetPerpendicularSnap(globalPoint, nearbyElements));
                    }
                }
            }
            
            if (InstanceData.PanelSnapProperties.SnapOrtho == true)
            {
                snapPoints.Add(GetOrthoSnap(globalPoint));
            }

            //First narrow down the list to all points within the snap distance
            List<SnapDefinition> withinSnapDistance = new List<SnapDefinition>();

            bool containsNonOrthoSnaps = false;

            for (int i = 0; i < snapPoints.Count; i++)
            {
                SnapDefinition p = snapPoints[i];

                if (p != null && p.SnapPoint != null)
                {
                    double distToGlobalPoint = p.SnapPoint.DistanceTo(globalPoint);

                    if (distToGlobalPoint <= InstanceData.PanelSnapProperties.SnapDistance)
                    {
                        withinSnapDistance.Add(p);

                        if (p.SnapType.Equals(SnapType.Ortho) == false)
                        {
                            containsNonOrthoSnaps = true;
                        }
                    }
                }
            }

            for (int i = 0; i < withinSnapDistance.Count; i++)
            {
                SnapDefinition p = withinSnapDistance[i];

                if (p != null)
                {
                    if (p.SnapPoint != null)
                    {
                        if (containsNonOrthoSnaps == true && p.SnapType.Equals(SnapType.Ortho) == true)
                        {
                            continue;
                        }

                        double distToGlobalPoint = p.SnapPoint.DistanceTo(globalPoint);

                        if (distToGlobalPoint < closestDist)
                        {
                            closestDist = distToGlobalPoint;

                            closestSnapPoint = p;
                        }
                    }
                }
            }

            if (closestSnapPoint != null)
            {
                if (closestSnapPoint.SnapPoint != null)
                {
                    if (closestSnapPoint.SnapType.Equals(SnapType.Ortho) == true)
                    {
                        if (InstanceData.PanelSnapProperties.PointSnapsOn == true)
                        {
                            if (InstanceData.PanelSnapProperties.SnapApparentOrthoIntersection == true)
                            {
                                SnapDefinition apparentOrthoSnap = GetApparentOrthoSnap(closestSnapPoint, globalPoint, nearbyElements);

                                if (apparentOrthoSnap != null && apparentOrthoSnap.SnapPoint != null)
                                {
                                    if (apparentOrthoSnap.SnapPoint.DistanceTo(globalPoint) <= InstanceData.PanelSnapProperties.SnapDistance)
                                    {
                                        closestSnapPoint = apparentOrthoSnap;
                                    }
                                }
                            }
                        }              
                    }

                    _snapPoint = closestSnapPoint;
                    _previouslySnapped = _isSnapped;
                    _isSnapped = true;
                    return closestSnapPoint.SnapPoint;
                }
            }

            _snapPoint = null;
            _previouslySnapped = _isSnapped;
            _isSnapped = false;
            return null;
        }

        private SnapDefinition GetEndPointSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            foreach (IDrawingElement elem in nearbyElements)
            {
                PointD elemClosestPoint = elem.ClosestNodalPointToPoint(globalPoint, NodeType.EndPoint);

                if (elemClosestPoint != null)
                {
                    if (elemClosestPoint.DistanceTo(globalPoint) < closestDist)
                    {
                        closestDist = elemClosestPoint.DistanceTo(globalPoint);

                        closestPoint = elemClosestPoint;
                    }
                }
            }

            return new SnapDefinition(closestPoint, SnapType.EndPoint);
        }

        private SnapDefinition GetMidPointSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            foreach (IDrawingElement elem in nearbyElements)
            {
                List<LineSegment> elemEdges = elem.Edges();

                foreach (LineSegment seg in elemEdges)
                {
                    PointD mp = seg.Midpoint();

                    if (mp.DistanceTo(globalPoint) < closestDist)
                    {
                        closestDist = mp.DistanceTo(globalPoint);

                        closestPoint = mp;
                    }
                }
            }

            return new SnapDefinition(closestPoint, SnapType.MidPoint);
        }

        private SnapDefinition GetCenterPointSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            foreach (IDrawingElement elem in nearbyElements)
            {
                List<DrawingNode> nodes = elem.Nodes;

                foreach (DrawingNode node in nodes)
                {
                    if (node.TypeOfNode.Equals(NodeType.CenterPoint) == true)
                    {
                        PointD cp = node.Point;

                        if (cp.DistanceTo(globalPoint) < closestDist)
                        {
                            closestDist = cp.DistanceTo(globalPoint);

                            closestPoint = cp;
                        }
                    }
                }
            }

            return new SnapDefinition(closestPoint, SnapType.CenterPoint);
        }

        private SnapDefinition GetQuadrantPointSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            foreach (IDrawingElement elem in nearbyElements)
            {
                List<PointD> quadPts = elem.QuadrantPoints();

                foreach (PointD p in quadPts)
                {
                    if (p.DistanceTo(globalPoint) < closestDist)
                    {
                        closestDist = p.DistanceTo(globalPoint);

                        closestPoint = p;
                    }
                }
            }

            return new SnapDefinition(closestPoint, SnapType.QuadrantPoint);
        }

        private SnapDefinition GetIntersectionSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            for (int i = 0; i < nearbyElements.Count - 1; i++)
            {
                IDrawingElement elem1 = nearbyElements[i];

                for (int j = i + 1; j < nearbyElements.Count; j++)
                {
                    IDrawingElement elem2 = nearbyElements[j];

                    if (elem1.IntersectsWithElement(elem2, out List<PointD> intersections) == true)
                    {
                        for (int k = 0; k < intersections.Count; k++)
                        {
                            double dist = intersections[k].DistanceTo(globalPoint);

                            if (dist < closestDist)
                            {
                                closestDist = dist;

                                closestPoint = intersections[k];
                            }
                        }
                    }
                }
            }
            
            return new SnapDefinition(closestPoint, SnapType.Intersection);
        }

        private SnapDefinition GetPerpendicularSnap(PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            for (int i = 0; i < nearbyElements.Count;i++)
            {
                IDrawingElement elem = nearbyElements[i];

                if (elem.HasPerpendicularIntersection(_orthoBasePoint, out List<PointD> perpIntersections) == true)
                {
                    for (int j = 0; j < perpIntersections.Count; j++)
                    {
                        PointD p = perpIntersections[j];

                        if (p.DistanceTo(globalPoint) < closestDist)
                        {
                            closestDist = p.DistanceTo(globalPoint);

                            closestPoint = p;
                        }
                    }
                }
            }

            return new SnapDefinition(closestPoint, SnapType.Perpendicular);
        }

        private SnapDefinition GetOrthoSnap(PointD globalPoint)
        {
            if (_orthoBasePoint != null)
            {
                double angleToGlobalPoint = globalPoint.GetAngleFromPoint(_orthoBasePoint); //angle in radians

                double angleToGlobalPointInDegrees = angleToGlobalPoint.ToDegrees(); //angle in degrees

                //Find the closest snap angle to the computed angle
                double smallestAngleDifference = 360.0; //degrees

                double closestSnapAngle = -1; //degrees

                for (double i = 0; i < 360; i+= InstanceData.PanelSnapProperties.OrthoSnapAngleInterval)
                {
                    double diff = Abs(angleToGlobalPointInDegrees - i);

                    if (diff < smallestAngleDifference)
                    {
                        smallestAngleDifference = diff;

                        closestSnapAngle = i;
                    }
                }

                if (smallestAngleDifference <= InstanceData.PanelSnapProperties.OrthoSnapAngleTolerance)
                {
                    LineSegment line = LineSegment.LineSegmentFromBasePointAndAngle(_orthoBasePoint, closestSnapAngle.ToRadians());

                    PointD snapPoint = line.GetClosestPerpendicularPointToInfiniteExtension(globalPoint);

                    return new SnapDefinition(snapPoint, SnapType.Ortho);
                }
            }

            return null;
        }

        private SnapDefinition GetApparentOrthoSnap(SnapDefinition orthoSnap, PointD globalPoint, List<IDrawingElement> nearbyElements)
        {
            LineSegment line = new LineSegment(new DrawingNode(_orthoBasePoint, NodeType.EndPoint), new DrawingNode(orthoSnap.SnapPoint, NodeType.EndPoint));

            double closestDist = InstanceData.MaxDraftingBound;

            PointD closestPoint = null;

            for (int i = 0; i < nearbyElements.Count; i++)
            {
                if (nearbyElements[i].IntersectsWithInfiniteLine(line, out List<PointD> intersections) == true)
                {
                    for (int k = 0; k < intersections.Count; k++)
                    {
                        double dist = intersections[k].DistanceTo(globalPoint);

                        if (dist < closestDist)
                        {
                            closestDist = dist;

                            closestPoint = intersections[k];
                        }
                    }
                }
            }

            if (closestPoint == null)
            {
                return null;
            }
            else
            {
                return new SnapDefinition(closestPoint, SnapType.ApparentOrthoIntersection);
            }
        }

        private Rect GetBufferedBoundingRectangle(PointD globalPoint, double bufferDistance)
        {
            double bufferDistMult = 1.0;

            int x0 = Convert.ToInt32(Floor(globalPoint.X - bufferDistMult * bufferDistance));
            int y0 = Convert.ToInt32(Floor(globalPoint.Y - bufferDistMult * bufferDistance));

            int width = Convert.ToInt32(Ceiling(globalPoint.X + bufferDistMult * bufferDistance) - x0);
            int height = Convert.ToInt32(Ceiling(globalPoint.Y + bufferDistMult * bufferDistance) - y0);

            return new Rect(x0, y0, width, height);
        }

        private void DrawSnap(ref Graphics g)
        {
            if (_isSnapped == true)
            {
                if (_snapPoint.SnapPoint != null)
                {
                    Pen myPen = new Pen(Color.Red);

                    Point pixelPoint = _snapPoint.SnapPoint.ConvertToPixelPoint(InstanceData.ZeroPoint, InstanceData.DrawingScale);

                    int w = 16;

                    int h = 16;

                    switch (_snapPoint.SnapType)
                    {
                        case SnapType.EndPoint:
                            g.DrawPolygon(myPen, Rectangle_SnapPoint(pixelPoint, w, h));
                            break;
                        case SnapType.CenterPoint:
                            g.DrawEllipse(myPen, Convert.ToInt32(pixelPoint.X - w / 2.0), 
                                Convert.ToInt32(pixelPoint.Y - h / 2.0),
                                w, h);
                            break;
                        case SnapType.QuadrantPoint:
                            g.DrawPolygon(myPen, Diamond_SnapPoint(pixelPoint, w, h));
                            break;
                        case SnapType.MidPoint:
                            g.DrawPolygon(myPen, Triangle_SnapPoint(pixelPoint, w, h));
                            break;
                        case SnapType.Perpendicular:
                            g.DrawPolygon(myPen, Perpendicular_SnapPoint(pixelPoint, w, h));
                            break;
                        case SnapType.Intersection:
                            Point[] pArr_i = X_SnapPoint(pixelPoint, w, h);
                            g.DrawLine(myPen, pArr_i[0], pArr_i[1]);
                            g.DrawLine(myPen, pArr_i[2], pArr_i[3]);
                            break;
                        case SnapType.ApparentOrthoIntersection:
                            Point[] pArr_aoi = X_SnapPoint(pixelPoint, w, h);
                            g.DrawLine(myPen, pArr_aoi[0], pArr_aoi[1]);
                            g.DrawLine(myPen, pArr_aoi[2], pArr_aoi[3]);

                            Point[] pArr_line = Ortho_SnapLine(_snapPoint.SnapPoint);
                            myPen.Color = Color.LightGray;
                            myPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                            g.DrawLine(myPen, pArr_line[0], pArr_line[1]);
                            break;
                        case SnapType.Ortho:
                            Point[] pArr_o = Ortho_SnapLine(_snapPoint.SnapPoint);
                            myPen.Color = Color.LightGray;
                            myPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                            g.DrawLine(myPen, pArr_o[0], pArr_o[1]);
                            break;
                        default:
                            break;
                    }

                    myPen.Dispose();
                }
            }
        }

        /// <summary>
        /// Draws an "X" snap symbol
        /// </summary>
        /// <param name="p"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        private Point[] X_SnapPoint(Point p, int w, int h)
        {
            Point[] pArr = new Point[4];

            Point p0 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y - h/2.0));
            Point p1 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y + h / 2.0));
            Point p2 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y - h / 2.0));
            Point p3 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y + h / 2.0));

            pArr[0] = p0;
            pArr[1] = p1;
            pArr[2] = p2;
            pArr[3] = p3;

            return pArr;
        }

        /// <summary>
        /// Draws a diamond snap symbol
        /// </summary>
        /// <param name="p"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        private Point[] Diamond_SnapPoint(Point p, int w, int h)
        {
            Point[] pArr = new Point[4];

            Point p0 = new Point(Convert.ToInt32(p.X), Convert.ToInt32(p.Y - h / 2.0));
            Point p1 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y));
            Point p2 = new Point(Convert.ToInt32(p.X), Convert.ToInt32(p.Y + h / 2.0));
            Point p3 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y));

            pArr[0] = p0;
            pArr[1] = p1;
            pArr[2] = p2;
            pArr[3] = p3;

            return pArr;
        }

        private Point[] Perpendicular_SnapPoint(Point p, int w, int h)
        {
            Point[] pArr = new Point[3];

            Point p0 = new Point(Convert.ToInt32(p.X), Convert.ToInt32(p.Y - h));
            Point p1 = new Point(Convert.ToInt32(p.X + w), Convert.ToInt32(p.Y - h));
            Point p2 = new Point(Convert.ToInt32(p.X + w), Convert.ToInt32(p.Y));
           
            pArr[0] = p0;
            pArr[1] = p1;
            pArr[2] = p2;

            return pArr;
        }

        private Point[] Triangle_SnapPoint(Point p, int w, int h)
        {
            Point[] pArr = new Point[3];

            Point p0 = new Point(Convert.ToInt32(p.X), Convert.ToInt32(p.Y - h / 2.0));
            Point p1 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y + h / 2.0));
            Point p2 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y + h / 2.0));
            
            pArr[0] = p0;
            pArr[1] = p1;
            pArr[2] = p2;

            return pArr;
        }

        private Point[] Rectangle_SnapPoint(Point p, int w, int h)
        {
            Point[] pArr = new Point[4];

            Point p0 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y - h / 2.0));
            Point p1 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y - h / 2.0));
            Point p2 = new Point(Convert.ToInt32(p.X + w / 2.0), Convert.ToInt32(p.Y + h / 2.0));
            Point p3 = new Point(Convert.ToInt32(p.X - w / 2.0), Convert.ToInt32(p.Y + h / 2.0));

            pArr[0] = p0;
            pArr[1] = p1;
            pArr[2] = p2;
            pArr[3] = p3;

            return pArr;
        }

        private Point[] Ortho_SnapLine(PointD p)
        {
            LineSegment line = GetOrthoSnapLine(p);

            Point p0 = line.StartNode.Point.ConvertToPixelPoint(InstanceData.ZeroPoint, InstanceData.DrawingScale);

            Point p1 = line.EndNode.Point.ConvertToPixelPoint(InstanceData.ZeroPoint, InstanceData.DrawingScale);

            Point[] arr = new Point[2];

            arr[0] = p0;

            arr[1] = p1;

            return arr;
        }

        private LineSegment GetOrthoSnapLine(PointD p)
        {
            Rect draftingWindow = GetDraftingWindowBounds();

            LineSegment line = new LineSegment(new DrawingNode(_orthoBasePoint, NodeType.EndPoint), new DrawingNode(p, NodeType.EndPoint));

            line.ExtendToFitWithinRectangle(draftingWindow);

            return line;
        }

        private Rect GetDraftingWindowBounds()
        {
            Point botLeft = new Point(0, DraftingPictureBox.Height);

            Point topRight = new Point(DraftingPictureBox.Width, 0);

            PointD globalBotLeft = botLeft.ConvertToGlobalCoord(InstanceData.ZeroPoint, InstanceData.DrawingScale);

            PointD globalTopRight = topRight.ConvertToGlobalCoord(InstanceData.ZeroPoint, InstanceData.DrawingScale);

            return new Rect(globalBotLeft.X, globalBotLeft.Y, globalTopRight.X - globalBotLeft.X, globalTopRight.Y - globalBotLeft.Y);
        }
        #endregion

        #region Moving
        private void SetDrawingStateToMove()
        {
            if (SelectedElements .Count >= 1)
            {
                _currentDrawingType = DrawingType.Move;

                InputTextBox.Text = "Enter move base point:";

                _readOnlyChars = InputTextBox.Text.Length;

                this.Cursor = Cursors.Cross;

                _lastCommand = "m";
            }
        }

        private void BeginMoveViaMouseClick(PointD globalPoint)
        {
            _moveBasePoint = globalPoint;

            _orthoBasePoint = globalPoint;

            _drawingInProgress = true;

            InputTextBox.Text = "Enter final point:";

            _readOnlyChars = InputTextBox.Text.Length;

            foreach (IDrawingElement elem in SelectedElements)
            {
                _tempMovedObjects.Add(elem.CreateCopy(globalPoint, globalPoint));
            }
        }

        private void ReturnKeyPressedWhileMoving()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (double.TryParse(lineSplit[0], out double x) == true)
                {
                    if (double.TryParse(lineSplit[1], out double y) == true)
                    {
                        PointD p = new PointD(x, y);

                        _moveBasePoint = p;

                        foreach (IDrawingElement elem in SelectedElements)
                        {
                            _tempMovedObjects.Add(elem.CreateCopy(_moveBasePoint, _moveBasePoint));
                        }

                        InputTextBox.Text = "Enter final point:";

                        _readOnlyChars = InputTextBox.Text.Length;

                        _drawingInProgress = true;
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            MoveObjectsToFinalLocation(p);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD p = new PointD(_moveBasePoint.X + xRel, _moveBasePoint.Y + yRel);

                                    MoveObjectsToFinalLocation(p);

                                    ResetDrawingState();

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void MoveTempMovedObjects(PointD globalPoint)
        {
            _tempMovedObjects.Clear();

            foreach (IDrawingElement elem in SelectedElements)
            {
                _tempMovedObjects.Add(elem.CreateCopy(_moveBasePoint, globalPoint));
            }
        }

        private void MoveObjectsToFinalLocation(PointD globalPoint)
        {
            foreach (IDrawingElement elem in SelectedElements)
            {
                InstanceData.DrawingElementQuadTree.Remove(elem);

                elem.Move(_moveBasePoint, globalPoint);

                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
            }

            UndoSupport.Actions.MoveAction mA = new UndoSupport.Actions.MoveAction()
            {
                BasePoint = _moveBasePoint,
                FinalPoint = globalPoint,
            };

            Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

            actions.Add(mA, new List<IDrawingElement>(SelectedElements));

            AddActionToList(new ActionOnElements(actions));
        }

        private void EndMoveViaMouseClick(PointD globalPoint)
        {
            MoveObjectsToFinalLocation(globalPoint);

            ResetDrawingState();

            CreateDraftingGraphics();
        }
        #endregion

        #region Copying and Pasting
        private void SetDrawingStateToCopy()
        {
            if (SelectedElements.Count >= 1)
            {
                if (_copyWithBasePoint == true)
                {
                    _currentDrawingType = DrawingType.CopyWithBasePoint;

                    InputTextBox.Text = "Enter base point:";

                    _readOnlyChars = InputTextBox.Text.Length;

                    this.Cursor = Cursors.Cross;
                }
                else
                {
                    _tempCopiedElements = new List<IDrawingElement>(SelectedElements);

                    _copyBasePoint = _tempCopiedElements[0].BasePoint();

                    _orthoBasePoint = _copyBasePoint;
                }

                _lastCommand = "";
            }
        }

        private void CopyElementsWithBasePointViaMouseClick(PointD globalPoint)
        {
            if (SelectedElements != null)
            {
                if (SelectedElements.Count >=1)
                {
                    _copyBasePoint = globalPoint;

                    _orthoBasePoint = globalPoint;

                    if (SelectedElements != null)
                    {
                        if (SelectedElements.Count >= 1)
                        {
                            _tempCopiedElements = new List<IDrawingElement>(SelectedElements);
                        }
                    }

                    ResetDrawingState();

                    CreateDraftingGraphics();
                }
            }
        }

        private void ReturnKeyPressedWhileCopying()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (double.TryParse(lineSplit[0], out double x) == true)
                {
                    if (double.TryParse(lineSplit[1], out double y) == true)
                    {
                        PointD p = new PointD(x, y);

                        _copyBasePoint = p;

                        if (SelectedElements != null)
                        {
                            if (SelectedElements.Count >= 1)
                            {
                                _tempCopiedElements = new List<IDrawingElement>(SelectedElements);
                            }
                        }

                        ResetDrawingState();
                    }
                }
            }
        }

        private void MoveTempCopiedObjects(PointD globalPoint)
        {
            _tempMovedObjects.Clear();

            foreach (IDrawingElement elem in _tempCopiedElements)
            {
                _tempMovedObjects.Add(elem.CreateCopy(_copyBasePoint, globalPoint));
            }
        }

        private void SetDrawingStateToPaste()
        {
            if (_tempCopiedElements.Count >= 1)
            {
                _currentDrawingType = DrawingType.Paste;

                _drawingInProgress = true;

                InputTextBox.Text = "Enter insertion point:";

                _readOnlyChars = InputTextBox.Text.Length;

                _orthoBasePoint = _copyBasePoint;

                this.Cursor = Cursors.Cross;

                _lastCommand = "paste";
            }
        }

        private void EndPasteViaMouseClick(PointD globalPoint)
        {
            PasteCopiedElements(globalPoint);

            ResetDrawingState();

            CreateDraftingGraphics();
        }

        private void ReturnKeyPressedWhilePasting()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == true)
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            PasteCopiedElements(p);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD p = new PointD(_copyBasePoint.X + xRel, _copyBasePoint.Y + yRel);

                                    PasteCopiedElements(p);

                                    ResetDrawingState();

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void PasteCopiedElements(PointD globalPoint)
        {
            if (_tempCopiedElements != null)
            {
                if (_tempCopiedElements.Count >= 1)
                {
                    List<IDrawingElement> pastedElements = new List<IDrawingElement>(_tempCopiedElements.Count);

                    foreach (IDrawingElement elem in _tempCopiedElements)
                    {
                        pastedElements.Add(elem.CreateCopy(_copyBasePoint, globalPoint));
                    }

                    AddDrawingElement(pastedElements);
                }
            }
        }
        #endregion

        #region Rotating
        private void SetDrawingStateToRotate()
        {
            if (SelectedElements.Count >= 1)
            {
                _currentDrawingType = DrawingType.Rotate;

                InputTextBox.Text = "Enter rotation base point:";

                _readOnlyChars = InputTextBox.Text.Length;

                this.Cursor = Cursors.Cross;

                _lastCommand = "ro";
            }
        }

        private void BeginRotateViaMouseClick(PointD globalPoint)
        {
            _rotateBasePoint = globalPoint;

            _orthoBasePoint = globalPoint;

            _drawingInProgress = true;

            InputTextBox.Text = "Enter final point, or angle (in degrees):";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileRotating()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (double.TryParse(lineSplit[0], out double x) == true)
                {
                    if (double.TryParse(lineSplit[1], out double y) == true)
                    {
                        PointD p = new PointD(x, y);

                        _rotateBasePoint = p;

                        foreach (IDrawingElement elem in SelectedElements)
                        {
                            _tempMovedObjects.Add(elem.CreateCopy(_rotateBasePoint, _rotateBasePoint));
                        }

                        InputTextBox.Text = "Enter final point, or angle (in degrees):";

                        _readOnlyChars = InputTextBox.Text.Length;

                        _drawingInProgress = true;
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            RotateElements(p);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD p = new PointD(_rotateBasePoint.X + xRel, _rotateBasePoint.Y + yRel);

                                    RotateElements(p);

                                    ResetDrawingState();

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (lineSplit.GetUpperBound(0) == 0)
                    {
                        if (double.TryParse(lineSplit[0], out double angle) == true)
                        {
                            RotateElements(angle);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }

        private void RotateTempRotatedElements(PointD globalPoint)
        {
            _tempMovedObjects.Clear();

            foreach (IDrawingElement elem in SelectedElements)
            {
                IDrawingElement rotElem = elem.CreateCopy(_rotateBasePoint, _rotateBasePoint);

                rotElem.Rotate(_rotateBasePoint, globalPoint);

                _tempMovedObjects.Add(rotElem);
            }
        }

        private void RotateElements(PointD globalPoint)
        {
            foreach (IDrawingElement elem in SelectedElements)
            {
                InstanceData.DrawingElementQuadTree.Remove(elem);

                elem.Rotate(_rotateBasePoint, globalPoint);

                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
            }

            double rotationAngle = globalPoint.GetAngleFromPoint(_rotateBasePoint);

            UndoSupport.Actions.RotateAction mA = new UndoSupport.Actions.RotateAction()
            {
                BasePoint = _rotateBasePoint,
                Angle = rotationAngle
            };

            Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

            actions.Add(mA, new List<IDrawingElement>(SelectedElements));

            AddActionToList(new ActionOnElements(actions));
        }

        /// <summary>
        /// Rotates the selected elements through the specified angle, in degrees
        /// </summary>
        /// <param name="angleInDegrees">Angle from horizontal, in degrees</param>
        private void RotateElements(double angleInDegrees)
        {
            double angleInRadians = angleInDegrees * (2.0 * PI) / 360.0;

            foreach (IDrawingElement elem in SelectedElements)
            {
                InstanceData.DrawingElementQuadTree.Remove(elem);

                elem.RotateThroughAngle(_rotateBasePoint, angleInRadians);

                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
            }


            UndoSupport.Actions.RotateAction mA = new UndoSupport.Actions.RotateAction()
            {
                BasePoint = _rotateBasePoint,
                Angle = angleInRadians
            };

            Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

            actions.Add(mA, new List<IDrawingElement>(SelectedElements));

            AddActionToList(new ActionOnElements(actions));
        }

        private void EndRotateViaMouseClick(PointD globalPoint)
        {
            RotateElements(globalPoint);

            ResetDrawingState();

            CreateDraftingGraphics();
        }
        #endregion

        #region Joining
        /// <summary>
        /// Joins objects in the selected objects list, if possible
        /// </summary>
        private void JoinSelectedElements()
        {
            if (SelectedElements != null && SelectedElements.Count > 1)
            {
                List<IDrawingElement> elementsJoined = new List<IDrawingElement>();

                List<IDrawingElement> joinedElements = new List<IDrawingElement>();

                int selectedElementCount = SelectedElements.Count;

                int count1 = 0;

                while (count1 < selectedElementCount - 1)
                {
                    IDrawingElement elem1 = SelectedElements[count1];

                    bool addedElem1 = false;

                    int count2 = count1 + 1;

                    while (count2 < selectedElementCount)
                    {
                        IDrawingElement elem2 = SelectedElements[count2];

                        if (elem1.JoinWith(elem2, out IDrawingElement newElem) == true)
                        {
                            if (addedElem1 == false)
                            {
                                elem1.Selected = false;

                                elementsJoined.Add(elem1);

                                addedElem1 = true;
                            }

                            elem1 = newElem;

                            elem2.Selected = false;

                            elementsJoined.Add(elem2);

                            SelectedElements.RemoveAt(count2);

                            selectedElementCount--;

                            count2 = count1 + 1;
                        }
                        else
                        {
                            count2++;
                        }
                    }

                    if (addedElem1 == true)
                    {
                        joinedElements.Add(elem1);

                        SelectedElements.RemoveAt(count1);

                        selectedElementCount--;

                        count1--;
                    }

                    count1++;
                }

                if (elementsJoined.Count > 0 && joinedElements.Count > 0)
                {
                    Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

                    actions.Add(new AddAction(), joinedElements);
                    actions.Add(new DeleteAction(), elementsJoined);

                    ActionOnElements newAction = new ActionOnElements(actions);

                    for (int i= 0; i < elementsJoined.Count; i++)
                    {
                        IDrawingElement elem = elementsJoined[i];

                        InstanceData.DrawingElementList.Remove(elem);

                        InstanceData.DrawingElementQuadTree.Remove(elem);
                    }

                    for (int i = 0; i < joinedElements.Count; i++)
                    {
                        IDrawingElement elem = joinedElements[i];

                        InstanceData.DrawingElementList.Add(elem);

                        InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                    }

                    AddActionToList(newAction);

                    ResetDrawingState();

                    CreateDraftingGraphics();

                    SetPropertyPanelAccordianVisibility();
                }
            }
        }
        #endregion

        #region Explode
        private void Explode()
        {
            if (SelectedElements != null && SelectedElements.Count > 0)
            {
                List<IDrawingElement> elementsExploded = new List<IDrawingElement>();

                List<IDrawingElement> explodedElements = new List<IDrawingElement>();

                int selectedCount = SelectedElements.Count;

                int count = 0;

                while(count < selectedCount)
                {
                    List<IDrawingElement> elemExplode = SelectedElements[count].Explode();

                    if (elemExplode.Count > 0)
                    {
                        SelectedElements[count].Selected = false;

                        elementsExploded.Add(SelectedElements[count]);

                        explodedElements.AddRange(elemExplode);

                        SelectedElements.RemoveAt(count);

                        selectedCount--;

                        count--;
                    }

                    count++;
                }

                if (elementsExploded.Count > 0 && explodedElements.Count > 0)
                {
                    Dictionary<DrawingAction, List<IDrawingElement>> actions = new Dictionary<DrawingAction, List<IDrawingElement>>();

                    actions.Add(new AddAction(), explodedElements);
                    actions.Add(new DeleteAction(), elementsExploded);

                    ActionOnElements newAction = new ActionOnElements(actions);

                    for (int i = 0; i < elementsExploded.Count; i++)
                    {
                        IDrawingElement elem = elementsExploded[i];

                        InstanceData.DrawingElementList.Remove(elem);

                        InstanceData.DrawingElementQuadTree.Remove(elem);
                    }

                    for (int i = 0; i < explodedElements.Count; i++)
                    {
                        IDrawingElement elem = explodedElements[i];

                        InstanceData.DrawingElementList.Add(elem);

                        InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                    }

                    AddActionToList(newAction);

                    ResetDrawingState();

                    CreateDraftingGraphics();

                    SetPropertyPanelAccordianVisibility();
                }
            }
        }
        #endregion

        #region Linear Array
        private void SetDrawingStateToLinearArray()
        {
            if (SelectedElements.Count >= 1)
            {
                _currentDrawingType = DrawingType.LinearArray;

                InputTextBox.Text = "Enter distance between elements [or enter/click first measurement point]:";

                _readOnlyChars = InputTextBox.Text.Length;

                this.Cursor = Cursors.Cross;

                _lastCommand = "arr";
            }
        }

        private void BeginLinearArrayViaMouseClick(PointD globalPoint)
        {
            if(_arrayDistanceSet == false)
            {
                if (_arrayDistBasePoint == null)
                {
                    _arrayDistBasePoint = globalPoint;

                    InputTextBox.Text = "Enter [or click] final measurement point:";

                    _readOnlyChars = InputTextBox.Text.Length;
                }
                else
                {
                    _linearArrayDistance = _arrayDistBasePoint.DistanceTo(globalPoint);

                    _arrayDistanceSet = true;

                    InputTextBox.Text = "Enter array base point:";

                    _readOnlyChars = InputTextBox.Text.Length;
                }
            }
            else
            {
                _arrayBasePoint = globalPoint;

                _orthoBasePoint = globalPoint;

                _drawingInProgress = true;

                InputTextBox.Text = "Enter final point:";

                _readOnlyChars = InputTextBox.Text.Length;
            }   
        }

        private void ReturnKeyPressedWhileLinearArraying()
        {
            string[] lineSplit = Regex.Split(InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars), ",");

            if (_drawingInProgress == false)
            {
                if (_arrayDistanceSet == false)
                {
                    if (lineSplit.GetUpperBound(0) == 0)
                    {
                        if (double.TryParse(lineSplit[0], out double arrDist) == true)
                        {
                            _linearArrayDistance = arrDist;

                            _arrayDistanceSet = true;

                            InputTextBox.Text = "Enter array base point:";

                            _readOnlyChars = InputTextBox.Text.Length;
                        }
                    }
                    else if (lineSplit.GetUpperBound(0) == 1)
                    {
                        if (double.TryParse(lineSplit[0], out double x) == true)
                        {
                            if (double.TryParse(lineSplit[1], out double y) == true)
                            {
                                PointD p = new PointD(x, y);

                                if (_arrayDistBasePoint == null)
                                {
                                    _arrayDistBasePoint = p;

                                    InputTextBox.Text = "Enter [or click] final measurement point:";

                                    _readOnlyChars = InputTextBox.Text.Length;
                                }
                                else
                                {
                                    _linearArrayDistance = _arrayDistBasePoint.DistanceTo(p);

                                    _arrayDistanceSet = true;

                                    InputTextBox.Text = "Enter array base point:";

                                    _readOnlyChars = InputTextBox.Text.Length;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            _arrayBasePoint = p;

                            InputTextBox.Text = "Enter final point:";

                            _readOnlyChars = InputTextBox.Text.Length;

                            _drawingInProgress = true;
                        }
                    }
                }
            }
            else
            {
                if (lineSplit.GetUpperBound(0) == 1)
                {
                    if (double.TryParse(lineSplit[0], out double x) == true)
                    {
                        if (double.TryParse(lineSplit[1], out double y) == true)
                        {
                            PointD p = new PointD(x, y);

                            LinearArraySelectedElements(p);

                            ResetDrawingState();

                            CreateDraftingGraphics();
                        }
                    }
                    else
                    {
                        if (String.Compare(lineSplit[0].Left(1), "@", ignoreCase: true) == 0)
                        {
                            string s1 = lineSplit[0].Right(lineSplit[0].Length - 1);

                            if (double.TryParse(s1, out double xRel) == true)
                            {
                                if (double.TryParse(lineSplit[1], out double yRel) == true)
                                {
                                    PointD p = new PointD(_arrayBasePoint.X + xRel, _arrayBasePoint.Y + yRel);

                                    LinearArraySelectedElements(p);

                                    ResetDrawingState();

                                    CreateDraftingGraphics();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void LinearArrayTempElements(PointD globalPoint)
        {
            _tempMovedObjects.Clear();

            double distFromBasePoint = _arrayBasePoint.DistanceTo(globalPoint);

            int numArrayPoints = Convert.ToInt32(Floor(distFromBasePoint / _linearArrayDistance));

            if (numArrayPoints > 0)
            {
                double uv_x = (globalPoint.X - _arrayBasePoint.X) / distFromBasePoint;

                double uv_y = (globalPoint.Y - _arrayBasePoint.Y) / distFromBasePoint;

                for (int i = 1; i < numArrayPoints; i++)
                {
                    PointD pastePoint = new PointD(_arrayBasePoint.X + uv_x * _linearArrayDistance * i,
                        _arrayBasePoint.Y + uv_y * _linearArrayDistance * i);

                    for (int j = 0; j < SelectedElements.Count; j++)
                    {
                        _tempMovedObjects.Add(SelectedElements[j].CreateCopy(_arrayBasePoint, pastePoint));
                    }
                }
            }
        }

        private void EndLinearArrayViaMouseClick(PointD globalPoint)
        {
            LinearArraySelectedElements(globalPoint);

            ResetDrawingState();

            CreateDraftingGraphics();
        }

        private void LinearArraySelectedElements(PointD globalPoint)
        {
            double distFromBasePoint = _arrayBasePoint.DistanceTo(globalPoint);

            int numArrayPoints = Convert.ToInt32(Floor(distFromBasePoint / _linearArrayDistance));

            if (numArrayPoints > 0)
            {
                List<IDrawingElement> arrayedElements = new List<IDrawingElement>();

                double uv_x = (globalPoint.X - _arrayBasePoint.X) / distFromBasePoint;

                double uv_y = (globalPoint.Y - _arrayBasePoint.Y) / distFromBasePoint;

                for (int i = 1; i < numArrayPoints; i++)
                {
                    PointD pastePoint = new PointD(_arrayBasePoint.X + uv_x * _linearArrayDistance * i,
                        _arrayBasePoint.Y + uv_y * _linearArrayDistance * i);

                    for (int j = 0; j < SelectedElements.Count; j++)
                    {
                        arrayedElements.Add(SelectedElements[j].CreateCopy(_arrayBasePoint, pastePoint));
                    }
                }

                AddDrawingElement(arrayedElements);
            }
        }
        #endregion

        #region Close Shape
        private void CloseShape()
        {
            _lastCommand = "cs";

            if (SelectedElements != null && SelectedElements.Count > 0)
            {
                Dictionary<IDrawingElement, IDrawingElement> elementsClosedAndTheirNewElement = new Dictionary<IDrawingElement, IDrawingElement>();

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    if (SelectedElements[i].CloseShape(out IDrawingElement newClosedElement) == true)
                    {
                        elementsClosedAndTheirNewElement.Add(SelectedElements[i], newClosedElement);
                    }
                }

                if (elementsClosedAndTheirNewElement.Count > 0)
                {
                    ActionOnElements action = new ActionOnElements();

                    AddAction add = new AddAction();

                    DeleteAction delete = new DeleteAction();

                    foreach (KeyValuePair<IDrawingElement, IDrawingElement> kvp in elementsClosedAndTheirNewElement)
                    {
                        action.AddAction(add, new List<IDrawingElement>() { kvp.Value });
                        action.AddAction(delete, new List<IDrawingElement>() { kvp.Key });

                        kvp.Key.Selected = false;

                        InstanceData.DrawingElementList.Remove(kvp.Key);
                        InstanceData.DrawingElementQuadTree.Remove(kvp.Key);

                        SelectedElements.Remove(kvp.Key);

                        InstanceData.DrawingElementList.Add(kvp.Value);
                        InstanceData.DrawingElementQuadTree.Insert(kvp.Value, kvp.Value.BoundingRectangle());
                    }

                    AddActionToList(action);

                    CreateDraftingGraphics();

                    SetPropertyPanelAccordianVisibility();
                }
            }

            InputTextBox.Clear();

            _readOnlyChars = 0;
        }
        #endregion

        #region Command List
        private void LoadCommandListForm(List<CadCommand> commandList)
        {
            InputTextBox.Clear();

            _readOnlyChars = 0;

            CommandListForm frm = new CommandListForm(_commandList);

            frm.ShowDialog();

            frm.Dispose();
        }
        #endregion

        #region Layer Properties
        private void LayerProperties()
        {
            _lastCommand = "layers";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            List<ComboBox> layersComboBoxes = new List<ComboBox>();

            List<Control> allChildControls = this.GetAllChildControls();

            for (int i = 0; i < allChildControls.Count; i++)
            {
                if (allChildControls[i].Name.Contains("Layer") == true)
                {
                    if (allChildControls[i].GetType().Equals(typeof(ComboBox)) == true)
                    {
                        layersComboBoxes.Add((ComboBox)allChildControls[i]);
                    }
                }
            }

            Dictionary<ComboBox, Layer> selectedLayers = new Dictionary<ComboBox, Layer>();

            for (int i = 0; i < layersComboBoxes.Count; i++)
            {
                selectedLayers.Add(layersComboBoxes[i], InstanceData.GetLayerFromName(layersComboBoxes[i].Text));
            }

            LayerPropertiesForm frm = new LayerPropertiesForm(InstanceData.Layers);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                InstanceData.Layers = frm.WorkingLayersList;

                foreach (KeyValuePair<ComboBox, Layer> kvp in selectedLayers)
                {
                    kvp.Key.Items.Clear();

                    int selIndex = -1;

                    for (int i = 0; i < InstanceData.Layers.Count; i++)
                    {
                        kvp.Key.Items.Add(InstanceData.Layers[i].Name);

                        if (InstanceData.Layers[i] == kvp.Value)
                        {
                            selIndex = i;
                        }
                    }

                    kvp.Key.SelectedIndex = selIndex;
                }

                _currentLayerDropdown.Items.Clear();

                int currentLayerSelIndex = 0;

                for (int i = 0; i < InstanceData.Layers.Count; i++)
                {
                    _currentLayerDropdown.Items.Add(InstanceData.Layers[i].Name);

                    if(InstanceData.Layers[i] == InstanceData.CurrentDrawingLayer)
                    {
                        currentLayerSelIndex = i;
                    }
                }

                _currentLayerDropdown.SelectedIndex = currentLayerSelIndex;

                CreateDraftingGraphics();
            }

            frm.Dispose();
        }
        #endregion

        #region Offsetting
        private void SetDrawingStateToOffset()
        {
            _lastCommand = "o";

            _currentDrawingType = DrawingType.Offset;

            InputTextBox.Text = $"Enter offset distance [{_offsetDistance}]:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileOffsetting()
        {
            string line = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

            if (_offsetDistanceSet == false)
            {
                if (string.IsNullOrEmpty(line) == true)
                {
                    //do nothing
                }
                else if (double.TryParse(line, out double res) == true)
                {
                    _offsetDistance = res;
                }
                else
                {
                    return; //User entered invalid text
                }

                if (_offsetDistance > 0)
                {
                    _offsetDistanceSet = true;

                    if (ElementsSelected() == true)
                    {
                        _drawingInProgress = true;

                        InputTextBox.Text = $"Select side of element to offset to:";

                        _readOnlyChars = InputTextBox.Text.Length;

                        this.Cursor = Cursors.Cross;
                    }
                    else
                    {
                        InputTextBox.Text = $"Select element to be offset:";

                        _readOnlyChars = InputTextBox.Text.Length;

                        this.Cursor = Cursors.Cross;
                    }
                }
                else
                {
                    InputTextBox.Text = $"Offset distance must be greater than 0, please re-enter distance:";

                    _readOnlyChars = InputTextBox.Text.Length;
                }              
            }          
        }

        private void MouseClickedWhileOffsetting(PointD globalPoint)
        {
            if (_drawingInProgress == false)
            {
                if (ElementsSelected() == true)
                {
                    _drawingInProgress = true;

                    InputTextBox.Text = $"Select side of element to offset to:";

                    _readOnlyChars = InputTextBox.Text.Length;

                    this.Cursor = Cursors.Cross;
                }
            }
            else
            {
                OffsetElements(globalPoint);
            }           
        }

        private void DrawTempOffsetElements(PointD globalPoint)
        {
            if (_drawingInProgress == true)
            {
                _tempMovedObjects.Clear();

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    IDrawingElement offsetElem = SelectedElements[i].Offset(_offsetDistance, globalPoint);

                    if (offsetElem != null)
                    {
                        _tempMovedObjects.Add(offsetElem);
                    }
                }
            }
        }

        private void OffsetElements(PointD globalPoint)
        {
            if (SelectedElements.Count > 0)
            {
                if (_offsetDistance > 0)
                {
                    List<IDrawingElement> offsetElements = new List<IDrawingElement>();

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement offsetElem = SelectedElements[i].Offset(_offsetDistance, globalPoint);

                        if (offsetElem != null)
                        {
                            offsetElements.Add(offsetElem);
                        }
                    }

                    if (offsetElements.Count > 0)
                    {
                        AddDrawingElement(offsetElements);

                        ResetDrawingState();

                        CreateDraftingGraphics();
                    }
                }
            }
        }
        #endregion

        #region Polygon Boolean Operations
        private bool _deleteSourcePolygons = false;

        private void SetDrawingStateToBooleanUnion()
        {
            _lastCommand = "booleanunion";

            string yOrN = (_deleteSourcePolygons == true) ? "y" : "n";

            InputTextBox.Text = $"Delete source polygons? (y/n) [{yOrN}]:";

            _readOnlyChars = InputTextBox.Text.Length;

            _currentDrawingType = DrawingType.BooleanUnion;
        }

        private void ReturnKeyPressedWhileBooleanUnion()
        {
            string line = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

            bool lineParsed = false;

            if (string.IsNullOrEmpty(line) == true || string.IsNullOrWhiteSpace(line) == true)
            {
                lineParsed = true;
            }
            if (string.Compare(line, "y", ignoreCase:true) == 0 || string.Compare(line, "yes", ignoreCase:true) == 0)
            {
                _deleteSourcePolygons = true;

                lineParsed = true;
            }
            else if (string.Compare(line, "n", ignoreCase: true) == 0 || string.Compare(line, "no", ignoreCase: true) == 0)
            {
                _deleteSourcePolygons = false;

                lineParsed = true;
            }

            if (lineParsed == true)
            {
                BooleanUnionPolygons();
            }
        }

        private void BooleanUnionPolygons()
        {
            if (ElementsSelected() == true)
            {
                if (SelectedElements.Count > 1)
                {
                    //must be at least two elements

                    //this operation only works for polygons, so establish list of only polygons
                    List<PolygonG> polyList = new List<PolygonG>();

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.GetType().Equals(typeof(PolygonG)) == true || elem.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                        {
                            polyList.Add((PolygonG)elem);
                        }
                    }

                    if (polyList.Count > 1)
                    {
                        //must be at lest two polygons
                        List<PolygonG> unionedPolygons = PolyBool.PolyBoolHelperMethods.BooleanUnion(polyList, 1.0E-6);

                        if (unionedPolygons.Count > 0)
                        {
                            for (int i = 0; i < unionedPolygons.Count; i++)
                            {
                                unionedPolygons[i].MatchProperties(polyList[0]);
                            }

                            List<IDrawingElement> addedElementList = new List<IDrawingElement>(unionedPolygons);

                            Dictionary<DrawingAction, List<IDrawingElement>> actionsDictionary = new Dictionary<DrawingAction, List<IDrawingElement>>();

                            AddAction addAction = new AddAction();

                            actionsDictionary.Add(addAction, addedElementList);

                            for (int i = 0; i < addedElementList.Count; i++)
                            {
                                IDrawingElement elem = addedElementList[i];

                                InstanceData.DrawingElementList.Add(elem);

                                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                            }

                            if (_deleteSourcePolygons == true)
                            {
                                DeleteAction deleteAction = new DeleteAction();

                                List<IDrawingElement> deletedElementList = new List<IDrawingElement>(polyList);

                                actionsDictionary.Add(deleteAction, deletedElementList);

                                for (int i = 0; i < polyList.Count; i++)
                                {
                                    IDrawingElement elem = polyList[i];

                                    elem.Selected = false;

                                    SelectedElements.Remove(elem);

                                    InstanceData.DrawingElementList.Remove(elem);

                                    InstanceData.DrawingElementQuadTree.Remove(elem);
                                }
                            }

                            ActionOnElements action = new ActionOnElements(actionsDictionary);

                            AddActionToList(action);
                        }
                    }
                }
            }

            ResetDrawingState();

            CreateDraftingGraphics();
        }

        private void SetDrawingStateToBooleanIntersection()
        {
            _lastCommand = "booleanintersection";

            string yOrN = (_deleteSourcePolygons == true) ? "y" : "n";

            InputTextBox.Text = $"Delete source polygons? (y/n) [{yOrN}]:";

            _readOnlyChars = InputTextBox.Text.Length;

            _currentDrawingType = DrawingType.BooleanIntersection;
        }

        private void ReturnKeyPressedWhileBooleanIntersection()
        {
            string line = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

            bool lineParsed = false;

            if (string.IsNullOrEmpty(line) == true || string.IsNullOrWhiteSpace(line) == true)
            {
                lineParsed = true;
            }
            if (string.Compare(line, "y", ignoreCase: true) == 0 || string.Compare(line, "yes", ignoreCase: true) == 0)
            {
                _deleteSourcePolygons = true;

                lineParsed = true;
            }
            else if (string.Compare(line, "n", ignoreCase: true) == 0 || string.Compare(line, "no", ignoreCase: true) == 0)
            {
                _deleteSourcePolygons = false;

                lineParsed = true;
            }

            if (lineParsed == true)
            {
                BooleanIntersectPolygons();
            }
        }

        private void BooleanIntersectPolygons()
        {
            if (ElementsSelected() == true)
            {
                if (SelectedElements.Count > 1)
                {
                    //must be at least two elements

                    //this operation only works for polygons, so establish list of only polygons
                    List<PolygonG> polyList = new List<PolygonG>();

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.GetType().Equals(typeof(PolygonG)) == true || elem.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                        {
                            polyList.Add((PolygonG)elem);
                        }
                    }

                    if (polyList.Count == 2)
                    {
                        //must be only two polygons
                        List<PolygonG> intersectedPolygons = PolyBool.PolyBoolHelperMethods.BooleanIntersection(polyList[0], polyList[1]);

                        if (intersectedPolygons.Count > 0)
                        {
                            for (int i = 0; i < intersectedPolygons.Count; i++)
                            {
                                intersectedPolygons[i].MatchProperties(polyList[0]);
                            }

                            List<IDrawingElement> addedElementList = new List<IDrawingElement>(intersectedPolygons);

                            Dictionary<DrawingAction, List<IDrawingElement>> actionsDictionary = new Dictionary<DrawingAction, List<IDrawingElement>>();

                            AddAction addAction = new AddAction();

                            actionsDictionary.Add(addAction, addedElementList);

                            for (int i = 0; i < addedElementList.Count; i++)
                            {
                                IDrawingElement elem = addedElementList[i];

                                InstanceData.DrawingElementList.Add(elem);

                                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                            }

                            if (_deleteSourcePolygons == true)
                            {
                                DeleteAction deleteAction = new DeleteAction();

                                List<IDrawingElement> deletedElementList = new List<IDrawingElement>(polyList);

                                actionsDictionary.Add(deleteAction, deletedElementList);

                                for (int i = 0; i < polyList.Count; i++)
                                {
                                    IDrawingElement elem = polyList[i];

                                    elem.Selected = false;

                                    SelectedElements.Remove(elem);

                                    InstanceData.DrawingElementList.Remove(elem);

                                    InstanceData.DrawingElementQuadTree.Remove(elem);
                                }
                            }

                            ActionOnElements action = new ActionOnElements(actionsDictionary);

                            AddActionToList(action);
                        }
                    }
                }
            }

            ResetDrawingState();

            CreateDraftingGraphics();
        }

        private void SetDrawingStateToBooleanDifference()
        {
            _lastCommand = "booleandifference";

            string yOrN = (_deleteSourcePolygons == true) ? "y" : "n";

            InputTextBox.Text = $"Delete source polygons? (y/n) [{yOrN}]:";

            _readOnlyChars = InputTextBox.Text.Length;

            _currentDrawingType = DrawingType.BooleanDifference;
        }

        private void ReturnKeyPressedWhileBooleanDifference()
        {
            string line = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

            bool lineParsed = false;

            if (string.IsNullOrEmpty(line) == true || string.IsNullOrWhiteSpace(line) == true)
            {
                lineParsed = true;
            }
            if (string.Compare(line, "y", ignoreCase: true) == 0 || string.Compare(line, "yes", ignoreCase: true) == 0)
            {
                _deleteSourcePolygons = true;

                lineParsed = true;
            }
            else if (string.Compare(line, "n", ignoreCase: true) == 0 || string.Compare(line, "no", ignoreCase: true) == 0)
            {
                _deleteSourcePolygons = false;

                lineParsed = true;
            }

            if (lineParsed == true)
            {
                BooleanDifferencePolygons();
            }
        }

        private void BooleanDifferencePolygons()
        {
            if (ElementsSelected() == true)
            {
                if (SelectedElements.Count > 1)
                {
                    //must be at least two elements

                    //this operation only works for polygons, so establish list of only polygons
                    List<PolygonG> polyList = new List<PolygonG>();

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.GetType().Equals(typeof(PolygonG)) == true || elem.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                        {
                            polyList.Add((PolygonG)elem);
                        }
                    }

                    if (polyList.Count == 2)
                    {
                        //must be only two polygons
                        List<PolygonG> intersectedPolygons = PolyBool.PolyBoolHelperMethods.BooleanDifference(polyList[0], polyList[1]);

                        if (intersectedPolygons.Count > 0)
                        {
                            for (int i = 0; i < intersectedPolygons.Count; i++)
                            {
                                intersectedPolygons[i].MatchProperties(polyList[0]);
                            }

                            List<IDrawingElement> addedElementList = new List<IDrawingElement>(intersectedPolygons);

                            Dictionary<DrawingAction, List<IDrawingElement>> actionsDictionary = new Dictionary<DrawingAction, List<IDrawingElement>>();

                            AddAction addAction = new AddAction();

                            actionsDictionary.Add(addAction, addedElementList);

                            for (int i = 0; i < addedElementList.Count; i++)
                            {
                                IDrawingElement elem = addedElementList[i];

                                InstanceData.DrawingElementList.Add(elem);

                                InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                            }

                            if (_deleteSourcePolygons == true)
                            {
                                DeleteAction deleteAction = new DeleteAction();

                                List<IDrawingElement> deletedElementList = new List<IDrawingElement>(polyList);

                                actionsDictionary.Add(deleteAction, deletedElementList);

                                for (int i = 0; i < polyList.Count; i++)
                                {
                                    IDrawingElement elem = polyList[i];

                                    elem.Selected = false;

                                    SelectedElements.Remove(elem);

                                    InstanceData.DrawingElementList.Remove(elem);

                                    InstanceData.DrawingElementQuadTree.Remove(elem);
                                }
                            }

                            ActionOnElements action = new ActionOnElements(actionsDictionary);

                            AddActionToList(action);
                        }
                    }
                }
            }

            ResetDrawingState();

            CreateDraftingGraphics();
        }
        #endregion

        #region Triangle Meshing
        private bool _deleteSourceMeshElements = false;

        private double _meshSize = 6.0;

        private bool _meshSizeSet = false;

        private void SetDrawingStateToTriangleNet()
        {
            _currentDrawingType = DrawingType.TriangleNet;

            _lastCommand = "trimesh";

            InputTextBox.Text = $"Enter mesh size [{_meshSize}]:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void SetDrawingStateToQuadMesh()
        {
            _currentDrawingType = DrawingType.QuadMesh;

            _lastCommand = "quadmesh";

            InputTextBox.Text = $"Enter mesh size [{_meshSize}]:";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        private void ReturnKeyPressedWhileTriangleNet()
        {
            string line = InputTextBox.Text.Right(InputTextBox.Text.Length - _readOnlyChars);

            if (_meshSizeSet == true)
            {
                bool lineParsed = false;

                if (string.IsNullOrEmpty(line) == true || string.IsNullOrWhiteSpace(line) == true)
                {
                    lineParsed = true;
                }
                if (string.Compare(line, "y", ignoreCase: true) == 0 || string.Compare(line, "yes", ignoreCase: true) == 0)
                {
                    _deleteSourceMeshElements = true;

                    lineParsed = true;
                }
                else if (string.Compare(line, "n", ignoreCase: true) == 0 || string.Compare(line, "no", ignoreCase: true) == 0)
                {
                    _deleteSourceMeshElements = false;

                    lineParsed = true;
                }

                if (lineParsed == true)
                {
                    //Check for any closed shapes in the selected elements list
                    bool containsClosedShapes = false;

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        if (SelectedElements[i].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                        {
                            containsClosedShapes = true;

                            break;
                        }
                    }

                    if (containsClosedShapes == false)
                    {
                        ResetDrawingState();

                        return;
                    }

                    List<PolygonG> mesh = MeshUsingTriangleNet(SelectedElements.ToList(), _meshSize);

                    if (_currentDrawingType.Equals(DrawingType.QuadMesh) == true)
                    {
                        List<PolygonG> quadMesh = new List<PolygonG>();

                        //Loop through triangles and combine
                        int ind = 0;

                        while (ind < mesh.Count - 1)
                        {
                            PolygonG poly1 = mesh[ind];

                            int ind2 = ind + 1;

                            while (ind2 < mesh.Count)
                            {
                                PolygonG poly2 = mesh[ind2];

                                int numSameNodes = 0;

                                List<DrawingNode> sameNodes = new List<DrawingNode>();

                                for (int i = 0; i < poly1.Nodes.Count - 1; i++)
                                {
                                    for (int j = 0; j < poly2.Nodes.Count - 1; j++)
                                    {
                                        if (poly1.Nodes[i].Point == poly2.Nodes[j].Point)
                                        {
                                            numSameNodes++;

                                            sameNodes.Add(poly1.Nodes[i]);

                                            break;
                                        }
                                    }
                                }

                                if (numSameNodes == 2)
                                {
                                    List<DrawingNode> differentNodes = new List<DrawingNode>();

                                    for (int i = 0; i < poly1.Nodes.Count - 1; i++)
                                    {
                                        bool isSame = false;

                                        for (int j = 0; j < sameNodes.Count; j++)
                                        {
                                            if (poly1.Nodes[i].Point == sameNodes[j].Point)
                                            {
                                                isSame = true;
                                            }
                                        }
                                        
                                        if (isSame == false)
                                        {
                                            differentNodes.Add(poly1.Nodes[i]);
                                        }
                                    }

                                    for (int i = 0; i < poly2.Nodes.Count - 1; i++)
                                    {
                                        bool isSame = false;

                                        for (int j = 0; j < sameNodes.Count; j++)
                                        {
                                            if (poly2.Nodes[i].Point == sameNodes[j].Point)
                                            {
                                                isSame = true;
                                            }
                                        }

                                        if (isSame == false)
                                        {
                                            differentNodes.Add(poly2.Nodes[i]);
                                        }
                                    }

                                    List<DrawingNode> quadNodes = new List<DrawingNode>(5);
                                    quadNodes.Add(sameNodes[0]);
                                    quadNodes.Add(differentNodes[0]);
                                    quadNodes.Add(sameNodes[1]);
                                    quadNodes.Add(differentNodes[1]);
                                    quadNodes.Add(sameNodes[0]);

                                    PolygonG quad = new PolygonG();
                                    quad.Nodes = quadNodes;
                                    quad.Layer = InstanceData.CurrentDrawingLayer;

                                    quadMesh.Add(quad);

                                    mesh.RemoveAt(ind);
                                    mesh.RemoveAt(ind2 - 1);

                                    ind -= 1;
                                    break;
                                }

                                ind2++;
                            }
                            ind++;
                        }

                        mesh = quadMesh;
                    }

                    if (mesh != null && mesh.Count > 0)
                    {
                        List<IDrawingElement> addedElementList = new List<IDrawingElement>(mesh);

                        Dictionary<DrawingAction, List<IDrawingElement>> actionsDictionary = new Dictionary<DrawingAction, List<IDrawingElement>>();

                        AddAction addAction = new AddAction();

                        actionsDictionary.Add(addAction, addedElementList);

                        for (int i = 0; i < addedElementList.Count; i++)
                        {
                            IDrawingElement elem = addedElementList[i];

                            elem.Layer = InstanceData.CurrentDrawingLayer;

                            InstanceData.DrawingElementList.Add(elem);

                            InstanceData.DrawingElementQuadTree.Insert(elem, elem.BoundingRectangle());
                        }

                        if (_deleteSourceMeshElements == true)
                        {
                            DeleteAction deleteAction = new DeleteAction();

                            List<IDrawingElement> deletedElementList = new List<IDrawingElement>(SelectedElements);

                            actionsDictionary.Add(deleteAction, deletedElementList);

                            for (int i = 0; i < SelectedElements.Count; i++)
                            {
                                IDrawingElement elem = SelectedElements[i];

                                elem.Selected = false;

                                InstanceData.DrawingElementList.Remove(elem);

                                InstanceData.DrawingElementQuadTree.Remove(elem);
                            }

                            SelectedElements.Clear();
                        }

                        ActionOnElements action = new ActionOnElements(actionsDictionary);

                        AddActionToList(action);

                        ResetDrawingState();

                        CreateDraftingGraphics();
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(line) || string.IsNullOrWhiteSpace(line))
                {
                    //do nothing
                    _meshSizeSet = true;
                }
                else if (double.TryParse(line, out double meshSize) == true)
                {
                    _meshSize = meshSize;

                    _meshSizeSet = true;
                }
                
                if (_meshSizeSet == true)
                {
                    string yOrN = (_deleteSourcePolygons == true) ? "y" : "n";

                    InputTextBox.Text = $"Delete source elements? (y/n) [{yOrN}]:";

                    _readOnlyChars = InputTextBox.Text.Length;
                }
            }
        }

        private List<PolygonG> MeshUsingTriangleNet(List<IDrawingElement> elementList, double meshSize)
        {
            List<PrePolygon> preMesh = CreateTriangleNetMesh(elementList, meshSize, InstanceData.Precision);

            if (preMesh != null && preMesh.Count > 0)
            {
                List<PolygonG> mesh = Triangle.TriangleHelperMethods.ConvertFromPrePolygons(preMesh);

                for (int i = 0; i < mesh.Count; i++)
                {
                    mesh[i].Layer = InstanceData.Layers[0];
                }

                return mesh;
            }
            else
            {
                return new List<PolygonG>();
            }
        }
        
        private void DisplayTriangleMeshOptions()
        {
            _lastCommand = "tmo";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            TriangleMeshOptions frm = new TriangleMeshOptions(InstanceData.TriangleMeshingOptions);

            frm.ShowDialog();

            frm.Dispose();
        }

        private void MakeHole()
        {
            _lastCommand = "makehole";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            if (ElementsSelected() == true)
            {
                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    IDrawingElement elem = SelectedElements[i];

                    if (elem.GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                    {
                        ((ClosedDrawingElement)elem).Hole = true;
                    }
                }
            }
        }
        #endregion

        #region Property Panel
        private void SetPropertyPanelAccordianVisibility()
        {
            if (ElementsSelected() == false)
            {
                foreach (KeyValuePair<ElementPropertyType, CollapsiblePanel> kvp in _propertyPanelDictionary)
                {
                    kvp.Value.ShowPanel = false;
                }
            }
            else
            {
                Dictionary<ElementPropertyType, int> numElementsContainingProperty = new Dictionary<ElementPropertyType, int>();

                foreach (ElementPropertyType type in Enum.GetValues(typeof(ElementPropertyType)))
                {
                    numElementsContainingProperty.Add(type, 0);
                }

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    IDrawingElement elem = SelectedElements[i];

                    List<ElementPropertyType> elemProperties = elem.PropertyTypeList;

                    for (int j = 0; j < elemProperties.Count; j++)
                    {
                        numElementsContainingProperty[elemProperties[j]] += 1;
                    }
                }

                foreach (KeyValuePair<ElementPropertyType, int> kvp in numElementsContainingProperty)
                {
                    if (kvp.Value > 0)
                    {
                        _propertyPanelDictionary[kvp.Key].ShowPanel = true;

                        if (kvp.Value == SelectedElements.Count)
                        {
                            _propertyPanelDictionary[kvp.Key].ContentEnabled = true;
                        }
                        else
                        {
                            _propertyPanelDictionary[kvp.Key].ContentEnabled = false;
                        }
                    }
                    else
                    {
                        _propertyPanelDictionary[kvp.Key].ShowPanel = false;
                    }
                }
            }

            _propertyPanelAccordianList.DisplayListControls();

            UpdateAllPropertyPanels();
        }

        private void UpdateAllPropertyPanels()
        {
            UpdatePropertyPanel_General();
            UpdatePropertyPanel_ClosedGeometry();
            UpdatePorpertyPanel_Rebar();
            UpdatePropertyPanel_Nodes();
            UpdatePropertyPanel_Materials();
            UpdatePropertyPanel_PostTensioning();
            UpdatePropertyPanel_OpenGeometry();
            UpdatePropertyPanel_UserProperties();
        }

        private void UpdatePropertyPanel_General()
        {
            if (ElementsSelected() == true)
            {
                List<Layer> layersSelected = new List<Layer>();
                List<DrawingElementType> elementTypesSelected = new List<DrawingElementType>();

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    IDrawingElement elem = SelectedElements[i];

                    if (layersSelected.Contains(elem.Layer) == false)
                    {
                        layersSelected.Add(elem.Layer);
                    }

                    if (elementTypesSelected.Contains(elem.ElementType) == false)
                    {
                        elementTypesSelected.Add(elem.ElementType);
                    }
                }

                if (_propertyPanelControlContainer.GeneralControls.LayerComboBox != null)
                {
                    ComboBox layerComboBox = _propertyPanelControlContainer.GeneralControls.LayerComboBox;

                    if (layersSelected.Count > 1)
                    {
                        layerComboBox.SelectedIndex = -1;
                    }
                    else
                    {
                        Layer selectedLayer = layersSelected[0];

                        for (int i = 0; i < layerComboBox.Items.Count; i++)
                        {
                            if (string.Compare(selectedLayer.Name, layerComboBox.Items[i].ToString()) == 0)
                            {
                                layerComboBox.SelectedIndex = i;

                                break;
                            }
                        }
                    }
                }

                if (_propertyPanelControlContainer.GeneralControls.ElementNameTextBox != null)
                {
                    TextBox elementNameTextBox = _propertyPanelControlContainer.GeneralControls.ElementNameTextBox;

                    if (elementTypesSelected.Count > 1)
                    {
                        elementNameTextBox.Text = "Multiple Types";
                    }
                    else
                    {
                        if (elementTypesSelected.Count == 1)
                        {
                            elementNameTextBox.Text = elementTypesSelected[0].GetDescription();
                        }
                    }
                }
            }
        }

        private void UpdatePropertyPanel_ClosedGeometry()
        {
            if (ElementsSelected() == true)
            {
                //Geometry text boxes
                if (SelectedElements.Count == 1)
                {
                    if (SelectedElements[0].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == true)
                    {
                        ClosedDrawingElement elem = (ClosedDrawingElement)SelectedElements[0];

                        _propertyPanelControlContainer.ClosedGeometryControls.AreaTextBox.Text = Convert.ToString(Round(elem.Area(), InstanceData.DisplayPrecision));

                        PointD centroid = elem.Centroid();

                        _propertyPanelControlContainer.ClosedGeometryControls.CentroidTextBox.Text = Convert.ToString(Round(centroid.X, InstanceData.DisplayPrecision)) + ", " + Convert.ToString(Round(centroid.Y, InstanceData.DisplayPrecision));

                        double perimeter = elem.Perimeter();

                        _propertyPanelControlContainer.ClosedGeometryControls.PerimeterTextBox.Text = Convert.ToString(Round(perimeter, InstanceData.DisplayPrecision));

                        double ixx = elem.Ixx();

                        _propertyPanelControlContainer.ClosedGeometryControls.IxxTextBox.Text = Convert.ToString(Round(ixx, InstanceData.DisplayPrecision));

                        double iyy = elem.Iyy();

                        _propertyPanelControlContainer.ClosedGeometryControls.IyyTextBox.Text = Convert.ToString(Round(iyy, InstanceData.DisplayPrecision));

                        double ixy = elem.Ixy();

                        _propertyPanelControlContainer.ClosedGeometryControls.IxyTextBox.Text = Convert.ToString(Round(ixy, InstanceData.DisplayPrecision));

                        double sxx = elem.Sxx();

                        _propertyPanelControlContainer.ClosedGeometryControls.SxxTextBox.Text = Convert.ToString(Round(sxx, InstanceData.DisplayPrecision));

                        double syy = elem.Syy();

                        _propertyPanelControlContainer.ClosedGeometryControls.SyyTextBox.Text = Convert.ToString(Round(syy, InstanceData.DisplayPrecision));

                        CheckBox cB = _propertyPanelControlContainer.ClosedGeometryControls.TreatAsHoleCheckBox;

                        if (elem.Hole == true)
                        {
                            cB.CheckState = CheckState.Checked;
                        }
                        else
                        {
                            cB.CheckState = CheckState.Unchecked;
                        }
                    }
                }
                else
                {
                    bool allElementsSelectedClosed = true;
                    bool allHoles = true;
                    bool allSolid = true;

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        if (SelectedElements[i].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == false)
                        {
                            allElementsSelectedClosed = false;

                            break;
                        }
                        else
                        {
                            ClosedDrawingElement elem = (ClosedDrawingElement)SelectedElements[i];

                            if (elem.Hole == true)
                            {
                                allSolid = false;
                            }
                            else
                            {
                                allHoles = false;
                            }
                        }
                    }

                    CheckBox cB = _propertyPanelControlContainer.ClosedGeometryControls.TreatAsHoleCheckBox;

                    if (allElementsSelectedClosed == true)
                    {
                        if (cB.GetType().Equals(typeof(CheckBoxTS)) == true)
                        {
                            CheckBoxTS cBts = (CheckBoxTS)cB;

                            if (allHoles == true)
                            {
                                cBts.CheckState = CheckState.Checked;
                            }
                            else if (allSolid == true)
                            {
                                cBts.CheckState = CheckState.Unchecked;
                            }
                            else
                            {
                                cBts.CheckState = CheckState.Indeterminate;
                            }
                        }
                        else
                        {
                            if (allHoles == true)
                            {
                                cB.CheckState = CheckState.Checked;
                            }
                            else
                            {
                                cB.CheckState = CheckState.Unchecked;
                            }
                        }                      
                    }
                    else
                    {
                        cB.CheckState = CheckState.Unchecked;
                    }

                    _propertyPanelControlContainer.ClosedGeometryControls.AreaTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.PerimeterTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.CentroidTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.IxxTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.IyyTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.IxyTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.SxxTextBox.Text = string.Empty;
                    _propertyPanelControlContainer.ClosedGeometryControls.SyyTextBox.Text = string.Empty;
                }
            }
        }

        private void UpdatePropertyPanel_Materials()
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.Material];

                if (panel.ContentEnabled == true)
                {
                    //if the panel is enabled, all elements have the material property

                    Dictionary<IGenericMaterial, int> allSelectedMaterials = new Dictionary<IGenericMaterial, int>();

                    bool nullMaterial = false;

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.Material != null)
                        {
                            if (allSelectedMaterials.ContainsKey(elem.Material) == true)
                            {
                                allSelectedMaterials[elem.Material] += 1;
                            }
                            else
                            {
                                allSelectedMaterials.Add(elem.Material, 1);
                            }
                        }
                        else
                        {
                            nullMaterial = true;
                        }
                    }

                    ComboBox materialComboBox = _propertyPanelControlContainer.MaterialControls.MaterialComboBox;

                    if (allSelectedMaterials.Count == 1 && nullMaterial == false)
                    {
                        IGenericMaterial selMat = allSelectedMaterials.Keys.ToList()[0];

                        if (selMat == null)
                        {
                            materialComboBox.SelectedIndex = -1;
                        }
                        else
                        {
                            int ind = -1;

                            for (int i = 0; i < materialComboBox.Items.Count; i++)
                            {
                                string s = Convert.ToString(materialComboBox.Items[i]);

                                if (string.Compare(s, selMat.Name) == 0)
                                {
                                    ind = i;

                                    break;
                                }
                            }

                            materialComboBox.SelectedIndex = ind;
                        }
                    }
                    else
                    {
                        materialComboBox.SelectedIndex = -1;
                    }
                }
            }
        }

        private void UpdatePorpertyPanel_Rebar()
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.Rebar];

                if (panel.Enabled == true)
                {
                    //the panel is only enabled if all elements have the rebar property
                    Dictionary<RebarDefinition, int> allSelectedBarDefinitions = new Dictionary<RebarDefinition, int>();

                    bool allRebar = true;
                    bool allNotRebar = true;
                    bool nullRebar = false;

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.IsRebar == false)
                        {
                            allRebar = false;
                        }

                        if (elem.IsRebar == true)
                        {
                            allNotRebar = false;
                        }

                        if (elem.Rebar != null)
                        {
                            if (allSelectedBarDefinitions.ContainsKey(elem.Rebar) == true)
                            {
                                allSelectedBarDefinitions[elem.Rebar] += 1;
                            }
                            else
                            {
                                allSelectedBarDefinitions.Add(elem.Rebar, 1);
                            }
                        }
                        else
                        {
                            nullRebar = true;
                        }
                    }

                    ComboBox rebarSizeComboBox = _propertyPanelControlContainer.RebarControls.RebarSizeComboBox;

                    if (allSelectedBarDefinitions.Count == 1 && nullRebar == false)
                    {
                        RebarDefinition selDef = allSelectedBarDefinitions.Keys.ToList()[0];
                        
                        if (selDef == null)
                        {
                            rebarSizeComboBox.SelectedIndex = -1;
                        }
                        else
                        {
                            int ind = -1;

                            for (int i = 0; i < rebarSizeComboBox.Items.Count; i++)
                            {
                                string s = Convert.ToString(rebarSizeComboBox.Items[i]);

                                if (string.Compare(s, selDef.Name) == 0)
                                {
                                    ind = i;

                                    break;
                                }
                            }

                            rebarSizeComboBox.SelectedIndex = ind;
                        }
                    }
                    else
                    {
                        rebarSizeComboBox.SelectedIndex = -1;
                    }

                    if (allRebar == true)
                    {
                        _propertyPanelControlContainer.RebarControls.TreatAsRebarCheckBox.CheckState = CheckState.Checked;
                    }
                    else if (allNotRebar == true)
                    {
                        _propertyPanelControlContainer.RebarControls.TreatAsRebarCheckBox.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        _propertyPanelControlContainer.RebarControls.TreatAsRebarCheckBox.CheckState = CheckState.Indeterminate;
                    }
                }
            }
        }

        private void UpdatePropertyPanel_Nodes()
        {
            if (ElementsSelected() == true)
            {
                if (SelectedElements.Count == 1)
                {
                    List<DrawingNode> nodes = SelectedElements[0].Nodes;

                    StringBuilder sB = new StringBuilder();

                    for (int i = 0; i < nodes.Count; i++)
                    {
                        DrawingNode n = nodes[i];

                        string nodeType = n.TypeOfNode.GetDescription();

                        string pointString = "[" + Convert.ToString(Round(n.Point.X, InstanceData.DisplayPrecision)) + ", " + Convert.ToString(Round(n.Point.Y, InstanceData.DisplayPrecision)) + "]";

                        sB.AppendLine(nodeType + " = " + pointString);
                    }

                    _propertyPanelControlContainer.NodesControls.NodesRichTextBox.Text = sB.ToString();
                }
                else
                {
                    _propertyPanelControlContainer.NodesControls.NodesRichTextBox.Clear();
                }
            }
            else
            {
                _propertyPanelControlContainer.NodesControls.NodesRichTextBox.Clear();
            }
        }

        private void UpdatePropertyPanel_PostTensioning()
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.PostTensioning];

                if (panel.ContentEnabled == true)
                {
                    //if the panel is enabled, all elements have the post tensioning property

                    Dictionary<PostTensioningDefinition, int> allSelectedPT = new Dictionary<PostTensioningDefinition, int>();
                    Dictionary<JackingAndGroutingType, int> allSelectedJaG = new Dictionary<JackingAndGroutingType, int>();
                    Dictionary<double, int> allSelectedPrestress = new Dictionary<double, int>();

                    bool nullPT = false;

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        IDrawingElement elem = SelectedElements[i];

                        if (elem.GetType().Equals(typeof(PostTensioningG)) == true)
                        {
                            PostTensioningG ptG = (PostTensioningG)elem;

                            if (ptG.TypeOfPT != null)
                            {
                                if (allSelectedPT.ContainsKey(ptG.TypeOfPT) == true)
                                {
                                    allSelectedPT[ptG.TypeOfPT] += 1;
                                }
                                else
                                {
                                    allSelectedPT.Add(ptG.TypeOfPT, 1);
                                }
                            }
                            else
                            {
                                nullPT = true;
                            }

                            if (allSelectedJaG.ContainsKey(ptG.JackingAndGrouting) == true)
                            {
                                allSelectedJaG[ptG.JackingAndGrouting] += 1;
                            }
                            else
                            {
                                allSelectedJaG.Add(ptG.JackingAndGrouting, 1);
                            }

                            if (allSelectedPrestress.ContainsKey(ptG.InitialPrestress) == true)
                            {
                                allSelectedPrestress[ptG.InitialPrestress] += 1;
                            }
                            else
                            {
                                allSelectedPrestress.Add(ptG.InitialPrestress, 1);
                            }
                        }
                    }

                    #region Post Tensioning Type Combo Box
                    ComboBox ptDefComboBox = _propertyPanelControlContainer.PostTensioningControls.PostTensioningTypeComboBox;

                    if (allSelectedPT.Count == 1 && nullPT == false)
                    {
                        PostTensioningDefinition selPT = allSelectedPT.Keys.ToList()[0];

                        if (selPT == null)
                        {
                            ptDefComboBox.SelectedIndex = -1;
                        }
                        else
                        {
                            int ind = -1;

                            for (int i = 0; i < ptDefComboBox.Items.Count; i++)
                            {
                                string s = Convert.ToString(ptDefComboBox.Items[i]);

                                if (string.Compare(s, selPT.Name) == 0)
                                {
                                    ind = i;

                                    break;
                                }
                            }

                            ptDefComboBox.SelectedIndex = ind;
                        }
                    }
                    else
                    {
                        ptDefComboBox.SelectedIndex = -1;
                    }

                    #endregion

                    #region Jacking and Grouting Combo Box
                    ComboBox jagComboBox = _propertyPanelControlContainer.PostTensioningControls.JackingAndGroutingComboBox;

                    if (allSelectedJaG.Count == 1)
                    {
                        JackingAndGroutingType jagType = allSelectedJaG.Keys.ToList()[0];

                        int ind = -1;

                        for (int i = 0; i < jagComboBox.Items.Count; i++)
                        {
                            string s = Convert.ToString(jagComboBox.Items[i]);

                            if (string.Compare(s, jagType.GetDescription()) == 0)
                            {
                                ind = i;

                                break;
                            }
                        }

                        jagComboBox.SelectedIndex = ind;
                    }
                    else
                    {
                        jagComboBox.SelectedIndex = -1;
                    }
                    #endregion

                    #region Initial Prestress Text Box
                    TextBox prestressTextBox = _propertyPanelControlContainer.PostTensioningControls.PretensionTextBox;

                    if (allSelectedPrestress.Count == 1)
                    {
                        double prestress = allSelectedPrestress.Keys.ToList()[0];

                        prestressTextBox.Text = Convert.ToString(prestress);
                    }
                    else
                    {
                        prestressTextBox.Clear();
                    }
                    #endregion
                }
            }
        }

        private void UpdatePropertyPanel_OpenGeometry()
        {
            if (ElementsSelected() == true)
            {
                //Geometry text boxes
                if (SelectedElements.Count == 1)
                {
                    if (SelectedElements[0].GetType().IsSubclassOf(typeof(OpenDrawingElement)) == true)
                    {
                        OpenDrawingElement elem = (OpenDrawingElement)SelectedElements[0];

                        _propertyPanelControlContainer.OpenGeometryControls.LengthTextBox.Text = Convert.ToString(Round(elem.Length(), InstanceData.DisplayPrecision));
                    }
                }
                else
                {
                    _propertyPanelControlContainer.OpenGeometryControls.LengthTextBox.Clear();
                }
            }
        }

        private void UpdatePropertyPanel_UserProperties()
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.UserProperties];

                if (panel.ContentEnabled == true)
                {
                    panel.ContentEnabled = false;   //Set to false to prevent objects updating on event firing
                                                    //Events will fire because datagrid cells changed


                    DataGridView dgv = _propertyPanelControlContainer.UserPropertyControls.DataGrid;

                    dgv.Rows.Clear();

                    Dictionary<string, Dictionary<UserPropertyType, List<object>>> propDictionary = new Dictionary<string, Dictionary<UserPropertyType, List<object>>>();

                    for (int i = 0; i < SelectedElements.Count; i++)
                    {
                        if (SelectedElements[i].UserProperties != null)
                        {
                            for (int j = 0; j < SelectedElements[i].UserProperties.Count; j++)
                            {
                                string propName = SelectedElements[i].UserProperties[j].PropertyName;
                                UserPropertyType propType = SelectedElements[i].UserProperties[j].PropertyType;
                                object propVal = SelectedElements[i].UserProperties[j].PropertyValue;

                                if (propDictionary.TryGetValue(propName, out Dictionary<UserPropertyType, List<object>> typeDictionary) == true)
                                {
                                    if (typeDictionary.TryGetValue(propType, out List<object> props) == true)
                                    {
                                        props.Add(propVal);
                                    }
                                    else
                                    {
                                        typeDictionary.Add(propType, new List<object>() { propVal });
                                    }
                                }
                                else
                                {
                                    Dictionary<UserPropertyType, List<object>> newTypeDictionary = new Dictionary<UserPropertyType, List<object>>();

                                    newTypeDictionary.Add(propType, new List<object>() { propVal });

                                    propDictionary.Add(propName, newTypeDictionary);
                                }
                            }
                        }
                    }

                    foreach (KeyValuePair<string, Dictionary<UserPropertyType, List<object>>> kvp1 in propDictionary)
                    {
                        if (kvp1.Value.Count == 1)
                        {
                            //if there is only one type of property associated with this name
                            foreach (KeyValuePair<UserPropertyType, List<object>> kvp2 in kvp1.Value)
                            {
                                if (kvp2.Value.Count == SelectedElements.Count)
                                {
                                    //Only add to the panel if all selected objects have the property defined

                                    dgv.Rows.Add();
                                    int lastRow = dgv.Rows.Count - 1;

                                    dgv[0, lastRow].Value = kvp1.Key;

                                    if (kvp2.Key.Equals(UserPropertyType.Boolean) == true)
                                    {
                                        DataGridViewCheckBoxCell cell = new DataGridViewCheckBoxCell();
                                        cell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                        cell.ThreeState = true;
                                        cell.IndeterminateValue = CheckState.Indeterminate;
                                        cell.TrueValue = CheckState.Checked;
                                        cell.FalseValue = CheckState.Unchecked;
                                        cell.Style.NullValue = CheckState.Indeterminate;

                                        dgv[1, lastRow] = cell;

                                        cell.ReadOnly = true;

                                        bool same = true;

                                        bool initVal = (bool)kvp2.Value[0];

                                        for (int i = 0; i < kvp2.Value.Count; i++)
                                        {
                                            if ((bool)kvp2.Value[i] != initVal)
                                            {
                                                same = false;
                                                break;
                                            }
                                        }

                                        if (same == true)
                                        {
                                            if (initVal == true)
                                            {
                                                cell.Value = CheckState.Checked;
                                            }
                                            else
                                            {
                                                cell.Value = CheckState.Unchecked;
                                            }
                                        }
                                        else
                                        {
                                            cell.Value = CheckState.Indeterminate;
                                        }
                                    }
                                    else if (kvp2.Key.Equals(UserPropertyType.Color) == true)
                                    {
                                        DataGridViewButtonCell cell = new DataGridViewButtonCell();
                                        cell.FlatStyle = FlatStyle.Popup;

                                        dgv[1, lastRow] = cell;
                                        dgv[1, lastRow].ValueType = typeof(Color);

                                        bool same = true;

                                        Color initVal = (Color)kvp2.Value[0];

                                        for (int i = 0; i < kvp2.Value.Count; i++)
                                        {
                                            Color c = (Color)kvp2.Value[i];

                                            if (c.R != initVal.R || c.G != initVal.G || c.B != initVal.B || c.A != initVal.A)
                                            {
                                                same = false;
                                                break;
                                            }
                                        }

                                        if (same == true)
                                        {
                                            dgv[1, lastRow].Style.BackColor = initVal;
                                        }
                                        else
                                        {
                                            dgv[1, lastRow].Style.BackColor = Color.Empty;
                                        }
                                    }
                                    else if (kvp2.Key.Equals(UserPropertyType.Text) == true)
                                    {
                                        dgv[1, lastRow].ValueType = typeof(string);

                                        bool same = true;

                                        string initVal = (string)kvp2.Value[0];

                                        for (int i = 0; i < kvp2.Value.Count; i++)
                                        {
                                            if (string.Compare(initVal, Convert.ToString(kvp2.Value[i]), ignoreCase: true) != 0)
                                            {
                                                same = false;
                                                break;
                                            }
                                        }

                                        if (same == true)
                                        {
                                            dgv[1, lastRow].Value = initVal;
                                        }
                                        else
                                        {
                                            dgv[1, lastRow].Value = string.Empty;
                                        }
                                    }
                                    else if (kvp2.Key.Equals(UserPropertyType.Number) == true)
                                    {
                                        dgv[1, lastRow].ValueType = typeof(double);

                                        bool same = true;

                                        double initVal = (double)kvp2.Value[0];

                                        for (int i = 0; i < kvp2.Value.Count; i++)
                                        {
                                            if (initVal != (double)kvp2.Value[i])
                                            {
                                                same = false;
                                                break;
                                            }
                                        }

                                        if (same == true)
                                        {
                                            dgv[1, lastRow].Value = initVal;
                                        }
                                        else
                                        {
                                            dgv[1, lastRow].Value = string.Empty;
                                        }
                                    }
                                }
                                
                            }
                        }
                    }

                    panel.ContentEnabled = true;
                }
            }
        }

        private void PropertyPanelLayersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox layersComboBox = (ComboBox)sender;

            if (layersComboBox.SelectedIndex >= 0)
            {
                string layerName = layersComboBox.Text;

                Layer selectedLayer = InstanceData.GetLayerFromName(layerName);

                if (selectedLayer != null)
                {
                    if (ElementsSelected() == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            SelectedElements[i].Layer = selectedLayer;
                        }

                        CreateDraftingGraphics();
                    }
                }
            }
        }

        private void PropertyPanelMaterialsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox materialsComboBox = (ComboBox)sender;

            if (materialsComboBox.SelectedIndex >= 0)
            {
                string matName = materialsComboBox.Text;

                IGenericMaterial selectedMaterial = InstanceData.GetMaterialFromName(matName);

                if (selectedMaterial != null)
                {
                    if (ElementsSelected() == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            if (SelectedElements[i].PropertyTypeList.Contains(ElementPropertyType.Material) == true)
                            {
                                SelectedElements[i].Material = selectedMaterial;
                            }
                        }
                    }
                }
            }
        }

        private void PropertyPanelHoleCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            if (ElementsSelected() == true)
            {
                bool allElementsSelectedClosed = true;

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    if (SelectedElements[i].GetType().IsSubclassOf(typeof(ClosedDrawingElement)) == false)
                    {
                        allElementsSelectedClosed = false;

                        break;
                    }
                }

                if (allElementsSelectedClosed == true)
                {
                    if (cB.CheckState.Equals(CheckState.Checked) == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            ClosedDrawingElement elem = (ClosedDrawingElement)SelectedElements[i];

                            elem.Hole = true;
                        }
                    }
                    else if (cB.CheckState.Equals(CheckState.Unchecked) == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            ClosedDrawingElement elem = (ClosedDrawingElement)SelectedElements[i];

                            elem.Hole = false;
                        }
                    }
                }
            }
        }

        private void PropertyPanelTreatAsRebarCheckBox_CheckStateChanged(object sender, EventArgs e)
        {
            CheckBox cB = (CheckBox)sender;

            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.Rebar];

                if (panel.ContentEnabled == true)
                {
                    if (cB.CheckState.Equals(CheckState.Checked) == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            SelectedElements[i].IsRebar = true;
                        }
                    }
                    else if (cB.CheckState.Equals(CheckState.Unchecked) == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            SelectedElements[i].IsRebar = false;
                        }
                    }

                    CreateDraftingGraphics();
                }
            }
        }

        private void PropertyPanelRebarSizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ElementsSelected() == true)
            {
                ComboBox cB = (ComboBox)sender;

                if (cB.SelectedIndex >= 0)
                {
                    string barName = cB.Text;

                    RebarDefinition barDef = InstanceData.GetRebarDefinitionFromName(barName);

                    if (barDef != null)
                    {
                        bool sizeChanged = false;

                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            IDrawingElement elem = SelectedElements[i];

                            if (elem.PropertyTypeList.Contains(ElementPropertyType.Rebar) == true)
                            {
                                if (elem.GetType().Equals(typeof(RebarSectionG)) == true)
                                {
                                    RebarSectionG rbsG = (RebarSectionG)elem;

                                    if (rbsG.Rebar.Equals(barDef) == false)
                                    {
                                        rbsG.Rebar = barDef;

                                        InstanceData.DrawingElementQuadTree.Remove(rbsG);

                                        InstanceData.DrawingElementQuadTree.Insert(rbsG, rbsG.BoundingRectangle());

                                        sizeChanged = true;
                                    }
                                }
                                else
                                {
                                    if (elem.Rebar == null || elem.Rebar.Equals(barDef) == false)
                                    {
                                        elem.Rebar = barDef;

                                        sizeChanged = true;
                                    }
                                }
                            }
                        }

                        if (sizeChanged == true)
                        {
                            CreateDraftingGraphics();
                        }
                    }
                }
            }
        }

        private void PropertyPanelPostTensioningDefinitionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.PostTensioning];

                if (panel.ContentEnabled == true)
                {
                    ComboBox cB = (ComboBox)sender;

                    if (cB.SelectedIndex >= 0)
                    {
                        string ptName = cB.Text;

                        PostTensioningDefinition ptDef = InstanceData.GetPostTensioningDefinitionFromName(ptName);

                        if (ptDef != null)
                        {
                            for (int i = 0; i < SelectedElements.Count; i++)
                            {
                                IDrawingElement elem = SelectedElements[i];

                                if (elem.GetType().Equals(typeof(PostTensioningG)) == true)
                                {
                                    PostTensioningG ptG = (PostTensioningG)elem;

                                    if (ptG.TypeOfPT.Equals(ptDef) == false)
                                    {
                                        ptG.TypeOfPT = ptDef;

                                        InstanceData.DrawingElementQuadTree.Remove(ptG);

                                        InstanceData.DrawingElementQuadTree.Insert(ptG, ptG.BoundingRectangle());
                                    }

                                }                               
                            }

                            CreateDraftingGraphics();
                        }
                    }
                }
            }
            
        }

        private void PropertyPanelJackingAndGroutingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ElementsSelected() == true)
            {
                CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.PostTensioning];

                if (panel.ContentEnabled == true)
                {
                    ComboBox cB = (ComboBox)sender;

                    if (cB.SelectedIndex >= 0)
                    {
                        string jackingAndGroutingName = cB.Text;

                        JackingAndGroutingType jagType = jackingAndGroutingName.GetEnumFromDescription<JackingAndGroutingType>();

                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            IDrawingElement elem = SelectedElements[i];

                            if (elem.GetType().Equals(typeof(PostTensioningG)) == true)
                            {
                                PostTensioningG ptG = (PostTensioningG)elem;

                                ptG.JackingAndGrouting = jagType;
                            }
                        }
                    }
                }
            }
        }

        private void PropertyPanelInitialPrestressTextBox_TextChanged(object sender, EventArgs e)
        {
            TextBox tB = (TextBox)sender;

            CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.PostTensioning];

            if (panel.ContentEnabled == true)
            {
                if (ElementsSelected() == true)
                {
                    if (double.TryParse(tB.Text, out double prestress) == true)
                    {
                        for (int i = 0; i < SelectedElements.Count; i++)
                        {
                            IDrawingElement elem = SelectedElements[i];

                            if (elem.GetType().Equals(typeof(PostTensioningG)) == true)
                            {
                                PostTensioningG ptG = (PostTensioningG)elem;

                                ptG.InitialPrestress = prestress;
                            }
                        }
                    }
                }
            }           
        }

        private void PropertyPanelUserPropertyDGV_CellContentChanged(object sender, DataGridViewCellEventArgs e)
        {
            CollapsiblePanel panel = _propertyPanelDictionary[ElementPropertyType.UserProperties];

            if (ElementsSelected() == true)
            {
                int colIndex = e.ColumnIndex;
                int rowIndex = e.RowIndex;

                if (colIndex == 1)
                {
                    if (panel.ContentEnabled == true)
                    {
                        try
                        {
                            bool showWarning = true;
                            bool overwritePropertyType = true;

                            DataGridView dgv = _propertyPanelControlContainer.UserPropertyControls.DataGrid;

                            string propName = Convert.ToString(dgv[0, rowIndex].Value);

                            for (int i = 0; i < SelectedElements.Count; i++)
                            {
                                bool exists = false;

                                ObservableList<UserProperty> userProps = SelectedElements[i].UserProperties;

                                int ind = -1;

                                if (userProps != null)
                                {
                                    for (int j = 0; j < userProps.Count; j++)
                                    {
                                        if (string.Compare(userProps[j].PropertyName, propName, ignoreCase: true) == 0)
                                        {
                                            exists = true;

                                            ind = j;

                                            break;
                                        }
                                    }
                                }

                                if (exists == false)
                                {
                                    if (userProps == null)
                                    {
                                        SelectedElements[i].UserProperties = new ObservableList<UserProperty>();

                                        userProps = SelectedElements[i].UserProperties;
                                    }
                                }

                                if (dgv[colIndex, rowIndex].GetType().Equals(typeof(DataGridViewCheckBoxCell)) == true)
                                {
                                    bool val = Convert.ToBoolean((int)dgv[colIndex, rowIndex].Value);

                                    if (exists == true)
                                    {
                                        if (userProps[ind].PropertyType.Equals(UserPropertyType.Boolean) == true)
                                        {
                                            userProps[ind].PropertyValue = val;
                                        }
                                        else
                                        {
                                            if (showWarning == true)
                                            {
                                                OverwritePropertyForm frm = new OverwritePropertyForm();

                                                if (frm.ShowDialog() == DialogResult.Yes)
                                                {
                                                    overwritePropertyType = true;
                                                }
                                                else
                                                {
                                                    overwritePropertyType = false;
                                                }

                                                showWarning = !frm.UseSameResponse;

                                                frm.Dispose();
                                            }

                                            if (overwritePropertyType == true)
                                            {
                                                userProps[ind] = new UserPropertyBoolean(propName, val);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        userProps.Add(new UserPropertyBoolean(Convert.ToString(dgv[0, rowIndex].Value), val));
                                    }
                                }
                                else if (dgv[colIndex, rowIndex].GetType().Equals(typeof(DataGridViewButtonCell)) == true)
                                {
                                    Color val = dgv[colIndex, rowIndex].Style.BackColor;

                                    if (exists == true)
                                    {
                                        if (userProps[ind].PropertyType.Equals(UserPropertyType.Color) == true)
                                        {
                                            userProps[ind].PropertyValue = val;
                                        }
                                        else
                                        {
                                            if (showWarning == true)
                                            {
                                                OverwritePropertyForm frm = new OverwritePropertyForm();

                                                if (frm.ShowDialog() == DialogResult.Yes)
                                                {
                                                    overwritePropertyType = true;
                                                }
                                                else
                                                {
                                                    overwritePropertyType = false;
                                                }

                                                showWarning = !frm.UseSameResponse;

                                                frm.Dispose();
                                            }

                                            if (overwritePropertyType == true)
                                            {
                                                userProps[ind] = new UserPropertyColor(propName, val);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        userProps.Add(new UserPropertyColor(Convert.ToString(dgv[0, rowIndex].Value), val));
                                    }
                                }
                                else if (dgv[colIndex, rowIndex].ValueType.Equals(typeof(string)) == true)
                                {
                                    string val = Convert.ToString(dgv[colIndex, rowIndex].Value);

                                    DataGridViewCell cell = dgv[colIndex, rowIndex];

                                    if (exists == true)
                                    {
                                        if (userProps[ind].PropertyType.Equals(UserPropertyType.Text) == true)
                                        {
                                            userProps[ind].PropertyValue = val;
                                        }
                                        else
                                        {
                                            if (showWarning == true)
                                            {
                                                OverwritePropertyForm frm = new OverwritePropertyForm();

                                                if (frm.ShowDialog() == DialogResult.Yes)
                                                {
                                                    overwritePropertyType = true;
                                                }
                                                else
                                                {
                                                    overwritePropertyType = false;
                                                }

                                                showWarning = !frm.UseSameResponse;

                                                frm.Dispose();
                                            }

                                            if (overwritePropertyType == true)
                                            {
                                                userProps[ind] = new UserPropertyText(propName, val);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        userProps.Add(new UserPropertyText(Convert.ToString(dgv[0, rowIndex].Value), val));
                                    }
                                }
                                else if (dgv[colIndex, rowIndex].ValueType.Equals(typeof(double)) == true)
                                {
                                    if (double.TryParse(Convert.ToString(dgv[colIndex, rowIndex].Value), out double val) == true)
                                    {
                                        if (exists == true)
                                        {
                                            if (userProps[ind].PropertyType.Equals(UserPropertyType.Number) == true)
                                            {
                                                userProps[ind].PropertyValue = val;
                                            }
                                            else
                                            {
                                                if (showWarning == true)
                                                {
                                                    OverwritePropertyForm frm = new OverwritePropertyForm();

                                                    if (frm.ShowDialog() == DialogResult.Yes)
                                                    {
                                                        overwritePropertyType = true;
                                                    }
                                                    else
                                                    {
                                                        overwritePropertyType = false;
                                                    }

                                                    showWarning = !frm.UseSameResponse;

                                                    frm.Dispose();
                                                }

                                                if (overwritePropertyType == true)
                                                {
                                                    userProps[ind] = new UserPropertyNumber(propName, val);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            userProps.Add(new UserPropertyNumber(Convert.ToString(dgv[0, rowIndex].Value), val));
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception($"User property value for property '{propName}' must be numeric.");
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("There was an error assigning a value to the user property:" + Environment.NewLine + Environment.NewLine + ex.Message);
                        }
                    }

                    UpdatePropertyPanel_UserProperties();
                }
            }           
        }

        private void PropertyPanelUserPropertyDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            var dgv = (DataGridView)sender;

            var cell = dgv[e.ColumnIndex, e.RowIndex];

            if (cell.GetType().Equals(typeof(DataGridViewCheckBoxCell)) == true)
            {
                DataGridViewCheckBoxCell cbCell = (DataGridViewCheckBoxCell)cell;

                if (cbCell.ThreeState == true)
                {
                    if (cbCell.Value.Equals(CheckState.Checked) == true)
                    {
                        cbCell.Value = CheckState.Unchecked;
                    }
                    else if (cbCell.Value.Equals(CheckState.Unchecked) == true)
                    {
                        cbCell.Value = CheckState.Checked;
                    }
                    else if (cbCell.Value.Equals(CheckState.Indeterminate) == true)
                    {
                        cbCell.Value = CheckState.Checked;
                    }
                }
                else
                {
                    if (cbCell.Value.Equals(CheckState.Checked) == true)
                    {
                        cbCell.Value = CheckState.Unchecked;
                    }
                    else if (cbCell.Value.Equals(CheckState.Unchecked) == true)
                    {
                        cbCell.Value = CheckState.Checked;
                    }
                }
            }
        }
        #endregion

        #region Paint Overrides
        public bool TransparentBackground { get; set; } = false;

        public bool ClipChildControls { get; set; } = true;

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            if (TransparentBackground == true)
            {
                base.OnPaintBackground(pevent);
            }
            else
            {
                if (ClipChildControls == false)
                {
                    base.OnPaintBackground(pevent);
                }
                else
                {
                    if (BackgroundImage != null)
                    {
                        base.OnPaintBackground(pevent);
                    }
                    else
                    {
                        Contract.Requires(pevent != null);
                        // We need the true client rectangle as clip rectangle causes
                        // problems on "Windows Classic" theme.  
                        Rectangle rect = this.ClientRectangle;
                        PaintBackgroundNoChildren(pevent, rect, BackColor);
                    }
                }
            }
        }

        protected void PaintBackgroundNoChildren(PaintEventArgs e, Rectangle rectangle, Color backColor)
        {
            List<Rectangle> clippedRectangles = ChildContrlsClientRectangles();

            PaintClippedBackground(e, rectangle, clippedRectangles, backColor);
        }

        protected void PaintClippedBackground(PaintEventArgs e, Rectangle clientRectangle, List<Rectangle> clippedRectangles, Color backColor)
        {
            Color color = backColor;

            Region r = new Region(clientRectangle);

            foreach (Rectangle hole in clippedRectangles)
            {
                r.Exclude(hole);
            }

            using (Brush brush = new SolidBrush(color))
            {
                foreach (Rectangle hole in clippedRectangles)
                {
                    e.Graphics.ExcludeClip(hole);
                }

                e.Graphics.FillRectangle(brush, clientRectangle);
            }

            r.Dispose();
        }

        protected List<Rectangle> ChildContrlsClientRectangles()
        {
            List<Rectangle> clientRectangles = new List<Rectangle>();

            foreach (Control c in Controls)
            {
                clientRectangles.Add(c.Bounds);
            }

            return clientRectangles;
        }
        #endregion

        #region Common Methods
        private bool ElementsSelected()
        {
            if (SelectedElements != null && SelectedElements.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Rebar Properties Dialog
        private void ShowRebarPropertiesDialog()
        {
            _lastCommand = "rbp";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            List<ComboBox> rebarComboBoxes = new List<ComboBox>();

            List<Control> allChildControls = this.GetAllChildControls();

            for (int i = 0; i < allChildControls.Count; i++)
            {
                if (allChildControls[i].Name.Contains("rebar") == true || allChildControls[i].Name.Contains("Rebar") == true)
                {
                    if (allChildControls[i].GetType().Equals(typeof(ComboBox)) == true)
                    {
                        rebarComboBoxes.Add((ComboBox)allChildControls[i]);
                    }
                }
            }

            Dictionary<ComboBox, RebarDefinition> selectedRebars = new Dictionary<ComboBox, RebarDefinition>();

            for (int i = 0; i < rebarComboBoxes.Count; i++)
            {
                if (string.IsNullOrEmpty(rebarComboBoxes[i].Text) == false)
                {
                    selectedRebars.Add(rebarComboBoxes[i], InstanceData.AvailableRebarSizes[rebarComboBoxes[i].Text]);
                }
            }

            RebarSizeEditorForm frm = new RebarSizeEditorForm(InstanceData.AvailableRebarSizes.Values.ToList(), InstanceData.DefaultRebarSizes.Values.ToList());

            if (frm.ShowDialog() == DialogResult.OK)
            {
                //Update the rebar dictionary
                InstanceData.AvailableRebarSizes.Clear();

                for (int i = 0; i < frm.TempRebarSizes.Count; i++)
                {
                    InstanceData.AvailableRebarSizes.Add(frm.TempRebarSizes[i].Name, frm.TempRebarSizes[i]);
                }

                foreach (KeyValuePair<ComboBox, RebarDefinition> kvp in selectedRebars)
                {
                    kvp.Key.Items.Clear();

                    int selIndex = -1;

                    int count = 0;

                    foreach (KeyValuePair<string, RebarDefinition> kvpDef in InstanceData.AvailableRebarSizes)
                    {
                        kvp.Key.Items.Add(kvpDef.Key);

                        if (kvpDef.Value == kvp.Value)
                        {
                            selIndex = count;
                        }

                        count++;
                    }

                    kvp.Key.SelectedIndex = selIndex;
                }

                CreateDraftingGraphics();
            }

            frm.Dispose();
        }

        #endregion

        #region Post Tensioning Properties Dialog
        private void ShowPostTensioningPropertiesDialog()
        {
            _lastCommand = "ptp";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            List<ComboBox> ptComboBoxes = new List<ComboBox>();

            List<Control> allChildControls = this.GetAllChildControls();

            for (int i = 0; i < allChildControls.Count; i++)
            {
                if (allChildControls[i].Name.Contains("Post Tensioning") == true || allChildControls[i].Name.Contains("PostTensioning") == true || allChildControls[i].Name.Contains("posttensioning") == true)
                {
                    if (allChildControls[i].GetType().Equals(typeof(ComboBox)) == true)
                    {
                        ptComboBoxes.Add((ComboBox)allChildControls[i]);
                    }
                }
            }

            Dictionary<ComboBox, PostTensioningDefinition> selectedPT = new Dictionary<ComboBox, PostTensioningDefinition>();

            for (int i = 0; i < ptComboBoxes.Count; i++)
            {
                if (string.IsNullOrEmpty(ptComboBoxes[i].Text) == false)
                {
                    selectedPT.Add(ptComboBoxes[i], InstanceData.PostTensioningDefinitionList[ptComboBoxes[i].SelectedIndex]);
                }
                else
                {
                    selectedPT.Add(ptComboBoxes[i], null);
                }
            }

            PostTensioningPropertiesForm frm = new PostTensioningPropertiesForm(InstanceData.PostTensioningDefinitionList, InstanceData.PostTensioningInnerSectionList, InstanceData.MaterialList, InstanceData.DrawingElementList);

            frm.ShowDialog();

            frm.Dispose();

            foreach (KeyValuePair<ComboBox, PostTensioningDefinition> kvp in selectedPT)
            {
                kvp.Key.Items.Clear();

                int selIndex = -1;

                int count = 0;

                for (int i = 0; i < InstanceData.PostTensioningDefinitionList.Count; i++)
                {
                    PostTensioningDefinition ptDef = InstanceData.PostTensioningDefinitionList[i];

                    kvp.Key.Items.Add(ptDef.Name);

                    if (ptDef.Equals(kvp.Value))
                    {
                        selIndex = count;
                    }

                    count++;
                }

                kvp.Key.SelectedIndex = selIndex;
            }

            CreateDraftingGraphics();
        }
        #endregion

        #region Right Click Actions
        private void FilterSelectedObjects()
        {
            if (ElementsSelected() == true)
            {
                List<Type> elementTypesSelected = new List<Type>();

                for (int i = 0; i < SelectedElements.Count; i++)
                {
                    Type elemType = SelectedElements[i].GetType();

                    if (elementTypesSelected.Contains(elemType) == false)
                    {
                        elementTypesSelected.Add(elemType);
                    }
                }

                SelectionFilterForm frm = new SelectionFilterForm(elementTypesSelected);

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    List<Type> filteredTypes = frm.FilteredTypes;

                    int i = 0;
                    int numSelected = SelectedElements.Count;

                    while (i < numSelected)
                    {
                        if (filteredTypes.Contains(SelectedElements[i].GetType()) == false)
                        {
                            SelectedElements[i].Selected = false;

                            SelectedElements.RemoveAt(i);

                            i--;

                            numSelected--;
                        }

                        i++;
                    }

                    CreateDraftingGraphics();

                    SetPropertyPanelAccordianVisibility();
                }
            }
        }

        private void BringSelectedObjectsToFront()
        {
            if (ElementsSelected() == true)
            {
                InstanceData.DrawingElementList.BringToFront(SelectedElements);

                ClearSelectedElements();
            }
        }

        private void SendSelectedObjectsToBack()
        {
            if (ElementsSelected() == true)
            {
                InstanceData.DrawingElementList.SendToBack(SelectedElements);

                ClearSelectedElements();
            }
        }
        #endregion

        #region FiberSectionAnalysis
        private void ShowAxialForceMomentDialog()
        {
            if (ElementsSelected() == true)
            {
                _lastCommand = "pm";

                AxialForceMomentAnalysisForm frm = new AxialForceMomentAnalysisForm(InstanceData.AxialForceMomentAnalysisCases, SelectedElements.ToList(), InstanceData.Precision, InstanceData.AllMaterialLimitStates, InstanceData.MaterialList);

                frm.ShowDialog();

                frm.Dispose();

                InputTextBox.Clear();

                _readOnlyChars = 0;
            }
        }

        private void ShowMomentCurvatureDialog()
        {
            if (ElementsSelected() == true)
            {
                _lastCommand = "mc";

                List<IDrawingElement> orderedSelectedList = SelectedElements.OrderByDescending(x => InstanceData.DrawingElementList.GetDrawOrder(x)).ToList();

                MomentCurvatureAnalysisForm frm = new MomentCurvatureAnalysisForm(InstanceData.MomentCurvatureAnalysisCases, orderedSelectedList, InstanceData.Precision, InstanceData.AllMaterialLimitStates, InstanceData.MaterialList);

                frm.ShowDialog();

                frm.Dispose();

                InputTextBox.Clear();

                _readOnlyChars = 0;
            }
        }
        #endregion

        #region Zooming
        private void ZoomWindow(PointD corner1, PointD corner2)
        {
            int pixelXmax = DraftingPictureBox.Width;
            int pixelYmax = DraftingPictureBox.Height;

            int pixelWidth = pixelXmax;
            int pixelHeight = pixelYmax;

            double xMin = Min(corner1.X, corner2.X);
            double xMax = Max(corner1.X, corner2.X);
            double yMin = Min(corner1.Y, corner2.Y);
            double yMax = Max(corner1.Y, corner2.Y);
            double xDist = xMax - xMin;
            double yDist = yMax - yMin;

            double drawingScale = Min(pixelWidth / xDist, pixelHeight / yDist);

            PointD screenCenter = new PointD(xMin + xDist / 2.0, yMin + yDist / 2.0);

            Point screeCenterPixel = new Point(Convert.ToInt32(pixelXmax / 2.0), Convert.ToInt32(pixelYmax / 2.0));

            InstanceData.DrawingScale = drawingScale;

            int zpX = Convert.ToInt32(screeCenterPixel.X - screenCenter.X * InstanceData.DrawingScale);
            int zpY = Convert.ToInt32(screeCenterPixel.Y + screenCenter.Y * InstanceData.DrawingScale);

            InstanceData.ZeroPoint = new Point(zpX, zpY);

            InstanceData.MagnificationLevel = InstanceData.DrawingScale / InstanceData.BaseDrawingScale;

            if (InstanceData.MagnificationLevel > 1)
            {
                InstanceData.WheelClicks = (Pow(InstanceData.MagnificationLevel, 2) - 1.0) / 3.0;
            }
            else if (InstanceData.MagnificationLevel < 1)
            {
                //(1.0 - InstanceData.MagnificationLevel) * (Pow(5.0, 1.0 / Abs(InstanceData.WheelClicks))) = 1.0;
                //(Pow(5.0, 1.0 / Abs(InstanceData.WheelClicks))) = 1.0 / (1.0 - InstanceData.MagnificationLevel);
                //(1.0 / Abs(InstanceData.WheelClicks)) * Log(5.0) = Log(1.0 / (1.0 - InstanceData.MagnificationLevel));
                //(1.0 / Abs(InstanceData.WheelClicks)) = Log(1.0 / (1.0 - InstanceData.MagnificationLevel)) / Log(5.0);
                //Abs(InstanceData.WheelClicks) = 1.0 / Log(1.0 / (1.0 - InstanceData.MagnificationLevel)) / Log(5.0);
                InstanceData.WheelClicks = -1.0 * (1.0 / (Log(1.0 / (1.0 - InstanceData.MagnificationLevel)) / Log(5.0)));
            }
            else
            {
                InstanceData.WheelClicks = 0;
            }

            InstanceData.PanelSnapProperties.SnapDistance = InstanceData.PanelSnapProperties.BaseSnapDistance / InstanceData.MagnificationLevel;

            InstanceData.SelectionDistance = InstanceData.BaseSelectionDistance / InstanceData.MagnificationLevel;

            _selectionBoxInitiated = false;

            ResetDrawingState();

            CreateDraftingGraphics();
        }

        private Cursor CreatMagnifyingGlassCursor()
        {
            Bitmap image = Properties.Resources.magnifying_glass1;

            float width = 20;
            float height = 20;

            var brush = new SolidBrush(Color.Black);

            float scale = Math.Min(width / image.Width, height / image.Height);

            var bmp = new Bitmap((int)width, (int)height);
            var graph = Graphics.FromImage(bmp);

            var scaleWidth = (int)(image.Width * scale);
            var scaleHeight = (int)(image.Height * scale);

            graph.FillRectangle(brush, new RectangleF(0, 0, width, height));
            graph.DrawImage(image, ((int)width - scaleWidth) / 2, ((int)height - scaleHeight) / 2, scaleWidth, scaleHeight);

            bmp.MakeTransparent(Color.White);

            UnSemi(bmp);

            return new Cursor(bmp.GetHicon());
        }

        private void UnSemi(Bitmap bmp)
        {
            Size s = bmp.Size;
            PixelFormat fmt = bmp.PixelFormat;
            Rectangle rect = new Rectangle(Point.Empty, s);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, fmt);
            int size1 = bmpData.Stride * bmpData.Height;
            byte[] data = new byte[size1];
            System.Runtime.InteropServices.Marshal.Copy(bmpData.Scan0, data, 0, size1);
            for (int y = 0; y < s.Height; y++)
            {
                for (int x = 0; x < s.Width; x++)
                {
                    int index = y * bmpData.Stride + x * 4;
                    // alpha,  threshold = 255
                    data[index + 3] = (data[index + 3] < 255) ? (byte)0 : (byte)255;

                    if (data[index + 0] > 240 && data[index + 1] > 240 && data[index + 2] > 240)
                    {
                        data[index + 0] = (data[index + 0] > 240) ? (byte)255 : (byte)0;
                        data[index + 1] = (data[index + 1] > 240) ? (byte)255 : (byte)0;
                        data[index + 2] = (data[index + 2] > 240) ? (byte)255 : (byte)0;
                        data[index + 3] = (byte)0;
                    }
                }
            }
            System.Runtime.InteropServices.Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
            bmp.UnlockBits(bmpData);
        }

        private void SetDrawingStateToZoomWindow()
        {
            _currentDrawingType = DrawingType.ZoomWindow;

            this.Cursor = _magnifyingCursor;

            _lastCommand = "zw";

            InputTextBox.Text = "";

            _readOnlyChars = InputTextBox.Text.Length;
        }

        public void ZoomExtents()
        {
            Rect r = GetExtents();

            ZoomWindow(new PointD(r.Left, r.Bottom), new PointD(r.Right, r.Top));
        }

        private Rect GetExtents()
        {
            double minX = -10;
            double maxX = 10;
            double minY = -10;
            double maxY = 10;

            for (int i = 0; i < InstanceData.DrawingElementList.Count; i++)
            {
                IDrawingElement elem = InstanceData.DrawingElementList[i];

                Rect r = elem.BoundingRectangle();

                if (i == 0)
                {
                    maxX = r.Right;
                    minX = r.Left;
                    maxY = r.Bottom;
                    minY = r.Top;
                }
                else
                {
                    maxX = Max(maxX, r.Right);
                    minX = Min(minX, r.Left);
                    maxY = Max(maxY, r.Bottom);
                    minY = Min(minY, r.Top);
                }
            }

            if (minX == maxX && minY == maxY)
            {
                return new Rect(-50, -50, 100, 100);
            }
            else if (minX == maxX)
            {
                return new Rect(minX - 0.05 * (maxY - minY), minY, maxX + 0.05 * (maxY - minY), maxY - minY);
            }
            else if (minY == maxY)
            {
                return new Rect(minX, minY - 0.05 * (maxX - minX), maxX, maxY + 0.05 * (maxX - minX));
            }
            else
            {
                return new Rect(minX, minY, maxX - minX, maxY - minY);
            }
        }
        #endregion

        #region Selected Objects Changed Events

        #endregion

        #region Material Editing
        private void ShowMaterialsDialog()
        {
            _lastCommand = "mat";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            MaterialSelectionForm frm = new MaterialSelectionForm(InstanceData.MaterialList);

            frm.ShowDialog();

            if (frm.DeletedMaterials.Count > 0)
            {
                ResetMaterialsWhereDeleted(frm.DeletedMaterials);

                RemoveMaterialLimitStates(frm.DeletedMaterials);
            }

            if (frm.AddedLimitStates.Count > 0)
            {
                InstanceData.AllMaterialLimitStates.AddRange(frm.AddedLimitStates);
            }

            frm.Dispose();

            UpdatePropertyPanelMaterialsComboBox();

            SetPropertyPanelAccordianVisibility();
        }

        private void ResetMaterialsWhereDeleted(List<IGenericMaterial> deletedMaterials)
        {
            if (InstanceData.DrawingElementList != null)
            {
                for (int i = 0; i < InstanceData.DrawingElementList.Count; i++)
                {
                    IDrawingElement elem = InstanceData.DrawingElementList[i];

                    if (elem.Material != null)
                    {
                        if (deletedMaterials.Contains(elem.Material) == true)
                        {
                            elem.Material = null;
                        }
                    }
                }
            }
        }

        private void RemoveMaterialLimitStates(List<IGenericMaterial> deletedMaterials)
        {
            if (InstanceData.AllMaterialLimitStates != null)
            {
                int ind = 0;

                while(ind < InstanceData.AllMaterialLimitStates.Count)
                {
                    LimitState ls = InstanceData.AllMaterialLimitStates[ind];

                    if (deletedMaterials.Contains(ls.Material) == true)
                    {
                        InstanceData.AllMaterialLimitStates.RemoveAt(ind);
                        ind--;
                    }

                    ind++;
                }

                if (InstanceData.AxialForceMomentAnalysisCases != null)
                {
                    for (int i = 0; i < InstanceData.AxialForceMomentAnalysisCases.Count; i++)
                    {
                        AxialForceMomentAnalysisCase pmCase = InstanceData.AxialForceMomentAnalysisCases[i];

                        if (pmCase.LimitStates != null)
                        {
                            ind = 0;

                            while (ind < pmCase.LimitStates.Count)
                            {
                                LimitState ls = pmCase.LimitStates[ind];

                                if (deletedMaterials.Contains(ls.Material) == true)
                                {
                                    pmCase.LimitStates.RemoveAt(ind);
                                    ind--;
                                }

                                ind++;
                            }
                        }
                    }
                }

                if (InstanceData.MomentCurvatureAnalysisCases != null)
                {
                    for (int i = 0; i < InstanceData.MomentCurvatureAnalysisCases.Count; i++)
                    {
                        MomentCurvatureAnalysisCase mcCase = InstanceData.MomentCurvatureAnalysisCases[i];

                        if (mcCase.LimitStates != null)
                        {
                            ind = 0;

                            while (ind < mcCase.LimitStates.Count)
                            {
                                LimitState ls = mcCase.LimitStates[ind];

                                if (deletedMaterials.Contains(ls.Material) == true)
                                {
                                    mcCase.LimitStates.RemoveAt(ind);
                                    ind--;
                                }

                                ind++;
                            }
                        }
                    }
                }
            }
        }

        private void UpdatePropertyPanelMaterialsComboBox()
        {
            ComboBox materialComboBox = _propertyPanelControlContainer.MaterialControls.MaterialComboBox;

            materialComboBox.Items.Clear();

            if (InstanceData.MaterialList != null)
            {
                for (int i = 0; i < InstanceData.MaterialList.Count; i++)
                {
                    materialComboBox.Items.Add(InstanceData.MaterialList[i].Name);
                }
            }
        }
        #endregion

        #region Program Options
        private void ShowProgramOptionsDialog()
        {
            _lastCommand = "opt";

            InputTextBox.Clear();

            _readOnlyChars = 0;

            ProgramOptionsForm frm = new ProgramOptionsForm(InstanceData, UseGDI);

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                if (frm.UseGDI == true)
                {
                    UseGDI = true;
                }
                else
                {
                    UseOpenGL = true;
                }

                CreateDraftingGraphics();
            }

            frm.Dispose();
        }
        #endregion

        #region Saving and Loading Instance Data
        public byte[] SavePanelInstanceData()
        {
            string jsonString = SerializePanelInstanceData();

            byte[] bytes = StringCompression.Zip(jsonString);

            return bytes;
        }

        private string SerializePanelInstanceData()
        {
            string serData = null;

            if (InstanceData != null)
            {
                JsonSerializer ser = new JsonSerializer();

                using (StringWriter sWriter = new StringWriter())
                {
                    ser.TypeNameHandling = TypeNameHandling.All;
                    ser.PreserveReferencesHandling = PreserveReferencesHandling.Objects;

                    ser.Serialize(sWriter, InstanceData);

                    serData = sWriter.ToString();
                }
            }

            return serData;
        }

        public void LoadPanelInstanceData(string serData)
        {
            if (DeSerializeInstanceData(serData) == true)
            {
                ResetDrawingState();

                for (int i = 0; i < InstanceData.DrawingElementList.Count; i++)
                {
                    InstanceData.DrawingElementQuadTree.Insert(InstanceData.DrawingElementList[i], InstanceData.DrawingElementList[i].BoundingRectangle());
                }

                CreateDraftingGraphics();

                UpdatePropertyPanelMaterialsComboBox();

                SetPropertyPanelAccordianVisibility();

                PopulatePanelToolStripLayersDropdown();

                _currentLayerDropdown.SelectedIndex = 0;

                _previousActions.Clear();
            }
        }

        public void LoadPanelInstanceData(byte[] serBytes)
        {
            string serData = StringCompression.Unzip(serBytes);

            LoadPanelInstanceData(serData);
        }

        private bool DeSerializeInstanceData(string serData)
        {
            ITraceWriter trace = new MemoryTraceWriter();

            try
            {
                if (serData != null && serData != string.Empty)
                {
                    using (Stream jsonStream = GenerateStreamFromString(serData))
                    {
                        using (StreamReader strReader = new StreamReader(jsonStream))
                        {
                            using (JsonTextReader jsonReader = new JsonTextReader(strReader))
                            {


                                JsonSerializer ser = new JsonSerializer();

                                ser.TraceWriter = trace;

                                ser.TypeNameHandling = TypeNameHandling.All;
                                ser.PreserveReferencesHandling = PreserveReferencesHandling.Objects;

                                CadPanelInstanceData instData = ser.Deserialize<CadPanelInstanceData>(jsonReader);

                                InstanceData = instData;

                                return true;
                            }
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while loading the CAD Panel instance data.  The string is likely not formatted correctly." + Environment.NewLine + Environment.NewLine + "Error:" + Environment.NewLine + ex.Message,"Error Loading Data",MessageBoxButtons.OK);

                return false;
            }
        }

        private Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        #endregion

    }
}
