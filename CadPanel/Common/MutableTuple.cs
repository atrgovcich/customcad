﻿using System;
using System.Collections;

namespace CadPanel.Common
{
    public class MutableTuple<T1, T2>
    {
        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }

        public MutableTuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(this, obj) == true)
            {
                return true;
            }
            else
            {
                if (obj.GetType().Equals(typeof(MutableTuple<T1, T2>)) == true)
                {
                    MutableTuple<T1, T2> castObj = (MutableTuple<T1, T2>)obj;

                    if (castObj.Item1.Equals(this.Item1) && castObj.Item2.Equals(this.Item2))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
