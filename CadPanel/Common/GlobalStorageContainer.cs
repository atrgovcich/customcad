﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.Common;
using CadPanel.DrawingElements;
using System.Drawing;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.Windows.Forms;
using ReinforcedConcrete.Reinforcement;
using ReinforcedConcrete.PostTensioning;
using CadPanel.Snapping;
using Materials;
using System.Drawing.Imaging;
using TriangleNetHelper.ContainerClasses;
using Materials.Inelastic;

namespace CadPanel.Common
{
    public static class GlobalStorageContainer
    {
        //#region Selection Properties
        //public static Color SelectedObjectLineColor = Color.Yellow;
        //public static Color SelectedObjectPointColor = Color.Green;
        //public static int SelectedObjectPointWidth = 3;
        //public static int SelectedObjectLineWidth = 1;
        //public static bool DrawVerticesOfMultipleObjects;
        //#endregion

        //#region TextDisplay
        //public static int DisplayPrecision = 2;
        //#endregion

        //#region Canvas Properties
        //public static int CanvasWidth = 1200;
        //public static int CanvasHeight = 600;
        //public static Point BitmapInsertionPoint = new Point(0, 0);
        
        //#endregion

        //#region Drawing Properties
        //public static double BaseDrawingScale = 20;
        //public static double DrawingScale = 20;
        //public static Point ZeroPoint = new Point(0, 0);
        //public static double MagnificationLevel = 1.0;
        //public static double PreviousMagnficationLevel = 1.0;
        //public static int WheelClicks = 0;
        //public static double PreviousMagnificationLevel = 1.0;
        //public static Rectangle MagnifyRectangle = new Rectangle(0, 0, CanvasWidth, CanvasHeight);
        //public static int RefreshRate = 25;
        //public static Layer CurrentDrawingLayer;
        //public static int MinDraftingBound = -1000000;
        //public static int MaxDraftingBound = 1000000;
        //public static int PointObjectSize = 4;
        //public static int Precision = 6;
        //#endregion

        //#region Layer Properties
        //public static List<Layer> Layers;
        //#endregion

        //#region Saving and Opening
        //public static string StartPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        //#endregion

        //#region Form Management
        //public static List<Form> FormList = new List<Form>();
        //#endregion

        //#region Drawing Element Management
        //public static List<IDrawingElement> DrawingElementList = new List<IDrawingElement>();
        //public static List<List<IDrawingElement>> PreviousDrawingStates = new List<List<IDrawingElement>>();
        //#endregion

        //#region Object Selection
        //public static double BaseSelectionDistance = 1.0;
        //public static double SelectionDistance = 1.0;
        //public static double SelectionToleranceForCoincidentElements = 0.001;
        //#endregion

        //#region Snap Properties
        //public static SnapProperties PanelSnapProperties { get; set; } = new SnapProperties();

        //#endregion

        //#region Materials
        //public static List<IGenericMaterial> MaterialList = new List<IGenericMaterial>();
        //#endregion

        //#region Rebar
        //public static Dictionary<string, RebarDefinition> AvailableRebarSizes = new Dictionary<string, RebarDefinition>();

        //public static Dictionary<string, RebarDefinition> DefaultRebarSizes = new Dictionary<string, RebarDefinition>();
        //#endregion

        //#region Meshing Options
        //public static double TriangleMeshSize = 3.0;
        //public static MeshingOptions TriangleMeshingOptions = new MeshingOptions();
        //#endregion

        //#region Post Tensioning
        //public static List<PostTensioningDefinition> PostTensioningDefinitionList = new List<PostTensioningDefinition>();
        //public static List<PostTensioningInnerSection> PostTensioningInnerSectionList = new List<PostTensioningInnerSection>();
        //#endregion

        //#region Constructors
        //static GlobalStorageContainer()
        //{
        //    InitializeDefaultRebar();
        //    InitializeDefaultPostTensioning();
        //}
        //#endregion

        //#region Public Methods
        //public static void InitializeDefaultRebar()
        //{
        //    if (AvailableRebarSizes != null)
        //    {
        //        AvailableRebarSizes.Clear();
        //    }
        //    else
        //    {
        //        AvailableRebarSizes = new Dictionary<string, RebarDefinition>();
        //    }

        //    AvailableRebarSizes.Add("3", new RebarDefinition() { Name = "3", Diameter = 0.375, Area = 0.11 });
        //    AvailableRebarSizes.Add("4", new RebarDefinition() { Name = "4", Diameter = 0.5, Area = 0.2 });
        //    AvailableRebarSizes.Add("5", new RebarDefinition() { Name = "5", Diameter = 0.625, Area = 0.31 });
        //    AvailableRebarSizes.Add("6", new RebarDefinition() { Name = "6", Diameter = 0.75, Area = 0.44 });
        //    AvailableRebarSizes.Add("7", new RebarDefinition() { Name = "7", Diameter = 0.875, Area = 0.6 });
        //    AvailableRebarSizes.Add("8", new RebarDefinition() { Name = "8", Diameter = 1.0, Area = 0.79 });
        //    AvailableRebarSizes.Add("9", new RebarDefinition() { Name = "9", Diameter = 1.128, Area = 1.0 });
        //    AvailableRebarSizes.Add("10", new RebarDefinition() { Name = "10", Diameter = 1.27, Area = 1.27 });
        //    AvailableRebarSizes.Add("11", new RebarDefinition() { Name = "11", Diameter = 1.41, Area = 1.56 });
        //    AvailableRebarSizes.Add("14", new RebarDefinition() { Name = "14", Diameter = 1.693, Area = 2.25 });
        //    AvailableRebarSizes.Add("18", new RebarDefinition() { Name = "18", Diameter = 2.257, Area = 4.0 });

        //    DefaultRebarSizes = new Dictionary<string, RebarDefinition>(AvailableRebarSizes);
        //}

        //public static void InitializeDefaultPostTensioning()
        //{
        //    //Must be called after initializing materials
        //    IGenericMaterial material = null;

        //    //0.5" diameter seven wire strand
        //    PostTensioningInnerSection innerSection = new SevenWireStrand(0.5);

        //    PostTensioningDefinition ptType = new PostTensioningDefinition("0.5\" 270ksi Seven Wire Strand", PostTensioningStrandType.SevenWireStrand, material, 0.5, 0.15);

        //    PostTensioningInnerSectionList.Add(innerSection);

        //    PostTensioningDefinitionList.Add(ptType);

        //    //0.6" diameter seven wire strand
        //    innerSection = new SevenWireStrand(0.6);

        //    ptType = new PostTensioningDefinition("0.6\" 270ksi Seven Wire Strand", PostTensioningStrandType.SevenWireStrand, material, 0.6, 0.217);

        //    PostTensioningInnerSectionList.Add(innerSection);

        //    PostTensioningDefinitionList.Add(ptType);

        //    //19-0.6" diameter seven wire strand
        //    innerSection = new NineteenStrandBundle(4.19, 0.6, PostTensioningStrandType.SevenWireStrand);

        //    ptType = new PostTensioningDefinition("19-0.6\" 270ksi Seven Wire Strand", PostTensioningStrandType.NineteenStrandBundle, material, 4.19, 4.123);

        //    PostTensioningInnerSectionList.Add(innerSection);

        //    PostTensioningDefinitionList.Add(ptType);
        //}
        //#endregion

    }
}
