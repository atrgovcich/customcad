﻿using System.Collections.Generic;
using FiberSectionAnalysis.AxialForceMomentAnalysis;
using FiberSectionAnalysis.LimitStates;
using System.Drawing;
using System.IO;
using System.Reflection;
using CadPanel.DrawingElements;
using System.Windows.Forms;
using Materials;
using ReinforcedConcrete.Reinforcement;
using ReinforcedConcrete.PostTensioning;
using TriangleNetHelper.ContainerClasses;
using CadPanel.Snapping;
using Materials.Inelastic.Concrete;
using Materials.Inelastic.Steel;
using FiberSectionAnalysis.MomentCurvatureAnalysis;
using Utilities.DataStructures.QuadTree;
using Utilities.Containers;
using Newtonsoft.Json;

namespace CadPanel.Common
{
    public class CadPanelInstanceData
    {
        #region Constants
        private const int BASE_WIDTH = 1200;
        private const int BASE_HEIGHT = 600;
        #endregion

        #region Public Properties
        public List<AxialForceMomentAnalysisCase> AxialForceMomentAnalysisCases { get; set; } = new List<AxialForceMomentAnalysisCase>();

        public List<MomentCurvatureAnalysisCase> MomentCurvatureAnalysisCases { get; set; } = new List<MomentCurvatureAnalysisCase>();

        public List<LimitState> MaterialLimitStatesPM { get; set; } = new List<LimitState>();

        public List<LimitState> MaterialLimitStatesMC { get; set; } = new List<LimitState>();

        public List<LimitState> AllMaterialLimitStates { get; set; } = new List<LimitState>();
        #endregion

        #region Selection Properties
        /// <summary>
        /// The line color to display for selected objects
        /// </summary>
        public Color SelectedObjectLineColor { get; set; } = Color.Yellow;
        /// <summary>
        /// The point color to display for selected objects
        /// </summary>
        public Color SelectedObjectPointColor { get; set; } = Color.Green;
        /// <summary>
        /// The width, in pixels, of the points for selected objects
        /// </summary>
        public int SelectedObjectPointWidth { get; set; } = 3;
        /// <summary>
        /// The width, in pixels, of the line for selected objects
        /// </summary>
        public int SelectedObjectLineWidth { get; set; } = 1;
        /// <summary>
        /// Should the program draw vertices when multiple objects are selected?
        /// Turning this to false may improve performance when many objects are selected.
        /// </summary>
        public bool DrawVerticesOfMultipleObjects { get; set; } = true;
        #endregion

        #region TextDisplay
        /// <summary>
        /// The number of decimal places to display
        /// </summary>
        public int DisplayPrecision { get; set; } = 2;
        #endregion

        #region Canvas Properties
        /// <summary>
        /// The canvas width, in pixels
        /// </summary>
        public int CanvasWidth { get; set; } = BASE_WIDTH;
        /// <summary>
        /// The canvas height, in pixels
        /// </summary>
        public int CanvasHeight { get; set; } = BASE_HEIGHT;
        /// <summary>
        /// The insertion piont of the bitmap
        /// </summary>
        public Point BitmapInsertionPoint { get; set; } = new Point(0, 0);

        #endregion

        #region Drawing Properties
        /// <summary>
        /// The base drawing scale for the panel
        /// </summary>
        public double BaseDrawingScale { get; set; } = 20;
        /// <summary>
        /// The current drawing scale for the panel
        /// </summary>
        public double DrawingScale { get; set; } = 20;
        /// <summary>
        /// The zero point relative to the panel container
        /// </summary>
        public Point ZeroPoint { get; set; } = new Point(0, 0);
        /// <summary>
        /// The magnification level (zoom)
        /// </summary>
        public double MagnificationLevel { get; set; } = 1.0;
        /// <summary>
        /// The number of wheel clicks that have happened (either positive or negative), used for zoom
        /// </summary>
        public double WheelClicks { get; set; } = 0;
        /// <summary>
        /// THe previous magnification level (zoom)
        /// </summary>
        public double PreviousMagnificationLevel { get; set; } = 1.0;
        /// <summary>
        /// Rectangle to zoom into
        /// </summary>
        public Rectangle MagnifyRectangle { get; set; } = new Rectangle(0, 0, BASE_WIDTH, BASE_HEIGHT);
        /// <summary>
        /// The refresh rate, in milliseconds, to redraw the panel when required.
        /// </summary>
        public int RefreshRate { get; set; } = 25;
        /// <summary>
        /// The current drawing layer to which all new elements will be added
        /// </summary>
        public Layer CurrentDrawingLayer { get; set; }
        /// <summary>
        /// The minimum x and y bounds in which drawing is allowed
        /// </summary>
        public int MinDraftingBound { get; set; } = -1000000;
        /// <summary>
        /// The maximum x and y bounds in which drawing is allowed
        /// </summary>
        public int MaxDraftingBound { get; set; } = 1000000;
        /// <summary>
        /// The size, in pixels, to display point objects
        /// </summary>
        public int PointObjectSize { get; set; } = 4;
        /// <summary>
        /// The internal precision to use when checking things such as coincidence and colinearity
        /// </summary>
        public int Precision { get; set; } = 6;
        /// <summary>
        /// Determines whether the program should use the QuadTree to draw only elements within the current window.
        /// If your file has less than 20,000 elements, or if you do not plan on consistently viewing all elements, set this to true.
        /// If your file has more than 20,000 elements, or if you plan to consistently view all elements, set this to false.
        /// Setting this to true increases performance when viewing a small subset of the total number of elements.
        /// However, it will decrease performance when there are tens-of-thousands of elements and you consistently view the entire set of elements.
        /// </summary>
        public bool UseQuadTreeWithOpenGL { get; set; } = false;
        #endregion

        #region Layer Properties
        /// <summary>
        /// List of all layers currently defined in the cad panel
        /// </summary>
        public List<Layer> Layers { get; set; }
        #endregion

        #region Saving and Opening
        /// <summary>
        /// The start path for the panel, used as the starting directory when opening or saving files
        /// </summary>
        public string StartPath { get; set; } = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        #endregion

        #region Form Management
        /// <summary>
        /// List of all of the forms currently open
        /// </summary>
        public List<Form> FormList { get; set; } = new List<Form>();
        #endregion

        #region Drawing Element Management
        /// <summary>
        /// List of all drawing elements in the cad panel
        /// </summary>
        public ObservableList<IDrawingElement> DrawingElementList { get; set; } = new ObservableList<IDrawingElement>();

        public QuadTree<IDrawingElement> DrawingElementQuadTree { get; set; }
        #endregion

        #region Object Selection
        /// <summary>
        /// Base selection distance at magnification level 0, in pixels.
        /// Change this value to adjust the seelection tolerance.
        /// </summary>
        public double BaseSelectionDistance { get; set; } = 1.0;
        /// <summary>
        /// Current selection distance, in pixels.  This value should only be mmodified automatically
        /// by an equation.  To change the selection tolerance via user input, modify the BaseSelectionDistance
        /// </summary>
        public double SelectionDistance { get; set; } = 1.0;
        /// <summary>
        /// Tolerance for coincident elements.  When this tolerance is met, the objects
        /// are added to the coincident selection list and can be tabbed through.
        /// </summary>
        public double SelectionToleranceForCoincidentElements { get; set; } = 0.001;
        #endregion

        #region Snap Properties
        /// <summary>
        /// Snap properties
        /// </summary>
        public SnapProperties PanelSnapProperties { get; set; } = new SnapProperties();

        #endregion

        #region Materials
        /// <summary>
        /// List of all materials defined in the panel
        /// </summary>
        public List<IGenericMaterial> MaterialList { get; set; } = new List<IGenericMaterial>();
        #endregion

        #region Rebar
        /// <summary>
        /// Dictionary of the rebar sizes defined in the panel
        /// </summary>
        public Dictionary<string, RebarDefinition> AvailableRebarSizes { get; set; } = new Dictionary<string, RebarDefinition>();
        /// <summary>
        /// Default rebar sizes (US system)
        /// </summary>
        public Dictionary<string, RebarDefinition> DefaultRebarSizes { get; set; } = new Dictionary<string, RebarDefinition>();
        #endregion

        #region Meshing Options
        /// <summary>
        /// Mesh size used for triangulation
        /// </summary>
        public double TriangleMeshSize = 3.0;
        /// <summary>
        /// Triangle.Net meshing options
        /// </summary>
        public MeshingOptions TriangleMeshingOptions = new MeshingOptions();
        #endregion

        #region Post Tensioning
        /// <summary>
        /// List of currently defined post-tensioning defintions
        /// </summary>
        public List<PostTensioningDefinition> PostTensioningDefinitionList = new List<PostTensioningDefinition>();
        /// <summary>
        /// List of currently defined post-tensioning inner sections
        /// </summary>
        public List<PostTensioningInnerSection> PostTensioningInnerSectionList = new List<PostTensioningInnerSection>();
        #endregion

        #region Constructors
        public CadPanelInstanceData()
        {
            InitializeMaterials();
            InitializeDefaultRebar();
            InitializeDefaultPostTensioning();
        }

        [JsonConstructor]
        public CadPanelInstanceData(bool nada)
        {

        }
        #endregion

        #region Private Methods
        
        private void InitializeMaterials()
        {
            //4 ksi concrete
            ManderConcrete conc1 = new ManderConcrete("4 ksi Concrete", 4, 0, 0, 0.002, 0.004, 0.006, 0);
            LimitState conc1_ls_pm = new LimitState() { Name = "ACI Crushing", Material = conc1, CompressionLimitStrain = 0.003, TensionLimitStrain = 1.0, TerminationLimitState = true };

            //6 ksi concrete
            ManderConcrete conc2 = new ManderConcrete("6 ksi Concrete", 6, 0, 0, 0.002, 0.004, 0.006, 0);
            LimitState conc2_ls_pm = new LimitState() { Name = "ACI Crushing", Material = conc2, CompressionLimitStrain = 0.003, TensionLimitStrain = 1.0, TerminationLimitState = true };

            //60 ksi steel
            ManderSteel steel1 = new ManderSteel("60 ksi Steel", 60, 90, 60.0 / 29000.0, 0.008, 0.09, 4);
            LimitState steel1_ls_pm = new LimitState() { Name = "Strain Hardening", Material = steel1, CompressionLimitStrain = 0.008, TensionLimitStrain = 0.008, TerminationLimitState = true };
            LimitState steel1_ls_mc = new LimitState() { Name = "Failure", Material = steel1, CompressionLimitStrain = 0.09, TensionLimitStrain = 0.09, TerminationLimitState = true };

            //75 ksi steel
            ManderSteel steel2 = new ManderSteel("75 ksi Steel", 75, 100, 75.0 / 29000.0, 0.008, 0.09, 4);
            LimitState steel2_ls_pm = new LimitState() { Name = "Strain Hardening", Material = steel2, CompressionLimitStrain = 0.008, TensionLimitStrain = 0.008, TerminationLimitState = true };
            LimitState steel2_ls_mc = new LimitState() { Name = "Failure", Material = steel2, CompressionLimitStrain = 0.09, TensionLimitStrain = 0.09, TerminationLimitState = true };

            //80 ksi steel
            ManderSteel steel3 = new ManderSteel("80 ksi Steel", 80, 105, 80.0 / 29000.0, 0.008, 0.09, 4);
            LimitState steel3_ls_pm = new LimitState() { Name = "Strain Hardening", Material = steel3, CompressionLimitStrain = 0.008, TensionLimitStrain = 0.008, TerminationLimitState = true };
            LimitState steel3_ls_mc = new LimitState() { Name = "Failure", Material = steel3, CompressionLimitStrain = 0.09, TensionLimitStrain = 0.09, TerminationLimitState = true };

            //PT steel
            ModifiedRambergOsgoodSteel steel4 = new ModifiedRambergOsgoodSteel("270 ksi Low Relaxation", 28130, 260, 276, 0.068, 240, 0.025, 10, 135);
            LimitState steel4_ls_pm = new LimitState() { Name = "Tension Yield", Material = steel4, CompressionLimitStrain = 0.068, TensionLimitStrain = 0.0107, TerminationLimitState = true };
            LimitState steel4_ls_mc = new LimitState() { Name = "Failure", Material = steel4, CompressionLimitStrain = 0.068, TensionLimitStrain = 0.068, TerminationLimitState = true };

            MaterialList.Add(conc1);
            MaterialList.Add(conc2);
            MaterialList.Add(steel1);
            MaterialList.Add(steel2);
            MaterialList.Add(steel3);
            MaterialList.Add(steel4);

            AllMaterialLimitStates.AddRange(new List<LimitState>() { conc1_ls_pm, conc2_ls_pm, steel1_ls_pm, steel1_ls_mc, steel2_ls_pm, steel2_ls_mc, steel3_ls_pm, steel3_ls_mc, steel4_ls_pm, steel4_ls_mc });
            MaterialLimitStatesPM.AddRange(new List<LimitState>() { conc1_ls_pm, conc2_ls_pm, steel1_ls_pm, steel2_ls_pm, steel3_ls_pm, steel4_ls_pm });
            MaterialLimitStatesMC.AddRange(new List<LimitState>() { steel1_ls_mc, steel2_ls_mc, steel3_ls_mc, steel4_ls_mc });
        }

        private void InitializeDefaultRebar()
        {
            if (AvailableRebarSizes != null)
            {
                AvailableRebarSizes.Clear();
            }
            else
            {
                AvailableRebarSizes = new Dictionary<string, RebarDefinition>();
            }

            AvailableRebarSizes.Add("3", new RebarDefinition() { Name = "3", Diameter = 0.375, Area = 0.11 });
            AvailableRebarSizes.Add("4", new RebarDefinition() { Name = "4", Diameter = 0.5, Area = 0.2 });
            AvailableRebarSizes.Add("5", new RebarDefinition() { Name = "5", Diameter = 0.625, Area = 0.31 });
            AvailableRebarSizes.Add("6", new RebarDefinition() { Name = "6", Diameter = 0.75, Area = 0.44 });
            AvailableRebarSizes.Add("7", new RebarDefinition() { Name = "7", Diameter = 0.875, Area = 0.6 });
            AvailableRebarSizes.Add("8", new RebarDefinition() { Name = "8", Diameter = 1.0, Area = 0.79 });
            AvailableRebarSizes.Add("9", new RebarDefinition() { Name = "9", Diameter = 1.128, Area = 1.0 });
            AvailableRebarSizes.Add("10", new RebarDefinition() { Name = "10", Diameter = 1.27, Area = 1.27 });
            AvailableRebarSizes.Add("11", new RebarDefinition() { Name = "11", Diameter = 1.41, Area = 1.56 });
            AvailableRebarSizes.Add("14", new RebarDefinition() { Name = "14", Diameter = 1.693, Area = 2.25 });
            AvailableRebarSizes.Add("18", new RebarDefinition() { Name = "18", Diameter = 2.257, Area = 4.0 });

            DefaultRebarSizes = new Dictionary<string, RebarDefinition>(AvailableRebarSizes);
        }

        private void InitializeDefaultPostTensioning()
        {
            //Must be called after initializing materials
            IGenericMaterial material = GetMaterialFromName("270 ksi Low Relaxation");

            //0.5" diameter seven wire strand
            PostTensioningInnerSection innerSection = new SevenWireStrand(0.5);

            PostTensioningDefinition ptType = new PostTensioningDefinition("0.5\" 270ksi Seven Wire Strand", PostTensioningStrandType.SevenWireStrand, material, 0.5, 0.15);

            PostTensioningInnerSectionList.Add(innerSection);

            PostTensioningDefinitionList.Add(ptType);

            //0.6" diameter seven wire strand
            innerSection = new SevenWireStrand(0.6);

            ptType = new PostTensioningDefinition("0.6\" 270ksi Seven Wire Strand", PostTensioningStrandType.SevenWireStrand, material, 0.6, 0.217);


            PostTensioningInnerSectionList.Add(innerSection);

            PostTensioningDefinitionList.Add(ptType);

            //19-0.6" diameter seven wire strand
            innerSection = new NineteenStrandBundle(4.19, 0.6, PostTensioningStrandType.SevenWireStrand);

            ptType = new PostTensioningDefinition("19-0.6\" 270ksi Seven Wire Strand", PostTensioningStrandType.NineteenStrandBundle, material, 4.19, 4.123);

            PostTensioningInnerSectionList.Add(innerSection);

            PostTensioningDefinitionList.Add(ptType);
        }
        #endregion

        #region Public Methods

        public void InitializeLayers()
        {
            Layers = new List<Layer>();

            Layer defaultLayer = new Layer("Default", 0, 1, Color.Blue);

            Layer rebarLayer = new Layer("Rebar", 1, 2, Color.Black);

            Layers.Add(defaultLayer);
            Layers.Add(rebarLayer);

            CurrentDrawingLayer = Layers[0];
        }

        public Layer GetLayerFromName(string name)
        {
            Layer ret = null;

            if (Layers != null && Layers.Count > 0)
            {
                for (int i = 0; i < Layers.Count; i++)
                {
                    if (string.Compare(Layers[i].Name, name) == 0)
                    {
                        ret = Layers[i];

                        break;
                    }
                }
            }

            return ret;
        }

        public IGenericMaterial GetMaterialFromName(string name)
        {
            IGenericMaterial ret = null;

            if (MaterialList != null && MaterialList.Count > 0)
            {
                for (int i = 0; i < MaterialList.Count; i++)
                {
                    if (string.Compare(MaterialList[i].Name, name) == 0)
                    {
                        ret = MaterialList[i];

                        break;
                    }
                }
            }

            return ret;
        }

        public RebarDefinition GetRebarDefinitionFromName(string name)
        {
            RebarDefinition ret = null;

            if (AvailableRebarSizes != null)
            {
                AvailableRebarSizes.TryGetValue(name, out ret);
            }

            return ret;
        }

        public PostTensioningDefinition GetPostTensioningDefinitionFromName(string name)
        {
            PostTensioningDefinition ret = null;

            if (PostTensioningDefinitionList != null && PostTensioningDefinitionList.Count > 0)
            {
                for (int i = 0; i < PostTensioningDefinitionList.Count; i++)
                {
                    if (string.Compare(PostTensioningDefinitionList[i].Name, name) == 0)
                    {
                        ret = PostTensioningDefinitionList[i];

                        break;
                    }
                }
            }

            return ret;
        }
        #endregion
    }
}
