﻿namespace CadPanel.Common
{
    public enum ThreeStateValues
    {
        False,
        True,
        Indeterminate
    }
}
