﻿using System;
using Newtonsoft.Json;

using Color = System.Drawing.Color;

namespace CadPanel.Common
{
    [Serializable]
    public class Layer : IDisposable
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public Color Color { get; set; }
        public Color SelectedColor { get; set; } = Color.Yellow;
        public int Width { get; set; }
        public bool Frozen { get; set; } = false;
        public bool Print { get; set; } = true;
        public bool Visible { get; set; } = true;


        [JsonConstructor]
        public Layer(string name, int number, int width, Color color)
        {
            Name = name;
            Number = number;
            Width = width;
            Color = color;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
            }
        }
    }
}
