﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Materials;
using static System.Math;
using CadPanel.DrawingElements;
using Utilities.Geometry;
using TriangleNet.Geometry;
using TriangleNetHelper.Geometry;
using TriangleNetHelper.ContainerClasses;
using TriangleNetHelper;
using Materials;

using Point = System.Drawing.Point;

namespace CadPanel.Common
{
    public static class CommonMethods
    {
        public static PointD ConvertToGlobalCoord(Point p, Point zeroPoint, double drawingScale)
        {
            double x = (p.X - zeroPoint.X) / drawingScale;
            double y = (zeroPoint.Y - p.Y) / drawingScale;

            return new PointD(x, y);
        }

        public static Point ConvertToPixelCoord(PointD p, Point zeroPoint, double drawingScale)
        {
            int x = Convert.ToInt32(zeroPoint.X + p.X * drawingScale);
            int y = Convert.ToInt32(zeroPoint.Y - p.Y * drawingScale);

            return new Point(x, y);
        }

        /// <summary>
        /// Checks which side of a line the point lies on.
        /// Returns less than 0 if p is left of the line passing through p0 & p1
        /// Returns 0 if p is on the line passing through p0 & p1
        /// Returns greater than 0 if p is right of the line passing through p0 & p1
        /// </summary>
        /// <param name="p0">Line start point</param>
        /// <param name="p1">Line end point</param>
        /// <param name="p">Point to check</param>
        /// <returns></returns>
        public static double IsLeft(PointD p0, PointD p1, PointD p)
        {
            double val = (p1.X - p0.X) * (p.Y - p0.Y) - (p.X - p0.X) * (p1.Y - p0.Y);

            return val;
        }

        public static CircleDefinition GetThreePointCircleEquation(PointD p1, PointD p2, PointD p3)
        {
            double x1, y1, x2, y2, x3, y3, cx, cy, r;

            x1 = p1.X;
            y1 = p1.Y;

            x2 = p2.X;
            y2 = p2.Y;

            x3 = p3.X;
            y3 = p3.Y;

            cx = ((Pow(x1, 2) + Pow(y1,2)) * (y2 - y3) + (Pow(x2, 2) + Pow(y2, 2)) * (y3 - y1) + (Pow(x3, 2) + Pow(y3, 2)) * (y1 - y2)) / (2 * (x1 * (y2 - y3) - y1 * (x2 - x3) + x2 * y3 - x3 * y2));
            cy = ((Pow(x1, 2) + Pow(y1, 2)) * (x3 - x2) + (Pow(x2, 2) + Pow(y2, 2)) * (x1 - x3) + (Pow(x3, 2) + Pow(y3, 2)) * (x2 - x1)) / (2 * (x1 * (y2 - y3) - y1 * (x2 - x3) + x2 * y3 - x3 * y2));
            r = Sqrt(Pow((cx - x1), 2) + Pow((cy - y1), 2));

            PointD centroid = new PointD(cx, cy);
            return new CircleDefinition(centroid, r);
        }

        public static List<PointD> GetIntersectionsOfTwoCircles(PointD c1, double r1, PointD c2, double r2)
        {
            //From:  https://math.stackexchange.com/questions/256100/how-can-i-find-the-points-at-which-two-circles-intersect

            List<PointD> intersections = new List<PointD>();

            double x1 = c1.X;

            double y1 = c1.Y;

            double x2 = c2.X;

            double y2 = c2.Y;

            double centerdx = c1.X - c2.X;

            double centerdy = c1.Y - c2.Y;

            double R = Sqrt(Pow(centerdx, 2) + Pow(centerdy, 2));

            if (!(Abs(r1-r2) <= R && R <= r1 + r2))
            {
                //No intersections exist
                return intersections;
            }

            double R2 = R * R;

            double R4 = R2 * R2;

            double a = (r1 * r1 - r2 * r2) / (2.0 * R2);

            double r2r2 = (r1 * r1 - r2 * r2);

            double c = Sqrt(2.0 * (r1 * r1 + r2 * r2) / R2 - (r2r2 * r2r2) / R4 - 1.0);

            double fx = (x1 + x2) / 2.0 + a * (x2 - x1);

            double gx = c * (y2 - y1) / 2.0;

            double ix1 = fx + gx;

            double ix2 = fx - gx;

            double fy = (y1 + y2) / 2.0 + a * (y2 - y1);

            double gy = c * (x1 - x2) / 2.0;

            double iy1 = fy + gy;

            double iy2 = fy - gy;

            PointD int1 = new PointD(ix1, iy1);

            PointD int2 = new PointD(ix2, iy2);

            intersections.Add(int1);

            if (int1 != int2)
            {
                intersections.Add(int2);
            }

            return intersections;
        }

        /// <summary>
        /// Checks if a point is inside of a polygon.  Returns 0 only when 'p' is outside of the polygon.
        /// Otherwise, returns != 0.
        /// </summary>
        /// <param name="p">Point to check</param>
        /// <param name="polygon">Polygon in question</param>
        /// <returns></returns>
        public static int WindingNumberInside(PointD p, PolygonG polygon)
        {
            //V = vertex
            //Vertices is list of polygon vertices
            //Returns winding number, which is == 0 only when V is outside of the polygon
            //Credit for this code goes to: http://geomalgorithms.com/a03-_inclusion.html
            List<PointD> Vertices = polygon.Nodes.Select(x=>x.Point).ToList();

            int wn = 0;

            int lastIndex = Vertices.Count;

            if (Vertices[0].X == Vertices[Vertices.Count - 1].X && Vertices[0].Y == Vertices[Vertices.Count - 1].Y)
            {
                lastIndex = Vertices.Count - 1;
            }

            //Loop through all edges of the polygon
            for (int i = 0; i < lastIndex; i++)
            {
                PointD nextPoint;

                if (i == lastIndex - 1)
                {
                    if (lastIndex == Vertices.Count)
                    {
                        nextPoint = Vertices[0];
                    }
                    else
                    {
                        nextPoint = Vertices[i + 1];
                    }
                }
                else
                {
                    nextPoint = Vertices[i + 1];
                }

                if (Vertices[i].Y <= p.Y) //If the start point 'y' value is less than the point's 'y' value
                {
                    if (nextPoint.Y > p.Y) //An upward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, p) > 0) //V is left of the dge
                        {
                            wn += 1;
                        }
                    }
                }
                else
                {
                    if (nextPoint.Y <= p.Y) //A downward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, p) < 0) //V is right of the edge
                        {
                            wn -= 1;
                        }
                    }
                }
            }

            return wn;
        }

        public static List<PrePolygon> CreateTriangleNetMesh(List<IDrawingElement> elementList, double meshSize, double precision)
        {
            double orthoTolerance = 1.0 * Pow(10.0, -precision);

            List<IShape> preShapeList = new List<IShape>();

            for (int i = 0; i < elementList.Count; i++)
            {
                IDrawingElement elem = elementList[i];

                IDrawingElement discretizedElement = elem.GetDiscretizedPoly(meshSize);

                if (discretizedElement != null)
                {
                    if (discretizedElement.GetType().Equals(typeof(PolygonG)) == true || discretizedElement.GetType().IsSubclassOf(typeof(PolygonG)) == true)
                    {
                        preShapeList.Add(Triangle.TriangleHelperMethods.ConvertPolygonGtoPrePolygon((PolygonG)discretizedElement));
                    }
                    else if (discretizedElement.GetType().Equals(typeof(PolylineG)) == true)
                    {
                        preShapeList.Add(Triangle.TriangleHelperMethods.ConvertPolylineGtoPrePolyline((PolylineG)discretizedElement));
                    }
                    else if (discretizedElement.GetType().Equals(typeof(PointG)) == true)
                    {
                        preShapeList.Add(Triangle.TriangleHelperMethods.ConvertPointGtoPreVertex((PointG)discretizedElement));
                    }
                }
            }

            if (preShapeList.Count > 0)
            {
                MeshingOptions meshOptions = new MeshingOptions();

                TriangleHelper helper = new TriangleHelper(preShapeList, meshSize, meshOptions, orthoTolerance);

                helper.CreateMesh();

                List<PrePolygon> preMesh = helper.RetrieveMesh;

                return preMesh;
            }
            else
            {
                return null;
            }
        }

        public static IGenericMaterial GetMaterialFromName(string name, List<IGenericMaterial> materials)
        {
            if (materials != null)
            {
                for (int i = 0; i < materials.Count; i++)
                {
                    if (string.Compare(name, materials[i].Name, ignoreCase: true) == 0)
                    {
                        return materials[i];
                    }
                }
            }

            return null;
        }
    }
}
