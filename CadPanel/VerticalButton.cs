﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CadPanel
{
    public partial class VerticalButton : Button
    {
        public string VerticalText { get; set; }
        private StringFormat _format = new StringFormat();


        public VerticalButton()
        {
            InitializeComponent();
            _format.Alignment = StringAlignment.Center;
            _format.LineAlignment = StringAlignment.Center;
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            base.OnPaint(pevent);

            pevent.Graphics.TranslateTransform(0, Height);
            pevent.Graphics.RotateTransform(270);
            pevent.Graphics.DrawString(VerticalText, Font, Brushes.Black, new Rectangle(0, 0, Height, Width), _format);
        }
    }
}
