﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.DrawingElements;
using Utilities.DataStructures.QuadTree;

namespace CadPanel.UndoSupport.Actions
{
    public abstract class DrawingAction
    {

        #region Abstract Methods
        public abstract void ReverseAction(List<IDrawingElement> affectedElements, ref IList<IDrawingElement> elementList, ref QuadTree<IDrawingElement> tree);
        #endregion
    }
}
