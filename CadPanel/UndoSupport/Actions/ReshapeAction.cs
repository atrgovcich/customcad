﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.DrawingElements;
using CadPanel.Common;
using Utilities.DataStructures.QuadTree;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.UndoSupport.Actions
{
    public class ReshapeAction : DrawingAction
    {
        #region Public Properties
        public DrawingNode Node { get; set; }
        public PointD OriginalPoint { get; set; }
        public PointD FinalPoint { get; set; }
        #endregion

        #region Constructors
        public ReshapeAction()
        {

        }

        public ReshapeAction(DrawingNode node, PointD origPoint, PointD finalPoint)
        {
            Node = node;
            OriginalPoint = origPoint;
            FinalPoint = finalPoint;
        }
        #endregion

        #region Public Implemented Methods
        public override void ReverseAction(List<IDrawingElement> affectedElements, ref IList<IDrawingElement> elementList, ref QuadTree<IDrawingElement> tree)
        {
            if (OriginalPoint != null && FinalPoint != null)
            {
                Node.Point.X = OriginalPoint.X;

                Node.Point.Y = OriginalPoint.Y;

                foreach (IDrawingElement elem in affectedElements)
                {
                    if (elem.Nodes.Contains(Node) == true)
                    {
                        elem.ActionAfterReshape();
                    }

                    tree.Remove(elem);

                    tree.Insert(elem, elem.BoundingRectangle());
                }
            }
        }
        #endregion

    }
}
