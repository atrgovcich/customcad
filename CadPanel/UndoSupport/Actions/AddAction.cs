﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.DrawingElements;
using Utilities.DataStructures.QuadTree;

namespace CadPanel.UndoSupport.Actions
{
    public class AddAction : DrawingAction
    {
        public AddAction()
        {

        }

        public override void ReverseAction(List<IDrawingElement> affectedElements, ref IList<IDrawingElement> elementList, ref QuadTree<IDrawingElement> tree)
        {
            foreach (IDrawingElement elem in affectedElements)
            {
                elementList.Remove(elem);
                tree.Remove(elem);
            }
        }
    }
}
