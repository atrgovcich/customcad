﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.Common;
using CadPanel.DrawingElements;
using Utilities.DataStructures.QuadTree;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.UndoSupport.Actions
{
    public class RotateAction : DrawingAction
    {
        public PointD BasePoint { get; set; }
        public double Angle { get; set; }

        public RotateAction()
        {

        }

        public override void ReverseAction(List<IDrawingElement> affectedElements, ref IList<IDrawingElement> elementList, ref QuadTree<IDrawingElement> tree)
        {
            if (BasePoint != null)
            {
                foreach (IDrawingElement elem in affectedElements)
                {
                    tree.Remove(elem);

                    elem.RotateThroughAngle(BasePoint, -Angle);

                    tree.Insert(elem, elem.BoundingRectangle());
                }
            }
        }
    }
}
