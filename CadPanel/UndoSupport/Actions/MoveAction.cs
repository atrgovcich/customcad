﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadPanel.DrawingElements;
using Utilities.DataStructures.QuadTree;
using CadPanel.Common;
using Utilities.Geometry;
using CadPanel.Extensions;

namespace CadPanel.UndoSupport.Actions
{
    public class MoveAction : DrawingAction
    {
        public PointD BasePoint { get; set; }
        public PointD FinalPoint { get; set; }

        public MoveAction()
        {

        }

        public override void ReverseAction(List<IDrawingElement> affectedElements, ref IList<IDrawingElement> elementList, ref QuadTree<IDrawingElement> tree)
        {
            if (BasePoint != null && FinalPoint != null)
            {
                foreach (IDrawingElement elem in affectedElements)
                {
                    tree.Remove(elem);

                    elem.Move(FinalPoint, BasePoint);

                    tree.Insert(elem, elem.BoundingRectangle());
                }
            }
        }
    }
}
