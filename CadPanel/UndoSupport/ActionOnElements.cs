﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.DataStructures.QuadTree;
using CadPanel.DrawingElements;
using CadPanel.UndoSupport.Actions;

namespace CadPanel.UndoSupport
{
    public class ActionOnElements
    {
        public Dictionary<DrawingAction, List<IDrawingElement>> Actions { get; set; }

        public ActionOnElements()
        {
            Actions = new Dictionary<DrawingAction, List<IDrawingElement>>();
        }

        public ActionOnElements(Dictionary<DrawingAction, List<IDrawingElement>> actions)
        {
            Actions = actions;
        }

        public void AddAction(DrawingAction action, List<IDrawingElement> elements)
        {
            if (Actions.ContainsKey(action) == true)
            {
                for (int i = 0; i < elements.Count; i++)
                {
                    if (Actions[action].Contains(elements[i]) == false)
                    {
                        Actions[action].Add(elements[i]);
                    }
                }
            }
            else
            {
                Actions.Add(action, elements);
            }
        }

        public void ReverseAction(IList<IDrawingElement> allElementsList, QuadTree<IDrawingElement> tree)
        {
            foreach (KeyValuePair<DrawingAction, List<IDrawingElement>> kvp in Actions)
            {
                kvp.Key.ReverseAction(kvp.Value, ref allElementsList, ref tree);
            }
        }
    }
}
