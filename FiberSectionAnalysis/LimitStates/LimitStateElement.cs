﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Elements;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.LimitStates
{
    public class LimitStateElement
    {
        #region Public Properties
        /// <summary>
        /// The element for which the limit state is evaluated.
        /// </summary>
        public IFiberSectionElement Element { get; set; }
        /// <summary>
        /// The strain in the furthest fiber that causes the material limit state to be reached.  Compression is positive, tension is negative.
        /// </summary>
        public double FurthestFiberStrain { get; set; }
        /// <summary>
        /// The limiting strain for the material.  Compression is positive, tension is negative.
        /// </summary>
        public double LimitStateStrain { get; set; }
        #endregion

        #region Constructors
        [JsonConstructor]
        public LimitStateElement()
        {

        }

        public LimitStateElement(IFiberSectionElement element, double furthestFiberStrain, double limitStateStrain)
        {
            Element = element;
            FurthestFiberStrain = furthestFiberStrain;
            LimitStateStrain = limitStateStrain;
        }
        #endregion
    }
}
