﻿using System;
using System.Collections.Generic;
using System.Text;
using Materials.Inelastic;
using Materials;

namespace FiberSectionAnalysis.LimitStates
{
    public class LimitState
    {
        #region Public Properties
        /// <summary>
        /// Name of the limit state, for organization purposes
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The material that the limit state applies to
        /// </summary>
        public IGenericMaterial Material { get; set; }
        /// <summary>
        /// The limiting compression strain, as a positive value
        /// </summary>
        public double CompressionLimitStrain { get; set; }
        /// <summary>
        /// The limiting tension strain, as a positive value
        /// </summary>
        public double TensionLimitStrain { get; set; }
        /// <summary>
        /// Whether this is a termination limit state.  If true, analysis stops when the limit state is reached.
        /// If false, the limit state is recorded and the analysis continues.
        /// </summary>
        public bool TerminationLimitState { get; set; }
        #endregion
    }
}
