﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.Elements;

namespace FiberSectionAnalysis.LimitStates
{
    public class LimitStateRecord
    {
        #region Public Properties
        public double Curvature { get; set; }
        public LimitState LimitStateReached { get; set; }
        public StrainDirectionType StrainDirection { get; set; }
        public IFiberSectionElement Element { get; set; }

        public bool Termination { get; set; }
        #endregion
    }
}
