﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace FiberSectionAnalysis.LimitStates
{
    public enum StrainDirectionType
    {
        [Description("Tension")]
        Tension,
        [Description("Comporession")]
        Compression
    }
}
