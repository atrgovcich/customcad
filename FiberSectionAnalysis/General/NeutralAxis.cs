﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;

namespace FiberSectionAnalysis.General
{
    public class NeutralAxis
    {
        #region Public Properties
        /// <summary>
        /// The neutral axis angle, in radians
        /// </summary>
        public double Angle { get; set; }
        /// <summary>
        /// The slope of the neutral axis line
        /// </summary>
        public double Slope { get; set; }
        /// <summary>
        /// The slope normal to the neutral axis line
        /// </summary>
        public double NormalSlope { get; set; }
        /// <summary>
        /// The Y-intercept of the neutral axis line
        /// </summary>
        public double YIntercept { get; set; }
        /// <summary>
        /// The netural axis line, set up so that compression is on the left side of the line
        /// </summary>
        public Edge Line { get; set; }
        #endregion
    }
}
