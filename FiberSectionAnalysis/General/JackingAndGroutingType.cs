﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace FiberSectionAnalysis.General
{
    public enum JackingAndGroutingType
    {
        [Description("Jacked, Grouted, Released")]
        JackedGroutedReleased,
        [Description("Jacked, Released, Grouted")]
        JackedReleasedGrouted,
        [Description("Unbonded")]
        Unbonded
    }
}
