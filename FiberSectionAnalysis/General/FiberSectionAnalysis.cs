﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Results;
using FiberSectionAnalysis.LimitStates;
using Materials;
using Materials.Inelastic.Concrete;
using static System.Math;
using FiberSectionAnalysis.Elements;
using Newtonsoft.Json;
using System.Linq;

namespace FiberSectionAnalysis.General
{
    public abstract class GeneralFiberSectionAnalysis
    {
        #region Private Backing Fields
        protected List<FiberSectionElementResults> _elementResults;

        protected List<FiberSectionAnalysisResult> _analysisResults;

        //protected List<KeyValuePair<IGenericMaterial, List<LimitState>>> _materialLimitStatesList;

        protected Dictionary<IGenericMaterial, List<LimitState>> _materialLimitStates;
        #endregion

        #region Private Variables
        protected Vector _unitVectorMajor;
        protected Vector _unitVectorMinor;
        protected NeutralAxis _neutralAxis;

        #endregion

        #region Public Properties
        /// <summary>
        /// The fiber section model containing all of the elements
        /// </summary>
        public FiberSection SectionModel { get; set; }
        /// <summary>
        /// Angle of bending, in radians, measured from the postiive X-axis
        /// </summary>
        public double AnalysisAngle { get; set; }
        /// <summary>
        /// The approximate number of points to be produced for the diagram
        /// </summary>
        public int NumberOfPoints { get; set; } = 30;
        /// <summary>
        /// Returns the list of analysis results from each curvature analyzed
        /// </summary>
        [JsonIgnore]
        public List<FiberSectionAnalysisResult> AnalysisResults
        {
            get
            {
                return _analysisResults;
            }
        }
        /// <summary>
        /// Returns a dictionary of post tensioning results, keyed off of the post tensioning elements
        /// </summary>
        [JsonIgnore]
        public List<FiberSectionElementResults> AllElementResults
        {
            get
            {
                return _elementResults;
            }
        }
        /// <summary>
        /// Whether the initial post tensioning force (if any) is included in the axial force-moment diagram.
        /// If set to false, the initial post tensioning force will be subtraced from the axial load diagram,
        /// producing a smaller interaction diagram.
        /// </summary>
        public bool IncludeInitialPostTensioningForceInAxialLoad { get; set; }
        /// <summary>
        /// Dictionary of material limit states that apply to this analysis, keyed off of the inelastic material.
        /// Each material can have multiple limit states assoicated with it.
        /// </summary>
        [JsonIgnore]
        public Dictionary<IGenericMaterial, List<LimitState>> MaterialLimitStates
        {
            get
            {
                return _materialLimitStates;
            }
            set
            {
                _materialLimitStates = value;

                //_materialLimitStatesList = _materialLimitStates.ToList();
            }
        }

        public List<KeyValuePair<IGenericMaterial, List<LimitState>>> MaterialLimitStatesList
        {
            get
            {
                if (_materialLimitStates != null)
                {
                    return _materialLimitStates.ToList();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                //_materialLimitStatesList = value;
                if (value != null)
                {
                    _materialLimitStates = value.ToDictionary(kv => kv.Key, kv => kv.Value);
                }
            }
        }
        #endregion

        #region Protected Methods
        protected void InstantiateVariables()
        {
            _elementResults = new List<FiberSectionElementResults>();

            _analysisResults = new List<FiberSectionAnalysisResult>();
        }

        protected void ComputeUnitVectors()
        {
            if (AnalysisAngle == 0)
            {
                _unitVectorMajor = new Vector(1, 0);
                _unitVectorMinor = new Vector(0, 1);
            }
            else if (AnalysisAngle == PI / 2.0)
            {
                _unitVectorMajor = new Vector(0, 1);
                _unitVectorMinor = new Vector(-1, 0);
            }
            else if (AnalysisAngle == PI)
            {
                _unitVectorMajor = new Vector(-1, 0);
                _unitVectorMinor = new Vector(0, -1);
            }
            else if (AnalysisAngle == 3.0 * PI / 2.0)
            {
                _unitVectorMajor = new Vector(0, -1);
                _unitVectorMinor = new Vector(1, 0);
            }
            else
            {
                double majorSlope = Tan(AnalysisAngle);
                double minorSlope = Tan(AnalysisAngle + PI / 2.0);

                if (AnalysisAngle > 0 && AnalysisAngle < PI / 2.0)
                {
                    _unitVectorMajor = new Vector(Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))), majorSlope * Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))));
                    _unitVectorMinor = new Vector(-Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))), minorSlope * -Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))));
                }
                else if (AnalysisAngle > PI / 2.0 && AnalysisAngle < PI)
                {
                    _unitVectorMajor = new Vector(-Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))), majorSlope * -Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))));
                    _unitVectorMinor = new Vector(-Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))), minorSlope * -Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))));
                }
                else if (AnalysisAngle > PI && AnalysisAngle < 3.0 * PI / 2.0)
                {
                    _unitVectorMajor = new Vector(-Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))), majorSlope * -Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))));
                    _unitVectorMinor = new Vector(Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))), minorSlope * Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))));
                }
                else if (AnalysisAngle > 3.0 * PI / 2.0 && AnalysisAngle < 2.0 * PI)
                {
                    _unitVectorMajor = new Vector(Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))), majorSlope * Sqrt(1.0 / (1.0 + Pow(majorSlope, 2))));
                    _unitVectorMinor = new Vector(Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))), minorSlope * Sqrt(1.0 / (1.0 + Pow(minorSlope, 2))));
                }
            }
        }

        protected void ComputeNeutralAxisProperties()
        {
            _neutralAxis = new NeutralAxis();

            _neutralAxis.Angle = AnalysisAngle;

            if (AnalysisAngle == PI / 2.0 || AnalysisAngle == 3.0 * PI / 2.0)
            {
                _neutralAxis.Slope = 0;
                _neutralAxis.YIntercept = 0;
                _neutralAxis.NormalSlope = 0;
            }
            else
            {
                _neutralAxis.Slope = Tan(AnalysisAngle);
                _neutralAxis.YIntercept = SectionModel.SectionCentroid.Y - _neutralAxis.Slope * SectionModel.SectionCentroid.X;

                if (_neutralAxis.Slope == 0)
                {
                    _neutralAxis.NormalSlope = 0;   //If the slope is zero, then the inverse is infinity.
                                                    //So we will set to zero and deal with it later.
                }
                else
                {
                    _neutralAxis.NormalSlope = -1 / _neutralAxis.Slope;
                }
            }

            _neutralAxis.Line = ComputeNeutralAxisLine(_neutralAxis);
        }

        protected Edge ComputeNeutralAxisLine(NeutralAxis nA)
        {
            PointD p1 = null;
            PointD p2 = null;

            double pX = 10;
            double nX = -10;

            if (nA.Angle >= 0 && nA.Angle < PI / 2.0)
            {
                p1 = new PointD(nX, nA.Slope * nX + nA.YIntercept);
                p2 = new PointD(pX, nA.Slope * pX + nA.YIntercept);
            }
            else if (nA.Angle == PI / 2.0)
            {
                p1 = new PointD(SectionModel.SectionCentroid.X, SectionModel.SectionCentroid.Y + nX);
                p2 = new PointD(SectionModel.SectionCentroid.X, SectionModel.SectionCentroid.Y + pX);
            }
            else if (nA.Angle > PI / 2.0 && nA.Angle < 3.0 * PI / 2.0)
            {
                p1 = new PointD(SectionModel.SectionCentroid.X + pX, nA.Slope * (SectionModel.SectionCentroid.X + pX) + nA.YIntercept);
                p2 = new PointD(SectionModel.SectionCentroid.X + nX, nA.Slope * (SectionModel.SectionCentroid.X + nX) + nA.YIntercept);
            }
            else if (nA.Angle == 3.0 * PI / 2.0)
            {
                p1 = new PointD(SectionModel.SectionCentroid.X, SectionModel.SectionCentroid.Y + pX);
                p2 = new PointD(SectionModel.SectionCentroid.X, SectionModel.SectionCentroid.Y + nX);
            }
            else if (nA.Angle > 3.0 * PI / 2.0 && nA.Angle < 2.0 * PI)
            {
                p1 = new PointD(SectionModel.SectionCentroid.X + nX, nA.Slope * (SectionModel.SectionCentroid.X + nX) + nA.YIntercept);
                p2 = new PointD(SectionModel.SectionCentroid.X + pX, nA.Slope * (SectionModel.SectionCentroid.X + pX) + nA.YIntercept);
            }

            return new Edge(p1, p2);
        }

        protected bool ValidateInputs(out string message)
        {
            message = string.Empty;

            if (SectionModel == null)
            {
                message = "Fiber section model was null";
                return false;
            }

            if (AnalysisAngle < 0 || AnalysisAngle > 2.0 * PI)
            {
                message = "Analysis angle was not between 0 and 2*PI";
                return false;
            }

            if (NumberOfPoints <= 0)
            {
                message = "The number of points making up the PM diagram is less than or equal to 0";
                return false;
            }

            return true;

        }

        /// <summary>
        /// Returns the perpendicular distance between the outermost tension and compression points in the section.
        /// </summary>
        /// <param name="nA">Neutral axis</param>
        /// <param name="furthestPoints">The furthest tension and compression points</param>
        /// <returns></returns>
        protected double GetTensionCompressionDistance(NeutralAxis nA, FurthestTensionCompressionPoints furthestPoints)
        {
            Edge parallelLine = nA.Line.CreateParallelEdgeAtPoint(furthestPoints.CompressionPoint);

            return parallelLine.PerpendicularDistance(furthestPoints.TensionPoint);
        }

        /// <summary>
        /// Returns the phi factor for the section based on the maximum rebar tension strain
        /// </summary>
        /// <param name="maxRebarTensionStrain">Maximum tension strain (tension is negative, so this should be the largest negative value for strain)</param>
        /// <returns></returns>
        protected double GetPhiFactor(double maxRebarTensionStrain)
        {
            double phiFactor = 1.0;

            if (maxRebarTensionStrain >= -0.002)
            {
                if (SectionModel.Confinement.Equals(ConfinementType.ClosedTies) == true)
                {
                    phiFactor = 0.65;
                }
                else
                {
                    phiFactor = 0.75;
                }
            }
            else if (maxRebarTensionStrain < -0.002 && maxRebarTensionStrain > -0.005)
            {
                if (SectionModel.Confinement.Equals(ConfinementType.ClosedTies) == true)
                {
                    phiFactor = 0.65 + (Abs(maxRebarTensionStrain) - 0.002) * (250.0 / 3.0);
                }
                else
                {
                    phiFactor = 0.75 + (Abs(maxRebarTensionStrain) - 0.002) * 50;
                }
            }
            else
            {
                phiFactor = 0.9;
            }

            return phiFactor;
        }

        /// <summary>
        /// Returns the element that will cause the analysis to terminate based on its material tension limit state.
        /// </summary>
        /// <param name="nA">Neutral axis</param>
        /// <param name="furthestPoints">Furthest tension and compression points on the section</param>
        /// <param name="phi">Current curvature</param>
        /// <returns></returns>
        protected LimitStateElement GetTensionLimitState(NeutralAxis nA, FurthestTensionCompressionPoints furthestPoints, double phi)
        {
            LimitStateElement lsElem = new LimitStateElement();

            double maxTensFiberStrain = -1; //Used to compute and record the smallest tension strain (the numerically smallest negative number)
                                            //in the furthest tension fiber that causes a termination limit state to be reached.

            List<IFiberSectionElement> allElements = SectionModel.AllFiberSectionElements;

            for (int i = 0; i < allElements.Count; i++)
            {
                IFiberSectionElement elem = allElements[i];

                PointD c = elem.Centroid();

                Edge edgeThroughCentroid = nA.Line.CreateParallelEdgeAtPoint(c);

                double dist = edgeThroughCentroid.PerpendicularDistance(furthestPoints.TensionPoint);

                double maxLimitStateTensionStrain = -10; //Just assign a large negative number to start

                bool elemHasTerminationLimitState = false;

                if (MaterialLimitStates.TryGetValue(elem.Material, out List<LimitState> lsList) == true)
                {
                    for (int j = 0; j < lsList.Count; j++)
                    {
                        LimitState ls = lsList[j];

                        if (ls.TerminationLimitState == true)
                        {
                            //Only check termination limit states
                            elemHasTerminationLimitState = true;

                            double lsTensionStrain = -1.0 * ls.TensionLimitStrain;  //Multiply by -1.0 because the limit strains are all stored as positive
                                                                                    //regardless of tension or compression.  But tension is treated as negative.

                            maxLimitStateTensionStrain = Max(maxLimitStateTensionStrain, lsTensionStrain);
                        }
                    }
                }

                double sectionStrain = maxLimitStateTensionStrain;

                if (elem.GetType().Equals(typeof(FiberSectionPostTensioningElement)) == true)
                {
                    FiberSectionPostTensioningElement ptElem = (FiberSectionPostTensioningElement)elem;

                    if (ptElem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedGroutedReleased) == true || ptElem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedReleasedGrouted) == true)
                    {
                        sectionStrain = maxLimitStateTensionStrain - ptElem.InitialPrestrain;
                    }
                    else
                    {
                        //This is for unbonded tendons.  The strain in unbonded tendons will remain relatively constant for all curvatures as any
                        //change in strains are distributed along the entire length of the tendons, making them insignificant.
                        //As such, simply set the section strain to a large negative number and ignore it.

                        sectionStrain = -10;
                    }
                }

                if (elemHasTerminationLimitState == true)
                {
                    double furthestTensionFiberStrain = sectionStrain - phi * dist;

                    if (furthestTensionFiberStrain > maxTensFiberStrain)
                    {
                        maxTensFiberStrain = furthestTensionFiberStrain;

                        lsElem = new LimitStateElement(elem, furthestTensionFiberStrain, maxLimitStateTensionStrain);
                    }
                }
            }

            return lsElem;
        }

        /// <summary>
        /// Returns the element that will cause the analysis to terminate based on its material compression limit state.
        /// </summary>
        /// <param name="nA">Neutral axis</param>
        /// <param name="furthestPoints">Furthest tension and compression points on the section</param>
        /// <param name="phi">Current curvature</param>
        /// <returns></returns>
        protected LimitStateElement GetCompressionLimitState(NeutralAxis nA, FurthestTensionCompressionPoints furthestPoints, double phi)
        {
            LimitStateElement lsElem = new LimitStateElement();

            double minCompressionFiberStrain = 1;   //Used to compute and record the smallest compression strain
                                                    //in the furthest compression fiber that causes a termination limit state to be reached.

            List<IFiberSectionElement> allElements = SectionModel.AllFiberSectionElements;

            for (int i = 0; i < allElements.Count; i++)
            {
                IFiberSectionElement elem = allElements[i];

                if (elem.GetType().Equals(typeof(FiberSectionPostTensioningElement)) == false)
                {
                    //No point in checking post-tensioning elements for compression limit strains.
                    //There should never be a case where they go into compression.

                    PointD c = elem.Centroid();

                    Edge edgeThroughCentroid = nA.Line.CreateParallelEdgeAtPoint(c);

                    double dist = edgeThroughCentroid.PerpendicularDistance(furthestPoints.TensionPoint);

                    double minLimitStateCompressionStrain = 10; //Just assign a large number to start

                    bool elemHasTerminationLimitState = false;

                    if (MaterialLimitStates.TryGetValue(elem.Material, out List<LimitState> lsList) == true)
                    {
                        for (int j = 0; j < lsList.Count; j++)
                        {
                            LimitState ls = lsList[j];

                            if (ls.TerminationLimitState == true)
                            {
                                //Only check termination limit states
                                elemHasTerminationLimitState = true;

                                double lsCompressionStrain = ls.CompressionLimitStrain;  //Multiply by -1.0 because the limit strains are all stored as positive
                                                                                         //regardless of tension or compression.  But tension is treated as negative.

                                minLimitStateCompressionStrain = Min(minLimitStateCompressionStrain, lsCompressionStrain);
                            }
                        }
                    }

                    if (elemHasTerminationLimitState == true)
                    {
                        double furthestCompressionFiberStrain = minLimitStateCompressionStrain + phi * dist;

                        if (furthestCompressionFiberStrain < minCompressionFiberStrain)
                        {
                            minCompressionFiberStrain = furthestCompressionFiberStrain;

                            lsElem = new LimitStateElement(elem, furthestCompressionFiberStrain, minLimitStateCompressionStrain);
                        }
                    }
                }
            }

            return lsElem;
        }
        #endregion
    }
}
