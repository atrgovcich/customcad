﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.LimitStates;
using Materials.Inelastic.Concrete;

namespace FiberSectionAnalysis.General
{
    [Serializable]
    public abstract class GeneralAnalysisCase
    {
        #region Properties
        /// <summary>
        /// Unique name of the analysis case
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Mesh size to use for fiber section discretization
        /// </summary>
        public double MeshSize { get; set; } = 2;
        /// <summary>
        /// Approximate number of points that will make up the analysis
        /// </summary>
        public int NumberOfPoints { get; set; } = 50;
        /// <summary>
        /// List of applicable limit states
        /// </summary>
        public List<LimitState> LimitStates { get; set; }
        /// <summary>
        /// Type of confinement that the section uses.  Used to determine phi factor.
        /// </summary>
        public ConfinementType ConfinementType { get; set; } = ConfinementType.ClosedTies;
        #endregion
    }
}
