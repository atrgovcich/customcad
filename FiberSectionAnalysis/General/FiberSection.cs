﻿using System;
using System.Collections.Generic;
using System.Text;
using Materials.Inelastic.Concrete;
using Utilities.Geometry;
using static System.Math;
using FiberSectionAnalysis.Elements;
using Utilities.DataStructures.QuadTree;
using Utilities.Containers;
using FiberSectionAnalysis.Geometry;
using Materials;
using TriangleNet.Geometry;
using TriangleNetHelper.Geometry;
using TriangleNetHelper.ContainerClasses;
using TriangleNetHelper;
using FiberSectionAnalysis.Helpers;
using Utilities.PolyBool;
using System.Linq;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.General
{
    public class FiberSection
    {
        #region Private Backing Fields
        [JsonProperty]
        private double _sectionArea;
        [JsonProperty]
        private PointD _sectionCentroid;
        [JsonProperty]
        private double _sectionMaxX;
        [JsonProperty]
        private double _sectionMaxY;
        [JsonProperty]
        private double _sectionMinX;
        [JsonProperty]
        private double _sectionMinY;

        private QuadTree<FiberSectionMeshElement> _meshQuadTree;
        #endregion

        #region Public Properties
        public List<IClosedShapeFS> SelectedShapes { get; set; } = new List<IClosedShapeFS>();
        /// <summary>
        /// The list of fiber mesh elements in the section
        /// </summary>
        public List<FiberSectionMeshElement> FiberMeshElements { get; set; }
        /// <summary>
        /// The list of fiber rebar elements in the section
        /// </summary>
        public List<FiberSectionRebarElement> FiberRebarElements { get; set; }
        /// <summary>
        /// The list of fiber post-tensioning elements in the section
        /// </summary>
        public List<FiberSectionPostTensioningElement> FiberPostTensioningElements { get; set; }
        /// <summary>
        /// A list of all of the fiber section elements (mesh, rebar, and post tensioning) in the section
        /// </summary>
        [JsonIgnore]
        public List<IFiberSectionElement> AllFiberSectionElements
        {
            get
            {
                List<IFiberSectionElement> allElements = new List<IFiberSectionElement>(FiberMeshElements.Count + FiberRebarElements.Count + FiberPostTensioningElements.Count);

                allElements.AddRange(FiberMeshElements);

                allElements.AddRange(FiberRebarElements);

                allElements.AddRange(FiberPostTensioningElements);

                return allElements;
            }
        }
        /// <summary>
        /// Type of confinement (spiral or hoops)
        /// </summary>
        public ConfinementType Confinement { get; set; }
        /// <summary>
        /// The gross area enclosed by the outer boundary of the section
        /// </summary>
        [JsonIgnore]
        public double SectionArea
        {
            get
            {
                return _sectionArea;
            }
        }
        /// <summary>
        /// The centroid of the outer boundary of the section
        /// </summary>
        [JsonIgnore]
        public PointD SectionCentroid
        {
            get
            {
                return _sectionCentroid;
            }
        }
        /// <summary>
        /// The maximum centroidal X-coordinate of all fiber elements
        /// </summary>
        [JsonIgnore]
        public double SectionMaxX
        {
            get
            {
                return _sectionMaxX;
            }
        }
        /// <summary>
        /// The minimum centroidal X-coordinate of all fiber elements
        /// </summary>
        [JsonIgnore]
        public double SectionMinX
        {
            get
            {
                return _sectionMinX;
            }
        }
        /// <summary>
        /// The maximum centroidal Y-coordinate of all fiber elements
        /// </summary>
        [JsonIgnore]
        public double SectionMaxY
        {
            get
            {
                return _sectionMaxY;
            }
        }
        /// <summary>
        /// The minimum centroidal Y-coordinate of all fiber elements
        /// </summary>
        [JsonIgnore]
        public double SectionMinY
        {
            get
            {
                return _sectionMinY;
            }
        }

        [JsonIgnore]
        public QuadTree<FiberSectionMeshElement> MeshQuadTree
        {
            get
            {
                return _meshQuadTree;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the section properties of the shape and stores them in fields
        /// </summary>
        public void CalculateSectionProperties()
        {
            double sumArea = 0;
            double sumAreaCx = 0;
            double sumAreaCy = 0;

            double maxX = 0;
            double minX = 0;
            double maxY = 0;
            double minY = 0;

            for (int i =0; i < FiberMeshElements.Count; i++)
            {
                FiberSectionMeshElement elem = FiberMeshElements[i];

                PointD cP = elem.Centroid();

                double area = elem.Area();

                if (i == 0)
                {
                    maxX = cP.X;
                    minX = cP.X;
                    maxY = cP.Y;
                    minY = cP.Y;
                }
                else
                {
                    maxX = Max(maxX, cP.X);
                    minX = Min(minX, cP.X);
                    maxY = Max(maxY, cP.Y);
                    minY = Min(minY, cP.Y);
                }

                sumArea += area;
                sumAreaCx += area * cP.X;
                sumAreaCy += area * cP.Y;
            }

            _sectionCentroid = new PointD(sumAreaCx / sumArea, sumAreaCy / sumArea);
            _sectionArea = sumArea;
            _sectionMaxX = maxX;
            _sectionMaxY = maxY;
            _sectionMinX = minX;
            _sectionMinY = minY;
        }

        /// <summary>
        /// Returns the furthest tension and compression points on the section, measured from
        /// the centroid and perpendicular to the neutral axis
        /// </summary>
        /// <param name="nA">Neutral axis</param>
        /// <returns></returns>
        public FurthestTensionCompressionPoints GetFurthestTensionCompressionPoints(NeutralAxis nA)
        {
            double maxCompDist = 0;
            double maxTensDist = 0;

            PointD compPoint = new PointD(0, 0);
            PointD tensPoint = new PointD(0, 0);

            for (int i = 0; i < FiberMeshElements.Count; i++)
            {
                PointD c = FiberMeshElements[i].Centroid();

                //If the point is to the left of the neutral axis line, it is on the compression side
                double dist = 0;

                if (nA.Angle == 0 || nA.Angle == PI)
                {
                    dist = Abs(c.Y - nA.Line.StartPoint.Y);
                }
                else if (nA.Angle == PI / 2.0 || nA.Angle == 3.0 * PI / 2.0)
                {
                    dist = Abs(c.X - nA.Line.StartPoint.X);
                }
                else
                {
                    double nAnormalYint = c.Y - nA.NormalSlope * c.X;

                    double xInt = (nAnormalYint - nA.YIntercept) / (nA.Slope - nA.NormalSlope);

                    double yInt = nA.NormalSlope * xInt + nAnormalYint;

                    dist = Sqrt(Pow(c.Y - yInt, 2) + Pow(c.X - xInt, 2));
                }

                SideOfLine side = nA.Line.WhichSide(c);

                if (side == SideOfLine.Left)
                {
                    if (dist > maxCompDist)
                    {
                        maxCompDist = dist;

                        compPoint = c;
                    }
                }
                else if (side == SideOfLine.Right)
                {
                    if (dist > maxTensDist)
                    {
                        maxTensDist = dist;

                        tensPoint = c;
                    }
                }
            }

            return new FurthestTensionCompressionPoints(tensPoint, compPoint);
        }

        /// <summary>
        /// Builds the quad tree of mesh elements
        /// </summary>
        public void BuildMeshQuadTree()
        {
            if (FiberMeshElements != null && FiberMeshElements.Count > 0)
            {
                _meshQuadTree = new QuadTree<FiberSectionMeshElement>();

                MinMaxBounds modelBounds = new MinMaxBounds();

                for (int i = 0; i < FiberMeshElements.Count; i++)
                {
                    MinMaxBounds elemBounds = FiberMeshElements[i].GetMinMaxBounds();

                    modelBounds = modelBounds.GetMaximumBounds(elemBounds);
                }

                Rect qtBounds = new Rect(new PointQT(modelBounds.MinX - 1, modelBounds.MinY - 1), new PointQT(modelBounds.MaxX + 1, modelBounds.MaxY + 1));

                _meshQuadTree.Bounds = qtBounds;

                for (int i = 0; i < FiberMeshElements.Count; i++)
                {
                    Rect elemBounds = FiberMeshElements[i].BoundingRectangle();

                    _meshQuadTree.Insert(FiberMeshElements[i], elemBounds);
                }
            }
        }

        public static FiberSection BuildFiberSectionFromListOfElements(List<GenericShape> shapeList, double meshSize, int precision)
        {
            FiberSection sectionModel = new FiberSection();

            List<PrePolygon> preMesh = sectionModel.CreateTriangleNetMesh(shapeList, meshSize, precision);

            List<GenericPolygon> polygons = TriangleNET.TriangleNetHelperMethods.ConvertFromPrePolgyonsToGenericPolygons(preMesh);

            List<FiberSectionMeshElement> meshElements = GeometryHelpers.ConvertGenericPolygonsToMeshElements(polygons);

            List<FiberSectionRebarElement> rebarElements = sectionModel.GetFiberRebarElements(shapeList);

            List<FiberSectionPostTensioningElement> postTensioningElements = sectionModel.GetFiberPostTensioningElements(shapeList);

            sectionModel.FiberMeshElements = meshElements;
            sectionModel.FiberRebarElements = rebarElements;
            sectionModel.FiberPostTensioningElements = postTensioningElements;

            sectionModel.BuildMeshQuadTree();

            sectionModel.AssignMaterialsToFiberElements(shapeList, meshSize);

            return sectionModel;
        }
        #endregion

        #region Private Methods
        private List<FiberSectionRebarElement> GetFiberRebarElements(List<GenericShape> shapeList)
        {
            List<FiberSectionRebarElement> rebarElements = new List<FiberSectionRebarElement>();

            for (int i = 0; i < shapeList.Count; i++)
            {
                GenericShape element = shapeList[i];

                if (element.GetType().Equals(typeof(RebarSectionFS)) == true)
                {
                    RebarSectionFS rebarSection = (RebarSectionFS)element;

                    IGenericMaterial inelMat = rebarSection.Material;

                    if (inelMat != null)
                    {
                        FiberSectionRebarElement rebarElement = new FiberSectionRebarElement(rebarSection.Centroid(), rebarSection.RebarSize, inelMat);

                        rebarElements.Add(rebarElement);
                    }
                }
            }

            return rebarElements;
        }

        private List<FiberSectionPostTensioningElement> GetFiberPostTensioningElements(List<GenericShape> shapeList)
        {
            List<FiberSectionPostTensioningElement> ptElements = new List<FiberSectionPostTensioningElement>();

            for (int i = 0; i < shapeList.Count; i++)
            {
                GenericShape element = shapeList[i];

                if (element.GetType().Equals(typeof(PostTensioningSectionFS)) == true)
                {
                    PostTensioningSectionFS ptSection = (PostTensioningSectionFS)element;

                    IGenericMaterial inelMat = ptSection.TypeOfPT.Material;

                    if (inelMat != null)
                    {
                        FiberSectionPostTensioningElement ptElement = new FiberSectionPostTensioningElement(ptSection.Center, ptSection.TypeOfPT, ptSection.InitialPrestress, ptSection.JackingAndGrouting);

                        ptElements.Add(ptElement);
                    }
                }
            }

            return ptElements;
        }

        private List<PrePolygon> CreateTriangleNetMesh(List<GenericShape> elementList, double meshSize, double precision)
        {
            double orthoTolerance = 1.0 * Pow(10.0, -precision);

            List<IShape> preShapeList = new List<IShape>();

            for (int i = 0; i < elementList.Count; i++)
            {
                GenericShape elem = elementList[i];

                if (elementList[i].GetType().Equals(typeof(RebarSectionFS)) == true || elementList[i].GetType().Equals(typeof(PostTensioningSectionFS)) == true)
                {
                    continue;
                }

                GenericShape discretizedElement = elem.GetDiscretizedPoly(meshSize);

                if (discretizedElement != null)
                {
                    if (discretizedElement.GetType().Equals(typeof(GenericPolygon)) == true || discretizedElement.GetType().IsSubclassOf(typeof(GenericPolygon)) == true)
                    {
                        preShapeList.Add(TriangleNET.TriangleNetHelperMethods.ConvertGenericPolygontoPrePolygon((GenericPolygon)discretizedElement));
                    }
                    else if (discretizedElement.GetType().Equals(typeof(GenericPolyline)) == true)
                    {
                        preShapeList.Add(TriangleNET.TriangleNetHelperMethods.ConvertGenericPolylineGtoPrePolyline((GenericPolyline)discretizedElement));
                    }
                    else if (discretizedElement.GetType().Equals(typeof(GenericPoint)) == true)
                    {
                        preShapeList.Add(TriangleNET.TriangleNetHelperMethods.ConvertGenericPointToPreVertex((GenericPoint)discretizedElement));
                    }
                }
            }

            if (preShapeList.Count > 0)
            {
                MeshingOptions meshOptions = new MeshingOptions();

                TriangleHelper helper = new TriangleHelper(preShapeList, meshSize, meshOptions, orthoTolerance);

                helper.CreateMesh();

                List<PrePolygon> preMesh = helper.RetrieveMesh;

                return preMesh;
            }
            else
            {
                return null;
            }
        }

        private void AssignMaterialsToFiberElements(List<GenericShape> shapeList, double meshSize)
        {
            List<IClosedShapeFS> allClosedElements = GetAllClosedELementsSelected(shapeList);

            SplitCrossingClosedShapes(ref allClosedElements, meshSize);

            Dictionary<IClosedShapeFS, List<IClosedShapeFS>> objectHeirarchy = GetObjectHeirarchies(allClosedElements);

            List<IClosedShapeFS> elementsAlreadyAssigned = new List<IClosedShapeFS>();

            List<IClosedShapeFS> remainingElements = new List<IClosedShapeFS>(allClosedElements);

            while (remainingElements.Count > 0)
            {
                List<IClosedShapeFS> elementsNotContainedByOthers = new List<IClosedShapeFS>();

                for (int i = 0; i < remainingElements.Count; i++)
                {
                    if (GetNumberOfElementsThatContainMe(objectHeirarchy, remainingElements, remainingElements[i]) == 0)
                    {
                        elementsNotContainedByOthers.Add(remainingElements[i]);
                    }
                }

                for (int i = 0; i < elementsNotContainedByOthers.Count; i++)
                {
                    //Get potential elements from quad tree
                    List<FiberSectionMeshElement> nearbyElements = MeshQuadTree.GetNodesInside(elementsNotContainedByOthers[i].BoundingRectangle()).ToList();

                    IGenericClosedShape cs = elementsNotContainedByOthers[i];

                    for (int j = 0; j < nearbyElements.Count; j++)
                    {
                        FiberSectionMeshElement meshElem = nearbyElements[j];

                        if (elementsNotContainedByOthers[i].ContainsPoint(meshElem.Centroid()) == true)
                        {
                            meshElem.Material = elementsNotContainedByOthers[i].Material;
                        }
                    }

                    remainingElements.Remove(elementsNotContainedByOthers[i]);
                }
            }

            List<FiberSectionMeshElement> nullMaterialElements = new List<FiberSectionMeshElement>();

            for (int i = 0; i < FiberMeshElements.Count; i++)
            {
                if (FiberMeshElements[i].Material == null)
                {
                    nullMaterialElements.Add(FiberMeshElements[i]);
                }
            }

            for (int i = 0; i < nullMaterialElements.Count; i++)
            {
                FiberMeshElements.Remove(nullMaterialElements[i]);
            }

            if (FiberRebarElements != null && FiberRebarElements.Count > 0)
            {
                for (int i = 0; i < FiberRebarElements.Count; i++)
                {
                    FiberSectionRebarElement bar = FiberRebarElements[i];

                    List<FiberSectionMeshElement> nearbyElements = MeshQuadTree.GetNodesInside(bar.Center.BoundingRectangle()).ToList();

                    if (nearbyElements != null)
                    {
                        for (int j = 0; j < nearbyElements.Count; j++)
                        {
                            if (nearbyElements[j].ContainsPoint(bar.Center) == true)
                            {
                                bar.MaterialRemoved = nearbyElements[j].Material;

                                break;
                            }
                        }
                    }

                }
            }

            if (FiberPostTensioningElements != null && FiberPostTensioningElements.Count > 0)
            {
                for (int i = 0; i < FiberPostTensioningElements.Count; i++)
                {
                    FiberSectionPostTensioningElement pt = FiberPostTensioningElements[i];

                    List<FiberSectionMeshElement> nearbyElements = MeshQuadTree.GetNodesInside(pt.Center.BoundingRectangle()).ToList();

                    if (nearbyElements != null)
                    {
                        for (int j = 0; j < nearbyElements.Count; j++)
                        {
                            if (nearbyElements[j].ContainsPoint(pt.Center) == true)
                            {
                                pt.MaterialRemoved = nearbyElements[j].Material;

                                break;
                            }
                        }
                    }

                }
            }
        }

        private List<IClosedShapeFS> GetAllClosedELementsSelected(List<GenericShape> shapeList)
        {
            List<IClosedShapeFS> allClosedElements = new List<IClosedShapeFS>();

            for (int i = 0; i < shapeList.Count; i++)
            {
                GenericShape elem = shapeList[i];

                IClosedShapeFS closedElem = elem as IClosedShapeFS;

                if (closedElem != null)
                {
                    if (elem.GetType().Equals(typeof(RebarSectionFS)) == true || elem.GetType().Equals(typeof(PostTensioningSectionFS)) == true)
                    {
                        //Don't add rebar or pt sections
                        continue;
                    }

                    if (closedElem.IsRebar == false)
                    {
                        //Only add elements that are not plan rebar
                        if (closedElem.Material != null)
                        {
                            //Only add elements that have a material assigned
                            if (closedElem.Hole == false)
                            {
                                allClosedElements.Add(closedElem);
                            }
                        }
                    }
                }
            }

            return allClosedElements;
        }

        private void SplitCrossingClosedShapes(ref List<IClosedShapeFS> allClosedElements, double meshSize)
        {
            int ind = 0;

            while (ind < allClosedElements.Count - 1)
            {
                IClosedShapeFS elem1 = allClosedElements[ind];

                GenericPolygon poly1 = null;

                if (elem1.GetType().Equals(typeof(GenericPolygon)) == true || elem1.GetType().IsSubclassOf(typeof(GenericPolygon)) == true)
                {
                    poly1 = (GenericPolygon)elem1;
                }
                else
                {
                    poly1 = (GenericPolygon)elem1.GetDiscretizedPoly(meshSize);
                }

                if (poly1 != null)
                {
                    int innerInd = ind + 1;

                    int startCount = allClosedElements.Count;

                    while (innerInd < startCount)
                    {
                        IClosedShapeFS elem2 = allClosedElements[innerInd];

                        GenericPolygon poly2 = null;

                        if (elem2.GetType().Equals(typeof(GenericPolygon)) == true || elem2.GetType().IsSubclassOf(typeof(GenericPolygon)) == true)
                        {
                            poly2 = (GenericPolygon)elem2;
                        }
                        else
                        {
                            poly2 = (GenericPolygon)elem2.GetDiscretizedPoly(meshSize);
                        }

                        if (poly2 != null)
                        {
                            if (poly1.IsFullyWithin(poly2) || poly2.IsFullyWithin(poly1))
                            {
                                //do nothing

                            }
                            else
                            {
                                List<GenericPolygon> intersectedPolygons = PolyBoolHelperMethods.BooleanIntersection(poly1, poly2);

                                List<PolygonFS> intersectedFiberPolygons;

                                if (intersectedPolygons != null && intersectedPolygons.Count > 0)
                                {
                                    intersectedFiberPolygons = new List<PolygonFS>(intersectedPolygons.Count);

                                    for (int i = 0; i < intersectedPolygons.Count; i++)
                                    {
                                        PolygonFS pgFS = new PolygonFS(intersectedPolygons[i].Points);

                                        pgFS.Material = elem2.Material;

                                        intersectedFiberPolygons.Add(pgFS);
                                    }

                                    List<GenericPolygon> differencedPolygons = PolyBoolHelperMethods.BooleanDifference(poly2, poly1);

                                    List<PolygonFS> differencedFiberPolygons;

                                    if (differencedPolygons != null && differencedPolygons.Count > 0)
                                    {
                                        differencedFiberPolygons = new List<PolygonFS>(differencedPolygons.Count);

                                        for (int i = 0; i < differencedPolygons.Count; i++)
                                        {
                                            PolygonFS pgFS = new PolygonFS(differencedPolygons[i].Points);

                                            pgFS.Material = elem2.Material;

                                            differencedFiberPolygons.Add(pgFS);
                                        }

                                        allClosedElements.RemoveAt(innerInd);

                                        allClosedElements.InsertRange(innerInd, differencedFiberPolygons);

                                        innerInd += differencedPolygons.Count - 1;

                                        startCount += differencedPolygons.Count - 1;

                                        //allClosedElements.InsertRange(innerInd + 1, intersectedFiberPolygons);

                                        //innerInd += intersectedPolygons.Count;

                                        //startCount += intersectedPolygons.Count;
                                    }
                                }
                            }
                        }

                        innerInd++;
                    }
                }
                ind++;
            }
        }

        private Dictionary<IClosedShapeFS, List<IClosedShapeFS>> GetObjectHeirarchies(List<IClosedShapeFS> allClosedELements)
        {
            Dictionary<IClosedShapeFS, List<IClosedShapeFS>> objectHeirarchy = new Dictionary<IClosedShapeFS, List<IClosedShapeFS>>();

            for (int i = 0; i < allClosedELements.Count; i++)
            {
                IClosedShapeFS elem1 = allClosedELements[i];

                List<IClosedShapeFS> elementsWithin = new List<IClosedShapeFS>();

                for (int j = 0; j < allClosedELements.Count; j++)
                {
                    if (j == i)
                    {
                        continue;
                    }

                    IClosedShapeFS elem2 = allClosedELements[j];

                    if (elem2.IsFullyWithin(elem1) == true)
                    {
                        elementsWithin.Add(elem2);
                    }
                }

                objectHeirarchy.Add(elem1, elementsWithin);
            }

            return objectHeirarchy;
        }

        public int GetNumberOfElementsThatContainMe(Dictionary<IClosedShapeFS, List<IClosedShapeFS>> objectHeirarchy, List<IClosedShapeFS> filterList, IClosedShapeFS elem)
        {
            int count = 0;

            foreach (KeyValuePair<IClosedShapeFS, List<IClosedShapeFS>> kvp in objectHeirarchy)
            {
                if (filterList.Contains(kvp.Key) == true)
                {
                    if (kvp.Value.Contains(elem) == true)
                    {
                        count++;
                    }
                }
            }

            return count;
        }
        #endregion

    }
}
