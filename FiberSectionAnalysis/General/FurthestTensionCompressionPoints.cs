﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.General
{
    public class FurthestTensionCompressionPoints
    {
        #region Public Properties
        /// <summary>
        /// The furthest point on the section in tension, measured perpendicular to the bending axis
        /// </summary>
        public PointD TensionPoint { get; set; }
        /// <summary>
        /// The furthest point on the section in compression, measured perpendicular to the bending axis
        /// </summary>
        public PointD CompressionPoint { get; set; }
        #endregion

        #region Constructors
        [JsonConstructor]
        public FurthestTensionCompressionPoints()
        {

        }

        public FurthestTensionCompressionPoints(PointD tensPoint, PointD compPoint)
        {
            TensionPoint = tensPoint;
            CompressionPoint = compPoint;
        }
        #endregion
    }
}
