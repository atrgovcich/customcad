﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiberSectionAnalysis.Results
{
    public class PostTensioningResult : FiberElementResult
    {
        #region Public Parameters
        /// <summary>
        /// The force that was removed due to the removal of material behind the bar
        /// </summary>
        public double ForceRemoved { get; set; }
        /// <summary>
        /// The moment about the X-axis that was removed due to the removal of material behind the bar
        /// </summary>
        public double MxRemoved { get; set; }
        /// <summary>
        /// The moment about the Y-axis that was removed due to the removal of material behind the bar
        /// </summary>
        public double MyRemoved { get; set; }
        /// <summary>
        /// The moment about the Major-axis that was removed due to the removal of material behind the bar
        /// </summary>
        public double MmajRemoved { get; set; }
        /// <summary>
        /// The moment about the Minor-axis that was removed due to the removal of material behind the bar
        /// </summary>
        public double MminRemoved { get; set; }
        #endregion
    }
}
