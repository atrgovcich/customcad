﻿using System.Collections.Generic;
using FiberSectionAnalysis.Elements;

namespace FiberSectionAnalysis.Results
{
    public class FiberSectionElementResults
    {
        #region Public Properties
        public double Curvature { get; set; }

        public Dictionary<FiberSectionMeshElement, FiberElementResult> MeshElementResults { get; set; }

        public Dictionary<FiberSectionRebarElement, RebarSectionResult> RebarElementResults { get; set; }

        public Dictionary<FiberSectionPostTensioningElement, PostTensioningResult> PostTensioningElementResults { get; set; }
        #endregion

        #region Constructors
        public FiberSectionElementResults()
        {
            MeshElementResults = new Dictionary<FiberSectionMeshElement, FiberElementResult>();

            RebarElementResults = new Dictionary<FiberSectionRebarElement, RebarSectionResult>();

            PostTensioningElementResults = new Dictionary<FiberSectionPostTensioningElement, PostTensioningResult>();
        }

        public FiberSectionElementResults(double curvature) : this()
        {
            Curvature = curvature;
        }
        #endregion
    }
}
