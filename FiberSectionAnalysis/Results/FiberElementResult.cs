﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiberSectionAnalysis.Results
{
    public class FiberElementResult
    {
        #region Public Properties
        /// <summary>
        /// Strain
        /// </summary>
        public double Strain { get; set; }
        /// <summary>
        /// Stress
        /// </summary>
        public double Stress { get; set; }
        /// <summary>
        /// Force
        /// </summary>
        public double Force { get; set; }
        /// <summary>
        /// Moment about the X-axis
        /// </summary>
        public double Mx { get; set; }
        /// <summary>
        /// Moment about the Y-axis
        /// </summary>
        public double My { get; set; }
        /// <summary>
        /// Moment about the major axis (axis of bending)
        /// </summary>
        public double Mmaj { get; set; }
        /// <summary>
        /// Moment about the minor axis (perpendicular to axis of bending)
        /// </summary>
        public double Mmin { get; set; }
        #endregion
    }
}
