﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.LimitStates;

namespace FiberSectionAnalysis.Results
{
    public class MomentCurvatureAnalysisResult : FiberSectionAnalysisResult
    {
        #region Public Properties
        /// <summary>
        /// The applied axial force at which the moment curvature analysis was run
        /// </summary>
        public double AppliedAxialForce { get; set; }
        /// <summary>
        /// The difference between the converged upon axial force and the applied axial force
        /// </summary>
        public double AxialForceDifference { get; set; }
        /// <summary>
        /// The number of iterations required for convergence
        /// </summary>
        public int NumberOfIterations { get; set; }
        /// <summary>
        /// List of limit states that were reached at this analysis step.  If the limit state was reached
        /// previously, it is not included in the list.
        /// </summary>
       public List<LimitStateRecord> LimitStatesReached { get; set; } = new List<LimitStateRecord>();
        /// <summary>
        /// The maximum strain in any of the fiber elements (compression strain)
        /// </summary>
        public double MaxFiberElementStrain { get; set; } = 0;
        /// <summary>
        /// The minimum strain in any of the fiber elements (tension strain)
        /// </summary>
        public double MinFiberElementStrain { get; set; } = 0;
        #endregion
    }
}
