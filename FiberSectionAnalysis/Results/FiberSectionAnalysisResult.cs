﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiberSectionAnalysis.Results
{
    public class FiberSectionAnalysisResult
    {
        #region Public Properties
        /// <summary>
        /// Section curvature at which the result is recorded, in rad/in
        /// </summary>
        public double Curvature { get; set; }
        /// <summary>
        /// Neutral axis angle, in radians, measured from positive X-axis
        /// </summary>
        public double NeutralAxisAngle { get; set; }
        /// <summary>
        /// Total axial force in section (positive compression)
        /// </summary>
        public double AxialForce { get; set; }
        /// <summary>
        /// Moment about the X-axis (positive right hand rule)
        /// </summary>
        public double MomentX { get; set; }
        /// <summary>
        /// Moment about the Y-axis (positive right hand rule)
        /// </summary>
        public double MomentY { get; set; }
        /// <summary>
        /// Moment about the major, or bending axis (positive right hand rule)
        /// </summary>
        public double MomentMajor { get; set; }
        /// <summary>
        /// Moment about the minor axis (positive right hand rule)
        /// </summary>
        public double MomentMinor { get; set; }
        /// <summary>
        /// Total reduced axial force in section (positive compression)
        /// </summary>
        public double ReducedAxialForce { get; set; }
        /// <summary>
        /// Reduced moment about the X-axis (positive right hand rule)
        /// </summary>
        public double ReducedMomentX { get; set; }
        /// <summary>
        /// Reduced moment about the Y-axis (positive right hand rule)
        /// </summary>
        public double ReducedMomentY { get; set; }
        /// <summary>
        /// Reduced moment about the major, or bending axis (positive right hand rule)
        /// </summary>
        public double ReducedMomentMajor { get; set; }
        /// <summary>
        /// Reduced moment about the minor axis (positive right hand rule)
        /// </summary>
        public double ReducedMomentMinor { get; set; }
        /// <summary>
        /// The axial force from mesh elements
        /// </summary>
        public double AxialForce_Concrete { get; set; }
        /// <summary>
        /// The axial force from rebar elements
        /// </summary>
        public double AxialForce_Rebar { get; set; }
        /// <summary>
        /// The axial force from post-tensioning elements
        /// </summary>
        public double AxialForce_PT { get; set; }
        /// <summary>
        /// The axial force removed due to the removal of material behind rebar and post-tensioning
        /// </summary>
        public double AxialForce_Removed { get; set; }
        /// <summary>
        /// The moment about the X-axis from mesh elements
        /// </summary>
        public double MomentX_Concrete { get; set; }
        /// <summary>
        /// The moment about the X-axis from rebar elements
        /// </summary>
        public double MomentX_Rebar { get; set; }
        /// <summary>
        /// The moment about the X-axis from PT elements
        /// </summary>
        public double MomentX_PT { get; set; }
        /// <summary>
        /// The moment about the X-axis that was removed due to the removal of material behind rebar and post-tensioning
        /// </summary>
        public double MomentX_Removed { get; set; }
        /// <summary>
        /// The moment about the Y-axis from mesh elements
        /// </summary>
        public double MomentY_Concrete { get; set; }
        /// <summary>
        /// The moment about the Y-axis from rebar elements
        /// </summary>
        public double MomentY_Rebar { get; set; }
        /// <summary>
        /// The moment about the Y-axis from PT elements
        /// </summary>
        public double MomentY_PT { get; set; }
        /// <summary>
        /// The moment about the Y-axis that was removed due to the removal of material behind rebar and post-tensioning
        /// </summary>
        public double MomentY_Removed { get; set; }
        /// <summary>
        /// The moment about the Major-axis from mesh elements
        /// </summary>
        public double MomentMajor_Concrete { get; set; }
        /// <summary>
        /// The moment about the Major-axis from rebar elements
        /// </summary>
        public double MomentMajor_Rebar { get; set; }
        /// <summary>
        /// The moment about the Major-axis from PT elements
        /// </summary>
        public double MomentMajor_PT { get; set; }
        /// <summary>
        /// The moment about the Major-axis that was removed due to the removal of material behind rebar and post-tensioning
        /// </summary>
        public double MomentMajor_Removed { get; set; }
        /// <summary>
        /// The moment about the Minor-axis from mesh elements
        /// </summary>
        public double MomentMinor_Concrete { get; set; }
        /// <summary>
        /// The moment about the Minor-axis from rebar elements
        /// </summary>
        public double MomentMinor_Rebar { get; set; }
        /// <summary>
        /// The moment about the Minor-axis from PT elements
        /// </summary>
        public double MomentMinor_PT { get; set; }
        /// <summary>
        /// The moment about the Minor-axis that was removed due to the removal of material behind rebar and post-tensioning
        /// </summary>
        public double MomentMinor_Removed { get; set; }
        /// <summary>
        /// The ACI 318 phi factor
        /// </summary>
        public double PhiFactor { get; set; }

        public double NeutralAxisDepth { get; set; }

        public string LimitState { get; set; }
        #endregion
    }
}
