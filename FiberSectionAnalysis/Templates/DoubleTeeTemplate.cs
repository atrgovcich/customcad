﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.Geometry;
using Utilities.Geometry;
using Utilities.Extensions.DotNetNative;
using static System.Math;

namespace FiberSectionAnalysis.Templates
{
    public class DoubleTeeTemplate : IFiberSectionTemplate
    {
        #region Public Properties
        public string TypeName { get; } = "Double Tee";
        public string InstanceName { get; set; }
        public List<PolygonFS> Boundaries { get; set; }
        public List<RebarSectionFS> Reinforcement { get; set; }
        public List<PostTensioningSectionFS> PostTensioning { get; set; }
        #endregion

        #region Protected Fields
        protected DoubleTeeDefinition _definition;
        #endregion

        #region Constructors
        public DoubleTeeTemplate(DoubleTeeDefinition definition)
        {
            _definition = definition;
        }
        #endregion

        #region Protected Methods
        protected PolygonFS BuildSectionOuterBoundary()
        {
            if (_definition != null)
            {
                double xDiff = (_definition.BeamWidthTop - _definition.BeamWidthBottom) / 2.0;

                List<PointD> pointList = new List<PointD>();

                //1st point, top left
                PointD p0 = new PointD(0, 0);
                pointList.Add(p0);

                //2nd point, top right
                PointD p1 = new PointD(_definition.FlangeWidth, 0);
                pointList.Add(p1);

                //3rd point
                PointD p2 = new PointD(_definition.FlangeWidth, -_definition.FlangeThickness);
                pointList.Add(p2);

                //4th point
                PointD p3 = new PointD(_definition.FlangeWidth - _definition.FlangeCantilever, -_definition.FlangeThickness);

                if (_definition.ChamferBeamToSlab == true)
                {
                    double chamferAngleRad = _definition.ChamferAngle.ToRadians();

                    double x = _definition.ChamferDepth / Tan(chamferAngleRad);

                    if (_definition.BeamWidthTop == _definition.BeamWidthBottom)
                    {
                        p3.X = p3.X + x;
                    }
                    else
                    {
                        double m = (_definition.BeamDepth - _definition.FlangeThickness) / xDiff;

                        double xAdj = -_definition.ChamferDepth / m;

                        p3.X = p3.X + x + xAdj;
                    }
                }

                pointList.Add(p3);

                //5th point (if exists)
                if (_definition.ChamferBeamToSlab == true)
                {
                    if (_definition.BeamWidthTop == _definition.BeamWidthBottom)
                    {
                        PointD p4 = new PointD(_definition.FlangeWidth - _definition.FlangeCantilever, -_definition.FlangeThickness - _definition.ChamferDepth);

                        pointList.Add(p4);
                    }
                    else
                    {
                        double m = (_definition.BeamDepth - _definition.FlangeThickness) / xDiff;

                        double xAdj = -_definition.ChamferDepth / m;

                        PointD p4 = new PointD(_definition.FlangeWidth - _definition.FlangeCantilever + xAdj, -_definition.FlangeThickness - _definition.ChamferDepth);

                        pointList.Add(p4);
                    }
                }

                //6th point
                PointD p5 = new PointD(_definition.FlangeWidth - _definition.FlangeCantilever - xDiff, -_definition.BeamDepth);

                pointList.Add(p5);

                //7th point
                PointD p6 = new PointD(p5.X - _definition.BeamWidthBottom, -_definition.BeamDepth);

                //8th point (if exists)
                if (_definition.ChamferBeamToSlab == true)
                {
                    if (_definition.BeamWidthTop == _definition.BeamWidthBottom)
                    {
                        PointD p7 = new PointD(p6.X, -_definition.FlangeThickness - _definition.ChamferDepth);

                        pointList.Add(p7);
                    }
                    else
                    {
                        double m = (_definition.BeamDepth - _definition.FlangeThickness) / xDiff;

                        double xAdj = -_definition.ChamferDepth / m;

                        PointD p4 = new PointD(_definition.FlangeWidth - _definition.FlangeCantilever + xAdj, -_definition.FlangeThickness - _definition.ChamferDepth);

                        pointList.Add(p4);
                    }
                }

                return new PolygonFS(pointList);
            }

            return null;
        }
        #endregion
    }
}
