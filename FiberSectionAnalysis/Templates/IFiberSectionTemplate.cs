﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.Geometry;

namespace FiberSectionAnalysis.Templates
{
    public interface IFiberSectionTemplate
    {
        string TypeName { get; }
        string InstanceName { get; set; }
        List<PolygonFS> Boundaries { get; set; }
        List<RebarSectionFS> Reinforcement { get; set; }
        List<PostTensioningSectionFS> PostTensioning { get; set; }

    }
}
