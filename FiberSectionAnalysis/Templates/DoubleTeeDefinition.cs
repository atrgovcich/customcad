﻿using System;
using System.Collections.Generic;
using System.Text;
using Materials;

namespace FiberSectionAnalysis.Templates
{
    public class DoubleTeeDefinition
    {
        #region Public Properties
        /// <summary>
        /// Flange thickness, in inches
        /// </summary>
        public double FlangeThickness { get; set; }
        /// <summary>
        /// Flange width (total), in inches
        /// </summary>
        public double FlangeWidth { get; set; }
        /// <summary>
        /// Flange cantilever (on each side), in inches
        /// </summary>
        public double FlangeCantilever { get; set; }
        /// <summary>
        /// Beam depth, from top of slab, in inches
        /// </summary>
        public double BeamDepth { get; set; }
        /// <summary>
        /// Beam width at the underside of flange, in inches
        /// </summary>
        public double BeamWidthTop { get; set; }
        /// <summary>
        /// Beam width at the bottom of beam, in inches
        /// </summary>
        public double BeamWidthBottom { get; set; }
        /// <summary>
        /// Should the beam-to-slab joint be chamfered?
        /// </summary>
        public bool ChamferBeamToSlab { get; set; }
        /// <summary>
        /// Chamfer angle, in degrees
        /// </summary>
        public double ChamferAngle { get; set; }
        /// <summary>
        /// Depth of chamfer along beam height, in inches
        /// </summary>
        public double ChamferDepth { get; set; } = 3.0;
        
        public IGenericMaterial ConcreteMaterial { get; set; }
        public IGenericMaterial RebarMaterial { get; set; }
        public IGenericMaterial PostTensioningMaterial { get; set; }
        /// <summary>
        /// Initial prestress in PT tendons, in ksi
        /// </summary>
        public double InitialPrestress { get; set; } = 162;
        #endregion
    }
}
