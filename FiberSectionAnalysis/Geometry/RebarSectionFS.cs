﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;
using ReinforcedConcrete.Reinforcement;

namespace FiberSectionAnalysis.Geometry
{
    public class RebarSectionFS : CircleFS
    {
        #region Public Properties
        public RebarDefinition RebarSize { get; set; }
        #endregion

        #region Constructors
        public RebarSectionFS(PointD center, RebarDefinition barSize, IGenericMaterial material) : base(center, barSize.Diameter / 2.0, material)
        {
            RebarSize = barSize;
        }
        #endregion
    }
}
