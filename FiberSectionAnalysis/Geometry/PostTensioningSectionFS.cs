﻿using Utilities.Geometry;
using ReinforcedConcrete.PostTensioning;
using FiberSectionAnalysis.General;

namespace FiberSectionAnalysis.Geometry
{
    public class PostTensioningSectionFS : CircleFS
    {
        #region Private Backing Fields
        protected PostTensioningDefinition _typeOfPT;
        #endregion

        #region Public Properties
        /// <summary>
        /// Post tensioning type
        /// </summary>
        public virtual PostTensioningDefinition TypeOfPT
        {
            get
            {
                return _typeOfPT;
            }
            set
            {
                _typeOfPT = value;
            }
        }
        /// <summary>
        /// Initial prestress in the post tensioning
        /// </summary>
        public double InitialPrestress { get; set; }

        /// <summary>
        /// The jacking and grouting sequence
        /// </summary>
        public JackingAndGroutingType JackingAndGrouting { get; set; }
        #endregion

        #region Constructors
        public PostTensioningSectionFS(PointD center, PostTensioningDefinition ptDefinition, double initialPrestress, JackingAndGroutingType jackingAndGrouting) : base(center, (ptDefinition != null) ? ptDefinition.OuterDiameter / 2.0 : 0, (ptDefinition != null) ? ptDefinition.Material : null)
        {
            TypeOfPT = ptDefinition;
            InitialPrestress = initialPrestress;
            JackingAndGrouting = jackingAndGrouting;
        }
        #endregion
    }
}
