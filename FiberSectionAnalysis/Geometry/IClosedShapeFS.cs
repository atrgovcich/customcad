﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;
using Utilities.DataStructures.QuadTree;

namespace FiberSectionAnalysis.Geometry
{
    public interface IClosedShapeFS : IGenericClosedShape
    {
        #region Public Properties
        IGenericMaterial Material { get; set; }
        bool IsRebar { get; set; }
        #endregion
    }
}
