﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.Geometry
{
    public class CircleFS : GenericCircle, IClosedShapeFS
    {
        #region Public Properties
        /// <summary>
        /// Inelastic fiber material assigned to the element
        /// </summary>
        public IGenericMaterial Material { get; set; }
        /// <summary>
        /// Is the element rebar?
        /// </summary>
        public bool IsRebar { get; set; }
        #endregion

        #region Constructors
        [JsonConstructor]
        public CircleFS(PointD center, double radius) : base(center, radius)
        {

        }

        public CircleFS(PointD center, double radius, IGenericMaterial material) : base(center, radius)
        {
            Material = material;
        }
        #endregion
    }
}
