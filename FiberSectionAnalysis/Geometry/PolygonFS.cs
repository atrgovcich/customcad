﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.Geometry
{
    public class PolygonFS : GenericPolygon, IClosedShapeFS
    {
        #region Public Properties
        public IGenericMaterial Material { get; set; }

        public bool IsRebar { get; set; }
        #endregion

        #region Constructors
        [JsonConstructor]
        public PolygonFS(List<PointD> points) : base(points)
        {

        }

        public PolygonFS(List<PointD> points, IGenericMaterial material) : base(points)
        {
            Material = material;
        }
        #endregion
    }
}
