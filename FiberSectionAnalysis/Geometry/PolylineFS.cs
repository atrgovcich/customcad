﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;

namespace FiberSectionAnalysis.Geometry
{
    public class PolylineFS : GenericPolyline
    {
        #region Public Properties
        public bool IsRebar { get; set; }

        public IGenericMaterial Material { get; set; }
        #endregion

        #region Constructors
        public PolylineFS(List<PointD> points) : base(points)
        {

        }
        #endregion
    }
}
