﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace FiberSectionAnalysis.Enums
{
    public enum SectionAxis
    {
        [Description("X Axis")]
        X,
        [Description("Y Axis")]
        Y,
        [Description("Major Axis")]
        Major,
        [Description("Minor Axis")]
        Minor
    }
}
