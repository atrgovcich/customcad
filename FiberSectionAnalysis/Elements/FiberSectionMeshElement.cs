﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using Materials;
using FiberSectionAnalysis.Geometry;
using Newtonsoft.Json;
namespace FiberSectionAnalysis.Elements
{
    public class FiberSectionMeshElement : PolygonFS, IFiberSectionElement
    {
        #region Public Properties

        #endregion

        #region Constants
        public const string NAME = "Fiber Section Mesh Element";
        #endregion

        #region Constructors
        [JsonConstructor]
        public FiberSectionMeshElement(List<PointD> points) : base(points)
        {

        }

        public FiberSectionMeshElement(List<PointD> points, IGenericMaterial material) : base(points, material)
        {

        }
        #endregion
    }
}
