﻿using System;
using System.Collections.Generic;
using System.Text;
using Materials;
using Utilities.Geometry;

namespace FiberSectionAnalysis.Elements
{
    public interface IFiberSectionElement
    {
        IGenericMaterial Material { get; set; }

        PointD Centroid();
    }
}
