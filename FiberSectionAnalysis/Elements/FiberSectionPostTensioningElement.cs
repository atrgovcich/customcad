﻿using Utilities.Geometry;
using Materials;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Geometry;
using ReinforcedConcrete.PostTensioning;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.Elements
{
    public class FiberSectionPostTensioningElement : PostTensioningSectionFS, IFiberSectionElement
    {
        #region Public Properties
        /// <summary>
        /// Inelastic fiber material that is removed from the greater section at
        /// the location of the element
        /// </summary>
        public IGenericMaterial MaterialRemoved { get; set; }
        /// <summary>
        /// Initial prestrain in the post tensioning
        /// </summary>
        [JsonIgnore]
        public double InitialPrestrain
        {
            get
            {
                return Material.MonotonicStrainStress(-1.0 * InitialPrestress);
            }
        }

        public override PostTensioningDefinition TypeOfPT
        {
            get
            {
                return _typeOfPT;
            }
            set
            {
                _typeOfPT = value;

                if (_typeOfPT != null)
                {
                    TendonArea = _typeOfPT.SteelArea;
                    Material = _typeOfPT.Material;
                    Radius = _typeOfPT.OuterDiameter / 2.0;
                }
            }
        }
        /// <summary>
        /// The actual area of tendon steel, which is smaller than the area enclosed by the sheathing
        /// </summary>
        public double TendonArea { get; set; }
        #endregion

        #region Constants
        public const string NAME = "Fiber Section Post Tensioning Element";
        #endregion

        #region Constructors
        /// <summary>
        /// Returns a new instance of the fiber section post tensioning class
        /// </summary>
        /// <param name="centroid">Centroid of element</param>
        /// <param name="radius">Outer radius of sheathing</param>
        /// <param name="area">Tendon area</param>
        /// <param name="type">Type of post tensioning</param>
        /// <param name="material">Inelastic fiber material assigned to the element</param>
        public FiberSectionPostTensioningElement(PointD center, PostTensioningDefinition ptDefinition, double initialPrestress, JackingAndGroutingType jackingAndGrouting) : base(center, ptDefinition, initialPrestress, jackingAndGrouting)
        {
            if (ptDefinition != null)
            {
                TendonArea = ptDefinition.SteelArea;
                Material = ptDefinition.Material;
            }
        }
        #endregion
    }
}
