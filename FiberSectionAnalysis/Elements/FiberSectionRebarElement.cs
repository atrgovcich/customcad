﻿using Utilities.Geometry;
using Materials;
using FiberSectionAnalysis.Geometry;
using ReinforcedConcrete.Reinforcement;

namespace FiberSectionAnalysis.Elements
{
    public class FiberSectionRebarElement : RebarSectionFS, IFiberSectionElement
    {
        #region Public Properties
        /// <summary>
        /// Inelastic fiber material that is removed from the greater section at
        /// the location of the element
        /// </summary>
        public IGenericMaterial MaterialRemoved { get; set; }
        #endregion

        #region Constants
        public const string NAME = "Fiber Section Rebar Element";
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the fiber section rebar class
        /// </summary>
        /// <param name="centroid">Centroid of element</param>
        /// <param name="radius">Radius of bar</param>
        public FiberSectionRebarElement(PointD centroid, RebarDefinition rebarSize, IGenericMaterial material) : base(centroid, rebarSize, material)
        {

        }

        #endregion
    }
}
