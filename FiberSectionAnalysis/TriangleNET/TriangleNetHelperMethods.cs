﻿using System;
using System.Collections.Generic;
using System.Text;
using TriangleNet.Geometry;
using TriangleNetHelper.Geometry;
using Utilities.Geometry;
using System.Linq;

namespace FiberSectionAnalysis.TriangleNET
{
    public static class TriangleNetHelperMethods
    {
        public static PrePolygon ConvertGenericPolygontoPrePolygon(GenericPolygon boundary)
        {
            //Converting into a PrePolygon, which is NOT closed (i.e. the first point does NOT equal the last point)

            List<PointD> polygonPointList = boundary.Points;

            List<PreVertex> vertices = new List<PreVertex>(polygonPointList.Count - 1);

            for (int i = 0; i < polygonPointList.Count - 1; i++)
            {
                vertices.Add(ConvertPointDtoPreVertex(polygonPointList[i]));
            }

            return new PrePolygon(vertices) { Hole = boundary.Hole };
        }

        public static List<GenericPolygon> ConvertFromPrePolgyonsToGenericPolygons(IEnumerable<PrePolygon> meshBoundaries)
        {
            var convertedMeshes = new List<GenericPolygon>();

            var boundaries = meshBoundaries.ToList();

            for (int i = 0; i < boundaries.Count; i++)
            {
                PrePolygon boundary = boundaries[i];

                convertedMeshes.Add(ConvertFromPrePolygonToGenericPolygon(boundary));
            }

            return convertedMeshes;
        }

        public static GenericPolygon ConvertFromPrePolygonToGenericPolygon(PrePolygon meshBoundary)
        {
            List<PreVertex> vertices = meshBoundary.Vertices;

            List<PointD> nodeList = new List<PointD>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++)
            {
                PreVertex pV = vertices[i];

                PointD pD = new PointD(pV.X, pV.Y);

                nodeList.Add(pD);
            }

            if (nodeList[0] == nodeList[nodeList.Count - 1])
            {
                nodeList[nodeList.Count - 1] = nodeList[0];
            }

            return new GenericPolygon(nodeList);
        }

        public static PrePolyline ConvertGenericPolylineGtoPrePolyline(GenericPolyline segment)
        {
            List<PointD> vertices = segment.Points;

            List<PreVertex> preVertices = new List<PreVertex>(vertices.Count);

            for (int i = 0; i < vertices.Count; i++)
            {
                PreVertex v = new PreVertex(vertices[i].X, vertices[i].Y);

                preVertices.Add(v);
            }

            return new PrePolyline(preVertices);
        }

        public static PreVertex ConvertPointDtoPreVertex(PointD point)
        {
            return new PreVertex(point.X, point.Y);
        }

        public static PreVertex ConvertGenericPointToPreVertex(GenericPoint point)
        {
            return new PreVertex(point.Point.X, point.Point.Y);
        }
    }
}
