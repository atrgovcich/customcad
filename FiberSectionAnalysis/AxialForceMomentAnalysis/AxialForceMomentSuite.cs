﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.LimitStates;
using Materials;
using Utilities.Geometry;
using FiberSectionAnalysis.Results;
using FiberSectionAnalysis.Enums;
using Newtonsoft.Json;
using System.Linq;

namespace FiberSectionAnalysis.AxialForceMomentAnalysis
{
    public class AxialForceMomentSuite
    {
        #region Private Backing Fields
        private List<AxialForceMoment> _analyses;

        //private List<KeyValuePair<IGenericMaterial, List<LimitState>>> _materialLimitStatesList;

        private Dictionary<IGenericMaterial, List<LimitState>> _materialLimitStates;
        #endregion

        #region Public Properties
        /// <summary>
        /// Returns the list of axial force moment analyses
        /// </summary>
        [JsonIgnore]
        public List<AxialForceMoment> Analyses
        {
            get
            {
                return _analyses;
            }
        }
        /// <summary>
        /// The list of all angles, in raidans, that will be analyzed
        /// </summary>
        public List<double> AnglesToAnalyze { get; set; }
        /// <summary>
        /// The fiber section model containing the mesh, rebar, and PT elements
        /// </summary>
        public FiberSection SectionModel { get; set; }
        /// <summary>
        /// The approximate number of points each axial force moment analysis should contain
        /// </summary>
        public int NumberOfPoints { get; set; }
        /// <summary>
        /// Dictionary of material limit states
        /// </summary>
        [JsonIgnore]
        public Dictionary<IGenericMaterial, List<LimitState>> MaterialLimitStates
        {
            get
            {
                return _materialLimitStates;
            }
            set
            {
                _materialLimitStates = value;

                //_materialLimitStatesList = _materialLimitStates.ToList();
            }
        }

        public List<KeyValuePair<IGenericMaterial, List<LimitState>>> MaterialLimitStatesList
        {
            get
            {
                if (_materialLimitStates != null)
                {
                    return _materialLimitStates.ToList();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                //_materialLimitStatesList = value;
                if (value != null)
                {
                    _materialLimitStates = value.ToDictionary(kv => kv.Key, kv => kv.Value);
                }
            }
        }
        /// <summary>
        /// Whether the initial post tensioning force (if any) is included in the axial force-moment diagram.
        /// If set to false, the initial post tensioning force will be subtraced from the axial load diagram,
        /// producing a smaller interaction diagram.
        /// </summary>
        public bool IncludeInitialPostTensioningForceInAxialLoad { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the axial force moment suite
        /// </summary>
        /// <param name="anglesToAnalyze">List of angles, in radians, at which an axial force moment analysis should be run</param>
        public AxialForceMomentSuite(List<double> anglesToAnalyze)
        {
            AnglesToAnalyze = new List<double>(anglesToAnalyze);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Performs an axial force-moment analysis on all of the angles specified
        /// </summary>
        public void AnalyzeSuite()
        {
            if (Validate(out string validationMessage) == false)
            {
                throw new Exception(validationMessage);
            }

            SectionModel.CalculateSectionProperties();

            _analyses = new List<AxialForceMoment>();

            for (int i = 0; i < AnglesToAnalyze.Count; i++)
            {
                _analyses.Add(new AxialForceMoment(SectionModel, AnglesToAnalyze[i], NumberOfPoints, MaterialLimitStates, IncludeInitialPostTensioningForceInAxialLoad));
            }

            Task[] taskArray = new Task[_analyses.Count];

            int count = 0;

            for (int i = 0; i < _analyses.Count; i++)
            {
                int ind = count;

                taskArray[ind] = Task.Run(() => _analyses[ind].RunAnalysis());

                count++;
            }

            Task.WaitAll(taskArray);
        }

        /// <summary>
        /// Gets the moment-moment diagram at a sepcified axial force
        /// </summary>
        /// <param name="axialForce">Axial force (compression positive)</param>
        /// <param name="reduced">Should the diagram be reduced (phi factor)?</param>
        /// <param name="coordSystem">Global or local coordinate system</param>
        /// <returns></returns>
        public List<PointD> GetMomentMomentDiagram(double axialForce, bool reduced, CoordinateSystem coordSystem)
        {
            try
            {
                if (Analyses != null && Analyses.Count >= 3)
                {
                    List<PointD> mmDiagram = new List<PointD>();

                    for (int i = 0; i < Analyses.Count; i++)
                    {
                        mmDiagram.Add(Analyses[i].InterpolatePM(axialForce, reduced, coordSystem));
                    }

                    mmDiagram.Add(mmDiagram[0]);

                    return mmDiagram;
                }

                return null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region ProtectedMethods
        protected bool Validate(out string message)
        {
            message = string.Empty;

            if (AnglesToAnalyze == null)
            {
                message = "The list of angles to analyze was null";

                return false;
            }

            if (AnglesToAnalyze.Count <= 0)
            {
                message = "The list of angles to analyze contains no items";

                return false;
            }

            if (SectionModel == null)
            {
                message = "The fiber section model was null";

                return false;
            }

            return true;
        }

        protected PointD InterpolatePM(AxialForceMoment analysis, double axialForce, bool reduced, CoordinateSystem coordSystem)
        {
            double pMax = (reduced == false) ? analysis.AnalysisResults[0].AxialForce : analysis.AnalysisResults[0].ReducedAxialForce;

            double pMin = (reduced == false) ? analysis.AnalysisResults[analysis.AnalysisResults.Count - 1].AxialForce : analysis.AnalysisResults[analysis.AnalysisResults.Count - 1].ReducedAxialForce;

            if (axialForce > pMax)
            {
                throw new Exception("The axial force entered for interpolation is greater than the maximum compression capacity of the section.");
            }
            else if (axialForce < pMin)
            {
                throw new Exception("The axial force entered for interpolation is greater than the maximum tensile capacity of the section.");
            }

            int indexBelow = -1; //index of the analysis where the axial force is less than the axial force entered

            for (int i = 0; i < analysis.AnalysisResults.Count; i++)
            {
                FiberSectionAnalysisResult res = analysis.AnalysisResults[i];

                double p = (reduced == false) ? res.AxialForce : res.ReducedAxialForce;

                if (p <= axialForce)
                {
                    indexBelow = i;

                    break;
                }
            }

            if (indexBelow <= 0)
            {
                throw new Exception("Could not find an axial force that is below the target axial force.  Interpolation failed.");
            }

            double pLow = (reduced == false) ? analysis.AnalysisResults[indexBelow].AxialForce : analysis.AnalysisResults[indexBelow].ReducedAxialForce;

            double pHigh = (reduced == false) ? analysis.AnalysisResults[indexBelow - 1].AxialForce : analysis.AnalysisResults[indexBelow - 1].ReducedAxialForce;

            double mxLow = 0;

            double myLow = 0;

            double mxHigh = 0;

            double myHigh = 0;

            if (coordSystem.Equals(CoordinateSystem.Global) == true)
            {
                mxLow = (reduced == false) ? analysis.AnalysisResults[indexBelow].MomentX : analysis.AnalysisResults[indexBelow].ReducedMomentX;

                myLow = (reduced == false) ? analysis.AnalysisResults[indexBelow].MomentY : analysis.AnalysisResults[indexBelow].ReducedMomentY;

                mxHigh = (reduced == false) ? analysis.AnalysisResults[indexBelow - 1].MomentX : analysis.AnalysisResults[indexBelow - 1].ReducedMomentX;

                myHigh = (reduced == false) ? analysis.AnalysisResults[indexBelow - 1].MomentY : analysis.AnalysisResults[indexBelow - 1].ReducedMomentY;
            }
            else
            {
                mxLow = (reduced == false) ? analysis.AnalysisResults[indexBelow].MomentMajor : analysis.AnalysisResults[indexBelow].MomentMajor;

                myLow = (reduced == false) ? analysis.AnalysisResults[indexBelow].MomentMinor : analysis.AnalysisResults[indexBelow].MomentMinor;

                mxHigh = (reduced == false) ? analysis.AnalysisResults[indexBelow - 1].MomentMajor : analysis.AnalysisResults[indexBelow - 1].MomentMajor;

                myHigh = (reduced == false) ? analysis.AnalysisResults[indexBelow - 1].MomentMinor : analysis.AnalysisResults[indexBelow - 1].MomentMinor;
            }

            double mx = (mxLow - mxHigh) / (pLow - pHigh) * (axialForce - pHigh) + mxHigh;

            double my = (myLow - myHigh) / (pLow - pHigh) * (axialForce - pHigh) + myHigh;

            return new PointD(mx, my);
        }
        #endregion
    }
}
