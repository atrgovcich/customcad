﻿using System;
using System.Collections.Generic;
using System.Text;
using Materials.Inelastic.Concrete;
using FiberSectionAnalysis.Loads;
using FiberSectionAnalysis.LimitStates;
using FiberSectionAnalysis.Enums;
using Utilities.Geometry;
using FiberSectionAnalysis.General;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.AxialForceMomentAnalysis
{
    public class AxialForceMomentAnalysisCase : GeneralAnalysisCase
    {
        #region Private Backing Fields
        private Dictionary<CrossSectionLoad, List<PointD>> _momentMomentDiagramsGlobal;
        private Dictionary<CrossSectionLoad, List<PointD>> _reducedMomentMomentDiagramsGlobal;
        private Dictionary<CrossSectionLoad, List<PointD>> _momentMomentDiagramsLocal;
        private Dictionary<CrossSectionLoad, List<PointD>> _reducedMomentMomentDiagramsLocal;
        #endregion

        #region Public Properties

        public AxialForceMomentAnalysisType AnalysisType { get; set; } = AxialForceMomentAnalysisType.SingleAngle;
        /// <summary>
        /// The single angle, in degrees, to analyze the section at
        /// </summary>
        public double SingleAngle { get; set; }
        /// <summary>
        /// The angle interval, in degrees, to perform analyses at
        /// </summary>
        public double AngleInterval { get; set; }
        public List<CrossSectionLoad> Loads { get; set; }
        public bool IncludeInitialPostTensioningForceInAxialLoad { get; set; } = false;

        [JsonIgnore]
        public AxialForceMomentSuite AnalysisSuite { get; set; }

        [JsonIgnore]
        public Dictionary<CrossSectionLoad, List<PointD>> MomentMomentDiagramsGlobal
        {
            get
            {
                return _momentMomentDiagramsGlobal;
            }
        }

        [JsonIgnore]
        public Dictionary<CrossSectionLoad, List<PointD>> ReducedMomentMomentDiagramsGlobal
        {
            get
            {
                return _reducedMomentMomentDiagramsGlobal;
            }
        }

        [JsonIgnore]
        public Dictionary<CrossSectionLoad, List<PointD>> MomentMomentDiagramsLocal
        {
            get
            {
                return _momentMomentDiagramsLocal;
            }
        }

        [JsonIgnore]
        public Dictionary<CrossSectionLoad, List<PointD>> ReducedMomentMomentDiagramsLocal
        {
            get
            {
                return _reducedMomentMomentDiagramsLocal;
            }
        }
        #endregion

        #region Public Methods
        public void PopulateMomentMomentDiagrams()
        {
            _momentMomentDiagramsGlobal = GetMomemntMomentDiagramsForLoadCases(false, CoordinateSystem.Global);

            _reducedMomentMomentDiagramsGlobal = GetMomemntMomentDiagramsForLoadCases(true, CoordinateSystem.Global);

            _momentMomentDiagramsLocal = GetMomemntMomentDiagramsForLoadCases(false, CoordinateSystem.Local);

            _reducedMomentMomentDiagramsLocal = GetMomemntMomentDiagramsForLoadCases(true, CoordinateSystem.Local);
        }
        #endregion

        #region Private Methods
        private Dictionary<CrossSectionLoad, List<PointD>> GetMomemntMomentDiagramsForLoadCases(bool reduced, CoordinateSystem coordSystem)
        {
            try
            {
                if (AnalysisSuite.Analyses != null && AnalysisSuite.Analyses.Count >= 3)
                {
                    if (Loads != null && Loads.Count > 0)
                    {
                        Dictionary<CrossSectionLoad, List<PointD>> allDiagrams = new Dictionary<CrossSectionLoad, List<PointD>>();

                        for (int i = 0; i < Loads.Count; i++)
                        {
                            allDiagrams.Add(Loads[i], AnalysisSuite.GetMomentMomentDiagram(Loads[i].P, reduced, coordSystem));
                        }

                        return allDiagrams;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
