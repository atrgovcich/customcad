﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Results;
using Utilities.Geometry;
using static System.Math;
using FiberSectionAnalysis.LimitStates;
using Materials;
using FiberSectionAnalysis.Elements;
using Materials.Inelastic.Concrete;
using FiberSectionAnalysis.Enums;
using Newtonsoft.Json;

namespace FiberSectionAnalysis.AxialForceMomentAnalysis
{
    public class AxialForceMoment : GeneralFiberSectionAnalysis
    {

        #region Constructors
        /// <summary>
        /// Creates a blank instance of the axial force-moment class
        /// </summary>
        [JsonConstructor]
        public AxialForceMoment()
        {

        }

        public AxialForceMoment(FiberSection sectionModel, double analysisAngle, int numberOfPoints, Dictionary<IGenericMaterial, List<LimitState>> materialLimitStates, bool includeInitialPostTensioningForceInAxialLoad)
        {
            SectionModel = sectionModel;
            AnalysisAngle = analysisAngle;
            NumberOfPoints = numberOfPoints;
            MaterialLimitStates = materialLimitStates;
            IncludeInitialPostTensioningForceInAxialLoad = includeInitialPostTensioningForceInAxialLoad;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Runs the axial force-moment analysis
        /// </summary>
        public void RunAnalysis()
        {
            if (ValidateInputs(out string validationMessage) == false)
            {
                throw new Exception(validationMessage);
            }
            else
            {
                InstantiateVariables();

                ComputeUnitVectors();

                ComputeNeutralAxisProperties();

                FurthestTensionCompressionPoints furthestPoints = SectionModel.GetFurthestTensionCompressionPoints(_neutralAxis);

                double tcDist = GetTensionCompressionDistance(_neutralAxis, furthestPoints);

                Edge compressionPointAxis = _neutralAxis.Line.CreateParallelEdgeAtPoint(furthestPoints.CompressionPoint);

                Edge tensionPointAxis = _neutralAxis.Line.CreateParallelEdgeAtPoint(furthestPoints.TensionPoint);

                double phi = 0; //Curvature, initially starts at 0

                LimitStateElement compressionLimitStateZeroCurvature = GetCompressionLimitState(_neutralAxis, furthestPoints, 0);

                LimitStateElement tensionLimitStateZeroCurvature = GetTensionLimitState(_neutralAxis, furthestPoints, 0);

                double maxStrainDifference = compressionLimitStateZeroCurvature.FurthestFiberStrain - tensionLimitStateZeroCurvature.FurthestFiberStrain;

                double phiIncrement = maxStrainDifference / tcDist / Convert.ToDouble(NumberOfPoints);

                int pointCounter = 0;

                double maxCompressionCurvature = 0;

                bool terminationLimitStateReached = false;

                #region Peg at Compression Limit State
                //Analysis pegged at compression limit state
                while(terminationLimitStateReached == false)
                {
                    FiberSectionAnalysisResult sectionResults = new FiberSectionAnalysisResult();

                    LimitStateElement compressionLimitState = GetCompressionLimitState(_neutralAxis, furthestPoints, phi);

                    LimitStateElement tensionLimitState = GetTensionLimitState(_neutralAxis, furthestPoints, phi);

                    double compressionFiberStrain = compressionLimitState.FurthestFiberStrain; //strain in the outermost compression fiber

                    double tensionFiberStrain = compressionFiberStrain - phi * tcDist; //strain in the outermost tension fiber

                    double initialSectionPrestressingForce = 0;

                    if (tensionFiberStrain < tensionLimitState.FurthestFiberStrain)
                    {
                        //We have exceeded the tension termination limit state
                        maxCompressionCurvature = phi - phiIncrement; //Max curvature before exceeding limit state is the previous step

                        break;
                    }

                    FiberSectionElementResults elementResults = new FiberSectionElementResults(phi);

                    #region Mesh Element Calculations
                    //Mesh element calculations
                    if (SectionModel.FiberMeshElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberMeshElements.Count; i++)
                        {
                            FiberSectionMeshElement elem = SectionModel.FiberMeshElements[i];

                            PointD c = elem.Centroid();

                            FiberElementResult res = new FiberElementResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            res.Strain = compressionFiberStrain - phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Concrete += res.Force;

                            sectionResults.MomentMajor_Concrete += res.Mmaj;

                            sectionResults.MomentMinor_Concrete += res.Mmin;

                            sectionResults.MomentX_Concrete += res.Mx;

                            sectionResults.MomentY_Concrete += res.My;

                            elementResults.MeshElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region Rebar Element Calculations
                    //Rebar element calculations
                    if (SectionModel.FiberRebarElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberRebarElements.Count; i++)
                        {
                            FiberSectionRebarElement elem = SectionModel.FiberRebarElements[i];

                            PointD c = elem.Centroid();

                            RebarSectionResult res = new RebarSectionResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            res.Strain = compressionFiberStrain - phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(res.Strain);

                            res.ForceRemoved = stressRemoved * elem.Area();

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Rebar += res.Force;

                            sectionResults.MomentMajor_Rebar += res.Mmaj;

                            sectionResults.MomentMinor_Rebar += res.Mmin;

                            sectionResults.MomentX_Rebar += res.Mx;

                            sectionResults.MomentY_Rebar += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.RebarElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region PT Element Calculations
                    //PT element calculations
                    if (SectionModel.FiberPostTensioningElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberPostTensioningElements.Count; i++)
                        {
                            FiberSectionPostTensioningElement elem = SectionModel.FiberPostTensioningElements[i];

                            PointD c = elem.Centroid();

                            PostTensioningResult res = new PostTensioningResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            double strain = compressionFiberStrain - phi * dist;

                            double ptStrain = elem.InitialPrestrain;

                            if (elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedGroutedReleased) == true || elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedReleasedGrouted) == true)
                            {
                                ptStrain += strain;
                            }

                            initialSectionPrestressingForce += elem.InitialPrestress * elem.TendonArea;

                            res.Strain = ptStrain;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.TendonArea;

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double areaRemoved = (elem.JackingAndGrouting.Equals(JackingAndGroutingType.Unbonded) == true) ? elem.Area() : elem.TendonArea;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(strain);

                            res.ForceRemoved = stressRemoved * areaRemoved;

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_PT += res.Force;

                            sectionResults.MomentMajor_PT += res.Mmaj;

                            sectionResults.MomentMinor_PT += res.Mmin;

                            sectionResults.MomentX_PT += res.Mx;

                            sectionResults.MomentY_PT += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.PostTensioningElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region Sum Section Forces
                    sectionResults.AxialForce = sectionResults.AxialForce_Concrete + sectionResults.AxialForce_Rebar + sectionResults.AxialForce_PT - sectionResults.AxialForce_Removed;

                    if (IncludeInitialPostTensioningForceInAxialLoad == false)
                    {
                        sectionResults.AxialForce -= initialSectionPrestressingForce;
                    }

                    sectionResults.MomentX = sectionResults.MomentX_Concrete + sectionResults.MomentX_Rebar + sectionResults.MomentX_PT - sectionResults.MomentX_Removed;

                    sectionResults.MomentY = sectionResults.MomentY_Concrete + sectionResults.MomentY_Rebar + sectionResults.MomentY_PT - sectionResults.MomentY_Removed;

                    sectionResults.MomentMajor = sectionResults.MomentMajor_Concrete + sectionResults.MomentMajor_Rebar + sectionResults.MomentMajor_PT - sectionResults.MomentMajor_Removed;

                    sectionResults.MomentMinor = sectionResults.MomentMinor_Concrete + sectionResults.MomentMinor_Rebar + sectionResults.MomentMinor_PT - sectionResults.MomentMinor_Removed;
                    #endregion

                    #region Calculate Reduced Values
                    double maxRebarTensionStrain = 0;

                    foreach(KeyValuePair<FiberSectionRebarElement, RebarSectionResult> kvp in elementResults.RebarElementResults)
                    {
                        maxRebarTensionStrain = Min(maxRebarTensionStrain, kvp.Value.Strain);
                    }

                    double phiFactor = GetPhiFactor(maxRebarTensionStrain);

                    double poFactor = GetPoFactor();

                    if (pointCounter == 0)
                    {
                        //pure compression
                        sectionResults.ReducedAxialForce = poFactor * phiFactor * sectionResults.AxialForce;
                    }
                    else
                    {
                        double fP0 = _analysisResults[0].ReducedAxialForce; //first point axial force

                        sectionResults.ReducedAxialForce = Min(fP0, phiFactor * sectionResults.AxialForce);
                    }

                    sectionResults.ReducedMomentX = sectionResults.MomentX * phiFactor;

                    sectionResults.ReducedMomentY = sectionResults.MomentY * phiFactor;

                    sectionResults.ReducedMomentMajor = sectionResults.MomentMajor * phiFactor;

                    sectionResults.ReducedMomentMinor = sectionResults.MomentMinor * phiFactor;

                    sectionResults.PhiFactor = phiFactor;

                    sectionResults.Curvature = phi;

                    sectionResults.LimitState = "C";

                    #endregion

                    #region Calculate Neutral Axis
                    if (phi == 0)
                    {
                        sectionResults.NeutralAxisDepth = double.PositiveInfinity;
                    }
                    else
                    {
                        sectionResults.NeutralAxisDepth = compressionFiberStrain / phi;
                    }
                    #endregion

                    _elementResults.Add(elementResults);
                    _analysisResults.Add(sectionResults);

                    pointCounter++;

                    phi += phiIncrement;
                }
                #endregion

                phi = maxCompressionCurvature;

                #region Peg at Tension Limit State
                while (phi > 0)
                {
                    phi = Max(phi - phiIncrement, 0);

                    FiberSectionAnalysisResult sectionResults = new FiberSectionAnalysisResult();

                    LimitStateElement compressionLimitState = GetCompressionLimitState(_neutralAxis, furthestPoints, phi);

                    LimitStateElement tensionLimitState = GetTensionLimitState(_neutralAxis, furthestPoints, phi);

                    double tensionFiberStrain = tensionLimitState.FurthestFiberStrain; //strain in the outermost tension fiber

                    double compressionFiberStrain = tensionFiberStrain + phi * tcDist; //strain in the outermost compression fiber

                    double initialSectionPrestressingForce = 0;

                    FiberSectionElementResults elementResults = new FiberSectionElementResults(phi);

                    #region Mesh Element Calculations
                    //Mesh element calculations
                    if (SectionModel.FiberMeshElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberMeshElements.Count; i++)
                        {
                            FiberSectionMeshElement elem = SectionModel.FiberMeshElements[i];

                            PointD c = elem.Centroid();

                            FiberElementResult res = new FiberElementResult();

                            double dist = tensionPointAxis.PerpendicularDistance(c);

                            res.Strain = tensionFiberStrain + phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Concrete += res.Force;

                            sectionResults.MomentMajor_Concrete += res.Mmaj;

                            sectionResults.MomentMinor_Concrete += res.Mmin;

                            sectionResults.MomentX_Concrete += res.Mx;

                            sectionResults.MomentY_Concrete += res.My;

                            elementResults.MeshElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region Rebar Element Calculations
                    //Rebar element calculations
                    if (SectionModel.FiberRebarElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberRebarElements.Count; i++)
                        {
                            FiberSectionRebarElement elem = SectionModel.FiberRebarElements[i];

                            PointD c = elem.Centroid();

                            RebarSectionResult res = new RebarSectionResult();

                            double dist = tensionPointAxis.PerpendicularDistance(c);

                            res.Strain = tensionFiberStrain + phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(res.Strain);

                            res.ForceRemoved = stressRemoved * elem.Area();

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Rebar += res.Force;

                            sectionResults.MomentMajor_Rebar += res.Mmaj;

                            sectionResults.MomentMinor_Rebar += res.Mmin;

                            sectionResults.MomentX_Rebar += res.Mx;

                            sectionResults.MomentY_Rebar += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.RebarElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region PT Element Calculations
                    //PT element calculations
                    if (SectionModel.FiberPostTensioningElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberPostTensioningElements.Count; i++)
                        {
                            FiberSectionPostTensioningElement elem = SectionModel.FiberPostTensioningElements[i];

                            PointD c = elem.Centroid();

                            PostTensioningResult res = new PostTensioningResult();

                            double dist = tensionPointAxis.PerpendicularDistance(c);

                            double strain = tensionFiberStrain + phi * dist;

                            double ptStrain = elem.InitialPrestrain;

                            if (elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedGroutedReleased) == true || elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedReleasedGrouted) == true)
                            {
                                ptStrain += strain;
                            }

                            initialSectionPrestressingForce += elem.InitialPrestress * elem.TendonArea;

                            res.Strain = ptStrain;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.TendonArea;

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double areaRemoved = (elem.JackingAndGrouting.Equals(JackingAndGroutingType.Unbonded) == true) ? elem.Area() : elem.TendonArea;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(strain);

                            res.ForceRemoved = stressRemoved * areaRemoved;

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_PT += res.Force;

                            sectionResults.MomentMajor_PT += res.Mmaj;

                            sectionResults.MomentMinor_PT += res.Mmin;

                            sectionResults.MomentX_PT += res.Mx;

                            sectionResults.MomentY_PT += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.PostTensioningElementResults.Add(elem, res);
                        }
                    }
                    #endregion

                    #region Sum Section Forces
                    sectionResults.AxialForce = sectionResults.AxialForce_Concrete + sectionResults.AxialForce_Rebar + sectionResults.AxialForce_PT - sectionResults.AxialForce_Removed;

                    if (IncludeInitialPostTensioningForceInAxialLoad == false)
                    {
                        sectionResults.AxialForce -= initialSectionPrestressingForce;
                    }

                    sectionResults.MomentX = sectionResults.MomentX_Concrete + sectionResults.MomentX_Rebar + sectionResults.MomentX_PT - sectionResults.MomentX_Removed;

                    sectionResults.MomentY = sectionResults.MomentY_Concrete + sectionResults.MomentY_Rebar + sectionResults.MomentY_PT - sectionResults.MomentY_Removed;

                    sectionResults.MomentMajor = sectionResults.MomentMajor_Concrete + sectionResults.MomentMajor_Rebar + sectionResults.MomentMajor_PT - sectionResults.MomentMajor_Removed;

                    sectionResults.MomentMinor = sectionResults.MomentMinor_Concrete + sectionResults.MomentMinor_Rebar + sectionResults.MomentMinor_PT - sectionResults.MomentMinor_Removed;
                    #endregion

                    #region Calculate Reduced Values
                    double maxRebarTensionStrain = 0;

                    foreach (KeyValuePair<FiberSectionRebarElement, RebarSectionResult> kvp in elementResults.RebarElementResults)
                    {
                        maxRebarTensionStrain = Min(maxRebarTensionStrain, kvp.Value.Strain);
                    }

                    double phiFactor = GetPhiFactor(maxRebarTensionStrain);

                    double poFactor = GetPoFactor();

                    if (pointCounter == 0)
                    {
                        //pure compression
                        sectionResults.ReducedAxialForce = poFactor * phiFactor * sectionResults.AxialForce;
                    }
                    else
                    {
                        double fP0 = _analysisResults[0].ReducedAxialForce; //first point axial force

                        sectionResults.ReducedAxialForce = Min(fP0, phiFactor * sectionResults.AxialForce);
                    }

                    sectionResults.ReducedMomentX = sectionResults.MomentX * phiFactor;

                    sectionResults.ReducedMomentY = sectionResults.MomentY * phiFactor;

                    sectionResults.ReducedMomentMajor = sectionResults.MomentMajor * phiFactor;

                    sectionResults.ReducedMomentMinor = sectionResults.MomentMinor * phiFactor;

                    sectionResults.PhiFactor = phiFactor;

                    sectionResults.Curvature = phi;

                    sectionResults.LimitState = "T";
                    #endregion

                    if (phi == 0)
                    {
                        sectionResults.NeutralAxisDepth = double.PositiveInfinity;
                    }
                    else
                    {
                        double cfs = tensionFiberStrain + phi * tcDist; //compression fiber strain

                        sectionResults.NeutralAxisDepth = cfs / phi;
                    }

                    _analysisResults.Add(sectionResults);

                    pointCounter++;
                }
                #endregion
            }
        }

        public PointD InterpolatePM(double axialForce, bool reduced, CoordinateSystem coordSystem)
        {
            double pMax = (reduced == false) ? AnalysisResults[0].AxialForce : AnalysisResults[0].ReducedAxialForce;

            double pMin = (reduced == false) ? AnalysisResults[AnalysisResults.Count - 1].AxialForce : AnalysisResults[AnalysisResults.Count - 1].ReducedAxialForce;

            if (axialForce > pMax)
            {
                throw new Exception("The axial force entered for interpolation is greater than the maximum compression capacity of the section.");
            }
            else if (axialForce < pMin)
            {
                throw new Exception("The axial force entered for interpolation is greater than the maximum tensile capacity of the section.");
            }

            int indexBelow = -1; //index of the analysis where the axial force is less than the axial force entered

            for (int i = 0; i < AnalysisResults.Count; i++)
            {
                FiberSectionAnalysisResult res = AnalysisResults[i];

                double p = (reduced == false) ? res.AxialForce : res.ReducedAxialForce;

                if (p <= axialForce)
                {
                    indexBelow = i;

                    break;
                }
            }

            if (indexBelow <= 0)
            {
                throw new Exception("Could not find an axial force that is below the target axial force.  Interpolation failed.");
            }

            double pLow = (reduced == false) ? AnalysisResults[indexBelow].AxialForce : AnalysisResults[indexBelow].ReducedAxialForce;

            double pHigh = (reduced == false) ? AnalysisResults[indexBelow - 1].AxialForce : AnalysisResults[indexBelow - 1].ReducedAxialForce;

            double mxLow = 0;

            double myLow = 0;

            double mxHigh = 0;

            double myHigh = 0;

            if (coordSystem.Equals(CoordinateSystem.Global) == true)
            {
                mxLow = (reduced == false) ? AnalysisResults[indexBelow].MomentX : AnalysisResults[indexBelow].ReducedMomentX;

                myLow = (reduced == false) ? AnalysisResults[indexBelow].MomentY : AnalysisResults[indexBelow].ReducedMomentY;

                mxHigh = (reduced == false) ? AnalysisResults[indexBelow - 1].MomentX : AnalysisResults[indexBelow - 1].ReducedMomentX;

                myHigh = (reduced == false) ? AnalysisResults[indexBelow - 1].MomentY : AnalysisResults[indexBelow - 1].ReducedMomentY;
            }
            else
            {
                mxLow = (reduced == false) ? AnalysisResults[indexBelow].MomentMajor : AnalysisResults[indexBelow].MomentMajor;

                myLow = (reduced == false) ? AnalysisResults[indexBelow].MomentMinor : AnalysisResults[indexBelow].MomentMinor;

                mxHigh = (reduced == false) ? AnalysisResults[indexBelow - 1].MomentMajor : AnalysisResults[indexBelow - 1].MomentMajor;

                myHigh = (reduced == false) ? AnalysisResults[indexBelow - 1].MomentMinor : AnalysisResults[indexBelow - 1].MomentMinor;
            }

            double mx = (mxLow - mxHigh) / (pLow - pHigh) * (axialForce - pHigh) + mxHigh;

            double my = (myLow - myHigh) / (pLow - pHigh) * (axialForce - pHigh) + myHigh;

            return new PointD(mx, my);
        }
        #endregion

        #region Protected Methods     
        /// <summary>
        /// Returns the Po factor (used to reduce axial load carrying capacity)
        /// </summary>
        /// <returns></returns>
        protected double GetPoFactor()
        {
            if (SectionModel.Confinement.Equals(ConfinementType.ClosedTies) == true)
            {
                return 0.8;
            }
            else
            {
                return 0.85;
            }
        }
        #endregion

    }
}
