﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace FiberSectionAnalysis.AxialForceMomentAnalysis
{
    public enum AxialForceMomentAnalysisType
    {
        [Description("Single Angle")]
        SingleAngle,
        [Description("Suite")]
        Suite
    }
}
