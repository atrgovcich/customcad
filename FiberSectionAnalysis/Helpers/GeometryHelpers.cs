﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.Elements;
using Utilities.Geometry;

namespace FiberSectionAnalysis.Helpers
{
    public static class GeometryHelpers
    {
        public static List<FiberSectionMeshElement> ConvertGenericPolygonsToMeshElements(List<GenericPolygon> polygons)
        {
            if (polygons != null)
            {
                List<FiberSectionMeshElement> elements = new List<FiberSectionMeshElement>(polygons.Count);

                for (int i = 0; i < polygons.Count; i++)
                {
                    elements.Add(ConvertGenericPolygonToMeshElement(polygons[i]));
                }

                return elements;
            }
            else
            {
                return null;
            }
        }

        public static FiberSectionMeshElement ConvertGenericPolygonToMeshElement(GenericPolygon polygon)
        {
            List<PointD> vertices = polygon.Points;

            return new FiberSectionMeshElement(vertices);
        }
    }
}
