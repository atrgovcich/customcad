﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiberSectionAnalysis.MomentCurvatureAnalysis
{
    public class SolutionSchemeParameters
    {
        public double Upper { get; set; } = 0;
        public double Lower { get; set; } = 0;
        public double ak { get; set; } = 0;
        public double bk { get; set; } = 0;
        public double ck { get; set; } = 0;
        public double fak { get; set; } = 0;
        public double fbk { get; set; } = 0;

    }
}
