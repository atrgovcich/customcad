﻿using System.Collections.Generic;
using FiberSectionAnalysis.General;
using Newtonsoft.Json;
using System;

namespace FiberSectionAnalysis.MomentCurvatureAnalysis
{
    [Serializable]
    public class MomentCurvatureAnalysisCase : GeneralAnalysisCase
    {
        #region Public Properties
        /// <summary>
        /// The bending angle, in degrees, measured counter-clockwise from the positive x-axis
        /// </summary>
        public double Angle { get; set; } = 0;
        /// <summary>
        /// The maximum number of iterations before the analysis will exit
        /// </summary>
        public int MaxNumberOfIterations { get; set; } = 20;
        /// <summary>
        /// The acceptable axial force difference (convergence tolerance) for the analysis
        /// </summary>
        public double AcceptableForceDifference { get; set; } = 1;
        /// <summary>
        /// Does the applied axial force include the pre-compression force from PT?
        /// </summary>
        public bool AppliedAxialForceIncludesPT { get; set; } = false;
        /// <summary>
        /// List of applied axial forces to run the analysis at.  Compression is positive, tension is negative.
        /// </summary>
        public List<double> AppliedAxialForces { get; set; }

        [JsonIgnore]
        public MomentCurvatureSuite AnalysisSuite { get; set; }
        #endregion

        #region Public Methods
        public void Analyze()
        {

        }
        #endregion
    }
}
