﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.MomentCurvatureAnalysis;
using FiberSectionAnalysis.General;
using Materials;
using FiberSectionAnalysis.LimitStates;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace FiberSectionAnalysis.MomentCurvatureAnalysis
{
    public class MomentCurvatureSuite
    {
        #region Private Backing Fields
        private List<MomentCurvature> _analyses;

        private Dictionary<IGenericMaterial, List<LimitState>> _materialLimitStates;
        #endregion

        #region Public Properties
        /// <summary>
        /// Returns the list of axial force moment analyses
        /// </summary>
        [JsonIgnore]
        public List<MomentCurvature> Analyses
        {
            get
            {
                return _analyses;
            }
        }
        /// <summary>
        /// The list of all angles, in raidans, that will be analyzed
        /// </summary>
        public List<double> AnglesToAnalyze { get; set; }
        public List<double> ForcesToAnalyze { get; set; }
        /// <summary>
        /// The fiber section model containing the mesh, rebar, and PT elements
        /// </summary>
        public FiberSection SectionModel { get; set; }
        /// <summary>
        /// The approximate number of points each axial force moment analysis should contain
        /// </summary>
        public int NumberOfPoints { get; set; }
        /// <summary>
        /// Maximum number of iterations before analysis will exit
        /// </summary>
        public int MaxNumberOfIterations { get; set; }
        /// <summary>
        /// The axial force convergence tolerance
        /// </summary>
        public double AxialForceConvergenceTolerance { get; set; }
        /// <summary>
        /// Dictionary of material limit states
        /// </summary>
        [JsonIgnore]
        public Dictionary<IGenericMaterial, List<LimitState>> MaterialLimitStates
        {
            get
            {
                return _materialLimitStates;
            }
            set
            {
                _materialLimitStates = value;
            }
        }

        public List<KeyValuePair<IGenericMaterial, List<LimitState>>> MaterialLimitStatesList
        {
            get
            {
                if (_materialLimitStates != null)
                {
                    return _materialLimitStates.ToList();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    _materialLimitStates = value.ToDictionary(kv => kv.Key, kv => kv.Value);
                }
            }
        }
        /// <summary>
        /// Whether the initial post tensioning force (if any) is included in the axial force-moment diagram.
        /// If set to false, the initial post tensioning force will be subtraced from the axial load diagram,
        /// producing a smaller interaction diagram.
        /// </summary>
        public bool IncludeInitialPostTensioningForceInAxialLoad { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the axial force moment suite
        /// </summary>
        /// <param name="anglesToAnalyze">List of angles, in radians, at which an axial force moment analysis should be run</param>
        public MomentCurvatureSuite(List<double> anglesToAnalyze, List<double> forcesToAnalyze)
        {
            AnglesToAnalyze = new List<double>(anglesToAnalyze);

            ForcesToAnalyze = new List<double>(forcesToAnalyze);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Performs an axial force-moment analysis on all of the angles specified
        /// </summary>
        public void AnalyzeSuite()
        {
            if (Validate(out string validationMessage) == false)
            {
                throw new Exception(validationMessage);
            }

            SectionModel.CalculateSectionProperties();

            _analyses = new List<MomentCurvature>();

            for (int i = 0; i < AnglesToAnalyze.Count; i++)
            {
                for (int j = 0; j < ForcesToAnalyze.Count; j++)
                {
                    _analyses.Add(new MomentCurvature(SectionModel, AnglesToAnalyze[i], ForcesToAnalyze[j], NumberOfPoints, MaxNumberOfIterations, AxialForceConvergenceTolerance, MaterialLimitStates, IncludeInitialPostTensioningForceInAxialLoad));
                }
            }

            Task[] taskArray = new Task[_analyses.Count];

            int count = 0;

            for (int i = 0; i < _analyses.Count; i++)
            {
                int ind = count;

                taskArray[ind] = Task.Run(() => _analyses[ind].RunAnalysis());

                count++;
            }

            Task.WaitAll(taskArray);
        }
        #endregion

        #region Protected Methods
        protected bool Validate(out string message)
        {
            message = string.Empty;

            if (AnglesToAnalyze == null)
            {
                message = "The list of angles to analyze was null";

                return false;
            }

            if (ForcesToAnalyze == null)
            {
                message = "The list of axial forces to analyze was null";

                return false;
            }

            if (AnglesToAnalyze.Count <= 0)
            {
                message = "The list of angles to analyze contains no items";

                return false;
            }

            if (ForcesToAnalyze.Count <= 0)
            {
                message = "The list of axial forces to analyze contains no items";

                return false;
            }

            if (SectionModel == null)
            {
                message = "The fiber section model was null";

                return false;
            }

            return true;
        }
        #endregion
    }
}
