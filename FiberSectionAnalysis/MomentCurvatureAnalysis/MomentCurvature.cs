﻿using System;
using System.Collections.Generic;
using Utilities.Geometry;
using FiberSectionAnalysis.General;
using FiberSectionAnalysis.Results;
using FiberSectionAnalysis.LimitStates;
using static System.Math;
using FiberSectionAnalysis.Elements;
using Materials;

namespace FiberSectionAnalysis.MomentCurvatureAnalysis
{
    public class MomentCurvature : GeneralFiberSectionAnalysis
    {

        #region Public Properties
        /// <summary>
        /// The axial force applied to the section at which the analysis is run.
        /// Compression is positive, tension is negative
        /// </summary>
        public double AppliedAxialForce { get; set; }
        /// <summary>
        /// The maximum number of iterations allowed before the analysis will terminate
        /// </summary>
        public int MaxNumberOfIterations { get; set; } = 20;
        /// <summary>
        /// The convergence tolerance for axial force.  When the difference between the computed
        /// axial force and the applied axial force is less than this value, the analysis will
        /// move on to the next step.
        /// </summary>
        public double AxialForceConvergenceTolerance { get; set; } = 5;

        public SolutionSchemeType SolutionScheme { get; set; } = SolutionSchemeType.Bisection;

        public List<string> AnalysisLog { get; set; } = new List<string>();
        #endregion

        #region Constructors
        public MomentCurvature(FiberSection sectionModel, double analysisAngle, double axialForce, int numberOfPoints, int maxIterations, double converganceTolerance, Dictionary<IGenericMaterial, List<LimitState>> materialLimitStates, bool includeInitialPostTensioningForceInAxialLoad)
        {
            SectionModel = sectionModel;
            AnalysisAngle = analysisAngle;
            NumberOfPoints = numberOfPoints;
            MaxNumberOfIterations = maxIterations;
            AppliedAxialForce = axialForce;
            AxialForceConvergenceTolerance = converganceTolerance;
            MaterialLimitStates = materialLimitStates;
            IncludeInitialPostTensioningForceInAxialLoad = includeInitialPostTensioningForceInAxialLoad;
        }
        #endregion

        #region Public Methods
        public void RunAnalysis()
        {
            InstantiateVariables();

            ComputeUnitVectors();

            ComputeNeutralAxisProperties();

            FurthestTensionCompressionPoints furthestPoints = SectionModel.GetFurthestTensionCompressionPoints(_neutralAxis);

            double tcDist = GetTensionCompressionDistance(_neutralAxis, furthestPoints);

            Edge compressionPointAxis = _neutralAxis.Line.CreateParallelEdgeAtPoint(furthestPoints.CompressionPoint);

            Edge tensionPointAxis = _neutralAxis.Line.CreateParallelEdgeAtPoint(furthestPoints.TensionPoint);

            double phi = 0; //Curvature, initially starts at 0

            LimitStateElement compressionLimitStateZeroCurvature = GetCompressionLimitState(_neutralAxis, furthestPoints, 0);

            LimitStateElement tensionLimitStateZeroCurvature = GetTensionLimitState(_neutralAxis, furthestPoints, 0);

            double maxStrainDifference = compressionLimitStateZeroCurvature.FurthestFiberStrain - tensionLimitStateZeroCurvature.FurthestFiberStrain;

            double maxRotation = 0.04; //radians

            double maxCurvature = maxRotation / (0.75 * tcDist);

            double phiIncrement = maxCurvature / Convert.ToDouble(NumberOfPoints);

            double ptForceToAdd = (IncludeInitialPostTensioningForceInAxialLoad == true) ? GetTotalInitialPrestressingForce() : 0;

            int pointCounter = 0;

            bool limitStateReached = false;

            while (limitStateReached == false)
            {
                MomentCurvatureAnalysisResult sectionResults = new MomentCurvatureAnalysisResult();

                sectionResults.AxialForce = AppliedAxialForce + 100 * AxialForceConvergenceTolerance;

                FiberSectionElementResults elementResults = new FiberSectionElementResults(phi);

                List<LimitStateRecord> limitStatesReached = new List<LimitStateRecord>();

                LimitStateElement compressionLimitState = GetCompressionLimitState(_neutralAxis, furthestPoints, phi);

                LimitStateElement tensionLimitState = GetTensionLimitState(_neutralAxis, furthestPoints, phi);

                double compressionFiberStrain = compressionLimitState.FurthestFiberStrain; //strain in the outermost compression fiber

                double tensionFiberStrain = tensionLimitState.FurthestFiberStrain; //strain in the outermost tension fiber

                int loopCounter = 0;

                double cPoint, upper, lower, ak, bk, ck, fak, fbk;

                cPoint = ak = bk = ck = fak = fbk = 0;

                upper = compressionFiberStrain;

                lower = (tensionFiberStrain + tcDist * phi);

                if (SolutionScheme.Equals(SolutionSchemeType.Bisection) == true)
                {
                    cPoint = (upper + lower) / 2.0;
                }
                else if (SolutionScheme.Equals(SolutionSchemeType.DoubleFalsePrecision) == true)
                {
                    ak = upper;
                    bk = lower;
                    cPoint = upper;
                }

                while (Abs(sectionResults.AxialForce - (AppliedAxialForce + ptForceToAdd)) > AxialForceConvergenceTolerance)
                {
                    sectionResults = new MomentCurvatureAnalysisResult();

                    sectionResults.AppliedAxialForce = AppliedAxialForce + ptForceToAdd;   //The total applied axial force must include the
                                                                                           //added PT force (if any) to compare apples-to-apples
                    limitStatesReached = new List<LimitStateRecord>();

                    elementResults = new FiberSectionElementResults(phi);

                    #region Mesh Element Calculations
                    //Mesh element calculations
                    if (SectionModel.FiberMeshElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberMeshElements.Count; i++)
                        {
                            FiberSectionMeshElement elem = SectionModel.FiberMeshElements[i];

                            PointD c = elem.Centroid();

                            FiberElementResult res = new FiberElementResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            res.Strain = cPoint - phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Concrete += res.Force;

                            sectionResults.MomentMajor_Concrete += res.Mmaj;

                            sectionResults.MomentMinor_Concrete += res.Mmin;

                            sectionResults.MomentX_Concrete += res.Mx;

                            sectionResults.MomentY_Concrete += res.My;

                            sectionResults.MaxFiberElementStrain = Max(res.Strain, sectionResults.MaxFiberElementStrain);

                            sectionResults.MinFiberElementStrain = Min(res.Strain, sectionResults.MinFiberElementStrain);

                            elementResults.MeshElementResults.Add(elem, res);

                            //Check if a limit state was reached
                            CheckForLimitStates(elem, res.Strain, phi, ref limitStatesReached);
                        }
                    }
                    #endregion

                    #region Rebar Element Calculations
                    //Rebar element calculations
                    if (SectionModel.FiberRebarElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberRebarElements.Count; i++)
                        {
                            FiberSectionRebarElement elem = SectionModel.FiberRebarElements[i];

                            PointD c = elem.Centroid();

                            RebarSectionResult res = new RebarSectionResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            res.Strain = cPoint - phi * dist;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.Area();

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(res.Strain);

                            res.ForceRemoved = stressRemoved * elem.Area();

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_Rebar += res.Force;

                            sectionResults.MomentMajor_Rebar += res.Mmaj;

                            sectionResults.MomentMinor_Rebar += res.Mmin;

                            sectionResults.MomentX_Rebar += res.Mx;

                            sectionResults.MomentY_Rebar += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.RebarElementResults.Add(elem, res);

                            //Check if a limit state was reached
                            CheckForLimitStates(elem, res.Strain, phi, ref limitStatesReached);
                        }
                    }
                    #endregion

                    #region PT Element Calculations
                    //PT element calculations
                    if (SectionModel.FiberPostTensioningElements != null)
                    {
                        for (int i = 0; i < SectionModel.FiberPostTensioningElements.Count; i++)
                        {
                            FiberSectionPostTensioningElement elem = SectionModel.FiberPostTensioningElements[i];

                            PointD c = elem.Centroid();

                            PostTensioningResult res = new PostTensioningResult();

                            double dist = compressionPointAxis.PerpendicularDistance(c);

                            double strain = cPoint - phi * dist;

                            double ptStrain = elem.InitialPrestrain;

                            if (elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedGroutedReleased) == true || elem.JackingAndGrouting.Equals(JackingAndGroutingType.JackedReleasedGrouted) == true)
                            {
                                ptStrain += strain;
                            }

                            res.Strain = ptStrain;

                            res.Stress = elem.Material.MonotonicStressStrain(res.Strain);

                            res.Force = res.Stress * elem.TendonArea;

                            res.Mx = res.Force * (c.Y - SectionModel.SectionCentroid.Y);

                            res.My = res.Force * (SectionModel.SectionCentroid.X - c.X);

                            res.Mmaj = res.Mx * _unitVectorMajor.X + res.My * _unitVectorMajor.Y;

                            res.Mmin = res.Mx * _unitVectorMinor.X + res.My * _unitVectorMinor.Y;

                            double areaRemoved = (elem.JackingAndGrouting.Equals(JackingAndGroutingType.Unbonded) == true) ? elem.Area() : elem.TendonArea;

                            double stressRemoved = elem.MaterialRemoved.MonotonicStressStrain(strain);

                            res.ForceRemoved = stressRemoved * areaRemoved;

                            res.MxRemoved = res.ForceRemoved * (c.Y - SectionModel.SectionCentroid.Y);

                            res.MyRemoved = res.ForceRemoved * (SectionModel.SectionCentroid.X - c.X);

                            res.MmajRemoved = res.MxRemoved * _unitVectorMajor.X + res.MyRemoved * _unitVectorMajor.Y;

                            res.MminRemoved = res.MxRemoved * _unitVectorMinor.X + res.MyRemoved * _unitVectorMinor.Y;

                            sectionResults.AxialForce_PT += res.Force;

                            sectionResults.MomentMajor_PT += res.Mmaj;

                            sectionResults.MomentMinor_PT += res.Mmin;

                            sectionResults.MomentX_PT += res.Mx;

                            sectionResults.MomentY_PT += res.My;

                            sectionResults.AxialForce_Removed += res.ForceRemoved;

                            sectionResults.MomentX_Removed += res.MxRemoved;

                            sectionResults.MomentY_Removed += res.MyRemoved;

                            sectionResults.MomentMajor_Removed += res.MmajRemoved;

                            sectionResults.MomentMinor_Removed += res.MminRemoved;

                            elementResults.PostTensioningElementResults.Add(elem, res);

                            //Check if a limit state was reached
                            CheckForLimitStates(elem, res.Strain, phi, ref limitStatesReached);
                        }
                    }
                    #endregion

                    #region Sum Section Forces
                    sectionResults.AxialForce = sectionResults.AxialForce_Concrete + sectionResults.AxialForce_Rebar + sectionResults.AxialForce_PT - sectionResults.AxialForce_Removed;

                    sectionResults.MomentX = sectionResults.MomentX_Concrete + sectionResults.MomentX_Rebar + sectionResults.MomentX_PT - sectionResults.MomentX_Removed;

                    sectionResults.MomentY = sectionResults.MomentY_Concrete + sectionResults.MomentY_Rebar + sectionResults.MomentY_PT - sectionResults.MomentY_Removed;

                    sectionResults.MomentMajor = sectionResults.MomentMajor_Concrete + sectionResults.MomentMajor_Rebar + sectionResults.MomentMajor_PT - sectionResults.MomentMajor_Removed;

                    sectionResults.MomentMinor = sectionResults.MomentMinor_Concrete + sectionResults.MomentMinor_Rebar + sectionResults.MomentMinor_PT - sectionResults.MomentMinor_Removed;
                    #endregion

                    #region Calculate Reduced Values
                    double maxRebarTensionStrain = 0;

                    foreach (KeyValuePair<FiberSectionRebarElement, RebarSectionResult> kvp in elementResults.RebarElementResults)
                    {
                        maxRebarTensionStrain = Min(maxRebarTensionStrain, kvp.Value.Strain);
                    }

                    double phiFactor = GetPhiFactor(maxRebarTensionStrain);

                    sectionResults.ReducedMomentX = sectionResults.MomentX * phiFactor;

                    sectionResults.ReducedMomentY = sectionResults.MomentY * phiFactor;

                    sectionResults.ReducedMomentMajor = sectionResults.MomentMajor * phiFactor;

                    sectionResults.ReducedMomentMinor = sectionResults.MomentMinor * phiFactor;

                    sectionResults.PhiFactor = phiFactor;

                    sectionResults.Curvature = phi;
                    #endregion

                    #region Calculate Neutral Axis
                    if (phi == 0)
                    {
                        sectionResults.NeutralAxisDepth = double.PositiveInfinity;
                    }
                    else
                    {
                        sectionResults.NeutralAxisDepth = compressionFiberStrain / phi;
                    }
                    #endregion

                    sectionResults.LimitStatesReached = limitStatesReached;

                    double axialForceDifference = sectionResults.AxialForce - (AppliedAxialForce - ptForceToAdd);

                    sectionResults.AxialForceDifference = axialForceDifference;

                    if (Abs(sectionResults.AxialForce - (AppliedAxialForce - ptForceToAdd)) <= AxialForceConvergenceTolerance)
                    {
                        _elementResults.Add(elementResults);
                        _analysisResults.Add(sectionResults);

                        if (limitStatesReached != null && limitStatesReached.Count > 0)
                        {
                            for (int j = 0; j < limitStatesReached.Count; j++)
                            {
                                if (limitStatesReached[j].Termination == true)
                                {
                                    limitStateReached = true;
                                }
                            }
                        }

                    }
                    else
                    {
                        if (SolutionScheme.Equals(SolutionSchemeType.Bisection) == true)
                        {
                            if (sectionResults.AxialForce > AppliedAxialForce - ptForceToAdd)
                            {
                                upper = cPoint;                          
                            }
                            else
                            {
                                lower = cPoint;
                            }

                            cPoint = (upper + lower) / 2.0;
                        }
                        else
                        {
                            if (loopCounter == 0)
                            {
                                if (axialForceDifference < 0 && phi > 0)
                                {
                                    cPoint = ak;
                                }
                                else
                                {
                                    fak = axialForceDifference;

                                    cPoint = bk;

                                    loopCounter++;
                                }
                            }
                            else if (loopCounter == 1)
                            {
                                fbk = axialForceDifference;

                                ck = bk - fbk * (bk - ak) / (fbk - fak);

                                cPoint = ck;
                            }
                            else
                            {
                                if (Sign(axialForceDifference) == Sign(fak))
                                {
                                    ak = ck;

                                    fak = axialForceDifference;
                                }
                                else if (Sign(axialForceDifference) == Sign(fbk))
                                {
                                    bk = ck;

                                    fbk = axialForceDifference;
                                }
                                else
                                {
                                    if (Abs(fak) < Abs(fbk))
                                    {
                                        bk = ck;

                                        fbk = axialForceDifference;
                                    }
                                    else
                                    {
                                        ak = ck;

                                        fak = axialForceDifference;
                                    }
                                }
                            }
                        }

                        if (loopCounter > MaxNumberOfIterations)
                        {
                            limitStateReached = true;

                            AnalysisLog.Add($"Convergence failure.  Reached the maximum number of iterations({MaxNumberOfIterations}) at a curvature of {Round(phi, 5)} rad/in.");

                            break;
                        }

                        loopCounter++;
                    }
                }

                phi += phiIncrement;
            }
        }


        #endregion

        #region Private Methods
        private double GetTotalInitialPrestressingForce()
        {
            double totalPrestressingForce = 0;

            if (SectionModel != null)
            {
                if (SectionModel.FiberPostTensioningElements != null && SectionModel.FiberPostTensioningElements.Count > 0)
                {
                    for (int i = 0; i < SectionModel.FiberPostTensioningElements.Count; i++)
                    {
                        FiberSectionPostTensioningElement fsPT = SectionModel.FiberPostTensioningElements[i];

                        totalPrestressingForce += fsPT.InitialPrestress * fsPT.TendonArea;
                    }
                }
            }

            return totalPrestressingForce;
        }

        private void CheckForLimitStates(IFiberSectionElement elem, double strain, double phi, ref List<LimitStateRecord> limitStatesReached)
        {
            if (MaterialLimitStates.TryGetValue(elem.Material, out List<LimitState> lsList) == true)
            {
                List<LimitStateRecord> localLimitStatesReached = new List<LimitStateRecord>();

                for (int j = 0; j < lsList.Count; j++)
                {
                    double compLS = lsList[j].CompressionLimitStrain;
                    double tensLS = -1.0 * lsList[j].TensionLimitStrain;

                    if (strain >= compLS)
                    {
                        LimitStateRecord lsRec = new LimitStateRecord()
                        {
                            Curvature = phi,
                            Element = elem,
                            StrainDirection = StrainDirectionType.Compression,
                            LimitStateReached = lsList[j],
                            Termination = lsList[j].TerminationLimitState
                        };
                    }
                    else if (strain <= tensLS)
                    {
                        LimitStateRecord lsRec = new LimitStateRecord()
                        {
                            Curvature = phi,
                            Element = elem,
                            StrainDirection = StrainDirectionType.Tension,
                            LimitStateReached = lsList[j],
                            Termination = lsList[j].TerminationLimitState
                        };
                    }
                }

                for (int j = 0; j < localLimitStatesReached.Count; j++)
                {
                    LimitStateRecord rec1 = localLimitStatesReached[j];

                    bool alreadyReached = false;

                    for (int k = 0; k < limitStatesReached.Count; k++)
                    {
                        LimitStateRecord rec2 = limitStatesReached[k];

                        if (rec2.LimitStateReached.Equals(rec1.LimitStateReached) == true)
                        {
                            if (rec2.StrainDirection.Equals(rec1.StrainDirection) == true)
                            {
                                alreadyReached = true;

                                break;
                            }
                        }
                    }

                    if (alreadyReached == false)
                    {
                        limitStatesReached.Add(rec1);
                    }
                }
            }
        }
        #endregion
    }
}
