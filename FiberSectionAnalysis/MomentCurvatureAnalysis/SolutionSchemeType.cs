﻿using System.ComponentModel;

namespace FiberSectionAnalysis.MomentCurvatureAnalysis
{
    public enum SolutionSchemeType
    {
        [Description("Bisection")]
        Bisection,
        [Description("Double False Precision")]
        DoubleFalsePrecision
    }
}
