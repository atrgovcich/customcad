﻿using System;
using System.Collections.Generic;
using System.Text;
using FiberSectionAnalysis.Enums;

namespace FiberSectionAnalysis.Loads
{
    public class CrossSectionLoad
    {
        #region Public Properties
        public string LoadCase { get; set; }
        /// <summary>
        /// Axial force on section.  Negative is a downward load, positive is an upward load.
        /// </summary>
        public double P { get; set; }
        public double Mx { get; set; }
        public double My { get; set; }
        public double Mx_top { get; set; }
        public double My_top { get; set; }
        public double Mx_bot { get; set; }
        public double My_bot { get; set; }
        public double Vx { get; set; }
        public double Vy { get; set; }
        public CoordinateSystem CoordinateSystem { get; set; } = CoordinateSystem.Local;
        #endregion
    }
}
