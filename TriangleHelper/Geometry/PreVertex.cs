﻿using System.Collections.Generic;
using static System.Math;

namespace TriangleNetHelper.Geometry
{
    public class PreVertex : IShape
    {
        public double X { get; set; }
        public double Y { get; set; }

        private const double EPS = 0.001;

        public bool Hole { get; } = false;
        public bool IsClosed { get; } = false;

        public PreVertex(double x, double y)
        {
            X = x;
            Y = y;
        }

        public PreVertex() : this(0, 0)
        {

        }

        public List<PreVertex> Vertices
        {
            get
            {
                return new List<PreVertex>() { this };
            }
            set
            {
                if (value.Count > 1)
                {
                    throw new System.Exception("A vertex cannot contain more than one point.");
                }
                else
                {
                    if (value.Count == 1)
                    {
                        X = value[0].X;
                        Y = value[0].Y;
                    }
                    else
                    {
                        X = 0;
                        Y = 0;
                    }
                }
            }
        }

        public List<PreEdge> Edges
        {
            get
            {
                return new List<PreEdge>();
            }
        }

        public bool IsSamePointWithinTolerance(PreVertex v, double orthogonalTolerance)
        {
            if (this.Equals(v))
            {
                return true;
            }
            else
            {
                if (Abs(X - v.X) <= orthogonalTolerance && Abs(Y - v.Y) <= orthogonalTolerance)
                {
                    return true;
                }
            }

            return false;
        }
        public static bool operator ==(PreVertex V0, PreVertex V1)
        {
            if (object.ReferenceEquals(V0, null) == true)
            {
                if (object.ReferenceEquals(V1, null) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (object.ReferenceEquals(V1, null) == true)
            {
                if (object.ReferenceEquals(V0, null) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (object.ReferenceEquals(V0, V1) == true)
            {
                return true;
            }
            else if (Abs(V0.X - V1.X) <= EPS && Abs(V0.Y - V1.Y) <= EPS)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(PreVertex V0, PreVertex V1)
        {
            if (object.ReferenceEquals(V0, null) == true)
            {
                if (object.ReferenceEquals(V1, null) == true)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (object.ReferenceEquals(V1, null) == true)
            {
                if (object.ReferenceEquals(V0, null) == true)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (object.ReferenceEquals(V0, V1) == true)
            {
                return false;
            }
            else if (Abs(V0.X - V1.X) <= EPS && Abs(V0.Y - V1.Y) <= EPS)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            if (object.ReferenceEquals(this, obj) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + X.GetHashCode();
            hash = hash * 23 + Y.GetHashCode();

            return hash;
        }

        public static PreVertex operator -(PreVertex V0, PreVertex V1)
        {
            return new PreVertex(V0.X - V1.X, V0.Y - V1.Y);
        }

        public static PreVertex operator +(PreVertex V0, PreVertex V1)
        {
            return new PreVertex(V0.X + V1.X, V0.Y + V1.Y);
        }

        public static PreVertex operator /(PreVertex V0, double denom)
        {
            return new PreVertex(V0.X / denom, V0.Y / denom);
        }

        public static PreVertex operator *(PreVertex V0, double mult)
        {
            return new PreVertex(V0.X * mult, V0.Y * mult);
        }

        public double DotProduct(PreVertex v)
        {
            return (X * v.X + Y * v.Y);
        }

        public double CrossProduct(PreVertex v)
        {
            return (X * v.Y - Y * v.X);
        }

        public double DistanceTo(PreVertex V)
        {
            return Sqrt(Pow(V.X - X, 2) + Pow(V.Y - Y, 2));
        }
    }
}
