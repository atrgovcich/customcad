﻿using System;
using static System.Math;

namespace TriangleNetHelper.Geometry
{
    public class PreEdge : IEquatable<PreEdge>
    {
        public PreVertex V0 { get; set; }
        public PreVertex V1 { get; set; }

        public PreEdge(PreVertex v0, PreVertex v1)
        {
            V0 = v0;
            V1 = v1;
        }
        public PreEdge() : this(new PreVertex(), new PreVertex())
        {

        }

        public static bool operator ==(PreEdge edge1, PreEdge edge2)
        {
            if (object.ReferenceEquals(edge1, null) == true)
            {
                if (object.ReferenceEquals(edge2, null) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (object.ReferenceEquals(edge2, null) == true)
            {
                if (object.ReferenceEquals(edge1, null) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (object.ReferenceEquals(edge1, edge2) == true)
            {
                return true;
            }
            else if (edge1.V0 == edge2.V0 && edge1.V1 == edge2.V1)
            {
                return true;
            }
            else if (edge1.V1 == edge2.V0 && edge1.V0 == edge2.V1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(PreEdge edge1, PreEdge edge2)
        {
            if (object.ReferenceEquals(edge1, null) == true)
            {
                if (object.ReferenceEquals(edge2, null) == true)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (object.ReferenceEquals(edge2, null) == true)
            {
                if (object.ReferenceEquals(edge1, null) == true)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (object.ReferenceEquals(edge1, edge2) == true)
            {
                return false;
            }
            else if (edge1.V0 == edge2.V0 && edge1.V1 == edge2.V1)
            {
                return false;
            }
            else if (edge1.V1 == edge2.V0 && edge1.V0 == edge2.V1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            if (object.ReferenceEquals(this, obj) == true)
            {
                return true;
            }
            else
            {
                if (V0 == ((PreEdge)obj).V0 && V1 == ((PreEdge)obj).V1)
                {
                    return true;
                }
                else if (V0 == ((PreEdge)obj).V1 && V1 == ((PreEdge)obj).V0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public bool Equals(PreEdge other)
        {
            if (other == null)
            {
                if (this == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


            if (object.ReferenceEquals(this, other) == true)
            {
                return true;
            }
            else
            {
                if (V0 == other.V0 && V1 == other.V1)
                {
                    return true;
                }
                else if (V0 == other.V1 && V1 == other.V0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public override int GetHashCode()
        {

            PreVertex vA = null;
            PreVertex vB = null;

            PreVertex origin = new PreVertex(0, 0);

            if (origin.DistanceTo(V0) < origin.DistanceTo(V1))
            {
                vA = V0;
                vB = V1;
            }
            else if (origin.DistanceTo(V1) < origin.DistanceTo(V0))
            {
                vA = V1;
                vB = V0;
            }
            else
            {
                if (V0.X < V1.X)
                {
                    vA = V0;
                    vB = V1;
                }
                else
                {
                    vA = V1;
                    vB = V0;
                }
            }

            int hash = 17;
            // Suitable nullity checks etc, of course :)
            hash = hash * 23 + vA.ToString().GetHashCode();
            hash = hash * 23 + vB.ToString().GetHashCode();

            return hash;
        }

        public double Length
        {
            get
            {
                return Sqrt(Pow(V1.X - V0.X, 2) + Pow(V1.Y - V0.Y, 2));
            }
        }

        public PreVertex Midpoint
        {
            get
            {
                return new PreVertex((V0.X + V1.X) / 2d, (V0.Y + V1.Y) / 2d);
            }
        }

        public bool ContainsVertex(PreVertex v)
        {
            double eps = 0.001;
            double minX = Min(V0.X, V1.X);
            double maxX = Max(V0.X, V1.X);
            double minY = Min(V0.Y, V1.Y);
            double maxY = Max(V0.Y, V1.Y);

            if (v.X >= minX && v.X <= maxX)
            {
                if (v.Y >= minY && v.Y <= maxY)
                {
                    //calculate slope from V0 to v

                    if (V0.X == v.X)
                    {
                        //vertical line
                        if (V1.X != V0.X)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        double m = (v.Y - V0.Y) / (v.X - V0.X);
                        double b = V0.Y - m * V0.X;

                        if (V0.X != V1.X)
                        {
                            double m2 = (V1.Y - V0.Y) / (V1.X - V0.X);
                            double b2 = V0.Y - m2 * V0.X;

                            if (Abs(m2 - m) <= eps && Abs(b2 - b) <= eps)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
