﻿using System.Collections.Generic;

namespace TriangleNetHelper.Geometry
{
    public class PrePolygon : IShape
    {
        public List<PreVertex> Vertices { get; set; }
        public bool Hole { get; set; } = false;
        public bool IsClosed { get; } = true;

        public PrePolygon(IEnumerable<PreVertex> vertices, bool isHole)
        {
            Vertices = new List<PreVertex>();
            foreach (PreVertex v in vertices)
            {
                Vertices.Add(v);
            }

            Hole = isHole;
        }

        public PrePolygon(IEnumerable<PreVertex> vertices) : this(vertices, false)
        {
        }
        public PrePolygon()
            : this(new List<PreVertex>(), false)
        {
        }

        public double SignedArea
        {
            get
            {
                double SignedA = 0;

                for (int i = 0; i < Vertices.Count-1; i++)
                {
                    double xi = Vertices[i].X;
                    double yi = Vertices[i].Y;
                    double xi1 = Vertices[i + 1].X;
                    double yi1 = Vertices[i + 1].Y;

                    SignedA += xi * yi1 - xi1 * yi;
                }

                SignedA = 0.5 * SignedA;

                return SignedA;
            }
        }

        public void ReverseDirection()
        {
            List<PreVertex> ReversedVertices = new List<PreVertex>(Vertices.Count);

            for (int i = Vertices.Count - 1; i >= 0; i--)
            {
                ReversedVertices.Add(Vertices[i]);
            }
        }

        public List<PreEdge> Edges
        {
            get
            {
                List<PreEdge> EdgeList = new List<PreEdge>();

                int LastIndex = Vertices.Count - 1;

                if (Vertices[0] == Vertices[Vertices.Count - 1])
                {
                    LastIndex = LastIndex - 1;
                }

                for (int i = 0; i <= LastIndex; i++)
                {
                    if (i< LastIndex)
                    {
                        EdgeList.Add(new PreEdge(Vertices[i], Vertices[i + 1]));
                    }
                    else
                    {
                        EdgeList.Add(new PreEdge(Vertices[i], Vertices[0]));
                    }
                }

                return EdgeList;
            }
        }
    }
}
