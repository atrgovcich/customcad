﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TriangleNetHelper.Geometry
{
    public class PrePolyline : IShape
    {
        public List<PreVertex> Vertices { get; set; } = new List<PreVertex>();
        public bool Hole { get; protected set; } = false;
        public bool IsClosed { get; } = false;

        public PrePolyline(List<PreVertex> vertices)
        {
            Vertices = vertices;
        }

        public List<PreEdge> Edges
        {
            get
            {
                List<PreEdge> EdgeList = new List<PreEdge>();

                for (int i = 0; i <= Vertices.Count-2; i++)
                {

                    EdgeList.Add(new PreEdge(Vertices[i], Vertices[i + 1]));

                }

                return EdgeList;
            }
        }
    }
}
