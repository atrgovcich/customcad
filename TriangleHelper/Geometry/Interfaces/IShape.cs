﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TriangleNetHelper.Geometry
{
    public interface IShape
    {
        List<PreVertex> Vertices { get; set; }
        bool IsClosed { get; }

        List<PreEdge> Edges { get; }

        bool Hole { get;}
    }
}
