﻿using TriangleNetHelper.Geometry;
using TriangleNet.Geometry;
using System.Collections.Generic;
using System;
using PolyBoolCS;
using static System.Math;

using static TriangleNetHelper.GeometryFunctions;
using Polygon = TriangleNet.Geometry.Polygon;
using Segment = TriangleNet.Geometry.Segment;
using System.Linq;

namespace TriangleNetHelper
{
    public class PreProcessor
    {

        public double ORTHOGONAL_TOLERANCE = 0.0078125;
        
        public PreProcessor(double orthogonalTolerance)
        {
            ORTHOGONAL_TOLERANCE = orthogonalTolerance;
        }
        public IPolygon ProcessGeometry(List<IShape> InputGeometry, double MeshSize)
        {
            List<IShape> Geometry = PopulateGeometryList(InputGeometry);
            List<IShape> holeGeometry = GetHolesPolygons(Geometry);

            List<IShape> Boundary = null;

            MakePolygonsCounterClockwise(ref Geometry);

            List<PolyBoolCS.Polygon> polygonList = GetPolyBoolPolygons(Geometry);

            if (polygonList.Count > 1)
            {
                PolyBoolCS.Polygon boundaryPolygon = GetUnionOfPolygons(polygonList);

                Boundary = ConvertPolyBoolRegionsToPrePolygons(boundaryPolygon);

                List<IShape> holesOnBoundary = GetHolesOnBoundary(holeGeometry, Boundary);

                if (holesOnBoundary.Count >= 1)
                {
                    List<PolyBoolCS.Polygon> holePolygonList = GetPolyBoolPolygons(holesOnBoundary);

                    PolyBoolCS.Polygon unionedHoles = null;

                    if (holesOnBoundary.Count > 1)
                    {
                        unionedHoles = GetUnionOfPolygons(holePolygonList);
                    }
                    else
                    {
                        unionedHoles = holePolygonList[0];
                    }

                    boundaryPolygon = GetDifferenceOfBoundaryPolygon(boundaryPolygon, unionedHoles);
                }

                List<IShape> holesInsideBoundary = GetHolesInsideBoundary(holeGeometry, holesOnBoundary);

                if (holesInsideBoundary.Count > 1)
                {
                    List<PolyBoolCS.Polygon> holesInsidePolygonList = GetPolyBoolPolygons(holesInsideBoundary);

                    PolyBoolCS.Polygon unionedHoles = GetUnionOfPolygons(holesInsidePolygonList);

                    holesInsideBoundary = ConvertPolyBoolRegionsToPrePolygons(unionedHoles);

                    foreach (IShape shape in holesInsideBoundary)
                    {
                        ((PrePolygon)shape).Hole = true;
                    }
                }

                Boundary = ConvertPolyBoolRegionsToPrePolygons(boundaryPolygon);

                foreach (IShape shape in holesInsideBoundary)
                {
                    Boundary.Add(shape);
                }

            }
            else
            {
                foreach (IShape shape in Geometry)
                {
                    if (shape is PrePolygon)
                    {
                        Boundary = new List<IShape>();
                        Boundary.Add(shape);
                    }
                }

                if (Boundary == null)
                {
                    throw new Exception("No polygon was defined in the mesh geometry.  No boundary could be determined.");
                }
            }


            for (int i = 0; i < Boundary.Count; i++)
            {
                if (Boundary[i].GetType() == typeof(PrePolygon))
                {
                    if (((PrePolygon)Boundary[i]).SignedArea < 0)
                    {
                        ((PrePolygon)Boundary[i]).ReverseDirection();
                    }
                }
            }



            List<PreEdge> interiorEdges = GetInternalEdges(Boundary, holeGeometry, Geometry);

            #region SPLIT INTERIOR EDGES THAT INTERSECT
            int count1 = 0;
            while (count1 < interiorEdges.Count)
            {
                int count2 = 0;
                while (count2 < interiorEdges.Count)
                {
                    bool deleted1 = false;

                    if (count2 != count1)
                    {
                        if (interiorEdges[count2].V0.IsSamePointWithinTolerance(interiorEdges[count1].V0, ORTHOGONAL_TOLERANCE))
                        {
                            interiorEdges[count2].V0 = interiorEdges[count1].V0;
                        }
                        else if (interiorEdges[count2].V0.IsSamePointWithinTolerance(interiorEdges[count1].V1, ORTHOGONAL_TOLERANCE))
                        {
                            interiorEdges[count2].V0 = interiorEdges[count1].V1;
                        }
                        else if (interiorEdges[count2].V1.IsSamePointWithinTolerance(interiorEdges[count1].V0, ORTHOGONAL_TOLERANCE))
                        {
                            interiorEdges[count2].V1 = interiorEdges[count1].V0;
                        }
                        else if (interiorEdges[count2].V1.IsSamePointWithinTolerance(interiorEdges[count1].V1, ORTHOGONAL_TOLERANCE))
                        {
                            interiorEdges[count2].V1 = interiorEdges[count1].V1;
                        }
                        else if (PointLiesOnLine(interiorEdges[count2].V0, interiorEdges[count2].V1, interiorEdges[count1].V0, ORTHOGONAL_TOLERANCE) == true)
                        {
                            if (interiorEdges[count2].V0.IsSamePointWithinTolerance(interiorEdges[count1].V0, ORTHOGONAL_TOLERANCE))
                            {
                                interiorEdges[count2].V0 = interiorEdges[count1].V0;
                            }
                            else if (interiorEdges[count2].V1.IsSamePointWithinTolerance(interiorEdges[count1].V0, ORTHOGONAL_TOLERANCE))
                            {
                                interiorEdges[count2].V1 = interiorEdges[count1].V0;
                            }
                            else
                            {
                                PreEdge newEdge1 = new PreEdge(interiorEdges[count2].V0, interiorEdges[count1].V0);
                                PreEdge newEdge2 = new PreEdge(interiorEdges[count1].V0, interiorEdges[count2].V1);
                                interiorEdges.RemoveAt(count2);
                                interiorEdges.Add(newEdge1);
                                interiorEdges.Add(newEdge2);

                                if (count2 < count1)
                                {
                                    count1--;
                                }
                                count2--;
                            }
                        }
                        else if (PointLiesOnLine(interiorEdges[count2].V0, interiorEdges[count2].V1, interiorEdges[count1].V1, ORTHOGONAL_TOLERANCE) == true)
                        {
                            if (interiorEdges[count2].V0.IsSamePointWithinTolerance(interiorEdges[count1].V1, ORTHOGONAL_TOLERANCE))
                            {
                                interiorEdges[count2].V0 = interiorEdges[count1].V1;
                            }
                            else if (interiorEdges[count2].V1.IsSamePointWithinTolerance(interiorEdges[count1].V1, ORTHOGONAL_TOLERANCE))
                            {
                                interiorEdges[count2].V1 = interiorEdges[count1].V1;
                            }
                            else
                            {
                                PreEdge newEdge1 = new PreEdge(interiorEdges[count2].V0, interiorEdges[count1].V1);
                                PreEdge newEdge2 = new PreEdge(interiorEdges[count1].V1, interiorEdges[count2].V1);
                                interiorEdges.RemoveAt(count2);
                                interiorEdges.Add(newEdge1);
                                interiorEdges.Add(newEdge2);

                                if (count2 < count1)
                                {
                                    count1--;
                                }
                                count2--;
                            }
                        }
                        else if (EdgesIntersect(interiorEdges[count1], interiorEdges[count2], ORTHOGONAL_TOLERANCE, out PreVertex Intersection) == true)
                        {
                            PreEdge newEdge1 = new PreEdge(interiorEdges[count2].V0, Intersection);
                            PreEdge newEdge2 = new PreEdge(Intersection, interiorEdges[count2].V1);

                            PreEdge newEdge3 = new PreEdge(interiorEdges[count1].V0, Intersection);
                            PreEdge newEdge4 = new PreEdge(Intersection, interiorEdges[count1].V1);

                            interiorEdges.RemoveAt(count1);

                            if (count1 < count2)
                            {
                                count2--;
                            }
                            count1--;

                            interiorEdges.RemoveAt(count2);

                            if (count2 <= count1)
                            {
                                count1--;
                            }
                            count2--;

                            interiorEdges.Add(newEdge1);
                            interiorEdges.Add(newEdge2);
                            interiorEdges.Add(newEdge3);
                            interiorEdges.Add(newEdge4);

                            deleted1 = true;
                        }
                    }
                    if (deleted1 == true)
                    {
                        break;
                    }
                    count2++;

                }
                count1++;
            }
            #endregion

            #region ADD VERTICES TO BOUNDARY AT INTERSECTIONS WITH INTERIOR EDGES
            foreach (IShape shape in Boundary)
            {
                List<PreVertex> shapeVertices = shape.Vertices;

                foreach (PreEdge edge in interiorEdges)
                {

                    int count = 0;

                    while (count < shapeVertices.Count - 1)
                    {
                        if (PointLiesOnLine(shapeVertices[count], shapeVertices[count + 1], edge.V0, ORTHOGONAL_TOLERANCE) == true)
                        {
                            if (edge.V0.IsSamePointWithinTolerance(shapeVertices[count], ORTHOGONAL_TOLERANCE))
                            {
                                edge.V0 = shapeVertices[count];
                            }
                            else if (edge.V0.IsSamePointWithinTolerance(shapeVertices[count + 1], ORTHOGONAL_TOLERANCE))
                            {
                                edge.V0 = shapeVertices[count + 1];
                            }
                            else
                            {
                                shapeVertices.Insert(count + 1, edge.V0);
                            }
                        }

                        if (PointLiesOnLine(shapeVertices[count], shapeVertices[count + 1], edge.V1, ORTHOGONAL_TOLERANCE) == true)
                        {
                            if (edge.V1.IsSamePointWithinTolerance(shapeVertices[count], ORTHOGONAL_TOLERANCE))
                            {
                                edge.V1 = shapeVertices[count];
                            }
                            else if (edge.V1.IsSamePointWithinTolerance(shapeVertices[count + 1], ORTHOGONAL_TOLERANCE))
                            {
                                edge.V1 = shapeVertices[count + 1];
                            }
                            else
                            {
                                shapeVertices.Insert(count + 1, edge.V1);
                            }
                        }

                        count++;
                    }
                }
                shape.Vertices = shapeVertices;
            }
            #endregion

            #region ADD INDIVIDUAL VERTICES TO SEGMENTS IF THEY LIE ON EDGE
            foreach (IShape shape in InputGeometry)
            {
                if (shape is PreVertex)
                {
                    PreVertex vert = (PreVertex)shape;

                    int count = 0;

                    foreach (IShape boundShape in Boundary)
                    {
                        List<PreVertex> shapeVertices = boundShape.Vertices;
                        int lastInd = shapeVertices.Count - 1;

                        count = 0;

                        bool boundaryClosed = false;

                        if (shapeVertices[0].X == shapeVertices[lastInd].X)
                        {
                            if (shapeVertices[0].Y == shapeVertices[lastInd].Y)
                            {
                                boundaryClosed = true;
                            }
                        }

                        int termIndex = (boundaryClosed == true) ? shapeVertices.Count - 1 : shapeVertices.Count;

                        while (count < termIndex)
                        {
                            int nextIndex = count + 1;

                            if (boundaryClosed == false)
                            {
                                if (count == termIndex - 1)
                                {
                                    nextIndex = 0;
                                }
                            }

                            if (PointLiesOnLine(shapeVertices[count], shapeVertices[nextIndex], vert, ORTHOGONAL_TOLERANCE) == true)
                            {
                                if (vert.IsSamePointWithinTolerance(shapeVertices[count], ORTHOGONAL_TOLERANCE))
                                {
                                    vert = shapeVertices[count];
                                }
                                else if (vert.IsSamePointWithinTolerance(shapeVertices[nextIndex], ORTHOGONAL_TOLERANCE))
                                {
                                    vert = shapeVertices[nextIndex];
                                }
                                else
                                {
                                    shapeVertices.Insert(nextIndex, vert);
                                }
                                break; //Point can only lie on one edge.  If two idges intersect at the location of the point, there is already a point defined there
                            }
                            count++;
                        }

                        boundShape.Vertices = shapeVertices;
                    }

                    count = 0;
                    while (count < interiorEdges.Count)
                    {
                        PreEdge edge = interiorEdges[count];

                        if (PointLiesOnLine(edge.V0, edge.V1, vert, ORTHOGONAL_TOLERANCE) == true)
                        {
                            if (vert.IsSamePointWithinTolerance(edge.V0, ORTHOGONAL_TOLERANCE))
                            {
                                vert = edge.V0;
                            }
                            else if (vert.IsSamePointWithinTolerance(edge.V1, ORTHOGONAL_TOLERANCE))
                            {
                                vert = edge.V1;
                            }
                            else
                            {
                                interiorEdges.Add(new PreEdge(edge.V0, vert));
                                interiorEdges.Add(new PreEdge(vert, edge.V1));
                                interiorEdges.RemoveAt(count);
                                count--;
                            }
                            break; //Point can only lie on one edge.  If two idges intersect at the location of the point, there is already a point defined there
                        }
                        count++;
                    }
                }
            }
            #endregion

            IPolygon input = GeneratePolygonBoundaries(Boundary, MeshSize);

            #region ADD INTERIOR EDGES AS SEGMENTS TO INPUT POLYGON
            foreach (PreEdge edge in interiorEdges)
            {
                Vertex V0 = new Vertex(edge.V0.X, edge.V0.Y, 2);
                Vertex V1 = new Vertex(edge.V1.X, edge.V1.Y, 2);

                foreach (Vertex p in input.Points)
                {
                    if (Abs(p.X - V0.X) <= ORTHOGONAL_TOLERANCE && Abs(p.Y - V0.Y) <= ORTHOGONAL_TOLERANCE)
                    {
                        V0 = p;
                    }

                    if (Abs(p.X - V1.X) <= ORTHOGONAL_TOLERANCE && Abs(p.Y - V1.Y) <= ORTHOGONAL_TOLERANCE)
                    {
                        V1 = p;
                    }
                }

                if (input.Points.Contains(V0) == true)
                {
                    if (input.Points.Contains(V1) == true)
                    {
                        input.Add(new Segment(V0, V1, 2));
                    }
                    else
                    {
                        input.Add(new Segment(V0, V1, 2), 1);
                    }
                }
                else if (input.Points.Contains(V1) == true)
                {
                    if (input.Points.Contains(V0) == true)
                    {
                        input.Add(new Segment(V0, V1, 2));
                    }
                    else
                    {
                        input.Add(new Segment(V0, V1, 2), 0);
                    }
                }
                else
                {
                    input.Add(new Segment(V0, V1, 2), true);
                }
            }
            #endregion

            #region ADD INDIVIDUAL VERTICES IF THEY DON'T ALREADY EXIST
            foreach (IShape shape in InputGeometry)
            {
                if (shape is PreVertex)
                {
                    PreVertex vert = (PreVertex)shape;

                    bool insideBoundary = false;
                    bool insideHole = false;

                    foreach (IShape boundShape in Boundary)
                    {
                        if (WindingNumber_Inside(vert, boundShape.Vertices) != 0)
                        {
                            insideBoundary = true;

                            if (boundShape.Hole == true)
                            {
                                insideHole = true;
                                break;
                            }
                        }
                    }

                    if (insideBoundary == true && insideHole == false)
                    {
                        Vertex V0 = new Vertex(vert.X, vert.Y, 3);

                        foreach (Vertex p in input.Points)
                        {
                            if (Abs(p.X - V0.X) <= ORTHOGONAL_TOLERANCE && Abs(p.Y - V0.Y) <= ORTHOGONAL_TOLERANCE)
                            {
                                V0 = p;
                                break;
                            }
                        }

                        if (input.Points.Contains(V0) == false)
                        {
                            input.Add(V0);
                        }
                    }

                }

            }
            #endregion

            return input;
        }

        private List<int> GetListOfDuplicatePolygons(List<IShape> InputGeometry)
        {
            List<int> Duplicates = new List<int>();
            for (int i = 0; i < InputGeometry.Count - 1; i++)
            {
                if (Duplicates.Contains(i) == false)
                {
                    for (int j = i + 1; j < InputGeometry.Count; j++)
                    {
                        if (InputGeometry[i] == InputGeometry[j])
                        {
                            if (Duplicates.Contains(j) == false)
                            {
                                Duplicates.Add(j);
                            }
                        }
                    }
                }
            }

            return Duplicates;
        }

        private List<IShape> PopulateGeometryList(List<IShape> InputGeometry)
        {
            List<int> Duplicates = GetListOfDuplicatePolygons(InputGeometry);

            List<IShape> Geometry = new List<IShape>();

            for (int i = 0; i < InputGeometry.Count; i++)
            {
                if (Duplicates.Contains(i) == false)
                {
                    Geometry.Add(InputGeometry[i]);
                }
            }

            return Geometry;
        }

        private IPolygon GeneratePolygonBoundaries(List<IShape> Geometry, double MeshSize)
        {
            Polygon input = new Polygon();

            foreach (IShape shape in Geometry)
            {
                int mark = Geometry.IndexOf(shape);

                List<PreVertex> vertices = shape.Vertices;

                int LastIndex = vertices.Count - 1;


                if (vertices[0].IsSamePointWithinTolerance(vertices[LastIndex],ORTHOGONAL_TOLERANCE))
                {
                    LastIndex -= 1;
                }

                //Create the input polygon, which is a list of vertices (DO NOT CLOSE THE SHAPE)
                List<Vertex> vertexList = new List<Vertex>();

                //Loop through the vertices
                for (int j = 0; j <= LastIndex; j++)
                {
                    PreVertex thisVertex = vertices[j];
                    PreVertex nextVertex = null;


                    if (j < LastIndex)
                    {
                        nextVertex = vertices[j + 1];
                    }
                    else
                    {
                        nextVertex = vertices[0];
                    }

                    //Add the vertex point
                    

                    //Get the list of discretized vertices between this point and the next
                    List<PreVertex> discretizedPreVertices = GetDiscretizedVerticesBetweenPoints(thisVertex, nextVertex, MeshSize);

                    //Check if this edge is co-linear with any other previous edge.  If so, remove any added vertices between the edge end points
                    for (int k = 0; k < mark; k++)
                    {
                        List<PreEdge> PrevEdges = Geometry[k].Edges;

                        foreach (PreEdge edge in PrevEdges)
                        {
                            if (edge.V0.IsSamePointWithinTolerance(thisVertex,ORTHOGONAL_TOLERANCE))
                            {
                                thisVertex = edge.V0;
                            }
                            else if (edge.V1.IsSamePointWithinTolerance(thisVertex, ORTHOGONAL_TOLERANCE))
                            {
                                thisVertex = edge.V1;
                            }

                            if (edge.V0.IsSamePointWithinTolerance(nextVertex, ORTHOGONAL_TOLERANCE))
                            {
                                nextVertex = edge.V0;
                            }
                            else if (edge.V1.IsSamePointWithinTolerance(nextVertex, ORTHOGONAL_TOLERANCE))
                            {
                                nextVertex = edge.V1;
                            }

                            if (IsColinear(edge, new PreEdge(thisVertex, nextVertex), ORTHOGONAL_TOLERANCE, out PreVertex Inter1, out PreVertex Inter2) == true)
                            {
                                int counter = 0;
                                while (counter < discretizedPreVertices.Count)
                                {
                                    if (thisVertex.X == nextVertex.X)
                                    {
                                        if (discretizedPreVertices[counter].Y > Inter1.Y && discretizedPreVertices[counter].Y < Inter2.Y)
                                        {
                                            discretizedPreVertices.RemoveAt(counter);
                                            counter -= 1;
                                        }
                                    }
                                    else
                                    {
                                        if (discretizedPreVertices[counter].X > Inter1.X && discretizedPreVertices[counter].X < Inter2.X)
                                        {
                                            discretizedPreVertices.RemoveAt(counter);
                                            counter -= 1;
                                        }
                                    }
                                    counter += 1;
                                }
                            }
                        }
                    }

                    vertexList.Add(new Vertex(thisVertex.X, thisVertex.Y, 1));

                    foreach (PreVertex v in discretizedPreVertices)
                    {
                        vertexList.Add(new Vertex(v.X, v.Y, 1));
                    }
                }

                input.AddContour(vertexList, 1, shape.Hole);
            }

            return input;
        }

        private List<PreVertex> GetDiscretizedVerticesBetweenPoints(PreVertex v1, PreVertex v2, double MeshSize)
        {
            List<PreVertex> vertices = new List<PreVertex>();

            double dist = Sqrt(Pow(v2.X - v1.X, 2) + Pow(v2.Y - v1.Y, 2));
            int numPts = Convert.ToInt32(Ceiling(dist / MeshSize));
            double distBtwn = dist / (numPts + 1);

            double x1 = v2.X - v1.X;
            double y1 = v2.Y - v1.Y;

            for (int i = 1; i <= numPts; i++)
            {
                double xn = v1.X + x1 / dist * (i * distBtwn);
                double yn = v1.Y + y1 / dist * (i * distBtwn);
                vertices.Add(new PreVertex(xn, yn));
            }

            return vertices;
        }

        #region OUTER BOUNDARY
        public void MakePolygonsCounterClockwise(ref List<IShape> geometry)
        {
            foreach (IShape shape in geometry)
            {
                if (shape is PrePolygon)
                {
                    if (((PrePolygon)shape).SignedArea < 0)
                    {
                        ((PrePolygon)shape).ReverseDirection();
                    }
                }
            }
        }

        public List<PolyBoolCS.Polygon> GetPolyBoolPolygons(List<IShape> geometry)
        {
            List<PolyBoolCS.Polygon> polygonList = new List<PolyBoolCS.Polygon>();

            foreach(IShape shape in geometry)
            {
                if (shape is PrePolygon)
                {
                    List<PolyBoolCS.Point> pointList = new List<PolyBoolCS.Point>(shape.Vertices.Count);

                    foreach(PreVertex v in shape.Vertices)
                    {
                        PolyBoolCS.Point p = new PolyBoolCS.Point(v.X, v.Y);

                        pointList.Add(p);
                    }

                    polygonList.Add(PolyBoolHelper.ConvertToPolygon(pointList));
                }
            }

            return polygonList;
        }

        public PolyBoolCS.Polygon GetUnionOfPolygons(List<PolyBoolCS.Polygon> polygonList)
        {
            PolyBoolCS.Polygon poly = null;

            if (polygonList.Count == 1)
            {
                return polygonList[0];
            }
            else
            {
                for (int i = 1; i < polygonList.Count; i++)
                {
                    if (i == 1)
                    {
                        poly = new PolyBoolCS.PolyBool().union(polygonList[0], polygonList[i]);
                    }
                    else
                    {
                        poly = new PolyBoolCS.PolyBool().union(poly, polygonList[i]);
                    }
                }
            }

            return poly;
        }

        public PolyBoolCS.Polygon GetDifferenceOfBoundaryPolygon(PolyBoolCS.Polygon boundaryPoly, PolyBoolCS.Polygon holePoly)
        {
            PolyBoolCS.Polygon poly = null;

            poly = new PolyBoolCS.PolyBool().difference(boundaryPoly, holePoly);

            return poly;
        }

        public List<IShape> ConvertPolyBoolRegionsToPrePolygons(PolyBoolCS.Polygon poly)
        {
            List<IShape> shapeList = new List<IShape>(poly.regions.Count);

            //Determine the outer-most shape
            //First, create a list that contains all of the shapes inside each other shape
            List<List<int>> heirarchy = new List<List<int>>(poly.regions.Count); //Stores a list of polygons that are inside of the polygon at the specified index

            for (int i = 0; i < poly.regions.Count; i++)
            {
                List<int> shapesInside = new List<int>();

                PolyBoolCS.PointList shapeVertList = poly.regions[i];

                for (int j = 0; j < poly.regions.Count; j++)
                {
                    if (j != i)
                    {
                        //Check to see if all of the points lie within any other region
                        PolyBoolCS.PointList shapeVertListToCompare = poly.regions[j];

                        bool allPointsInside = true;

                        foreach (PolyBoolCS.Point p in shapeVertListToCompare)
                        {
                            if (WindingNumber_Inside(p, shapeVertList) == 0)
                            {
                                allPointsInside = false;
                                break;
                            }
                        }

                        if (allPointsInside == true)
                        {
                            shapesInside.Add(j);
                        }
                    }
                }

                heirarchy.Add(shapesInside);
            }

            //The outermost boundaries are those that are not contained within any other shape
            List<int> outerBoundaries = new List<int>();

            for (int i = 0; i < heirarchy.Count; i++)
            {
                bool isWithinAnother = false;

                for (int j = 0; j < heirarchy.Count; j++)
                {
                    if (j != i)
                    {
                        if (heirarchy[j].Contains(i) == true)
                        {
                            isWithinAnother = true;
                            break;
                        }
                    }
                }

                if (isWithinAnother == false)
                {
                    outerBoundaries.Add(i);
                }
            }

            //Every other shape will be treated as a hole

            foreach (PolyBoolCS.PointList region in poly.regions)
            {
                int ind = poly.regions.IndexOf(region);

                List<PolyBoolCS.Point> pointList = PolyBoolHelper.ConvertRegionToPointList(region);

                List<PreVertex> vertexList = new List<PreVertex>(pointList.Count);

                foreach (PolyBoolCS.Point p in pointList)
                {
                    vertexList.Add(new PreVertex(p.x, p.y));
                }

                bool isHole = false;

                if (outerBoundaries.Contains(ind) == false)
                {
                    isHole = true;
                }

                shapeList.Add(new PrePolygon(vertexList, isHole));
            }

            return shapeList;
        }

        private List<IShape> GetHolesOnBoundary(List<IShape> geometry, List<IShape> boundary)
        {
            List<IShape> ColinearHoles = new List<IShape>();

            foreach (IShape shape in geometry)
            {
                bool colinear = false;

                if (shape is PrePolygon)
                {
                    if (shape.Hole == true)
                    {
                        foreach(IShape bound in boundary)
                        {
                            foreach(PreEdge shapeEdge in shape.Edges)
                            {
                                foreach (PreEdge boundEdge in bound.Edges)
                                {
                                    if (IsColinear(shapeEdge,boundEdge,ORTHOGONAL_TOLERANCE,out PreVertex Inter1, out PreVertex Inter2) == true)
                                    {
                                        ColinearHoles.Add(shape);
                                        colinear = true;
                                        break;
                                    }
                                }

                                if (colinear == true)
                                {
                                    break;
                                }
                            }
                            if (colinear == true)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return ColinearHoles;
        }

        private List<IShape> GetHolesPolygons(List<IShape> geometry)
        {
            List<IShape> HolePolygons = new List<IShape>();

            foreach (IShape shape in geometry)
            {
                if (shape is PrePolygon)
                {
                    if (shape.Hole == true)
                    {
                        HolePolygons.Add(shape);
                    }
                }
            }

            return HolePolygons;
        }

        private List<IShape> GetHolesInsideBoundary(List<IShape> allHoles, List<IShape> boundaryHoles)
        {
            List<IShape> holesInsideBoundary = new List<IShape>();

            foreach (IShape shape in allHoles)
            {
                if (boundaryHoles.Contains(shape) == false)
                {
                    holesInsideBoundary.Add(shape);
                }
            }

            return holesInsideBoundary;
        }

        private List<IShape> GetClosedShapesThatAreNotHoles(List<IShape> geometry)
        {
            List<IShape> list = new List<IShape>();

            foreach (IShape shape in geometry)
            {
                if (shape is PrePolygon)
                {
                    if (shape.Hole == false)
                    {
                        list.Add(shape);
                    }
                }
            }

            return list;
        }
        #endregion

        #region INTERNAL EDGES
        private List<PreEdge> GetInternalEdges(List<IShape> boundaryList, List<IShape> holeList, List<IShape> geometry)
         {
            List<PreEdge> interiorEdges = new List<PreEdge>();

            //Create a unified list with both boundary and holes
            List<IShape> allShapes = new List<IShape>(boundaryList.Count + holeList.Count);
            foreach (IShape shape in boundaryList)
            {
                allShapes.Add(shape);
            }
            foreach(IShape shape in holeList)
            {
                allShapes.Add(shape);
            }

            foreach(IShape shape in geometry)
            {
                List<PreEdge> edgeList = new List<PreEdge>();

                if (shape is PrePolygon || shape is PrePolyline)
                {
                    edgeList = shape.Edges;
                }

                foreach (PreEdge edge in edgeList)
                {
                    //First check to see if one of the vertices lie within or on the outer boundary
                    EdgeVerticesOnBoundary edgeWithinBoundary = IsEdgeWithinBoundary(edge, boundaryList);
                    
                    bool insideBoundary_V0 = edgeWithinBoundary.V0;
                    bool insideBoundary_V1 = edgeWithinBoundary.V1;

                    //If one of the edge points lies within the boundary, then check for colinearity
                    if (insideBoundary_V0 == true || insideBoundary_V1 == true)
                    {
                        List<PreEdge> edgesInside = new List<PreEdge>();
                        edgesInside.Add(edge);

                        int count = 0;
                        while(count <= edgesInside.Count-1)
                        {
                            PreEdge edgeToCheck = edgesInside[count];

                            //Next, check to see if the current edge segment is colinear with a boundary edge
                            bool foundColinear = false;

                            foreach (IShape boundShape in allShapes)
                            {
                                foreach (PreEdge boundEdge in boundShape.Edges)
                                {
                                    if (IsColinear(edgeToCheck, boundEdge, ORTHOGONAL_TOLERANCE, out PreVertex Intersection1, out PreVertex Intersection2) == true) //If the edge is colinear with this boundary segment
                                    {
                                        SplitColinearEdges(edgeToCheck, Intersection1, Intersection2, ref edgesInside);
                                        
                                        foundColinear = true;
                                        edgesInside.RemoveAt(count); //Remove the edge in question.  Remember, we split this edge and added it's sub-segments to the end of the list
                                        count--;
                                        break;
                                    }
                                }

                                if (foundColinear == true)
                                {
                                    break;
                                }
                            }
                            count++;
                        }

                        //Next, check to see whether any segments have a vertex outside the boundary
                        //If so, find the intersection and add only the interior line segment to the list
                        count = 0;
                        while (count <= edgesInside.Count - 1)
                        {
                            PreEdge edgeToCheck = edgesInside[count];

                            List<PreVertex> intersectionPoints = new List<PreVertex>();
                            foreach (IShape boundShape in boundaryList)
                            {
                                foreach (PreEdge boundEdge in boundShape.Edges)
                                {
                                    if (EdgesIntersect(edgeToCheck, boundEdge, ORTHOGONAL_TOLERANCE, out PreVertex intersection) == true)
                                    {
                                        bool vertExists = false;

                                        foreach (PreVertex vert in intersectionPoints)
                                        {
                                            if (intersection.IsSamePointWithinTolerance(vert, ORTHOGONAL_TOLERANCE))
                                            {
                                                vertExists = true;
                                                break;
                                            }
                                        }

                                        if (vertExists == false)
                                        {
                                            intersectionPoints.Add(intersection);
                                        }
                                    }
                                }
                            }

                            List<PreVertex> newVertexPoints = new List<PreVertex>();
                            newVertexPoints.Add(edgeToCheck.V0);
                            newVertexPoints.Add(edgeToCheck.V1);

                            if (intersectionPoints.Count >= 1)
                            {
                                foreach (PreVertex vert in intersectionPoints)
                                {
                                    bool vertExists = false;

                                    foreach (PreVertex defVertex in newVertexPoints)
                                    {
                                        if (defVertex.IsSamePointWithinTolerance(vert, ORTHOGONAL_TOLERANCE))
                                        {
                                            vertExists = true;
                                            break;
                                        }
                                    }

                                    if (vertExists == false)
                                    {
                                        int index = -1;

                                        for (int i = 0; i < newVertexPoints.Count - 1; i++)
                                        {
                                            if (Abs(newVertexPoints[i].X - newVertexPoints[i + 1].X) <= ORTHOGONAL_TOLERANCE)//If vertical line
                                            {
                                                if (newVertexPoints[i].Y < newVertexPoints[i + 1].Y)
                                                {
                                                    if (vert.Y > newVertexPoints[i].Y && vert.Y < newVertexPoints[i + 1].Y)
                                                    {
                                                        index = i + 1;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (vert.Y < newVertexPoints[i].Y && vert.Y > newVertexPoints[i + 1].Y)
                                                    {
                                                        index = i + 1;
                                                        break;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (newVertexPoints[i].X < newVertexPoints[i + 1].X)
                                                {
                                                    if (vert.X > newVertexPoints[i].X && vert.X < newVertexPoints[i + 1].X)
                                                    {
                                                        index = i + 1;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (vert.X < newVertexPoints[i].X && vert.X > newVertexPoints[i + 1].X)
                                                    {
                                                        index = i + 1;
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (index > 0)
                                        {
                                            newVertexPoints.Insert(index, vert);
                                        }
                                    }
                                }
                            }

                            if (newVertexPoints.Count > 2)
                            {
                                //if there are only two points, they are the endpoints.
                                for (int i = 0; i < newVertexPoints.Count - 1; i++)
                                {
                                    edgesInside.Add(new PreEdge(newVertexPoints[i], newVertexPoints[i + 1]));
                                }
                                edgesInside.RemoveAt(count);
                                count--;
                            }

                            count++;
                        }

                        //Check to see if the midpoint of each segment lies within the booundary.  It is possible that a segment is created
                        //where the vertices lie on the boundary and the rest of the line lies outside.  If the midpoint is outside of the
                        //boundary, remove the segment.
                        count = 0;
                        while(count<=edgesInside.Count-1)
                        {
                            PreEdge insideEdge = edgesInside[count];

                            bool insideBoundary = false;

                            foreach (IShape boundShape in boundaryList)
                            {
                                if (WindingNumber_Inside(insideEdge.Midpoint,boundShape.Vertices) != 0)
                                {
                                    insideBoundary = true;
                                    break;
                                }
                            }

                            if (insideBoundary == false)
                            {
                                edgesInside.RemoveAt(count);
                                count--;
                            }

                            count++;
                        }

                        //Check to see if the new segments are colinear with any segments that have already been added.
                        //If so, split the segments and keep only the unique segments.
                        count = 0;
                        while (count <= edgesInside.Count - 1)
                        {
                            PreEdge edgeToCheck = edgesInside[count];

                            foreach (PreEdge addedEdge in interiorEdges)
                            {
                                if (IsColinear(edgeToCheck, addedEdge, ORTHOGONAL_TOLERANCE, out PreVertex Intersection1, out PreVertex Intersection2) == true)
                                {
                                    SplitColinearEdges(edgeToCheck, Intersection1, Intersection2, ref edgesInside);

                                    edgesInside.RemoveAt(count);
                                    count--;
                                    break;
                                }
                            }
                            count++;
                        }

                        

                        //Next, check to see whether any segments cross an object defined as a hole
                        //If so, find the intersection and try to add segments outside of the hole
                        count = 0;
                        while (count <= edgesInside.Count - 1)
                        {
                            PreEdge edgeToCheck = edgesInside[count];

                            bool foundIntersection = false;

                            foreach (IShape holeShape in holeList)
                            {
                                foreach(PreEdge holeEdge in holeShape.Edges)
                                {
                                    if (EdgesIntersect(edgeToCheck,holeEdge, ORTHOGONAL_TOLERANCE, out PreVertex Intersection) == true)
                                    {
                                        if (Intersection.IsSamePointWithinTolerance(edgeToCheck.V0, ORTHOGONAL_TOLERANCE) == false && Intersection.IsSamePointWithinTolerance(edgeToCheck.V1, ORTHOGONAL_TOLERANCE) == false)
                                        {
                                            if (Intersection.IsSamePointWithinTolerance(edgeToCheck.V0, ORTHOGONAL_TOLERANCE) == false)
                                            {
                                                edgesInside.Add(new PreEdge(edgeToCheck.V0, Intersection));
                                            }

                                            if (Intersection.IsSamePointWithinTolerance(edgeToCheck.V1, ORTHOGONAL_TOLERANCE) == false)
                                            {
                                                edgesInside.Add(new PreEdge(Intersection, edgeToCheck.V1));
                                            }

                                            foundIntersection = true;

                                            edgesInside.RemoveAt(count);
                                            count--;
                                            break;
                                        }
                                    }
                                }
                                if (foundIntersection == true)
                                {
                                    break;
                                }
                            }
                            count++;
                        }

                        //Next, check to see if the midpoint of edges falls within any holes
                        //It is possible that an edge was split whose vertices fall on the boundary of a hole
                        count = 0;
                        while (count <= edgesInside.Count - 1)
                        {
                            PreEdge edgeToCheck = edgesInside[count];

                            foreach (IShape holeShape in holeList)
                            {
                                if (WindingNumber_Inside(edgeToCheck.Midpoint, holeShape.Vertices) != 0)
                                {
                                    edgesInside.RemoveAt(count);
                                    count--;
                                    break;
                                }
                            }
                            count++;
                        }

                        foreach (PreEdge insideEdge in edgesInside)
                        {
                            interiorEdges.Add(insideEdge);
                        }
                    }
                }
            }

            return interiorEdges;
        }

        private void SplitColinearEdges(PreEdge edgeToCheck, PreVertex Intersection1, PreVertex Intersection2, ref List<PreEdge> edgesInside)
        {

            double edge_xmin = Min(edgeToCheck.V0.X, edgeToCheck.V1.X);
            double edge_ymin = Min(edgeToCheck.V0.Y, edgeToCheck.V1.Y);
            double edge_xmax = Max(edgeToCheck.V0.X, edgeToCheck.V1.X);
            double edge_ymax = Max(edgeToCheck.V0.Y, edgeToCheck.V1.Y);

            if (Abs(edgeToCheck.V0.X - edgeToCheck.V1.X) <= ORTHOGONAL_TOLERANCE) //If the edge in question is a vertical line
            {
                if (Intersection1.Y > edge_ymin) //If the 1st intersection Y value is greater than the minimum Y value of the edge
                {
                    if (edgeToCheck.V0.Y == edge_ymin) //If the V0 vertex of the edge is equal to the minimum y value of the edge
                    {
                        edgesInside.Add(new PreEdge(edgeToCheck.V0, Intersection1));
                    }
                    else
                    {
                        edgesInside.Add(new PreEdge(edgeToCheck.V1, Intersection1));
                    }
                }

                if (Intersection2.Y < edge_ymax) //If the 2nd intersection Y value is less than the maximum Y value of the edge
                {
                    if (edgeToCheck.V0.Y == edge_ymax)
                    {
                        edgesInside.Add(new PreEdge(Intersection2, edgeToCheck.V0));
                    }
                    else
                    {
                        edgesInside.Add(new PreEdge(Intersection2, edgeToCheck.V1));
                    }
                }
            }
            else //The edge is not a vertical line
            {
                if (Intersection1.X > edge_xmin)
                {
                    if (edgeToCheck.V0.X == edge_xmin)
                    {
                        edgesInside.Add(new PreEdge(edgeToCheck.V0, Intersection1));
                    }
                    else
                    {
                        edgesInside.Add(new PreEdge(edgeToCheck.V1, Intersection1));
                    }
                }

                if (Intersection2.X < edge_xmax)
                {
                    if (edgeToCheck.V0.X == edge_xmax)
                    {
                        edgesInside.Add(new PreEdge(Intersection2, edgeToCheck.V0));
                    }
                    else
                    {
                        edgesInside.Add(new PreEdge(Intersection2, edgeToCheck.V1));
                    }
                }
            }
        }

        private struct EdgeVerticesOnBoundary
        {
            public bool V0;
            public bool V1;
        }

        private EdgeVerticesOnBoundary IsEdgeWithinBoundary(PreEdge edge,List<IShape> boundaryList)
        {
            bool insideBoundary_V0 = false;
            bool insideBoundary_V1 = false;

            foreach (IShape boundShape in boundaryList)
            {
                if (WindingNumber_Inside(edge.V0, boundShape.Vertices) != 0)
                {
                    insideBoundary_V0 = true;
                }
                else
                {
                    foreach (PreEdge boundEdge in boundShape.Edges)
                    {
                        if (PointLiesOnLine(boundEdge.V0, boundEdge.V1, edge.V0, ORTHOGONAL_TOLERANCE) == true)
                        {
                            insideBoundary_V0 = true;
                            break;
                        }
                    }
                }

                if (WindingNumber_Inside(edge.V1, boundShape.Vertices) != 0)
                {
                    insideBoundary_V1 = true;
                }
                else
                {
                    foreach (PreEdge boundEdge in boundShape.Edges)
                    {
                        if (PointLiesOnLine(boundEdge.V0, boundEdge.V1, edge.V1, ORTHOGONAL_TOLERANCE) == true)
                        {
                            insideBoundary_V1 = true;
                            break;
                        }
                    }
                }

                if (insideBoundary_V0 == true && insideBoundary_V1 == true)
                {
                    break;
                }
            }

            return new EdgeVerticesOnBoundary() { V0 = insideBoundary_V0, V1 = insideBoundary_V1 };
        }
        #endregion
    }
}
