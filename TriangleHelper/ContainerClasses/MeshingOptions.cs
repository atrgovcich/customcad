﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TriangleNetHelper.ContainerClasses
{
    public class MeshingOptions
    {
        public bool ConformingDelaunay { get; set; } = true;
        public double MinimumAngle { get; set; } = 28;
        public double MaximumAngle { get; set; } = 120;
        public bool Convex { get; set; } = false;
        public bool QualityMesh { get; set; } = true;
        public bool SweepLine { get; set; } = false;
        public int NumberOfRefinements { get; set; } = 0;
        public int NumberOfSmooth { get; set; } = 3;
        public bool RefineAfterSmooth { get; set; } = true;
    }

}
