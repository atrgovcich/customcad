﻿using System.Collections.Generic;
using static System.Math;
using TriangleNet.Geometry;
using TriangleNet;
using TriangleNet.Topology;
using TriangleNet.Smoothing;
using TriangleNet.Meshing;
using TriangleNet.Meshing.Algorithm;
using TriangleNetHelper.Geometry;
using TriangleNetHelper.ContainerClasses;

namespace TriangleNetHelper
{
    public class TriangleHelper
    {
        public double ORTHOGONAL_TOLERANCE = 0.0078125;

        public List<IShape> InputGeometry { get; set; }
        public double MeshSize { get; set; }
        public MeshingOptions MeshOptions { get; set; }

        private Mesh GeneratedMesh;

        public TriangleHelper(List<IShape> geometry, double meshSize, MeshingOptions options, double orthogonalTolerance)
        {
            InputGeometry = geometry;
            MeshSize = meshSize;
            MeshOptions = options;
        }

        public void CreateMesh()
        {
            PreProcessor meshPreProcessor = new PreProcessor(ORTHOGONAL_TOLERANCE);

            IPolygon inputPolygon = meshPreProcessor.ProcessGeometry(InputGeometry, MeshSize);

            Triangulate(inputPolygon);
            for (int i = 1; i <= MeshOptions.NumberOfRefinements; i++)
            {
                Refine();
            }
            for (int i = 1; i <= MeshOptions.NumberOfSmooth; i++)
            {
                Smooth();
                if (MeshOptions.RefineAfterSmooth == true)
                {
                    Refine();
                }
            }
        }

        public List<PrePolygon> RetrieveMesh
        {
            get
            {
                ICollection<Triangle> Triangles = GeneratedMesh.Triangles;

                List<PrePolygon> TriangleList = new List<PrePolygon>();

                foreach(Triangle tri in Triangles)
                {
                    TriangleList.Add(ConvertTriangleToPolygon(tri));
                }

                return TriangleList;
            }
        }

        private PrePolygon ConvertTriangleToPolygon(Triangle tri)
        {
            PreVertex v0 = new PreVertex(tri.GetVertex(0).X, tri.GetVertex(0).Y);
            PreVertex v1 = new PreVertex(tri.GetVertex(1).X, tri.GetVertex(1).Y);
            PreVertex v2 = new PreVertex(tri.GetVertex(2).X, tri.GetVertex(2).Y);

            List<PreVertex> vertices = new List<PreVertex>()
            {
                v0,
                v1,
                v2,
                v0
            };
            
            PrePolygon poly = new PrePolygon(vertices);
            return poly;
        }
        private void Triangulate(IPolygon input)
        {
            if (input == null) return;

            var options = new ConstraintOptions();
            var quality = new QualityOptions();

            if (MeshOptions.ConformingDelaunay == true)
            {
                options.ConformingDelaunay = true;
                
            }
            if (MeshOptions.QualityMesh == true)
            {
                quality.MinimumAngle = MeshOptions.MinimumAngle;
                quality.MaximumAngle = Min(MeshOptions.MaximumAngle, 180);
            }
            if (MeshOptions.Convex == true)
            {
                options.Convex = true;
            }

            if (MeshOptions.SweepLine == true)
            {
                GeneratedMesh = (Mesh)input.Triangulate(options, quality, new SweepLine());
            }
            else
            {
                GeneratedMesh = (Mesh)input.Triangulate(options, quality);
            }
        }

        private void Refine()
        {
            if (GeneratedMesh == null) return;

            double area = 1.25 * (1d / 2d) * MeshSize * MeshSize;

            var quality = new QualityOptions();

            quality.MaximumArea = area;

            quality.MinimumAngle = MeshOptions.MinimumAngle;

            quality.MaximumAngle = Min(MeshOptions.MaximumAngle, 180);

            GeneratedMesh.Refine(quality, MeshOptions.ConformingDelaunay);

        }

        private void Smooth()
        {
            if (GeneratedMesh == null ) return;

            if (!GeneratedMesh.IsPolygon)
            {
                return;
            }

            var smoother = new SimpleSmoother();

            smoother.Smooth(this.GeneratedMesh);
        }
    }
}
