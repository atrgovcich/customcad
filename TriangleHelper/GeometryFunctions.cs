﻿using System.Collections.Generic;
using TriangleNetHelper.Geometry;
using static System.Math;
using System.Linq;

namespace TriangleNetHelper
{
    public static class GeometryFunctions
    {
        /// <summary>
        /// Returns true if the point v2 lies on the lined defined by v0 and v1
        /// </summary>
        /// <param name="v0">Start point of line</param>
        /// <param name="v1">End point of line</param>
        /// <param name="v2">Point to test</param>
        /// <param name="orthogonalTolerance">Tolerance when comparing x and/or y coordinates</param>
        /// <param name="distanceTolerance">Tolerance when comparing distance between two points</param>
        /// <returns></returns>
        public static bool PointLiesOnLine(PreVertex v0, PreVertex v1, PreVertex v2, double orthogonalTolerance)
        {
            double MaxX = Max(v0.X, v1.X);
            double MinX = Min(v0.X, v1.X);

            double MaxY = Max(v0.Y, v1.Y);
            double MinY = Min(v0.Y, v1.Y);

            if (Abs(v0.X - v1.X) <= orthogonalTolerance)
            {
                if (Abs(v2.X - v0.X) <= orthogonalTolerance)
                {
                    if (v2.Y >= MinY && v2.Y <= MaxY)
                    {
                        return true;
                    }
                    else if (Abs(v2.Y - v0.Y) <= orthogonalTolerance && Abs(v2.X - v0.X) <= orthogonalTolerance)
                    {
                        return true;
                    }
                    else if (Abs(v2.Y - v1.Y) <= orthogonalTolerance && Abs(v2.X - v1.X) <= orthogonalTolerance)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                double m = (v1.Y - v0.Y) / (v1.X - v0.X);
                double b = v0.Y - m * v0.X;

                if (v2.X >= MinX && v2.X <= MaxX)
                {
                    if (Abs(v2.Y - (m * v2.X + b)) <= orthogonalTolerance)
                    {
                        return true;
                    }
                    else if (Abs(v2.Y - v0.Y) <= orthogonalTolerance && Abs(v2.X - v0.X) <= orthogonalTolerance)
                    {
                        return true;
                    }
                    else if (Abs(v2.Y - v1.Y) <= orthogonalTolerance && Abs(v2.X - v1.X) <= orthogonalTolerance)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool IsColinear(PreEdge edge1, PreEdge edge2, double orthogonalTolerance, out PreVertex int1, out PreVertex int2)
        {
            double edge1_xmin = Min(edge1.V0.X, edge1.V1.X);
            double edge1_ymin = Min(edge1.V0.Y, edge1.V1.Y);
            double edge1_xmax = Max(edge1.V0.X, edge1.V1.X);
            double edge1_ymax = Max(edge1.V0.Y, edge1.V1.Y);

            double edge2_xmin = Min(edge2.V0.X, edge2.V1.X);
            double edge2_ymin = Min(edge2.V0.Y, edge2.V1.Y);
            double edge2_xmax = Max(edge2.V0.X, edge2.V1.X);
            double edge2_ymax = Max(edge2.V0.Y, edge2.V1.Y);


            if (Abs(edge1.V0.X - edge1.V1.X) <= orthogonalTolerance)
            {
                if (Abs(edge2.V0.X - edge2.V1.X) <= orthogonalTolerance)
                {
                    if (Abs(edge1.V0.X - edge2.V0.X) <= orthogonalTolerance)
                    {
                        if ((edge1_ymin >= edge2_ymin || Abs(edge1_ymin - edge2_ymin) <= orthogonalTolerance) && edge1_ymin < edge2_ymax)
                        {
                            int1 = new PreVertex(edge1_xmin, edge1_ymin);

                            if (edge1_ymax <= edge2_ymax || Abs(edge1_ymax - edge2_ymax) <= orthogonalTolerance)
                            {
                                int2 = new PreVertex(edge1_xmax, edge1_ymax);
                            }
                            else
                            {
                                int2 = new PreVertex(edge2_xmax, edge2_ymax);
                            }

                            return true;
                        }
                        else if ((edge2_ymin >= edge1_ymin || Abs(edge2_ymin - edge1_ymin) <= orthogonalTolerance) && edge2_ymin < edge1_ymax)
                        {
                            int1 = new PreVertex(edge2_xmin, edge2_ymin);

                            if (edge2_ymax <= edge1_ymax || Abs(edge2_ymax - edge1_ymax) <= orthogonalTolerance)
                            {
                                int2 = new PreVertex(edge2_xmax, edge2_ymax);
                            }
                            else
                            {
                                int2 = new PreVertex(edge1_xmax, edge1_ymax);
                            }

                            return true;
                        }
                    }
                }
            }
            else
            {
                double m1 = (edge1.V1.Y - edge1.V0.Y) / (edge1.V1.X - edge1.V0.X);
                double b1 = edge1.V0.Y - m1 * edge1.V0.X;

                double maxSlopeDiff = (2.0 * orthogonalTolerance) / (Abs(edge1.V1.X - edge1.V0.X) - 2.0 * orthogonalTolerance);

                if (Abs(edge2.V0.X - edge2.V1.X) > orthogonalTolerance)
                {
                    double m2 = (edge2.V1.Y - edge2.V0.Y) / (edge2.V1.X - edge2.V0.X);
                    double b2 = edge2.V0.Y - m2 * edge2.V0.X;

                    if (Abs(b2 - b1) <= orthogonalTolerance && Abs(m2 - m1) <= maxSlopeDiff)
                    {
                        if ((edge2_xmin >= edge1_xmin || Abs(edge2_xmin - edge1_xmin) <= orthogonalTolerance) && edge2_xmin < edge1_xmax)
                        {
                            int1 = new PreVertex(edge2_xmin, m2 * edge2_xmin + b2);

                            if (edge2_xmax <= edge1_xmax || Abs(edge2_xmax - edge1_xmax) <= orthogonalTolerance)
                            {
                                int2 = new PreVertex(edge2_xmax, m2 * edge2_xmax + b2);
                            }
                            else
                            {
                                int2 = new PreVertex(edge1_xmax, m1 * edge1_xmax + b1);
                            }

                            return true;
                        }
                        else if ((edge1_xmin >= edge2_xmin || Abs(edge1_xmin - edge2_xmin) <= orthogonalTolerance) && edge1_xmin < edge2_xmax)
                        {
                            int1 = new PreVertex(edge1_xmin, m1 * edge1_xmin + b1);

                            if (edge1_xmax <= edge2_xmax || Abs(edge1_xmax - edge2_xmax) <= orthogonalTolerance)
                            {
                                int2 = new PreVertex(edge1_xmax, m1 * edge1_xmax + b1);
                            }
                            else
                            {
                                int2 = new PreVertex(edge2_xmax, m2 * edge2_xmax + b2);
                            }

                            return true;
                        }
                    }
                }
            }

            int1 = null;
            int2 = null;
            return false;
        }

        public static double IsLeft(PreVertex V0, PreVertex V1, PreVertex V2)
        {
            //Returns >0 if V2 is left of line passing through V0 and V1
            //Returns 0 if V2 is on the line
            //Returns <0 if V2 is right of the line passing through V0 and V1

            return (V1.X - V0.X) * (V2.Y - V0.Y) - (V2.X - V0.X) * (V1.Y - V0.Y);
        }

        public static double IsLeft(PolyBoolCS.Point V0, PolyBoolCS.Point V1, PolyBoolCS.Point V2)
        {
            //Returns >0 if V2 is left of line passing through V0 and V1
            //Returns 0 if V2 is on the line
            //Returns <0 if V2 is right of the line passing through V0 and V1

            return (V1.x - V0.x) * (V2.y - V0.y) - (V2.x - V0.x) * (V1.y - V0.y);
        }

        public static int WindingNumber_Inside(PreVertex V, List<PreVertex> Vertices)
        {
            //Requires that polygon be closed (i.e. last point = first point)
            //V = vertex
            //Vertices is list of polygon vertices
            //Returns winding number, which is == 0 only when V is outside of the polygon
            //Credit for this code goes to: http://geomalgorithms.com/a03-_inclusion.html

            int wn = 0;

            //Loop through all edges of the polygon
            for (int i = 0; i < Vertices.Count - 1; i++)
            {
                if (Vertices[i].Y <= V.Y) //If the start point 'y' value is less than the point's 'y' value
                {
                    if (Vertices[i + 1].Y > V.Y) //An upward crossing
                    {
                        if (IsLeft(Vertices[i], Vertices[i + 1], V) > 0) //V is left of the dge
                        {
                            wn += 1;
                        }
                    }
                }
                else
                {
                    if (Vertices[i + 1].Y <= V.Y) //A downward crossing
                    {
                        if (IsLeft(Vertices[i], Vertices[i + 1], V) < 0) //V is right of the edge
                        {
                            wn -= 1;
                        }
                    }
                }
            }

            return wn;
        }

        public static int WindingNumber_Inside(PolyBoolCS.Point V, PolyBoolCS.PointList Vertices)
        {
            //V = vertex
            //Vertices is list of polygon vertices
            //Returns winding number, which is == 0 only when V is outside of the polygon
            //Credit for this code goes to: http://geomalgorithms.com/a03-_inclusion.html

            int wn = 0;

            int lastIndex = Vertices.Count;

            if (Vertices[0].x == Vertices[Vertices.Count - 1].x && Vertices[0].y == Vertices[Vertices.Count - 1].y)
            {
                lastIndex = Vertices.Count - 1;
            }

            //Loop through all edges of the polygon
            for (int i = 0; i < lastIndex; i++)
            {
                PolyBoolCS.Point nextPoint;

                if (i == lastIndex - 1)
                {
                    if (lastIndex == Vertices.Count)
                    {
                        nextPoint = Vertices[0];
                    }
                    else
                    {
                        nextPoint = Vertices[i + 1];
                    }
                }
                else
                {
                    nextPoint = Vertices[i + 1];
                }

                if (Vertices[i].y <= V.y) //If the start point 'y' value is less than the point's 'y' value
                {
                    if (nextPoint.y > V.y) //An upward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, V) > 0) //V is left of the dge
                        {
                            wn += 1;
                        }
                    }
                }
                else
                {
                    if (nextPoint.y <= V.y) //A downward crossing
                    {
                        if (IsLeft(Vertices[i], nextPoint, V) < 0) //V is right of the edge
                        {
                            wn -= 1;
                        }
                    }
                }
            }

            return wn;
        }

        public static bool EdgesIntersect(PreEdge edge1, PreEdge edge2, double orthogonalTolerance, out PreVertex intersection)
        {
            double edge1_xmin = Min(edge1.V0.X, edge1.V1.X);
            double edge1_ymin = Min(edge1.V0.Y, edge1.V1.Y);
            double edge1_xmax = Max(edge1.V0.X, edge1.V1.X);
            double edge1_ymax = Max(edge1.V0.Y, edge1.V1.Y);

            double edge2_xmin = Min(edge2.V0.X, edge2.V1.X);
            double edge2_ymin = Min(edge2.V0.Y, edge2.V1.Y);
            double edge2_xmax = Max(edge2.V0.X, edge2.V1.X);
            double edge2_ymax = Max(edge2.V0.Y, edge2.V1.Y);

            intersection = null;
            double xint, yint;

            if (Abs(edge1.V0.X - edge1.V1.X) <= orthogonalTolerance)
            {
                if (Abs(edge2.V0.X - edge2.V1.X) <= orthogonalTolerance)
                {
                    return false;
                }
                else
                {
                    double m2 = (edge2.V1.Y - edge2.V0.Y) / (edge2.V1.X - edge2.V0.X);
                    double b2 = edge2.V0.Y - m2 * edge2.V0.X;

                    xint = edge1.V0.X;
                    yint = m2 * xint + b2;
                }
            }
            else
            {
                double m1 = (edge1.V1.Y - edge1.V0.Y) / (edge1.V1.X - edge1.V0.X);
                double b1 = edge1.V0.Y - m1 * edge1.V0.X;

                if (Abs(edge2.V0.X - edge2.V1.X) <= orthogonalTolerance)
                {
                    xint = edge2.V0.X;
                    yint = m1 * xint + b1;
                }
                else
                {
                    double m2 = (edge2.V1.Y - edge2.V0.Y) / (edge2.V1.X - edge2.V0.X);
                    double b2 = edge2.V0.Y - m2 * edge2.V0.X;

                    if (m1 == m2)
                    {
                        return false;
                    }
                    else
                    {
                        xint = (b2 - b1) / (m1 - m2);
                        yint = m1 * xint + b1;
                    }
                }
            }

            bool withinEdge1 = false;
            bool withinEdge2 = false;

            if (Abs(edge1.V0.X - edge1.V1.X) <= orthogonalTolerance)
            {
                if ((yint >= edge1_ymin || Abs(yint - edge1_ymin) <= orthogonalTolerance) && (yint <= edge1_ymax || Abs(yint - edge1_ymax) <= orthogonalTolerance))
                {
                    withinEdge1 = true;
                }
            }
            else
            {
                if ((xint >= edge1_xmin || Abs(xint - edge1_xmin) <= orthogonalTolerance) && (xint <= edge1_xmax || Abs(xint - edge1_xmax) <= orthogonalTolerance))
                {
                    if ((yint >= edge1_ymin || Abs(yint - edge1_ymin) <= orthogonalTolerance) && (yint <= edge1_ymax || Abs(yint - edge1_ymax) <= orthogonalTolerance))
                    {
                        withinEdge1 = true;
                    }
                }
            }

            if (Abs(edge2.V0.X - edge2.V1.X) <=orthogonalTolerance)
            {
                if ((yint >= edge2_ymin || Abs(yint - edge2_ymin) <= orthogonalTolerance) && (yint <= edge2_ymax || Abs(yint - edge2_ymax) <= orthogonalTolerance))
                {
                    withinEdge2 = true;
                }
            }
            else
            {
                if ((xint >= edge2_xmin || Abs(xint - edge2_xmin) <= orthogonalTolerance) && (xint <= edge2_xmax || Abs(xint - edge2_xmax) <= orthogonalTolerance))
                {
                    if ((yint >= edge2_ymin || Abs(yint - edge2_ymin) <= orthogonalTolerance) && (yint <= edge2_ymax || Abs(yint - edge2_ymax) <= orthogonalTolerance))
                    {
                        withinEdge2 = true;
                    }
                }
                else if (Abs(xint - edge2_xmin) <= orthogonalTolerance || Abs(xint - edge2_xmax) <= orthogonalTolerance)
                {
                    if (Abs(yint - edge2_ymin) <= orthogonalTolerance || Abs(yint - edge2_ymax) <= orthogonalTolerance)
                    {
                        withinEdge2 = true;
                    }
                }
            }

            if (withinEdge1 == true && withinEdge2 == true)
            {
                intersection = new PreVertex(xint, yint);

                if (intersection == edge1.V0)
                {
                    intersection = edge1.V0;
                }
                else if (intersection == edge1.V1)
                {
                    intersection = edge1.V1;
                }
                else if (intersection == edge2.V0)
                {
                    intersection = edge2.V0;
                }
                else if (intersection == edge2.V1)
                {
                    intersection = edge2.V1;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }

    
}
