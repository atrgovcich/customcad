﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materials.General
{
    public static class Constants
    {
        // Within imperial system
        public const int InchesPerFoot = 12;
        public const int PoundsPerKip = 1000;
        public const double KipPerCubicInchToPoundPerCubicFoot = 1000.0 * 12.0 * 12.0 * 12.0;
        public const double PoundPerCubicFootToKipPerCubicInch = 1.0 / 1000.0 / (12.0 * 12.0 * 12.0);
        public const double KsiToPsi = 1000.0;
        public const double PsiToKsi = 0.001;

        // Within metric system
        public const int MillimetersPerMeter = 1000;
        public const int NewtonsPerKilonewton = 1000;
        public const int KilonewtonPerMeganewton = 1000;

        // Between imperial and metric
        public const double InchesPerMeter = 39.37007874;
        public const double KipsPerKilonewton = 0.2248089438;
        public const double KipsPerKilogram = 0.00045359237;
        public const double KipPerCubicInchToKilogramPerCubicMeter = 27679968.0;

        // Other
        public const double DegreesPerRadian = 180 / Math.PI;
        public static readonly double SpecificGravityPerDensity = 1000d * Math.Pow(12d, 3) / 62.4;
    }
}
