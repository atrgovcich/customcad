﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materials
{
    public interface IGenericMaterial
    {
        #region Properties
        string Name { get; set; }
        double ElasticModulus { get; }
        double Density { get; }
        #endregion

        #region Public Methods
        double MonotonicStressStrain(double strain);

        double MonotonicStrainStress(double stress);

        double MaxCompressionStrain();

        double MaxTensionStrain();
        #endregion
    }
}
