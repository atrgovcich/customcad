﻿using Newtonsoft.Json;
using static System.Math;

namespace Materials.Elastic
{
    public class IsotropicMaterial : ElasticMaterial
    {
        #region Constructors

        [JsonConstructor]
        public IsotropicMaterial() { }

        public IsotropicMaterial(string name, double elasticModulus, double poissonRatio, double shearModulus, double density)
        {
            Name = name;
            E1 = elasticModulus;
            E2 = elasticModulus;
            E3 = elasticModulus;
            G12 = shearModulus;
            G13 = shearModulus;
            G23 = shearModulus;
            Nu12 = poissonRatio;
            Nu13 = poissonRatio;
            Nu21 = poissonRatio;
            Nu23 = poissonRatio;
            Nu31 = poissonRatio;
            Nu32 = poissonRatio;
            Density = density;
        }

        #endregion

        public override double[,] PlaneStressConstitutiveMatrix2D(double f11 = 1, double f22 = 1, double f12 = 1)
        {
            //This is the stiffness matrix, which is the inverse of the compliance matrix
            //Uses engineering strain
            //Plane stress formulation

            double E11 = (E / (1d - Pow(Nu, 2))) * f11;
            double E12 = (E / (1d - Pow(Nu, 2))) * Nu * Min(f11, f22);
            double E13 = 0d;
            double E21 = (E / (1d - Pow(Nu, 2))) * Nu * Min(f11, f22);
            double E22 = (E / (1d - Pow(Nu, 2))) * f22;
            double E23 = 0d;
            double E31 = 0d;
            double E32 = 0d;
            double E33 = (E / (1d - Pow(Nu, 2))) * 0.5 * (1d - Nu) * f12;

            double[,] D = new double[,]
            {
                {E11,E12,E13 },
                {E21,E22,E23 },
                {E31,E32,E33 }
            };

            return D;
        }

        public override double MonotonicStrainStress(double stress)
        {
            return stress / E;
        }

        public override double MonotonicStressStrain(double strain)
        {
            return E * strain;
        }
    }
}
