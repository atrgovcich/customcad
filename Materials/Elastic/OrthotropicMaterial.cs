﻿using System.Collections.Generic;
using Newtonsoft.Json;
using static System.Math;
using MathNet.Numerics.LinearAlgebra;
using static Utilities.MatrixLibrary.MatrixLibrary;

namespace Materials.Elastic
{
    public class OrthotropicMaterial : ElasticMaterial
    {
        #region Constructors

        [JsonConstructor]
        private OrthotropicMaterial() { }

        public OrthotropicMaterial(string name, double density, double e1, double e2, double e3, double nu12, double nu21, double nu13, double nu31, double nu23, double nu32, double g12, double g13, double g23)
        {
            Name = name;
            Density = density;
            E1 = e1;
            E2 = e2;
            E3 = e3;
            Nu12 = nu12;
            Nu21 = nu21;
            Nu13 = nu13;
            Nu31 = nu31;
            Nu23 = nu23;
            Nu32 = nu32;
            G12 = g12;
            G13 = g13;
            G23 = g23;
        }

        [JsonConstructor]
        public OrthotropicMaterial(string name, double density, double e1, double e2, double e3, double nu12, double nu13, double nu23, double g12, double g13, double g23)
        {
            Name = name;
            Density = density;
            E1 = e1;
            E2 = e2;
            E3 = e3;
            Nu12 = nu12;
            Nu13 = nu13;
            Nu23 = nu23;
            G12 = g12;
            G13 = g13;
            G23 = g23;

            Nu21 = E2 / E1 * Nu12;
            Nu31 = E3 / E1 * Nu13;
            Nu32 = E3 / E2 * Nu23;
        }

        // When initializing material similar to an Isotropic material
        public OrthotropicMaterial(string name, double elasticModulus, double poissonRatio, double shearModulus, double density)
        {
            Name = name;
            E1 = elasticModulus;
            E2 = elasticModulus;
            E3 = elasticModulus;
            G12 = shearModulus;
            G13 = shearModulus;
            G23 = shearModulus;
            Nu12 = poissonRatio;
            Nu13 = poissonRatio;
            Nu21 = poissonRatio;
            Nu23 = poissonRatio;
            Nu31 = poissonRatio;
            Nu32 = poissonRatio;
            Density = density;
        }

        #endregion

        public double[,] ComplianceMatrix
        {
            //Uses engineering strain
            get
            {
                double S11 = 1.0 / E1;
                double S12 = -Nu21 / E2;
                double S13 = -Nu31 / E3;
                double S21 = -Nu12 / E1;
                double S22 = 1.0 / E2;
                double S23 = -Nu32 / E3;
                double S31 = -Nu13 / E1;
                double S32 = -Nu23 / E2;
                double S33 = 1.0 / E3;
                double S44 = 1.0 / G23;
                double S55 = 1.0 / G13;
                double S66 = 1.0 / G12;

                return new double[,]
                    {
                        { S11, S12, S13, 0, 0, 0 },
                        {S12,S22,S23,0,0,0 },
                        {S13,S23,S33,0,0,0 },
                        {0,0,0,S44,0,0 },
                        {0,0,0,0,S55,0 },
                        {0,0,0,0,0,S66 }
                    };
            }
        }

        public double[,] StiffnessMatrix
        {
            get
            {
                Matrix<double> compliance = Matrix<double>.Build.DenseOfArray(ComplianceMatrix);

                Matrix<double> stiffness = compliance.Inverse();

                return stiffness.ToArray();
            }
        }

        public double[,] PlaneStressStiffnessMatrix3D()
        {
            //Uses engineering strain
            //Returns the 3D (6x6) plane stress stiffness matrix
            double[,] dummyStress = new double[,]
            {
                {1 },
                {1 },
                {0 },
                {0 },
                {0 },
                {1 }
            };

            List<int> DoFtoCondense = new List<int>() { 2, 3, 4 };

            StaticCondensationReturn condensation = StaticCondensation(StiffnessMatrix, dummyStress, DoFtoCondense, true);

            return condensation.Kc;
        }

        public override double[,] PlaneStressConstitutiveMatrix2D(double f11 = 1, double f22 = 1, double f12 = 1)
        {
            //Uses engineering strain
            //Returns the 2D (3x3) plane stress stiffness matrix
            double[,] dummyStress = new double[,]
            {
                {1 }, //sigma 11
                {1 }, //sigma 22
                {0 }, //sigma 33
                {0 }, //sigma 23
                {0 }, //sigma 13
                {1 } //sigma 12
            };

            List<int> DoFtoCondense = new List<int>() { 2, 3, 4 };

            StaticCondensationReturn condensation = StaticCondensation(StiffnessMatrix, dummyStress, DoFtoCondense, false);

            double[,] D = condensation.Kc;
            D[0, 0] = D[0, 0] * f11;
            D[0, 1] = D[0, 1] * Min(f11, f22);
            D[1, 1] = D[1, 1] * f22;
            D[1, 0] = D[1, 0] * Min(f11, f22);
            D[2, 2] = D[2, 2] * f12;
            return D;
        }

        public override double MonotonicStrainStress(double stress)
        {
            return stress / E;
        }

        public override double MonotonicStressStrain(double strain)
        {
            return E * strain;
        }
    }
}
