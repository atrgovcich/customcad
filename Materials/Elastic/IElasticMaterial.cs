﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Materials.Elastic
{
    public interface IElasticMaterial
    {
        string Name { get; set; }
        double Density { get; set; }
        double E1 { get; set; }
        double E2 { get; set; }
        double E3 { get; set; }
        double Nu12 { get; set; }
        double Nu21 { get; set; }
        double Nu13 { get; set; }
        double Nu31 { get; set; }
        double Nu23 { get; set; }
        double Nu32 { get; set; }
        double G12 { get; set; }
        double G13 { get; set; }
        double G23 { get; set; }

        double Nu { get; set; } //Average of the Nu12 and Nu21 values
        double E { get; set; } //Should return E11
        double G { get; set; } //Should return G12

        double SpecificGravity { get; }


        double[,] PlaneStressConstitutiveMatrix2D(double f11 = 1, double f22 = 1, double f12 = 1);
    }
}
