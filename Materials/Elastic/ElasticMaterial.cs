﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Materials.General;

namespace Materials.Elastic
{
    public abstract class ElasticMaterial : IElasticMaterial, IGenericMaterial
    {
        public string Name { get; set; }
        public double Density { get; set; }
        public double E1 { get; set; }
        public double E2 { get; set; }
        public double E3 { get; set; }
        public double Nu12 { get; set; }
        public double Nu21 { get; set; }
        public double Nu13 { get; set; }
        public double Nu31 { get; set; }
        public double Nu23 { get; set; }
        public double Nu32 { get; set; }
        public double G12 { get; set; }
        public double G13 { get; set; }
        public double G23 { get; set; }

        public double E
        {
            get
            {
                return E1;
            }
            set
            {
                E1 = value;
                E2 = value;
                E3 = value;
            }
        }

        public double ElasticModulus
        {
            get
            {
                return E;
            }
        }

        public double G
        {
            get
            {
                return G12;
            }
            set
            {
                G12 = value;
                G13 = value;
                G23 = value;
            }
        }

        public double Nu
        {
            get
            {
                return Nu12;
            }
            set
            {
                Nu12 = value;
                Nu13 = value;
                Nu21 = value;
                Nu23 = value;
                Nu31 = value;
                Nu32 = value;
            }
        }

        public double SpecificGravity
        {
            get
            {
                return Density * Constants.SpecificGravityPerDensity;
            }
        }

        public abstract double[,] PlaneStressConstitutiveMatrix2D(double f11 = 1, double f22 = 1, double f12 = 1);

        public abstract double MonotonicStressStrain(double strain);

        public abstract double MonotonicStrainStress(double stress);

        public double MaxCompressionStrain()
        {
            throw new NotFiniteNumberException();
        }

        public double MaxTensionStrain()
        {
            throw new NotFiniteNumberException();
        }
    }
}
