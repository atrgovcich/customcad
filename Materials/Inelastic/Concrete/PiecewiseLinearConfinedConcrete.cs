﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Concrete
{
    public class PiecewiseLinearConfinedConcrete : ConfinedConcreteMaterial
    {
        #region Public Properties
        /// <summary>
        /// Confined core compressive strength (ksi)
        /// </summary>
        public double Fcc { get; set; }
        /// <summary>
        /// Tension strength (ksi)
        /// </summary>
        public double Ft { get; set; }
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double ecu { get; set; }
        #endregion

        #region Public Implemented Properties
        public override double CompressiveStrength
        {
            get
            {
                return Fc;
            }
        }

        public override double TensileStrength
        {
            get
            {
                double m_Ec = Ec * 1000.0;

                double m_fc = Fc * 1000.0;

                double fcr = -4.0 * Sqrt(m_fc);

                double ecr = fcr / m_Ec;

                return -1.0 * m_Ec * ecr / 1000.0;  //Return positive tensile strength
            }
        }

        public override double ElasticModulus
        {
            get
            {
                return Ec;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public PiecewiseLinearConfinedConcrete(string name) : base(name)
        {

        }

        public PiecewiseLinearConfinedConcrete(string name, double compressiveStrength, 
            double coreStrength, double tensionStrength, double crushingStrain) : this(name)
        {
            Fc = compressiveStrength;
            Fcc = coreStrength;
            Ft = tensionStrength;
            ecu = crushingStrain;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return ecu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            double m_fc = Fc * 1000.0;

            double m_Ec = Ec * 1000.0;

            double fcr = -4.0 * Sqrt(m_fc);

            double ecr = fcr / m_Ec;

            double et0 = 5.0 * ecr;

            return Abs(et0);
        }

        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            double m_fcc = Fcc * 1000.0; //convert to psi

            double m_fc = Fc * 1000.0;

            double m_Ec = Ec * 1000.0;

            double n = 0.8 + m_fc / 2500.0;

            double eci = m_fc / m_Ec * (n / (n - 1.0));

            double ecc = eci * (1.0 + 5.0 * (m_fcc / m_fc - 1.0));

            double R = 1.0 / (1.0 + (1.0 / m_Ec) * (m_fcc / -ecc));

            double ecn = (2.0 / 3.0) * ecu;

            double fcn = -((-R * (-ecn / -ecc) / (R - 1.0 + Pow(-ecn / -ecc, R))) * m_fcc);

            double beta = 1.0 + 0.3 * (1.0 - m_fcc / m_fc);

            double fck = 1.03 * m_fcc;

            double eck = beta * ecc;

            double fce = (2.0 / 3.0) * fck;

            double ece = fce / m_Ec;

            double Ecs = (fcn - fck) / (ecn - eck);

            double fcr = -4.0 * Sqrt(m_fc);

            double ecr = fcr / m_Ec;

            double et0 = 5.0 * ecr;

            double Ets = fcr / (ecr - et0);

            double b1 = -Ets * et0;

            //Tension side
            if (strain < 0 && strain >= ecr)
            {
                if (strain >= ecr)
                {
                    stress = m_Ec * strain;
                }
                else if (strain < ecr && strain >= et0)
                {
                    stress = Ets * strain + b1;
                }
                else
                {
                    stress = 0;
                }
            }

            //Compression side
            if (strain > 0)
            {
                if (strain <= ece)
                {
                    stress = m_Ec * strain;
                }
                else if (strain > ece && strain <= eck)
                {
                    double m = (fck - fce) / (eck - ece);

                    double b2 = fce - m * ece;

                    stress = m * strain + b2;
                }
                else if (strain > eck && strain <= ecu)
                {
                    double b3 = fck - Ecs * eck;

                    stress = Ecs * strain + b3;
                }
                else
                {
                    stress = 0;
                }
            }

            return stress / 1000.0;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
