﻿
namespace Materials.Inelastic.Concrete
{
    public enum ElasticModulusEquationType
    {
        NormalStrength,
        HighStrength
    }
}
