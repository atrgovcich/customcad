﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Concrete
{
    /// <summary>
    /// A modified version of the Mander confined concrete model that ensures conformity
    /// with the unconfined model at low strains and low confining stress
    /// </summary>
    public class RestrepoConfinedConcrete : ConfinedConcreteMaterial
    {
        #region Public Properties
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double ecu { get; set; }
        /// <summary>
        /// Confined core compressive strength (ksi)
        /// </summary>
        public double Fcc { get; set; }
        #endregion

        #region Public Implemented Properties
        public override double CompressiveStrength
        {
            get
            {
                return Fc;
            }
        }

        public override double TensileStrength
        {
            get
            {
                double CC_ElasticMod = Ec * 1000.0;

                double CC_CompressionStrength = Fc * 1000.0;

                double fcr = 4.0 * Sqrt(CC_CompressionStrength);

                double ecr = fcr / CC_ElasticMod;

                
                return CC_ElasticMod * ecr / 1000.0; //return ksi
            }
        }

        public override double ElasticModulus
        {
            get
            {
                return Ec;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public RestrepoConfinedConcrete(string name) : base(name)
        {

        }

        public RestrepoConfinedConcrete(string name, double comporessiveStrength, double coreStrength,
            double crushingStrain) : this(name)
        {
            Fc = comporessiveStrength;
            Fcc = coreStrength;
            ecu = crushingStrain;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return ecu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            double CC_ElasticMod = Ec * 1000.0;

            double CC_CompressionStrength = Fc * 1000.0;

            double fcr = 4.0 * Sqrt(CC_CompressionStrength);

            double ecr = fcr / CC_ElasticMod;

            return Abs(3 * ecr);
        }

        /// <summary>
        /// Returns the stress at a given strain
        /// </summary>
        /// <param name="strain">Strain in material (compression positive)</param>
        /// <returns></returns>
        public override double MonotonicStressStrain(double strain)
        {
            strain = -1.0 * strain; //This model assumes tension is positive and compression negative

            double stress = 0;

            double CC_CoreStrength = Fcc * 1000.0;

            double CC_CompressionStrength = Fc * 1000.0;

            double CC_ElasticMod = Ec * 1000.0;

            double CC_CrushingStrain = -ecu;

            double n = 0.8 + CC_CompressionStrength / 2500.0;

            double eci = -CC_CompressionStrength / CC_ElasticMod * (n / (n - 1.0));

            double fcr = 4.0 * Sqrt(CC_CompressionStrength);

            double ecr = fcr / CC_ElasticMod;

            double strAtPeak = eci * (1.0 + 5.0 * (CC_CoreStrength / CC_CompressionStrength - 1.0));

            double R = 1.0 / (1.0 + (1.0 / CC_ElasticMod) * (CC_CoreStrength / strAtPeak));

            if (strain > 0)
            {
                if (strain < ecr)
                {
                    stress = CC_ElasticMod * strain;
                }
                else if (strain >= ecr)
                {
                    stress = 0.7 * fcr / (1.0 + Sqrt(500.0 * strain));
                }
            }
            else
            {
                if (strain >= CC_CrushingStrain)
                {
                    stress = (-R * (strain / strAtPeak) / (R - 1.0 + Pow(strain / strAtPeak, R))) * CC_CoreStrength;
                }
                else
                {
                    stress = 0;
                }
            }

            return -stress / 1000.0;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
