﻿using static System.Math;
using Materials.General;

namespace Materials.Inelastic.Concrete
{
    public abstract class InelasticConcreteMaterial : IInelasticMaterial
    {
        #region Public Implemented Properties
        public string Name { get; set; }
        /// <summary>
        /// Density, in kip/in^3
        /// </summary>
        public double Density
        {
            get
            {
                return _density;
            }
            set
            {
                _density = value;

                ComputeElasticModulus();
            }
        }
        public bool AutoChooseElasticModulusEquation { get; set; } = true;
        #endregion

        #region Backing Fields
        protected double _fc;
        protected ElasticModulusEquationType _elasticModulusEquation = ElasticModulusEquationType.NormalStrength;
        protected double _density = 150.0 * Constants.PoundPerCubicFootToKipPerCubicInch; //kip/in^3
        #endregion

        #region Public Properties
        /// <summary>
        /// Concrete compressive strength (ksi)
        /// </summary>
        public double Fc
        {
            get
            {
                return _fc;
            }
            set
            {
                _fc = value;

                if (AutoChooseElasticModulusEquation == true)
                {
                    if (_fc > 6.0)
                    {
                        _elasticModulusEquation = ElasticModulusEquationType.HighStrength;
                    }
                    else
                    {
                        _elasticModulusEquation = ElasticModulusEquationType.NormalStrength;
                    }
                }

                Ec = ComputeElasticModulus();
            }
        }
        /// <summary>
        /// Concrete elastic modulus (ksi)
        /// </summary>
        public double Ec { get; set; }
        public bool Confined { get; protected set; } = false;

        public ElasticModulusEquationType ElasticModulusEquation
        {
            get
            {
                return _elasticModulusEquation;
            }
            set
            {
                _elasticModulusEquation = value;

                Ec = ComputeElasticModulus();
            }
        }
        #endregion

        #region Public Abstract Properties
        public abstract double CompressiveStrength { get; }
        public abstract double TensileStrength { get; }
        public abstract double ElasticModulus { get; }

        #endregion

        #region Constructors
        protected InelasticConcreteMaterial(string name)
        {
            Name = name;
        }
        #endregion

        #region Public Implemented Methods
        public abstract double MonotonicStressStrain(double strain);

        public abstract double MonotonicStrainStress(double stress);
        #endregion

        #region Public Methods
        public double ComputeElasticModulus()
        {
            double modulus = 0;

            if (ElasticModulusEquation.Equals(ElasticModulusEquationType.NormalStrength) == true)
            {
                modulus = 33.0 * Pow(Density * Constants.KipPerCubicInchToPoundPerCubicFoot, 1.5) * Sqrt(Fc * Constants.KsiToPsi);
            }
            else if (ElasticModulusEquation.Equals(ElasticModulusEquationType.HighStrength) == true)
            {
                modulus = 40000.0 * Sqrt(Fc * Constants.KsiToPsi) + 1000000.0;
            }

            return modulus * Constants.PsiToKsi;
        }

        public double ComputeElasticModulus(double fc)
        {
            double modulus = 0;

            if (fc <= 6)
            {
                modulus = 33.0 * Pow(Density * Constants.KipPerCubicInchToPoundPerCubicFoot, 1.5) * Sqrt(fc * Constants.KsiToPsi);
            }
            else if (fc > 6)
            {
                modulus = 40000.0 * Sqrt(fc * Constants.KsiToPsi) + 1000000.0;
            }

            return modulus * Constants.PsiToKsi;
        }

        public double ComputeElasticModulus(double fc, ElasticModulusEquationType eqnType)
        {
            double modulus = 0;

            if (eqnType.Equals(ElasticModulusEquationType.NormalStrength) == true)
            {
                modulus = 33.0 * Pow(Density * Constants.KipPerCubicInchToPoundPerCubicFoot, 1.5) * Sqrt(fc * Constants.KsiToPsi);
            }
            else if (eqnType.Equals(ElasticModulusEquationType.HighStrength) == true)
            {
                modulus = 40000.0 * Sqrt(fc * Constants.KsiToPsi) + 1000000.0;
            }

            return modulus * Constants.PsiToKsi;
        }
        #endregion

        #region Public Abstract Methods
        public abstract double MaxCompressionStrain();

        public abstract double MaxTensionStrain();
        #endregion
    }
}
