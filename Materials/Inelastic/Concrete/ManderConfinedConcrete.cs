﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Concrete
{
    public class ManderConfinedConcrete : ConfinedConcreteMaterial
    {
        #region Public Properties
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double ecu { get; set; }
        /// <summary>
        /// Tension strength (ksi)
        /// </summary>
        public double Ft { get; set; }
        /// <summary>
        /// Tension strain
        /// </summary>
        public double et { get; set; }
        /// <summary>
        /// Confined core compressive strength (ksi)
        /// </summary>
        public double Fcc { get; set; }
        #endregion

        #region Public Implemented Properties
        public override double CompressiveStrength
        {
            get
            {
                return Fc;
            }
        }

        public override double TensileStrength
        {
            get
            {
                return Ft;
            }
        }

        public override double ElasticModulus
        {
            get
            {
                return Ec;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public ManderConfinedConcrete(string name) : base(name)
        {
            Fc = 4;
            Fcc = 6;
            ecu = 0.02;
            TieSpacing = 4;
            
        }

        public ManderConfinedConcrete(string name, double compressiveStrength, double coreCompressiveStrength, double tensionStrength,
            double crushingStrain, double tensionStrain, double tieSpacing, ConfinementType confiningType) : this(name)
        {
            Fc = compressiveStrength;
            Fcc = coreCompressiveStrength;
            Ft = tensionStrength;
            ecu = crushingStrain;
            et = tensionStrain;
            TieSpacing = tieSpacing;
            ConfiningType = confiningType;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return ecu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            if (Ft < 0)
            {
                return Abs(2.0 * et);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the stress at a given strain.  Returns stress in ksi.
        /// </summary>
        /// <param name="strain">Strain in the material</param>
        /// <returns></returns>
        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            double strAtPeak = 0.002 * (1.0 + 5.0 * (Fcc / Fc - 1.0));

            double Esec = Fcc / strAtPeak;

            double X = strain / strAtPeak;

            double R = Ec / (Ec - Esec);

            if (strain < 2.0 * et)
            {
                stress = 0;
            }
            else if (strain < et)
            {
                if (Ft < 0)
                {
                    stress = Ft + Ec * (et - strain);
                }
                else
                {
                    stress = 0;
                }
            }
            else if (strain < 0)
            {
                if (Ft < 0)
                {
                    stress = strain * Ec;
                }
                else
                {
                    stress = 0;
                }
            }
            else if (strain <= ecu)
            {
                stress = (Fcc * X * R) / (R - 1.0 + Pow(X, R));
            }
            else
            {
                stress = 0;
            }

            return stress;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
