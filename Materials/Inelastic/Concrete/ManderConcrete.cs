﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Concrete
{
    public class ManderConcrete : InelasticConcreteMaterial
    {
        #region Public Properties
        /// <summary>
        /// Post crushing strength (ksi)
        /// </summary>
        public double Fpc { get; set; }
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double ecu { get; set; }
        /// <summary>
        /// Strain at peak stress
        /// </summary>
        public double eps { get; set; }
        /// <summary>
        /// Spalling strain
        /// </summary>
        public double esp { get; set; }
        /// <summary>
        /// Tension strength (ksi)
        /// </summary>
        public double Ft { get; set; }
        /// <summary>
        /// Tension strain
        /// </summary>
        public double et { get; set; }
        #endregion

        #region Public Implemented Properties
        public override double CompressiveStrength
        {
            get
            {
                return Fc;
            }
        }

        public override double TensileStrength
        {
            get
            {
                return Ft;
            }
        }

        public override double ElasticModulus
        {
            get
            {
                return Ec;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public ManderConcrete(string name) : base(name)
        {
            Fc = 4;
            esp = 0.006;
            ecu = 0.004;
            eps = 0.002;
        }

        public ManderConcrete(string name, double compressiveStrength, double postCrushingStrength,
            double tensionStrength, double strainAtPeakStress, double crushingStrain, double spallingStrain,
            double tensionStrain) : this(name)
        {
            Fc = compressiveStrength;
            Fpc = postCrushingStrength;
            Ft = tensionStrength;
            eps = strainAtPeakStress;
            ecu = crushingStrain;
            esp = spallingStrain;
            et = tensionStrain;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return ecu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            if (Ft < 0)
            {
                return Abs(2.0 * et);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the stress at a given strain
        /// </summary>
        /// <param name="strain">Strain in the material</param>
        /// <returns></returns>
        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            double Esec = Fc / eps;

            double X = strain / eps;

            double R = Ec / (Ec - Esec);

            double failStress = Fc * (ecu / eps) * R / (R - 1.0 + Pow(ecu / eps, R));

            if (strain < 2.0 * et)
            {
                stress = 0;
            }
            else if (strain < et)
            {
                if (Ft < 0)
                {
                    stress = Ft + Ec * (et - strain);
                }
                else
                {
                    stress = 0;
                }
            }
            else if (strain < 0)
            {
                if (Ft < 0)
                {
                    stress = Ec * strain;
                }
                else
                {
                    stress = 0;
                }
            }
            else if (strain < ecu)
            {
                stress = (Fc * X * R) / (R - 1.0 + Pow(X, R));
            }
            else if (strain < esp)
            {
                stress = failStress + (Fpc - failStress) * ((strain - ecu) / (esp - ecu));
            }
            else
            {
                stress = 0;
            }

            return stress;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
