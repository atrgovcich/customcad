﻿using System;
using static System.Math;
using Materials.General;
using Newtonsoft.Json;

namespace Materials.Inelastic.Concrete
{
    public class HsuHighStrengthConcrete : InelasticConcreteMaterial
    {
        #region Public Properties
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double ecu { get; set; }
        /// <summary>
        /// Strain at peak stress
        /// </summary>
        public double eps { get; set; }
        /// <summary>
        /// Strain at 0.3 * F'c
        /// </summary>
        public double ec3 { get; set; }
        /// <summary>
        /// Tension strength (ksi)
        /// </summary>
        public double Ft { get; set; }
        /// <summary>
        /// Tension strain
        /// </summary>
        public double et { get; set; }
        #endregion

        #region Public Implemented Properties
        public override double CompressiveStrength
        {
            get
            {
                return Fc;
            }
        }

        public override double TensileStrength
        {
            get
            {
                return Ft;
            }
        }

        public override double ElasticModulus
        {
            get
            {
                return Ec;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public HsuHighStrengthConcrete(string name) : base(name)
        {
            Fc = 10;
            eps = 0.002;
            ec3 = 0.00319;
            ecu = 0.008;
            et = 0.00015;
            Ft = 0.75;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Unique material name</param>
        /// <param name="elasticModulus">Elastic modulus of concrete (ksi)</param>
        /// <param name="compressiveStrength">Compressive strength of concrete (ksi)</param>
        /// <param name="tensionStrength">Tension strength of concrete (ksi)</param>
        /// <param name="strainAtPeakStress">Strain at peak stress</param>
        /// <param name="strainAt3Fc">Strain at 0.3 * F'c</param>
        /// <param name="ultimateStrain">Ultimate concrete compressive strain</param>
        /// <param name="tensionStrain">Ultimate concrete tensile strain</param>
        public HsuHighStrengthConcrete(string name, double compressiveStrength, double tensionStrength, 
            double strainAtPeakStress, double strainAt3Fc, double ultimateStrain, double tensionStrain) : base(name)
        {
            Fc = compressiveStrength;
            Ft = tensionStrength;
            eps = strainAtPeakStress;
            ec3 = strainAt3Fc;
            ecu = ultimateStrain;
            et = tensionStrain;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return ecu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            if (Ft > 0)
            {
                return Abs(2.0 * et);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Returns the stress at a given strain
        /// </summary>
        /// <param name="strain">Strain in the material</param>
        /// <returns></returns>
        public override double MonotonicStressStrain(double strain)
        {
            //This particular model is formulated in MPa.  As such, all values must be 
            //converted into MPa prior to computing

            double stress_mpa = 0;

            double g_conc = Density * Constants.KipPerCubicInchToKilogramPerCubicMeter; //weight of concrete in kg/m^3
            double fc_mpa = Fc * 1000.0 * 0.00689475729; //convert f'c to mpa
            double ft_mpa = Ft * 1000.0 * 0.00689475729; //convert f't to mpa
            double Ec_mpa = Ec * 1000.0 * 0.00689475729; //convert Ec to mpa

            double beta = Pow(fc_mpa / 65.23, 3) + 2.59;
            double Eit = 0.0736 * Pow(g_conc, 1.51) * Pow(fc_mpa, 0.3);
            double e_0 = (1680.0 + 7.1 * fc_mpa) * Pow(10.0, -6);
            int n = 0;

            if (strain <= eps)
            {
                n = 1;
            }
            else
            {
                if (fc_mpa <= 62)
                {
                    n = 1;
                }
                else if (fc_mpa > 62 && fc_mpa <= 76)
                {
                    n = 2;
                }
                else if (fc_mpa > 76 && fc_mpa <= 90)
                {
                    n = 3;
                }
                else if (fc_mpa > 90)
                {
                    n = 5;
                }
            }

            if (strain < 2.0 * et)
            {
                stress_mpa = 0;
            }
            else if (strain <= et)
            {
                if (-ft_mpa < 0)
                {
                    stress_mpa = ft_mpa + Ec_mpa * (et - strain);
                }
                else
                {
                    stress_mpa = 0;
                }
            }
            else if (strain < 0)
            {
                if (ft_mpa < 0)
                {
                    stress_mpa = Ec_mpa * strain;
                }
                else
                {
                    stress_mpa = 0;
                }
            }
            else if (strain > 0 && strain <= ec3)
            {
                stress_mpa = fc_mpa * ((n * beta * (strain / eps)) / (n * beta - 1 + Pow(strain / eps, n * beta)));
            }
            else if (strain > ec3 && strain <= ecu)
            {
                stress_mpa = 0.3 * fc_mpa * Pow(E, -0.8 * Pow(strain / eps - ec3 / eps, 0.5));
            }
            else
            {
                stress_mpa = 0;
            }

            return stress_mpa * 145.0377 / 1000.0; //convert back into ksi
        }

        /// <summary>
        /// Returns the strain at a given stress.
        /// NOT IMPLEMENTED
        /// </summary>
        /// <param name="stress">Stress in material (ksi)</param>
        /// <returns></returns>
        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
