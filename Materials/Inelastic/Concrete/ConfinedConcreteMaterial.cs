﻿namespace Materials.Inelastic.Concrete
{
    public abstract class ConfinedConcreteMaterial : InelasticConcreteMaterial
    {
        #region Public Properties
        public double TieSpacing { get; set; }
        public ConfinementType ConfiningType { get; set; }
        #endregion

        #region Constructors
        protected ConfinedConcreteMaterial(string name) : base(name)
        {
            Confined = true;
        }
        #endregion
    }
}
