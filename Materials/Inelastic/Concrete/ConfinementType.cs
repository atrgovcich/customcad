﻿namespace Materials.Inelastic.Concrete
{
    public enum ConfinementType
    {
        Spiral,
        ClosedTies
    }
}
