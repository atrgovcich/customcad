﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Steel
{
    public class KentSteel : InelasticSteelMaterial
    {
        #region Public Properties
        public double Fy { get; set; }
        public double Fu { get; set; }
        public double ey { get; set; }
        public double esh { get; set; }
        public double eu { get; set; }
        #endregion

        #region Public Implemented Properties
        [JsonIgnore]
        public override double YieldStress
        {
            get
            {
                return Fy;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public KentSteel(string name) : base(name)
        {

        }

        public KentSteel(string name, double yieldStress, double ultimateStress, double yieldStrain, 
            double strainAtStrainHardening, double ultimateStrain) : this(name)
        {
            Fy = yieldStress;
            Fu = ultimateStress;
            ey = yieldStrain;
            esh = strainAtStrainHardening;
            eu = ultimateStrain;

            Es = Fy / ey;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return eu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            return eu;
        }

        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            int strainMod = 1;

            if (strain < 0)
            {
                strainMod = -1;

                strain = -strain;
            }

            if (strain <= ey)
            {
                stress = Es * strain;
            }
            else if (strain <= esh)
            {
                stress = Fy;
            }
            else if (strain <= eu)
            {
                double q = eu - esh;

                double m = ((Fu / Fy) * Pow(30.0 * q + 1.0, 2) - 60.0 * q - 1.0) / (15.0 * Pow(q, 2));

                stress = Fy * ((m * (strain - esh) + 2.0) / (60.0 * (strain - esh) + 2.0) + ((strain - esh) * (60.0 - m)) / (2.0 * Pow(30.0 * q + 1.0, 2)));
            }
            else
            {
                stress = 0;
            }

            return stress * strainMod;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
