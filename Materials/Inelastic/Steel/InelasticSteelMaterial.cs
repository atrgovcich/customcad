﻿
namespace Materials.Inelastic.Steel
{
    public abstract class InelasticSteelMaterial : IInelasticMaterial
    {
        #region Public Properties
        /// <summary>
        /// Unique name of the material
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Density of the material (kip/in^3)
        /// </summary>
        public double Density { get; set; } = 0.000278; //kip/in^3
        /// <summary>
        /// Elastic modulus of steel
        /// </summary>
        public double Es { get; set; } = 29000.0; //ksi
        /// <summary>
        /// Elastic modulus of the material (ksi)
        /// </summary>
        public double ElasticModulus
        {
            get
            {
                return Es;
            }
        }
        #endregion

        #region Public Abstract Properties
        /// <summary>
        /// Yield stress of the material (ksi)
        /// </summary>
        public abstract double YieldStress { get; }
        #endregion

        #region Construcotrs
        /// <summary>
        /// Creates a new steel material with a monotonic stress/strain relationship
        /// </summary>
        /// <param name="name"></param>
        public InelasticSteelMaterial(string name)
        {
            Name = name;
        }
        #endregion

        #region Public Abstract Methods
        public abstract double MonotonicStressStrain(double strain);

        public abstract double MonotonicStrainStress(double stress);

        public abstract double MaxCompressionStrain();

        public abstract double MaxTensionStrain();
        #endregion

    }
}
