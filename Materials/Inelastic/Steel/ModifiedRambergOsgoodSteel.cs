﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Steel
{
    public class ModifiedRambergOsgoodSteel : InelasticSteelMaterial
    {
        #region Public Properties
        /// <summary>
        /// Yield stress (ksi)
        /// </summary>
        public double Fy { get; set; } = 260;
        /// <summary>
        /// Ultimate stress (ksi)
        /// </summary>
        public double Fu { get; set; } = 276;
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double eu { get; set; } = 0.068;
        public double FP0 { get; set; } = 240;
        public double R { get; set; } = 0.025;
        public double C { get; set; } = 10;
        /// <summary>
        /// Stress, in compression, at which the material goes plastic (ksi)
        /// </summary>
        public double Fpc { get; set; } = 135;
        #endregion

        #region Public Implemented Properties
        [JsonIgnore]
        public override double YieldStress
        {
            get
            {
                return Fy;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public ModifiedRambergOsgoodSteel(string name) : base(name)
        {
            Es = 28130;
        }

        public ModifiedRambergOsgoodSteel(string name, double yieldStress, double ultimateStress, double ultimateStrain,
            double fp0, double r, double c, double fpc) : this(name, 28130, yieldStress, ultimateStress, ultimateStrain, fp0, r, c, fpc)
        {

        }

        public ModifiedRambergOsgoodSteel(string name, double elasticModulus, double yieldStress, double ultimateStress, double ultimateStrain,
            double fp0, double r, double c, double fpc) : base(name)
        {
            Es = elasticModulus;
            Fy = yieldStress;
            Fu = ultimateStress;
            eu = ultimateStrain;
            FP0 = fp0;
            R = r;
            C = c;
            Fpc = fpc;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return eu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            return eu;
        }

        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            strain = -strain; //this model takes tension as positive, whereas the program uses compression as positive

            int strainMod = -1;

            if (strain > 0)
            {
                if (strain <= eu)
                {
                    stress = Min(Es * strain * (R + (1.0 - R) / Pow(1.0 + Pow((1.0 - R) * Es * strain / FP0, C), 1.0 / C)), Fu);
                }
                else
                {
                    stress = 0;
                }
            }           
            else if (strain < 0)
            {
                //Compression
                //Convert to positive value

                strain = -strain;

                strainMod = -strainMod;

                if (strain > eu)
                {
                    stress = 0;
                }
                else
                {
                    stress = Min(Es * strain * (R + (1.0 - R) / Pow(1.0 + Pow((1.0 - R) * Es * strain / FP0, C), 1.0 / C)), Fu);

                    stress = Min(stress, Fpc);
                }
            }

            return strainMod * stress;
        }

        public override double MonotonicStrainStress(double stress)
        {
            double upper = Max(0, Sign(stress) * eu);

            double lower = Min(0, Sign(stress) * eu);

            double strain = (upper + lower) / 2.0;

            int counter = 0;

            int maxCounter = 20;

            double retStress = MonotonicStressStrain(strain);

            while (counter <= maxCounter && Abs(retStress - stress) > 0.1)
            {
                counter++;

                if (retStress > stress)
                {
                    upper = strain;
                }
                else
                {
                    lower = strain;
                }

                strain = (lower + upper) / 2.0;

                retStress = MonotonicStressStrain(strain);

                if (counter == maxCounter && Abs(retStress - stress) > 0.1)
                {
                    throw new Exception("Could not compute stress given strain for Modified Ramberg Osgood steel model.");
                }
            }

            return strain;
        }
        #endregion
    }
}
