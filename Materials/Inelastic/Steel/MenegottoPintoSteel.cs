﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Steel
{
    public class MenegottoPintoSteel : InelasticSteelMaterial
    {
        #region Public Properties
        /// <summary>
        /// Yield stress (ksi)
        /// </summary>
        public double Fy { get; set; }
        /// <summary>
        /// Ultimate stress (ksi)
        /// </summary>
        public double Fu { get; set; }
        /// <summary>
        /// Yield strain
        /// </summary>
        public double ey { get; set; }
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double eu { get; set; }
        /// <summary>
        /// Material hardening ratio
        /// </summary>
        public double b { get; set; }
        /// <summary>
        /// Transition constant
        /// </summary>
        public double R { get; set; }
        /// <summary>
        /// Constant A1
        /// </summary>
        public double A1 { get; set; }
        /// <summary>
        /// Constant A2
        /// </summary>
        public double A2 { get; set; }
        #endregion

        #region Public Implemented Properties
        [JsonIgnore]
        public override double YieldStress
        {
            get
            {
                return Fy;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public MenegottoPintoSteel(string name) : base(name)
        {
            Fy = 60;
            Fu = 90;
            ey = 0.00207;
            eu = 0.09;
            b = 0.003;
            R = 2;
            A1 = 18.5;
            A2 = 0.15;

            Es = Fy / ey;
        }

        public MenegottoPintoSteel(string name, double yieldStress, double ultimateStress, double yieldStrain, 
            double ultimateStrain, double hardeningRatio, double transitionConstant, double a1, double a2) : base(name)
        {
            Fy = yieldStress;
            Fu = ultimateStress;
            ey = yieldStrain;
            eu = ultimateStrain;
            b = hardeningRatio;
            R = transitionConstant;
            A1 = a1;
            A2 = a2;

            Es = Fy / ey;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return eu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            return eu;
        }

        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            int strainMod = 1;

            if (strain < 0)
            {
                strainMod = -1;

                strain = -strain;
            }

            double estar = strain / ey;

            if(strain <= eu)
            {
                stress = Min((b * estar + ((1.0 - b) * estar) / Pow(1.0 + Pow(estar, R), 1.0 / R)) * Fy, Fu);
            }
            else
            {
                stress = 0;
            }

            return stress * strainMod;
        }

        /// <summary>
        /// Returns the strain given a stress.  Strain is positive (+) for compression, negative (-) for tension.
        /// Note that the stress must be less than the ultimate stress, otherwise an exception will be thrown.
        /// </summary>
        /// <param name="stress">Stress (ksi)</param>
        /// <returns>The strain, as a double, given the stress input</returns>
        public override double MonotonicStrainStress(double stress)
        {
            double upper = Max(0, Sign(stress) * eu);

            double lower = Min(0, Sign(stress) * eu);

            double strain = (upper + lower) / 2.0;

            int counter = 0;

            int maxCounter = 20;

            double retStress = MonotonicStressStrain(strain);

            while (counter <= maxCounter && Abs(retStress - stress) > 0.1)
            {
                counter++;

                if (retStress > stress)
                {
                    upper = strain;
                }
                else
                {
                    lower = strain;
                }

                strain = (lower + upper) / 2.0;

                retStress = MonotonicStressStrain(strain);

                if (counter == maxCounter && Abs(retStress - stress) > 0.1)
                {
                    throw new Exception("Could not compute stress given strain for Menegotto Pinto steel model.");
                }
            }

            return strain;
        }
        #endregion
    }
}
