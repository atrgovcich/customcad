﻿using System;
using static System.Math;
using Newtonsoft.Json;

namespace Materials.Inelastic.Steel
{
    public class ManderSteel : InelasticSteelMaterial
    {
        #region Public Properties
        /// <summary>
        /// Yield stress (ksi)
        /// </summary>
        public double Fy { get; set; }
        /// <summary>
        /// Yield strain
        /// </summary>
        public double ey { get; set; }
        /// <summary>
        /// Strain at the onset of strain hardening
        /// </summary>
        public double esh { get; set; }
        /// <summary>
        /// Ultimate stress
        /// </summary>
        public double Fu { get; set; }
        /// <summary>
        /// Ultimate strain
        /// </summary>
        public double eu { get; set; }
        /// <summary>
        /// Exponent
        /// </summary>
        public double P { get; set; }
        #endregion

        #region Public Implemented Properties
        [JsonIgnore]
        public override double YieldStress
        {
            get
            {
                return Fy;
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public ManderSteel(string name) : base(name)
        {
            Fy = 60;
            Fu = 90;
            ey = 0.00207;
            eu = 0.09;
            esh = 0.008;
            P = 3;
        }

        public ManderSteel(string name, double yieldStress, double ultimateStress, double yieldStrain, double strainAtStrainHardening,
            double ultimateStrain, double p) : this(name)
        {
            Fy = yieldStress;
            Fu = ultimateStress;
            ey = yieldStrain;
            eu = ultimateStrain;
            esh = strainAtStrainHardening;
            P = p;

            Es = Fy / ey;
        }
        #endregion

        #region Public Implemented Methods
        /// <summary>
        /// Returns the maximum compression strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxCompressionStrain()
        {
            return eu;
        }

        /// <summary>
        /// Returns hte maximum tension strain (as a positive value)
        /// </summary>
        /// <returns></returns>
        public override double MaxTensionStrain()
        {
            return eu;
        }

        public override double MonotonicStressStrain(double strain)
        {
            double stress = 0;

            int strainMod = 1;

            if (strain < 0)
            {
                strainMod = -1;

                strain = -strain;
            }

            if (strain < ey)
            {
                stress = Es * strain;
            }
            else if (strain < esh)
            {
                stress = Fy;
            }
            else if (strain <= eu)
            {
                stress = Fu - (Fu - Fy) * Pow((eu - strain) / (eu - esh), P);
            }
            else
            {
                stress = 0;
            }

            return stress * strainMod;
        }

        public override double MonotonicStrainStress(double stress)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
