﻿namespace PolyBoolCS
{
    public class PolyBoolVertex
    {
        public double X { get; set; }
        public double Y { get; set; }

        public PolyBoolVertex(double x, double y)
        {
            X = x;
            Y = y;
        }

        public PolyBoolVertex() : this(0, 0)
        {

        }
    }
}
