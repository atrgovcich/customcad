﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace PolyBoolCS
{
    public static class PolyBoolHelper
    {
        public static Polygon ConvertToPolygon(List<Point> vertices)
        {
            List<PointList> region = new List<PointList>();

            PointList list = new PointList(vertices.Count);


            foreach (Point v in vertices)
            {
                if (vertices.IndexOf(v) == vertices.Count - 1)
                {
                    if (vertices[0].x != v.x || vertices[0].y != v.y)
                    {
                        list.Add(new Point(v.x, v.y));
                    }
                }
                else
                {
                    list.Add(new Point(v.x, v.y));
                }
            }

            region.Add(list);

            Polygon poly = new Polygon()
            {
                inverted = false,
                regions = region
            };

            return poly;
        }

        public static List<Point> ConvertRegionToPointList(PointList region)
        {
            List<Point> list = new List<Point>(region.Count + 1);

            foreach (Point p in region)
            {
                list.Add(p);
            }

            //list.Add(region[0]); //Add the first point as the last point, closing the shape

            return list;
        }
    }
}