﻿using Utilities.MathFunctions;

namespace Units.Conversion
{
    public static class Utilities
    {
        public static int DEFAULT_SIGNIFICANT_DIGITS = 3;

        public static string ToLaTexString(double value, Unit unit)
        {
            return $@"{value.RoundToSignificantDigits(DEFAULT_SIGNIFICANT_DIGITS)} \text{{ }} {unit.Description}";
        }
        
        public static string ToUnitlessLaTexString(double value)
        {
            return $@"{value.RoundToSignificantDigits(DEFAULT_SIGNIFICANT_DIGITS)}";
        }
    }
}