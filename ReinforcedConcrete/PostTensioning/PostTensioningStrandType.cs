﻿using System.ComponentModel;

namespace ReinforcedConcrete.PostTensioning
{
    public enum PostTensioningStrandType
    {
        [Description("Three Wire Strand")]
        ThreeWireStrand,
        [Description("Seven Wire Strand")]
        SevenWireStrand,
        [Description("19 Strand Bundle")]
        NineteenStrandBundle
    }
}
