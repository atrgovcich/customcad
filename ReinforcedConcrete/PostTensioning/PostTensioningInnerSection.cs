﻿using System.Collections.Generic;
using Utilities.Geometry;

namespace ReinforcedConcrete.PostTensioning
{
    public abstract class PostTensioningInnerSection
    {
        public string Name { get; set; }
        public double OuterDiameter { get; set; }
        public PostTensioningStrandType StrandType { get; set; }

        protected int _numberOfWires;

        #region Constructors
        public PostTensioningInnerSection(double outsideDiameter)
        {
            OuterDiameter = outsideDiameter;
        }
        #endregion

        #region Public Abstract Methods
        public abstract List<GenericCircle> GetWireCircles(PointD c);
        #endregion
    }
}
