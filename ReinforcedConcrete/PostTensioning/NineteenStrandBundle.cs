﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using Utilities.Geometry;
using static Utilities.Geometry.GeometryFunctions;

namespace ReinforcedConcrete.PostTensioning
{
    public class NineteenStrandBundle : MultiStrandBundle
    {
        #region Public Properties

        #endregion

        #region Constructors
        public NineteenStrandBundle(double outsideDiameter, double internalStrandDiameter, PostTensioningStrandType internalStrandType) : base(outsideDiameter)
        {
            Name = "19 Strand Bundle";
            _numberOfWires = 19;
            StrandType = PostTensioningStrandType.NineteenStrandBundle;
            TypeOfInternalStrand = internalStrandType;
            InternalStrandDiameter = internalStrandDiameter;
        }
        #endregion

        #region Public Overridden Methods
        public override List<GenericCircle> GetWireCircles(PointD c)
        {
            List<GenericCircle> circleList = new List<GenericCircle>();

            double maxStrandDiam = OuterDiameter / 5.0;

            double usedStrandDiam = Min(InternalStrandDiameter, maxStrandDiam);

            int numberOfInnerCircleStrands = 6;

            int numberOfOuterCircleStrands = 12;

            double angleBetweenInnerStrands = 360.0 / numberOfInnerCircleStrands;

            double angleBetweenOuterStrands = 360.0 / numberOfOuterCircleStrands;

            //---------------------------------------------------------------------------------------------------
            //Define the central strand and add it's wire circles
            //---------------------------------------------------------------------------------------------------
            GenericCircle c0 = new GenericCircle(c, usedStrandDiam / 2.0);

            circleList.Add(c0);

            PostTensioningInnerSection strand = CreateInternalStrand(usedStrandDiam, TypeOfInternalStrand);

            circleList.AddRange(strand.GetWireCircles(c0.Center));

            //---------------------------------------------------------------------------------------------------
            //Now loop through and add the inner circle strands and their wires
            //---------------------------------------------------------------------------------------------------
            for (double i = 0; i <= 359.0; i += angleBetweenInnerStrands)
            {
                double angleRad = i * 2.0 * PI / 360.0;

                PointD centroid = new PointD(c.X + maxStrandDiam * Cos(angleRad), c.Y + maxStrandDiam * Sin(angleRad));

                GenericCircle cNext = new GenericCircle(centroid, usedStrandDiam / 2.0);

                circleList.Add(cNext);

                circleList.AddRange(strand.GetWireCircles(cNext.Center));
            }

            //---------------------------------------------------------------------------------------------------
            //Now loop through and add the outer circle strands and their wires
            //---------------------------------------------------------------------------------------------------
            for (double i = angleBetweenOuterStrands / 2.0; i <= 359.0; i += angleBetweenOuterStrands)
            {
                double angleRad = i * 2.0 * PI / 360.0;

                PointD centroid = new PointD(c.X + 2 * maxStrandDiam * Cos(angleRad), c.Y + 2 * maxStrandDiam * Sin(angleRad));

                GenericCircle cNext = new GenericCircle(centroid, usedStrandDiam / 2.0);

                circleList.Add(cNext);

                circleList.AddRange(strand.GetWireCircles(cNext.Center));
            }

            return circleList;
        }

        #endregion

        #region Private Methods
        protected PostTensioningInnerSection CreateInternalStrand(double diam, PostTensioningStrandType internalStrandType)
        {
            if (internalStrandType.Equals(PostTensioningStrandType.SevenWireStrand) == true)
            {
                return new SevenWireStrand(diam);
            }
            else if (internalStrandType.Equals(PostTensioningStrandType.ThreeWireStrand) == true)
            {
                return new ThreeWireStrand(diam);
            }
            else
            {
                //default
                return new SevenWireStrand(diam);
            }
        }
        #endregion

    }
}
