﻿using Materials;
using static System.Math;
using Newtonsoft.Json;

namespace ReinforcedConcrete.PostTensioning
{
    public class PostTensioningDefinition
    {
        #region Public Properties
        public double OuterDiameter { get; set; }
        public double SteelArea { get; set; }
        public IGenericMaterial Material { get; set; }
        public PostTensioningStrandType InnerStrandType { get; set; } = PostTensioningStrandType.SevenWireStrand;
        public double InnerStrandDiameter { get; set; } = 0.5;
        public string Name { get; set; }
        #endregion

        #region Private Fields
        [JsonProperty]
        private PostTensioningInnerSection _ptSection;
        [JsonProperty]
        private PostTensioningStrandType _strandType;
        #endregion

        #region Public Getters and Setters
        [JsonIgnore]
        public PostTensioningStrandType StrandType
        {
            get
            {
                return _strandType;
            }
            set
            {
                _strandType = value;

                if (value.Equals(PostTensioningStrandType.ThreeWireStrand) == true)
                {
                    _ptSection = new ThreeWireStrand(OuterDiameter);
                }
                else if (value.Equals(PostTensioningStrandType.SevenWireStrand) == true)
                {
                    _ptSection = new SevenWireStrand(OuterDiameter);
                }
                else if (value.Equals(PostTensioningStrandType.NineteenStrandBundle) == true)
                {
                    _ptSection = new NineteenStrandBundle(OuterDiameter, InnerStrandDiameter, InnerStrandType);
                }
            }
        }

        [JsonIgnore]
        public PostTensioningInnerSection PTSection
        {
            get
            {
                return _ptSection;
            }
        }

        [JsonIgnore]
        public double EncasedArea
        {
            get
            {
                return PI / 4.0 * Pow(OuterDiameter, 2);
            }
        }
        #endregion

        #region Constructors
        [JsonConstructor]
        public PostTensioningDefinition()
        {

        }

        public PostTensioningDefinition(string ptName, PostTensioningStrandType strand, IGenericMaterial mat, double diameter, double area) :
            this(ptName, strand, mat, diameter, area, PostTensioningStrandType.SevenWireStrand, 0.5)
        {

        }

        public PostTensioningDefinition(string ptName, PostTensioningStrandType strand, IGenericMaterial mat, double diameter, double area, PostTensioningStrandType innerStrand, double innerStrandDiam)
        {
            Name = ptName;
            OuterDiameter = diameter;
            Material = mat;
            SteelArea = area;
            InnerStrandType = innerStrand;
            InnerStrandDiameter = innerStrandDiam;
            StrandType = strand;
        }
        #endregion
    }
}
