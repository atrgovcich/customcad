﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinforcedConcrete.PostTensioning
{
    public abstract class MultiStrandBundle : PostTensioningInnerSection
    {
        #region Public Properties
        public PostTensioningStrandType TypeOfInternalStrand { get; set; }
        public double InternalStrandDiameter { get; set; }
        #endregion

        #region Constructors
        protected MultiStrandBundle(double outsideDiameter) : base(outsideDiameter)
        {

        }
        #endregion
    }
}
