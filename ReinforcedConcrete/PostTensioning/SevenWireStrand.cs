﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Math;
using Utilities.Geometry;
using static Utilities.Geometry.GeometryFunctions;

namespace ReinforcedConcrete.PostTensioning
{
    public class SevenWireStrand : PostTensioningInnerSection
    {
        #region Constructors
        public SevenWireStrand(double outsideDiameter) : base(outsideDiameter)
        {
            Name = "Seven Wire Strand";
            _numberOfWires = 7;
            StrandType = PostTensioningStrandType.SevenWireStrand;
        }
        #endregion

        #region Overridden Public Methods
        public override List<GenericCircle> GetWireCircles(PointD c)
        {
            List<GenericCircle> circleList = new List<GenericCircle>();

            double wireDiam = OuterDiameter / 3.0;

            double angleBetween = 360.0 / (_numberOfWires - 1.0);

            //Define the central circle
            GenericCircle c0 = new GenericCircle(c, wireDiam / 2.0);

            circleList.Add(c0);

            //Define the exterior circles
            for (double i = 0; i <= 359; i += angleBetween)
            {
                double angleRad = i * 2.0 * PI / 360.0;

                PointD centroid = new PointD(c.X + wireDiam * Cos(angleRad), c.Y + wireDiam * Sin(angleRad));

                GenericCircle cNext = new GenericCircle(centroid, wireDiam / 2.0);

                circleList.Add(cNext);
            }

            return circleList;
        }
        #endregion
    }
}
