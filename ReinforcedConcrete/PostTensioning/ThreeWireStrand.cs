﻿using System;
using System.Collections.Generic;
using System.Text;
using Utilities.Geometry;
using static System.Math;
using static Utilities.Geometry.GeometryFunctions;

namespace ReinforcedConcrete.PostTensioning
{
    public class ThreeWireStrand : PostTensioningInnerSection
    {
        #region Constructors
        public ThreeWireStrand(double outsideDiameter) : base(outsideDiameter)
        {
            Name = "Three Wire Strand";
            _numberOfWires = 3;
            StrandType = PostTensioningStrandType.ThreeWireStrand;
        }
        #endregion

        #region Overridden Public Methods
        public override List<GenericCircle> GetWireCircles(PointD c)
        {
            List<GenericCircle> circleList = new List<GenericCircle>();

            double angleBetween = 120.0;

            for (double i = 30.0; i <= 359.0; i += angleBetween)
            {
                double angleRad = i * 2.0 * PI / 360.0;

                double l0 = OuterDiameter / 2.0 * 0.25;

                double a0 = angleRad - (angleBetween / 2.0) * 2.0 * PI / 360.0;
                double a1 = angleRad + (angleBetween / 2.0) * 2.0 * PI / 360.0;

                PointD p0 = new PointD(c.X + l0 * Cos(a0), c.Y + l0 * Sin(a0));
                PointD p1 = new PointD(c.X + l0 * Cos(a1), c.Y + l0 * Sin(a1));
                PointD p2 = new PointD(c.X + OuterDiameter / 2.0 * Cos(angleRad), c.Y + OuterDiameter / 2.0 * Sin(angleRad));

                CircleDefinition circleDef = GetThreePointCircleEquation(p0, p1, p2);

                GenericCircle cNew = new GenericCircle(circleDef.Centroid, circleDef.Radius);

                circleList.Add(cNew);
            }

            return circleList;
        }
        #endregion
    }
}
