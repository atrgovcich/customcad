﻿using System;
using System.Collections.Generic;

namespace ReinforcedConcrete.PostTensioning
{
    public static class PostTensioningMethods
    {
        public static PostTensioningDefinition GetPostTensioningDefinitionFromName(string name, List<PostTensioningDefinition> ptList)
        {
            for (int i = 0; i < ptList.Count; i++)
            {
                if (String.Compare(name, ptList[i].Name, ignoreCase: true) == 0)
                {
                    return ptList[i];
                }
            }

            return null;
        }
    }
}
