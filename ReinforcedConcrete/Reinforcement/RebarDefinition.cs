﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinforcedConcrete.Reinforcement
{
    public class RebarDefinition
    {
        public double Diameter { get; set; }
        public double Area { get; set; }
        public string Name { get; set; }
    }
}
